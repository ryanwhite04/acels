---
title: Australia-China Emerging Leaders Summit
description: The Signature Initiative of the Australia-China Youth Association
type: page
---

<style>
    h2 {
        color: #BE1302
    }
    blockquote img{
        float: left;  
        clear: both;  
        width: 240px;
        margin: 24px;  
    }
    blockquote img:nth-of-type(2n){
        float: right;
    }
    #logos img {
        width: 120px;
    }
</style>

## What is ACELS?

The Australia-China Emerging Leaders Summit (ACELS) is a signature initiative of the Australia-China Youth Association. ACELS is hosted twice annually, once in Australia and once in China annually and focuses on developing a generation of Indo-Pacific literate young professionals. 

{{< youtube id="w1fOo5dIEeQ" autoplay="true" >}}

## What is the Australia-China Youth Association?

The Australia-China Youth Association (ACYA) is the preeminent stakeholder for youth in Australia-China space. ACYA is the only non-for-profit non-governmental organisation devoted to promoting engagement between students and young professionals in Australia and China. ACYA has a vast network across Australia and Greater China with 18 Australian and 7 Chinese chapters comprising of over 6,000 members. Since foundation in 2008, ACYA has been the premier platform for youth engagement in the bilateral relationship and continues to strengthen relationship through our three core pillars:

- Education,
- Careers, and
- People-to-People Exchange.

## Why ACELS Perth?

{{< youtube id="cF4VPGeMkyo" autoplay="true" >}}

Entering its seventh year, ACELS Perth will be the ninth iteration of the Australia China Youth Association signature initiative. This will be the first time that the conference will be held in Western Australia and will capitalise on the unique Australia-China partnerships in Western Australia. Now more than ever is the best time to explore the Western Australia-China relationship as the local economy undergoes a diversification in non-mining sectors.

As Australia’s only state that shares a common time zone with China, Western Australia not only offers a unique geographical advantage compared to the Eastern States but represents 80% of China's investment into Australia over the past five years.

It is these reasons why ACYA has selected Perth to be the location for the ninth iteration of the Australia-China Emerging Leaders Summit.

As we see the emergence of the Indo-Pacific region, delegates will explore the current Australia-China relationship and how this complementary relationship will develop in the Asian century. From this basis, the theme that has been selected for ACELS Perth is: **Australia-China: a complementary partnership in an emerging Indo-Pacific**

## Testimonials

> ![Julie Bishop](/images/profiles/Julie Bishop.jpg)
> Events like ACELS, which unite passionate and talented youth in the Australia-China space, are essential to nurture the future of the bilateral relationship. Strengthening people-to-people connections and enhancing youth engagement is at the heart of the New Colombo Plan, which is supporting more than 4700 Australian undergraduates to live, study and work in China.  
> -- Julie Bishop, Former Minister for Foreign Affairs 

> ![Jan Adams](/images/profiles/Jan Adams.jpg)
> The people-to-people connections between Australia and China are the real strength of the relationship … ACELS is a particularly important part of that, because we need young leaders to have full exposure to each other in China and Australia to keep improving and expanding these networks.  
> -- Jan Adams, Australia’s Ambassador to the People’s Republic of China.

## Schedule

- **Day 1: Theme: Human Mobility: A Globalised World**
    - Delegates arrive
    - Lunch
    - Introduction
    - Bonding activity
    - Global Talk: Immigration to Australia
    - Free Time/KTV
- **Day 2: Diplomacy: The Australia-China Narrative**
    - China Consulate Tour
    - Lunch
    - Theme Panel Discussion
    - State Parliament House tour
    - Theme Masterclass
    - Technology and Financial Services Talk
    - Sponsored Networking Dinner
    - Office Tour
- **Day 3: International Trade, Imports and Exports**
    - Mining Company Tour
    - Panel Discussion
    - Lunch
    - Workshop
    - Gala Dinner
- **Day 4: Soft Power, Influence and Culture**
    - King's Park and downtown public art tour
    - Farewell brunch
    - Delegates depart

## Contact Us

Send us a message [here](/contact-us) or to <acels@acya.org.au>

## Current Sponsors

{{< figure class="logo" link="//tourism.wa.gov.au" src="/images/logos/Tourism Western Australia.jpg" title="Tourism Western Australia" >}}
{{< figure class="logo" link="//pcb.com.au" src="/images/logos/Perth Convention Bureau - Wide.jpg" title="Perth Convention Bureau" >}}

## Previous Sponsors

{{< figure class="logo" src="/images/logos/Schwarzmann Scholars.png" title="Schwarzmann Scholars" >}}
{{< figure class="logo" src="/images/logos/King & Wood Mallesons.png" title="King & Wood Mallesons" >}}
{{< figure class="logo" src="/images/logos/Herbert Smith Freehills.png" title="Herbert Smith Freehills" >}}
{{< figure class="logo" src="/images/logos/Australian Government.svg" title="Australian Government" >}}
{{< figure class="logo" src="/images/logos/Trade Victoria.png" title="Trade Victoria" >}}
{{< figure class="logo" src="/images/logos/Australia-China Business Council.jpg" title="Australia-China Business Council" >}}
{{< figure class="logo" src="/images/logos/China Studies Centre.jpg" title="China Studies Centre" >}}
{{< figure class="logo" src="/images/logos/Westpac.png" title="Westpac" >}}
{{< figure class="logo" src="/images/logos/Department of Foreign Affairs and Trade.png" title="Department of Foreign Affairs and Trade" >}}
{{< figure class="logo" src="/images/logos/Geoff Hardy.jpg" title="Geoff Hardy" >}}
{{< figure class="logo" src="/images/logos/KPMG.svg" title="KPMG" >}}
{{< figure class="logo" src="/images/logos/Foundation for Australian Studies in China.jpg" title="Foundation for Australian Studies in China" >}}
{{< figure class="logo" src="/images/logos/Australian Studies Centre.png" title="Australian Studies Centre" >}}
{{< figure class="logo" src="/images/logos/ShineWing.png" title="ShineWing" >}}
{{< figure class="logo" src="/images/logos/Australia-China Council.svg" title="Australia-China Council" >}}
{{< figure class="logo" src="/images/logos/Austrade.svg" title="Austrade" >}}
{{< figure class="logo" src="/images/logos/Confucius Institute.png" title="Confucius Institute" >}}
{{< figure class="logo" src="/images/logos/The University of Melbourne.png" title="The University of Melbourne" >}}