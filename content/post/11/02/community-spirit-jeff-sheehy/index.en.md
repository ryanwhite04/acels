{
  "title": "Community Spirit",
  "author": "ACYA",
  "date": "2011-02-01T19:00:55+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<footer>- Jeff Sheehy</footer>
<p>I have been back in Brisbane now for a month after spending a semester studying Chinese at Peking University. It was quite a timely return I must say. I got home on the morning of January 13, just as the Brisbane River was peaking. It was quite a surreal experience. 24 hours earlier I had been waiting in a very cold Beijing for my flight home. The following day I was to find myself caked from head to toe in thick mud as a group of us helped out a friend's place (I made sure I was the muddiest so it looked like I was doing the most work...). The generosity and community spirit was something I didn't realise still existed in this tech-savvy world where I thought "community" didn't really have much to do with where you lived. But alas, it did.</p>

<!--more-->

<p>On Friday afternoon I found myself in a 70 person strong sandbag line. My friend's neighbour's house backed onto the Brisbane River, and owing to the torrent of water passing by his backyard, the bank was starting to give way. I suspect out of the 70 odd people there that about a handful actually knew who lived in the house, but nonetheless, people stayed for about three hours of tough physical labour to get the sandbags to the bank. Strangers brought food and bottled water and the jovial spirit that filled the air was quite heartening.</p>

<p>Over the coming days back at my friend's house, we often had to turn volunteers away, and even on one occasion the State Emergency Service, who begrudged that they had been put out of business by the army of volunteers. On Saturday, 11,500 people showed up to one single council volunteer centre to be bused out to disaster-hit areas. On that same day, a group of men came to my friend's house from Noosa (about a two hours drive away) and managed to shift, in one morning, what I would estimate to be about a tonne's worth of furniture the 50m down the driveway. One of them, much to our bewilderment, was an ex-world number one tennis player from the 1990s - again, a heartening and surreal experience!</p>

<p>The floods also affected the University of Queensland where around 2000 Chinese students come to study. A lot of the sporting fields next to the river have become contaminated as a result of the inundation, swimming pools will be out of action as well the tennis courts for quite some time. The vast majority (if not all) of the teaching buildings remained unaffected.</p>

<p>After a few quieter days the following week, I recalled how a Chinese ACYD delegate had told me how she and her friend's had travelled to Sichuan after the devastating earthquake of 2008. They had travelled a long distance simply to comfort those in need or donate their time to some useful cause in the aftermath of one of China's worst natural disasters. Recently, both China and Australia have sent rescue teams to Christchurch following the earthquake there.</p>

<p>While we come from different cultures and speak different languages, perhaps we Australians and Chinese are not that dissimilar after all!</p>