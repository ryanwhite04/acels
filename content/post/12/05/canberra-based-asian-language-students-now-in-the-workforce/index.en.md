{
  "title": "Canberra-based Asian language students, now in the workforce",
  "author": "ACYA",
  "date": "2012-05-18T09:27:58+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>Did you study an Asian language at university? Are you now in the workforce and based in Canberra? If yes, we are looking to recruit you to take part in a focus group. The focus group will explore the experiences of those who studied an Asian language at university, their motivations for doing so, and their experiences. The focus group will also look at whether participants are currently using their language skills in their current job and whether this language experience is valued by their current and previous employers. The focus group will bring together around 6 participants who will all discuss and share their thoughts on the topic.</p>
<p>The focus group will be held from 2-4pm on Sunday 27 May at the Australian National University. The group is being conducted by four graduate students completing a Social Research Design course with the Australian Demographic and Social Research Institute. </p>
<p>If you are interested please email Veronica at vronwa@hotmail.com for further information. </p>