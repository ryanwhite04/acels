{
  "title": "ACYA's Andrea Myles on \"Engaging Asia\" panel at Sydney's Vivid festival",
  "author": "ACYA",
  "date": "2012-05-22T01:41:26+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>There are 1.2 billion people under the age of 30 in China and India. In China alone, a blossoming cultural explosion represents a huge new market: there are 322 million people aged between 16 and 30. So how do creative industries identify, connect with, and engage our peers in the region?</p>
<p>National Director of the Engaging China Project Andrea Myles joins trend spotter and DaBaoGe founder Andy Miller, alongside Kerry Comerford from Andrew Lloyd Webber’s Really Useful Company, with more guests to be announced.</p>
<p>June 4, 7:30pm - 9:30pm. Tickets $25.</p>
<p><a href="http://www.vividsydney.com/events/engaging-asia-presented-by-creative-sydney/" target="_blank" style="color: #1155cc; font-family: arial, sans-serif; font-size: 13px; line-height: normal; background-color: rgba(255, 255, 255, 0.917969);">http://www.vividsydney.com/events/engaging-asia-presented-by-creative-sydney/</a><span style="color: #222222; font-family: arial, sans-serif; font-size: 13px; line-height: normal; background-color: rgba(255, 255, 255, 0.917969);">&nbsp;</span></p>