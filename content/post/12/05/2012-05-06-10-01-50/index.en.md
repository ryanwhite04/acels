{
  "title": "建立者致词",
  "author": "ACYA",
  "date": "2012-05-06T10:01:50+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="centercontent" style="background-color: transparent; border-width: 0px; font-style: normal; font-weight: normal; margin: 0px 0px 0px 10px; padding: 0px; outline-width: 0px; vertical-align: baseline; width: 520px; float: left; overflow: hidden; color: #555555; font-family: Helvetica,Arial,sans-serif; font-size: 13px; font-variant: normal; letter-spacing: normal; line-height: 16px; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div class="clearpad" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 3px; outline-width: 0px; vertical-align: baseline;">
<div class="item-page" style="background-color: #ffffff; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 7px; outline-width: 0px; vertical-align: baseline; position: relative; color: #555555;">
<p style="margin-top: 0px; margin-bottom: 10px; color: #000000; font-family: 'Lucida Sans Unicode'; font-size: 13px; line-height: 18px;">ACYA was founded in 2008 when Australian university students studying in Beijing got together to find ways to improve information resources available to young Australians who had an interest in China. It was felt that the information provided by universities was too general, out-of-date and didn’t cater to the everyday needs of most students.</p>
<p style="margin-top: 0px; margin-bottom: 10px; color: #000000; font-family: 'Lucida Sans Unicode'; font-size: 13px; line-height: 18px;">In the Beijing student district of Wudaokou, Henry Makeham, Huw Pohlner and a group of like-minded undergraduate exchange students decided on the objective and form of their information resource: an organisation would be established that would provide services for young Chinese and Australians with an interest in forging relations between their two countries. The organisation would actively bring people together to share and learn from each other to achieve this end, and would be based on three pillars: Careers, Education and People-to-People Exchange.</p>
<p style="margin-top: 0px; margin-bottom: 10px; color: #000000; font-family: 'Lucida Sans Unicode'; font-size: 13px; line-height: 18px;">Since 2008, ACYA has exceeded the greatest expectations: it has endured, grown to over 2000 members, provided countless opportunities to students and young professionals and strengthened the Australia-China relationship. Growing rapidly, ACYA now has several initiatives in addition to the main Association: The Australia-China Youth Dialogue (ACYD), the Engaging China high schools program and the Australia-China Young Professionals Initiative (ACYPI). Each has only been made possible through the tireless work of a team of volunteers across Australia and China (nobody in ACYA gets paid a cent) and incredible support from business, government and the community.</p>
<p style="margin-top: 0px; margin-bottom: 10px; color: #000000; font-family: 'Lucida Sans Unicode'; font-size: 13px; line-height: 18px;">ACYA has followed an important growth trajectory as we have built support networks and gained access to the highest levels of government and academia but kept our target audience - you! - front of mind. This is ultimately a creation of young people, for young people. If you want to be involved in this exciting organisation, if you have ideas about things we should be doing, if you care about the future of ACYA as much as we do, we would love to hear from you.</p>
<p style="margin-top: 0px; margin-bottom: 10px; color: #000000; font-family: 'Lucida Sans Unicode'; font-size: 13px; line-height: 18px;" align="center">Henry Makeham                                Huw Pohlner</p>
</div>
</div>
</div>
<div id="rightbar-w" style="background-color: transparent; border-width: 0px; font-style: normal; font-weight: normal; margin: 0px; padding: 5px; outline-width: 0px; vertical-align: baseline; width: 200px; float: right; color: #555555; font-family: Helvetica,Arial,sans-serif; font-size: 13px; font-variant: normal; letter-spacing: normal; line-height: 16px; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div id="sidebar" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline-width: 0px; vertical-align: baseline;">
<div class="module" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px 0px 15px; padding: 3px; outline-width: 0px; vertical-align: baseline; border-bottom-left-radius: 7px; border-bottom-right-radius: 7px; width: 190px;">
<div class="inner" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline-width: 0px; vertical-align: baseline;">
<div class="h3c" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline-width: 0px; vertical-align: baseline;">
<div class="h3r" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline-width: 0px; vertical-align: baseline;">
<div class="h3l" style="background-color: transparent; border-width: 0px; font-style: inherit; font-weight: inherit; margin: 0px; padding: 0px; outline-width: 0px; vertical-align: baseline;"> </div>
</div>
</div>
</div>
</div>
</div>
</div>