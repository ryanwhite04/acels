{
  "title": "Applications for AustCham Scholarships now open!",
  "author": "ACYA",
  "date": "2012-05-06T06:24:32+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph;"><span style="font-family: 'Calibri','sans-serif'; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; color: black; mso-themecolor: text1;" lang="EN-US">Applications for the 2013 AustCham Graduate Scholarships are now open!</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; mso-pagination: none; mso-layout-grid-align: none; text-autospace: none;"><span style="font-family: 'Calibri','sans-serif'; mso-bidi-font-family: Arial; color: black; mso-themecolor: text1;" lang="EN-US">The AustCham Scholarship scholarship is designed for students who have a long term passion for strengthening this relationship through their business or cultural pursuits. A typical candidate will be in their final year of studies (or have recently graduated) and have undertaken China focused work experience or academic studies. </span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; mso-pagination: none; mso-layout-grid-align: none; text-autospace: none;"><span style="color: black; font-family: Calibri, sans-serif;">The scholarship will place talented Australian graduates in key Australian industries in China through its comprehensive language, mentoring and work placement program.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph; mso-pagination: none; mso-layout-grid-align: none; text-autospace: none;"><span style="color: black; font-family: Calibri, sans-serif;">Although Mandarin language skills are not required to apply for this position, candidates who have taken the initiative in developing core cultural and language skills will be highly regarded.</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph;"><span style="color: black; font-family: Calibri, sans-serif;">For more information contact Sarah Liu:</span></p>
<p class="MsoNormal" style="text-align: justify; text-justify: inter-ideograph;"><span lang="EN-US"><a href="mailto:graduatecareers@austcham.org"><span style="font-family: Calibri, sans-serif; color: black;">graduatecareers@austcham.org</span></a></span><span style="font-family: 'Calibri','sans-serif'; mso-bidi-font-family: Arial; color: black; mso-themecolor: text1;" lang="EN-US"> or visit:</span><span style="font-family: 'Calibri','sans-serif'; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; color: black; mso-themecolor: text1;" lang="EN-US"> http://austcham.org/resources/austcham-china-scholarship-program</span></p>