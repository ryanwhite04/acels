{
  "title": "ACYA Meets Kevin Rudd; helps him reach out to international students",
  "author": "ACYA",
  "date": "2012-05-24T14:24:36+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:PunctuationKerning/>
  <w:DrawingGridVerticalSpacing>7.8 pt</w:DrawingGridVerticalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
  <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:SpaceForUL/>
   <w:BalanceSingleByteDoubleByteWidth/>
   <w:DoNotLeaveBackslashAlone/>
   <w:ULTrailSpace/>
   <w:DoNotExpandShiftReturn/>
   <w:AdjustLineHeightInTable/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>
<![endif]--></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="MsoNormal"><span lang="EN-AU">On 16 May 2012, representatives from ACYA UNSW and USYD met with Kevin Rudd at Macquarie University. Mr Rudd held an intimate and frank roundtable discussion with students from five universities – UNSW, USYD, Macquarie, UTS and the University of Wollongong – on problems faced by Chinese international students in Sydney.</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="MsoNormal"><span lang="EN-AU">Safety and travel concessions were recurrent issues at the meeting. Mr Rudd inquired of participants whether they had ever experienced violence or racism, and said he was interested in learning whether Chinese international students encounter particular problems in Australia that other international students do not have. He urged anyone who was the victim of an attack to communicate the incident with him through his <a href="http://www.weibo.com/u/2726223703">Weibo</a>. At the same time, he stressed common sense, such as avoiding pubs and isolated areas late at night. On the topic of international student travel concessions, he promised to speak with the NSW and Victorian State premiers.</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="MsoNormal"><span lang="EN-AU">ACYA pointed out to Mr Rudd that at many universities, the general lack of integration between local and Chinese students was a broader problem. Mr Rudd expressed his hope for more interaction between the two student bodies, while noting the need for more Australians to study in Asia. Finally, he affirmed that university is the best opportunity for young Australians and young Chinese to form rich and lasting cross-cultural friendships.</span></p>
<p>&nbsp;</p>