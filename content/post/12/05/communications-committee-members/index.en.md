{
  "title": "Communications Committee Members",
  "author": "ACYA",
  "date": "2012-05-18T09:32:19+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><span style="color: #000000; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 11px; line-height: 14px; text-align: left;">ACYA is seeking Communications Committee Members to help build our online presence in both Chinese and English. International students are encouraged to apply! Contact joel.wing-lun@acya.org.au for more information.</span></p>