{
  "title": "Journal",
  "author": "ACYA",
  "date": "2012-05-05T06:42:09+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<img style="display: block; margin-left: auto; margin-right: auto;" alt="Chinese-Umbrellas" src="images/Chinese-Umbrellas.jpg" width="430" height="111" />

The <em>ACYA Journal of Australia-China Relations</em> (“the ACYA Journal”) project was launched in 2011 as a platform for Australian and Chinese students and young professional to publish in-depth academic essays, high-quality opinion articles, and creative multimedia projects centred upon Australia-China affairs.
The 2012 ACYA Journal, renamed the '<em>ACYA Journal of Australia-China Affairs</em>', builds upon the success of the inaugural edition by widening its call for submissions to all Australian and Chinese universities, making the Journal completely bilingual, obtaining an ISSN, instituting a process of academic peer-review for the essay component, and creating prizes for the best essay, opinion article, and creative piece submitted. The 2012 Journal was launched at the 2012 Australia China Youth Dialogue, held in Beijing and Chengdu, and is freely available in soft copy, as well as hard copies being distributed to ACYD delegates, ACYA sponsors and partners, and all ACYA Chapters across Australia and China.</div>


If you have any suggestions for content, questions, or would just like to find out more about how to be involved, please don't hesitate to contact Neil Thomas on <a href="mailto:neil.thomas@acya.org.au">neil.thomas@acya.org.au</a>.

2012 Journal

ISSN: 2201-3814

<a href="/Documents/Journals/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf">Click to view the 2012 Journal</a>
2011 Journal
<a href="/Documents/Journals/ACYA%20Journal%20FINAL.pdf">Click to view the 2011 Journal</a>
