{
  "title": "ACYA's Dimity Mannering: Orient express risks neglecting Europe",
  "author": "ACYA",
  "date": "2012-05-12T09:02:15+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><a href="http://www.smh.com.au/opinion/society-and-culture/orient-express-risks-neglecting-europe-20120510-1yfgb.html#ixzz1ue0Mirqc" style="color: #003399;"><span style="text-decoration: underline;">http://www.smh.com.au/opinion/society-and-culture/orient-express-risks-neglecting-europe-20120510-1yfgb.html#ixzz1ue0Mirqc</span></a></p>
<p>On any night at any of tens of networking events from Beijing to Jakarta, the air heaves with the low drone of Australian accents. Here the next generation of Australian professionals stand and mingle, swilling drinks.</p>
<p>They arrive as students, interns, or young graduates, embracing the blossoming Asia-Australia relationship and the abundance of consultancies, chambers of commerce, embassies, law firms and banks setting up across the continent.</p>
<p>There has never been a better time to be an Australian with dreams of Asia. The Australian government, private enterprise and interest groups are showering young people with scholarships and establishing valuable employment and networking channels.</p>
<p>The activities to promote the region and support Australians there are smart: Asia will be our cash cow in the coming century.</p>
<p>But while it makes sense, Australia's failure to acknowledge anywhere else as being of value should sound warning bells. We should not let ourselves be ignorant of what we miss when we fail to recognise a world outside the boom regions and beyond our direct line of vision. I have had experience of both: I live in Beijing, working in law; but I spent four years in Europe studying, and working for the European Union and related agencies.</p>
<p>Speaking in Brussels last week, the European Union Trade Commissioner, Karel De Gucht, articulated what Australia had grown deaf to amid the northern hemisphere's economic malaise: ''People should not forget that Europe is still the world's No. 1 economy.''</p>
<p>Add to that the fact the EU is Australia's second-largest trading partner - a place where a deal with a Belgian company means you have customers minutes away in Germany, the Netherlands or France. European businesses employ 500,000 staff in Australia. Of the world's top 20 innovative countries, 10 are in Europe. The EU is Australia's biggest foreign investor.</p>
<p>Whether passe or in an economic pit, Europe is, and will continue to be, a huge economic force - especially as it recovers from recession and expands into the eastern states and possibly Turkey. If Australia fails to encourage its younger citizens to think of a whole world rather than just a region next door, we risk missing out on economic opportunities that exist now and into the future.</p>
<p>Policymakers may argue that engaging solely with Asia will have limited economic effect. But this lack of engagement is impoverishing to our political, legal, social and cultural development.</p>
<p>As a student at university, my interest in Europe was sparked by the revelation that Europe was phasing out the battery-farmed egg. Perplexed as to how Australia could shrug it off as too hard, while a community stretching from Ireland to Bulgaria could change, opened up a new world of ideas and possibilities. If Australia considered Europe, might an answer be clearer?</p>
<p>Other areas where Europe trumps the world - human rights, environment and health - are all areas in which we need help. Lessons on how we might make progress in these areas will not come from Asia; we need people plugged into elsewhere.</p>
<p>But for students whose interests lie elsewhere, a future beyond the Orient looks lonely and financially burdensome. The irony is that the difficulty in pursuing study and work in Europe has little to do with its economic climate but rather the failure to promote or help fund the opportunities that exist.</p>
<p>For Australians, programs directed at Europe are scarce. Students interested in the region must compete for a pool of Australian scholarships against applicants who are also lavished with a huge number of scholarships open solely for Asia-focused projects.</p>
<p>The fact that Europe is not considered an area of ''national interest'' means selection committees can struggle to justify the value in funding an applicant to venture outside Asia.</p>
<p>Australian chambers of commerce and business groups also seem unaware of the need to promote Europe to a younger generation, making no effort to raise awareness of internships with the European Union, the international courts, the United Nations, NGOs and many private sector schemes, despite the value placed on anglophones. This leaves Australians wanting to work or study to find their own opportunities and to fund them independently.</p>
<p>The Asian century will be a good thing for Australia. However, we need to start asking ourselves what opportunities - economic and otherwise - we will miss if we offer no help to young people to engage with regions outside Asia, particularly Europe. As long as we refuse to put anywhere else on our agenda, we refuse to imagine what more we could be.</p>
<p><strong>Dimity Mannering is a Beijing-based law graduate and representative of the Australia-China Youth Association. She has also worked for the European Union.</strong></p>