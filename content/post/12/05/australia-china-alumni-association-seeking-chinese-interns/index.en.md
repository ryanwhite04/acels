{
  "title": "Australia China Alumni Association seeking Chinese interns",
  "author": "ACYA",
  "date": "2012-05-09T01:50:24+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:TargetScreenSize>1024x768</o:TargetScreenSize>
 </o:OfficeDocumentSettings>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:PunctuationKerning/>
  <w:DrawingGridVerticalSpacing>7.8 pt</w:DrawingGridVerticalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
  <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:SpaceForUL/>
   <w:BalanceSingleByteDoubleByteWidth/>
   <w:DoNotLeaveBackslashAlone/>
   <w:ULTrailSpace/>
   <w:DoNotExpandShiftReturn/>
   <w:AdjustLineHeightInTable/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:UseFELayout/>
  </w:Compatibility>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]--></p>
<table class="MsoNormalTable" style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 425.8pt; border: 1pt solid black; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Organisation Introduction</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Position Title</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Social Media and Development Assistant</span></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Location</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: 宋体;">北京市朝阳区八里庄西里</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">98</span><span style="font-size: 10pt; font-family: 宋体;">号</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: 宋体;">住邦</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">2000</span><span style="font-size: 10pt; font-family: 宋体;">商务中心</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">3</span><span style="font-size: 10pt; font-family: 宋体;">号楼</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">1206</span><span style="font-size: 10pt; font-family: 宋体;">室</span></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Duration</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Negotiable</span></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Associated Duties</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
<p class="MsoNormal"><em><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Online Presence</span></em><em><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> Management<br /> <br /> </span></em></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">       </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Identify, edit, upload and maintain content (text, images, video) on a daily basis for </span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">ACAA’s</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> bilingual websites and other online resources and platforms (e</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">.</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">g</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">.</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> Weibo</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">, RenRenWang, KaiXin, etc.</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">)</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">.</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"><br /> <br /> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">       </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Contribute to continuous improvements to the design, layout and creative content of the Embassy’s online resources and platforms.<br /> <br /> </span></p>
<p class="MsoNormal"><em><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Translation</span></em><em></em></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 18pt; text-indent: -18pt;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">         </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Translate relatively complex materials from English to Chinese and Chinese to English in a variety of formats (eg, media alerts, website materials, blog and microblog content).</span></p>
<p class="MsoNormal"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> </span></p>
<p class="MsoNormal"><em><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Other Work </span></em><em></em></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 18pt; text-indent: -18pt;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">         </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Assist maintenance of the </span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">ACAA</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> contact database.<br /> <br /> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 18pt; text-indent: -18pt;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">         </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Assist </span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">in event planning and management.</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"><br /> <br /> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 18pt; text-indent: -18pt;"><span style="font-size: 10pt; font-family: Symbol;" lang="EN-AU"><span>·<span style="font: 7pt 'Times New Roman';">         </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Provide back up for </span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">other</span><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> colleagues where required and perform other duties as directed.</span></p>
<span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"><br style="page-break-before: always;" clear="all" /> </span></span>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Desired Skills</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 12pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-US"><br /> <br /> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Ability to accurately translate materials from English to Chinese and from Chinese to English (eg, media reports and news releases, biographies, events and other information, microblog entries).</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Experience with Chinese Social Media platforms (e.g. Weibo, RenRenWang, KaiXinWang, etc.).</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial;" lang="EN-GB">Some previous experience with interpreting from English to Chinese and Chinese to English is desirable, but not essential.</span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Ability to provide effective administrative and event management support, and requisite skills including accuracy, timeliness and attention to detail.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Ability to communicate </span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">at a native level in oral and </span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">writ</span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">te</span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">n</span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Mandarin.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Ability to organise and prioritise workloads so as to complete given projects and assignments in a timely manner.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 24pt; text-indent: -24pt;"><span style="font-size: 10pt; font-family: Tunga; color: black;" lang="EN-GB"><span>-<span style="font: 7pt 'Times New Roman';">              </span></span></span><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB">Initiative; flexibility; adaptability; ability to operate effectively as a member of a work team.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial; color: black;" lang="EN-GB"> </span></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Working Hours / Conditions</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US">Hours: Negotiable</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US">Work: Unpaid</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US">Candidate must have available their own personal computer.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Required Documentation for Application</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US">1 page cover letter.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US">Maximum 2 page CV.</span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-US"> </span></p>
</td>
</tr>
<tr>
<td style="width: 425.8pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #0c0c0c; padding: 0cm 5.4pt;" colspan="2" valign="top" width="568">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Contact Information</span></strong></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Name</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Michael Dalic</span></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Phone Number</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">8586 6259 </span></p>
</td>
</tr>
<tr>
<td style="width: 175.5pt; border-right: 1pt solid black; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color black black; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% #999999; padding: 0cm 5.4pt;" valign="top" width="234">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU">Email Address</span></strong></p>
</td>
<td style="width: 250.3pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color black black -moz-use-text-color; padding: 0cm 5.4pt;" valign="top" width="334">
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"><a href="mailto:michael@austchinaalumni.org">michael@austchinaalumni.org</a></span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p class="MsoNormal"><span style="font-size: 10pt; font-family: Arial;" lang="EN-AU"> </span></p>
<p>{jcomments on}</p>
<p> </p>