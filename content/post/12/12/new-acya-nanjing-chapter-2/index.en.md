{
  "title": "<!--:en-->New ACYA Nanjing Chapter<!--:--><!--:zh-->New ACYA Nanjing Chapter<!--:-->",
  "author": "ACYA",
  "date": "2012-12-16T08:45:42+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->More than 35 people celebrated the Australia-China Youth Association (ACYA) Nanjing Chapter kick-off party at La Villa Bar &amp; Restaurant on Sunday 14 October.

The Nanjing Chapter is set to host regular Sunday get-togethers, Language Exchange sessions and social functions for the growing Australia-China community in the area.
To be placed on the mailing list for upcoming activities email <a href="mailto:acya.nanjing@acya.org.au">acya.nanjing@acya.org.au</a>

Emily Dunn, Communications Officer, ACYA Nanjing Chapter, 22 October 2012

<hr />

<a href="http://eepurl.com/rdVav">Sign up to the mailing list</a>
<a href="https://www.facebook.com/groups/ACYANanjing/">Join us on Facebook</a>
<a href="http://page.renren.com/601560975">Join us on Renren</a>
<a href="http://weibo.com/u/3051574965">Join us on Weibo</a>
<a href="http://us6.campaign-archive1.com/home/?u=e4737596af8e715aff88ba01e&amp;id=2019098365">Check out past newsletters</a>

Please contact ACYA's Nanjing's President Steve Carton (<a href="mailto: stevecarton@acya.org.au">stevecarton@acya.org.au</a>) for information on how you can get involved in ACYA in Nanjing.<!--:-->