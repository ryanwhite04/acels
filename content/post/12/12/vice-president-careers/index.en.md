{
  "title": "Vice-President (Careers)",
  "author": "ACYA",
  "date": "2012-12-03T10:08:40+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><b><span style="font-family: 'Arial','sans-serif';">Position: </span></b><span style="font-family: 'Arial','sans-serif';">Careers Vice-President</span><b><span style="font-family: 'Arial','sans-serif';"> <br /></span></b></p>
<p><b><span style="font-family: 'Arial','sans-serif';">Criteria:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Strong organisational and time management skills</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Ability to oversee a range of activities</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong networking skills / keen to facilitate relationships </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong ability to priorities and organise time effectively</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Vision and a knowledge of members’ interests </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Excellent communication skills (including mandarin)</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong understanding of ACYA”s history, processes and visions for the future</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Ability to deliver outcomes for the benefits of members</span></li>
</ul>
<p><span style="font-family: 'Arial','sans-serif';">&nbsp;</span><b><span style="font-family: 'Arial','sans-serif';">Core Duties and Responsibilities:&nbsp; </span></b><span style="font-family: 'Arial','sans-serif';"><br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Management of stakeholder relationships with partners</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Collation of information for and applications by members</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Update and maintain Careers section of the ACYA website</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Network with stakeholders to provide internships for ACYA members</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Maintain a visible presence on the ACYA Facebook, Linkedin pages</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Co-ordinate a sub-committee to search for internships to advertise</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Liaise and communicate with Careers VPs/Representatives from University Chapters </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Communicate with interested parties to put internship/job opportunities on website</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Work with various parties to manage the application process for internships</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Answering inquiries about internship opportunities</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Collating CVs and informing people of potential opportunities</span></li>
</ul>
<p><span style="font-family: 'Arial','sans-serif';">&nbsp;</span><b><span style="font-family: 'Arial','sans-serif';">Expectations:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Leadership experience</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Experience in ACYA </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong interest in Sino-Australian relations </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Ability to commit energy and time </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Cross-cultural fluency</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Management skills </span></li>
</ul>
<p><b><span style="font-family: 'Arial','sans-serif';">Time Commitment:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<p><span style="font-family: 'Arial','sans-serif';">5 – 10 Hours per week (fluctuates seasonally)</span></p>
<p><b><span style="font-family: 'Arial','sans-serif';">&nbsp;Length of Time Held Position: </span></b></p>
<p><span style="font-family: 'Arial','sans-serif';">&nbsp;1 year </span></p>