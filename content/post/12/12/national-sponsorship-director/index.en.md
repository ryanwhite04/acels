{
  "title": "National Sponsorship Director ",
  "author": "ACYA",
  "date": "2012-12-03T10:04:33+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Position: </span></b><span style="font-family: 'Arial','sans-serif';">National Sponsorship Director</span><b><span style="font-family: 'Arial','sans-serif';"> <br /></span></b></p>
<p class="PlaceholderText1CxSpLast"><b><span style="font-family: 'Arial','sans-serif';">Criteria:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Vision and passion to expand sponsorship opportunities </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong organisational and time management skills</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong networking skills / keen to facilitate relationships </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Excellent communication and negotiation skills </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Excellent English and sound Mandarin skills </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Knowledge of ACYA as an organisation </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Knowledge in sponsorship structure in an organisation and experience in sponsorship role is highly preferable but not determinative</span></li>
</ul>
<p class="PlaceholderText1CxSpLast"><b><span style="font-family: 'Arial','sans-serif';">Core Duties and Responsibilities: </span></b><span style="font-family: 'Arial','sans-serif';"><br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Build deep and ongoing relationships with existing sponsors </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Liaise and work with sponsors to co-ordinate events and update sponsors with opportunities or developments of ACYA in general</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Set up meetings with potential sponsors</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Draft and update sponsorship related documents e.g. pitch book, strategy proposal, sponsorship letters or emails etc. </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Search for sponsorship opportunities on a regular basis</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Plan and expand ACYA National sponsorship portfolio </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Devise and refine an ongoing sponsorship strategy for ACYA National. </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Co-ordinate and guide sponsorship efforts ACYA-wide.</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Develop and maintain ACYA’s National Sponsorship Database.</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Conduct ACYA’s Sponsor Feedback (Survey) on a regular basis</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Attend networking events and deliver pitch. </span></li>
</ul>
<p class="PlaceholderText1CxSpMiddle"><span style="font-family: 'Arial','sans-serif';">&nbsp;</span><b><span style="font-family: 'Arial','sans-serif';">Expectations:</span></b><span style="font-family: 'Arial','sans-serif';"></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Genuine commitment and passion to Sino-Australian relations</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Experience in sponsorship and leadership is preferable </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Experience in ACYA is desirable but not determinative</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Experience in professional networking and working in a corporate environment. </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Negotiations and Communications skills in English and Mandarin </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Mandarin skills for drafting sponsorship documents is preferable</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Teamwork and Management skills </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Organisation and time management skills</span><span style="font-family: 'Arial','sans-serif';"> <br /></span></li>
</ul>
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Time Commitment:</span></b><span style="font-family: 'Arial','sans-serif';"> </span></p>
<p class="PlaceholderText1CxSpMiddle"><span style="font-family: 'Arial','sans-serif';">10-15 hours average per week.</span><b><span style="font-family: 'Arial','sans-serif';"> <br /></span></b></p>
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Length of Time Held Position: </span></b></p>
<p class="PlaceholderText1CxSpLast"><span style="font-family: 'Arial','sans-serif';">1 year </span></p>
<p><span style="font-family: 'Arial','sans-serif';">&nbsp;</span></p>