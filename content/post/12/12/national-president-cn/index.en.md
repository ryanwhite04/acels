{
  "title": "National President (Cn)",
  "author": "ACYA",
  "date": "2012-12-03T09:58:32+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><span style="font-family: arial,helvetica,sans-serif;"><b>Position: </b>National President (China) </span></p>
<p><span style="font-family: arial,helvetica,sans-serif;"><b>Personal Criteria:</b> </span></p>
<ul>
<li><span style="font-family: arial,helvetica,sans-serif;">High level of commitment to ACYA and building communities;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Strong organisational, time management, and logistic skills;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Ability to lead a range of activities in diverse geographical locations and cultural environments;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Drive and ability to seek out and creatively develop new initiatives, projects, and events; </span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Strong networking skills and a dedication to facilitating institutional relationships; </span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Vision, understanding, and active knowledge of members’ interests; </span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Excellent communication skills, especially written; and</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Sound mandarin skills.</span><span style="font-family: arial,helvetica,sans-serif;"> <br /></span></li>
</ul>
<p><span style="font-family: arial,helvetica,sans-serif;"><b>Core Duties and Responsibilities: </b></span></p>
<ul>
<li><span style="font-family: arial,helvetica,sans-serif;">Leading the ACYA Executive;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Co-ordinating, being abreast of, and ensuring the efficacy of: daily extensive email and Skype correspondence; web-based meetings, project planning, and management; and ground-level Chapter activity;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Building ACYA’s public image, social media presence, and web profile;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Forming and maintaining positive relationships and working with sponsors and institutional partners, especially those based in the PRC – China Policy, Australia-China Studies Centres, AustCham, Australia-China Alumni Association, Future China, the All-China Youth Federation, the China University Media Union, China Youth Daily, Xinwen She, the Australian Embassy, TransAsia Lawyers, CSA, the International Students Offices at key Universities;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Regularly attending, participating in, and speaking at existing and potential sponsors’ and partners’ events;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Representing ACYA at sponsorship/collaboration meetings and negotiations;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Determining and reaching out to new sponsors and driving sponsorship pitches; </span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Overseeing and providing extensive support to ACYA Beijing, Shanghai, and Nanjing Chapters;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Promoting, managing, and expanding the activities of ACYA’s Chapters;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Growing ACYA’s Chapters;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Coordinating and attending regular social, educational, and careers Chapter events at least every fortnight, usually more regularly;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Coordinating the National Executive and projects with National President (Australia);</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Ensuring ACYA National actively and effectively engages with members, meets their needs, and provides support to them for developing their own initiatives and ideas;</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Working with Australia-China Youth Association Group – Australia-China Youth Dialogue, Australia-China Young Professionals’ Initiative, and the Engaging China Project</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Attending and contributing to ACYA-Group meetings; </span></li>
</ul>
<p><span style="font-family: arial,helvetica,sans-serif;"><b>Time Commitment:</b> </span></p>
<p><span style="font-family: arial,helvetica,sans-serif;">25-30 hours average per week. Expect highs pre-ACYD and in the lead up to the establishment of chapters. Composing and responding to email correspondence at least three times a day. </span></p>
<p><span style="font-family: arial,helvetica,sans-serif;"><b>Length of Time Held Position: </b></span></p>
<p><span style="font-family: arial,helvetica,sans-serif;">1 year </span></p>