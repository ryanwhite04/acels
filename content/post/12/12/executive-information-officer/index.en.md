{
  "title": "Executive Information Officer",
  "author": "ACYA",
  "date": "2012-12-03T10:45:07+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Position: </b>Executive Information Officer </span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Criteria:</b> </span></p>
<ul>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants must have experience working with the back end of websites</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants must have in depth knowledge of Joomla</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants must have a base understanding of HTML and CSS as a minimum</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants with Photoshop and Illustrator capabilities are preferred</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants must be highly reliable and trustworthy, with previous experience in a role of responsibility.</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">Applicants must show a keen interest in Australia-China relations</span></li>
</ul>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Core Duties and Responsibilities: </b></span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">The ACYA Executive Information Officer is directly responsible for the maintenance and development of all of ACYA’s online functions, including but not exclusive to, the ACYA website (<a href="http://www.acya.org.au">www.acya.org.au</a>), the ACYA Group’s email functions.</span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Expectations:</b> </span></p>
<ul style="margin-top: 0in;">
<li><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">Ability to work as part of a time</span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">Highly reliable </span></li>
<li><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">Prepared to commit time and energy </span></li>
</ul>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Time Commitment:</b> </span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">15-20 hours average per week. </span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;"><b>Length of Time Held Position: </b></span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 10pt;">1 year<b> </b></span></p>