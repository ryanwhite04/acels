{
  "title": "National Treasurer",
  "author": "ACYA",
  "date": "2012-12-03T10:37:30+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpFirst"><em><b><span style="font-family: 'Arial','sans-serif'; font-style: normal;">Position:</span></b></em><em><span style="font-family: 'Arial','sans-serif';"> </span></em><em><span style="font-family: 'Arial','sans-serif'; font-style: normal;">National Treasurer</span></em><em><span style="font-family: 'Arial','sans-serif';"></span></em></p>
<p style="page-break-after: avoid;"><em><span style="font-family: 'Arial','sans-serif'; font-style: normal;"></span></em><b><span style="font-family: 'Arial','sans-serif'; color: black;">Criteria:</span></b><span style="font-family: 'Arial','sans-serif'; color: black;"> </span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Strong organizational skills</span></li>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Good time management skills</span></li>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Sound communication skills</span></li>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Good interpersonal skills</span></li>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Highly responsible individual </span><span style="font-family: 'Arial','sans-serif'; color: black;"></span></li>
<li><span style="font-family: 'Arial','sans-serif'; color: black;">Book keeping experience (preferred) </span><span style="font-family: 'Arial','sans-serif'; color: black;"></span><em><span style="font-family: 'Arial','sans-serif'; font-style: normal;"> <br /></span></em></li>
</ul>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><b><span style="font-family: 'Arial','sans-serif'; color: black;">Core Duties and Responsibilities:</span></b><em><span style="font-family: 'Arial','sans-serif';"></span></em></p>
<p><span style="font-family: 'Arial','sans-serif';"><span></span></span><span style="font-family: 'Arial','sans-serif';">Budget</span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Create a budget that is responsible and prudently allocates funds to the most important projects</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Adhere to the budget, but also be flexible for worthy projects</span><span style="font-family: 'Arial','sans-serif';"> </span></li>
</ul>
<p><span style="font-family: 'Arial','sans-serif';">Financial Reporting</span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Be transparent</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Keep transparent and clear records</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Keep organised records</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Reconcile bank accounts with personal accounts at least once per semester</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Produce income statements and balance sheets for the national conferenc<b>e</b></span></li>
</ul>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';"></span></b><span style="font-family: 'Arial','sans-serif';"><span></span></span><span style="font-family: 'Arial','sans-serif';">Reimbursement</span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Be as prompt as possible</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Have a mind for future expenditures</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Plan out all reimbursements for the foreseeable future</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Keep a good record of all reimbursements, and reconcile with financial records and bank statement</span></li>
</ul>
<p style="page-break-after: avoid;"><b><span style="font-family: 'Arial','sans-serif'; color: black;">Expectations:</span></b><span style="font-family: 'Arial','sans-serif'; color: black;"></span></p>
<ul style="margin-top: 0in;">
<li style="color: black; margin-bottom: 0.0001pt; page-break-after: avoid;"><span style="font-family: 'Arial','sans-serif';">Able to communicate and liaise effectively with various Chapters, National Executive</span><span style="font-family: 'Arial','sans-serif';">s, Sponsors and other payees/payers.</span><span style="font-family: 'Arial','sans-serif';"></span></li>
<li style="color: black; margin-bottom: 0.0001pt; page-break-after: avoid;"><span style="font-family: 'Arial','sans-serif';">Be timely with communications and execution of payments/reimbursements </span><span style="font-family: 'Arial','sans-serif';"></span></li>
<li style="color: black; margin-bottom: 0.0001pt; page-break-after: avoid;"><span style="font-family: 'Arial','sans-serif';">Highly reliable and trustworthy</span><span style="font-family: 'Arial','sans-serif';"></span></li>
</ul>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif'; color: black;"></span><b><span style="font-family: 'Arial','sans-serif';">Time Commitment</span></b></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">5</span><span style="font-family: 'Arial','sans-serif';">-1</span><span style="font-family: 'Arial','sans-serif';">0</span><span style="font-family: 'Arial','sans-serif';"> hours per week.</span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Length of Time Held Position</span></b><span style="font-family: 'Arial','sans-serif';"></span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpLast"><span style="font-family: 'Arial','sans-serif';">1 year</span><span style="font-family: 'Arial','sans-serif';"></span></p>