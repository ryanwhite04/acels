{
  "title": "National Publications Director",
  "author": "ACYA",
  "date": "2012-12-03T10:06:04+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><b><span style="font-family: 'Arial','sans-serif';">Position</span></b><span style="font-family: 'Arial','sans-serif';">: Publications Director <br /></span></p>
<p><b><span style="font-family: 'Arial','sans-serif';">Personal Criteria: <br /></span></b></p>
<ul style="margin-top: 0in;">
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Excellent English-language written communication skills</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Proficient in the intermediate-advanced use of publishing software such as Microsoft Publisher, Mailchimp, Microsoft Word, and others</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Strong leadership skills and confidence</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Excellent people-to-people communications skills including ability to organise and chair meetings, manage committees, task-master, and delegate</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Extremely well-organised with excellent strategic planning and time-management skills</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Willingness to sometimes put in the hard yards when something just needs to be done</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Interest in and commitment to advancing Australia-China relations through publication media</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Experience within ACYA preferable</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Intermediate-Advanced degree of English-Chinese bilingualism preferable</span><b><span style="font-family: 'Arial','sans-serif';"> <br /></span></b></li>
</ul>
<p><b><span style="font-family: 'Arial','sans-serif';">Core Duties and Responsibilities:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<ul style="margin-top: 0in;">
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Coordinating, managing, and editing all ACYA publications</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Responsible for compiling, publishing, and promoting monthly ACYA AustraliaBites newsletter. E.g. <a href="http://acya.org.au/Australia%20Bites/AustraliaBites%20Issue%209.pdf"></a><a href="http://acya.org.au/Australia%20Bites/AustraliaBites%20Issue%209.pdf">http://acya.org.au/Australia%20Bites/AustraliaBites%20Issue%209.pdf</a></span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Responsible for compiling, publishing, and promoting monthly ACYA ChinaBites newsletter (with ACYA ChinaBites Sub-Committee). E.g. <a href="http://acya.org.au/China%20Bites/ChinaBites%20Issue%207.pdf"></a><a href="http://acya.org.au/China%20Bites/ChinaBites%20Issue%207.pdf">http://acya.org.au/China%20Bites/ChinaBites%20Issue%207.pdf</a></span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Responsible for the compiling, publishing, and promotion of the annual ACYA Journal of Australia-China Affairs (with ACYA Journal Sub-Committee). E.g. <a href="http://acya.org.au/Publications/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf"></a><a href="http://acya.org.au/Publications/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf">http://acya.org.au/Publications/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf</a></span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Responsible for the establishment of the ongoing planned-to-be-annual ACYA Opportunities Guide Project (with ACYA Opportunities Guide Sub-Committee). See: <a href="http://acya.org.au/index.php/en/publications/opportunities-guide"></a><a href="http://acya.org.au/index.php/en/publications/opportunities-guide">http://acya.org.au/index.php/en/publications/opportunities-guide</a></span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Innovating and improving the content, layout, and distribution channels of all existing ACYA Publications</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Sourcing translators in coordination with the ACYA National Executive to improve the bilingual reach of all ACYA publications</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Managing, task-mastering, and chairing the meetings of all Publications Portfolio Sub-Committees</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Working closely with the Sponsorships Director to establish publications-related ACYA sponsorship drives</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Liaising with ACYD, ACYPI, and ECP to inform and encourage the awareness, dissemination, and use of ACYA publications across the Australia-China Group</span></li>
<li style="margin-bottom: 0.0001pt;"><span style="font-family: 'Arial','sans-serif';">Attending and contributing to all ACYA National Executive meetings</span><span style="font-family: 'Arial','sans-serif';"> <br /></span></li>
</ul>
<p><b><span style="font-family: 'Arial','sans-serif';">Time Commitment:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<p><span style="font-family: 'Arial','sans-serif';">Approximately 10-15 hours per week. More during the last week of each month when AustraliaBites and ChinaBites are published. Much more in the lead up to the publishing of the ACYA Journal at the ACYD.</span></p>
<p><b><span style="font-family: 'Arial','sans-serif';">Length of Time to Hold Position: <br /></span></b></p>
<p><span style="font-family: 'Arial','sans-serif';">1 Year</span></p>
<p><span style="font-size: 12pt; font-family: 'Arial','sans-serif';"><br style="page-break-before: always;" clear="all" /> </span></p>