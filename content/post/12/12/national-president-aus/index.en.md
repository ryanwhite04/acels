{
  "title": "National President (Aus)",
  "author": "ACYA",
  "date": "2012-12-03T09:51:26+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><b><span>Position: </span></b><span>National President (Aus) </span></p>
<p><b><span>Criteria:</span></b><span> </span></p>
<ul style="margin-top: 0in;">
<li><span>Ability to respond to emails quickly</span></li>
<li><span>Excellent time management skills</span></li>
<li><span>Strong managerial skills and awareness</span></li>
<li><span>Preparedness to oversee a range of different initiatives and different personnel </span></li>
<li><span>Excellent networking skills</span></li>
<li><span>Strong ability to facilitate/lead projects where appropriate</span></li>
<li><span>Sound knowledge of members’ interests and expectations</span></li>
<li><span>Strong understanding of ACYA’s history, processes and vision for future</span></li>
<li><span>Excellent communication skills (incl. mandarin)</span></li>
<li><span>Ability to plan and strategic awareness of ACYA’s current and future position</span></li>
</ul>
<p><b><span>Core Duties and Responsibilities: </span></b></p>
<ul style="margin-top: 0in;">
<li><span>Managing the overall development of ACYA in Australia, including:</span>
<ul style="margin-top: 0in;">
<li><span>Chapter development (there are now 11 chapters in Australia)</span></li>
<li><span>Managing the national executive, along with the National President (China)</span></li>
</ul>
</li>
<li><span>Facilitating establishment of more ACYA chapters across Australia in line with ACYA’s overall aims</span></li>
<li><span>Working with sponsors and associate partners</span></li>
<li><span>Facilitating, leading and encouraging the work of other National exec members with the National President (PRC)</span></li>
<li><span>Holding National exec members accountable when work that was promised has not been done, and more enjoyably, praising the great work of exec members or chapters</span></li>
<li><span>Ensuring workload is spread among team with clear delegation and support focused on the different exec portfolios </span></li>
<li><span>Representing ACYA at meetings with sponsors/stakeholders and (less often) in media</span></li>
<li><span>Attending local Chapter events wherever possible</span></li>
<li><span>Representing ACYA at events</span></li>
<li><span>Chairing ACYA National Executive meetings, and also Chapter Presidents meetings </span></li>
</ul>
<p><b><span>Expectations:</span></b><span> </span></p>
<ul style="margin-top: 0in;">
<li><span>Leadership experience</span></li>
<li><span>Extensive experience with ACYA</span></li>
<li><span>Proven interest in and commitment to Sino-Australian relations </span></li>
<li><span>Prepared to commit time and energy</span></li>
<li><span>Cross-cultural fluency</span></li>
<li><span>Ability manage/facilitate as appropriate</span></li>
<li><span>Confident spokesperson</span></li>
<li><span>Diligent, efficient and timely email correspondence </span></li>
</ul>
<p><b><span>Time Commitment:</span></b><span> </span></p>
<p><span>15-30 hours average per week. </span></p>
<p><b><span>Length of Time Held Position: </span></b></p>
<p><span>1 year </span></p>