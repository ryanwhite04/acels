{
  "title": "Vice-President (People-to-people)",
  "author": "ACYA",
  "date": "2012-12-03T10:21:08+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><b><span style="font-family: 'Arial','sans-serif';">Position:</span></b><span style="font-family: 'Arial','sans-serif';"> National Vice President People to People</span></p>
<p><span style="font-family: 'Arial','sans-serif';"></span><b><span style="font-family: 'Arial','sans-serif';">Core Duties and Responsibilities: </span></b><span style="font-family: 'Arial','sans-serif';"></span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">&nbsp;The people to people exchange component of ACYA aims to promote genuine cultural engagement amongst its members and participants. It recognises that the foundation of the Australia-China relationship should be the individual contacts amongst its’ citizenry. Whether the interaction is between Australian and Chinese students, or involving professionals, P2P plays an important role in ACYA’s development. </span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">&nbsp;Time Commitment</span></b><span style="font-family: 'Arial','sans-serif';"></span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">10-15 hours per week.</span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">&nbsp;</span><b><span style="font-family: 'Arial','sans-serif';">Length of Time Held Position</span></b><span style="font-family: 'Arial','sans-serif';"></span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">1 year.</span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Where is the portfolio heading:</span></b></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpMiddle"><span style="font-family: 'Arial','sans-serif';">The most important new thing for 2013 will be the commencement and maintenance of the Chinese Student Liaisons. Sam is more than willing to help out to make sure this starts off well. </span></p>
<p style="margin-left: 0in; text-indent: 0in;" class="NoSpacingCxSpLast"><span style="font-family: 'Arial','sans-serif';">&nbsp;It would also be great to see an online ACE Space get off the ground – we discussed this but had difficulties with implementation. </span></p>