{
  "title": "National Communication Director",
  "author": "ACYA",
  "date": "2012-12-03T10:07:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p class="PlaceholderText1CxSpFirst"><b><span style="font-family: 'Arial','sans-serif';">Position: </span></b><span style="font-family: 'Arial','sans-serif';">National Communication Director</span></p>
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Core Duties and Responsibilities: <br /></span></b></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Promoting and representing ACYA in the media</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Developing ACYA’s social media presence in English and Chinese language media</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Co-ordinating media and social media strategy</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Producing the monthly ACYA E-Bulletin</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Co-ordinating a team to fulfil these responsibilities</span></li>
</ul>
<p class="PlaceholderText1CxSpLast"><b><span style="font-family: 'Arial','sans-serif';">Expectations:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<ul>
<li><span style="font-family: 'Arial','sans-serif';">Excellent written and spoken English</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Ideally, excellent written and spoken Chinese</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Media/writing/editing experience</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Experience with social media</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong organisational and time management skills</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Strong networking skills / keen to facilitate relationships </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Ability to work with and manage a diverse team</span></li>
<li><span style="font-family: 'Arial','sans-serif';">Knowledge of ACYA as an organisation </span></li>
<li><span style="font-family: 'Arial','sans-serif';">Cultural awareness and cross-cultural communication skills</span><span style="font-family: 'Arial','sans-serif';"> <br /></span></li>
</ul>
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Time Commitment:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<p class="PlaceholderText1CxSpMiddle"><span style="font-family: 'Arial','sans-serif';">10-15 hours average per week. </span></p>
<p class="PlaceholderText1CxSpMiddle"><b><span style="font-family: 'Arial','sans-serif';">Length of Time Held Position:</span></b><span style="font-family: 'Arial','sans-serif';"> <br /></span></p>
<p class="PlaceholderText1CxSpLast"><span style="font-family: 'Arial','sans-serif';">1 year </span></p>