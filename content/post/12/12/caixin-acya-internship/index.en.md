{
  "title": "Caixin ACYA Internship",
  "author": "ACYA",
  "date": "2012-12-03T11:45:25+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Caixin Media Company Limited is a media group dedicated to providing financial and business news and information through periodicals, online content, mobile apps, conferences, books and TV/video programs. Caixin Media aims to blaze a trail that helps traditional media prosper in the new media age through integrated multimedia platforms.

Led by Hu Shuli, who is internationally recognised for her achievements in journalism, the editorial staff at Caixin Media is well-known for independent thinking and professional practices. They are insiders with a profound understanding of China's economic and social transition. They are sharp observers with a global vision. They are the torchbearers of professional journalism, known for providing high-quality, credible content.

<!--more-->

Caixin Media in Beijing will give you the opportunity to work on multi-media projects, research various news topics and participate in reporting on international affairs. This is an unpaid internship for 3-6 months

Interns must be native English speakers with strong mandarin language skills. Candidates with a background in journalism, English literature or East Asian studies are preferred.

We are currently accepting three-month intern applications to start from January

Please send your cover letter, CV and writing samples to <a href="mailto:sam.wall@acya.org.au">sam.wall@acya.org.au</a>