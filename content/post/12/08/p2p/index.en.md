{
  "title": "P2P (人际交流)",
  "author": "ACYA",
  "date": "2012-08-02T06:51:21+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><span style="color: #808080;"></span></p>
<p><span style="background-color: white;"></span></p>
<p><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: black; background-color: white;">ACYA&nbsp;understands the importance of meaningful personal engagement between our members. It is this third limb of&nbsp;ACYA&nbsp;- People to People Exchange (</span><span style="font-size: 10pt; line-height: 115%; font-family: SimSun; color: black; background-color: white;">人际交流</span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: black; background-color: white;">) - that fosters this engagement for our China-Australia geared members. Through our chapter events and extensive online presence,&nbsp;ACYA&nbsp;offers a portal for members to share their stories, interests and questions with like-minded peers. This section of the website provides invaluable contact information and advice from professionals and students alike, whilst also offering personal insights into what life is like for a foreigner in China and Australia.</span></p>
<p><span style="background-color: white;"></span></p>