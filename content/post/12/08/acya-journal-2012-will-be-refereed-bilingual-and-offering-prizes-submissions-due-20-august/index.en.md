{
  "title": "ACYA Journal 2012 will be Refereed, Bilingual, and Offering Prizes: Submissions Due 20 August",
  "author": "ACYA",
  "date": "2012-08-02T06:07:30+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<h1>ACYA Journal 2012</h1>
<div>To allow for extra time after the university holidays, the deadline for submissions to the ACYA Journal has been pushed back to 11:59pm Monday 20 August, so make sure to use this opportunity to&nbsp;be published! This&nbsp;year the ACYA Journal will be <strong>bilingual</strong>, <strong>peer-reviewed</strong>, and offering <strong>cash prizes</strong> of up to $500! We are calling for submissions of academic essays, opinion pieces, and creative work in either&nbsp;<strong>English or Chinese</strong>. The Journal will be launched at the 2012 ACYD in October. Please click <a style="font-weight: normal; color: #336699; text-decoration: underline;" title="http://acya.us1.list-manage1.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=ce88a84629&amp;e=441a212d5b" href="http://acya.us1.list-manage1.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=ce88a84629&amp;e=441a212d5b" target="_blank">here</a> for more details!</div>
<div>&nbsp;</div>
<div>为了在学校假期结束后给参赛者们留出更多的时间,中澳青年联合会学报将2012征文的截止时间推迟到<span style="text-decoration: underline;">8月20日（周一）晚上11</span><span style="text-decoration: underline;">点钟59分</span>。希望大家千万不要错过一次在经由专家评审的双语刊物中展现自我和赢取大奖的机会！今年的中澳青年联合会学报将引入提供达<strong>500澳元</strong>的奖励！现在学报将为其下三个部分公开向社会征集<strong>中文</strong>或<strong>英文</strong>稿件，分别为：学术文章部分，个人评论部分和原创作品部分。学报将之后在2012年10月的中澳青年对话论坛发布。请点击<a style="font-weight: normal; color: #336699; text-decoration: underline;" title="http://acya.us1.list-manage1.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=93168ba992&amp;e=441a212d5b" href="http://acya.us1.list-manage1.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=93168ba992&amp;e=441a212d5b" target="_blank">这里</a>查看更多详情！</div>