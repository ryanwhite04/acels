{
  "title": "ACYA in Australian Financial Review",
  "author": "ACYA",
  "date": "2012-08-31T09:56:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>Asian literacy is touted as a sure-fire way into exciting, well-paid jobs of the future, but university graduates possessing so-called Asian literacy skills insist there is no easy route.</p>
<p>By Primrose Riordan,&nbsp;Australian Financial Review, 23 July:</p>
<p>Asian literacy is touted as a sure-fire way into the exciting, well-paid jobs of the future, but university graduates possessing so-called Asian literacy skills insist there is no easy route.</p>
<p>Far from having employers knocking on their doors, finding meaningful work is a matter of relentless networking, unpaid internships and expensive exchange programs, they say.</p>
<p>Fiona Lawrie, 26, is an arts graduate with majors in politics and Chinese language who worked for one of the big four banks in Melbourne for more than two years before realising she was getting no closer to being deployed to China itself.</p>
<p>She said her exposure to China – unpaid internships at Chinese companies in every university summer break – was “100 per cent responsible” for her landing the job. But she found herself unable to use the skills that made her so attractive to her employer in the first place.</p>
<p>“Big Australian companies are more enthusiastic about sending more senior staff up to Asia, those with 10 to 15 years’ experience, so it’s easier to justify the relocation cost,” she said.</p>
<p>Ms Lawrie, who grew up in Singapore, has a bachelor of arts in Chinese and politics from the University of Melbourne and studied Mandarin in Taiwan and China, decided to go it alone. She moved to China after meeting another Australian working in Beijing to set up an awareness-raising organisation called Thirst.</p>
<p>Thirst is an initiative of the Forum of Young Global Leaders at the World Economic Forum and uses China as a base for raising awareness about global water issues through social networking campaigns. Ms Lawrie also does consulting work with Australian firms looking to enter the Chinese market.</p>
<p>Australian-born Joel Wing-Lun graduated from the University of Sydney with an honours degree in Chinese studies. As part of his studies he studied Mandarin at Tsinghua University in Beijing.</p>
<p>Mr Wing-Lun avoided the Australian workforce altogether and after graduating last year he moved to China for an unpaid internship. He has since landed a paid position with Beijing-based international research firm, China Policy.</p>
<p>He credits his involvement with the Australia-China Youth Association while at university for giving him a leg up into paid employment.</p>
<p>In 2010, the association established an Australia-China Young Professionals Initiative which brings together people aged 22 to 35 years who are working in Asia.</p>
<p>The association’s Australian president, Jeffrey Sheehy, said although there were a lot of financial incentives on offer for Australians to study in China, finding work could be very difficult.</p>
<p>“Often graduates may need to work for an Australian firm or company and hope to gain a rotation in an Asian office,” he said.</p>
<p>But these positions are highly sought after, he said. The alternative was to try to find volunteer work with a Hong Kong or China-based organisation, although there was no guarantee that this would turn into paid employment.</p>
<p>Mr Sheehy said language skills were often not enough, and students needed a strong network of contacts in China, the focus of associations such as ACYA. “Applying the language in a complex or technical business or legal environment can prove difficult, especially in important matters,” he said.</p>
<p>In Australia, the government is awaiting a report on Australia in the Asian century by former Treasury secretary Ken Henry.</p>
<p>Also wading into the debate, the Opposition has announced plans for more university exchange programs and targets for high school language learning. Australia is not alone in paying greater attention to Asia. Universities in the United States and Europe are expanding exchange programs to China.</p>
<p>The Chinese government also has targets for increasing the number of foreigners studying there.</p>
<p>Working in the Australian pavilion during the 2010 World Expo in Shanghai was an eye opener for Asian studies graduate Joanna Bayndrian regarding the sheer competition for employment in China.</p>
<p>Ms Bayndrian works at the Penrith Regional Arts Gallery. She said her experience in China no doubt made her stand out from other applicants.</p>
<p>But the benefits of Asian knowledge were likely to emerge in the longer term, she said.</p>
<p>“Arts industries in China are booming and the skills I’m learning here are transferable,” she said.</p>
<p>Ms Lawrie said that as the European debt crisis deepened, Australians were increasingly competing with graduates from elsewhere for jobs in China.</p>
<p>“Australian companies need to be cultivating graduates with strong links to China. There is the need to make sure we have more of an edge,” she said.</p>
<p>“A lot of people think they can just walk into management positions in China, but a native with perfect bilingual language skills will have a better chance,” she said.</p>
<p>“If you go over there without experience and networks, it’s a trap. You end up teaching English.”</p>