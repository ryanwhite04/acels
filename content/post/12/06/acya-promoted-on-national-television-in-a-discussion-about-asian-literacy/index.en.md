{
  "title": "ACYA promoted on national television in a discussion about Asian literacy",
  "author": "ACYA",
  "date": "2012-06-15T07:34:14+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>{jcomments on}<strong>The Australia-China Youth Association (ACYA) UNSW</strong> hosted the event, Diplomatic Careers in Asia, on 15 May with the goal of providing students with the confidence to take their first step into the Australian foreign service. The event was supported by the School of International Studies and the Confucius Institute at UNSW, and was exceptionally well-attended.</p>
<p>&nbsp;Four speakers gave insightful and engaging presentations: <strong>Lyndall Partington</strong>, from the UNSW Careers Centre, gave an informative breakdown of the DFAT application process; <strong>Laurie Smith</strong>, the Executive Director of International Operations at Austrade, opened minds to positions in the Australian foreign service beyond DFAT and spoke on how he became passionate about Asia; <strong>James Hudson</strong>, the East Asia Advisor at CSIRO International, discussed how CSIRO plays a role in global responses to global challenges, and what they don’t teach you about at university about diplomacy; and <strong>Huw Pohlner</strong>, the Manager for Strategy at Asialink, spoke on the growing importance of Asia to Australian diplomacy. <strong>The event received news coverage from SBS Mandarin News Australia, and was broadcast on SBS on 30 May at 5:30pm</strong>. (<a class="external-link" href="http://www.sbs.com.au/chinese/video/581077/" target="_blank" title="http://www.sbs.com.au/chinese/video/581077/
sbs"><span title="http://www.sbs.com.au/chinese/video/581077/"><span title="http://www.sbs.com.au/chinese/video/581077/">http://www.sbs.com.au/chinese/video/581077/</span></span></a>)</p>
<p>Please click on the name to view that person's presentation on YouTube:</p>
<p>Welcome and <a class="external-link" href="http://www.youtube.com/watch?v=aeVDw2HZCDU&amp;feature=relmfu" target="_blank" title="http://www.youtube.com/watch?v=aeVDw2HZCDU&amp;feature=relmfu
Lyndall Partington"><span title="http://www.youtube.com/watch?v=aeVDw2HZCDU&amp;feature=relmfu"><span title="http://www.youtube.com/watch?v=aeVDw2HZCDU&amp;feature=relmfu"><strong title="http://www.youtube.com/watch?v=aeVDw2HZCDU&amp;feature=relmfu">Lyndall Partington</strong></span></span></a></p>
<p><a class="external-link" href="http://www.youtube.com/watch?v=pa5tpVSJ2js&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=2&amp;feature=plpp_video" target="_blank" title="http://www.youtube.com/watch?v=pa5tpVSJ2js&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=2&amp;feature=plpp_video
Laurie Smith"><span title="http://www.youtube.com/watch?v=pa5tpVSJ2js&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=2&amp;feature=plpp_video"><span title="http://www.youtube.com/watch?v=pa5tpVSJ2js&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=2&amp;feature=plpp_video"><strong title="http://www.youtube.com/watch?v=pa5tpVSJ2js&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=2&amp;feature=plpp_video">Laurie Smith</strong></span></span></a></p>
<p><a class="external-link" href="http://www.youtube.com/watch?v=1uzGM7i8j2Q&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=3&amp;feature=plpp_video" target="_blank" title="http://www.youtube.com/watch?v=1uzGM7i8j2Q&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=3&amp;feature=plpp_video
James Hudson"><span title="http://www.youtube.com/watch?v=1uzGM7i8j2Q&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=3&amp;feature=plpp_video"><span title="http://www.youtube.com/watch?v=1uzGM7i8j2Q&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=3&amp;feature=plpp_video"><strong title="http://www.youtube.com/watch?v=1uzGM7i8j2Q&amp;list=UUthjY71kw6kubDED3pNn9Yg&amp;index=3&amp;feature=plpp_video">James Hudson</strong></span></span></a></p>
<p><a class="external-link" href="http://www.youtube.com/watch?v=ROqOrpkf2iM&amp;feature=relmfu" target="_blank" title="http://www.youtube.com/watch?v=ROqOrpkf2iM&amp;feature=relmfu
Huw Pohlner"><span title="http://www.youtube.com/watch?v=ROqOrpkf2iM&amp;feature=relmfu"><span title="http://www.youtube.com/watch?v=ROqOrpkf2iM&amp;feature=relmfu"><strong title="http://www.youtube.com/watch?v=ROqOrpkf2iM&amp;feature=relmfu">Huw Pohlner</strong></span></span></a></p>
<p><a class="external-link" href="http://www.youtube.com/watch?v=OuNKlCwvNqs&amp;feature=relmfu" target="_blank" title="http://www.youtube.com/watch?v=OuNKlCwvNqs&amp;feature=relmfu
Q&amp;A"><span title="http://www.youtube.com/watch?v=OuNKlCwvNqs&amp;feature=relmfu"><span title="http://www.youtube.com/watch?v=OuNKlCwvNqs&amp;feature=relmfu"><strong title="http://www.youtube.com/watch?v=OuNKlCwvNqs&amp;feature=relmfu">Q &amp; A</strong></span></span></a></p>