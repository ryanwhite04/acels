{
  "title": "In the Heart of the Union",
  "author": "ACYA",
  "date": "2012-06-23T22:39:18+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
We've just concluded 2.5 days of meetings and workshops in Brussels, the administrative capital of the European Union.
The exposure to high-level policymakers and politicians has been impressive - we've spoken with two members of cabinet for EU Commissioners and High Representatives, a Member of the European Parliament and the Chief Operating Officer of the European External Action Service (the EU's relatively new diplomatic service).

A core message has been consistently reiterated: the EU is still China's largest trading partner, it is internally unbalanced but nevertheless viable, and what is required is more not less integration.

Official optimism is encouraging, but one wonders how the EU will move forward if a Greek exit becomes reality. Should contagion set in, and other members states with larger more interconnected economies face the prospect of falling out of the Euro-zone, the notion of a European union will be under even greater threat.

This is particularly concerning because of the degree of integration currently experienced in the EU, and the even greater integration that is proposed. One feels that the fates of individual members states in other regional organisations (say, ASEAN) would not have as large an impact on the future of the organisation itself, due to the lack of a profound monetary union and the retention of fundamental sovereign rights (such as the ability to negotiate FTAs).

I have also been struck by the fact that the European Union really is a psychological union as well - if a number of countries were to exit, that psychological union may be permanently weakened.

<p>Over the next week in Turin, the role of China will become the centre of our focus, and our experiences in Berlin and Brussels will bring valuable new perspectives as we look back toward the Asia-Pacific in earnest.</p>