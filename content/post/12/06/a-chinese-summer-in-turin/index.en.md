{
  "title": "A Chinese Summer in Turin",
  "author": "ACYA",
  "date": "2012-06-29T22:07:21+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>[Note – written on 28 June; published on 29 June]</p>
<p class="MsoNormal">The fourth day of the TOChina Summer School has just concluded and the 50-odd participants (including the 12 GEV Fellows) have heard now from Professors Geremie Barmé, David Shambaugh, Yu Yongding and Maurizio Marinelli.</p>
<p class="MsoNormal">Temperatures are slowly rising in the classroom (the University of Torino department we are using is about to move and all air-conditioning has been stripped from the building) and debates are starting to unfold. Debates about China’s past and future, its relations with other powers, external anxieties and internal tensions, and the stories China tells itself about itself.</p>
<p class="MsoNormal">Is China entering a new <span style="font-family: 宋体; mso-ascii-font-family: Cambria; mso-hansi-font-family: Cambria; mso-bidi-font-family: 宋体; mso-ansi-language: EN-AU; mso-fareast-language: ZH-CN;" lang="ZH-CN">盛世</span> (golden age)? Is the US-China trade imbalance really anything to get worked up about? Chongqing model or Guangdong model? How has non-Chinese scholarship on China changed over recent decades? How do urban planning projects in China project ideologies, express cultural values and exercise power? What scenarios can be played out for the development of the party-state over the medium to long term?</p>
<p class="MsoNormal">These questions have all been posed and debated.</p>
<p class="MsoNormal">We have also received a keynote address from former Italian Prime Minister and European Commission President Romano Prodi. Prodi is (cautiously) upbeat on the future of the EU and determined to see China play an integral role in the future of EU economies.</p>
<p class="MsoNormal">While we sweat in the lecture hall, passions have also been rising in the piazzas of this old city. Italy has made it through to the final of the Euro 2012 football competition and a crowd of several thousand turned out tonight to watch the semi-final against Germany live on the big screen. Italian flags and vuvuzelas dominated the night and no-one could possibly have been sleeping within a half kilometre of the town square.</p>
<p class="MsoNormal">The austere Germans left the ground forlornly as the indebted Italians celebrated. In one of several quirks of history that have come about on the football ground during this cup, Italy and Spain, whose leaders have just engaged in an extended bout of brinksmanship to push Chancellor Merkel and other EU leaders to agree to softened terms on debt repayment and fiscal discipline, will now meet in the final.</p>
<p class="MsoNormal">Next, I’ll be recording my comments in response to Professor Barmé’s lectio magistralis on ‘the China story’. Each GEV Fellow is required to provide brief response to a high-level lecture delivered by a TOChina expert and I’ll be one of three people speaking after Professor Barmé tomorrow.</p>
<p class="MsoNormal">{jcomments on}</p>