{
  "title": "30.03.12 - First Blog Post. Ever.",
  "author": "ACYA",
  "date": "2012-06-30T09:32:42+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p class="MsoNormal"><span lang="EN-GB">Brisbane is cooler now; the season has changed into autumn – a season that apparently many people have an affinity for. I can’t say I agree, personally I love spring the best despite my allergies, and yet I don’t welcome her drier sister as much. I’ve been told autumn is essentially the same as spring minus the rogue pollen in the air, and you’d think that I’d like it more due to that but I don’t. I don’t hate it but I certainly don’t love it. Perhaps it’s because I don’t like winter, autumn does signify that winter is looming after all. Sigh. In with the summer dresses and out with the woollen jumpers.</span></p>
<p class="MsoNormal"><span lang="EN-GB">Things to project your adoration to:</span></p>
<p class="MsoNormal"><span lang="EN-GB">Word art home deco – I’ve always been a lover of home deco but alas, I have neither a place of my own nor enough money to expend on such luxuries...so I deal with my creative urges with ogling &lt;&lt;http://www.ebay.com.au/itm/Personalised-Wooden-Name-Plaques-Words-Letters-Wall-Door-Art-craft-Sign-/110768299522?pt=UK_Home_Garden_Decorative_Accents_LE&amp;hash=item19ca4e1e02&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">TED – Larry Smith: Why you will fail to have a great career – this is so insightful! I would love to have this guy as a professor, he’s so funny! I’ll be posting a TED video with each post, but I highly implore you to go and take a look yourself because there’s so many videos that I have yet to find that I guarantee you’ll love &lt;&lt;http://www.ted.com/talks/larry_smith_why_you_will_fail_to_have_a_great_career.html&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">TED – Peter Saul: Let’s talk about dying – truthful and is a good elbow to the chest (if you’re a female, you’ll know what I mean, or if you’re male imagine a kick to the groin) reminder that you do have family, and you really do need to talk to them about such matters that are undoubtedly imminent. If you’re a doctor or thinking of becoming a doctor, particularly in the ICU - WATCH THIS! &lt;&lt;http://www.ted.com/talks/peter_saul_let_s_talk_about_dying.html&gt;&gt; </span></p>
<p class="MsoNormal"><span lang="EN-GB">Adam Carolla – In Fifty Years We’ll All Be Chicks: ...And other Complains from an Angry Middle-Aged White Guy – hilariously witty, uncouth, rant-tastic novel by Adam Carolla - one of the best ranters out there! Check out his show &lt;&lt;<a href="http://www.adamcarolla.com/">http://www.adamcarolla.com</a>&gt;&gt;! Purchase the book, borrow the book, steal the book (not that I’m in any way encouraging you to steal), do whatever you need to do to get your hands on a copy and READ IT! &lt;&lt;http://booko.com.au/works/793335&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">Jung and Briggs Myers Personality Test – I know many of you will chalk this down to silly mumbo-jumbo but it does hold truth to it (though Wikipedia reports that the reliability and validity of such categorisation of people has been subject to criticism) as long as you answer honestly. There’ll always be self-reported bias (which I am currently having drilled into me with behavioural statistics) and falsifiability issues but to an extent there is truth. Once you’ve obtained your score, type it in Wikipedia and you’ll see the temperaments and types. E-mail me at <a href="mailto:liuissa.zhen@uqconnect.edu.au">liuissa.zhen@uqconnect.edu.au</a> on what personality type you are and whether you think it yields honest results, I’d love to know!<strong> </strong>&lt;&lt;http://www.humanmetrics.com/cgi-win/jtypes2.asp&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB"> </span></p>
<p class="MsoNormal"><span lang="EN-GB">Musique to bop your head to:</span></p>
<p class="MsoNormal"><span lang="EN-GB">She &amp; Him – Thieves &lt;&lt;http://www.youtube.com/watch?v=SeAgLIaHj0M&amp;ob=av2e&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">The Shirelles – Mama Said &lt;&lt;http://www.youtube.com/watch?v=ns1exm8Y5r4&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">Nick Hollywood – Deep Henderson &lt;&lt;http://www.youtube.com/watch?v=42KnNQp9ccY&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">The JaneDear Girls – Wildflower &lt;&lt;http://www.youtube.com/watch?v=NC0IhlquYlI&amp;ob=av2e&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB">Survivor – Burning Heart &lt;&lt;http://www.youtube.com/watch?v=lYlkYkHkZxs&gt;&gt;</span></p>
<p class="MsoNormal"><span lang="EN-GB"> </span></p>
<p class="MsoNormal"><span lang="EN-GB">Zany Weird Thoughts:</span></p>
<p class="MsoNormal"><span lang="EN-GB">Brisbane weather = moodiest teenage girl, and you all thought Mother Nature was bad.</span></p>
<p class="MsoNormal"><span lang="EN-GB">If our atmosphere contained enough gravity to have us walking on the sky and airplanes flew on the ground.</span></p>
<p class="MsoNormal"><span lang="EN-GB">What if the Brisbane River was a waterfall? At first I thought it’d be all beautiful and mesmerising, the kind you’d expect in Hollywood films or nature documentaries then I realised it’s brown...</span></p>
<p class="MsoNormal"><span lang="EN-GB">Remember, Alice from Alice in Wonderland said that you should always imagine 6 unimaginable things before breakfast.</span></p>
<p class="MsoNormal"><span lang="EN-GB"> </span></p>
<p class="MsoNormal"><span lang="EN-GB">Recipe of the Month:</span></p>
<p class="MsoNormal"><span lang="EN-GB">Honey Ice Tea (+ Honey Milk Tea)</span></p>
<p class="MsoNormal"><span lang="EN-GB">Jug</span></p>
<p class="MsoNormal"><span lang="EN-GB">Cup/Mug</span></p>
<p class="MsoNormal"><span lang="EN-GB">Tea (bag/leaves, cheap/expensive, preferably un-sweet as honey will be added)</span></p>
<p class="MsoNormal"><span lang="EN-GB">Honey/sugar (I prefer honey as it is a healthier alternative)</span></p>
<p class="MsoNormal"><span lang="EN-GB">Water (duh!)</span></p>
<p class="MsoNormal"><span lang="EN-GB">It’s quite a simple recipe, suitable for those dreary summer days Brisbane is prone to. Make a few cups of strong tea and pour it into the jug (without the leaves if loose). If you prefer sugar over honey, add however much sugar you’d like to the tea and stir until dissolved then add some water until desired ratio of tea strength to sweetness has been achieved. If you’re adding honey, add some water first and wait for it to cool below 40 degrees Celsius before adding any honey. This is due to the fact that the main bee enzyme (invertase) decays and medicinal properties are destroyed when heated. </span></p>
<p class="MsoNormal"><span lang="EN-GB">Refrigerate until chilled and voila! Ice tea.</span></p>
<p class="MsoNormal"><span lang="EN-GB">Note: feel free to add some lemon slices and/or mint leaves to enhance flavour and some ice cubes to keep it chilled!</span></p>