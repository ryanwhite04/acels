{
  "title": "Passing Through China to Talk About China",
  "author": "ACYA",
  "date": "2012-06-15T14:41:48+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
I'm currently waiting in Guangzhou airport. I'll sit here for a few hours before flying on to Berlin, where Global Emerging Voices will kick off on Sunday. In the first few days, I'll meet and converse with a former cultural counsellor at the German Embassy in Beijing, a banker, a businessperson, and several academics, not to mention my fellow... fellows.

Most of the time we'll be talking about China, or Europe and China, or Germany and China, but these few hours are the only time I'll spend <em>in</em> China as part of this program.

<!--more-->

It's a strange sensation to pass <em>through</em> China on the way to Europe to convene and talk about China. Right now, other <a href="http://gev.twai.it/index.php?view=FELLOWS&year=2012">GEV fellows</a> from the region will also be getting on long-haul flights only to land and turn their attention back in the direction from which they came.

Yet it is precisely that apparent contradiction that will hopefully make GEV a unique and timely experience. Our program begins on the day Greece holds its second election this year, to restore political stability in the face of crippling indebtedness. We end the fortnight in Torino/Turin as nervous Italian investors diversify their interests so as to avoid being caught out by a sudden loss of confidence.

The shifting of the centre of global economic activity from the US/Europe to Asia (specifically East Asia) throws current events in the Eurozone into sharp historical relief. The dominant world order of the last several hundred years may quite dramatically be giving way under our feet as we meet in Berlin, Brussels and Torino.

The recent emergence of state-enhanced capitalism as a genuine contender with classical neoliberal (UK, US) and coordinated capitalism (Germany) - governments control 3/4 of the world's fossil fuel reserves and sovereign wealth funds account for at least 1/8 of global investment - could yet be more than a short-lived trend. China's development model will be the bellwether.

Whatever the future may hold, what GEV has right, just like ACYA's own Youth Dialogue, is the make-up of the group that will meet in Europe. Myself and one other Australian will stand out at a gathering that features almost entirely Asian voices. It is these sorts of sustained efforts at frank dialogue between not just Asian and 'Western' counterparts, but representatives of starkly different models for the attainment of prosperity and development (including different models <em>within</em> Asia), that carry with them the best chance of ensuring global change for global benefit.

I look forward to spreading all of the good news that ACYA has to offer to those that I meet in Europe and to sharing my learnings with ACYA's members through this blog.

<img src="http://www.twai.it/upload/png/gev-logo.png" border="0" alt="" align="left" style="display: block; margin-left: auto; margin-right: auto;" />
