{
  "title": "ACYA at the University of Adelaide",
  "author": "ACYA",
  "date": "2012-06-03T11:59:22+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 14.4pt; mso-pagination: widow-orphan; background: white;" align="left"><span style="font-size: 9.0pt; font-family: 'Verdana','sans-serif'; mso-fareast-font-family: SimSun; mso-bidi-font-family: SimSun; color: black; mso-font-kerning: 0pt;" lang="EN-US">ADELAIDE, Australia – ACYA at the University of Adelaide is off to a flying start. Formed in May 2012, members have participated in activities such as Chinese debating, Mahjong and Chinese Discussion groups.</span></p>
<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 14.4pt; mso-pagination: widow-orphan; background: white;" align="left"><span style="font-size: 9.0pt; font-family: 'Verdana','sans-serif'; mso-fareast-font-family: SimSun; mso-bidi-font-family: SimSun; color: black; mso-font-kerning: 0pt;" lang="EN-US">ACYA Adelaide is a social and an educational resource for students with an interest in Chinese or Australian Culture.&nbsp; Our goal is to improve the connection between the Chinese and non-Chinese students in campus by bringing them together and sharing Chinese and Australian culture.</span></p>
<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 14.4pt; mso-pagination: widow-orphan; background: white;" align="left"><span style="font-size: 9.0pt; font-family: 'Verdana','sans-serif'; mso-fareast-font-family: SimSun; mso-bidi-font-family: SimSun; color: black; mso-font-kerning: 0pt;" lang="EN-US">ACYA engages with students through social activities, guest-speaker seminars and games. ACYA Adelaide will also assist international students in finding employment and internships within Adelaide, and assisting local students to find internships and work-experience in China.</span></p>
<p><span style="font-size: 9.0pt; font-family: 'Verdana','sans-serif'; mso-fareast-font-family: SimSun; mso-bidi-font-family: SimSun; color: black; mso-font-kerning: 0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA;" lang="EN-US">ACYA Adelaide is looking forward to its formal opening celebration, as well as to dinner functions, pub crawls and events in association with the Confucius Institution of Adelaide.&nbsp;</span></p>