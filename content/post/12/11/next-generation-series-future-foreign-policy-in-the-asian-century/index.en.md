{
  "title": "Next Generation Series - Future Foreign Policy in the Asian Century",
  "author": "ACYA",
  "date": "2012-11-07T09:48:02+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>&nbsp;</p>
<p>As part of Asialink's Next Generation series, Asialink, the University of Melbourne, Australia's Role in the World&nbsp;and the Australia Network present a unique television event; <em><strong>Future foreign policy in the Asian Century.</strong></em> This event will bring together two of Australia’s next generation of foreign policymakers – the Liberal Party’s Kelly O’Dwyer and Richard Marles of the ALP – both lower house members of Federal Parliament.<br /> &nbsp;<br /> The two will participate in a debate moderated by the ABC’s <a title="External link (will open in new window)" class="external-link" href="http://www.abc.net.au/profiles/content/s2233684.htm" target="_blank">Bev O’Connor</a> and then respond to questions from a small audience of invited guests.<br /> &nbsp;<br /> The resulting event will be packaged into a television program broadcast across the Asia Pacific region on Australia Network and domestically on ABC News 24.<br /> &nbsp;<br /> We invite you to register interest in attending by <strong>completing an online form <a href="http://www.asialink.unimelb.edu.au/calendar/events/featured/Future_Foreign_Policy/registration" target="_blank">here</a> by Wednesday 6 November.</strong><br /> <br /> The event will require attendees to be at the University of Melbourne from 6:00-8:30pm on 13 November, 2012.</p>
<p><a href="http://www.asialink.unimelb.edu.au/calendar/events/featured/Future_Foreign_Policy">Read More &gt;&gt;</a></p>