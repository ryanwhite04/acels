{
  "title": "2012 ACYA Journal of Australia-China Affairs Released",
  "author": "ACYA",
  "date": "2012-11-01T07:26:16+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div>&nbsp;</div>
<div><img style="display: block; margin-left: auto; margin-right: auto;" alt="ACYA-Journal-Large" src="images/ACYA-Journal-Large.gif" height="125" width="430" /></div>
<div>&nbsp;</div>
<div>On behalf of the 2012 ACYA Journal Sub-Committee, we would like to welcome you to the 2012 edition of the ACYA Journal of Australia-China Affairs.</div>
<div><br />Building on from the pioneering success of the inaugural ACYA Journal in 2011, in 2012 the renamed ACYA Journal of Australia-China Affairs made further strides in Australia-China youth engagement by instituting the ACYA Prizes for Australia-China Youth Scholarship (English and Chinese) to attract a broad range of quality submissions, conducting an academic referee process to ensure the value of published works, and rendering the major Opinion Article section entirely bilingual in order to amplify the reach of the Journal throughout both Australia and China.</div>
<div>&nbsp;</div>
<div>
<div><span style="color: #0070c0;" color="#0070C0"><b>ISSN </b><b>: 2201-3814</b></span></div>
</div>
<div>&nbsp;</div>
<div><a href="Publications/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf">Click to view the Journal Online &gt;&gt;</a></div>
<div>&nbsp;</div>
<div><a href="Publications/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.rar">Click to download Journal &gt;&gt;</a></div>