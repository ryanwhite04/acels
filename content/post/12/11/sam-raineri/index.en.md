{
  "title": "Sam Raineri",
  "author": "ACYA",
  "date": "2012-11-07T09:49:09+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>Rooms to rent in share house at West Chermside. Modern 2 storey brick home. House and bedrooms fully furnished. Fridge, freezer, washing machine, dryer and tv. Electricity, gas, broadband included in rent. Close in shops and express bus stop to QUT Kelvin Grove and Gardens Point metres away. Ideal for International Students. Just provide your own food, toiletries laundry and dishwashing products. Single rooms $195 per week. Double room $ 230 per week and a Double room with ensuite $250 per week all on a 6 month lease. Contact Sam on &nbsp;0403485628 or email myrain@optusnet.com.au</p>