{
  "title": "New ACYA Chapters!",
  "author": "ACYA",
  "date": "2012-09-12T09:56:27+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: black; background-color: white;">ACYA is celebrating the recent addition of a Nanjing chapter to its ever-growing network. Earlier in the year, ACYA chapters were established at Monash University and the University of Adelaide. More recently, Queensland University of Technology also joined the ACYA ranks. QUT have been focusing on weekly social and networking events. Still in its infancy, the QUT chapter is looking to fill all of its executive roles in order to provide “an A grade chapter”. Those interested in getting involved with ACYA QUT can contact Jake Kelder on 0421125827.</span></p>
<p><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: black; background-color: white;">The Nanjing chapter brings the number of ACYA chapters in China to three. ACYA Nanjing anticipates a regular influx of young students and professionals, particularly due to the generosity of <em><span style="font-style: normal;">Victorian Government’s new Hamer Scholarships</span></em><span class="apple-converted-space">&nbsp;program. ACYA in Nanjing hopes to fill executive roles in the coming weeks, and events to attract local participation in the chapter. Those interested can contact Steve Carton on </span></span><span><a href="mailto:steve.carton@acya.org.au"><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; background-color: white;"></span></a><a href="mailto:steve.carton@acya.org.au">steve.carton@acya.org.au</a></span><span class="apple-converted-space"><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif; color: black; background-color: white;"> or join the Facebook group </span></span><span><a href="https://www.facebook.com/groups/ACYANanjing/"><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;"></span></a><a href="https://www.facebook.com/groups/ACYANanjing/">https://www.facebook.com/groups/ACYANanjing/</a></span><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;"></span></p>
<p>&nbsp;</p>
<p><i><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;">The Facing Heaven Gateway in Cohen Place, Melbourne, was a gift from the Jiangsu Government as part of the sister state relationship with Victoria. ACYA Nanjing hopes to strengthen these ties.</span></i><span style="font-size: 10pt; line-height: 115%; font-family: Arial, sans-serif;"></span></p>