{
  "title": "Nanjing",
  "author": "ACYA",
  "date": "2012-09-05T09:37:22+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Nominations for ACYA Nanjing 2013 Executive Positions now open

We are currently seeking to fill several positions on the ACYA Nanjing executive team. These positions include Secretary/Treasurer, Events Coordinator (possibly 2 positions) and People to People Manager.

<!--more-->


<h2>Secretary / Treasurer</h2>
Responsibilities include:
<ul>
<li>Organising executive meetings</li>
<li>Managing chapter’s online and offline resources</li>
<li>Working with communications team to ensure all enquiries are effectively managed</li>
<li>>Helping to draft and edit proposals</li>
<li>Budgeting chapter’s resources</li>
<li>Identifying fund-raising opportunities</li>
<li>Keeping track of cash flows</li>
<li>Be as transparent as possible with executive team</li>
<li>Establishing clear guidelines related to use and expenditure of chapter funds</li>
</ul>

Candidates should be financially responsible and willing to participate more broadly in other areas of the chapter’s scope. The position requires a very small commitment of at most a couple of hours a week. Those who have ideas and want to pursue them through ACYA are welcome to take on this position while at the same time working to achieve other goals related to improving and bridging Australia-China relations.

<h2>Events Coordinator (Potentially 2 positions)</h2>
Responsibilities include:
<ul>
<li>Manage chapter’s calendar</span></li>
<li>Communicate events effectively across executive and membership base</li>
<li>Identify existing events that ACYA Nanjing may be able to get involved with or partner with</li>
<li>Be able to negotiate with a broad spectrum of the community in Nanjing</li>
<li>Be willing to network and actively seek out notable identities that may be willing to offer formal or informal speeches to ACYA Nanjing’s members</li>
</ul>

We are looking for someone who may already have a good network in Nanjing or China more generally. One of our stated goals for this semester is to organise for a notable speaker to provide ACYA Nanjing members with a speech of some sort. We are hoping to recruit someone who may have the network to help source such a person. Along with that we plan to hold a Tertiary Education in Australia forum, providing Chinese students with access to those with inside knowledge. Your responsibilities would include making this event seamless. We have two positions available (one Chinese, one Australia). We are happy to make it one position providing we can find someone who is proficiently bilingual. The position will likely require a couple of hours a week on average. Those who have ideas and want to pursue them through ACYA are welcome to take on this position while at the same time working to achieve other goals related to improving and bridging Australia-China relations.


<h2>People to People Manager</h2>
Responsibilities include:
<ul>
<li>Contact and communicate with local universities in order to find out whether there are:

  1)Australians studying there
  2) Chinese studying there
  3) Australian studies centre, that may have an interest in ACYA Nanjing, our activities and resources
</li>
<li>Liaise with student unions to see if we can get involved with joint events</li>
<li>Look to cities outside of Nanjing that may have organisations or universities with an interest in working with ACYA Nanjing and co-ordinate with the executive to work on day trips to those cities to offer speeches</li>
</ul>


The position would ideally be filled by a local Chinese person. However anyone with excellent Chinese language skills will suffice. Candidates must be willing to actively contact and pursue opportunities to connect with aforementioned groups and report to the People to People Vice President on a regular basis with progress. The position will likely require a couple of hours per week on average. Those who have ideas and want to pursue them through ACYA are welcome to take on this position while at the same time working to achieve other goals related to improving and bridging Australia-China relations.

Please contact Chapter President Steve Carton, <a href="mailto:steve.carton@acya.org.au">steve.carton@acya.org.au</a> to nominate yourself or wish to know more.

05/02/2013

------------------------------------------------------------------------------------------------------

<h2>ACYA-ACAA Christmas Party</h2>

ACYA Nanjing celebrated Christmas dinner with members of Australia China Alumni Association Nanjing Chapter at La Villa Bar and Restaurant on Saturday 15th December. The night included a delicious roast dinner, drinks and door prizes.

<img src="images/Image1.jpg" alt="" height="361" width="488" />


'Exchange'
ACYA Nanjing in partnership with Language Connection runs 'Exchange' language event every Saturday. If you want to practice your Chinese or English and want to help others practice their second language, come along. All welcome!

Every Saturday - 2pm - 4pm
Country Rd Restaurant and Bar, 20-1 Nanxiucun, Shanghai Rd,
南京市鼓楼区上海路南秀村, 20-1号 Country Rd Restaurant
靠近金银街与汉口路
Contact: 15850540137
Email: <a href="mailto:dan.ednie@acya.org.au">dan.ednie@acya.org.au</a>

<h2>New ACYA Nanjing Chapter</h2>

More than 35 people celebrated the Australia-China Youth Association (ACYA) Nanjing Chapter kick-off party at La Villa Bar & Restaurant on Sunday 14 October.
The Nanjing Chapter is set to host regular Sunday get-togethers, Language Exchange sessions and social functions for the growing Australia-China community in the area.
To be placed on the mailing list for upcoming activities email <a href="mailto:nanjing@acya.org.au">acya.nanjing@acya.org.au</a>
Please contact ACYA's Nanjing's President Steve Carton at <a href="mailto:stevecarton@acya.org.au">stevecarton@acya.org.au</a> for information on how you can get involved in ACYA in Nanjing.