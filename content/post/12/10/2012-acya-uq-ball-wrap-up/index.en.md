{
  "title": "2012 ACYA UQ Ball Wrap Up",
  "author": "ACYA",
  "date": "2012-10-24T03:10:27+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div>&nbsp;</div>
<div>The 2012 ACYA Ball was a brilliant success, engaging all 4 of the project’s stated objectives. The maximum attendance of 100 pax was met, and interest in the event greatly exceeded this number. Particular mention should be made to the attendance on the night to the following guests:</div>
<div>- Councilor Vicki Howard of Central Ward, Brisbane (including the Fortitude Valley &amp; Chinatown Areas)</div>
<div>- Irene Ho, President of the Singapore Business Council of Australia</div>
<div>- Wei Guo, Secretary of the Chinese Consulate of Brisbane</div>
<div>- Ben Kelly, Business Manager at the UQ Confucius Institute</div>
<div>- Dr. Geoff Chen, Senior Deputy Director of the UQ Confucius Institute</div>
<div>&nbsp;</div>
<div>In addition, the UQ Confucius Institute generously sponsored a Chinese Tai Chi performance for the night, which tied in perfectly with the musical performances conducted by various members of the ACYA. All performances were well received by the attendees.</div>
<div>&nbsp;</div>
<div>The leveraging of the 2012 ACYA Ball as a base for the promotion of the organization’s national and local (UQ) visions, strategy and future operations and also as an opportunity for previous non-members to engage with the organization was a major success. The 2012 ACYA National President, Jeffrey Sheehy, a current student at UQ gave a 5 minute speech outlining the organization’s vision beyond 400 local members and 3,300 global members.</div>
<div>&nbsp;</div>
<div>Ample time was provided for attendees to socialize with each other. Existing executive members of the UQ Chapter were identified by a decorative ribbon pinned to their wear for the night, and<br />they were approached by many non-members seeking more information about the organization.</div>
<div>UQ Chapter membership has grown from 400 to 490 in the two weeks after the event.</div>
<div>&nbsp;</div>
<div>Executives also actively introduced key personnel and other interested members to the guests of note on the night. All guests gave their compliments to the UQ Chapter for the organization and operation of the event, and are looking forward to the 2013 occurrence of a similar event.</div>
<p><img style="display: block; margin-left: auto; margin-right: auto;" alt="UQ-Ball-Main" src="images/UQ-Ball-Main.jpg" height="305" width="450" /></p>