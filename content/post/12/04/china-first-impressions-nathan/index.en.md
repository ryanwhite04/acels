{
  "title": "China: First Impressions - Nathan (王内森)",
  "author": "ACYA",
  "date": "2012-04-11T19:07:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<h2>China: First Impressions</h2>
<p class="author">Nathan (王内森) - February 2011</p>
<p>It's only starting to hit me that I'll be in China, in this strange university and country, for a whole year. For the next 12 months, I won't be Nathan, I'll be Wáng Nèisen (王内森). Some people have asked me why I would actively choose the name Wang, despite its crude English double entendre. The answer is twofold: firstly, Wang represents my family name: it starts with w, translates to "King", and my family name would be almost impossible to transliterate into mandarin. Secondly, I've been studying Chinese for 2 years now, and if I laughed whenever I heard someone say "wang" or "dong", then I would have to study a different language. I think by far the funniest name to call yourself in China would be something like "qing chongqiang" (pronounced in English this would be "Ching ChongChang". I wouldn’t have the nerve to call myself this. But it would be funny).</p>
<p>Some people have asked me how many white people I would be around. Often this was asked sheepishly, using some sort of very transparent euphemism:<br /> "Will there be any Australians there?"<br /> "probably not. It's China. By "Australians" do you mean "white people"?"<br /> "haha. Yeah"<br /> *questioner laughs but still shifts uncomfortably and looks to the side*<br /> Kudos goes to a friend of mine, who bluntly asked me how many white people I'd be living with.</p>
<p>The answer is that there aren't very many Australians (Australians, not <em>Australians</em> ;)). There are a handful of Australians, some Europeans, a fair few North Americans, a handful of Japanese, and a hell of a lot of Koreans.</p>
<p>I live in a room to myself in a large international student dorm. The social life is vibrant, but it means that I'll have to make an active effort to make Chinese friends.</p>
<p>I've done 3 weeks' worth of classes, after suffering a Herculean series of bureaucratic nightmares involving visas, university registration, documents being lost in Chinese post, all made worse by my mediocre Chinese. Of the frustrating exercises in bureaucracy that I endured, most hilarious was the "foreigner physical examination form". This form is required for all visas longer than 6 months. It requires a blood test, a chest X ray, and an ECG. I paid for these in Hong Kong, to find when I returned to Beijing that they only really cared about the blood test – if you have a blood born disease like AIDS, then you can't get a visa. I'm AIDS free, just in case anyone was wondering. The amusing element of the form itself is its tick-the-box questionnaire. I quote:<br /> Do you have any of the following disorders endangering public order and security?</p>
<table width="300px">
<tbody>
<tr>
<td>Toxicomania</td>
<td>yes/no</td>
</tr>
<tr>
<td>Mental Confusion</td>
<td>yes/no</td>
</tr>
<tr>
<td>Manic Psychosis</td>
<td>yes/no</td>
</tr>
<tr>
<td>Hallucinatory Psychosis</td>
<td>yes/no</td>
</tr>
</tbody>
</table>
<p>I'm not sure what Toxicomania is, but if I had it and I wanted a Chinese visa, I wouldn't tell the doctor. One section of the form asks for a description of the form asks the doctor to describe my "nourishment". The doctor wrote "normal".</p>
<p>I first arrived in China in December, to travel around the country with companions Dre, Tobi and Kit in the two months before term starts.</p>
<p>In many respects we exhibited almost all the clichés that are associated with Asian tourists in Australia. We talked loudly on public transport in our strange foreign language. When people talked to us in Chinese, Tobi, Dre and Kit had no idea what they were saying. I would only have some idea of what they were saying, and would respond in grammatically incorrect and barely comprehensible sentences, to the visible irritation person of the local person. Conversations would often come to unfortunate ends with blank stares. We abused the language barrier by talking about people right in front of their faces. We were constantly snapping photos.</p>
<p>Often we would take photos of things that were utterly bizarre to us, yet so normal to the Chinese. They would stare at me taking photos, looking at me as though I had a benign and quaint form of insanity, before turning away, obviously thinking to themselves "ahh, those crazy laowai". On the Yangtze River cruise that we went on, snacks that were sold included whole roast tiny bird on a stick. The sticks were the size of paddle pop sticks, and the bird was maybe the size of a baseball. Chinese people would start eating them by biting the head off whole. I took a photo of these pitifully bony birds-on-sticks and was stared at. In supermarkets in china, you can buy live seafood. I took a photo of tanks full of live, moving crabs, and was again rewarded by piercing, puzzled stares. Crazy laowai. Don't you have live crabs in supermarkets where you come from?</p>
<p>In most of China, western tourists are a considered a fantastic novelty. Sometimes we were viewed with a fascination and awe – sometimes at a mild level, sometimes on a massive scale. Sometimes this awe and fascination would not be unlike the awe and fascination surrounding a famous rock star, sometimes this would be not unlike the awe and fascination shown by humans viewing a gorilla at the zoo. Sometimes we were even treated like normal people. Sometimes random people would go up to us and ask for them or their children to be photographed with us. Sometimes they would not ask permission, and simply photograph us. In the immortal words of that great wordsmith, Eminem: "y'all act like you've never seen a white person before..."</p>
<p>Here are few miscellaneous things that you may or may not know about China:</p>
<p>I am told that there are some numbers which have started to be used as insults. Someone who is "er" (2) is very dumb. A "san ba" (38) is an annoying girl who won't shut up. Why these numbers are used I'm not sure, except for the fact that many Chinese numbers have associations according to other Chinese words that their pronunciation resembles: for example, 4 is an unlucky number because its pronunciation is for similar to the word for death.</p>
<p>Because the meaning of words changes according to the tone, jokes can be made by using words that appear to say one thing (according to the characters) but sound very similar to other words. For example, "grass mud horse" said without tones sounds ("cao ni ma") exactly the same as "*#$@ your mother". A website appeared in 2009 that explained the mythical beast of the grass mud horse: The horse only eats "Fertile Grass" – or, alternatively, it only eats "*&amp;^% me". The Grass Mud horse is a noble animal but it is apparently under threat, by a species of river crab (he xie). The crabs are destroying the cao ni ma's habitat, namely the Grass Mud Horse Gobi desert, the Cao Ni Ma Ge Bi: "$#@% your mother's *&amp;#%". "River crab" sounds very similar to the Chinese word for "harmony". This is significant because the concept of a harmonious (he xie) society is the justification given by the authorities for their relentless censorship of the media and internet. In other words, the Grass Mud Horse is at once a very crude joke, and a symbol of defiance against oppression. Google image search "grass mud horse", you'll see that plushie toys of the creature can be bought.</p>
<p>The standard toilet in China is a flushing squat toilet. In theory this is more hygienic, because you don't physically touch anything. In practice they are almost always disgusting, not to mention less comfortable to use. Public toilets do not have toilet paper, so a personal supply must be kept at all times. If one is caught without said personal supply, then the otherwise useless 1 Jiao (2 AU cents) notes suddenly have a new use to them. Using these notes for this purpose, I was struck by the eerie thought that if I had been caught using money for the purposes of personal hygiene—even as late as the early seventies—then I probably would have been executed for it. My motives were apolitical, but it would not have mattered. Besmirching something with the Chairman's image would have resulted in me being declared an enemy of the state.</p>