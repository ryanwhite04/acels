{
  "title": "Australia in the Asian Century Competitions",
  "author": "ACYA",
  "date": "2012-04-10T21:31:59+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
The Australian Institute of International Affairs has created a series of online competitions to encourage students to be a part the debate around the White Paper on Australia in the Asian Century.

There are three competitions:
  Be Heard: a short essay competition focusing on Australia in the Asian Century
  How You See It: a multimedia competition for students to reveal how they see Australia in the Asian Century through images, short video clips or infographic presentations
  Make a Pitch: a competition for students to create a pitch to either the Australia Network or Radio Australia for a new program to promote Australia in Asia

Prizes include:
  Pitch your concept to the Australia Network or Radio Australia
  Meet with Australia in the Asian Century Taskforce members and/or attend an Asian Century event, including travel costs
  Be published in an ACCESS publication
  Complementary membership to an AIIA Branch <a href="http://www.australiasroleintheworld.org.au/australia-in-the-asian-century-competition/" target="_blank" rel="nofollow nofollow">http://www.australiasroleintheworld.org.au/australia-in-the-asian-century-competition/</a>