{
  "title": "Partaaay time! - Cao Yuanshan (曹远山)",
  "author": "ACYA",
  "date": "2012-04-11T19:21:59+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Partaaay time!</h2>
<p class="author">Cao Yuanshan (曹远山) - March 2011</p>
<p>Every once in a while HK transforms into an uninhibited partaaay mecca. There's New Year's Eve, Halloween and this weekend it was the HK Sevens, probably the wildest annual bash that this thriving city throws up.</p>
<p>Over three days spanning the last weekend of March, 24 rugby teams from around the globe descend into HK and play the glorious game of rugby sevens. Sevens is a faster, shorter and, even to a purist like me, more exhilarating version of rugby union, played by teams of seven players instead of fifteen. Gone are the long kicks, penalty shots and endless scrum repacks which are replaced by fast running, slick passing and hallelujah high scores. Each game lasts only 14 minutes with a 2 minute half-time break. Group matches are churned through on Friday and Saturday while Sunday is the business end with the finals.</p>
<p>In case you have no idea what rugby is – SHAME ON YOU – it's rugby world cup year so jump onto Wikipedia and Youtube and familiarize yourself with the "game they play in heaven". Just kidding, you'll soon come to grips if you're living in Australia because rugby is the topic of conversation in every bar, pub, club across the country and with the world cup to be held just across the Tasman, I can only imagine the amount of sheep jokes that will be floating around when the tournament kicks off.</p>
<p>Anyway back to the Sevens. The weekend is not just about the rugby, accompanying good sport is always amazing partying. The South Stand at the stadium is a sea of fancy-dressed, inebriated, parochial and most importantly, FUN spectators that sing, chant, drink and dance their way through the games. I was amongst this mass on Sunday and had a whale of a time. I was showered with beer, sledged by poms and drowned out by their bloody "Sweet Chariot" anthem. The Charlie Sheen appreciation group, North-Bondi lifeguards, Mainland officers, Kick-arse + Hit-girl, Spongebob, black swans, Mankinis, Wonderwoman (or was it Wonderman?) were just some of the wonders I met. As the day grew towards its crescendo, the Cup final between England and NZ (my beloved Australia had lost to South Africa for the Plate title), I'd already lost my voice and could no longer continue my verbal jousts with the army of poms surrounding me. Luckily, my brothers from NZ won the day and the disgusted looks of the English drew a big smirk across my face.</p>
<p>The end of the sporting facet does not signal the end of the festivities. Most of the fans, still in their sumptuous costumes, spill into neighboring Lan Kwai Fong (aka party central) and revel until the new dawn!</p>
<p>Will I be back next year? HELL YEAH! And I Hope to see more Aussies in the stands so we can out-gun those bloody poms. If ever you're in town towards the end of March, don't miss this massive celebration.</p>
</div>