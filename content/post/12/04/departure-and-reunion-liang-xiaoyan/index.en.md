{
  "title": "Departure and Reunion - Liang Xiaoyan (梁小艳)",
  "author": "ACYA",
  "date": "2012-04-11T19:27:30+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<h2>Departure and Reunion</h2>
<p class="author">Liang Xiaoyan (梁小艳) - February 2011</p>
<p>Apart from life and death, departure and reunion are two countering yet recurrent themes that keep in play in one's life. Back to 2005 when I first left home for a university far away, I was thrilled to be finally out of the reign of my over-protective parents and primitive way of country life. But in less than one year's time, I have realized how wise our predecessors are when they say "east or west, home is best". As I grow older, I spend most of my time in a cosmopolitan city, only to grow more emotionally attached to my parents, siblings and the little town I grew up in. Thus, in this Spring Festival holiday season, I allowed myself a luxurious break of almost 40 days to have my fill of "HOME", which should make my departure somewhat sweet. Disappointedly, when I had to get on the airplane today, my insides were churning like crazy. There was an ache in my heart with a strong longing to stay as I stared at my parents with their faces full of reluctances to goodbye. An old Chinese saying summarized it well: "the most bitter experience of life is departure" (人生最苦是离别).</p>
<p>The two-and-a-half hour flight was nice and smooth. The minute I got out of the plane at Shanghai Hongqiao Airport, I was overwhelmed with the robust pulse of the city, which formed a distinct contrast with the peaceful and quiet country life I just left behind. There were so many people aiming for the same train which made getting on the subway an annoying chore, but once on it, the task of fighting for a standing space with my suitcase, my computer case and my purse was even more tiresome. I finished the one-hour subway ride almost with my back arched over my luggage. It was such a delight to finally get off at the school stop and meet with my roommate who came to pick me up. It turned out that she and some other girls had been preparing a "dormitory feast" for the happy reunion tonight. Before I even got to jump in the shower, they dragged me to join their washing, peeling, chopping, cooking, tasting, and commenting party. The girls from Sichuan brought some home-made spicy sausages, the girl from Hunan contributed some red pepper sauce, and the girl from Henan shared with us her Mum's secret-recipe Pickles whose raw materials were unidentifiable to even her. Besides these, a whole package of noodles was cooked with some lettuce and Chinese cabbage. At the end of the meal, I handed out to everyone candies and packaged apricots that I had brought from home as dessert. The reunion reached climax when everyone got actively involved in the topic of the TV Program "If you are the one" (非诚勿扰)—A dating show about love and marriage of Jiangsu TV which generated tons of teasing and laughter.</p>
<p>For me, tonight was a perfect first night back to Shanghai, the city which is notorious in its capacity to provide dwellers with a genuine sense of belonging. Today has been a perfect day, with bitter departure and sweet reunion integrated, which were eventually put to a memorable past by a whole bright beginning of tomorrow.</p>