{
  "title": "Education",
  "author": "ACYA",
  "date": "2012-04-10T16:47:06+10:00",
  "image": "",
  "categories": [],
  "tags": [
    "Education"
  ],
  "locations": [],
  "organisations": [],
  "resources": []
}
Please visit our Why China page and learn why YOU should consider studying in China.

Visit our Chinese Universities pages to read exclusive reviews from ACYA members who have previously studied in Chinese Universities.
Our Chinese Universities pages will provide you with useful student perspectives of their experiences as well as impressions of the course, accommodation and student life in various Chinese universities.

<!--more-->

Our China Scholars Directory page provides a directory of Australian-based academics with research interests in China who can provide advice for Australian students who plan to study in China.

Click onto the Engaging China Project to see our flagship educational project - increasing awareness of China in schools.
If you’re a student, visit this page to find out how you can become a presenter - or, if you’re a Chinese teacher - visit this page to find out how your school could get someone to present to your students.

If you're interested in studying in China via an exchange program, remember to check your home university's study exchange page to check which Chinese institutions your university has affiliations with:
<h3>Australian Capital Territory</h3>
<ul>
 	<li><a href="http://info.anu.edu.au/studyat/International_Office/exchange">Australian National University</a></li>
 	<li><a href="http://www.canberra.edu.au/study-abroad/travel-os/study-os">University of Canberra</a></li>
</ul>
<h3>New South Wales</h3>
<ul>
 	<li><a href="http://www.international.mq.edu.au/abroad/partners/partner_directory.aspx?pageIdentifier=abroad">Macquarie University</a></li>
 	<li><a href="http://www.international.unsw.edu.au/exchange/exchangepartners.html">University of New South Wales</a></li>
 	<li><a href="http://fmweb01.ucc.usyd.edu.au/international/exchange">University of Sydney</a></li>
 	<li><a href="http://sitesearch.uts.edu.au/international/exchange">University of Technology Sydney</a></li>
 	<li><a href="http://www.newcastle.edu.au/Resources/">Newcastle University</a></li>
 	<li><a href="http://www.uws.edu.au/international/exchange_programs">University of Western Sydney</a></li>
</ul>
<h3>Queensland</h3>
<ul>
 	<li><a href="http://www.bond.edu.au/study/international/internationalexchange/institutions.html">Bond University</a></li>
 	<li><a href="http://www.uq.edu.au/uqabroad/china">University of Queensland</a></li>
 	<li><a href="http://www.jcu.edu.au/student/exchange">James Cook University</a></li>
 	<li><a href="http://www.exchanges.qut.edu.au/outbound">Queensland University of Technology</a></li>
 	<li><a href="http://www.griffith.edu.au/international/exchange/outgoing-exchange/partner-institutions">Griffith University</a></li>
</ul>
<h3>South Australia</h3>
<ul>
 	<li><a href="http://www.adelaide.edu.au/student/study_abroad/partners/">University of Adelaide</a></li>
 	<li><a href="http://www.flinders.edu.au/international-students/study-abroad-exchange/partner/asian-student-%20exchange-partner-institutions.cfm#China">Flinders University</a></li>
 	<li><a href="http://www.unisa.edu.au/exchange/partners/default.asp#China">University of South Australia</a></li>
</ul>
<h3>Tasmania</h3>
<ul>
 	<li><a href="http://www.international.utas.edu.au/static/StudentMobility">University of Tasmania (HK only)</a></li>
</ul>
<h3>Victoria</h3>
<ul>
 	<li><a href="http://www.deakin.edu.au/future-students/student-exchange/exchange/exchange-locations.php#chin">Deakin University</a></li>
 	<li><a href="http://www.latrobe.edu.au/international/edabroad/exchange/partners">La Trobe University</a></li>
 	<li><a href="http://www.monash.edu.au/students/studyabroad/programs/partners/china.html">Monash University</a></li>
 	<li><a href="http://www.rmit.edu.au/browse;ID=qfohsdoihm1v1">Royal Melbourne Institute of Technology</a></li>
 	<li><a href="http://www.mobility.unimelb.edu.au/outgoing/apply/partners/partners.html">University of Melbourne</a></li>
 	<li><a href="http://courses.swinburne.edu.au/Partners/BrowseExchangePartners.aspx#People">Swinburne University of Technology</a></li>
 	<li><a href="http://internationalvuedu.easycgi.com/international/opea/OPEA_Exchange_List.asp?SortBy=Country">Victoria University</a></li>
 	<li><a href="http://www.ballarat.edu.au/fdp/international/forms/Exchange_SA_Partners_2009.pdf">University of Ballarat</a></li>
</ul>
<h3>Western Australia</h3>
<ul>
 	<li><a href="http://outboundstudy.curtin.edu.au/where/">Curtin University of Technology</a></li>
 	<li><a href="http://www.ecu.edu.au/international/studyabroad/preparing/asianpartners.html">Edith Cowan University (HK only)</a></li>
 	<li><a href="http://www.international.uwa.edu.au/index/sesa/student_exchange_out_test?sq_content_src=%20%2BdXJsPWh0dHAlM0ElMkYlMkZ3d3cuYWRtaW4udXdhLmVkdS5hdSUyRmljJTJGY21zJTJGc2%20VvdXQuYXNwJmFsbD0x">University of Western Australia</a></li>
</ul>
<h3>Multi-state</h3>
<ul>
 	<li><a href="http://www.acu.edu.au/international/study_options/education_abroad/outgoing_students/exchange_part%20ner_institutions/#china">Australian Catholic University</a></li>
</ul>