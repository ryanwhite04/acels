{
  "title": "Canberra Culture - Amy Wei",
  "author": "ACYA",
  "date": "2012-04-11T19:14:31+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Canberra Culture</h2>
<p class="author">Amy Wei - March 2011</p>
<p>Canberra has been at various occasions lovingly coined as the 'Crapital' or 'Crapberra', or more often simply just 'the Hole'. As promised in the last blog, I'm going to try my best to give it a proper defence, or at least one to the extent it deserves. As small and quiet as it is in comparison to some other national Capitals around the world, there are nevertheless some unique features of Canberra that is hard to find elsewhere in Australia. These are on the most part due to the wealth of federal funding it receives.</p>
<p>Skyfire is one such example, an annual fireworks show held over Lake Burley Griffin, synchronised to music blasted out of speakerphones that lasts approximately 20 minutes. It has become a tradition in Canberra, and every year draws what seems like the whole of the Canberran population to the banks of the Lake for a nice evening in the late summer breeze. A tip to those who prefer a more quiet and private viewing (perhaps for a romantic date?) – the top of Black Mountain also offers an excellent view but away from the crowd. Where else can a whole city population gather for a free community event that costs hundreds of thousands of dollars to put on?</p>
<p>A week before Skyfire, a friend and I in a sudden bout of spontaneity decided to go to 'Symphony in the Park—A Night on Broadway'. Another free annual community event, this time held in the Commonwealth Park. The standard of the performances and the calibre of the performers greatly exceeded our expectations. To have Peter Cousens, who played the Phantom in the Phantom of the Opera on the West End serenade you with 'Music of the Night' was a soul-melting experience.</p>
<p>I am aware that not all of my peers are passionate Broadway (and Glee) fans like my friend and I, and the only thing buzzing in Canberra most of the time are the humongous bees around campus, but when your text book fails to enthral, some fireworks and beautiful music are not all that bad company for a few twilight hours.</p>
</div>