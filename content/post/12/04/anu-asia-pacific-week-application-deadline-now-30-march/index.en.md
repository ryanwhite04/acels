{
  "title": "ANU Asia Pacific Week application deadline now 30 March",
  "author": "ACYA",
  "date": "2012-04-10T21:23:19+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="news_itemcontentarea">
<div id="contentpage" class="contentpage">
<p style="text-align: left;"><span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;">ANU Asia Pacific Week has extended the applications deadline to 30 March 5pm (Canberra Time) due to a last minute surge in applications forms. </span><br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;" /> <br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;" /> <span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;">Just to clarify who is allowed to attend this fantastic conference; any advanced undergraduate students as well as masters and doctoral students with an interest in the Asia Pacific region are allowed to apply. There is a wide variety of scholarships available to both international and Australian delegates.</span><br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;" /> <br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;" /> <span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;">For more information, including how to apply, go to </span><span id="TSRSpan_1" class="TSRSpan" style="cursor: pointer; display: inline-block; padding: 0px 2px 0px 0px; color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px;"><img class="TSRWebRatingIcon" src="chrome-extension://heoldelcflnigdllmlopiefhkkobendj/_locales/en/img/tooltip/webicon_green.gif" border="0" style="border-width: 0px; margin: 0px; padding: 0px; width: 16px; height: 16px; vertical-align: middle;" /></span><a href="http://asiapacificweek.anu.edu.au/" target="_blank" rel="nofollow nofollow" style="cursor: pointer; color: #3b5998; text-decoration: none; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; background-color: #b8eab8;">http://asiapacificweek.anu.edu.au/</a></p>
</div>
</div>
<p> </p>