{
  "title": "Board of Advisors",
  "author": "ACYA",
  "date": "2012-04-05T19:08:14+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
ACYA has developed a Board of Advisors. We have appointed 8 mentors who have an active interest in the Sino-Australian relationship to join our Board of Advisors and help enhance ACYA's rapid growth.

<!--more-->

The Board meets twice a year (via video conference) and provides their input with respect to the current and future direction of ACYA.

Please click here to view the ACYA Board of Advisors Terms of Agreement.

<a href="images/ACYA_BOA_Terms_of_Agreement.pdf">Terms of Agreement</a>.
<h2>Professor Geremie R. Barmé</h2>
Director of the Australian Centre on China in the World

After graduating in Asian Studies from the ANU (majoring in Chinese and Sanskrit), Geremie studied at universities in the People's Republic of China (1974-77) and Japan (1980-83), with periods working as a journalist, freelance writer and translator in China. Geremie's research work in Chinese culture and intellectual history has been interspersed with film, web site and writing projects in the United States and China.

Geremie's research interests include 20th century Chinese intellectual and cultural history; contemporary Chinese cultural and intellectual debates; modern historiography; Ming-Qing literature and aesthetics; Cultural Revolution history (1950s-70s) and Beijing, its history and reconstruction.
<ul>Geremie's key publications include:
 	<li>The Gate of Heavenly Peace, three-hour documentary film (associate director and main writer), 1995.</li>
 	<li>Shades of Mao: The Posthumous Cult of the Great Leader, Armonk, NY, M.E. Sharpe, 1996.</li>
 	<li>In the Red: On Contemporary Chinese Culture, New York, Columbia University Press, 1999.</li>
 	<li>An Artistic Exile: A life of Feng Zikai (1898-1975), Berkeley, University of California Press, 2002, awarded the Joseph Levenson Prize for Modern China, 2004.</li>
 	<li>Morning Sun, two-hour documentary film (co-directed and co-produced with Carma Hinton and Richard Gordon; co-written with Carma Hinton), 2003, awarded the John E. O'Conner Film Award, The American Historical Association, 2005.</li>
 	<li>(ed. with Miriam Lang) China Candid: The People on the People's Republic, by Sang Ye, University of California Press, 2006.</li>
 	<li>(ed. with Claire Roberts) The Great Wall of China, Sydney, Powerhouse Museum, 2006.</li>
</ul>
Gereme's cites his personal career highlights to be the premiere of The Gate of Heavenly Peace at the New York Film Festival in 1995; premiere of Morning Sun at the Berlin International Film Festival in 2003; being awarded the Joseph Levenson Prize for Modern China, 2004; working with the New York photographer Lois Conner on the Garden of Perfect Brightness (Yuanming Yuan) in Beijing (1998-2003); and working on the sacred geography of China with the photographer Lois Conner and the Sinologist and translator John Minford.
<h2>Associate Professor Vivienne Bath</h2>
Vivienne Bath is an Associate Professor at the Faculty of Law, University of Sydney, Director of the Centre for Asian and Pacific Law at the University of Sydney and Deputy Director of the Australian Network for Japanese Law. She teaches International Business Law, courses on Doing Business in China and Chinese law

She has first class honours in Chinese and in Law from the Australian National University, and a Master of Laws from Harvard University. Prior to joining the Faculty of Law, she was a partner of international firm Coudert Brothers, working in the Hong Kong and Sydney offices, and specialising in commercial law, with a focus on foreign investment and commercial transactions in the People's Republic of China.

She previously practised as a commercial lawyer in New York and Sydney, and worked in the Federal Office of Parliamentary Counsel as a parliamentary draftsman. She speaks Chinese and German.
<ul>Her most recent publications and presentations include:
 	<li>Burnett and Bath, Law of International Business in Australasia, Federation Press 2009.</li>
 	<li>Bath, V, "China and WTO Transparency Requirements‟ in China's Integration with the Global Economy WTO Accession, Foreign Direct Investment and International Trade pp53-70 (Chunlai Chen ed., 2009 Edward Edgar, Cheltenham UK).</li>
 	<li>Bath, V, "Reducing the Role of Government – the Chinese Experiment," (2008) Asian Journal of Comparative Law Vol 3, issue 1, Article 9.</li>
 	<li>Bath, V, "The Company Law and Foreign Investment Enterprises in the People‟s Republic of China – parallel systems of Chinese-foreign regulation," (2007) 30(3) UNSW Law Journal 774.</li>
 	<li>Bath, V, "Inbound and outbound Investment – the Quandary for Chinese regulators," International Investment Arbitration conference, Sydney University 19-20 February 2010.</li>
 	<li>Bath, V, "Inbound and outbound Investment – the Quandary for Chinese regulators," International Investment Arbitration conference, Sydney University 19-20 February 2010.</li>
 	<li>Bath V, "China, Foreign Investment and Mandatory Rules," presented at the Asian Law Institute conference, Hong Kong University 30 May 2009.</li>
</ul>
<h2>Mr. Rowan Callick</h2>
<h3>Professional Background</h3>
Mr Rowan Callick grew up in England, graduating with a BA Honours from Exeter University.

He worked for a daily newspaper in the north east before moving in 1976 to Papua New Guinea, where he became general manager of a locally owned publishing, printing and retail group.

In 1987 he moved to Australia, working for almost 20 years for The Australian Financial Review, finally as Asia Pacific Editor. He was China Correspondent for The Australian Financial Review, based in Hong Kong, from 1996-2000.

From 1990-1992 he was a senior writer with Time magazine.

Mr Callick joined The Australian at the start of 2006, as China Correspondent. After three years in Beijing, he became The Australian's Asia-Pacific Editor at the start of 2009.
<h3>Board Memberships</h3>
Member of the National Advisory Council on Aid Policy from 1994-96

Member of the Board of Australia Indonesia Institute from 2001-2006

Member the Foreign Minister's Foreign Affairs Council from 2003-2006

Member of the advisory board of the Institute of Excellence in the Asia-Pacific Region at the Australian National University from 2005-2006
<h3>Publications</h3>
Mr Callick's book "Comrades &amp; Capitalists: Hong Kong Since the Handover" was published by the University of NSW Press in 1998.

He won the Graham Perkin Award for Journalist of the Year for 1995, and two Walkley Awards, for Asia-Pacific coverage, for 1997 and 2007.

He is married with two children.
<h2>Mr. Clinton Dines</h2>
Clinton Dines graduated from Griffith University in Australia in 1978 and INSEAD in France in 1996. He studied Asian Studies/Economics and Business.

Clinton went to China in early 1979 on a post-graduate program arranged by Griffith and has lived and worked continuously in the Greater China Region since then, mostly in mainland China.

After spending the early part of his career with the Jardine Matheson Group, Santa Fe Transport Group and Asia Securities Venture Capital, he joined BHP 1988, serving for 21 years as the Company's senior executive in China before retiring from the role in August 2010.

He is currently a Non-Executive Director of Kazakhmys PLC, a Visiting Fellow of the Lowy Institute for International Policy and a Member of the Griffith University Council.

Clinton was a prime mover in the formation of the Australian Chamber of Commerce in China in the early 1990s, a Founding Governor of the Capital Club in Beijing in 1994 and has served on the Committee of the Shanghai Rugby Football Club since 2002. He is on the Committees of the Sport for All Foundation and the Oriental Mining Club, and is a member of the Half the Sky Foundation Australian Board.
<h2>Dr. Jianzhong Lu</h2>
Dr. Lu is a BHP Billiton senior executive currently based in Shanghai and regularly travels between Shanghai, Beijing and Australia for work. He has led a highly diverse and colourful life, with many notable career accomplishments. As an undergraduate student, Dr. Lu studied Chemical Engineering at Zhejiang University in China, which eventually led him to complete his Masters in Industrial Management and Innovation at the Grande Ecole Centrale de Paris in France. He went on to complete his PhD degree in Electronic Commerce at the University of Royal Melbourne Institute of Technology in Australia.

Upon graduation, he worked for the United Nations as China's UN Trade Efficiency Program Country Manager. He worked closely with the Chinese Ministry of Foreign Trade and Cooperation and went on to become an Expert Diplomat for the United Nations Conference on Trade and Development but later decided that his expertise was more suitable for the private sector. In 2000, he began his career with the now BHP Billiton group and moved to Australia, where he oversaw the successful merger between BHP Limited and Billiton Plc in 2001. His positions within BHP Billiton has included Country Manager of Technology in China, where he oversaw R&amp;D Strategy, Development, Collaborations with CAS and mining industries in mainland China. He has also managed Business Development within Uranium resources and other joint venture establishments in China. He is currently the Vice President of Corporate Affairs and oversees strategic relations and development collaborations with the Chinese government, media and other community stakeholders.

In his spare time, Dr. Lu enjoys the outdoors and is a big fan of Aussie Rules football. Dr. Lu is passionate about giving back to society and is also actively involved in several NGOs both in China and internationally. He was also a Visiting Senior Lecturer at Monash University for the MBA program in Electronic Commerce.
<h2>Mr. Laurie Pearcey</h2>
Executive Director and Company Secretary, Australia-China Business Council Laurie Pearcey is Executive Director and Company Secretary of the Australia-China Business Council.

Laurie is a fluent Mandarin speaker and a Visiting Fellow in Pacific and Asian History at the Australian National University. Laurie was appointed as a Scholar of the Order of Australia Association Foundation by the Governor-General for community leadership and excellence in Chinese Studies. He is a University Medalist in both Asian Studies and International Studies at the University of New South Wales.

Laurie has previously worked as a presenter on China Central Television's International Chinese language station and worked for a range of MPs including Kevin Rudd and also as a policy adviser to the former Queensland Minister for Environment, Heritage and Natural Resources.

Laurie is a Member of the Department of Resources, Energy and Tourism's Approved Destination Scheme's Advisory Panel, which provides a representative interface between government and the tourism industry. He is a Member of the Sydney Asia Alliance.

As Executive Director and Company Secretary of the Australia China Business Council, Laurie is responsible for working with the Chairman and the Board of Directors on research, policy and corporate governance as well as managing key relationships with the Australian Government and Chinese consular corps and coordinating the Council's national engagement with corporate Australia.

Laurie is committed to multiculturalism and has engaged in community work for the City of Sydney's Chinese New Year Celebrations, the City of Parramatta, the NSW Chinese Language Education Council, SBS Mandarin Radio and is the inaugural recipient of the NSW-Vietnamese Community Association Prize for outstanding service to the NSW Asian community.
<h2>Mr. Edward Smith</h2>
Managing Director, Beijing Consulting Group &amp; Founder, Australia China Alumni Association

Edward is a highly experienced China business advisor who has lived and worked in China since 1990. He holds a Bachelor of Information Systems degree from Monash University (for which he received a full-scholarship) and is a fluent Mandarin speaker, having studied Chinese language, history and politics at the Beijing Second Foreign Language Institute, the University of Melbourne, and the Johns Hopkins Centre at Nanjing University.

Edward's career has spanned working as a computer programmer and systems analyst at National Australia Bank in Melbourne, working for the Immigration Department at the Australian Embassy in Beijing, and working with Austrade where he helped set up the Australian Consulate General in Guangzhou.

He has also worked at Fortune 500 Japanese trading house, Kanematsu, in Melbourne and at a US consulting firm in Beijing which specializes in assisting Western companies to invest and establish operations in China. Edward has assisted numerous Fortune 500 companies to set up or expand their operations in China has undertaken major projects in sectors including aerospace, specialty inks, printing and packaging, ICT, construction, entertainment, etc.

Edward co-founded the Beijing Consulting Group in 2000 with two partners. Since then, BCG has grown rapidly to become a leading Australia-China consulting firm with several offices across China. BCG operates nationally across China and has assisted numerous Western firms to establish businesses in China. BCG has also undertaken a number of projects for various Australian government departments.
<ul>Edward currently serves or has served on the Board of Directors of several organizations, including:
 	<li>The Australian Chamber of Commerce in Beijing (1999-2008)</li>
 	<li>Lai Te (Beijing) Electric &amp; Electronic Equipment Co. Ltd. (2004-2009), a Sino-Australian joint venture company which manufactures power saving lighting equipment which is sold across China and exported to a number of other countries.</li>
 	<li>The Australia China Alumni Association – a not-for-profit organization established in 2007 to link Alumni of Australian Universities in China to each other and their Universities and to raise the profile of Australian education in China. (<a href="www.AustChinaAlumni.org">www.AustChinaAlumni.org</a>)</li>
</ul>
Edward served as Deputy-Chair of AustCham Beijing from November 2002 to March 2007 and has previously served as Chair of the AustCham Education &amp; Training Working Group as well as being an active member of the AustCham Government Relations &amp; External Relations Committees.

Edward also served as member of the judging panel for the AustCham Australia China Business Awards in 2006 and the Australia China Alumni Awards in 2009-2010. He was a regular contributor to Australia China Connections magazine for many years.

Edward has been a guest lecturer for Melbourne Business School's EMBA classes in China was a judging panelist for the University of South Australia's MBA program China Intensive School program from 2008-2010. He has also addressed Senior Executive Groups from CISCO, National Australia Bank, Deutsche Bank, etc.

Edward is a regular presenter at China Business &amp; Education conferences and joined the Australia China Youth Association's (ACYA) Board of Advisors in 2010.

Edward is married with 3 children and resides permanently in Beijing.
<h2>Mr. Antony Dapiran</h2>
Partner, Davis Polk &amp; Wardwell LLP, Hong Kong

Antony Dapiran is a leading international lawyer, and has lived and worked in Greater China for over 12 years. He spent 11 years with Freshfields Bruckhaus Deringer LLP, where he was most recently managing partner of the firm's Beijing office. He has recently joined the global law firm Davis Polk &amp; Wardwell LLP in Hong Kong as a founding partner of their Hong Kong practice.

Antony advises on corporate transactions, specialising in securities offerings, private equity and cross-border mergers and acquisitions. He has advised on the largest and most complex initial public offerings (IPOs) in the China market, on transactions raising in aggregate more than US$58bn, including most recently the IPO of Agricultural Bank of China, the world's largest IPO to date. He is also counsel to Fortune China 100 companies advising them on regulatory, compliance and general corporate matters.

Antony also has an active pro bono practice, and has advised China-based children's charity Half the Sky Foundation on a ground-breaking partnership with China's Ministry of Civil Affairs, and their strategic alliance with the China Care Foundation.

Antony is admitted as a legal practitioner in Hong Kong, England &amp; Wales, New South Wales and the High Court of Australia. He is listed in the Chambers Asia, IFLR1000 and Asia Pacific Legal 500 directories of leading business lawyers, as well as the Guide to the World's Leading Capital Markets Lawyers.

Antony was named one of the "Advance Asia 50" Australian leaders in Asia, and has been recognised by The American Lawyer as one of the "New China Hands". He is a frequent commentator on Chinese business and legal matters, and his views have been quoted in media including the Wall Street Journal, New York Times, Business Week, Sydney Morning Herald and South China Morning Post.

Antony was educated at The University of Melbourne and Peking University, and is fluent in Mandarin.