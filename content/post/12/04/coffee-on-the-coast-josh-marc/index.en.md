{
  "title": "Coffee on the Coast - Josh Marc",
  "author": "ACYA",
  "date": "2012-04-11T19:20:04+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Coffee on the Coast</h2>
<p class="author">Josh Marc - April 2011</p>
<p>In my last blog I stressed the benefits of living on the Gold Coast specifically in terms of lifestyle and work life balance. One such thing, which to me encapsulates the very essence of a positive lifestyle and a productive work life balance, is the joy of a good coffee. Indeed as many Australians will attest, the love of a beautiful flat white (a standard Australian expression for a drink consisting of one third espresso and two thirds frothed milk) is both alluring and addictive, that is quite the combination. However a bad flat white can leave one despondent and unruly, such is the importance of good coffee.</p>
<p>The main road that runs parallel to the beach from the north of the GC right down to the south is spotted with many a place to grab a flat white. However their quality is variable, so I shall steer you to a place which combines a wonderfully laid back atmosphere with a good quality espresso. The place I talk of is called Crema Coffee and is located on Tedder Avenue main beach. My advice is to start with a flat white then move on to the espresso.</p>
<p>My case, that is my love of coffee is definitely not an isolated occurrence, Australia has embraced coffee as a social drink with addictive force. It has become a phenomenon that is beyond fashionable. Coffee culture in Australia and indeed in southeast Queensland where I am from has grown into quite the market with many small vendors opening. To savour the taste and company with whom you share it is a delight and it is probable that such a relaxing culture will be quite successful in the generally laidback atmosphere of the GC. I look forward to sampling more from such establishments.</p>
<p>Hope you find your coffee and enjoy. I know I will.</p>
</div>