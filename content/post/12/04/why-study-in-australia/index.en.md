{
  "title": "Why Study in Australia?",
  "author": "ACYA",
  "date": "2012-04-10T16:49:25+10:00",
  "image": "",
  "categories": [],
  "tags": [
    "Education"
  ],
  "locations": [],
  "organisations": [],
  "resources": []
}
Australia is a lucky country, being endowed with wide-open spaces, modern cosmopolitan cities and a slow pace of life — all of which make it an ideal place to study and to live.

<!--more-->

To someone who has never been there before "Australia" may evoke images of bouncing kangaroos and cuddly koalas — there are a lot of kangaroos and cuddly koalas, but it is more than this. Australia is a place where a bit of 'hard yakka' (hard work) can lead to a better life — you get back what you put in. It's a place of mateship and egalitarianism, where a neighbour is always willing to lend a hand, and where no one is better than another. In short, it's a great place to live.

Australia is also a place with many strong educational institutions, with great vocational and university courses (please see our review Australian universities page) — not to mention courses on studying English. And the education does not stop at the classroom door. Australian cities are vibrant cultural microcosms, with a gargantuan array of festivals, museums, concerts and restaurant/bars — all these venues offer the opportunity to increase your cultural awareness and experiences. One night you can dine on the finest French-Australian fusion cuisine, another you can go to a free water festival (Melbourne's Moomba festival for instance). Australia has it all.

So, for a chance to get a good education and to have a good time while doing so， Australia is the place to be.

For more information on studying in Australia please visit <a href="http://studyinaustralia.gov.au/Sia/zh/Home.htm">this website</a>