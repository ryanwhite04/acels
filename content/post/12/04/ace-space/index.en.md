{
  "title": "ACE Space",
  "author": "ACYA",
  "date": "2012-04-10T17:11:52+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Each of ACYA's chapters operates an ACE Space - a weekly gathering where young Chinese and young Australians can meet to practice their language skills, offer advice, exchange ideas, watch movies and just generally to catch-up. The Australia-China Exchange Space takes many forms across the chapters, with some choosing to call it by different names, but ultimately with the same substance of promoting cross-cultural friendships and understanding. Contact your local Chapter President for information on how you can participate in your chapters ACE Space.