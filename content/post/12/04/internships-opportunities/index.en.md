{
  "title": "Internships Opportunities",
  "author": "ACYA",
  "date": "2012-04-10T21:20:21+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="contentarea_box" style="text-align: left;"><a href="index.php/en/careers" target="_blank"><img src="images/careers_shanghai-base-jump_resized2.jpg" border="0" alt="Representing in Careers Shanghai China Australia" title="China Australia ACYA Careers" /></a></div>
<div class="contentarea_box" style="text-align: left;">Considering a career on the mainland or down under? Check out ACYA's one-stop-shop for all Australia-China related internship and career information. </div>