{
  "title": "Extending a hand of friendship in wake of Sydney train attack",
  "author": "ACYA",
  "date": "2012-04-25T04:58:21+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>72</o:PixelsPerInch>
  <o:TargetScreenSize>1024x768</o:TargetScreenSize>
 </o:OfficeDocumentSettings>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Normal</w:View>
  <w:Zoom>0</w:Zoom>
  <w:PunctuationKerning/>
  <w:DrawingGridVerticalSpacing>7.8 pt</w:DrawingGridVerticalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
  <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:SpaceForUL/>
   <w:BalanceSingleByteDoubleByteWidth/>
   <w:DoNotLeaveBackslashAlone/>
   <w:ULTrailSpace/>
   <w:DoNotExpandShiftReturn/>
   <w:AdjustLineHeightInTable/>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]--><!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]--></p>
<p class="MsoNormal">The Australia-China Youth Association (ACYA) condemns the reported assault of two Chinese international students on a late-night train in Sydney early yesterday morning. ACYA deplores the actions of the perpetrators and offers sincere regret to the two students injured. </p>
<p class="MsoNormal"><span lang="EN-AU">We would like these students, and Chinese students everywhere, to understand that this attack, including the racist assault, stands in stark contrast to the values of the vast majority of Australians. That is, an abhorrence towards assaults on vulnerable persons, towards failing to help those in need and towards racism in any form. </span></p>
<p class="MsoNormal"><span lang="EN-AU">This event is not only tragic for those involved, but also damaging to Australia’s reputation in China. This event has reverberated thousandfold around China, on <em>weibo</em> and in mainstream media. </span></p>
<p class="MsoNormal"><span lang="EN-AU">Chinese students and citizens must understand the rarity of this attack. ACYA implores that this attack was not directed at Chinese, but rather an opportunistic crime that unfortunately involved a number of persons, including two Chinese students. Nevertheless, the incident is extremely serious, and the concerns of the Chinese student community should be addressed at the highest levels of both state and federal government.</span></p>
<p class="MsoNormal"><span lang="EN-AU">Given the importance of China to Australia’s future, governments should focus on building cultural understanding between Australians and Chinese, and eliminating racism in schools and the broader community. On this ANZAC Day, Australians should extend the hand of friendship to Chinese students, as ACYA has. The future of Australia-China relations lies in the positive interaction between Australian and Chinese youth, not in callous acts of violence on late-night trains.</span></p>
<p> </p>