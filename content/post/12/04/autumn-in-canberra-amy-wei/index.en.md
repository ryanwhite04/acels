{
  "title": "Autumn in Canberra - Amy Wei",
  "author": "ACYA",
  "date": "2012-04-11T19:15:01+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Autumn in Canberra</h2>
<p class="author">Amy Wei - April 2011</p>
<p>I was recently asked what my idea of an ideal date would be, and I responded with, "April 25th, because it's not too hot, not too cold and I only need a light jacket." Aside from my shameless hijack of a Miss Congeniality quote, it did lead me to reflect on the amazing autumnal weather Canberra has had recently.</p>
<p>One of the privileges of living in Canberra is that it is a city that experiences four seasons, of which autumn would have to be one of the prettiest. Whether you are walking across campus, by the lake or on the way to parliament house, you will see a full kaleidoscope of colours, veiled by a clear blue sky.</p>
<p>Going out of my way to step on an extra crunchy looking leaf has become a new hobby of mine. The only downside is its brevity. But while it lasts, the picturesque view from my windows is most uplifting during these arduous assessment-filled weeks.</p>
</div>