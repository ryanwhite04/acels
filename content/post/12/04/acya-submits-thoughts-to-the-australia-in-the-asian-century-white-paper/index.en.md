{
  "title": "ACYA Submits thoughts to the Australia in the Asian Century White Paper",
  "author": "ACYA",
  "date": "2012-04-10T21:28:34+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<p>ACYA has written and submitted its thoughts on Australia in the Asian Century to be considered for inclusion by the Australian Government in its upcoming white paper.</p>
<p> </p>
<p><a href="../PDF/2011_ACYD_White_Paper_Submission_part_1_of_3.pdf">Read the Submission here</a></p>
</div>