{
  "title": "Rudd in damage control as media storm over Sydney bashing of Chinese students escalates",
  "author": "ACYA",
  "date": "2012-04-25T07:58:00+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><img src="images/krudd.jpg" border="0" alt="" style="display: block; margin-left: auto; margin-right: auto;" /></p>
<p> </p>
<p>Former foreign affairs minister Kevin Rudd has started damage control to head off a potential diplomatic row with China after a media storm over an alleged attack on two Chinese students in Sydney.</p>
<div style="overflow: hidden; color: #000000; background-color: #ffffff; text-align: left; text-decoration: none;"><a href="http://www.smh.com.au/national/rudd-in-damage-control-as-media-storm-over-sydney-bashing-of-chinese-students-escalates-20120425-1xl4h.html#ixzz1t2QJjmqI">Read more </a></div>
<p>{jcomments on}</p>