{
  "title": "Only in China! - Scott Bulman",
  "author": "ACYA",
  "date": "2012-04-11T19:09:33+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Only in China!</h2>
<p class="author">Scott Bulman - April 2011</p>
<p>Today, during my Intellectual Property class on copyright law infringement and enforcement, my lecturer's computer was playing up. When the screen froze and the Professor was forced to reboot, he apologised to the class for the delay and cursed his pirated copy of Windows XP!</p>
<p>Walking home after class through the gate of my University, I noticed that a small, police outpost shelter had been newly erected. The small 3x3m wooden structure now provides the perfect shelter from the sun for the ladies who stand out the front of the East gate and sell fake graduation certificates, tax receipts and personal IDs!</p>
<p>After changing in to shorts and runners at home, I decided to head to the gym, where I was greeted by the distinct smell of KFC wafting in from the personal trainer's office!</p>
<p>On the way home from the gym, I decided to stop by the local supermarket to pick up a few groceries. The entrance to the pedestrian street however was blocked a black Porshce 4WD. The driver had managed to wedge his car in between two sings which clearly labelled stated "Pedestrian Street. No Entry!"</p>
</div>