{
  "title": "Gold Coast - Josh Marc",
  "author": "ACYA",
  "date": "2012-04-11T19:19:30+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Gold Coast</h2>
<p class="author">Josh Marc - March 2011</p>
<p>For those who are new to the Gold Coast (GC) and as such unaware of its natural beauty and many contradictions, this blog can hopefully act to clarify and also provide some information as to life outside of the main cities of Australia.</p>
<p>The GC is situated in the southeast corner of Queensland, it is our sixth most populous city in Australia as well as becoming just recently the most populous non-capital city (overtaking Newcastle). GC is home to many natural beauties making it a prized tourist destination and an interesting place for locals. The combination of being nestled between picturesque surfing beaches and the rainforest filled hinterland provides locals and tourists alike with a backdrop to remember. Although it probably should be said that such a combination is not uncommon along the east coast of Australia.</p>
<p>The geographical wonders of this city are a significant reason as to why people end up living here, furthermore the GC has several universities, one such being Griffith University, where I currently study Medicine. The campus is located quite central and is just a short drive away from the famous beaches. This access to the natural resources is a valuable medium for many students (and people alike) as it provides a perfect opportunity to de-stress from the days events. This more relaxed lifestyle is an important part of the Australian coastal psyche, and has resulted what has been termed in Australia as a "sea change". That being the move from the long hours and gruelling schedules of city life to a more lifestyle focused existence on the coast. The attraction of such a lifestyle is evidenced by the large move and subsequent increases in population of coastal towns right down the east coast. The "call of the ocean" for many is something rather innate in us and a sense of calm is experienced when at the beach. I understand that for many Chinese people this concept of being 'drawn' is unfathomable as our beaches are very different. However if the opportunity presents itself to go to the beach whilst you are here in Australia I would unequivocally recommend going, just remember to swim between the flags.</p>
<p>As such having this ultimate source at my doorstep is something special and much appreciated at the end of the day. The balance between work and life is something easily achieved on the coast.</p>
</div>
<p> </p>