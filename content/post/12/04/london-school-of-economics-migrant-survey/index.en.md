{
  "title": "London School of Economics Migrant Survey",
  "author": "ACYA",
  "date": "2012-04-10T21:30:19+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<p><span style="font-family: arial,sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.918);">The London School of Economics and Political Science is conducting a major global survey of migrants. If you moved to Australia from China after 1989 and have been here for more than 5 years (even if you are a student and may not continue to live in Australia)</span><strong style="font-family: arial,sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.918);">, </strong><span style="font-family: arial,sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.918);">you can contribute to this survey - </span><a href="https://www.survey.bris.ac.uk/lsewebsite/bric_migrants" target="_blank" style="color: #1155cc; font-family: arial,sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.918);">https://www.survey.bris.ac.uk/lsewebsite/bric_migrants</a><span style="font-family: arial,sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.918);">. Please share the link with friends who meet the criteria!</span></p>
</div>
<p> </p>