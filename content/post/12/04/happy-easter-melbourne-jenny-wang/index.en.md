{
  "title": "Happy Easter, Melbourne! - 王骁 (Jenny Wang)",
  "author": "ACYA",
  "date": "2012-04-11T19:24:58+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Happy Easter, Melbourne!</h2>
<p class="author">王骁 (Jenny Wang) - April 2011</p>
<p>最近放假了，复活节，一周假期。</p>
<p>基督教会组织了一次晚宴 to address Jesus' death for us. 大家都一身中东人的扮相，模拟当时Jesus 带领众人从埃及沙漠迁徙的场景，模拟食物，模拟心情。然后观看了电影 "The Passion of the Christ", 相信看过的人都被震撼过。Jesus 在酷刑的折磨下，仍像一只silent lamb, 没有丝毫的呻吟和怨言，只是默默地承受着一切。</p>
<p>Easter break, 街上却没有想象中的拥挤。超市里陈列着各式各样的Easter eggs, 五颜六色，大大小小。‘罪恶'的巧克力就这样又一次侵入了我们的生活。无法抗拒的诱惑！</p>
<p>还记得几年前在寄宿家庭过Easter 的事情。 他们有三个小孩子仍在相信Easter传说的年纪。Easter有个Easter Bunny, 就好像 Christmas 有个Santa Clause. 这只大兔子会在晚上小朋友们都睡着的时候出来；如果这个小朋友是个好孩子，这只兔子就会跳来把巧克力蛋留在他的枕边。所以当这个小朋友醒来发现枕边有 Easter egg时，他会非常开心，因为他知道Easter Bunny loves him/her and he/ she is a good kid. 当然这是传说，但是为了保留孩子们的那份憧憬和天真，有心的爸爸妈妈们便就扮演起了Easter Bunny的角色。深夜，小孩子都睡了，寄宿家庭妈妈拿出面粉和白天精心设计好的兔子脚丫形状的剪纸，放在地上。她把面粉洒在中间镂空的剪纸上，这样，地 上出现了一个兔子脚丫的形状，一个一个的隔着一定的距离，从门口排到每个小孩子的房间，还在每个人的枕边放了一堆的巧克力蛋。</p>
<p>很感动这份良苦用心，想想他们已经这样坚持了好几年，而且还要坚持下去好几年，但是他们很开心，因为第二天早上这些小孩子们的喜悦，笑脸，尖叫会将整栋房子填满。</p>
<p>大家都期盼着放假，但是这个假期却被复习和assignments 挤爆。拿起电话，打给朋友，所有人都在忙，晕头转向，丝毫没有在享受假期。 But yeah well, Happy Easter anyway!</p>
</div>