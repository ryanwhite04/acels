{
  "title": "The Postgraduate Certificate of Laws - Cao Yuanshan (曹远山) ",
  "author": "ACYA",
  "date": "2012-04-11T19:21:17+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<h2>The Postgraduate Certificate of Laws</h2>
<p class="author">Cao Yuanshan (曹远山) - February 2011</p>
<p>Having grown up in Sydney since migrating from Shenzhen in 1997, I began to tire of the same surroundings of the harbour city. Don't get me wrong, Sydney is probably the most livable city in the world, with splendid weather, awesome beaches and good looking people, but towards the end of my LLB degree I was increasingly drawn to the blossoming Middle Kingdom. At the end of 2008, I jumped at the opportunity to do an internship at a UK law firm in Hong Kong and I followed this up by completing a local internship in Sydney in January 2009. These forays provided a good contrast and I noticed that the work in HK was much grander in scale and more international in flavour. Further, I sensed the deals in HK were only going to get bigger as China continues to rocket ahead.</p>
<p>Therefore, when the HK firm offered me a training contract I jumped aboard without a second thought. Then 2010 rolled around and as I packed my bags for HK, a sense of trepidation hit me as I realised that I will be leaving my friends and network behind and I will be journeying to a completely foreign land and have to build up my social base from scratch. Nevertheless, this was overcome by the excitement of knowing that I will be getting amongst it in Greater China where unprecedented modernisation, growth and transformation are taking place and where sky truly is the limit.</p>
<p>But before I can sink my teeth into all the mega deals, I have to overcome this little obstacle called the Postgraduate Certificate of Laws (PCLL). It is a mandatory course which is followed by a 2 year training period after which one can become a locally admitted lawyer. The NSW equivalent is the PLT.</p>
<p>Only 3 universities in HK offer the PCLL: the University of Hong Kong, the Chinese University of HK and the City University of HK. It is an intensive course taught face-to-face over 9 months. Yes, it means unlike the NSW PLT you have to attend classes in person up to 5 days a week for 9 months.</p>
<p>I'm currently doing mine at the Chinese University and have 2 more months remaining on the calendar. I have found the course to be a drag more than anything else. You do learn some practical things and get to draft various documents, but I believe these skills can be better honed by on-the-job training so the PCLL should be shortened considerably.</p>
<p>Where you do your PCLL is completely up to you. The Chinese University and City University are seen as the slightly easier options as HKU is renowned for its torturous first semester and inhumane exam times. However, if you manage to secure a training contract before starting the PCLL, your firm will usually cover the expenses and won’t really care which provider you choose.</p>
<p>Just a word of warning if you intend to do the PCLL at the Chinese University. The course is taught at the graduate law centre in the Bank of America Tower in Admiralty on HK Island (close to the HSBC main building). That means if you’re a foreigner like me you have two choices when it comes to accommodation. You can rent a flat on HK Island in cheaper districts like Sai Wan (West of Sheung Wan) or on the Kowloon side around Austin. Alternatively, during the application process you can apply for postgraduate accommodation at the university’s main campus in Shatin, which was the option I adopted. Now this might sound attractive at first, but you’ll soon discover that your face will be plastered against the train door every morning as you join the morning rush to get to HK Island. The travelling time is around one hour in each direction and you have to tough it out for 9 months. The only saving grace for me is the cheap rent at HK$2300 per month.</p>
<p>So only 2 more months to go, I can’t wait!</p>