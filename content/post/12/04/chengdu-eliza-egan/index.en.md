{
  "title": "Chengdu - Eliza Egan",
  "author": "ACYA",
  "date": "2012-04-11T19:17:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Chengdu</h2>
<p class="author">Eliza Egan - March 2011</p>
<p>Chengdu (成都). Ever heard of it before? Perhaps you have; it <em>is</em> the capital of Sichuan province. Or perhaps (if you fancy yourself a philontherianist) you know it for its pandas. Or perhaps (if you consider yourself a bit of a chef), you know Chengdu for the deliciously, addictively spicy food. Whichever category you fall into, I must confess that before coming to Chengdu, I fell into my own unique niche: 'none of the above'.</p>
<p>Instead, I was (actually still am, now I think of it) a 20 year old, Melburnian who commenced my gap year in January. I closed my University books for a year to go off and experience the language I am studying, and challenge myself/my perception of the world. So, rather than finding myself being <em>taught</em> here in Chengdu, I am in fact <em>teaching</em>. I am <strong>the teacher</strong>. An English teacher at a high school, to be exact.</p>
<p>But I am putting the proverbial cart before the horse—I should introduce myself at least a little. My name is Eliza, however, the distressing difficulty of dipthongs means that I’m now colloquially known as 'Ali', or 爱丽. Hence my Chinese name means 'love and beautiful'. It is genuinely refreshing to have such a flattering and favourable name ('Eliza' means 'eternal light', I believe). Unfortunately, the occurrence of Earth Hour last weekend made me realise that 'eternal light' isn’t overly avant-garde these days... so 'Ali' it shall happily be!</p>
<p>And so, each week I teach 900 students aged about 15. Each weekend I find myself exploring more of this fascinating city, encountering the most peculiar of experiences, and incrementally (but by no means detrimentally) falling in love with this country. I never cease to be entertained by its everyday quirks. Case example: The 'interesting' English sentences found on perhaps the most mundane of products. Last week's winner was your standard notebook professing to be 'the most comfortable notebook you have ever run into. You will feel like writing with it all the time'. Um... reality check: hand-eye coordination may not be my strength; I will happily concede that, but I can proudly profess that I have never run into a notebook before. But hey, if the literary genius behind such pearls of wisdom ran into the said notebook and encountered such erudite inspiration, look, I'm not hate it because I ain't it!</p>
<p>Each month I hope to provide a kaleidoscope into my time here in Chengdu - it really is an amazing place. In the meantime, however, I hope this introduction suffices in telling a little about the lifestyle of an expat Chengduian!</p>
</div>