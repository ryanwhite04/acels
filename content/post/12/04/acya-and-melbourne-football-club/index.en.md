{
  "title": "ACYA and Melbourne Football Club",
  "author": "ACYA",
  "date": "2012-04-10T21:34:03+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
New and exciting adventures continue to roll on for ACYA – RMIT Chapter. Joining forces with the University of Melbourne Chapter, members were given the opportunity to attend a unique trip to the hallowed turf of Australian Rules football, the Melbourne Cricket Ground; made possible by the extremely generous support of the Melbourne Demons Football Club and their Partnerships Manager, Tom Parker.

The day began with the gift of official scarves from the Demons and tickets to the National Sports Museum. There, the local students introduced the basics of the oval-balled sport to Chinese international friends, as well as sampled on sport simulation drills such as goal kicking, archery, and cycling.

The time neared as places were taken on the stands to prepare for the Demons’ Round 1 opening clash with the Brisbane Lions. Braving the chilly conditions, and with hot dog and pies never too far away, the spectators witnessed a emotion driven match, all the while our international friends coming to terms with the Football lingo such as a mark, speckie and smother.

Unfortunately, the Demons could not come home with a win – going down to the Lions by 41 points. Nevertheless, it was an enjoyable and enriching experience to share the Australian sporting culture to overseas visitors and we all have a Melbourne memento to keep us warm in winter!

By Kevin Lay