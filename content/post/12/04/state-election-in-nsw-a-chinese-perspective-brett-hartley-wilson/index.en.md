{
  "title": "State Election in NSW: A Chinese perspective - Brett Hartley-Wilson",
  "author": "ACYA",
  "date": "2012-04-11T19:31:27+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>State Election in NSW: A Chinese perspective</h2>
<p class="author">Brett Hartley-Wilson - March 2011</p>
<p><img src="../Blog/2011march/sydney.jpg" border="0" alt="NSW election" width="250px" style="float: right;" /></p>
<p>With the New South Wales State election imminent and the almost certain demise of the long-serving Labor Government many Chinese will be taking a step back and reflecting on the role of their section of the community within the political debate and why is their representation comparatively smaller than other ethnic demographics.</p>
<p>With cultural demographics represented across both federal and state electorates it is surprising that such a large, robust and successful community which has integrated seamlessly into mainstream multi-cultural Australia would have such a disproportionally small representation. When thinking about this issue as a whole there is main question to be asked: do Chinese need candidates nominated straight out of their community in order to feel fully represented?</p>
<p>Do Chinese needs candidates that are taken from their community to feel fully represented? Historically in Chinese dense electorates around Sydney including: Strathfield (18.63%), Oatley (15.06%) and Ryde (15.05%) candidates are from both sides for the lower house have never had Chinese heritage, however this has not stopped the Chinese community swinging behind a candidate like the ALP's Virginia Judge in 2003, Chinese voters, appear to be attracted to party who can best articulate their desire for better services and take a hardline against racism; regardless of their racial heritage. An impromptu poll I conducted of Chinese youth studying at the University of New South Wales of twenty students better public services and discrimination which the two main issues that they would be voting on. When I asked the same twenty students did they feel excluded from the political process seven admitted that they found it difficult to actively engage in politics; when I asked whether they believed that Chinese candidates would be better able to represent them twelve out of twenty responded suggested they would. So can it be inferred that especially among Chinese youth that there is a desire for greater Chinese representation? The short answer must be yes, but there are seemingly deeper underlying issues about that 'access' that the Chinese community has to the broader political process.</p>
<p>Politicians, both state and federal need to more than simply 'cultivate' the Chinese community the way a scientist would cultivate cells within a Petri dish; a community that contributes so much to our society deserves its own unique representation, but it's not just up to our incumbent leaders to bring Chinese further into our political process, it's also about young Chinese making their voices heard and be willing to challenge themselves to be strong voices for the things that they care about, much like Vietnamese candidate Dai Le in Cabramatta, the ageless party machines of both liberal and Labor were built to maintain the status quo, the only way it can be altered is by yelling loud enough.</p>
</div>