{
  "title": "Partner with us ",
  "author": "ACYA",
  "date": "2012-04-10T17:20:37+10:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<h2>Sponsor Us</h2>
<h3>How can you sponsor ACYA?</h3>
<p>ACYA’s current sponsorship policy is based on two levels of support: Gold and Silver Sponsors.</p>
<p>Gold Sponsorship is AUD $10,000 for 12 months. The benefits of Gold Sponsorship include:</p>
<ul>
<li><span>Gold Sponsors will have an exclusive marketing presence on the main page of the website (Silver Sponsors will not);</span></li>
<li><span>ACYA will limit Gold Sponsors to one company/institution from each industry;</span></li>
<li><span>Gold Sponsors’ logos will be present in all marketing materials that are ACYA related (Silver Sponsors will not);</span></li>
<li><span>ACYA will be able to assist in forwarding CVs of suitable candidates for internship/graduate opportunities;</span></li>
<li><span>ACYA will be able to assist in organising on-campus events at our respective chapters; and</span></li>
<li><span>Further tailored co-operation if beneficial for your company/organisation.</span></li>
</ul>
<p>If your company / organisation were to sponsor ACYA as a Silver Sponsor, the cost would be AUD $4,000 for 12 months. The benefits of Silver Sponsorship include:</p>
<ul>
<li><span>ACYA will have a dedicated section in our website that your company/organisation could sponsor;</span></li>
<li><span>ACYA will be able to assist in forwarding CVs of suitable candidates for internship/graduate opportunities; and</span></li>
<li><span>ACYA Silver Sponsors will be part of a rotating ‘In Focus’ Silver Sponsor review on the main page.</span></li>
</ul>
<p>If you have any questions regarding sponsorship, please do not hesitate to contact us: Henry.Makeham@acya.org.au</p>
</div>