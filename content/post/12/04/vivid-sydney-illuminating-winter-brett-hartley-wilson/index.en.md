{
  "title": "Vivid Sydney: Illuminating Winter - Brett Hartley-Wilson ",
  "author": "ACYA",
  "date": "2012-04-11T19:34:31+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<p class="author">Brett Hartley-Wilson - May 2011</p>
<p>With nothing distinctly Chinese on at this time of year, perhaps besides the wonderful 'G'Day China' forum that was presented as part of the Sydney Writer's Festival I feel that it might be a opportune moment to remind the ACYA community in Sydney about the 'Vivid Sydney' festival; the largest music, lights and ideas festival in the southern hemisphere.</p>
<p><img src="../Blog/2011may/sydney.jpg" border="0" alt="Vivid Sydney" width="600px" /></p>
<p>The rather grandiose intention set out by the festival organisers is to "celebrate Sydney as the creative hub of the Asia-Pacific", a claim that may well be disputed by Hong Kong, Beijing, Tokyo or Los Angeles (no Melbourne, not you), but be that beside the point this two-week long gala-event is a great opportunity for young Chinese to step and engage with their city in a whole new way. There is literally a smorgasbord of events to satisfy even the most uncultured Mingong with the glittery focal point being the collage of images projected on and around Sydney's most iconic architecture, in particular the Opera House. If you're in the mood to wind down after a uni exam perhaps, then, until the 13th of June there is no better way to shake off the end of semester blues than heading down to Circular Quay or The Rocks and soaking up the canvas of colour and grabbing a bite to eat.</p>
<p>Of course the festival is not limited to the colour and light there are so many other things going on. For those who love music there is a lot to be excited about with acts like: Cut Copy, Architecture in Helsinki, The Cure, Tame Impala as well as legendary Saxophonist Sonny Rollins. Outside of music there are several creative design forums, a Vivid exhibition at the Museum of Contemporary Art as well as several productions being held within the colour-splashed sails of the Opera House, and don't worry if you're on a tight budget because there are over 10 free events offered throughout the Vivid Festival.</p>
<p>It is important for the Chinese youth community to get involved in and support these citywide events, only through participating is it possible to get to know the place you live and get involved in the wider community. Sydney offers up several different festivals throughout the year including: Crave Sydney, Sydney Film Festival, Sydney Writer's Festival, Chinese New Year Celebrations, the Sydney Festival as well as the Sydney Comedy Festival. So take the time this May and June to get involved in Vivid Sydney and get to know your city's soul.</p>
</div>