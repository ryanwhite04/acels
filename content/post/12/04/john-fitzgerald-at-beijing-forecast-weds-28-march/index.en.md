{
  "title": "John Fitzgerald at Beijing Forecast - Weds. 28 March",
  "author": "ACYA",
  "date": "2012-04-10T21:27:20+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div id="contentpage" class="contentpage">
<p><span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;">John Fitzgerald, Director of the Ford Foundation in China (former professor of Chinese history and author of "Big White Lie"), will discuss the work of the Ford Foundation and the challenges facing domestic and foreign philanthropy in China at the BEIJING FORECAST, Wednesday 28 March.</span><br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;" /> <br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;" /> <span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;">Hosted by China Policy, the cost of 60rmb includes excellent refreshments.</span><br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;" /> <br style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;" /> <span style="color: #000000; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; font-size: 11px; line-height: 14px; text-align: left;">RSVP to tom.williams@acya.org.au by Monday, March 26</span></p>
</div>