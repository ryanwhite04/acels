{
  "title": "New Year, New Wish",
  "author": "ACYA",
  "date": "2012-07-29T13:52:01+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="Section0">
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">大三，新的一年，新的一学期正式开始了。</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">总觉得自己一个人时间久了，心态会发生偏离，信心会有所减少，所以上个学期考试一结束就坐上了回国的航班。回去感受家庭的温暖，让长时间恐惧的心充实安全感。</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">回国</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">经历了一些，看到国内社会的变化，感受到国内的人文氛围，学了开车，交了朋友，很开心，学到了不少，觉得得到了自己想要的，心态回到了自己所期待的状态。</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">心</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">态不同了</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">不再那么急于求成</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">开始脚踏实地的努力</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">很珍惜各种机会</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">锻炼自己</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">表现自己</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">认真的学习</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">认真的做事</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">认真的交朋友</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">.</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'Calibri';">&nbsp;</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">而且每每走在墨大的校园里，抬起</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">头</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">看到宏伟高大的古建筑</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">想到上课的情景</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">就会提醒自己要更加珍惜</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">.</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">大三也忙碌了，可能前两年也着急，但是没有真正面</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">临就业问题的时候，一切的着急都不那么生动。学习，打工，开始自己尝试着做生意，就业申请，维系朋友，没有什么是简单的，但是因为困难，才更有挑战的必要，更有提升的空间。</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">编辑部，救济贫困中国学生的组织，合唱团，舞蹈团</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">,</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">各种志愿者活动，都积极参加，为的是不虚度年华，不让回忆只是在一片空白；也为证明自己的存在，更为一个更好的自己。</span></p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">&nbsp;</p>
<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;"><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MS Gothic';">告</span><span style="mso-spacerun: 'yes'; font-size: 11.0000pt; font-family: 'MingLiU';">诉自己要坚持，无论如何要坚持，努力让自己乐观持续积极灿烂的开放。</span></p>
</div>