{
  "title": "The ACYA Group",
  "author": "ACYA",
  "date": "2012-07-31T08:29:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div><span style="font-family: arial,helvetica,sans-serif; color: #808080; font-size: 12pt;"><strong>&nbsp;</strong></span></div>
<div><span style="font-family: arial,helvetica,sans-serif; color: #808080; font-size: 12pt;"><strong>Australia-China Youth Association Group (ACYA Group)</strong></span></div>
<div><span style="font-family: arial,helvetica,sans-serif; color: #808080; font-size: 12pt;"><strong>&nbsp;</strong></span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">The ACYA Group’s mission is to inspire Australians and Chinese to work together in realising their vision of a more prosperous, sustainable and interconnected world. By providing meaningful and frequent engagement between young Australians and Chinese, empowering motivated young people to successfully implement new initiatives, and engaging established leaders in a range of fields from both countries, the ACYA Group promotes greater cooperation and engagement between Australia and China and empowers young people who share our vision. ACYA Group consists of four initiatives: Australia-China Youth Association (ACYA); Australia-China Youth Dialogue (ACYD); Australia-China Young Professionals Initiative (ACYPI); and Engaging China Project (ECP).</span></div>
<p>&nbsp;</p>
<hr class="dotted" />
<h1 class="sponsor-img"><strong>&nbsp;</strong></h1>
<div class="sponsor-img"><a href="http://www.acya.org.au/"><img style="margin-right: 10px; float: left;" src="http://acyd.org.au/wp-content/uploads/2012/05/acya-125x125.png" height="125" width="125" /></a><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt; color: #808080;"><strong>Australia-China Youth Association (ACYA)</strong></span></div>
<div class="sponsor-img"><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt; color: #808080;"><strong>&nbsp;</strong></span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">The Australia-China Youth Association focuses chiefly on young people between 18 and 24. ACYA’s broad mandate is to promote greater engagement between young Chinese and young Australians and is one of four initiatives under the ‘ACYA Group’ umbrella.</span></div>
<div>&nbsp;</div>
<div><span style="font-family: arial,helvetica,sans-serif;">ACYA operates around three pillars – careers, education and people-to-people exchange. It is headed by the ACYA National Executive which helps co-ordinate ACYA’s website, social media presence, numerous publications, as well as relations with interested partners and sponsors. ACYA’s chapter network sits underneath the ACYA National Executive, with regional chapters in Shanghai and Beijing, as well as eight separate university chapters in Australia, namely: the University of Queensland, Macquarie University, the University of Sydney, the University of New South Wales, the Australian National University, the University of Melbourne, the Royal Melbourne Institute of Technology and the University of Western Australia. It is an extensive and enviable network of young people interested in Australia and China. The chapters host a wide variety of events, including language classes, sport days, BBQs, lecture series, dinners, karaoke sessions, beach trips and many other activities designed to promote genuine friendships across cultural divides. Each chapter holds a weekly ‘Australia-China Exchange Space’ event (or variation thereof), as well as other major events throughout the semester. Between them, the chapters host about 30-40 events throughout the year.</span></div>
<div>&nbsp;</div>
<div><span style="font-family: arial,helvetica,sans-serif;">Both ACYA National and the chapter network continue to actively promote Australia-China relations at a grassroots level through a variety of events and through a verity of mediums.</span></div>
<p>&nbsp;</p>
<hr class="dotted" />
<h1 class="sponsor-img"><strong>&nbsp;</strong></h1>
<p><img style="margin: 0px 10px 0px 0px; float: left;" alt="ACYD-125x125" src="images/ACYD-125x125.jpg" height="125" width="125" /><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt; color: #808080;"><strong>Australia-China Youth Dialogue (ACYD)</strong></span></p>
<p>The ACYD is a youth-driven bilateral week-long conference. It is held annually, alternating between Australia and China. The ACYD aims to promote sophisticated cross-cultural understanding between emerging leaders from Australia and China between 18 and 35 years of age. 15 Australian and 15 Chinese delegates are brought together with established experts and leaders from government, business, university and NGOs. The dialogue challenges delegates by exposing them to ideas and issues from a wide range of fields and disciplines; no delegate has a background in every area covered during the dialogue. Despite the rigorous curriculum, ACYD is a youth driven event; fun is never too far away!</p>
<p>Modelled on the Australian American Leadership Dialogue, the ACYD was conceived in response to an article written by Professor Stephen FitzGerald, Australia’s first Ambassador to China. The article drew attention to the need for more institutionalised dialogue between Australia and China, through both government and non-government organisations. There is no other youth bilateral platform for regular, extensive discussion of issues of importance between Australia and China.</p>
<p>The inaugural ACYD was held in Beijing and Shanghai in 2010. The second ACYD was held in Canberra and Sydney in 2011. Speakers at the 2011 ACYD included Bob Hawke, Craig Emerson, Michael Kirby, Dennis Richardson, Martin Parkinson, Peter Drysdale, Clinton Dines and Nicola Wakefield Evans. The delegates of the 2011 ACYD made a well-received submission to the Australian in the Asian Century White Paper, establishing a new feature for future Dialogues: a delegate produced report on issues of bilateral importance to Australia and China.</p>
<div>&nbsp; </div>
<hr />
<p>&nbsp;</p>
<h1 class="sponsor-img"><a href="http://www.acypi.org.au/"><img style="margin-right: 10px; float: left;" src="http://acyd.org.au/wp-content/uploads/2012/06/acypi-125x125.png" height="125" width="125" /></a><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt; color: #808080;"><strong>Australia-China Youth Professionals Initiative (ACYPI)</strong></span></h1>
<div class="sponsor-img"><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt;"><strong>&nbsp;</strong></span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">The Australia-China Young Professionals Initiative, an initiative of the Australia-China Youth Association, is the premier platform for young professionals in Australia and China to engage with the most significant issues of the bilateral relationship. ACYPI holds events in Melbourne, Canberra, Sydney, Shanghai and Beijing where young professionals aged 22 – 35 can develop a deeper understanding of the issues, opportunities and challenges facing Australia and China. Through holding such events, ACYPI aims to provide young professionals, entrepreneurs and academics with the opportunity to:</span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">1. Engage with the key issues of the Sino-Australian relationship</span><br /><span style="font-family: arial,helvetica,sans-serif;"> 2. Gain insight from leaders in business, government and academia of both countries</span><br /><span style="font-family: arial,helvetica,sans-serif;"> 3. Connect with peers from various industries all with a common interest in Australia-China relations</span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">&nbsp;</span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">Since being established in December 2010, thousands of young professionals have attended ACYPI events in both countries, with speakers including (amongst many others):</span></div>
<div>
<ul>
<li><span style="font-family: arial,helvetica,sans-serif;">Ms Frances Adamson, Australian Ambassador to China</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Prof Peter Drysdale, intellectual architect of APEC</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Mr Graham Hodges, Deputy CEO, ANZ</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Mr Clinton Dines, Former CEO (China), BHP Billiton</span></li>
<li><span style="font-family: arial,helvetica,sans-serif;">Ms Heather Ridout, RBA Board Member</span></li>
</ul>
</div>
<div><span style="font-family: arial,helvetica,sans-serif;">We look forward to welcoming you to one of our events in the near future. Should you have any further questions about ACYPI, please don’t hesitate to contact us<span style="font-size: 10pt;"><a title="Contact Us" href="http://www.acypi.org.au/contact/" target="_blank">&nbsp;</a></span></span><span style="font-size: 10pt;"><a href="http://www.acypi.org.au/contact/">here</a></span><span style="font-family: arial,helvetica,sans-serif;">.</span></div>
<p>&nbsp;</p>
<hr class="dotted" />
<h1 class="sponsor-img"><strong>&nbsp;</strong></h1>
<div class="sponsor-img"><a href="http://www.engagingchinaproject.org.au/"><img style="margin-right: 10px; float: left;" src="http://acyd.org.au/wp-content/uploads/2012/05/ecp-125x125.png" height="125" width="125" /></a><span style="font-family: arial,helvetica,sans-serif; font-size: 12pt; background-color: #ffffff; color: #808080;"><strong>Engaging China Project</strong></span></div>
<div class="sponsor-img"><strong>&nbsp;</strong></div>
<div><span style="font-family: arial,helvetica,sans-serif;">The Engaging China Project is a nation-wide China—literacy initiative designed to ignite the idea of China in the hearts and minds of Australian high school students aged 15-18. We deliver face-to-face workshops in high schools for students, teachers, parents and principals as well as providing an online China-literacy resource for young people. Our Engaging China Project Ambassadors are young people with a China story to tell and are passionate about boosting the next generation of China-literate Australians by imparting their experiences. Our team are largely Generation Y and drawn from&nbsp;ACYA&nbsp;and the&nbsp;ACYPI, have all lived, worked, travelled and/or studied in China, are Mandarin-speakers and have advanced skills in China engagement.&nbsp;&nbsp;As China becomes a part of daily life in Australia, our team is working hard to ensure the pathway to China-literacy is supported, encouraged and most importantly inspirational and fun for the most internationally and digitally connected people on the planet, Generation Z. If you would like to get involved with this project as an Ambassador or school, please go to&nbsp;<span style="font-size: 10pt;"><a href="http://www.engagingchinaproject.org.au">www.engagingchinaproject.org.au</a></span>&nbsp;and register your interest!</span></div>
<div><span style="font-family: arial,helvetica,sans-serif;">&nbsp;</span></div>