{
  "title": "Taiwonderful - Camping and Craziness During the Taiwan Treasure Map Program",
  "author": "ACYA",
  "date": "2015-08-27T02:55:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "FotorCreated",
      "type": "image",
      "src": "FotorCreated.jpg"
    },
    {
      "title": "IMG_3970",
      "type": "image",
      "src": "IMG_3970.jpg"
    }
  ]
}
<i></i>The School of Forestry and Resource Conservation at National Taiwan University has for the past few years run the "Taiwan Nature Treasure Map Program” (TNTMP) for international students. The program provides a paid internship opportunity for international students to stay in a selected site for one month, to explore the beauty of Taiwan from a ‘cultural and natural perspective’.
<p style="text-align: justify;"><i>Chloe Demspey was lucky enough to be selected as a participant on the program this year. Chloe is currently pursuing a double degree in Law and Arts, and says she was interested in using her Mandarin (having finished her Diploma last year) and getting a deeper understanding of the ‘controversial island’ while on the program.</i></p>
<p style="text-align: justify;"><i>ACYA National Education Officer Declan Fry had the chance to catch up with Chloe, a fellow Perth-resident and adventure-lover, to talk about her time spent camping in gorges, starting a blog, and sweating…a lot…during her Taiwan trip.</i></p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/08/FotorCreated.jpg"><img class="aligncenter size-large wp-image-22532" src="http://www.acya.org.au/wp-content/uploads/2015/08/FotorCreated-1024x768.jpg" alt="FotorCreated" width="466" height="350" /></a>
<p style="text-align: center;"><strong> How did you hear about the 2015 Taiwan Nature Treasure Map Program? </strong></p>
<p style="text-align: justify;">Having liked the ACYA National Facebook page, I saw an advertisement for the TNTMP a few days before applications were due and decided to throw my hat in the ring. I always figure that there’s no harm or commitment in applying for something you think you might be interested in.</p>
<p style="text-align: center;"><strong>Where did you spend most of your internship? </strong></p>
<p style="text-align: justify;">I was placed at the Taipingshan (太平山) Internship site. Taipingshan is a huge national park that was a historic logging centre, especially during Japanese rule. I actually lived and spent most of my time in Luodong, the nearest city to the national park.</p>
<p style="text-align: center;"><strong>What background are most of the participants in the program from?</strong></p>
<p style="text-align: justify;">We were all from very diverse backgrounds. I had no idea what to expect. I was surprised that there were actually forestry students who had come from as far as the USA just to do the program. I never even knew there was such a course of study as ‘forestry’! There was really no overriding similarity tying together the majority of participants; there were people from the Philippines, France, Denmark, Malaysia, USA, Canada, Taiwan, and, of course, Australia!</p>
<p style="text-align: center;"><strong>The program is described as helping international students explore the beauty of Taiwan from a cultural and natural perspective. What was the most beautiful aspect of Taiwan you discovered? </strong></p>
<p style="text-align: justify;">Taiwan is a wonderful destination for students; aside from its beauty and uniqueness, it’s incredibly accessible, and cheap. English is also widely spoken.</p>
<p style="text-align: justify;">Culturally and socially Taiwan has a very distinct personality, influenced by its unique history and inhabitants. The university ensured that each internship site had at least one domestic Taiwanese student, and, culturally, this was probably the aspect I most valued. Being able to make a real Taiwanese friend and live with them was fantastic for someone like myself who usually has a million questions when in a foreign culture. Taiwanese people are renowned for being friendly, helpful and good fun, which I certainly found to be the case. As I often find when travelling, it was the people and their stories that I found most important to my cultural experience.</p>
<p style="text-align: justify;">Nature-wise, I have to be a typical tourist and say I fell in love with Taroko Gorge. Where else can you actually love camping without a mattress on hard stony ground? After a long day of trekking above fast-running clear water framed by quartz cliffs, it was like sleeping on clouds. Closer to Taipei I loved the hikes through Yangmingshan National Park (陽明山國家公園).</p>
<p style="text-align: center;"><strong>Your blog (<a href="http://www.taiwonderful.wordpress.com">taiwonderful.wordpress.com</a>) gives a sense of some of the less romantic aspects of travel – sleeplessness, discomfort, and the like – what was the most uncomfortable aspect of your trip? </strong></p>
<p style="text-align: justify;">Overwhelmingly Taiwan is a very comfortable travel destination, my most uncomfortable experience was definitely self-inflicted; camping without a mattress on hard concrete outside a visitor’s centre because we arrived in the middle of the night and were lost. This predicament was exacerbated by the fact I didn’t bring any pyjamas and was camping in a public place with big windows into our tent and so had to cover my body in the plastic fly tent in 40°C heat. I sweated so much that night!</p>
<p style="text-align: center;"><strong>Interns are expected to create a treasure map highlighting something special/valuable they found in the internship site. What was the idea behind this, and what sort of treasure map did you create? </strong></p>
<p style="text-align: justify;">The idea behind the treasure map was for it to be created as a resource to encourage other young travellers to venture out into Taiwan’s more remote areas, which I love. Our treasure map combined the forestry and technical knowledge of my French-Mexican intern buddy, and our archaeology-expert Taiwanese counterpart intern. Needless to say, my addition was generally along the lines of, “this place has lots of those cute little birds that have the funny beaks” or, “this path had many trees of the green and spiky variety”.</p>
<p style="text-align: center;"><strong>What have you learnt from the experience that you can apply back home?</strong></p>
<p style="text-align: justify;">A true appreciation and some partially greater understanding of nature is definitely something I will really value, especially since while I was over there I read a particularly illuminating book on climate change (This Changes Everything by Naomi Klein). Taiwan’s incredibly unique and bio diverse environment is a perfect example of how complex and precious nature is. I also feel that as somebody very involved in the Australia-China space, seeing Taiwan through the lens of my own experience really deepens my understanding of the region.</p>
<p style="text-align: center;"><strong>What advice do you have for others looking for educational opportunities in the Australia-China space? </strong></p>
<p style="text-align: justify;">Apply, apply, apply! There are plenty of opportunities out there, but you do have to put effort into finding them and actually applying. I hear people so often say that such and such an opportunity doesn’t exactly suit them because they have study/work/life commitments. Everyone has commitments, but you have to be flexible and believe that you’re good enough for people/organisations to be flexible for you. Apply now, negotiate later! Stay involved with organisations like ACYA and keep your eyes peeled on pages like <a href="http://www.asiaoptions.org">Asia Options</a> and <a href="http://youngopportunities.org">Young Opportunities Australia</a> so when something comes up you feel connected enough to be a good candidate.</p>
More information about the program can be viewed <a href="http://entwtreasuremap.weebly.com/about-the-program.html">here. </a>