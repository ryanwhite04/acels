{
  "title": "ACYA-MTC Scholarship Open for 2015-16",
  "author": "ACYA",
  "date": "2015-08-24T12:52:16+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "The MTC Scholarship Program",
      "type": "application",
      "src": "The-MTC-Scholarship-Program-1.pdf"
    },
    {
      "title": "Application Form",
      "type": "application",
      "src": "Application-Materials-and-Info_2015-161.pdf"
    },
    {
      "title": "ACYA-MTC Front Page",
      "type": "image",
      "src": "ACYA-MTC-Front-Page.png"
    }
  ]
}
ACYA is offering 3 scholarships to study at the National Taiwan Normal University’s Mandarin Training Centre (NTNU MTC) during their Winter Term (Dec 1, 2015 – Feb 26, 2016).

This is a fantastic opportunity for ACYA members to continue their Chinese language studies in one of Taiwan’s most prestigious educational institutions. MTC has drawn participants from around the world, including former Australian Prime Minister Kevin Rudd.

In the spirit of ACYA, studying in Taiwan will allow students unique insight into the cultural ethos of a vibrant and diverse community, as well as the opportunity to forge cross-cultural connections and contribute to the Australia-Sino relationship.

For ACYA UQ and Taipei member Jack Fisher, studying at Taiwan’s oldest and best-known Chinese language institute was a fantastic experience: “The teachers were extremely supportive and friendly. The activities organized by MTC each added something different to my learning experience – whether it be learning to row a Dragon Boat or learning how to cook Stinky Tofu… My time at MTC was nothing short of life-changing.”

The scholarship is for one semester with a waived tuition fee and will be awarded to serious students of excellence to promote the study of Mandarin Chinese. Successful applicants should also demonstrate an interest in Aus-Sino relations.

<b>To Apply:</b>
Please send the application form, a copy of your resume and a cover letter that outlines how you meet the above criteria and how the program will benefit you to education@acya.org.au before <b>Saturday October 10th</b>. If you are part of an ACYA chapter, please state the Chapter in which you are a member.
You can download the application form here: <a href="http://www.acya.org.au/wp-content/uploads/2015/08/Application-Materials-and-Info_2015-161.pdf">Application Form</a>

The 7 items required for the application are:
(1) One Letter of Recommendation from a Chinese instructor (Must use the MTC Scholarship Reference Form)
(2) MTC application form
(3) An original copy of the student’s most recent university transcript
(4) Financial Statement showing at least US$2,500 within the past 3 months (original only)
(5) Passport information page (photocopy acceptable)
(6) MTC Survey of student’s language background
(7) A comprehensive Study Plan, students should submit a study plan essay between 300-600 words (ie. Students should elaborate on why they are interested in studying Mandarin Chinese, why have they chosen Taiwan, how long they plan on studying, and what they hope to gain from studying at the MTC.)

<b>About the MTC:</b>
The Mandarin Training Center (MTC), established in 1956, is affiliated with National Taiwan Normal University (NTNU) and is Taiwan’s oldest and best-known Chinese language institute. In cooperation with the Graduate Institute of Teaching Chinese as a Foreign/Second Language, the Graduate Institute of International Sinology Studies, the Department of Applied Chinese Languages and Literature, and the Department of Chinese Language and Culture for International Students at NTNU, the MTC is recognised as one of the world’s leading research and teaching institutions. For more information, download the <a href="http://www.acya.org.au/wp-content/uploads/2014/07/acya-mtc-info.pdf">MTC brochure</a>, visit http://www.mtc.ntnu.edu.tw or their <a href="https://www.facebook.com/mtc.ntnu?fref=ts">Facebook page</a>.