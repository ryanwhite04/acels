{
  "title": "A One Day Insight into the Chinese E-Commerce Industry",
  "author": "ACYA",
  "date": "2015-08-16T13:41:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "China ecommerce event Austrade July 2015 MLC Building Sydney - Australian Government © 2015",
      "type": "image",
      "src": "06X0377.jpg"
    }
  ]
}
<p style="text-align: justify;">The growing importance of China’s emerging middle class and their purchasing power is as remarkable as the meteoric rise of China’s economy since the turn of the millennium. According to forecasts from ANZ, 326 million middle class will emerge in China’s urban areas from 2014 to 2030. That’s roughly 14 times the current Australian population!</p>
<p style="text-align: justify;">But what does this mean for Australia?</p>
<p style="text-align: justify;">This growing middle class has subsequently created an opportunity for Australian businesses to supply that demand. Shifting consumer sentiments towards health and organic food products in China has emerged as consumers are paying more attention than ever to health products, fuelled partly by food safety concerns. By 2015 China’s health food market is expected to grow by $70 billion, more than tripling from $20 billion in 2010, with the global prices of nuts (such as cashews and almonds) at an all-time high driven by Chinese imports.</p>
So just how have Australian businesses been reaching into the Chinese market and its consumers?
<p style="text-align: justify;">The answer is online channels. On the 1st of July I was lucky to be given the chance to volunteer at an e-commerce seminar hosted by the Australian Trade Commission (Austrade) at their office in Martin Place, Sydney. This event involved representatives from influential Chinese e-commerce platforms such as Alibaba, JD Worldwide, Yihaodian and VIP.com presenting their insights to Australian SMEs entering the Chinese e-commerce industry. Recognise any of these companies? They’re China’s largest online retailers. As explained by Maggie Zhou, a director from Alibaba, “E-commerce is not just a business model in China anymore, it’s a lifestyle”.</p>
<p style="text-align: justify;">Indeed, the Chinese e-commerce industry is currently the largest in the world, surpassing America in 2013. The Chinese e-commerce industry has seen explosive growth with a compound annual growth rate of over 100% between 2003-2011. The seminar saw all arrays of Australian manufacturers and exporters in attendance, competing to gain a position for their products in the Chinese market through the e-commerce platform. Products pitched to the e-commerce companies ranged from high-end organic berry sauces to potato chips. There was a turnout of over 200 participants representing Australian SMEs on the day.</p>
<p style="text-align: justify;">On the day I helped set up the venue and was on hand to ensure the event ran smoothly. Additionally, I had the pleasure of personally working with Mr Chris Guo, the Vice-President of Yihaodian in managing the Yihaodian stand and attending to prospective clients. This wasn’t just an event volunteering experience, I also had the chance to network with both Australian and Chinese business representatives and the expertise they shared was equally invaluable. Seeing the business relationship between Australia and China in action was a fascinating experience for me. Instead of just numbers and figures, as they portray on the news, I saw everyday Australian businesses with their own products. Products that they believed might just become a breakthrough in the Chinese market. One particular example that stands out in my memory is a man representing his family-run business, kindly offering a sample of potato chips packages for me and Mr Guo to take home to try.</p>
<p style="text-align: justify;">It has most definitely been an inspiring experience for me in more ways than one, and I encourage all readers to get involved!</p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/08/06X0377.jpg"><img class="aligncenter size-large wp-image-22489" src="http://www.acya.org.au/wp-content/uploads/2015/08/06X0377-1024x683.jpg" alt="China ecommerce event Austrade July 2015 MLC Building Sydney - Australian Government © 2015" width="450" height="299" /></a>

<hr />

<em id="Andy">
Written by Andy Choy. Andy is a final year student studying Bachelor Commerce/Arts at UNSW, majoring in Finance and Chinese Studies. He is a committee member of the UNSW ACYA chapter and holds a strong interest in international business.
</em>