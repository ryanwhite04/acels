{
  "title": "In conversation with Heidi Han, Chinese Online Content Producer at the SBS",
  "author": "ACYA",
  "date": "2015-08-28T12:03:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Heidi Han",
      "type": "image",
      "src": "Heidi-Han.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Heidi-Han.jpg" rel="attachment wp-att-23933"><img class="size-medium wp-image-23933 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Heidi-Han-211x300.jpg" alt="Heidi Han" width="211" height="300" /></a>

This month, we spoke to Heidi Han, the Chinese Online Content Producer at Special Broadcasting Service (SBS). SBS is Australia’s public broadcasting network, which has a focus on reflecting the multicultural communities in Australian society. Heidi answers our questions about her work and gives us some of her valuable career insights.

<strong>What’s a typical day of work for you?</strong>
I check what’s happening around the world, I go to the morning bulletin that my radio colleagues cover - the Mandarin program runs from 7am-9am. I go back to see if there’s any news or developments I can produce into a news article to publish online and share on social media. I look after the Mandarin and Cantonese website and their social media channels on Weibo and Facebook.

I was studying accounting before. But I like to write articles in Chinese, so before graduating, some of my articles were read by the director of a local Chinese media publication. They needed someone to cover their community news, casually and on-call. So that’s how I started. I went to events on behalf of them, I wrote articles, I did interviews, and I wrote food reviews. So after I got permanent residence, they offered me a full-time job - and I accepted it. But I think it took me about two years to really decide that I wouldn’t go back to accounting. Letting it go would be a huge change for me, just because I’d be giving up a professional career. Writing for local Chinese media had lower job security, it didn’t pay well. So I did struggle to make a decision.

<strong>What actually made you decide to make this bold move?</strong>
I think getting to know myself, getting to know the world, what interests myself and what interests the people. Even if it wasn’t a big role, it wasn’t a big media organisation, but still somehow I did feel a responsibility to report what’s in the news. Also, just knowing that people rely on my media to know what’s happening in Australia. You also try to find a way to talk to the people in the community, and because you work in the media you get the chance to reflect their view to other stakeholders. The job really opened my eyes; I was a fresh graduate, I was from a totally different culture and language background. Working for local community media allowed me to stand at the forefront of the community and talk to the people who mattered.

<strong>What tips do you have for someone who wants to get their foot in the door of a professional field (not necessarily media)?</strong>
I think if you’re really passionate about something, really want to gain experience in something, really want to see the inside of the industry, don’t worry too much about what you get from a short-term internship or casual job. Always look at the bigger picture, look a couple of years ahead. When you look back, you’ll realise that everything you do now, you have learned from, you won’t have done it for nothing. Only if you devote yourself to a certain thing, you will be able to get a return from your efforts. But you don’t work for the return in the first place.

As a student, understand your advantages and understand your disadvantages. But one thing that’s for sure is you’ve got to be out there. You’ve got to be working on something; you can’t be sitting at home waiting for opportunities. You never know what will happen and what it might link to.

<strong>What advice would you give to students and young professionals who want to embark on a career in the Australia-China space?</strong>
I wouldn’t see myself as very successful career-wise. But one thing I believe I did right was to link my career to the increasing importance of China. That was something I believed back then: that the relationship between Australia and China could only be stronger and better than before. Knowing Chinese will open up more opportunities in all different industries. Don’t put all your eggs in one basket but try to use your background and language skills wisely.

<hr />

This interview first appeared in the August 2015 issue of AustraliaBites. Read the original <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=f5d8d0cc06">here</a>.

&nbsp;