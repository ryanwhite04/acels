{
  "title": "ACELS Sydney 2015 wraps up",
  "author": "ACYA",
  "date": "2015-08-27T16:39:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA ACELS",
      "type": "image",
      "src": "ACYA-ACELS-FP.png"
    },
    {
      "title": "ACYA thanks 2015 Sponsors",
      "type": "image",
      "src": "G7914040AA.jpg"
    }
  ]
}
Over 4 days between July 30th and August 2nd 2015, 80 of the brightest young minds from across Australia and China congregated in Sydney for the annual Australia China Emerging Leader’s Summit (ACELS) hosted by the Australia-China Youth Association (ACYA). This summit continued on the success of the Bilateral Youth Leadership Workshop held in 2014.

As one of ACYA’s key events for youth development, ACELS delegates participated in panel and networking events to further their understanding of the Australia-China relationship. Guests on the panel included Professor Kerry Brown, Director of the China Studies Centre at the University of Sydney, Mr Clinton Dines, former President of BHP Billiton China, Mr James Hudson, CEO of Australia China Business Council (NSW), Mr David Thomas, CEO of Think Global Consulting and Vice President of Australia China Business Council (NSW), and Mr Chris Carr, Senior Associate at King & Wood Mallesons. Discussions ranged from details of the recently signed Australia-China Free Trade Agreement to the nuances of corporate etiquette in the Australia-China space. Queensland University of Technology delegate Jackson Barton welcomed "the opportunity to engage with high level corporates and academics who were so engaged in the China space and the wider Asia-Pacific region" describing this experience as "invaluable". 

In addition to hearing from prominent members of the Australia-China space with first-hand experience, ACELS provided delegates with a “wonderful platform for cross-cultural communication with other emerging leaders”. Indeed, this networking opportunity was a common highpoint for many delegates of the summit.  Mark Gifford, a delegate from Ballarat felt that ACELS provided a platform to leverage the “unique skills and interests of delegates in areas such as cultural exchange and foreign language skills” for the purpose of strengthening the Sino-Australia relationship.  Katrina, a delegate from University of Technology, Sydney shared this sentiment and “enjoyed developing her networks in the Australia-China space” while Angela Lee, a delegate from the University of New South Wales mentioned that this was “the highlight of the weekend!”

Finally, upskilling workshops led by ACYA President - Jimmy Zeng, ACYA Managing Director - David Douglas and ACYA General Manager of Australia - James Castle, provided delegates with the necessary skills to transform their passion into real action after the summit. Chris White, a Bond University student relished the “opportunity to meet like-minded people” and was even "inspired to establish a new ACYA chapter at Bond University”. 

ACYA would like to acknowledge the invaluable support of our major partners, the Australia China Council, the China Studies Centre at the University of Sydney, Australia China Business Council and Wines by Geoff Hardy. In addition, a sincere thanks must go to all of our speakers who very generously shared their time to impart their wisdom and experience.

**ACYA is now inviting partners for ACELS 2016. Contact Ryan Cunningham, Business Development Director, at <business_development@acya.org.au> for more information!**

*With over 23 chapters across China and Australia collectively forming a 5000+ strong cross-border community, ACYA is the sole NGO at the forefront for connecting like-minded Chinese and Australian youths who aspire towards building stronger ties between both nations. ACYA seeks to promote long-lasting relationships without borders, and provide opportunities from both sides to learn and mutually benefit under this unique bilateralism.*

<a href="http://www.acya.org.au/wp-content/uploads/2015/08/G7914040AA.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/08/G7914040AA-1024x259.jpg" alt="ACYA thanks 2015 Sponsors" width="580" height="147" class="aligncenter size-large wp-image-22556" /></a>