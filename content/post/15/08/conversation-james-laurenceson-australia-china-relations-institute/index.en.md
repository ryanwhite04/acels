{
  "title": "In conversation with James Laurenceson, Deputy Director and Professor at the Australia-China Relations Institute",
  "author": "ACYA",
  "date": "2015-08-28T11:03:48+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "James Laurenceson",
      "type": "image",
      "src": "yB0Esaw8.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/yB0Esaw8.jpg" rel="attachment wp-att-23895"><img class="size-medium wp-image-23895 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/yB0Esaw8-300x300.jpg" alt="James Laurenceson" width="300" height="300" /></a>
This month, we spoke to James Laurenceson, Deputy Director and Professor of the Australia-China Relations Institute at the University of Technology. He previously held a long-term teaching position at the University of Queensland, as well as two overseas appointments at Shandong University and Shimonoseki City University. His research focuses exclusively on the Chinese economy and has been published in international, peer-reviewed journals including China Economic Review and China Economic Journal. In addition, James regularly comments on contemporary developments in the Chinese economy across various media outlets, including Australian Financial Review, The Conversation and Business Spectator.

<strong>I understand you obtained your PhD in Economics from the University of Queensland (UQ). When did you first become interested in the field of economics, and in particular Chinese economics?</strong>
I’ve been asked that question often. I look back to my high school economics assignments where, even then, I was writing about China. Once I began my undergraduate studies at university, I was drawn to any course that touched on China. My Honours thesis was on Chinese urban food consumption patterns and my PhD on financial reform in China, so it’s been a passion that I’ve carried all the way through my career. I suspect it’s because the Chinese economy is very different to Australia, which presents an attractive and interesting challenge for me.

Most economists are classified as a microeconomist, macroeconomist, econometrician or a modeller. I’ve always called myself a China Economy Specialist – someone who specialises in a country, rather than an area of economics.

<strong>You’re currently the Deputy Director and Professor at the Australia-China Relations Institute (ACRI). What initially brought you to this role?</strong>
What drew me to this role was the opportunity to specialise in China. At UQ, I taught a course on Chinese economy but I also taught general microeconomics and macroeconomics courses. I would be writing for academic journals, researching China’s economy and digging right into historical data. Now, my job consists of following daily news and engaging with key decision-makers in the Australia-China space, from companies to politicians and the media. The way I communicate is also very different. These groups don’t read academic journals; they read the Sydney Morning Herald, Australian Financial Review and The Australian. My articles are still informed by research but I’ve changed the way that they’re packaged.

<strong>How would you describe your work at ACRI?</strong>
Research and events are the two key arms of ACRI. In terms of research, ACRI comments in an academic capacity across all aspects of the Australia-China relationship – which is pretty broad! The Director of ACRI and former Foreign Minister is Professor Bob Car, and he handles anything to do with international politics, foreign policy and security. On the other hand, I look after issues in the economics space in the Australia-China relationship. Traditionally, this has mainly been focused on the resources sector, though this is now changing to span across services, tourism, education and foreign direct investments. My job is to be abreast of all these different aspects, as well as to provide commentary and analysis.

There’s also a very active events calendar at ACRI. For instance, we’ve been hosting a Prime Minister series where we speak to former Prime Ministers about their work in Australia-China relations. Last month, we featured <a href="http://www.australiachinarelations.org/content/prime-ministers-series-bob-hawke">Bob Hawke</a>. We also presented a <a href="http://newsroom.uts.edu.au/events/2015/03/china-correspondents-panel">China Correspondents Panel</a>, where former Australian media that were based in Beijing over different time periods discussed their experiences.

<strong>As someone who regularly comments in the media on the Australia-China relationship, you must be exposed to many public critics.</strong>
There is a small minority but the average member of the Australian public is not anti-China. They’re cautious. They place themselves in the middle ground – ACRI actually recently conducted a <a href="http://www.eastasiaforum.org/2015/05/27/what-australians-really-think-about-a-rising-china/">survey</a> on this topic in May. I think the reason is twofold.

First, China’s rise in Australia’s national consciousness has been pretty rapid. Ten years ago was when the resources boom was starting to build. That’s when the average Australian started becoming aware of China. Ten years is not a long time, given the exposure that we’ve had to other countries such as Japan and the US and UK.

As for the second reason, I think Xi Jinping said it very well when he gave his address to Federal Parliament and said “<a href="http://www.abc.net.au/news/2014-11-17/xi-peace/5897680">China’s the big guy in the crowd</a>”. The world is watching China and they’re just not sure how the big guy is going to react. China has the second largest economy in the world, the largest population and the third largest landmass. For these reasons, China is always going to promote a more cautious response than the average country.

<strong>What do you predict for the long-term future of China’s economy?</strong>
Fundamentally, I’m very positive about the future direction of China’s economic prospects. I don’t become caught up in any day-to-day gloom because in the long run, China still has many easy sources of productivity growth and a reform dividend that can be unlocked. There’s vast potential for catch-up, particularly as the majority of China still lags behind first tier cities such as Beijing, Shanghai and Guangzhou.

The main reason I am so optimistic about China is that ultimately, the Chinese government is incredibly pragmatic. They need to continuously improve living standards, which won’t happen if a status quo is maintained. There may be some short-term fluctuations or slowdowns, but long-term, I think that pragmatism will drive China’s economic reform and put it up to a higher income level than it is now.

<strong>Do you think Australia is doing enough to take advantage of where the Chinese economy is headed down the track?</strong>
I don’t think so and I do worry about that. China is by far one of our most important trading partners. We export double the amount to China than we do for our second largest customer Japan. Going forward, the majority of growth and potential in Australia’s trade is going to come from China. Unfortunately, we don’t put a lot of effort into this relationship. For instance, until one year ago, there was no ACRI. In fact, there was no think-tank in Australia devoted to the Australian-China relationship. When I was teaching at UQ, I started the first undergraduate course on Chinese economy in 2008 across all Australian universities. Given the real-world importance that China holds for Australia, I think we don’t put nearly enough emphasis on the Australia-China relationship, as we should.

<strong>What’s your advice to young people looking to pursue studies or a career into the Australia-China relationship?</strong>
My first piece of advice is to be prepared to back yourself. Sometimes you can spend too much time listening to all the noise that you read about China from people who may not know China very well at all. My second piece of advice would be to inject yourself into the Australia-China space and take up any opportunity to network. It’s not a large space; in fact it’s a pretty small circle. If you look at all the boards of the institutions that delve into China, from Asia Society to the China Studies Centre at the University of Sydney, you’ll see the same names popping up over and over again. Get to know those key decision-makers in the circle. Personally, I love to meet young Australians who are passionate about the Australia-China relationship because they’re great working partners. You can’t be in this field without being passionate.

<hr />

This interview was conducted by Olivia Gao. It appeared in the August 2015 issue of ChinaBites – read the original version <a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=1d5f6a684e">here</a>.

&nbsp;