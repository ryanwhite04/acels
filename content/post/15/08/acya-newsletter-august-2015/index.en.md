{
  "title": "<!--:en-->ACYA Newsletter August 2015<!--:-->",
  "author": "ACYA",
  "date": "2015-08-19T02:42:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA ACELS newsletter",
      "type": "image",
      "src": "ACYA-ACELS-newsletter.png"
    }
  ]
}
<!--:en--> In this edition ACYA kicks off semester two with the Australia China Emerging Leaders Summit!
<a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&id=1aa016a65f">Please click here to read ACYA Newsletter August 2015 Edition</a><!--:-->