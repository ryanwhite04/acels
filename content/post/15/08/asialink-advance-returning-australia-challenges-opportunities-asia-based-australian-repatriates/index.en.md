{
  "title": "Asialink and Advance - Returning to Australia – Challenges and Opportunities for Asia-based Australian Repatriates",
  "author": "ACYA",
  "date": "2015-08-05T04:47:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "11412168_809246015856859_1359157017355040023_n",
      "type": "image",
      "src": "11412168_809246015856859_1359157017355040023_n-e1438749922290.jpg"
    },
    {
      "title": "11412168_809246015856859_1359157017355040023_n",
      "type": "image",
      "src": "11412168_809246015856859_1359157017355040023_n1-e1438750042132.jpg"
    },
    {
      "title": "11412168_809246015856859_1359157017355040023_n",
      "type": "image",
      "src": "11412168_809246015856859_1359157017355040023_n2-e1438750289209.jpg"
    }
  ]
}
In recent years, there has been as many as 100,000 Australians working in Asia. This number has been steadily increasing, posing a series of new opportunities and challenges for our expatriates returning to Australia. Expatriates returning from China and the rest of Asia are well-equipped to improve the expertise of Australian businesses heavily reliant on Asia, aiding them with country-specific cultural, market and industry knowledge. 

Last month, ACYA's Victor Luo (UNSW Chapter President), Cynthia Yuan (Macquarie University Chapter President), Jerry Ding (National Treasurer) and Jani Song (UWA Chapter President) attended panel discussions around Australia hosted by Asialink and Advance discussing how Australian expatriates returning home from Asia can put their knowledge and experiences from the region to use and apply it to an Australian business context. 

At the 'Returning to Australia – Challenges and Opportunities for Asia-based Australian Repatriates' event, they heard from a panel of experts drawing from their own experiences of challenges faced and the role that expatriates can play in helping Australian businesses become more 'Asia-literate'. The panellists included Mr Doug Ferguson, Head of Asia Business Group & China Business Practice at KPMG, Ms Cathryn Carver, Senior Managing Director, International & Institutional Banking at ANZ and BlueScope Steel’s General Manager of Sales, Marketing, Research, International, Mr Jason Ellis. 

Victor related to the panellists as a young Australian professional having lived overseas for a long period, overcoming the various cultural and language barriers. He agreed that it was important for Australian businesses to explore the array of opportunities in Asia for long-term growth. 
 
This was echoed by Cynthia who also thought the panel's emphasis on the importance of raising Asia-capable leaders was inspiring for younger Australians. She took away that Australian organisations need to look more towards offshore experiences and have greater cross-cultural understanding to operate effectively as regional and international entities. 

Other ACYA executive members in attendance found that the panellists were well-experienced and gave valuable insights about the potential importance of Australia’s returning expatriates for helping other companies build bridges with Asia. The event also provided useful advice to those considering a career-path oriented towards Asia. 

Asialink and Advance, both Associate Partners of ACYA, regularly host events providing platforms for meaningful discussions in the Australia-China relationship.