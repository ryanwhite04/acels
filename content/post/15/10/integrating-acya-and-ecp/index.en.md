{
  "title": "Integrating ACYA and the Engaging China Project",
  "author": "ACYA",
  "date": "2015-10-03T12:44:20+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ECP",
      "type": "image",
      "src": "ECP.jpg"
    },
    {
      "title": "Merging ACYA and ECP",
      "type": "image",
      "src": "Merging-ACYA-and-ECP.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/09/ECP.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/09/ECP-1024x681.jpg" alt="ECP" width="561" height="374" class="aligncenter size-large wp-image-22582" /></a>
It is with great excitement that the ACYA National Executive announces the union of ACYA and the Engaging China Project (ECP), long considered to be a sister organisation in the Australia China youth bilateral space. For more information on the ECP, see their website <a href="http://www.engagingchinaproject.org.au/">here</a>

With this merger, itself a natural extension of the collaboration between both organisations’ volunteers over the years, administrative responsibility of the ECP will be passed entirely from the ECP National Director and state teams to the Education Portfolio within the current ACYA National Executive structure. ECP finances will be managed by the National Treasurer. In keeping with ACYA’s organisational mission, the National Executive envisages two key strategic benefits from this combination.

The first of these two benefits is that the provision of ACYA’s own resources, human capital and networks can now be deployed in order to further develop the ECP’s internal management processes, deliver on event ideas, and channel manpower to support ECP’s youth ambassadors in their mission of assisting secondary schools with their classroom engagement of contemporary China. In short, ECP will benefit from increased access to resources as it continues to demystify China and make it attractive in the eyes of students.

Secondly, ACYA’s direct management of ECP will provide immediate visibility to secondary school students of the wide scope for engagement in Australia-China youth affairs. Students will not only have their secondary school experience enriched through the shared experiences and stories of ECP ambassadors (many of whom are also ACYA members/past executives and young professionals), they will also be directly introduced to the online content and opportunities that are available to them upon graduation made possible by ACYA. 

The anticipated result of this combination is to provide a natural avenue of progression for young talent with a passion for China or Australia-China relations from secondary school, via the ECP, to university. There, they will be able to connect with one of ACYA’s 17 Australian Chapters, tapping into a diverse talent pool and network of associated opportunities. In doing so, the sustainability of ECP’s face-to-face workshops in classrooms will be enhanced by the ability for ambassadors to sell a much more comprehensive “Australia-China” value proposition to secondary school students.

The ACYA National Executive looks forward to seeing the fruits of this combined effort over the years, as we work to integrate ECP’s New South Wales and Western Australia operations into the Education portfolio. Upon successful integration and proof of concept via a strong track record from ECP’s volunteers, ACYA will then seek to expand ECP’s presence to other states in Australia and continue to develop ACYA into the first port of call for any Australians or Chinese interested in the Australia-China space. 

The Executive would also like to take this opportunity to thank the Australia China Council, the Australian National University Centre for China in the World, the Confucius Institute at the University of New South Wales and the Australia China Youth Professionals Initiative for their support of the Engaging China Project. Special thanks also to outgoing National Director Andrea Myles – Co-founder and Chief Executive Officer of the recently successful China Australia Millennial Project – for her efforts in developing the ECP since its inception and making it the fantastic organisation it is today.