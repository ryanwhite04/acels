{
  "title": "ACYA receives Australia-China Council grant for ACELS",
  "author": "ACYA",
  "date": "2015-10-23T00:47:06+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA ACC Media Release Front Page",
      "type": "image",
      "src": "ACYA-ACC-Media-Release-Front-Page.png"
    }
  ]
}
ACYA is both excited, and honoured to announce that it will receive a $25,000 grant from the Department of Foreign Affairs & Trade in the 2015-16 round of the Australia China Council Grants Program. 

Through their annual competitive grants program, the Australia-China Council supports projects across China, Hong Kong, Taiwan, Macau and Australia that broaden and strengthen Australia-China relations in education, business, culture and the arts.

The Australia-China Council grants are intended to provide seed funds for innovative proposals relevant to the objectives of the Australia-China Council’s Strategic Plan 2014-18. 

The funds received by ACYA will be used to support the costs associated with the Australia China Emerging Leaders’ Summit (ACELS).

The ACELS is one of ACYA’s flagship initiatives for youth development in the Australia China space. In 2015, ACELS, hosted at the prestigious University of Sydney, with the support of the Australia China Business Council, the University of Sydney China Studies Centre, and Wines by Geoff Hardy, summit delegates participated in an intensive schedule of forums, workshops, round-table sessions, a varied speaker program and networking sessions with prominent Australia-China stakeholders. 

The 2015 ACELS delegates were handpicked from a wide range of applicants, representing a selection of youth at the forefront of shaping Australia-China relations in the 21st century.

ACYA is excited to have the support of the Australia China Council in developing and watching the next generation of leaders emerge in this exciting Australia China space.

National President, Jimmy Zeng, acknowledged the support of the Australia China Council:

<i>“On behalf of ACYA’s membership, the hundreds of volunteers that make up ACYA’s National Executive, various Chapter Committee Executives, and ad hoc events and projects personnel, I would like to thank the Australia China Council for their generous financial support. This has enabled ACYA to deliver a high quality intensive development program with its partners that gives aspiring young talent access to networks, the opportunity to build their knowledge of the bilateral relationship, and come up with new ways to advance it. ACYA’s long standing partnership with the Australia China Council continues to be a cornerstone of the organisation’s success.”</i>