{
  "title": "Australia-China Youth Association – Australia-China Relations Institute  at the University of Technology Sydney Research Internship Program",
  "author": "ACYA",
  "date": "2015-10-30T11:56:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA ACRI",
      "type": "image",
      "src": "ACYA-ACRI.png"
    }
  ]
}
The Australia-China Youth Association (ACYA) is pleased to announce a partnership with the Australia-China Relations Institute (ACRI) at the University of Technology Sydney. The partnership will see ACYA and ACRI offer a co-branded Research Internship Program over the 2015-16 Australian summer period. 

The ACYA-ACRI Research Internship Program compliments a suite of existing joint research- and policy-based internship opportunities ACYA has procured for its diverse membership base of 5000 members. 

The Australia-China Relations Institute (ACRI) exists to illuminate the Australia-China relationship, with a particular focus on economics and business. It is Australia’s only think-tank devoted to the bilateral relationship. Its work is based on a positive and optimistic view of Australia-China relations. The Australia China Youth Association (ACYA) is the only youth-administered NGO actively focused on developing the future youth leaders of the Australia-China relationship. 

The joint ACYA-ACRI Research Internship Program seeks:

1.	To enable both Australian domestic students and Chinese international students to gain valuable, practical research experience based at ACRI at the University of Technology Sydney (UTS)

2.	To enable aspiring youth talent with a demonstrated academic track record in the field of economics and business to contribute to this area of Australia-China relationship in a meaningful, public way, and identify issues with the potential to shape bilateral affairs 

3.	As a result of the above, to further illuminate the Australia-China relationship to the general public and promote insights to inform policy development and bilateral business relationships

Interns in the ACYA-ACRI Research Internship Program will be provided with the opportunity to pursue an Australia-China focused Economics research project of their own initial design. This research will be pursued under the supervision of ACRI’s Deputy Director. Currently in this position is Professor James Laurenceson. Professor Laurenceson is a leading authority on the Chinese economy, having previously held appointments at the University of Queensland, Australia and Shandong University, China. He is also the immediate past president of the Chinese Economics Society of Australia (CESA) and his academic research has been published in leading journals including the China Economic Review. His opinion pieces have appeared in outlets such as Australian Financial Review and The Australian, and his commentary is regularly sought by the Australian and Chinese press. 

For details on the ACYA-ACRI Research Internship Program and how to apply visit: http://www.acya.org.au/education/scholarships/acri-internship/

For further information on ACRI visit: http://www.uts.edu.au/research-and-teaching/our-research/australia-china-relations-institute
