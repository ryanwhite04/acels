{
  "title": "Tu Youyou, Nobel Laureate: Traditional Insights, Modern Methods",
  "author": "ACYA",
  "date": "2015-10-13T05:14:41+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "handout_08oct15_fe_education26_tu_youyou",
      "type": "image",
      "src": "handout_08oct15_fe_education26_tu_youyou-e1444713179242.jpg"
    }
  ]
}
<p style="text-align: justify;">The first week of October is always an important one in China. The first day commemorates the first session of the Central People’s Government and the six days which follow make up <a href="http://www.travelchinaguide.com/intro/festival/national.htm">the nation’s ‘golden week’</a> (黄金周). This year’s ‘golden week’ has certainly lived up to its name because China’s first <a href="http://www.nobelprize.org/nobel_prizes/medicine/laureates/2015/press.html">Nobel Laureate for the Medical Sciences was announced by the Nobel Assembly in Sweden</a>. The winner was Tu Youyou (屠呦呦), who shared the award with William C. Campbell and Ōmura Satoshi.</p>
<p style="text-align: justify;">International awards and ceremonies like the <a href="http://www.theatlantic.com/notes/2015/10/the-significance-of-a-nobel-prize-for-a-chinese-scientist/409105/">Nobel Prize or Olympic Games are important</a> in the sense that they contribute to the development of a nation’s character. Hosting the Olympic Games demonstrates to the world that a nation can organise an event of the profoundest kind of complexity. Similarly, winning a Nobel Prize in the Sciences demonstrates that a nation can solve problems of the profoundest kind of complexity. China has now done both things and it is justifiably <a href="http://news.xinhuanet.com/english/2015-10/06/c_134686049.htm">proud of its most recent Laureate, Tu Youyou.</a></p>
<p style="text-align: justify;">The Xinhua News Agency, one part of China’s state-run media, has commented on the significance of <a href="http://news.xinhuanet.com/english/indepth/2015-10/06/c_134687366.htm">Tu’s achievement</a>: “Not only is Tu the first Chinese Nobel Laureate in natural science, she is the first Nobel Laureate who received all scientific training in China. The award she won made Chinese scientists gain tremendous confidence.” As China seeks to <a href="http://www.forbes.com/sites/jnylander/2015/09/14/chinas-investment-in-elite-universities-pays-off-new-ranking/">improve the performance of its universities in the world university rankings</a>, a Nobel Laureate in the sciences couldn’t have come at a better time.</p>
<p style="text-align: justify;">Tu won her share in the prize through rather unconventional means. In China, Tu is known as a <a href="http://en.people.cn/n/2015/1006/c90000-8958353.html">‘three-withouts-scientist’</a>. Tu has spent no time studying abroad, no membership with the Chinese Academy of Sciences and no doctorate. However Tu’s unconventional background may have contributed to her success. In the late 1960s, Western treatments like <a href="http://www.nobelprize.org/nobel_prizes/medicine/laureates/2015/press.html">chloroquine and quinine</a> were used a great deal, but to little avail. The rate of malaria was on the rise and it was hitting the poor the hardest. Meanwhile, the USA and North Vietnam were at war and soldiers were dying from malaria. North Vietnam were an important ally to China and it was with this context that Mao Zedong set up a military project named ‘Project 523’ to find a solution to malaria. Tu started reading ancient Chinese texts on Chinese medicine for insight. Artemisia annua looked like a plant that would have the properties needed to kill the malaria parasite.</p>
<p style="text-align: justify;">After screening artemisia annua’s effectiveness in treating animals infected with malaria, Tu became perplexed because her results were inconsistent. It was unclear as to which chemical of the plant actually killed malaria parasites. Tu revised the ancient Chinese texts again and found some insight. After a lot of work and research, Tu found that artemisinin was the chemical that needed isolating and it was to be found in artemisia annua.</p>
<p style="text-align: justify;">The texts Tu consulted suggested that artemisia annua was often soaked and boiled in ancient times. This was a profound insight. However, Tu realised that boiling Artemisia annua would likely destroy artemisinin, the chemical needed to kill malaria parasites. With this in mind, she decided to use an ether-based solvent to isolate artemisinin. This was a technique that came from Western science. Tu’s isolation of artemisinin combined insights from Traditional Chinese Medicine with methods from Western Science.</p>
<p style="text-align: justify;">Tu’s discovery of artemisinin came to be of profound importance for the world. <a href="http://www.nobelprize.org/nobel_prizes/medicine/laureates/2015/press.html">The Nobel Committee</a> puts it well by writing:</p>
<p style="text-align: justify;"><i>“Malaria infects close to 200 million individuals yearly. Artemisinin is used in all malaria-ridden parts of the world. When used in combination therapy, it is estimated to reduce mortality from by more than 20% overall and by more than 30% in children. For Africa alone, this means that more than 100 000 lives are saved each year.” </i></p>
<p style="text-align: justify;">Artemisinin not only symbolises a great deal of human lives saved but also demonstrates that some insights from Traditional Chinese Medicine can complement the scientific rigour of Western Medicine. In this sense, Tu’s Nobel Prize symbolises the finest kind of East-West collaboration.</p>


<hr />

<i>Written by Charlie Lyons Jones. Charlie is studying a Bachelor of Arts with majors in Chinese and East Asian Studies at the University of Melbourne. His main interests are Imperial Chinese history, modern Chinese intellectual history and the international politics of Northeast Asia</i>