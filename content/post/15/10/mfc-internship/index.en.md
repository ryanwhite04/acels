{
  "title": "Melbourne Football Club: The Role of Sport in Connecting Cultures",
  "author": "ACYA",
  "date": "2015-10-05T10:31:50+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "12048492_968591579869384_1959689480_n",
      "type": "image",
      "src": "12048492_968591579869384_1959689480_n.jpg"
    },
    {
      "title": "12092190_968591559869386_1411711817_n",
      "type": "image",
      "src": "12092190_968591559869386_1411711817_n.jpg"
    }
  ]
}
<p style="text-align: justify;"><i>Melbourne Football Club (MFC), in conjunction with the University of Melbourne have recently launched the <a href="http://www.melbournefc.com.au/engagingmelb">Engaging Melbourne Program</a>. This program aims to reach out to the international student community in Melbourne, especially Chinese students, by getting them involved in AFL. The project was launched during AFL’s Multicultural round on August 9th, which included a dragon parade at the MCG before the AFL match. Alexandra Meek from ACYA was fortunate enough to interview Cindy Runhui Hao, who is currently interning with MFC.</i></p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/10/12048492_968591579869384_1959689480_n.jpg"><img class="aligncenter size-full wp-image-22787" src="http://www.acya.org.au/wp-content/uploads/2015/10/12048492_968591579869384_1959689480_n.jpg" alt="12048492_968591579869384_1959689480_n" width="400" height="300" /></a>
<p style="text-align: center;"><b>Cindy, I understand you’ve been Melbourne based for a few years now.
Can you please tell me a bit more about yourself ?</b></p>
<p style="text-align: justify;">My name is Cindy Runhui Hao and my Chinese name is 郝润慧. I am from Hebei Province and I’ve lived in Melbourne for almost seven years. I came to Melbourne in February 2009 when I started my foundation studies at Trinity College (part of Melbourne University). I then completed the Bachelor of Commerce and a Masters in Finance, both at Melbourne University.</p>
<p style="text-align: justify;">I got my Permanent Residency in January 2015 and I am currently working full-time as a Business Development Associate at Zomato (formerly Urbanspoon). I am also interning at Melbourne Football Club and have been in this role since the start of the year.</p>
<p style="text-align: center;"><b>Can you please tell us more about the Engaging Melbourne Program.
What is the program hoping to achieve? </b></p>
<p style="text-align: justify;">The main objective of the Engaging Melbourne Program is to connect the Chinese community with the city of Melbourne, as most Chinese people are very isolated from the local Australian community. MFC believe footy is a good way to bring both cultures together. From my own experience, I also believe sports, music, and food are great channels to learn about another culture.</p>
<p style="text-align: justify;">The official launch event of the Engaging Melbourne Program was the game on 9th August, the AFL multicultural round. We incentivised the public by offering free footy tickets and tried to attract more attention and interest by organising a dragon parade before the game. This event was more successful than anticipated with over 100 attendees. We also have a series of <a href="http://www.melbournefc.com.au/engagingmelb">five other interesting events</a> planned for the coming months.</p>
<p style="text-align: justify;">Importantly, this is a long term program for MFC which will last about three years and also involves the Victorian Government and the City of Melbourne. Ultimately, MFC hope that the international community will be incentivised to attend footy games through the special events being run and in the longer term will not need these incentives and will attend more games. Through this they will be more connected to the community and will understand Aussie culture better. The idea of this program came from our Strategic Partnership Manager at MFC, Tom Parker. Tom understands Chinese culture and language very well. He also brought China Southern Airlines in as MFC’s second largest sponsor, which is amazing!</p>
<p style="text-align: center;"><b>Working full-time and interning with MFC must keep you very busy!
What has your role as an intern involved?</b></p>
<p style="text-align: justify;">The working hours are very flexible and everyone has the autonomy to manage their own time, it has a time commitment of 8 hours per week. Most of the interns are university students, and some work full-time.</p>
<p style="text-align: justify;">My tasks involve communicating with my Chinese networks in my spare time. Some of these are language training schools, immigration agencies and student clubs at Melbourne University. I conducted a few meetings with student club committee members to ensure they understand the program. I have also helped promote the events to these groups. I have also been able to use my experience as an international student to help shape the program. In addition, I have helped translate AFL rules.</p>
<p style="text-align: center;"><b>As former international student yourself, how has your involvement in this program enriched your experience living in Melbourne?</b></p>
<p style="text-align: justify;">I think that to truly understand a culture you need to immerse yourself amongst the local people, footy is a very good channel to do this. At first, I was given free tickets but now I have attended over five footy games (including the grand final) and have genuinely enjoyed it – I’m a Melbourne Demons fan now! Experiencing the atmosphere and passion of a footy game has been a great experience for me. Sport is something that makes people more positive and happy - I would encourage other international students to get involved.</p>
<p style="text-align: justify;">Working as an intern, I have built long term friendships with my team. We have the passion and enthusiasm to connect both cultures. I also learnt a lot about leadership from our manager Tom Parker. Being humble and down-to-earth is a valuable quality I learnt from Tom.</p>
<p style="text-align: justify;">In retrospect, I feel really grateful for being part of ACYA, as it has allowed me to meet so many people with similar passions and interests - I first learnt about this opportunity through a friend at ACYA. It is also great to meet so many Australian people who are interested in Chinese culture and language. For a project like this we need more like-minded people to contribute their time and energy. Students who are interested in the program and would like to get involved can find out more information <a href="http://www.melbournefc.com.au/engagingmelb">here</a>.</p>
<p style="text-align: justify;"><img class="aligncenter size-full wp-image-22788" src="http://www.acya.org.au/wp-content/uploads/2015/10/12092190_968591559869386_1411711817_n.jpg" alt="12092190_968591559869386_1411711817_n" width="300" height="400" /></p>