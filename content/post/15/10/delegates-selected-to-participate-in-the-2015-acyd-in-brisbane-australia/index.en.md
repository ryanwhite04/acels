{
  "title": "Delegates selected to participate in the 2015 ACYD in Brisbane, Australia",
  "author": "ACYA",
  "date": "2015-10-16T12:29:26+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "image",
      "type": "image",
      "src": "image.png"
    },
    {
      "title": "download",
      "type": "image",
      "src": "download-e1444998191991.png"
    },
    {
      "title": "tumblr_mn8b8sLRb61rkz363o1_1280",
      "type": "image",
      "src": "tumblr_mn8b8sLRb61rkz363o1_1280.jpg"
    },
    {
      "title": "download (1)",
      "type": "image",
      "src": "download-1.png"
    },
    {
      "title": "ACYD News",
      "type": "image",
      "src": "download-11.png"
    }
  ]
}
The Australia-China Youth Dialogue (ACYD) is delighted to announce today the delegates selected to participate in the 2015 ACYD to be held in Brisbane, Australia from 4-7 December.

From the nearly 250 applications, 30 delegates have been selected to participate in the Dialogue.

Full biographies of the selected delegates can be viewed <a href="http://acyd.org.au/delegates">here</a>.

Short listing delegates proved a difficult task for the selection committee as the overall candidate standard was extremely high.
Founder and Chairman Henry Makeham said ‘ACYD is delighted to announce the 2015 delegates who are once again the best and the brightest young leaders. These delegates are not only experts in their chosen fields, they also have a deep and passionate interest in the Australia-China relationship. This years’ delegate cohort includes an Olympic medalist, a Federal Member of Parliament, senior managers from ASX listed companies and leaders from Chinese think tanks and academic institutions’.

Delegates will participate in an intensive four day program with experts discussing issues pertinent to the Australia-China relationship including creative industries, trade and investment, entrepreneurship, tourism and security.  

Highlights of the program include a welcome lunch with Former Foreign Minister Professor Bob Carr, evening reception hosted by the Queensland Governor at Government House and at the gala dinner held at the Queensland Art Gallery of Modern Art.

Sijia Liu, a 2015 delegate with extensive experience within different United Nations agencies, said ‘I believe I can learn a lot about China-Australia relations and Asia-Pacific issues at ACYD, and not only will it hugely benefit my career, I also expect to meet lifelong friends.’

Technical Expert at Telstra and 2015 delegate, Ming Deng describes himself as ‘passionate about the Australia-China relationship.’

He says ‘The ACYD offers me a unique opportunity to deepen my understanding of Australia-China relations. It is the premium platform for me to meet, connect and collaborate with young leaders from both Australia and China.

This year’s delegates will join another one hundred and fifty outstanding ACYD delegate alumni who have, or are moving into, positions of influence and leadership within the Australia-China relationship.

Congratulations to the selected delegates. We wish them an enjoyable and productive Dialogue.

ACYD would like to thank its partners below for their ongoing support.

 
Media enquiries:
Olivia Bowden
Communications Manager
ACYD
Olivia.Bowden@acyd.org.au

<a href="http://www.acya.org.au/wp-content/uploads/2015/10/image.png"><img src="http://www.acya.org.au/wp-content/uploads/2015/10/image.png" alt="image" width="561" height="535" class="alignright size-full wp-image-22900" /></a>