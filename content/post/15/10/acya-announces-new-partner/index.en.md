{
  "title": "ACYA announces a new partner!",
  "author": "ACYA",
  "date": "2015-10-20T11:25:54+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA Geoff Hardy Front page",
      "type": "image",
      "src": "ACYA-Geoff-Hardy-Front-page.png"
    }
  ]
}
Last month, the Australia China Youth Association (ACYA) was delighted to announce its national wine sponsor- Wines by Geoff Hardy. ACYA’s chapters across Australian and Chinese cities will stand to benefit from this cooperation.

Wines by Geoff Hardy has three wineries based in South Australia and produces some of Adelaide’s most premium-quality wines. Their cellar door experience in McLaren Vale and Adelaide Hills has been consistently rated 5-star. Wines by Geoff Hardy hopes to be able to increase their presence from this partnership in both the domestic and Chinese markets with their expertise in producing the finest wines. 

ACYA’s first collaboration with Wines by Geoff Hardy was during the Australia-China Emerging Leaders Summit that was held in Sydney in August earlier this year. The organisers, as well as many attendees, have complimented the wine company’s contribution to have enhanced the successfulness of the event.

Like its collaboration with Wines by Geoff Hardy, ACYA is continuously seeking more mutually-beneficial and innovative partnerships with local and international organisations. ACYA is proud to support Wines by Geoff Hardy in its growing success internationally and at home as we look forward to continue working with them in the future.

See their website here: http://www.winesbygeoffhardy.com.au/
