{
  "title": "Interning in Beijing’s NGO Sector",
  "author": "ACYA",
  "date": "2015-10-08T03:42:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "12041839_10153168693683067_361299084_n",
      "type": "image",
      "src": "12041839_10153168693683067_361299084_n.jpg"
    },
    {
      "title": "12083645_10153168693593067_47675038_n",
      "type": "image",
      "src": "12083645_10153168693593067_47675038_n.jpg"
    }
  ]
}
<p style="text-align: justify;">I was fortunate to be accepted into <a href="http://www.crccasia.com">CRCC</a> Asia’s internship program during Beijing’s steamy hot summer. CRCC brings students, graduates and business people alike from around the world to China, facilitating an industry placement relevant to their work or study discipline. As a University of Sydney student undertaking a double major in Chinese and Government, I was looking for any opportunity to work on my fluency and to gain a glimpse into how business works in China. I was placed at <a href="http://chinadevelopmentbrief.cn/directory/yiyun-social-innovation-center/"> Yiyun Social Innovation Centre (溢云)</a>, a Beijing-based NGO. With support from <a href="http://bicentennial.westpacgroup.com.au/scholarships/asian-exchange/">Westpac’s Bicentennial Foundation for Asian Exchange</a>, I was able to participate in the program. CRCC also organised trips to the Forbidden City, acrobatic shows, Mandarin classes, community outreach activities and even a camping trip to the Great Wall.</p>
<p style="text-align: justify;"><a href="http://www.acya.org.au/wp-content/uploads/2015/10/12041839_10153168693683067_361299084_n.jpg"><img class="aligncenter size-full wp-image-22782" src="http://www.acya.org.au/wp-content/uploads/2015/10/12041839_10153168693683067_361299084_n.jpg" alt="12041839_10153168693683067_361299084_n" width="400" height="300" /></a></p>
<p style="text-align: justify;">Beijing is a mammoth city; it stretches far and wide with endless ring roads and a labyrinth of metro lines, and new stations popping up here and there. Yiyun is located on the north-west edge of the city, about an hour and a half commute one way, in a semi-detached house in a quiet neighbourhood. It was funny to see the confused look from the compound security guards every day as I strolled in; apparently it wasn’t common to find foreigners wandering about this area.</p>
<p style="text-align: justify;">Yiyun is a boutique NGO firm that specialises in the online dissemination of public welfare advertisements. It works to promote fundraising for social projects in China such as helping to find missing children, organising ride-sharing for people struggling to get home during the Spring Festival and raising awareness about marginalised groups like veteran soldiers and people with rare diseases. Collectively these advertisements have been viewed more than 1 billion times since Yiyun initiated this function.</p>
<p style="text-align: justify;">Yiyun has also built an online platform, Yitu (溢图) that enables users to create, upload and canvass areas lacking in basic needs. An example of its use is during earthquake relief, whereby users can create maps to better locate particular areas that need certain attention. Currently, one of the most highly viewed maps is a water safety map that identifies areas around China with reported incidents of water pollution, contamination or drainage issues.</p>
<p style="text-align: justify;">The variety of projects they were involved in was incredible and allowed me to experience many different aspects of NGO work. My responsibilities included translation, market research for potential new projects, reading and reviewing UN reports, and facilitating communication with other international NGOs in the hope of collaborating on projects.</p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/10/12083645_10153168693593067_47675038_n.jpg"><img class="aligncenter size-full wp-image-22783" src="http://www.acya.org.au/wp-content/uploads/2015/10/12083645_10153168693593067_47675038_n.jpg" alt="12083645_10153168693593067_47675038_n" width="400" height="300" /></a>
<p style="text-align: justify;">Everyone in the office was Chinese, so I had to work hard to converse and find out what was going on. The greatest thing about the company was the relaxed working environment; it was important to combine work and play. We had a ping pong table, cycling machines and the occasional movie at lunchtime. Working in a house meant that we cooked and ate lunch together everyday. I got to try a lot of new Chinese dishes and chat with my colleagues while they tried to teach me slang expressions in various Chinese accents. They even requested I cook ‘Western food’ one day so I had to rustle up a big pasta dish with a bacon and tomato sauce. With chopsticks, this was a challenge!</p>
<p style="text-align: justify;">As it was my first time being involved in an NGO and my first time working in China, it is hard to make a valid judgement on NGOs in general in China. As far as I was aware, Yiyun’s business operations were fairly efficient and they had managed to generate substantial partnership links with companies such as Baidu. Yiyun had also earned awards and donations from associations such as the United Nations Development Programme (UNDP). However it was apparent that “red tape” (quote in English from one of my colleagues) was still an issue and there remain obvious barriers for NGO autonomy in China.</p>
<p style="text-align: justify;">The month’s internship was an amazing opportunity to get a taste for Chinese life in terms of the practicalities of living and the idiosyncrasies of business. I have been really lucky to work in a fun, caring and fascinating NGO and I hope to stay for a longer stint working in China in the future.</p>


<hr />

<i>Cameron Hunter (胡空满) is a 3rd year Arts (Languages) student from the University of Sydney. He is currently on exchange at Peking University, supported by the Westpac Bicentennial Foundation.</i>