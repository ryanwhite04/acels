{
  "title": "Exploring Asia Capability: My Internship Experience with Asialink Business",
  "author": "ACYA",
  "date": "2015-09-07T12:28:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_2818",
      "type": "image",
      "src": "IMG_2818.jpg"
    }
  ]
}
<p style="text-align: justify;">While studying in China last year I had the opportunity to visit eBay’s China HQ in Shanghai. The office had a buzzing, innovative and exciting culture – an atmosphere that concealed eBay’s otherwise turbulent history in China. The American e-commerce giant exited China in 2006 after failing to match local firm Alibaba’s product Taobao, which utilises its superior knowledge of local consumer habits to strengthen its market strategy.</p>
<p style="text-align: justify;">EBay’s unsuccessful ventures (when I visited in 2014, the Chinese subsidiary was in its third incarnation) were a reminder that duplicating proven business models into new markets – ones with unique cultural and consumer environments – will not always result in replicated success.</p>
<p style="text-align: justify;">Asialink, an organisation charged with promoting Asia awareness within Australia, has long recognised this reality. In 2012 it launched a National Taskforce to develop an Asia capable workforce – what has now become Asialink Business. Its National Strategy Paper, ‘Developing an Asia Capable Workforce’, identified 11 capabilities that both individuals and organisations need in order to successfully enter and engage with Asian markets.</p>
<p style="text-align: justify;">From February this year I was privileged to intern at Asialink Business under the direction of Natalie Cope, Asialink’s Manager of Corporate Partnerships and Development. I had multiple opportunities to partake in meetings with corporate partners and see for myself the strategies major Australian firms are implementing to engage more meaningfully with Asia. Sitting in meetings also allowed me to see upfront the value that Asialink Business adds to its partners as a peak Asia-engagement body.</p>
<p style="text-align: justify;">Of course, homogenising “Asia” as a single vehicle for trade would be imprudent; markets in the region are highly unique, diverse and nuanced. Accordingly, Asialink Business develops and executes programs that seek to educate businesses about Asian market diversity. In the time I was interning in the Sydney office, Asialink ran targeted events on Japan, Indonesia, India and China. I had a role in assisting at many of these events which offered great exposure to the China and Asia-engaged professional community in Sydney.</p>
<p style="text-align: center;"><a href="http://www.acya.org.au/wp-content/uploads/2015/09/IMG_2818.jpg"><img class="aligncenter size-full wp-image-22629" src="http://www.acya.org.au/wp-content/uploads/2015/09/IMG_2818.jpg" alt="IMG_2818" width="466" height="350" /></a></p>
<p style="text-align: justify;">Of the many projects and initiatives I assisted with during the internship, Asialink’s Next Gen event series was perhaps the most relevant. Asialink Business, with partner Westpac Bicentennial Foundation, united three peak youth bodies from the China, Indonesia and India spaces for a conversation on opportunities for youth engagement in Asia. The events took place in Melbourne and Sydney and it was extremely rewarding to see this project from inception to execution.</p>
<p style="text-align: justify;">As a Government and International Relations student, it is readily apparent that Asia is strategically, economically and culturally important to Australia’s future. Yet broad acceptance of this fact has not yet been widely recognised – even in student and young professional circles. The opportunity to gain insight into what Asialink Business is doing to increase Asian awareness amongst younger generations was very rewarding.</p>
<p style="text-align: justify;">Another valuable component of the program was conducting research and compiling briefings based on the findings. One startling statistic I came across was that, according to a 2014 report by PwC, only 9% of Australian businesses operate in Asian markets. This is despite our country’s extensive trade ties and geographical proximity to the Asia-Pacific.</p>
Asialink works to address this deficit, and it is needed. With a sharp decline in Australia’s mining boom, services will be a big contributor to Australia’s trade with Asia in future decades, especially as Asia’s middle class widens. Yet, there is still an evident reluctance amongst Australian businesses to take the plunge and enter Asian markets.
<p style="text-align: justify;">EBay has since re-entered China with a revised business model that better reflects market demand. But its history serves as a reminder that understanding Asia is, and will be, critical to anyone seeking an international career in many sectors in the 21st century.</p>
<p style="text-align: justify;">In this regard, my internship with Asialink was an immensely valuable experience to have at the beginning of my career. Thanks to the generosity of Natalie and Peter in the Sydney office, and the wider Asialink Business team, my experience was professionally enriching, dynamic, and valuable to my career progression. I leave with a recognition that being “Asia Capable” is critical to my career heading into the future.</p>


<hr />

<i>
Written by Samuel Johnson. Sam is completing a BA in Government and International Relations at the University of Sydney. Between February and July 2015 he interned with Asialink Business. He currently serves as Treasurer at ACYA's University of Sydney Chapter.
</i>