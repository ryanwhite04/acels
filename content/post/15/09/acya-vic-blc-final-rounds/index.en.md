{
  "title": "ACYA VIC BLC Final Rounds",
  "author": "ACYA",
  "date": "2015-09-07T12:03:10+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA VIC BLC Front page",
      "type": "image",
      "src": "ACYA-VIC-BLC-Front-page.jpg"
    },
    {
      "title": "ACYA VIC BLC Chinese Poster",
      "type": "image",
      "src": "11994059_10207378453036903_1208027751_o.jpg"
    },
    {
      "title": "ACYA VIC BLC English Poster",
      "type": "image",
      "src": "12001757_10207378452996902_812568862_o.jpg"
    }
  ]
}
ACYA VIC Bilingual Language Competition's (BLC) inaugural state-wide English and Chinese bilingual language competition is drawing to an exciting close with our long-awaited Final Round. Finalists will be notified via email over the weekend of their final results.

Join us to witness some amazing bilingual student speeches, guest speeches and performances on the night.
We look forward to your attendance.

Tickets may be purchased here: http://tiny.cc/ACYAVICBLCFinals
ACYA has a number of FREE tickets to give out to our members. 

Please contact us in regards to this. 
For large groups and school groups, please send @ACYA VIC BLC a message.

Click attending here: https://www.facebook.com/events/879378732146736/

<a href="http://www.acya.org.au/wp-content/uploads/2015/09/12001757_10207378452996902_812568862_o.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/09/12001757_10207378452996902_812568862_o.jpg" alt="ACYA VIC BLC English Poster" width="300" height="424" class="alignleft size-full wp-image-22623" /></a>
<a href="http://www.acya.org.au/wp-content/uploads/2015/09/11994059_10207378453036903_1208027751_o.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/09/11994059_10207378453036903_1208027751_o.jpg" alt="ACYA VIC BLC Chinese Poster" width="300" height="424" class="alignleft size-full wp-image-22622" /></a>