{
  "title": "Announcing the THIRST internship",
  "author": "ACYA",
  "date": "2015-09-30T14:32:31+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "THIRST Internship",
      "type": "image",
      "src": "THIRST-Internship.jpg"
    }
  ]
}
Thirst is a water conservation and education NGO, which seeks to educate and raise awareness about the water crisis in schools and universities across China. An initiative of the World Economic Forum, Thirst educates students about ‘Virtual Water’, the hidden water consumed in the manufacturing of a product or good, and seeks to inform the next generation of global consumers to make ‘water-wise’ consumer choices and to more generally reduce their water consumption. Having reached over 100,000 students with the WE Water Experience presentation, and a further 20,000 students through large-scale activities and events, their dedicated team want to expand their operations. To do this, they need your support!

Interested in this internship opportunity? Go to  http://www.acya.org.au/thirst-internship/ to check out position details and to apply!

<b>Applications close 5pm AEST 14th October</b>