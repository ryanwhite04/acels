{
  "title": "In conversation with Andrew Hogg, Regional General Manager of Greater China at Tourism Australia",
  "author": "ACYA",
  "date": "2015-09-28T11:33:25+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Andrew Hogg",
      "type": "image",
      "src": "Andrew-Hogg.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Andrew-Hogg.jpg" rel="attachment wp-att-23900"><img class="size-full wp-image-23900 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Andrew-Hogg.jpg" alt="Andrew Hogg" width="250" height="250" /></a>

This month, we interviewed Andrew Hogg, who joined Tourism Australia in June as Regional General Manager Greater China, based in Shanghai. Andrew's career to date has included a number of senior roles in a career spanning more than 26 years with Qantas across finance, sales, marketing and operations.

<strong>What does your role as Regional General Manager for Greater China at Tourism Australia involve?</strong>
I'm responsible for driving Tourism Australia’s strategies to grow demand from Australia’s fastest growing tourism market for international visitors. China, in particular, has been identified as the single most important market for achieving the Australian tourism industry’s goals to grow the sector to up to $115 billion annually by the end of the decade as part of the Tourism 2020 strategy. The China market alone is currently worth $5.4 billion annually and is expected to be worth as much as $13 billion annually by 2020.

<strong>I understand you had been with Qantas for many years as General Manager for China. What drew you to your current position as Regional General Manager Greater China at Tourism Australia?</strong>
Having been with Qantas for many years and having the opportunity to live in many great locations has kept my interest in the tourism industry. With my new position as Regional General Manager Greater China for Tourism Australia I remain involved in this industry. The role provided me with the opportunity to remain in China and continue my involvement in the ever growing tourism industry.

<strong>What or who sparked your original interest in China?</strong>
There are many things that have interested me in China, the people and culture, the diversity in food and the ever changing and growing China. It is a wonderful place to live and work.

<strong>What do you believe are the greatest challenges facing Australian tourism in regards to attracting the China market?</strong>
Australia is a wonderful place to visit, see friends, study and work. Australia has a unique place in the world with its natural beauty, friendly people and world class tourism products. Our challenge is to increase the awareness of Australia and convince more Chinese to visit Australia.

<strong>How do you bring others on board with your vision and goals for Tourism Australia?</strong>
A vision must be shared with clear communication, common goals and enthusiasm.

<strong>What advice do you have for young professionals looking to grow their careers in China-Australia relations, that you wish you could have told yourself?</strong>
Language skills would have to be one of the most important things. I wish I had studied mandarin when I was a lot younger. Also a clear understanding of each country’s economy, people and culture. This will help you have a better understanding of the history of the relationship and also how to improve it.

<hr />

This interview was conducted by Olivia Gao for the September 2015 issue of ChinaBites. Read the original version <a href="https://www.google.com/url?hl=en-GB&amp;q=http://us1.campaign-archive2.com/?u%3Ddb2f0ea3a68f632de3088eada%26id%3D9aa594f282%26e%3D9cd1615cba&amp;source=gmail&amp;ust=1464521258817000&amp;usg=AFQjCNGxa52h8-KBQ2jBGWMXGeRczNSNKA">here</a>.

&nbsp;