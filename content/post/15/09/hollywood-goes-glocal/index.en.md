{
  "title": "Hollywood goes Glocal for China",
  "author": "ACYA",
  "date": "2015-09-24T01:50:14+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled2.png"
    },
    {
      "title": "",
      "type": "image",
      "src": "Untitled3.png"
    },
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled4.png"
    }
  ]
}
<i><strong>To tap into the 1.3 billion human goldmine that is the burgeoning Chinese market, Hollywood heavyweights have resorted to some amusing but tremendously desperate tactics. </strong></i>
<p style="text-align: justify;">“I’m from the future. You should go to China,” utters a time travelling assassin to his naïve younger self in the 2012 science fiction movie <i>Looper</i>. So the latter hesitantly heads to neo-Shanghai, where iconic landmarks such as The Bund and the Oriental Pearl Tower serve as backdrops to a passionate two minute montage of his successive years. <i>Towering stacks of Money. Bacchanalian parties. Fast cars. And a feisty new love with a Chinese girl.</i></p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled2.png"><img class="aligncenter size-full wp-image-22685" src="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled2.png" alt="Untitled" width="500" height="263" /></a>
<p style="text-align: justify;">The lesson there? The path to wealth, happiness and prosperity lies within the Middle Kingdom – and for Hollywood’s big guns hoping to guarantee the future of their own industry, this too is the path they must walk, for there is no bigger slice of the pie than that which lies across the Pacific.</p>
<p style="text-align: justify;">While US box office revenue has slid steadily in the last few years, China’s box office has shown jaw-dropping growth rates of 30% in 2013 and 34% in 2014. It has surpassed Japan to become the world’s second largest movie market, and the first market outside North America to exceed US$4 billion in revenue. In just five years, the Motion Picture Association of America predicts that the Chinese market will topple even the US, seriously shaking up global market demands.</p>


[caption id="attachment_22686" align="aligncenter" width="425"]<a href="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled3.png"><img class="size-full wp-image-22686" src="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled3.png" alt="Figure 1 : Source: Motion Picture Association of America" width="425" height="250" /></a> Figure 1: Source: Motion Picture Association of America[/caption]
<p style="text-align: justify;">While it is clear that more Chinese can now afford to splash cash at their local cinema, rapid infrastructural and technological developments in China’s domestic film industry have also been rocking Hollywood’s boat. For example, an average of nine new cinemas open in China every day. These facilities include huge IMAX 3D screens, perfect for those ubiquitous Marvel and DC blockbusters that are laying down a solid film trend for the next few years. In fact, the pace of global growth in digital 3D screens increased for the first time since 2007 and this is largely due to China and the Asia Pacific region as a whole, where the proportion of digital screens that are 3D now exceeds 70%.</p>
<p style="text-align: justify;">In other concerning news for Hollywood, China’s richest man, Wang Jianlin, had signed up mega A-listers Leonardo DiCaprio and Nicole Kidman to create the world’s biggest state-of-the-art film studio in Qingdao. Pictures of the two stars cosying up to Chinese producers in the eastern seaside city were all over the internet during the studio’s opening in September 2013. The <i>Independent</i> even hailed this <a href="http://www.independent.co.uk/arts-entertainment/films/news/the-rise-of-chollywood-billionaire-wang-jianlin-signs-leonardo-dicaprio-and-nicole-kidman-to-create-worlds-biggest-film-studio-8834953.html">‘The Rise of Chollywood’.</a></p>
<p style="text-align: justify;">So what do the people of Hollywood think of all this? In early 2013, director <a href="http://insidemovies.ew.com/2013/04/18/china-hollywood-tom-cruise-joss-whedon/">Joss Whedon</a> summed up the industry’s sentiments in a candid statement to reporters asking about Marvel’s upcoming <i>Avengers</i> sequel: “If someone came to me and said, ‘We’re looking into doing a chunk of this [movie] in China’—well, I’d have to think about it. China is on my radar. It can’t not be at this point.”</p>
<p style="text-align: justify;">The writing is on the wall – Hollywood needs to get in with the Chinese market, but the hulking question is still <i>how</i>? How does one make a movie that is set in America, starring American actors and spoken in English appeal to both western and Chinese audiences? How does one bridge the vast cultural differences between the two regions? Take into account China’s strict quota of 34 foreign movie releases per year and there’s no doubt that Hollywood and other foreign producers will need to step up their game if they are to even get a foothold in the egregiously tough Chinese market.</p>
<p style="text-align: justify;">So far, it seems like Hollywood has settled on an age-old strategy – the same one that propelled Japanese pop culture into the new zeitgeist of the 80s and 90s, and now Korean pop music to our ears. It’s called <strong>‘glocalisation’</strong> – tailoring international content to suit the tastes and cultural values of a target region (local + global = glocal). Like <a href="http://news.bbc.co.uk/2/hi/programmes/click_online/9159905.stm">Keiji Inafune,</a> an executive at Capcom once said about the marketing of Japanese videogames outside of Japan: “It’s like sushi. Everyone loves sushi in the West, but you can’t just serve sushi over there like it is in Japan.”</p>
<p style="text-align: justify;">So in an ironic twist of fate, Hollywood has been learning from Asian pop culture, and is now tweaking its films to pander to the unique tastes of Chinese viewers. However, most of its attempts have gone awry, leading to scoffs from amused audiences rather than admiration. Here are a few examples, some passable, but most of them of risible:</p>
<p style="text-align: justify;">• 2012’s <i>Looper</i> relocated parts of the film from France to Shanghai and cast mainland Chinese actress Qing Xu as Willis’ love interest. Unfortunately, she had no lines in the movie and is widely observed to have appeared purely to give the film its <a href="http://www.tealeafnation.com/2012/10/pandering-misfire-sino-centric-loopers-lesson-for-hollywood-producers/">‘Chinese element’.</a></p>
<p style="text-align: justify;">• 2012’s <a href="http://screenrant.com/red-dawn-villains-china-north-korea-schrad-106177/"><i>Red Dawn</i></a>, originally about a Chinese military invasion of America, changed its invaders to North Koreans at the eleventh hour, recognising the need to salvage the movie’s appeal for the Chinese market.</p>
<p style="text-align: justify;">• 2013 blockbuster <i>Ironman 3</i> inserted four minutes of extra footage just for the Chinese release. Mainland Chinese actor Wang Xueqi played Dr. Wu, who operates on Tony Stark and fulfils a bit of product placement by drinking from a carton of <i>Gu Li Duo</i> (a Chinese milk drink) during his limited screen time. Top mainland actress Fan Bingbing was cast as a sexy nurse who worries about the success of the operation. It was an incredibly boring scene and the <a href="http://www.hollywoodreporter.com/news/iron-man-3-china-scenes-450184">Chinese audience was not so impressed.</a></p>
<p style="text-align: justify;">• Released in May 2014, <i>X-Men: Days of Future Past</i> cast Fan Bingbing as the Marvel superhero <i>Blink</i> who possesses the power of teleportation. In the comics, <a href="http://www.wantchinatimes.com/news-subclass-cnt.aspx?cid=1104&amp;MainCatID=11&amp;id=20140523000058"><i>Blink</i> is not Asian.</a></p>
<p style="text-align: justify;">• 2014’s <i>Transformers: Age of Extinction</i> was a serial offender of nonsensical Chinese product placements. Characters were seen drinking Chinese energy and milk drinks, driving Chinese cars, and having a fridge filled with duck meat (a seven second shot for a Chinese takeaway chain that sells duck necks) – all in the American state of Texas. But the weirdest ‘product placement’ is not about a product at all, but a political system – authoritarianism. As noted by other <a href="http://www.hollywoodreporter.com/news/chinese-audiences-bemused-by-transformers-716025">commentators</a>, the film depicts the US Chief of Staff as a ditz who can’t get anything right until a firm overseer played by Kelsey Grammar steps in to fix the problem of evil alien transformers. Apparently, this is a situation that can only be solved by the decisiveness of an authoritarian ruler.</p>
<p style="text-align: justify;">• 2015’s <i>Mission Impossible: Rogue Nation</i> generated great interest in China and abroad with rumours of respected actress Zhang Jingchu being cast as a significant character, one which <i>Variety</i> claimed to be “integral to a major plot twist”. She ended up appearing for a total of 30 seconds.</p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled4.png"><img class="aligncenter size-full wp-image-22687" src="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled4.png" alt="Untitled" width="350" height="269" /></a>
<p style="text-align: justify;">At this point, Hollywood is trying, but it’s certainly not doing a very promising job. As the <a href="http://www.hollywoodreporter.com/news/iron-man-3-china-scenes-450184">consensus</a> goes, simply inserting extraneous scenes or actors with ‘Chinese elements’ is not only patronising, but will not stand against the increasing popularity of domestic Chinese movies like 2013’s slapstick comedy <i>Lost in Thailand</i>, which became that year’s highest grossing movie in China. If Hollywood truly wants to win over Chinese viewers, it’s time to get rid of the corny placements of Chinese actors and Chinese consumer products. Instead, it needs to cast Chinese or Chinese-American actors as real and important characters, not as superfluous ‘oriental’ props. Until then though, Chinese audiences can enjoy watching actors drink Chinese milk drinks in America.</p>


<hr />

<em id="Cindy"> Written by Cindy He (何诗婷). Cindy is completing a Bachelor of Laws and Arts at Monash University, with a major in Asian Studies. Cindy possesses a strong passion for Asian history, politics, and pop culture. She has previously worked as a news correspondent for 3CW Chinese Radio Station and Beijing-based China Radio International. She is currently a regular writer for a Melbourne print publication E-Magazine. </em>