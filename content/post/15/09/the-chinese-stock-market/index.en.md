{
  "title": "Understanding the Chinese Stock Market",
  "author": "ACYA",
  "date": "2015-09-17T05:37:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled.png"
    },
    {
      "title": "",
      "type": "image",
      "src": "Untitled1.png"
    },
    {
      "title": "Unknown",
      "type": "image",
      "src": "Unknown.jpeg"
    }
  ]
}
<p style="text-align: justify;">The Chinese stock market has been making headlines lately. After a period of exuberance and high returns, prices have declined with the Shanghai Composite Index falling approximately <a href="http://www.bloomberg.com/quote/SHCOMP:IND">40%</a> since its high of 5166 points in June. On 24 August, since dubbed ‘Black Monday’, the index fell 8.5% – its largest one day fall since 2007. The fall triggered a sell-off in Western markets and has contributed to widespread fear that China’s economy is in trouble. But what role does the stock market play in the wider Chinese economy?</p>
<p style="text-align: justify;">New rules introduced in December 2014 relaxed limits on margin lending, allowing more investors to borrow money to invest in the market. In April 2015 rules were also changed to allow individuals to open up to 20 trading accounts, thereby giving easier access to the market.</p>
<p style="text-align: justify;">State-controlled media also encouraged the bull market. In April 2015 the <a href="http://en.people.cn">People’s Daily</a> told readers <a href="http://www.theguardian.com/world/2015/jul/16/why-chinas-stock-market-bubble-was-always-bound-burst">“Tulips and bitcoins are bubbles…</a> but if A-shares are seen as the bearer of the Chinese dream, then they contain massive investment opportunities.” This media attention promoted the widespread belief that the government was underpinning the market, giving investors false confidence that prices would not fall.</p>


[caption id="attachment_22662" align="aligncenter" width="425"]<a href="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled1.png"><img class="size-full wp-image-22662" src="http://www.acya.org.au/wp-content/uploads/2015/09/Untitled1.png" alt="Returns on the Shanghai Composite Index in the past 12 months" width="425" height="250" /></a> Returns on the Shanghai Composite Index in the past 12 months[/caption]
<p style="text-align: justify;">As prices started to decline, government intervention continued, with various state-backed Chinese financial firms directly buying stocks in an attempt to stabilise prices. According to <a>Goldman Sachs</a> the government has spent approximately 1.5 trillion yuan stabilising prices in the past three months. In addition, major shareholders were prevented from selling their holdings, state-owned enterprises were encouraged to repurchase shares and interest rates were cut. Ultimately, this intervention was unable to prop up the market with the market currently tracking close to 3,000 points.</p>
<p style="text-align: justify;">It is important to remember that by global standards the Chinese stock market is immature. As China moves away from an investment-driven economic growth model to a consumption-driven model, it requires robust financial markets to support this change. <a>Chinese consumption levels</a> have been very low at approximately 36% of GDP for the past three decades; for developed economies consumption constitutes around 80% of GDP.</p>
<p style="text-align: justify;">Chinese people tend to save, with very few investment options available for these savings. The property market is the main investment vehicle and it fuels almost a quarter of GDP (and in the past few years has been an overheated market). A strong stock market will allow China’s vast savings to be used more productively by Chinese companies, and is fundamental in achieving efficient resource and capital allocation.</p>
<p style="text-align: justify;">The stock market is also a vehicle through which China’s state-owned enterprises can be privatised, thereby improving productivity and stimulating economic growth.</p>
<p style="text-align: justify;">Western markets attempt to be efficient in nature and stock market movements tend to be correlated with economic activity. At present, the Chinese market is irrational and much more of a speculative vehicle. The stock market also has a high percentage of retail investors with only <a href="http://theconversation.com/explainer-what-role-does-the-stock-market-play-in-the-chinese-economy-46691">20% of participants being institutional investors</a>, which means that those with little knowledge of markets (many with less than a high school education) are driving prices. This is reflected in experts’ comments. For example, Li Daokui of Tsinghua University <a href="http://finance.sina.com.cn/zl/china/20150524/091522252526.shtml">remarked</a> in May that <a href="http://www.chinadaily.com.cn/2014-09/06/content_18558036.htm">“dancing aunties”</a> were dabbling into the stock market. The notion that the stock market is a <a href="http://www.smh.com.au/business/comment-and-analysis/the-chinese-stock-market-is-the-worlds-biggest-casino-20150708-gi7q1f.html">casino</a> is also a recurring metaphor.</p>
<p style="text-align: justify;">Ultimately, the immaturity of the stock market and relatively small number of participants (<a href="http://www.economist.com">less than a fifth</a> of China’s households invest in shares) means that the decline is not correlated with wider economic activity and will not have a catastrophic effect on the Chinese economy. However, the crash represents a slowdown in wider financial system reform and liberalisation for China and is yet another setback in China’s quest for sophisticated and efficient capital markets.</p>
<p style="text-align: justify;">It is also symbolic of a larger problem that China must overcome; how to successfully scale back the government’s administrative power in order to give more play to the role of the market in driving economic growth. This combination of communism and free-market economics, coupled with the sheer size of the Chinese economy will lead to many hurdles and perhaps more stock market booms and busts in the coming years.</p>


<hr />

<i>
Written by Alexandra Meek. Alex is the editor of the ACYA blog and is a Junior Policy Associate at the University of Sydney China Studies Centre.</i>

This post was originally published by the <a href="http://www.chinastudiescentre.com.au/politics/understanding-the-chinese-stock-market/">University of Sydney China Studies Centre</a>