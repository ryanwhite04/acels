{
  "title": "2015 Leizhou Volunteering Trip Report",
  "author": "ACYA",
  "date": "2015-09-07T11:27:09+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA Leizhou Frontpage",
      "type": "image",
      "src": "ACYA-Leizhou-Frontpage.jpg"
    },
    {
      "title": "ACYA Leizhou Volunteers",
      "type": "image",
      "src": "6941681891b557a0be83bdf5a85b7d7.jpg"
    }
  ]
}
<p style="text-align: justify;">Arriving into Leizhou train station after a seven-hour journey from Guangzhou, I wasn’t sure what to expect for the coming ten days. Despite being about the same size as Adelaide, Leizhou is a relative unknown in both Australia and China, with almost no foreign presence. Getting off the train to be greeted with welcome signs and a bunch of excited Chinese university students, however, set the tone for what was to be a fantastic trip.</p>
<p style="text-align: justify;">The Leizhou Volunteer Trip, organized by ACYA in co-ordination with the Leizhou College Students Volunteer Association (LZCVA), brings in native English speakers to help out at an English camp for school kids on summer holiday. The camp - with its wonderful catchphrase ‘One Summer Camp; One Hot Dream’ - is organized and run by local university students studying English in cities across China returning home for the summer, and gives the kids a chance to experience English outside the rigid Chinese education system. Myself and two other Australian ACYA volunteers were tasked with trying to get them enjoying learning and speaking English despite their often-crippling shyness. With my admittedly poor Chinese language skills and over 200 participants, I’d be lying if I said this wasn’t initially a little daunting!</p>
<p style="text-align: justify;">The students were divided into seven different classes and with some help from a Chinese teacher we collectively spent an hour with each class, each day, with sessions on spoken English and Australian culture. As most English teachers are native Chinese-speakers and very few kids had come into contact with foreigners before, we focused on getting them talking and participating as much as possible. A series of games and tongue twisters —a favourite being ‘a villainous Victorian village’s vegemite’ allowed us to show everyone a bit of Australia while getting kids excited about English. We finished every class by teaching them to sing the timeless classic ‘Kookaburra sits in the Old Gum Tree’, which was then performed at the end of camp ceremony. There really is nothing quite like 200 students screaming a nursery rhyme about a laughing bird at fan-girl levels.</p>
<p style="text-align: justify;">In a city that gets very few foreign visitors, anyone who volunteers at the camp can expect to be treated like royalty. We stayed in homestays with the university students and were invited out to lunches and events almost every day—including dinner with the head of the Communist Party in Leizhou. In return for being taught to make dumplings and constantly being fed delicious food, we had a bit of fun trying to make an Australian dish for everyone to try. After discovering it was almost impossible to find pasta (and being told for the 50th time that noodles were the same thing…) we ended up making sandwiches. These were surprisingly very well received, becoming the most photographed item of the day and a hit on WeChat moments. We capped off our stay with a couple of days spent seeing the sights of Leizhou, parts of rural China and—something no trip to China should be without—a wild night of karaoke.</p>
<p style="text-align: justify;">While our trip was short, it was a great experience for both us and the Chinese university and school students and we forged relationships and contacts that will last well beyond our departure. It became clear as we were leaving the school with each class screaming, ‘I love you’, asking us to come back, that our presence at the camp had made an impact. Further, despite studying English, the university students running the camp had never really interacted with foreigners and hearing of their newly found motivation to improve their English to work and travel in Australia was heartening. It’s this level of engagement that lays the foundation of the relationships between our two countries and a promising future for both Australia and China. I cannot recommend this trip enough and hope it carries on into the future and continues to bring young Australians to the ‘real China’, beyond the reach of most foreigners.</p>

<a href="http://www.acya.org.au/wp-content/uploads/2015/09/6941681891b557a0be83bdf5a85b7d7.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/09/6941681891b557a0be83bdf5a85b7d7-1024x700.jpg" alt="ACYA Leizhou Volunteers" width="600" height="411" class="aligncenter size-large wp-image-22617" /></a>

<i>Content and photos by Leizhou Volunteering Trip 2015 volunteer Nicholas Harding</i>

If you'd like to know more about the Leizhou Volunteering Trip click <a href="http://www.acya.org.au/education/leizhoutrip/">here</a>. <b>Applications have closed for 2015 but keep an eye out for the 2016 trip!</b>