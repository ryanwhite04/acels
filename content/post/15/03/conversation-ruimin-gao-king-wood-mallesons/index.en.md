{
  "title": "In conversation with Ruimin Gao, Solicitor in Dispute Resolution and China Projects at King & Wood Mallesons",
  "author": "ACYA",
  "date": "2015-03-28T10:25:20+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "110116-118 Graduates in PH 11 April 11",
      "type": "image",
      "src": "110116-118-Graduates-in-PH-11-April-11.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/03/110116-118-Graduates-in-PH-11-April-11.jpg" rel="attachment wp-att-23871"><img class="size-medium wp-image-23871 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2015/03/110116-118-Graduates-in-PH-11-April-11-300x200.jpg" alt="110116-118 Graduates in PH 11 April 11" width="300" height="200" /></a>

This month, ChinaBites interviewed <a href="http://au.linkedin.com/pub/ruimin-gao/81/221/a2a">Ruimin Gao</a>, a Sydney-based lawyer specialising in Dispute Resolution and China Projects at King &amp; Wood Mallesons. She was previously an Adviser to Prime Minister Kevin Rudd and a Senior Legal Officer in the Attorney-General's Department. Ruimin has a Master of Laws (International Law) from the Australian National University and a Bachelor of Laws (Hons)/Bachelor of Commerce dual degree from the University of Queensland.

<strong>Apparently this is a question you get asked a lot – Where are you from?</strong>
I was born in China, lived in Sweden, then went to school and grew up in Queensland. After university I moved to Canberra for work and more recently became a Sydneysider. The short answer I usually give is: “Here”.

<strong>What are your earliest memories of China?</strong>
My hometown is the north-eastern industrial city of Shenyang, which today has a modest population of about 8 million. My memories of childhood are of the constant crush of people everywhere and freezing winters that could get down to minus 20 degrees.

<strong>What are your earliest memories of Australia?</strong>
I came to Australia in the early 1990s. I spoke Chinese, Swedish and a handful of English words. I remember eating my first meat pie on the first day of school and being the only Asian kid in my class. Over the years I have seen the demographic shift in Australia along with a greater awareness and pride in our multicultural society. I have grown to love this country and proudly call myself Australian.

<strong>What has your career been like to date?</strong>
Unpredictable! I studied law and my first job was working for a Judge in the Supreme Court. After that I joined a Commonwealth Government Department in Canberra and worked in international law, before taking up a role as an Adviser to Kevin Rudd. Now I am a commercial lawyer based in Sydney with a global law firm, where I am involved in developing the firm’s China practice.

<strong>How useful are your Chinese language skills in your work?</strong>
At work I regularly meet clients or contacts from China who, although they can hold a conversation in English, are visibly relieved and much more expressive when I offer to speak in Chinese. Language can be a real obstacle in communication and I see messages get lost in translation all the time. One thing that often strikes me is that most Chinese professionals, especially if they work in international companies or businesses, will commonly have a working proficiency in English. Unfortunately the same cannot be said of Australian professionals and a working proficiency in Chinese. It is still rare to meet non-Chinese Australians who can converse or conduct business in Chinese. But hopefully this is starting to change. When I was growing up the popular language subjects were always French, Japanese or German. I studied Chinese at home and at weekend schools for years, knowing it was an important part of my heritage but never imagining I would need it other than with my family. Nowadays of course people from all over the world are studying Chinese and I meet more and more Australians who have studied or worked in China. This is really encouraging and very important for Australia to position itself in the Asian century.

<strong>How important is it for Australians to be China-literate?</strong>
China is now the largest or second largest economy in the world, depending on which standard of measure is used, and Australia’s biggest trading partner. It is a rising global power and will continue to grow in its influence in the course of this century. This is a reality that Australia cannot ignore. Learning a language is about much more than just abstract words or phrases – it’s about acquiring an understanding of a different culture. For a long time the Western world has seen China as an exotic and foreign land with inherently different cultural values. In reality, China and Australia actually have a lot in common, particularly in their desires to build a better future for their respective peoples. Appreciating this is an important basis for our bilateral diplomatic, trade and business relationships.

<hr />

This interview featured in the March 2015 issue of ChinaBites. See the original version <a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=251ef5ec1c&amp;e=b58c6e9a8a">here</a>.