{
  "title": "The New Colombo Plan expands Chinese horizons for Australian students - Kate Duff, Assistant Secretary, New Colombo Plan Secretariat ",
  "author": "ACYA",
  "date": "2015-03-17T11:59:45+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYD Emma",
      "type": "image",
      "src": "ACYD-Emma-e1426593937364.jpg"
    },
    {
      "title": "ACYD Emma",
      "type": "image",
      "src": "49da3bcbeacc4193a28cb3272e170f75_n1-e1426597219319.jpg"
    },
    {
      "title": "ACYD Emma",
      "type": "image",
      "src": "49da3bcbeacc4193a28cb3272e170f75_n11.jpg"
    }
  ]
}
<p style="text-align: center;"><strong>‘The New Colombo Plan expands Chinese horizons for Australian students’</strong></p>
<p style="text-align: center;"><strong>Kate Duff, Assistant Secretary, New Colombo Plan Secretariat</strong><strong> </strong></p>
A signature foreign policy initiative, the New Colombo Plan aims to lift knowledge of the Indo-Pacific in Australia by supporting Australian undergraduates to study and undertake internships in the region.

The New Colombo Plan involves a scholarships program for study of up to one year and internships or mentorships, and a flexible mobility grants program for short and longer-term study, internships, mentorships, practicums, and research.

During the pilot phase of this program in 2014, over 1,300 young Australian undergraduates from universities across the country had the opportunity to study, live, and work in Indonesia, Singapore, Japan, and Hong Kong.

In its second year, the New Colombo Plan is expanding to offer more students the chance to experience the vibrant cultures and stimulating study and work opportunities in the region. In 2015, more than 3,000 Australian students will be supported to study and intern in the region under the New Colombo Plan, with numbers to increase in future years.

This is an exciting time for the New Colombo Plan and the Australian Government welcomes the participation of Mainland China and Taiwan and the continuation of Hong Kong as New Colombo Plan host locations.

The experiences of New Colombo Plan participants are diverse and varied. They undertake different types of programs across a range of disciplines. The program gives participants an international education experience, pushing their boundaries both intellectually and culturally, and offers them an opportunity to excel in their chosen field of study which will help them prepare for their future career.

Mainland China is one of the most popular destinations for 2015, with more than 520 Australian students expected to study there this year. Fields of study include law, finance, engineering, and computer science.

In 2015, seven Australian universities will receive mobility funding for projects in Taiwan, supporting study and internships for around 54 undergraduate students. The mobility projects are across a range of fields, including nursing, dentistry, commerce, public policy and administration, tourism, computing, engineering, and mathematics.

Around 70 undergraduate students are expected to undertake study and internships in Hong Kong in 2015. Fields of study include business, engineering, law, and human welfare services. This builds on the 130 students who are already undertaking study programs in Hong Kong during the pilot phase.

Private sector engagement and support are important elements of the New Colombo Plan, particularly given the program’s emphasis on internships and mentorships to ensure students gain insights into how businesses operate in the region and build professional networks.

New Colombo Plan internships are a monitored work or professional experience. They can range from one week to six months of work, be unpaid or paid, undertaken full-time after a study period, or part-time during a course of study.For example, a law firm may offer internships to a number of law students participating in the New Colombo Plan scholarship program, following the completion of their in-country semester-based study.

Mentorships are personal development relationships where a business professional or academic helps to guide a student in his or her work or career to support their learning and professional growth. They are undertaken concurrently with a student’s study program. For example, a consulting company may offer mentorships to students from a range of disciplines participating in the New Colombo Plan in one of their offices in the region.

Students are responsible for arranging their specific internship or mentorship, but businesses and universities can work together to design internship and mentorship programs that suit the needs of both parties.

The majority of scholars and many mobility students will undertake internships or mentorships as part of their New Colombo Plan experience.

Offering internships and mentorships connects businesses and NGOs with talented young Australians and promotes their organisation to Australian universities and students.

Private sector organisations across the region can get involved by registering to participate in the New Colombo Plan Internship and Mentorship Network. The Network promotes information exchange between businesses, universities, and students about internship and mentorship programs on offer under the New Colombo Plan. Enquiries about the Internship and Mentorship Network can be emailed to <a href="mailto:ncp.business@dfat.gov.au">ncp.business@dfat.gov.au</a>.

Emma Moore, a Bachelor of Law and Commerce (Economics) student at Monash University, is already taking advantage of the exciting work placement opportunities on offer under the New Colombo Plan.  Emma recently completed an exchange semester at the University of Hong Kong, where she studied the Chinese legal system, international public law, and international financial markets.

In January 2015, Emma commenced a month-long internship with Herbert Smith Freehills in their Hong Kong Corporate and Dispute Resolution teams. Emma is currently being mentored by Mr James Gray, Telstra Global’s legal counsel.

In addition to her studies and internship plans, Emma was chosen as one of the 14 Australian delegates to attend the fifth annual Australia China Youth Dialogue (ACYD) in Beijing in November 2014. At the ACYD delegates discussed matters relevant to Australia and China including global issues such as health, energy, food security, environmental sustainability, and trade.

Emma has made the most of the learning opportunities and life experiences offered through the New Colombo Plan. She says, “living in Hong Kong in and of itself has been an enriching experience…what I didn’t realise was the sheer depth of everything that I would have the opportunity to delve into whilst in Hong Kong… thanks to the Australian Government for making this amazing opportunity possible.”

<a href="http://www.acya.org.au/wp-content/uploads/2015/03/ACYD-Emma.jpg"><img class="aligncenter wp-image-22057 size-full" src="http://www.acya.org.au/wp-content/uploads/2015/03/ACYD-Emma.jpg" alt="ACYD Emma" width="422" height="281" /></a>
<p style="text-align: center;"><em>NCP Hong Kong Scholar Emma Moore speaking at the 2014 ACYD in Beijing. Australian Embassy.</em></p>
<p style="text-align: left;">We look forward to more New Colombo Plan participants engaging with ACYA’s network of impressive, engaged young Australians and Chinese while in China, and on their return to Australia.</p>
The New Colombo Plan mobility and scholarships program guidelines for the 2016 round will be published in March 2015. To find out more about this initiative please go to <a href="http://www.dfat.gov.au/new-colombo-plan">www.dfat.gov.au/new-colombo-plan</a> or contact <a href="mailto:ncp.secretariat@dfat.gov.au">ncp.secretariat@dfat.gov.au</a>.