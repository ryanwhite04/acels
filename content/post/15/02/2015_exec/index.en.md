{
  "title": "<!--:en-->ACYA's 2015 Executive<!--:-->",
  "author": "ACYA",
  "date": "2015-02-22T01:03:36+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->With the conclusion of our AGM, ACYA National is pleased to announce the executive team for 2015!

<em>Non-Executive Chair</em>: Jimmy Zeng
<em>Managing Director</em>: David Douglas
<em>Australia Manager:</em> James Castle
<em>China Manager:</em> William Fong
<em>Portfolio Manager: </em>Georgia Sands
<em>Treasurer: </em>Jerry Ding
<em>Secretary:</em> Eliza Egan

<span style="text-decoration: underline;"> <strong>Portfolio Directors</strong></span>

<em>Careers:</em> Robert James Scott Malcolm
<em>People-to-People:</em> Martin Ding
<em>Translations:</em> Allen Ai
<em>Publications:</em> Emma Moore
<em>IT:</em> Arsalan Khalid
<em>Communications:</em> Yu Feng Nie

Thanks once again to the 2014 executives and everything that they achieved! Want to get involved in ACYA throughout 2015? Check out the "Join ACYA" tab for opportunities!<!--:-->