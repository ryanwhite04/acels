{
  "title": "ACYA Journal of Australia-China Affairs 2014",
  "author": "ACYA",
  "date": "2015-04-27T11:12:21+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA Journal 2014",
      "type": "image",
      "src": "ACYA-Journal-20141.png"
    }
  ]
}
ACYA has just released the fourth volume of the Journal of Australia-China Affairs, co-published with the China Studies Centre at the University of Sydney. The Journal provides a high-quality platform for students and young professionals in the Australia-China space to publish academic essays, opinion articles and creative work. The Journal is peer-reviewed by the China Studies Centre and rendered completely English-Chinese bilingual by the ACYA Translation Subcommittee.

The Journal offers thought-provoking perspectives on a diverse range of issues connected to the Australia-China relationship. Topics in this volume range from the roles of Australia and China in fighting Ebola to the bilateral LNG industry, early Chinese migrant communities in Australia, tea culture in Australia and China, Chinese art exhibitions in Australia, Australians contributing to outdoor tourism in China and dialogue between our news organisations.

As highlighted by Kerry Brown, Director of the China Studies Centre, in his Foreword to the Journal, Xi Jinping’s visit to Australia in November 2014 is perhaps the closest Australia has come to a ‘China epiphany’. The Journal exemplifies the ‘imagination and innovation’ that Xi expressed he hoped to see in the Australia-China relationship. As Australia moves into a future that will be increasingly shaped by China, publications such as the Journal will serve an increasingly important role in connecting the thinking of Australians and Chinese about our relationship with each other

Thomas Williams, ACYA President in 2014, says the Journal is a ‘testament to ACYA members’ indefatigable enthusiasm for and ability to promote the bilateral relationship and Australia-China literacy’. 

The Journal can be read <a href="http://sydney.edu.au/china_studies_centre/images/content/ccpublications/ACYA_Journal/2014/Journal%20of%20Australia-China%20Affairs%202014.pdf">here</a>