{
  "title": "<!--:en-->ACYA-Asialink Internship applications now open!<!--:-->",
  "author": "ACYA",
  "date": "2015-04-09T11:16:03+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA Journal 2014",
      "type": "image",
      "src": "ACYA-Journal-2014.png"
    }
  ]
}
<!--:en--><strong>ACYA-ASIALINK (SYDNEY) INTERNSHIP PROGRAM</strong>

We recruit for our Sydney Internship program two to three times per year. Based in Asialink’s Sydney office, the internships are unpaid and will require one (maximum two) day(s) per week over 12 weeks.

Applications open approximately 6-8 weeks prior to the start date. An exit interview will be held at the end of the internship to gain feedback to ensure that the program continues to provide interns with valuable experience and learning. Asialink is not able to provide interns with:
- Monetary allowances or remuneration
- Accommodation or accommodation allowances
- Travel allowance

<strong>ABOUT ASIALINK</strong>

Asialink is Australia's leading centre for the promotion of public understanding of the countries of Asia and of Australia's role in the region. It is a key provider of information, training and professional networks. Asialink works with business, government, philanthropic and cultural partners to initiate and strengthen Australia-Asia engagement.

<strong>POSITION DESCRIPTION</strong>

Location: Asialink (Sydney) Level 21 56 Pitt Street, Sydney NSW 2000
Attendance: One or two days per week (day(s) of attendance are flexible)
Starting date: Week beginning Monday 25th May.
Hours: 9.30am to 5pm (with option to attend occasional events outside those hours)
Direct report: Asialink Manager of Partnerships and Development

An Asialink internship is an opportunity to be involved with Australia’s leading centre for Asia-Australia engagement, to expand knowledge of Asia-Australia issues across government, business, arts and education, and to develop Asia-Australia contacts and capabilities.

<strong>Tasks include (but are not limited to):</strong>
• Assisting with research
• Supporting corporate partnership development and other stakeholder outreach and campaigns
• Participating in strategic brainstorming sessions
• Participating in internal and external meetings as required (and relevant)
• Assisting with planning, curating and implementing events and other initiatives
• Writing communications for web and print
• Potential of assisting with design of collateral (print, e-communications, proposals and PowerPoint)
• Handover training to new intern at the end of the internship

<strong>Selection criteria </strong>
• Good written and verbal communication skills
• Research skills
• Proficiency in writing, editing and proofreading
• Basic knowledge in Microsoft Office suite with knowledge of Photoshop, InDesign and iMovie or Final Cut Pro highly regarded
• Strong interpersonal skills
• Self-motivated and a creative thinker
• Initiative and the ability to work independently while part of a team
• Willingness to learn and undertake a variety of tasks
• Demonstrated interest in Asia and Asia-Australia affairs
• Asian language skills (regarded favourably)
• Ability to multi-task, prioritise and work efficiently

<strong>Benefits </strong>
This position will allow the successful candidate to:
• Deepen their research and report-writing skills
• Further develop analytical and communications skills
• Gain experience communicating with people from a range of industries and sectors
• Gain a close understanding of Asialink and its work, in particular the work of Asialink’s business capability division, Asialink Business
• Gain a deeper understanding of Australia-China business and business activities

<strong>How to apply</strong>
To apply for the voluntary internship and make an important contribution to Australia’s engagement with Asia, please email your resume and a <strong>cover letter</strong> to gm_aus@acya.org.au.

ACYA will review the applications and determine a shortlist of candidates. Asialink will select an intern after interviewing the shortlisted candidates.

In your cover letter <strong>(no more than one page)</strong>, please explain why you would like to intern for Asialink in Sydney and why you are suitable for the role.

Please note that applications without a cover letter will not be considered.

Please email your application to: gm_aus@acya.org.au Deadline: 24th April 2015.<!--:-->