{
  "title": "<!--:en-->ACYA-RBS IMBA Scholarship<!--:-->",
  "author": "ACYA",
  "date": "2015-04-10T14:45:03+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "中国人民大学西门",
      "type": "image",
      "src": ".jpg"
    },
    {
      "title": "螢幕快照 2014-12-04 下午12.38.12",
      "type": "image",
      "src": "-2014-12-04-下午12.38.12.png"
    },
    {
      "title": "中国人民大学西门-618x358",
      "type": "image",
      "src": "-618x358-e1488333118402.png"
    }
  ]
}
<!--:en--><a href="http://www.acya.org.au/wp-content/uploads/2014/12/中国人民大学西门.jpg"><img class="size-medium wp-image-21930 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/12/中国人民大学西门-300x200.jpg" alt="中国人民大学西门" width="300" height="200" /></a>

The Australia-China Youth Association (ACYA) and The Renmin University of China Business School (RBS) are proud to announce that applications for the ACYA-RBS International MBA Scholarship are now open!

This being the first scholarship struck by ACYA with a Chinese mainland university, ACYA is thrilled to be able to provide this opportunity to one qualified member in 2015. The Scholarship will provide an ACYA member with a full scholarship over the 2-year degree.

ACYA President Tom Williams commented, “We commend and thank Renmin Business School for helping provide this fantastic opportunity to an ACYA member. This agreement demonstrates ACYA’s commitment to providing high quality educational opportunities to its members.”

An interview with the scholarship’s inaugural recipient, Shaun Harkness can be found here: <a href="http://www.acya.org.au/2014/10/interview-shaun-harkness-inaugural-recipient-acya-rmb-imba-scholarship/">http://www.acya.org.au/2014/10/interview-shaun-harkness-inaugural-recipient-acya-rmb-imba-scholarship/</a>

Through applying for this scholarship, applicants will also be considered for government and city based scholarships on offer at RUC. RBS International MBA students also have the opportunity to undertake exchange with multiple other prestigious universities around the world!

Interested applicants should send the listed application materials below to <a href="mailto:education@acya.org.au">education@acya.org.au</a>.

Applications for this round close <strong>May 8<sup>th</sup></strong>
Applications for the final round close on <strong>May 8<sup>th</sup>.</strong>

For more information on the scholarship and the International MBA program please visit <a href="http://mbaen.rbs.org.cn/">http://mbaen.rbs.org.cn/</a>. General enquiries about ACYA scholarships should be directed to the <strong>ACYA National Education Director Nathania Tangvisethpat</strong> (<a href="mailto:education@acya.org.au">education@acya.org.au</a>).

ACYA once again reiterates its delight at being able to provide this opportunity to our members and we wish you the best of luck in your applications!
<strong>About</strong>

<strong><em>Renmin University of China</em></strong>

<em>Established in 1937 Renmin University of China is one of the most prestigious universities in China, and is particularly renowned for its work across humanities and social sciences.</em>

<em> Its School of Business was founded in 1950 and is renowned as the Cradle of China’s Business Education. It is highly regarded by the People’s Republic of China’s Ministry of Education and their International Master of Business Administration (IMBA) program has the European Quality Improvement System and the Association to Advance Collegiate Schools of Business accreditation.</em>

<em>The RBS International MBA program provides students with the unique opportunity to study international business and management in the international market that is China. Students are also able to utilize Renmin University of China’s facilities and RBS opportunities within the IMBA, such as MBA Business mentor program and specialization in Chinese or International Business practices</em>

<em>Scholarship Details </em>

<em>Duration</em>: Length of International MBA program (2 years full time)

Program <strong>must </strong>be taken full-time.

<em>Expected Start - Graduation:</em> September 2015 - June 2017

<em>Location: </em>Renmin University of China, Beijing, China

<em>Amount:</em> Full Tuition scholarship <em>(Accommodation and living expenses NOT included)</em>
<h1><em>Eligibility</em></h1>
<ul>
	<li>Candidates are evaluated on previous academic performance, professional experiences, interview feedback and GMAT or GRE scores (GRE DI: 3735 Renmin U China School Business)</li>
	<li>Two years or longer of relevant work experience</li>
	<li>A bachelor's degree or above</li>
	<li>A competitive GMAT or GRE score (can be submitted at a later date)</li>
</ul>
o   Note: excelling at these exams may entitle you to receive an additional cash reward on top of the scholarship: http://mbaen.rbs.org.cn/admission-financing.php
<ul>
	<li>Non-Chinese citizenship</li>
</ul>
<strong><em> </em></strong>

<em>To Apply</em>

Please send the following materials to <a href="mailto:education@acya.org.au">education@acya.org.au</a>
<ul>
	<li>A cover letter outlining how you meet the above criteria and how the program will benefit you,</li>
	<li>An up to date resume</li>
	<li>Completed <a title="application form" href="http://mbaen.rbs.org.cn/downloadfile.php?id=12">application form</a></li>
	<li>A completed IMBA <a title="Scholarship Form" href="http://mbaen.rbs.org.cn/downloadfile.php?id=13" target="_blank">Scholarship form </a></li>
	<li>Two photocopies of bachelor or higher degree certificates</li>
	<li>Two original copies of official transcripts</li>
	<li>Two recommendation letters</li>
	<li>Two copies of the passport main page</li>
	<li>Four recent passport photos</li>
</ul>
<h1>For specific questions regarding the program</h1>
Check out the RBS IMBA Frequently Asked Questions page: <a href="http://mbaen.rbs.org.cn/imba-frequently.php">http://mbaen.rbs.org.cn/imba-frequently.php</a>

OR

Contact RBS International MBA Office: <a href="http://mbaen.rbs.org.cn/about-contact.php">http://mbaen.rbs.org.cn/about-contact.php</a>

IMBA Office

Room 614, Mingde Business Building

59 Zhongguancun Street, Haidan District

Beijing, 100872, China
Tel: +86 1062 514 665, +86 1062 515 699

Email: <a href="mailto:imba@rbs.org.cn">imba@rbs.org.cn</a>, <a href="mailto:zhangying@rbs.org.cn">zhangying@rbs.org.cn</a>

Fax: 86-10-82509167

OR

Nathania Tangvisethpat - ACYA National Education Director:
Email: <a href="mailto:education@acya.org.au">education@acya.org.au</a><!--:-->