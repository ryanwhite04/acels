{
  "title": "<!--:en-->ACYA Newsletter: March 2015<!--:-->",
  "author": "ACYA",
  "date": "2015-04-14T07:50:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->ACYA is back for 2015! In this edition there is a great scholarship on offer as well as an internship opportunity with Asialink Sydney!

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=eab15d00d3">Please click here to read ACYA Newsletter: March 2015 Edition</a><!--:-->