{
  "title": "ACYA Leizhou Volunteering Trip 2015",
  "author": "ACYA",
  "date": "2015-05-12T11:17:58+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Leizhou",
      "type": "image",
      "src": "Leizhou1.jpg"
    },
    {
      "title": "Leizhou2",
      "type": "image",
      "src": "Leizhou2.jpg"
    },
    {
      "title": "Leizhou Volunteer Trip",
      "type": "image",
      "src": "Leizhou-Banner.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/05/Leizhou1.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/05/Leizhou1.jpg" alt="Leizhou" width="580" height="387" class="aligncenter size-full wp-image-22212" /></a>

<p>ACYA is pleased to announce the Leizhou Volunteering Trip for 2015!</p>
<p><br />
The Leizhou Volunteering Trip is a one-week annual volunteer English teaching program in Leizhou, China, organised by ACYA and Leizhou College Students Volunteer Association (LZCVA). Volunteers will teach English to local high school students while being exposed to the &lsquo;real China&rsquo; far from the tourist trail and first-tier cities, and contributing to a rural community in a meaningful way. Not only is it a fun and rewarding experience, it also aims to promote Aussie culture to our Chinese counterparts, allow participants to develop strong relationships with young Chinese people and continue to lay the foundations for a bright future of cooperation between ACYA and LZCVA.</p>

<p>The program will include 5 days of volunteering, followed by a one-day guided tour of Leizhou, and an exciting English competition with the students.</p>
Dates: Monday, July 20, 2015 to Monday, July 27, 2015
Location: Leizhou, China
Accommodation will be provided and transportation fees (excluding airfares) will be fully funded.</p>
<h1>Application:</h1>

<i align="center">&ldquo;It was an unforgettable and life-changing experience for me.&rdquo; &ndash; <em>Ann Yu, 2014 volunteer</em></i>
<p><b>Applications for this year have now closed, check out the volunteer report <a href="http://www.acya.org.au/2015/09/2015-leizhou-volunteering-trip-report/">here</a></b></p>

<h1>Contact:</h1> 
<p>For any enquiries about the trip, please feel free to contact:<br />

Nathania Tangvisethpat &ndash;
ACYA National Education Director<br />
Email:&nbsp;<a href="mailto:education@acya.org.au">education@acya.org.au</a></p>


<a href="http://www.acya.org.au/wp-content/uploads/2015/05/Leizhou2.jpg"><img src="http://www.acya.org.au/wp-content/uploads/2015/05/Leizhou2.jpg" alt="Leizhou2" width="410" height="308" class="aligncenter size-full wp-image-22213" /></a>