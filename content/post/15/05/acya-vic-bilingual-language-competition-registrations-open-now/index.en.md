{
  "title": "ACYA VIC Bilingual Language Competition Registrations Open Now!",
  "author": "ACYA",
  "date": "2015-05-25T05:02:22+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "blcposter",
      "type": "image",
      "src": "blcposter.png"
    },
    {
      "title": "BLC banner",
      "type": "image",
      "src": "BLC-banner.png"
    },
    {
      "title": "BLC banner alternate",
      "type": "image",
      "src": "BLC-banner-alternate.png"
    },
    {
      "title": "BLC banner1",
      "type": "image",
      "src": "BLC-banner1.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/05/blcposter.png"><img src="http://www.acya.org.au/wp-content/uploads/2015/05/blcposter-724x1024.png" alt="blcposter" width="434" height="614" class="aligncenter size-large wp-image-22277" /></a>

The inaugural ACYA VIC Bilingual Language Competition (BLC) will provide a platform for Victorian tertiary students to showcase their language ability and cultural understanding to a statewide audience.

Registrations open May 25, 2015.

Watch the promotional video here:
https://www.youtube.com/watch?v=NiHGj7Phj3w

For more information, head to www.facebook.com/ACYAVICBLC or go to their website: www.acyavic-blc.com.

Registrations close July 24, 2015!