{
  "title": "Asialink & Westpac Bicentennial Foundation’s Next Generation Series",
  "author": "ACYA",
  "date": "2015-05-29T03:44:48+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Asialink Business",
      "type": "image",
      "src": "Asialink_Business_logo_-_small_.png"
    },
    {
      "title": "Unknown",
      "type": "image",
      "src": "Unknown.jpeg"
    }
  ]
}
<p style="text-align: justify;">For Australia, the most important implication of the rapid change in the world’s geo-pattern, especially the rise of Asia, is to cultivate talents capable of Asia engagement. Asialink’s Next Generation Series, which was held jointly with the Westpac Bicentennial Foundation, sparked some timely and pertinent conversations regarding this issue.</p>
<p style="text-align: justify;">On 23rd April, I attended the talk named “Conversations with Australia’s Next Generation of Asia Capable Leaders” in Sydney, and met some of the most prominent young figures in the Australia-Asia space.</p>
<p style="text-align: justify;">The panel discussion featured Jean Dong, Yaara Bou Melhem and Nicholas Mark, all of whom spoke of how a bi-cultural or multi-cultural awareness has helped their career advancement. Jean is the founder and managing director of Spark Corporation, a promising consulting firm that helps Australian businesses to develop a proper engagement strategy in the Chinese market, and was one of Australia’s top 100 fastest growing start-ups in 2013. Her experiences have also involved a role in leading one of the negotiations in the Australia-China Free Trade Agreement. Her deep understanding of both cultures enabled her to evaluate and communicate the needs of each party in a professional manner, contributing to the reaching of an agreement.</p>
<p style="text-align: justify;">Nicholas is an expert in legal workings involving Indonesian companies. Unlike Jean who has Chinese origins, he grew up in Australia and learned Indonesian purely out of interest. Apart from his job, which involves helping Indonesian clients in their legal proceedings or Australian businesses eager to understand the Indonesian legal system, his interest led him so far as to publish a children’s book in Indonesia. His experience is a perfect example of how mastering a relatively unpopular foreign language can make one a “market niche” in the talent pool. That being said, he specifically shared his projection of the Indonesian economy’s rise in the near future, and the huge opportunities unfolding with the boom of the largest archipelago country in the world.</p>
<p style="text-align: justify;">Yaraa’s gave the audience unique insight into working in a cross-cultural environment without being able to speak the local language. She has been to exotic places including the Middle East, where she witnessed how war and conflict can affect the life of millions. She explained that the secret of winning trust from people of foreign cultures is to show a bit more than respect: a genuine interest and effort to learn is essential when language is the barrier.</p>
<p style="text-align: justify;">If to establish relationships and win trust is the learning objectives, then to promote the awareness of engagement with Asian counterparts is the strategic imperative facing Australian youth leaders. Questions from the audience showed a legitimate concern about the promptness of a government-led change. There was a consensus amongst the panelists that business institutions and grass root youth leaders is and will be the driving force in this initiative. Both Australia Indonesia Youth Association (AIYA) and Australia China Youth Association (ACYA), for example, have programs promoting Asian languages in local primary schools. The Westpac Bicentennial Foundation’s Asia Exchange Scholarship also aims to provide opportunities for Australian youth to explore Asia from the perspective of career developments and form long lasting bonds through which future opportunity will flow.</p>
<p style="text-align: justify;">Equally, if not more thought provoking was the opening speech by Mr. Bala Swaminathan. As Westpac’s President and General Manager Asia, his interpretations of the implication of an Asian age for youth were luminous with some ready-witted humor. He explained that negative yield (negative interest) we went through as a byproduct of the GFC had never existed for thousands of years, and similar 'non-existences' of the past will appear even faster. Dealing with new cultures and new markets is only one of the many challenges coming along with this rapidly changing world, and the next generation will find themselves moving across borders constantly. Therefore, engagement and adaptation is necessary and will be necessary not only when one is located offshore. Being alert to changes happening within Australia and discerning a corridor beyond the surfaces between the continents will bring real advantages.</p>


<hr />

<img class="bites-avatar" src="http://placehold.it/100x100" />
<p style="text-align: left;"><em id="Cecilia"> Cecilia Ren is an active observer in the space of Australia-Asia business and political relations. She is currently pursuing her Master of Commerce at the University of Sydney, with majors in Accounting and Finance. Prior to her study in Australia, she studied in Singapore and the United States. </em></p>