{
  "title": "China Policy Traineeship",
  "author": "ACYA",
  "date": "2015-05-02T12:48:25+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "China Policy",
      "type": "image",
      "src": "China-Policy.png"
    },
    {
      "title": "China Policy1",
      "type": "image",
      "src": "China-Policy1.png"
    }
  ]
}
China Policy is a Beijing-based strategic advisory and research company. Our research team’s detailed, systematic coverage of China’s own domestic policy discussion gives us our edge, and our clients' confidence in our advice. It provides the evidence base from which we tailor our guidance on strategic China questions. Clients include large multinationals, financial institutions and international organisations.

The China Policy-ACYA Traineeship Program aims to improve the policy-related skill sets of Australian graduates, extending their understanding from headline issues to the deep structural underpinning of China’s policy challenges. All our positions offer an unparalleled opportunity to get inside the policy debates and learn how to map and track the policy discussion. Positions are normally not available to recent BA graduates.

Our core sectors include: economy; energy, resources and environment; social policy; governance and law; and food and water security.

We encourage exceptional candidates to contact ACYA ahead of deadlines to express an interest in a placement in subsequent rounds, or to fill a casual vacancy.

For more information visit policycn.com.

Position Title: China Policy Trainees
We are offering six-month trainee positions under the ACYA-China Policy Traineeship Program. Positions will start in mid July 2015, although timing may be negotiated for successful applicants.

Job Summary
Trainees will be placed into a relevant role based on their application and experience. We have positions available in the following teams:

- research: the trainee will work with a portfolio team following policy development and publishing cp.positions, cp.signals, cp.observer and cp.focus studies. They are encouraged to develop their own specialist expertise while in Beijing, and will be supported in drafting China Policy publications in their areas of interest.
- editing and communication:the trainees will work with the management team to assist in editing, design and formatting documents. They will manage our web-based publications and develop our web presence. Training will be offered in Adobe Suite. Trainees are encouraged to use design software to develop infographics.

Knowledge and Skills
The following outlines applicants’ preferred knowledge and skills. However because candidates will be assessed based on their individual skill set, we encourage high calibre applicants to apply regardless of background and experience.

- social science MA graduate or final year MA student
- knowledge of core policy sector/s
- exceptional standard of English
- advanced Chinese (necessary for the research trainee position)
- eye for detail and clear communicator
- good inter-cultural communication skills
- knowledge of Adobe Creative Suite (advantage for the communication position)
- ability to work under pressure and to tight deadlines

Other Requirements
Candidates should be flexible, able to take initiative, and be willing to work overtime if necessary.

Visa and remuneration
Trainees receive a stipend to cover living expenses while in China. They are responsible for their own travel and health insurance costs. With the support of the ACYA-China Policy Trainee Program, they can expect to be issued an M visa for six months.

Please indicate position title and preferred team (if applicable) in your application letter, and email your application letter and CV to: careers@acya.org.au

Please note: Applications will close Sunday 17 May 2015.