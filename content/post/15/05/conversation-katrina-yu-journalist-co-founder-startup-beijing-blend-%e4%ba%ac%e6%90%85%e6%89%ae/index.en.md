{
  "title": "In conversation with Katrina Yu, CCTV journalist and co-founder of startup Beijing Blend (京搅扮)",
  "author": "ACYA",
  "date": "2015-05-28T11:52:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Katrina Yu",
      "type": "image",
      "src": "Katrina-Yu.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Katrina-Yu.jpg" rel="attachment wp-att-23925"><img class="size-medium wp-image-23925 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Katrina-Yu-294x300.jpg" alt="Katrina Yu" width="294" height="300" /></a>

This month, we were fortunate to catch up with Katrina Yu about expatriating from Australia to work in China. Katrina is an experienced journalist and television presenter currently working at CCTV in Beijing, following a three-year stint at SBS World News, where she reported from Australia, the Phillipines, Singapore and Hong Kong. Katrina has a passion in multi-cultural story-telling, as can be seen in the media startup she co-founded: Beijing Blend (京搅扮). She was also nominated for Young Australian Journalist of the Year in 2013 in the prestigious Walkley Awards.

<strong>Can you tell us a bit about what your work now?</strong>
I work as a television presenter for CCTV International’s Travelogue program and am also involved in other projects. I’m a co-founder of Beijing Blend, a media start-up focusing on giving audiences outside of China insight to the country’s hottest culture and conversation.

<strong>Why did you decide to leave Australia to work in China?</strong>
I had always been interested in living and working in Asia because of my background and because of how the region seemed to be developing and changing so quickly. And there’s nowhere else in the world that seems to be changing faster than China. That’s incredibly exciting and I wanted to see and experience it for myself.

Also after being raised in the Middle East and Australia, and also studying in Europe, I was interested in living somewhere completely different. China posed a much bigger challenge, as well as an intriguing question about what direction my life might take thereafter. Staying in Australia I was faced with a more stable and predictable path, yet China was a big unknown - scary and irresistible all at once.

<strong>How would you comment on the workplace culture in China?</strong>
I think to work in China, or in a Chinese company, you need to understand the overall Chinese culture of collectivism, and also be aware that attitudes to structure and hierarchy are quite different to that of Australia. It’s important to be adaptable and be willing to listen and observe the practices of a particular workplace, because so many things are done differently to what Australians might be used to. For example so much official communication takes place through Wechat messaging, which is what I wasn’t used to back home. Overall you and your Chinese colleagues are learning from each other, so don’t be afraid to ask questions – and expect to laugh off funny or awkward instances of miscommunication or confusion from time to time!

<strong>What has been the most rewarding experience during your career in China?</strong>
I’m incredibly fortunate that my career takes me around the country to experience many different parts of China. I quickly realised that China was as diverse in land and lifestyle as perhaps the whole of Europe! I think one of the most rewarding things is to encounter so many different people, some who have never left their particular remote village, and have them welcome me so warmly into their home, and share with me their story. Those personal encounters are priceless.

<strong>What has been a major obstacle throughout your career journey, especially during your transition to China?</strong>
I wouldn’t call it an obstacle, but a challenge is having to relearn the ways of doing things in my workplace and industry. Media in Australia, or in the west generally, has developed very differently as an industry to media in China.
Also in Australia I had come out of university with a strong understanding of how important networks were in helping to expand my career. I had taken time over the years to do build a professional network around me in Sydney, and coming to China I had to leave that behind and start from scratch.

<strong>What advice do you have for students/young professionals aiming to work in the Australia-China space?</strong>
Build networks, speak to people and take the plunge. The differences are vast and you may never quite feel 100% ready, but you’ll learn most of what you need to know by doing rather than speculating. Learn Chinese, of course, but also educate yourself as much as possible as to what is happening in China in your particular industry, or particular area of the Australia-China space. Follow and subscribe to the important magazines, blogs etc. Attend events by relevant organisations and sit down for coffee with those who are happy to give you further insight about the nuts and bolts of areas you’d like to work in. As with anything don’t be afraid to reach out and take risks.

Follow Katrina's start-up Beijing Blend here:
<a href="http://youtube.com/c/beijingblend">youtube.com/c/beijingblend</a>
<a href="http://facebook.com/beijingblend">facebook.com/beijingblend</a>
Tweet <a href="http://twitter.com/BeijingBlend">@BeijingBlend</a>

<hr />

This interview was conducted for the May 2015 issue of AustraliaBites. Read the issue <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=af3d19bfe7">here</a>.

&nbsp;