{
  "title": "A Golden Age of China: Qianlong Emperor 1736-1795",
  "author": "ACYA",
  "date": "2015-05-18T01:51:15+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Qianlong-slide-1232x693",
      "type": "image",
      "src": "Qianlong-slide-1232x693.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/05/Qianlong-slide-1232x693.jpg"><img class="aligncenter size-medium wp-image-22258" src="http://www.acya.org.au/wp-content/uploads/2015/05/Qianlong-slide-1232x693-300x169.jpg" alt="Qianlong-slide-1232x693" width="300" height="169" /></a>
<p style="text-align: justify;">There have been over one hundred Chinese emperors. I know this because I Googled it. But of these innumerable rulers I could name two at best. Now, thanks to an ACYA excursion to the National Gallery of Victoria I can name one more – The Qianlong Emperor. The National Gallery of Victoria (NGV) is holding 'A Golden Age of China: Qianlong Emperor 1736 – 1795' until the 21st of June. The pieces hail from The Palace Museum, Beijing, with support from the Australia China Council and other major sponsors. Jointly organised by the Monash and LaTrobe University ACYA chapters, a group of approximately twenty ACYA members visited the NGV to meet the Qianlong Emperor. Through art and untold treasures visitors gained a real understanding of the emperor's private and public life.</p>
<p style="text-align: justify;">The first room introduced us to the man who ascended the throne as the Sixth Manchu Emperor. He dominated the room from a portrait in which he appeared more like a European monarch than a Chinese emperor. He scrutinised us as we milled about, waiting for our tour to begin. Our guide arrived and led us to the feet of the emperor. The emperor was born Hongli, son of the Yongzheng Emperor, and was promising from a young age. His grandfather, the Kangxi Emperor, praised his fine military mind and he was fond of hunting from horseback with a bow and arrow. Our guide paused to gesture across to a sturdy and unadorned bow sitting behind glass next to a quiver of red satin and gold thread. The emperor, she continued, came from the proud Manchu minority and honoured its traditions and language.</p>
<p style="text-align: justify;">Walking right, we approached a painting three times the size of the portrait. A hunting party led by the emperor wound through a mountain landscape. The occasional copse of miserable pine trees hid intricately captured deer, these twists revealed the provenance of the painter. Italian Jesuit Guiseppe Castiglione was a favourite of Qianlong's Court and his Western artistic influence bled through every piece. Fine Art majors in the tour murmured and nodded, having long since picked up on clues in the fixed perspective and naturalistic flourishes. The plaque by the painting invited children to search for small details: rifles slung across hunters' shoulders, trained hunting falcons, a tiger slinking out of frame. As we rounded the corner into the next room our eyes lingered on the painting.</p>
<p style="text-align: justify;">The guide continued into the next dim room, often pausing at length by major pieces to delve into their history, artistry and significance. Starting with a robe, a necklace or hat and tying in other encased and illuminated smaller pieces, she slowly built a story. Deep yellow was the colour of the emperor. Dragons with five claws represented the insignia of the emperor. His status was woven into his garb. Even the most casual wear of the Qianlong Emperor was covered in small symbols telling of his infinite wisdom, Mandate of Heaven and filial piety. The stifling room matched the stifling clothes. It was a relief to enter the third room with its tall roof and clean lighting.</p>
<p style="text-align: justify;">In the first two rooms the Qianlong Emperor was playing prince, wearing painstaking costumes between bouts on horseback. The third room, however, showed him living the role he was born to play. Here we saw him at diplomacy with Northern tribes, observing religious rites and paying obeisance to his ancestors. Amongst the works from Castiglione were pieces that departed from the European view of the Qianlong court. The four seasons became characters in these new paintings. Motifs held auspicious meanings and our guide helped us see the subtle ways in which the artists flattered the emperor. An ear of lingzhi fungus drawn in the corner of a painting encoded longevity. A ceramic still-life depicted a crab, split pomegranate and scattered seeds on a plain white plate - an elaborate pun. The deciphered parts revealed a wish for a long line of bright children. Making our way to the final section we began to understand the scope of the Qianlong Emperor's genius.</p>
<p style="text-align: justify;">The Qianlong Emperor was a deft artist and linguist, making him an expert at seeking the attention of the world. Nothing demonstrated this better than the final portrait of the emperor – this time portrayed as the Manjusri, the Bodhisattva of Wisdom. Castiglione's portrait inserted the emperor into the Buddhist pantheon. Works engraved with Tibetan, Manchu, Mongolian and other languages revealed his broad focus. The walls of the final room dripped with calligraphy and prose, confirming the Qianlong Emperor was a consummate ruler.</p>
<p style="text-align: justify;">As we exited through the gift shop we talked about our favourite pieces and the skilful curation of the emperor's early life to his final days. We found a t-shirt with the emperor's familiar face and alternately stretched then crumpled it. Now smiling and young, now wrinkled and old.</p>


<hr />

<em id="will">Written by Will Breedon. Will is a BA/BEc graduate from Monash University and an officer on the P2P National team, working with Victorian ACYA Chapters.</em>