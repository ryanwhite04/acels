{
  "title": "Announcing the ACYA-MTC Scholarship Winners",
  "author": "ACYA",
  "date": "2015-12-06T05:34:50+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Announcing the MTC Scholarship Winners",
      "type": "image",
      "src": "Announcing-the-MTC-Scholarship-Winners.png"
    }
  ]
}
Congratulations to Natalie Omond and Kate Kalinova, the two winners of this year's ACYA-MTC scholarship. The scholarship provides students with the chance to study at the National Taiwan Normal University’s Mandarin Training Centre (NTNU MTC) during their Winter Term (Dec 1, 2015 – Feb 26, 2016).

This is a fantastic opportunity for ACYA members to continue their Chinese language studies in one of Taiwan’s most prestigious educational institutions. The MTC has drawn participants from around the world, including former Australian Prime Minister Kevin Rudd.

In the spirit of ACYA, studying in Taiwan will allow students unique insight into the cultural ethos of a vibrant and diverse community, as well as the opportunity to forge cross-cultural connections and contribute to the Australia-Sino relationship.

We wish Natalie and Kate all the best in their studies!

<b>About this year's winners:</b>
<b>Natalie</b> is a student at the University of Adelaide. She holds a Diploma of Chinese and has attended both Beijing Foreign Studies University and Shandong University. 

<b>Kate</b> is a student at the University of New South Wales. She volunteered with AIESEC in China in 2014 and hopes to take introductory Chinese this semester. 

<b>About the MTC:</b>
The Mandarin Training Center (MTC), established in 1956, is affiliated with National Taiwan Normal University (NTNU) and is Taiwan’s oldest and best-known Chinese language institute. In cooperation with the Graduate Institute of Teaching Chinese as a Foreign/Second Language, the Graduate Institute of International Sinology Studies, the Department of Applied Chinese Languages and Literature, and the Department of Chinese Language and Culture for International Students at NTNU, the MTC is recognized as one of the world’s leading research and teaching institutions. 

For more information, please visit their official website: http://mtc.ntnu.edu.tw/mtcweb/index.php?lang=en
