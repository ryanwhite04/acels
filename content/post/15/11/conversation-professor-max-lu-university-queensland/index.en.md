{
  "title": "In conversation with Professor Max Lu, Provost and Senior Vice-President at the University of Queensland",
  "author": "ACYA",
  "date": "2015-11-28T10:52:21+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Max Lu",
      "type": "image",
      "src": "Max-Lu.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Max-Lu.jpg" rel="attachment wp-att-23878"><img class="size-medium wp-image-23878 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Max-Lu-217x300.jpg" alt="Max Lu" width="217" height="300" /></a>
This month, we spoke to Professor Max Lu (逯高清), the Provost and Senior Vice-President of the University of Queensland. An accomplished researcher with expertise in nanotechnology, materials science and chemical engineering, Professor Lu has over 25 years of teaching and research experience. He is a Thomson Reuters Highly Cited Scientist in both Chemistry and Materials Science as listed in the World's Most Influential Scientific Minds (2014). Following this interview, Professor Lu was appointed as the next President and Vice-Chancellor of the University of Surrey, commencing April 2016.

<strong>You came to Australia as a PhD student. What was life like in the 1980s as a Chinese international student?</strong>
I arrived in Brisbane on a PhD scholarship in 1987. When I first arrived, I spoke very little English and couldn’t understand the Australian accent. It took me almost a year to converse in English effectively. There was also a huge affluence gap between China and Australia. With the exchange rate then, the amount when I converted my RMB into Australian dollars totalled a $20 note. I remember the taxi fare to my accommodation costing $17, so I had $3 left and had to borrow money from friends until I got my PhD stipend!

At that time, there were only a small number of international students and a very small number from China. As an overseas student, the biggest challenge was to overcome the barrier of language and culture. Life was challenging in terms of being a long way from home and living in a new environment, as well as learning about and integrating into Australian society.

<strong>What does your role as Provost of The University of Queensland involve?</strong>
As Provost, I am the standing deputy to the Vice-Chancellor. I’m responsible for strategic, budget and infrastructure planning, managing faculties and institutes, and ensuring the academic and financial performance of these organisational units. The greatest part of my role is enabling the academic and professional staff in the facilities and institutes to do their best in delivering a great experience for our students, and to educate the next generation of global citizens and leaders in their fields.

<strong>Race Discrimination Commissioner Tim Soutphommasane believes that a ‘bamboo ceiling’ exists and blocks Asian migrants from positions of power. What is your opinion on this statement?</strong>
Although I don’t agree entirely, I do believe there are some elements of discrimination based on race and socioeconomic status. There still exists a small fraction of people who are ultra-conservative. However, in terms of the percentage representation of various ethnic groups in senior leadership positions, I believe it depends on many factors and not just others’ perceptions of race or colour. Many individuals in positions of power are incredibly hardworking, made an effort to assimilate into mainstream Australian culture, and took advantage of the opportunities that presented themselves. I believe it’s possible to achieve highly — you make your own luck, as long as you have the right attitude and are willing to work hard.

<strong>Do you believe China’s growing global economic power changed the higher education sector?</strong>
China’s economic growth has been miraculous and has greatly benefited Australia’s economy in terms of importing Australian energy and mineral resources. It’s also enabled many wealthy families to send their children overseas for education purposes, which has supported the Australian higher education sector tremendously. The higher education sector has taken advantage of China’s growth very well, and positioned itself to not only take on more Chinese students, but also to collaborate with Chinese institutions and companies in research and development.

<strong>What would your advice be to young Chinese-Australians who want to work in higher education or are seeking leadership positions?</strong>
First and foremost, you have to love what you do, find your passion, and set some high and long-term goals. Acquire appropriate qualifications first and then relentlessly pursue excellence in whatever you do. For those aspiring to work in higher education and towards leadership positions in either teaching or research, find fields in which you’re genuinely interested. To then be a leader, you must also have strong interpersonal skills in order to communicate effectively with others and inspire people to follow you towards a shared vision. A good leader should always have the interests of others at heart. Advancing the interests of the collective, whether it be an organisation or a university, should always be the top priority.

<hr />

This interview appeared in the November 2015 issue of ChinaBites. Read the original version <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=05cd0418c8">here</a>.