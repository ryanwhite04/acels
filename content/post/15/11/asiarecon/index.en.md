{
  "title": "AsiaRecon Tech Tour 2015",
  "author": "ACYA",
  "date": "2015-11-18T08:05:49+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Screen Shot 2015-11-18 at 6.51.01 PM",
      "type": "image",
      "src": "Screen-Shot-2015-11-18-at-6.51.01-PM.png"
    },
    {
      "title": "Asia-Recon-02-1024x768",
      "type": "image",
      "src": "Asia-Recon-02-1024x768.jpg"
    },
    {
      "title": "Screen Shot 2015-11-18 at 7.00.33 PM",
      "type": "image",
      "src": "Screen-Shot-2015-11-18-at-7.00.33-PM.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/11/Screen-Shot-2015-11-18-at-7.00.33-PM.png"><img class="aligncenter size-full wp-image-23045" src="http://www.acya.org.au/wp-content/uploads/2015/11/Screen-Shot-2015-11-18-at-7.00.33-PM.png" alt="Screen Shot 2015-11-18 at 7.00.33 PM" width="550" height="263" /></a>
<p style="text-align: justify;">AsiaRecon is a program set up to help build Australia’s engagement across the technology, innovation and entrepreneurial sector within Asia by promoting the transfer of people, ideas and capital. AsiaRecon recently completed their first Singapore-­China program which involved 15 Australian delegates spending 8 days in Singapore, Shanghai and Beijing.</p>
<p style="text-align: justify;">Delegates visited exciting technology and innovation hubs including: Innovation X, a global innovation platform backed by the Coca-­Cola Company; Baidu, China’s dominant search company; Fudan Tech Park, an innovation research centre within Fudan University; and Zhongguancun, which has been described as China’s Silicon Valley.</p>
<p style="text-align: justify;">Alexandra Meek from ACYA was fortunate enough to chat with Mathew Benjamin, one of the Co-­Founders of AsiaRecon. Mathew has previously worked for NAB Asia in Mumbai, was a consultant for the Australia China Business Council and also spent 5 years in the real estate funds management industry. Mathew was on the ACYA National Executive in 2014.</p>
<p style="text-align: center;"><b>What inspired you to start AsiaRecon? Why is technology, innovation and entrepreneurship important to Australia?</b></p>
<p style="text-align: justify;">I started AsiaRecon along with my Co-­Founder Jason Lim with the mission to increase Australia's engagement with Asia across the technology and innovation space. We thought that if we could show people, rather than tell people what's happening across Asia, there would be greater impetus for Australia to be more active on both fronts - "Asia engagement" and "Innovation".</p>
<p style="text-align: justify;">The inspiration for me was quite clear; Asia is going to remain a key driver of economic growth for a long time to come and 50% of the world’s GDP is forecast to come from Asia within the coming years. The change taking place across many parts of Asia is significant, not only are Asian economies transitioning but this change has important flow on effects. Within this, I also noticed significant trends. For example, in China, three of China’s four richest men are internet entrepreneurs. In India, frugal innovation, very different to the innovation happening in other parts of Asia, is having significant impacts on global value chains, particularly related to manufacturing ­- this is seen through companies such as Foxconn. In Indonesia, smart­phone penetration rates are some the highest in the region. On a regional level, mobile’s share of web in Asia was 4.5% in 2005 and by 2015 increased nearly ten fold to 43.3%. Looking then at the business models operating within these markets, for example telecommunications companies, you will see they have business models akin to technology companies.</p>
<p style="text-align: justify;">Each of these countries are liberalising and benefiting from capital inflows from venture capital and private equity, and attempts to commercialise IP are increasing significantly. At the same time, government within the region have a heavy top­down emphasis on innovation and entrepreneurship policy, greatly supporting and accelerating growth.</p>
<p style="text-align: justify;">Considering all of this, it became very clear why we should be pursuing this initiative. My finance background also saw this as a great opportunity to get involved and learn about the drivers behind Asia’s push to move up the value chain across the areas of technology and innovation.</p>
<p style="text-align: center;"><b>The tour involved visiting a variety of different organisations and companies. What were some key insights that delegates were able to take from these diverse offices?</b></p>
<p style="text-align: justify;">After meeting with over 35+ companies (including Google, Baidu, Alibaba, Sequoia Capital and others) and over 220+ investors, entrepreneurs and thought leaders, it became obvious that Australia wasn’t the only country within the Asia Pacific region wanting to become an “innovation nation”. This was a very timely observation, considering we now have a government that has been reinvented around a need to increase productivity and innovation in Australia.</p>
<p style="text-align: justify;">In Singapore we witnessed a strong level of policy coordination between industry, government and startups. Also, understanding the importance of creating communities and contributing to the infrastructure within innovation ecosystems, the Singaporean Government made significant investments in creating “hard infrastructure” or “shells for innovation”. We saw this at BASH, Singapore's biggest all­-in-­one startup facility spanning across more than 25,000 square feet. BASH allows the tech building community to collaborate across the entire value chain ­- accelerators, investors, incubators, and entrepreneurs. A shell for innovation, facilities like BASH are unique in fostering collaboration and accelerating the network effect. We also learnt that the Singapore Government provides up to 77 different schemes and grants, incentivising academics to commercialise, kids to code and students to create companies. I should note, that this is both good and bad, as some policy, like all policy, isn’t as effective as policy advisors had forecast or politicians had hoped. Everyone in the space, not just in Singapore, is admittedly trying crack the policy code.</p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/11/Asia-Recon-02-1024x768.jpg"><img class="aligncenter size-full wp-image-23041" src="http://www.acya.org.au/wp-content/uploads/2015/11/Asia-Recon-02-1024x768.jpg" alt="Asia-Recon-02-1024x768" width="550" height="412" /></a>
<p style="text-align: center;"><b>Now that the tour has concluded, what does this mean for the delegates? How will the relationships formed benefit the innovation and technology scene in Australia?</b></p>
<p style="text-align: justify;">We are aware that our current Prime Minister Malcolm Turnbull, who was Minister for Communications at the time of the tour, and who sent two senior staffers along from the Department of Communications has been briefed on the tour and its findings. Along those lines, policy advisors and representatives on both sides of politics are increasing their familiarity with the innovation and technology scene within the Asia Pacific region -­ which is exciting!</p>
<p style="text-align: justify;">We know a number of investors and entrepreneurs who have benefited immensely from the trip, unfortunately I can’t go into detail here for confidentiality reasons. Delegates benefited in a number of ways from meeting venture capitalists and angels investors, to meeting key groups within the ecosystem to explore strategic partnerships.</p>
<p style="text-align: justify;">We don’t underestimate the impact starting a small initiative like this has within Australia. Awareness alone can be a powerful catalyst. We are seeing this unfold now. Significant change takes time, but in the meantime look out for Australian investors, entrepreneurs, corporatations and government making a more direct and coordinated approach to Asia engagement across the technology and innovation space within Asia ­- there’s a lot of stuff in the pipeline!</p>
<p style="text-align: center;"><b>For you personally, what was the highlight of the tour?</b></p>
<p style="text-align: justify;">We had diversity of perspective not only from our host and partners, but also from the delegates that participated. I’d have to say this was an incredible feature of this trip. Entrepreneurs, investors, corporates, accelerators and incubator managers, government representatives and policy advisors, universities and research institutions all participated in the knowledge exchange. There are very few platforms that offer this level of diversity and as such the applied learning opportunity was unique.</p>
<p style="text-align: justify;">Also, more personally, another highlight for me was reflecting on the trip with Co-­Founder and friend Jason Lim on the Great Wall at Mutianyu. I first met Jason in the Quad at the University of Sydney in 2014. I was the Project Manager for the ACYA National Conference in 2014 and invited him to speak. After the event Jason Lim and I spoke about the need to increase engagement with Asia in the technology and innovation space. He had mentioned that he was exploring a tech tour as an idea with the goal of increasing Australia's engagement in Asia's tech and innovation space. I was very intrigued, so we decided we would try to execute the idea.</p>
<p style="text-align: justify;">Neither of us knew what would happen as a result. Looking back 14 months, I consider myself very lucky to have had the opportunity to work with Jason on this. It just goes to show how important organisations like ACYA are at facilitating engagement between young Australians and Chinese interested in further understanding each other's countries.</p>


<hr />

<i>More information about AsiaRecon can be found on their <a href="https://web.facebook.com/asiarecon/?fref=ts">Facebook page</a> or <a href="http://www.asiarecon.com">website</a></i>