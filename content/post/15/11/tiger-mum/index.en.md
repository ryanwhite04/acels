{
  "title": "The Tiger Mum Phenomenon",
  "author": "ACYA",
  "date": "2015-11-06T02:12:52+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Unknown",
      "type": "image",
      "src": "Unknown.jpeg"
    }
  ]
}
<p style="text-align: justify;">The ‘Tiger Mum’ phenomenon is a necessity borne of uncertain economic standing and a middle class with far less places than people who want to enter it. These conditions drive an ultra-competitive clamouring for class ascendency beginning in early education, spurred on by anxious parents aspiring to deliver their children a secure future. Conversely, the nonchalant attitude to parenting promulgated in the West is a product of stable societal wealth and empowered by the safety net of a welfare system and generous minimum wages. This assuredness affords Westerners the luxury of gentle, worry-free parenting and jettisons fears of a lifetime of manual labour and subsistence income for their offspring.</p>
<p style="text-align: justify;">Although difficult to understand in the cosseted sphere of the developed world, ‘Tiger Mum’ parenting emerges from a deep love of one’s children and a desire to see them freed from the toilsome lives of the generation before. Unlike most parents in affluent nations, Tiger parents have witnessed first-hand the struggle to extricate oneself from poverty when not equipped with the tools of a strong education and reputable degree. To allow their children to slide into an identical rut before they are mature enough to understand the lifelong consequences could only be considered a form of neglect; thus the fanatical push for outstanding academic results from an early age. These results are crucial in many parts of the world, namely China, to squeeze through the harsh filter of university entrance exams in the battle for a qualification. Unlike in Australia, where bachelor degrees are becoming the norm and are no longer a ticket to employment, Chinese tertiary education is less abundant and graduates depart with the ability to bypass unvalued and unskilled work. Where the stakes are so high and the odds are so low, this style of parenting is the sole option to fulfil the goals all parents share: a secure and comfortable future for those whom they raise.</p>
<p style="text-align: justify;">Common critiques of Tiger parenting overlook the roots of its existence, however, the concept suffers when carried by diaspora to the developed world. In such circumstances, forceful education from birth is not required to ensure wellbeing in the years ahead. Rather, incessantly pushing children to succeed appears as a one-eyed focus on money and status, not relishing the fulfilling and leisurely life afforded to members of well-off societies. Indeed, the stability which Tiger parenting strives to impress is in many ways attained by virtue of living in such places, negating the need to push children onwards, potentially damaging the relationship with their filial figures. This cultural disconnect is also present in schoolyard relationships, wherein peers are not saddled with the same high expectations. Children of Tiger parents thus bridge the crevice between approaches of the predominate culture and their parents, not an insignificant burden while navigating schoolyard social structures. However, children’s efforts fuelled by Tiger parenting are an investment. Despite initial challenges and reactionary resentment, children are seldom ungrateful when the fruits arise and are left with a stronger sense of discipline and mature understanding.</p>
<p style="text-align: justify;">Tiger parenting must be viewed in the context in which it emerged. Western critiques are prone to ignoring the origins of the parenting style, while condescendingly assuming all people are blessed with social safety nets. Although Tiger parenting loses relevance in affluent contexts and indeed gives too great esteem to social status and material success, it is naïve to expect parenting habits to change in an instant following generations of enforcement. While Tiger parenting may not be the perfect solution, it aims to address the economic insecurity and poverty endemic in this imperfect world. The love expressed through that should not be forgotten.</p>


<hr />

<i>Written by Jonathon Tree (恒明俊) for the ACYA Victoria Bilingual Competition 2015. Jonathon was the first runner up in the advanced level. </i>

ACYA VIC BLC is always on the look out for dedicated individuals who want to make a difference within the Australia-China space. If you are interested in joining the ACYA VIC BLC team and are currently studying at one of the five Victorian chapter universities, please email your resume and cover letter with full contact details to acyavic.blc@gmail.com.

For more information about ACYA VIC BLC, please visit the ACYA VIC BLC <a href="http://www.facebook.com/ACYAVICBLC">Facebook page</a> or <a href="http://www.acyavic-blc.com">website</a>