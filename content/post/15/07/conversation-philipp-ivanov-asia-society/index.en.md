{
  "title": "In conversation with Philipp Ivanov, CEO of Asia Society Australia",
  "author": "ACYA",
  "date": "2015-07-28T11:02:44+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Philipp Ivanov",
      "type": "image",
      "src": "PI-IMG_0822.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/PI-IMG_0822.jpg" rel="attachment wp-att-23897"><img class="size-medium wp-image-23897 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/PI-IMG_0822-251x300.jpg" alt="Philipp Ivanov" width="251" height="300" /></a>

This month, we interviewed Philipp Ivanov, CEO of Asia Society Australia, a leading social enterprise dedicated to advancing connectivity between Australia and the Indo-Pacific region and a part of the Asia Society’s global network of 11 centres. Prior to this position, Philipp was a policy officer and Manager of the Australia-China Council at the Department of Foreign Affairs and Trade, where he was one of the principal authors of the Australia in the Asian Century - China Country Strategy. Philipp has also held managerial and advisor roles at the University of Sydney, La Trobe University and International Education Network. He received classical Chinese language, economy and history training at the Far Eastern National University in Russia, studied in Jilin and Liaoning Normal universities in China, and holds a Master of Educational Leadership and Management from RMIT University in Australia.

<strong>I understand you received class Chinese language, economy and history training from the Far Eastern National University in Russia. What was it about China that first sparked your interest?</strong>
The first aspect is the geography and the history. I grew up in Vladivostok, Russia, which is a four-hour drive from the Chinese border, a one-hour flight to Tokyo, and a one-hour drive to the North Korean border. When we were in school, it was normal to study an Asian language, similar to how students will study English in China. As I grew up in the 80s and early 90s, we all knew China was a sleeping giant, particularly since the Chinese economic reform had just commenced in the late 80s.

The second aspect is my interest in Chinese philosophy during school. I went for a three-month exchange in my fourth year of university to Jilin University. I finished my degree in Russia, and then I went again for a year as a postgraduate student to Dalian. Afterwards, I moved to Shenyang where I lived and worked for around six years. I think I can consider myself an honorary “东北人” now (northeasterner).

<strong>How was your experience working and living in China?</strong>
When you study China, you look at it as an outsider. Living in the country allows you to really experience what people live through every day. It helped me understand what life is about in China — I don’t like the word ‘culture’ sometimes, because it’s misused very often. I prefer to use the term ‘life’, as I think it better encompasses the culture and daily experiences.

I worked a few different roles in China but one that was very valuable, and probably the most difficult, was my work in Shenyang for two years, from 2000 to 2002. I didn’t get to know many expat communities. I watched the news every day and did the same activities as my Chinese colleagues. It was a different approach to thinking, work and daily experiences. I also spent a lot of time by myself as I used to travel to some rural locations for work. It was a formative experience for me, and my years in Shenyang a highlight of my six years in China. Although when I was living there, I didn’t think I was having a good time! It could be a bit lonely and the work could be challenging. However, I learned most of the things I know about China in those two years and it provided a very valuable foundation.

My interest in politics started when I was in Dalian in 1999, during my first semester at a Chinese university. At the time, Bo Xilai was the mayor of Dalian and he visited our campus. He gave a speech, shook our hands, and introduced himself to all the international students. He was one of the very few politicians in China that had real charisma. Before I met him, I was more interested in Chinese philosophy, geography and culture. However, Bo Xilai stood out and made me realise I had a preconceived idea about how politics worked in China. He was a very visible mayor, with his introduction of the annual fashion festival in Dalian and ambition to transform Dalian into the cleanest city in China. It sparked in me an interest in Chinese politics and policies.

<strong>After living in China, you then moved to Australia. Why Australia and Australia-China affairs?</strong>
I was already working for a consortium of Australian universities operating in China, creating and developing programs where students could participate in offshore education. I was also looking to study my Masters in Educational Leadership at RMIT University. In addition, I met my wife in Dalian, who was Australian — it all came together. When I started working for an Australian company in China, I realised what an incredible force global education was and how education has become such an internationalised sector. Through that, I came to understand Australian politics and economy as international education is a huge part of Australia’s international image and trade. The more I worked in Australia, the more I became involved in the Australia-China relationship.

<strong>You’ve been in your current role with the Asia Society since January. What drew you to this role, and what goals are you hoping to achieve?</strong>
It was an intellectual offer that I couldn’t refuse. Although I had no immediate plans to leave DFAT, I felt that this was an incredible opportunity as I have always been interested in helping social enterprises grow and expand. Asia Society was originally founded by the Rockefeller family in 1957 and is now a global organisation with 11 centres across the world, arriving in Australia in 1997. I want to take Asia Society Australia to a whole new level, and the time and the environment is right. In fact, we need many more institutions to build up Australia’s engagement capabilities with Asia because I think we are behind. We need to be more connected with Asia through different channels, whether through arts, tourism, business or politics. There’s a real need for these types of institutions to drive engagement that sit outside political or commercial agendas.

<strong>What vision and goals do you have planned for Asia Society?</strong>
We are implementing a five-year strategic plan, ‘<a href="http://asiasociety.org/australia/asia-society-australia-2020">Asia Society Australia 2020</a>’. The ambition for the plan is for Asia Society Australia to be Australia’s leading and most innovative platform for engagement with Asia, across business and policy, the arts, and education.

First, we are looking to grow membership. We have a strong network of leading Australian companies but we are still aiming to grow this network. If you look at Australian life, not a single sector is left untouched by the rise of Asia: tourism, hospitality, viticulture, agriculture, everything. We need to grow our network and engage more companies, education, government and cultural institutions to become part of the Asia Society family.

Second, we want to diversify our programming and build our thought-leadership capacity. For example, we launched a new platform called ‘Insights’, which will bring different our and our members’ and partners’ insights and thought-leadership elements together. Next year, we will also launch a platform for the younger generation and emerging young leaders — not going to say too much but stay posted.

Third, we have an ambition to create a physical space, a hub, in Sydney that celebrates the Australia-Asia connection. This will be a long-term process but we have incredible examples of how it was done in New York, where Asia Society began, and our largest centre in Hong Kong, which is an incredibly creative and beautiful centre. Our goal is for people to open a newspaper and read about what’s happening at Asia Society this weekend. It could be a movie or a food event, an exhibition or policy dialogue. It will be a space that brings all these elements together.

So those are our ambitions: to grow our network, to diversify and be innovative in offerings, and to create a space in five years time, hopefully sooner. Sydney deserves this type of place and we need an institution that celebrates Asia-Australia connections. Sydney is our gateway into Asia Pacific and it’s Asia Pacific’s gateway into Australia. We think we’re in the right position to start designing this hub but of course, we’ll need partners and support.

<strong>I think it’s a great idea to have such a positive and welcoming space that celebrates Australia’s relationship with Asia. There’s been a great deal of fear and criticism recently in the media, particularly around China and the free-trade agreement.</strong>
I understand there are political and security anxieties but we should never lose sight of the incredible economic opportunity that China offers. I think it’s important for the Australia-China relationship to continue the pace that current and past governments have set, from Gough Whitlam to Tony Abbott. Despite their different takes on political and economic issues, all Prime Ministers had one thing in common: they were always positive about China. I think it’s fundamentally important for governments to continue to be optimistic about the incredible opportunities China’s emergence as a global power is presenting to the world.

For this approach to continue, we need to continuously innovate and look for ways to build this relationship at different levels and continuously set goals, not just managing the relationship on a day-to-day basis. Ten years ago, we started negotiating a free-trade agreement and last year we concluded it. There were ambitions to have a closer relationship on a political level, which led to meetings between the Foreign Affairs Minister and the Defence Minister, as well as a strategic partnership where the Prime Minister and the President or Premier meet every year.

It’s incredibly important to continuously set new targets and goals, such as “What do we want to do with China?” “Okay, we’ve done the free trade agreement, great, what’s the next step?” It’s important to have an ambition and concrete goals. As stated by Kevin Rudd in his <a href="http://belfercenter.ksg.harvard.edu/publication/25237/summary_report.html">report</a> on the future of US-China relationship, it’s essential to continue to set goals and work on initiatives and projects that are practical and can bring the people and countries together. It could be across different areas, from arts, to politics, defence, or even a joined natural disaster relief program, as recommended by Linda Jakobsen ('<a href="http://www.lowyinstitute.org/publications/australia-china-ties-search-political-trust">Australia-China Ties: In Search of Political Trust</a>').

<strong>Do you have any advice for students and young professionals looking for a career in Australia-China relations?</strong>
I think it’s important not to give up and get distracted by what is often seen as lack of appreciation for people with Chinese language skills or a Chinese background. It’s important to persevere and be a champion of the Asia-Australia connection. Even though a lot of the time, these skills are not recognised in Australian workplaces and people don’t look for these skills when they read CVs, they will. Sooner or later — most likely sooner — because Asia is so fundamentally important and these skills will be in demand. It’s important not to get distracted by temporary lack of awareness of the importance of these skills and background.

<hr />

This interview was conducted by Olivia Gao for the July 2015 issue of ChinaBites. Read the original version <a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=8b75b0dc36">here</a>.

&nbsp;