{
  "title": "Announcing the Australia China Emerging Leaders Summit",
  "author": "ACYA",
  "date": "2015-07-02T02:42:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS Webpage banner",
      "type": "image",
      "src": "ACELS-Webpage-banner.png"
    },
    {
      "title": "618x358_acelsclimbhigh",
      "type": "image",
      "src": "618x358_acelsclimbhigh.jpg"
    }
  ]
}
This year, the Australia-China Youth Association (ACYA) is proud to be collaborating with the brightest minds from the Australia-Chinese space to deliver the Australia-China Emerging Leaders Summit (ACELS).