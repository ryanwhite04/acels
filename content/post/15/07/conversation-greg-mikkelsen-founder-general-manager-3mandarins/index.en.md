{
  "title": "In conversation with Greg Mikkelsen, Founder and General Manager of 3mandarins",
  "author": "ACYA",
  "date": "2015-07-28T11:58:04+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Greg Mikkelsen",
      "type": "image",
      "src": "Greg-Mikkelsen.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Greg-Mikkelsen.jpg" rel="attachment wp-att-23928"><img class="size-medium wp-image-23928 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Greg-Mikkelsen-242x300.jpg" alt="Greg Mikkelsen" width="242" height="300" /></a>

This month, wet sat down with Greg Mikkelsen, who is the Founder and General Manager of 3mandarins, a consultancy which helps businesses tap in to the Chinese-speaking market. An ACYA alumni himself, Greg shares his exciting success story with AustraliaBites readers.

<strong>What is 3mandarins and how did you come up with the concept?</strong>
3mandarins helps Australian businesses break into the Chinese market. Our mission is to basically assist companies in terms of developing a brand and a marketing message that’s going to resonate with the Chinese audience. So there’s a lot of opportunities for Australian companies to sell products or services to the Chinese market, whether that’s domestically here - for example in property or tourism, hospitality - or selling products into China, so maybe exporting products over there. But what we’ve identified as lacking in the Australian market is this expertise in terms of branding, marketing or cultural awareness of what it is that Chinese customers are looking for. So that’s where we fill that gap.

<strong>What’s a typical work day for you at 3mandarins?</strong>
A large part of what we do is to network with businesses who are working in this space. So we’re attending networking events on a weekly basis, three or four networking events a week. Connecting with Australian companies, that are looking to break into the Chinese market, or Australian companies who already have and are operating in the Chinese market and are doing so quite successfully. Also Chinese companies that are also looking to engage with Australia, whether they’re looking for investment opportunities or if they’re looking to immigrate to Australia.

In terms of other things, day-to-day meeting with clients; we help clients with their strategy. So we’ll sit down with clients and help design a China engagement strategy where we work through different options they could choose in terms of the marketing platforms, branding messages that they might want to use or other companies they might want to forms strategic partnerships with.

<strong>How did you find yourself creating 3mandarins?</strong>
It wasn’t long that I was out of uni. I kind of had always wanted to do something like this but I didn’t know it would take the form of 3mandarins exactly but right from the beginning I guess I had always wanted to work as a consultant (it’s not a very good word) helping businesses set up or shop in China.

3mandarins took its current form because I realised that there’s a lot of potential for Australian companies locally, dealing with inbound Chinese customers. There’s plenty of opportunity right here in Australia that a lot of Australian companies can capitalise on. The business started to focus on those kind of customers, rather than big grand expansions into China, which is probably something we’d like to get onto down the track but not currently what we’re doing.

<strong>How did you get so involved in Chinese culture?</strong>
I guess growing up, I had quite a bit of exposure to Asian culture, because I learned Japanese at school and as part of that, my parents thought it’d be a good idea to have exchange students come stay with us. We kind of had a bit of an international house. Then I spent a bit of time in Japan after school and I also lived in Thailand. And so I got quite familiar with Asian culture and it was when I started university that I thought I should learn an Asian language and Chinese kind of became the smart option; the business-savvy side of myself said ‘choose Chinese’, even though I thought it would be ridiculously difficult. So I did it and just really liked it and just kept going through university, and picked up a lot of internships along the way.

I was always motivated to improve my language skills and that was my motivation throughout university. So I looked for opportunities to use my Chinese outside of the classroom. I was also studying law at the same time; I worked for a Chinese law firm in Sydney as a paralegal while I was at university, dealing with Chinese clients, speaking with Chinese clients on a daily basis. I did an internship in China in Wuhan as a translator at a university there in their environmental law department. So I kind of tried to use Chinese and law together as much as possible. Even when I was studying in Denmark, I worked for a Danish international law firm that had a China desk, so I worked on their China desk from Denmark and that was what sustained my interest and I was able to utilise Chinese. And then it just got to the point that it was what I did – I became the China guy! There’s a few of them out there, I don’t really like being known as that. But people start to identify you as that, probably because it’s a rare or niche thing to have spent so much time learning and specialise in and so people value that.

<strong>How has ACYA prepped you or shaped you for the career you are in now?</strong>
Prior to my involvement with ACYA, I only saw Chinese studies as a language learning exercise. Whilst I had in the back of my mind that there’d be career opportunities, they never presented themselves in the classroom or in different areas like that. It was only once I’d joined ACYA that I’d started to network with people in the Australia-China space. And that’s when I really saw the potential to utilise Chinese, and by that stage I’d already been learning Chinese for three years. I had the language skills up my sleeve by that time and I was able to utilise them. And it opened doors in the Australia-China space to collaborate with other people, work on projects. Not just the language skills, but having spent time in China also helped. So ACYA really opened doors in terms of opportunities to work on projects such as the HOME Project, where you would then meet with members of government or people higher up in, so the university structures, people from commercial space, meeting with different businesses that focused on China and kind of starting to network there. That’s when I started to realise that there was an Australia-China space; I became aware of its existence, and thought that’s where I needed to be operating in a position myself. And when I started 3mandarins, my very first thing to do was to join the Australia-China Business Council (ACBC), because that’s the space for businesses - just as ACYA is the space for students. The ACBC is really the space for businesses that want to be involved with that space and it’s a really great networking opportunity.

<strong>Any tips for young people interested in engaging with China?</strong>
I think firstly, you’ve got to be willing to step out of your comfort zone. Probably a cliché thing, but don’t be afraid to put yourself in boring situations or awkward situations. There’s been countless times along this journey where I’ve just gone: “what am I doing at this place at this time? Why me, why am I doing this?” It’s just weird or not usually something that I would find interesting. But it kind of all becomes part and parcel.

Of you’re looking to get internships with businesses, put yourself out there - just send them emails. I sent so many emails to different companies that weren’t hiring, just saying: “Can I work for you? These are my skill sets, this is what I’ve done”. Some will buy it, they’re not all going to be takers, but you’ve got to cast the net wide and hope you get an opportunity and go with it. You kind of collect all these experiences and you start to gain more perspective on where you want to go, what direction you want to take and you’ve increased your network. You also got experience to show your competence, which will open up more doors for you in the future.

When you’re dealing with another culture or another people, you just have to take what comes and go with the flow. Be a sponge, take things in and don’t take yourself too seriously. Get out there and just put yourself out there. You don’t necessarily have to learn Chinese, but if you do learn Chinese, look for opportunities to use it with people, chat to them, and use every opportunity.

<hr />

This interview first appeared in the July 2015 issue of AustraliaBites. Read the issue <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=e6850dcfd1">here</a>.

&nbsp;