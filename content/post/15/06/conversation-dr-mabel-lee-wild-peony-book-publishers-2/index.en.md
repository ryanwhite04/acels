{
  "title": "In conversation with Dr Mabel Lee, founder of Wild Peony Book Publishers and translator of Nobel Prize-winning novel 'Soul Mountain'",
  "author": "ACYA",
  "date": "2015-06-28T11:01:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Mabel Lee",
      "type": "image",
      "src": "Mabel-Lee.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/Mabel-Lee.jpg" rel="attachment wp-att-23883"><img class="size-medium wp-image-23883 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/05/Mabel-Lee-300x300.jpg" alt="Mabel Lee" width="300" height="300" /></a>

Born in country New South Wales, Dr Mabel Lee grew up speaking Cantonese and English with equal fluency. Her interest in Chinese cultural affairs were developed and cultivated during her tertiary studies at the University of Sydney, where she obtained her BA Hons I and PhD in Chinese Studies. She is best known for her translation of Gao Xingjian's novel, <em>Soul Mountain</em>.

This translation took Mabel seven years, with an additional two years to find a publisher for the book in Australia. Three months after publication, Mabel found herself at the centre of international recognition and acclaim when Gao Xingjian was declared as the Nobel Laureate for Literature in 2000. Today, she is one of one of Australia's leading authorities on Chinese literature and translation, the founder of Wild Peony Book Publishers, and Adjunct Professor of Chinese Studies at the University of Sydney.

<strong>I read that when you began your undergraduate studies at the University of Sydney, you only knew 100 Chinese characters. What or who sparked your interest in studying Chinese culture—from your PhD in late-Qing dynasty intellectual history to your later translation work?</strong>
At Parramatta High School, we had a couple of history lessons on China’s failure to cope in its dealings with the industrialised Western nations and Japan. I was interested in why the Chinese intellectual elites of the 19th century had psychologically resisted institutional change, and why the Chinese since those times had left their villages to work in countries such as Australia and the USA where they were despised and subjected to laws that discriminated against them. I could write 100 Chinese characters when I enrolled in Chinese Studies at the University of Sydney, but I needed to learn 10,000 characters so that I could read the Chinese writings of those times. Little of these materials existed in translation, so I had to translate or summarise these for my PhD research. I also needed to learn all these characters to pass my undergraduate courses.

<strong>How did you meet Gao Xingjian and come to translate his novel, Soul Mountain? What drew you to his work?</strong>
It was the poetic language of Gao Xingjian’s novel Soul Mountain that immediately attracted me as I leafed through the book while I chatted with mutual friends in his Paris home. This was our first meeting but before leaving, I asked if he had a translator for the novel and if he wanted me. However, because literary translation is not regarded as an academic activity in Australia, as a full time academic it was hard to find time to work on the translation, and even harder in those times to find a publisher. We signed a contract with HarperCollins Australia in 1999, and the English edition was published in July 2000. When it was announced in October 2000 that Gao Xingjian had won the Nobel Prize for Literature, we were still looking for a US or UK publisher.

<strong>Have attitudes changed towards translated Chinese literature, given China’s growing global significance? If so, how?</strong>
Interest in translated Chinese literature is growing rapidly in the Anglo-speaking world but should not be related to China’s growing global significance. To do so at present would favour negative depictions of China. However, a much more encouraging development seems to be emerging. Literary translation from various languages has always been valued throughout Europe and in the countries of Asia. Finally literary translation is coming to be valued more in the Anglo-speaking world, and points to a focus on literary excellence of the original work and the translation. This will take forward good literary works and their translations, whether it is originally created in Chinese or another language.

Indicators in the region are the NSW Premier’s Prize for Translation rising from $5000 in 2001 to $30,000 in 2015, the establishment in 2015 of a Translation Prize by the Australian Academy for the Humanities, and the focus on literary translators at the 2015 Singapore Writers Festival.

<strong>What advice do you have for students who are considering Chinese Studies at university?</strong>
Over the past half century, Chinese Studies in Australia has lost its language-based Chinese literature courses to Chinese film and Chinese media studies courses. This trend is also reflected in the universities of China and the rest of the world. Undoubtedly there will be some films that will endure because of their relevance and inspiration to humanity for many generations, but ensure that the comparative difficulty of learning to read widely in Chinese literature (past and present) does not prevent you from reading works by the greatest reflective minds of China, and experiencing the full expressive beauty of the Chinese language of their authors.

<hr />

This interview was conducted by Donna Lu. It featured in the June 2015 issue of ChinaBites –read the original version <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=576858908e">here</a>.