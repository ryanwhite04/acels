{
  "title": "ACYA-RBS IMBA Scholarship Winner Announced!",
  "author": "ACYA",
  "date": "2015-06-30T10:24:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Renmin",
      "type": "image",
      "src": "Renmin.png"
    }
  ]
}
The Australia-China Youth Association (ACYA) and the Renmin University of China Business School (RBS) are proud to announce that the ACYA-RBS International MBA Scholarship has been awarded this year to a student of the ANU, Robert Bin Yang. 

A full interview with Robert can be found here: http://www.acya.org.au/2015/06/acyarbsinterview/

Established in 2014, this scholarship was the first of its kind to be established between ACYA and a mainland Chinese university.  ACYA was thrilled to be able to provide this opportunity to our members again this year. The Scholarship provides an ACYA member with a full scholarship over the 2-year degree.

Through applying for this scholarship, applicants will also be considered for government and city based scholarships on offer at RUC. RBS International MBA students also have the opportunity to undertake exchange with multiple other prestigious universities around the world.

For more information on the scholarship and the International MBA program, please visit http://mba.rbs.org.cn/en/, or the RBS IMBA Frequently Asked Questions page: http://mbaen.rbs.org.cn/imba-frequently.php.

General enquiries about ACYA scholarships should be directed to the ACYA National Education Director Nathania Tangvisethpat (education@acya.org.au) 

ACYA once again thanks all those who applied this year and wishes all its members the best of luck in applying for next year!