{
  "title": "China Matters Youth Policy Meeting",
  "author": "ACYA",
  "date": "2015-06-02T06:16:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Screen Shot 2015-06-02 at 3.26.05 PM",
      "type": "image",
      "src": "Screen-Shot-2015-06-02-at-3.26.05-PM.png"
    },
    {
      "title": "Screen Shot 2015-06-02 at 4.03.31 PM",
      "type": "image",
      "src": "Screen-Shot-2015-06-02-at-4.03.31-PM.png"
    },
    {
      "title": "Screen Shot 2015-06-02 at 4.17.22 PM",
      "type": "image",
      "src": "Screen-Shot-2015-06-02-at-4.17.22-PM.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/06/Screen-Shot-2015-06-02-at-3.26.05-PM.png"><img class="aligncenter size-full wp-image-22313" src="http://www.acya.org.au/wp-content/uploads/2015/06/Screen-Shot-2015-06-02-at-3.26.05-PM.png" alt="Screen Shot 2015-06-02 at 3.26.05 PM" width="502" height="245" /></a>
<p style="text-align: justify;">I was fortunate enough to attend the inaugural <a href="http://chinamatters.org.au">China Matters</a> Youth Policy Meeting held on the 6th May 2015 at the University of Sydney. The event was co-hosted by Ms Linda Jakobson, Founding Director of China Matters, and Professor Kerry Brown, Director of the University of Sydney China Studies Centre. The meeting invited 36 leading postgraduate, honours, undergraduate students and young professionals to engage in rigorous discussion of the issues concerning Australia’s relationship with China. The conversations took place under Chatham House rules and there was a fantastic amount of interesting ideas articulated, especially around topics such as the effects on the Australia-China relationship of Australia’s transition to a post minerals economy, as well as the implications for Australia’s trade security with China’s maritime activities in Asia-Pacific waters.</p>
<p style="text-align: justify;">The general discussion pertained to two questions. What does China want from the region? What does Australia want from China? Participants were asked by the panelists to form one or two policy recommendations for the Australian government regarding how we should engage with China in the context of China’s growing geopolitical importance and influence in our region.</p>
<p style="text-align: justify;">Some of the core recommendations made included the need for the Australian government to formulate a more coherent strategy for engaging in China, and to build more substantive relations with other regional ‘middle powers’ such as South Korea.</p>
<p style="text-align: justify;">Another interesting issue that was raised was the idea that from the perspective of the ordinary observer, it does not appear that the Australian government has a clear ‘plan’ when it comes to our engagement with China. This was responded to with the claim that every action Australia takes in engaging with China is heavily considered by foreign affairs bureaucrats. Others in the room stated that they believed it was clear that Australia’s strategy towards China is one of ‘hedging’ which was aptly summarised as ‘make money, rely on the US’.</p>
<p style="text-align: justify;">I raised the point that is seems that China has so far failed to assume some of the responsibilities on the global stage that come with being an economically dominant power. Compared to major economies such as the US, EU and Germany, China has distanced itself from some of the thornier international issues. The primary security concern that is articulated to the Australian public from political leaders pertains to the rise of the Islamic State. We have also heard concerns about Russian aggression in violating Ukrainian sovereignty, especially in the wake of the MH17 crash in 2014. On these two issues however, China is not very vocal. Whereas the US, Japan and European nations have enforced sanctions on Russia and have been steadfastly resolute in their opposition to the events in Ukraine, China has refused to intertwine itself in this diplomatic dispute. The implications this has for the Australia-China relationship is that Australia would want China, in the same way that its other close economic partners such as the US and Japan do, to share its view on international security issues and be an active supporter of its actions.</p>
<p style="text-align: justify;">In reflecting on the Australia-China relationship I believe it is important that Australia play a cautiously diplomatic role in its relationship with China. Our relationship with China will always be strongly related to our economic interests, and for this reason there is a lot at stake. However, this also does not mean that we should compromise on our core values as a democratic nation that prioritises individual freedoms and the upholding of principles of international law. In these areas it is no secret that China has a poor reputation. I personally believe there is scope in our relationship with China to send a stronger message that we expect China to adhere to international law and progress towards greater transparency, democracy and freedom in its domestic affairs.</p>


<hr />

<em id="Tim"> Written by Timothy Sullivan (苏立文). Timothy is completing the Bachelor Arts/Law at the University of Sydney. He has previously studied at Peking University, Beijing and Fudan University, Shanghai.</em>

More information on China Matters and the Youth Policy Meeting can be viewed <a href="http://chinamatters.org.au/?page_id=525">here</a>.