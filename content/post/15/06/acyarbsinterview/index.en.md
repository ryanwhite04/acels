{
  "title": "Interview with Robert Bin Yang, recipient of the 2015 ACYA-RBS IMBA Scholarship",
  "author": "ACYA",
  "date": "2015-06-30T10:22:39+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Robert Bin Yang is a young Chinese-born Australian and recipient of the ACYA-RBS International MBA scholarship at Renmin University. Robert migrated to Australia when he was eight to live in Canberra, obtaining his Bachelor's degree in Asia-Pacific Studies, and Master's Degree in Business from the Australian National University. 
He has taken advanced Chinese courses in both Peking and Tsinghua University, and was employed by the Canberra State Government for six years in a number of management and administration roles.
ACYA National Education Officer Declan Fry had the chance to interview Robert on his views of the Australia-China relationship and how he plans to contribute to fostering Chinese-Australian ties through this unique opportunity.

<b>1. How did your interest in the relationship between China and Australia begin?</b>
 
As a Chinese-born Australian, I strongly identify with my homeland; therefore, China has always been a great interest of mine. However, in struggling with my identity in my childhood (was I Australian or was I Chinese?) my interest in China and having a future in China would remain unstirred in me until a much later date. It wasn't until I personally shook the hand of past Prime Minister Kevin Rudd (a well-known Sinophile) that the passion deep within me finally awoke. When I saw importance Mr. Rudd placed on China, it once again sparked my interest in my ancestral homeland. I felt a calling home. 

As a result of his influence, I then went on to pursue a similar degree to his at his alma mater. In this way, my dream of becoming a part of the bridge that connects China and Australia began. This is significant because Australia (as a colonised country) has a 200 year history and Chinese people have been part of Australia since the gold rush of the 1850s, meaning that Australia and China have had a long and productive history together. It is in wanting to continue this relationship that I see a role for my future career and ambitions.
 
<b>2. How did you find out about the Australia-China Youth Association, and what suggestions would you have for future ACYA-RBS IMBA Scholarship applicants?</b>
 
I was already an active member of various student societies at ACYA's inception at the Australian National University. During my undergraduate years, I forged partnerships between existing student organisations and ACYA. My suggestion for future ACYA-RBS IMBA students is that in order to better understand China, one must embrace its culture, master the language, and believe in our future success. Doing so isn't so much to gain a mechanical tool that will aid you in future career opportunities; rather, it is a way of life.  
 
<b>3. Why did you choose to study your MBA in China?</b>
 
When my family first migrated to Australia, they were going after the great Australian dream. In a similar vein, I think the reason why I chose to study for my MBA in China is that I am going after the Chinese dream. China, with the world's second largest economy and the largest population, holds endless career opportunities for people who take a deep interest in business. 
 
<b>4. What advantages are there in coming to Beijing to complete a two- year MBA program instead of staying in Australia?</b>
 
Beijing is currently one of, if not the most, centralised cities in China and therefore presents many unique opportunities to build a career and to foster networks with experts and industry leaders.
 
<b>5. What do you plan to do after finishing the Renmin IMBA?</b>
 
After gaining these necessary experiences my goal is to become a successful entrepreneur. This is the golden age of entrepreneurship, and I very much want to make my impact in this period. The Renmin University IMBA offers many opportunities to work, as well as undertake internships, in large enterprises in China. I personally think that this is a great starting point for my career goals in life.

<b>Contact</b>
Further information and other Australia-China scholarship opportunities can be found on the ACYA website.
For enquiries regarding the ACYA-RBS International MBA Scholarship, please contact the National Education Director:
Nathania Tangvisethpat 
education@acya.org.au