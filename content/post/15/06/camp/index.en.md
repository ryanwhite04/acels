{
  "title": "CAMP 2015",
  "author": "ACYA",
  "date": "2015-06-15T05:11:51+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_0167",
      "type": "image",
      "src": "IMG_0167.jpg"
    },
    {
      "title": "Screen Shot 2015-06-15 at 1.08.09 PM",
      "type": "image",
      "src": "Screen-Shot-2015-06-15-at-1.08.09-PM.png"
    },
    {
      "title": "FOA",
      "type": "image",
      "src": "FOA.png"
    },
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled.png"
    },
    {
      "title": "Unxiety",
      "type": "image",
      "src": "Unxiety.png"
    },
    {
      "title": "Andrea",
      "type": "image",
      "src": "andrea-dumplings.jpg"
    },
    {
      "title": "BRIDGECLIMB - CHINA AUSTRALIA MELLENNIAL PROJECT - 1 JUNE.2015",
      "type": "image",
      "src": "Close_up_group_shot_CAMP_01_06_15.jpg"
    },
    {
      "title": "Screen-Shot-2015-05-23-at-1.14.29-PM",
      "type": "image",
      "src": "Screen-Shot-2015-05-23-at-1.14.29-PM.png"
    },
    {
      "title": "Screen Shot 2015-06-15 at 3.12.41 PM",
      "type": "image",
      "src": "Screen-Shot-2015-06-15-at-3.12.41-PM.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2015/06/Screen-Shot-2015-05-23-at-1.14.29-PM.png"><img class="aligncenter size-full wp-image-22375" src="http://www.acya.org.au/wp-content/uploads/2015/06/Screen-Shot-2015-05-23-at-1.14.29-PM.png" alt="Screen-Shot-2015-05-23-at-1.14.29-PM" width="587" height="212" /></a>
<p style="text-align: justify;">The <a href="http://www.australiachina.org">China Australia Millennial Project (CAMP)</a> is a world-first project uniting 130 top young leaders from China and Australia for a bilateral business incubator across a broad range of industries. CAMP held their five day summit in Sydney earlier in June during the Vivid Ideas Festival. Alexandra Meek from ACYA interviewed Andrea Myles, the Co-Founder and CEO of CAMP, to find out more about the initiative.</p>
<p style="text-align: center;"><strong>What inspired you to found CAMP? </strong></p>
<p style="text-align: justify;">If I’m really honest, I was inspired by a sense of frustration actually. With op-ed after op-ed in the news from commentators talking about what we “should” be doing to activate Australia’s role in the Asian Century but aren’t actually doing, to looking around at my mates who had returned from China or were from China finding difficulty in getting a job which capitalised on their amazing skills, to looking at statistics which tell us that China is our most important trading partner but only a small fraction of Australian CEO’s have any Asia experience, to my own personal career frustration at finding a niche for my particular set of skills and goals. It all culminated with an explosive conversation with a mate. We brainstormed what we could do to make a tonne of progress in a short amount of time and light a fire of inspiration and enthusiasm under the most important players in the Australia-China relationship today, business, government and of course young people. Something had to be done so we’re doing it.</p>
&nbsp;
<p style="text-align: center;">CAMP delegates were separated into teams or ‘ThinkTanks’ and were involved in training in the lead up to the five day summit, what did this involve?</p>
<p style="text-align: justify;">Our delegates or “CAMPers”, as we lovingly call them, were involved in a 9 week online innovation incubator where they took a look at a challenge question, posed by their ThinkTank partner and conducted research, brainstormed new solutions, but most importantly, got to know each other. For example the Westpac Digital Disruption ThinkTank focused on the question: “How might we transform the future of banking to meet the needs of individuals and organisations in a digital and participation economy?” The ThinkTank groups were gently guided by an industry expert from the corporate partner and an innovation expert to help them think through the issues and collaborate better. We also had intercultural experts supporting the process.</p>
<p style="text-align: justify;">My fascination with China began in 2002 when I saved and saved and went on my first ever overseas adventure. Had initiatives like ACYA and CAMP existed when I’d started out, that would have made a massive difference. As the online CAMP incubator went along, I found myself wanting to participate as a CAMPer more and more hehe, which I think is the ultimate test.</p>
&nbsp;
<p style="text-align: center;">The summit last week began with a panel discussion, ‘Leading Innovation in the Asian Century’. What key insights were shared by the panel?</p>
<p style="text-align: justify;">It was such a great session. We discussed the “locks” creating barriers to collaboration and the “keys” to unlocking great progress and identified cultural competency, the right networks and partners and great government policy as ways to move forward. What I loved the most was that 900 people flooded in and packed out Sydney’s Town Hall to have a discussion about innovation and China. This just would not have happened 5 years ago. There is a great appetite out there if you know how to tap into what is important to people right now. With the CHAFTA signed and in formation and Sydney established as an RMB Hub, there has never been so much curiosity in the community about how to connect with China. We like to think that CAMP is helping to guide that conversation by injecting a bit of soul and fun into it and creating deep connections between people who genuinely have shared values and know and like each other.</p>
<a href="http://www.acya.org.au/wp-content/uploads/2015/06/IMG_0167.jpg"><img class="aligncenter size-large wp-image-22344" src="http://www.acya.org.au/wp-content/uploads/2015/06/IMG_0167-1024x406.jpg" alt="IMG_0167" width="512" height="203" /></a>

&nbsp;
<p style="text-align: center;">The Summit concluded with the CAMP Gala Dinner where delegates pitched their co-created new ideas to venture capitalists and leaders from the business community. What were some of the ideas that the ThinkTanks pitched?</p>
<p style="text-align: justify;">The outputs of the ThinkTanks blew everyone away! We were testing a model of new collaboration and creating a new blueprint for bilateral relations and boy, did our CAMPers rise to the challenge! The Tourism ThinkTank “Future of Awesome” pitched a new app inviting Chinese bloggers to write content for regional tourism providers who have a great offering for travellers from China, but no way to connect to them. People’s Choice Winner was Unxiety, a website delivering stress-reducing gamified programs for the 1 in 3 people who suffer from anxiety in China and Australia.</p>
<p style="text-align: justify;">The winners of CAMP 2015 were “Elumin8”, a team of five young Australian and Chinese entrepreneurs who took out the top prize at the inaugural CAMP Gala Dinner in front of an audience of over 350 at the City of Sydney’s Town Hall. The Energy and Sustainable Living ThinkTank team’s winning idea, the ‘ipod of energy’, is designed to change the way people monitor and engage with their energy consumption by ‘making energy visible’.</p>
&nbsp;
<p style="text-align: center;">Now that the five day summit has concluded, what does this mean for the delegates?</p>
<p style="text-align: justify;">It’s so awesome to see that the collaborations are continuing on now that the summit is complete. It’s safe to say that every single CAMPer left with new connections, who they will be continuing conversations with in the future. Our WeChat is going off with brainstorming ideas for CAMP 2016, applications open in October 2015. CAMPers are currently involved in a 3 week post-program and on-ramping into our alumni program which is being co-created by them. The CAMP philosophy is founded on the premise that out of great relationships comes great business and great professional development. But relationship development comes first and above all other things. We’re not talking about the sorts of “relationships” which are abstractly described as “people to people links”, but actual friendships where trust and empathy are the bones and muscles and fresh, co-created ideas are the blood racing through the arteries. There’s never been a more exciting time in the relationship. Put simply, more business isn’t the key to business development in the Asian Century. The key to unlocking the magic within the Asian Century lies with smart, young people pouring in fresh ideas and new methods of collaboration which will really see this century come alive.</p>
&nbsp;
<p style="text-align: center;">For you personally, what was the highlight of CAMP 2015?</p>
<p style="text-align: justify;">For me the highlight of CAMP was seeing all of our CAMPers together on that first day. Because we had all been chatting via WeChat and collaborating on our learning platform Practera, we all felt like we knew each other quite well. In the first workshop session, I saw people who had only known each other face to face for a few hours, leaning in, challenging each other’s ideas, smiling and laughing and exchanging views. It was pure magic and it was the moment when I knew we were really onto something.</p>
<p style="text-align: justify;">I’m also really proud that CAMP is an inclusive experience, not an exclusive one. Sure we have 130 brilliantly talented people in the room, but they come from all walks of life and backgrounds. When we recruit, we’re not just looking for people who got the highest marks at the most prestigious uni or won a million awards. We look for people who have been active and delivered something unique and useful into the world and shown personal initiative. We achieved something really unique when we recruited 50% female delegates, experts and speakers, showing that women have a clear role to play in the Asian Century. The best way to predict the future is to create it, and we want to see a future where it is normal to have diversity in leadership. Hence, via CAMP we are nurturing a diverse array of emerging leaders who will step into leadership roles at any moment whilst giving them the confidence to collaborate with a diverse array of people too.</p>
<p style="text-align: justify;">CAMP applications open in October 2015. Express interest at <a href="http://www.australiachina.org">http://www.australiachina.org</a></p>


<hr />

<a href="http://www.acya.org.au/wp-content/uploads/2015/06/andrea-dumplings.jpg"><img class="alignleft size-thumbnail wp-image-22354" src="http://www.acya.org.au/wp-content/uploads/2015/06/andrea-dumplings-150x150.jpg" alt="Andrea" width="150" height="150" /></a>
<em> Andrea Myles is the Co-Founder and CEO of CAMP. She is a China-lover who has previously headed up the <a href="http://www.acbc.com.au">Australia China Business Council</a> and co-founded the <a href="http://www.engagingchinaproject.org.au/study-in-china/">Engaging China Project</a>. In 2014, she was also named one of Australia’s 100 Most Influential Women by Westpac and the Australian Financial Review.</em>