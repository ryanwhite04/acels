{
  "title": "Scott Gigante",
  "author": "Dawn Qiu",
  "date": "2017-09-07T01:39:41+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Alumni Spotlight",
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS16-36",
      "type": "image",
      "src": "ACELS16-36.jpg"
    }
  ]
}
See what ACYA alumni have been doing to advance the Australia-China relationship. Hayden Wilkinson interviews Scott Gigante as he prepares to head off to Yale University.

<!--more-->

- ## What or who sparked your original interest in China?
  My love of languages was sparked by an exchange to France in high school. I wanted to study another language at university, and I wanted a language that would be a challenge and would also come in handy in later life. Chinese ticked both of those boxes better than any other.

- ## What has been your experience of working and living in China?
  I first traveled to China in 2014, with Huawei’s ‘Seeds of the Future’ internship program, a three week trip through Beijing and Shenzhen. Six months later, I left Australia for the life-changing journey that is the New Colombo Plan Scholarship. I studied Chinese and Environmental Science at Fudan University and took a six-month internship at VMware Shanghai, the Chinese R&D branch of a multinational computer science company. It was here that I developed both language skills and cultural understanding, and also broadened my understanding of Chinese and global business culture.

- ## Could you tell us about your career path leading up to today?
  I graduated with a Bachelor of Science in Mathematics and Statistics at the University of Melbourne at the end of 2016. During my degree, I took internships in computational chemistry, software engineering and biomedical informatics. It was this last internship, at the Walter & Eliza Hall Institute of Medical Research (WEHI), which became my full-time job post graduation, working on data analysis from the output of a handheld DNA sequencer called the Oxford Nanopore Technologies’ MinION. In January I was accepted to Yale University, where I will begin my Ph.D. in Computational Biology and Bioinformatics this coming September, working in roughly the same field as I do at WEHI. I hope that in the future I can combine my work with my interest in China, engaging in collaborative partnerships with Chinese researchers, some of whom are producing some of the best work in our field at present.

- ## What does your current work involve?
  I use a combination of mathematics, statistics, and computer science to approach problems in biomedical research. My current projects look at applying computational methods pioneered in natural language processing to detect DNA modifications which are involved in the formation of cancer.

- ## What influenced you to pursue a career in your field and what are the benefits?
  I have always known that I would like to work in a field which engaged my ongoing interest in mathematics and computer science, but the combination of these skills is applicable in such a broad range of fields that I didn’t really know where to start. My internship at WEHI inspired me to pursue a career in biomedical informatics, simply because the problems we are working to resolve have genuine impacts on people all around the world. The knowledge that my work might help diagnose somebody’s disease sooner and improve their chances of survival is enough motivation for a lifetime!

- ## Did you imagine this would be your career? What would first-year you think of what you are doing now?
  Absolutely not! In fact, I didn’t even know this field existed. As recently as 2015, when I was living in China, I didn’t know whether I wanted to be a software engineer, a diplomat or an academic! I always made sure to make the decision that allowed me to make the decision later, this became somewhat of a mantra for me. Studying mathematics and Chinese kept all of these doors open, and even today I don’t think it would be too late to change.

- ## What was your time in ACYA like? What were the most valuable things you got out of being part of ACYA?
  ACYA formed a mainstay of my community engagement while I was in Shanghai, and became my link back to China on returning. I relished the opportunity to engage with Chinese students both at home and abroad, which allowed me to improve both my language abilities and my understanding the rich Chinese culture that surrounded me.

- ## Did you get a chance to take on any leadership roles within ACYA? If so, what benefits did you get from it?
  I’m of the view that if I am going to do something, I’m not going to do it by halves. I joined ACYA Shanghai as chapter secretary and took it upon myself to organise an 80-person careers summit, which was a rewarding experience and set me up to pursue further leadership opportunities with ACYA. At the Annual General Meeting, a month after I returned to Australia, I was elected to be ACYA’s General Manager – Portfolios, where I looked after a team of 70 volunteers in areas including Careers, Education, People-to-People, Media, Business Development and more. This was one of the most formative positions I have taken in my life to date – I learned skills in website development, project management, visual design, team building, managing external relationships and much, much more. The role exposed me to a broad range of ACYA’s activities across both countries and forced me to learn on the job, a very valuable skill to have.

- ## What’s your best memory from your involvement in ACYA?
  The highlight of my time with ACYA would have to be organising and attending ACELS Sydney 2016. The team put in hours and hours of work into preparing a high-quality conference and sharing the key outcomes with the wider ACYA community. It was also a great opportunity to meet in person some of the people I had been working with all year!

- ## Do you have any advice for students and recent graduates looking to work in your field?
  Keep your mind and opportunities open – bioinformatics is a new and developing field, and like many nascent areas of research, an open mind and willingness to learn are just as important as any background knowledge you may have!
