{
  "title": "ACYA Guangzhou President explores regional relationships through Model APEC Conference",
  "author": "ACYA",
  "date": "2017-09-14T14:26:51+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Adelaide Michael",
      "type": "image",
      "src": "adealide1.jpg"
    },
    {
      "title": "Model APEC",
      "type": "image",
      "src": "model-apec-e1505751320703.jpg"
    }
  ]
}
On the 17th of August, I arrived in Yangzhou, to a conference that would pivot my understanding of relationships in the Asia Pacific. It was the MODEL APEC, and before all the nerves, excitement and joy that were to come, a feeling of severe under-qualification was the first emotional order of the day. The students who came from around China had won multiple competitive rounds; their knowledge of APEC and diplomatic eloquence of speech had been tried and tested. Meanwhile, I slipped through the doors with a reserved ticket for ACYA members. Never mind though, because, on the second-day corporate clothes, chandeliers, a big horseshoe table, and cameras that moved across the ceiling filled me with a buzz that was enough to steal my attention from myself.

<!--more-->

<img src="http://www.acya.org.au/wp-content/uploads/2017/09/model-apec-300x225.jpg" alt="" width="300" height="225" class="alignnone size-medium wp-image-26692" />

Just like a model, there was a dual scale of significance to every part of the conference; while learning about how our community cooperates to navigate towards a better, shared future, I found a parallel application at a personal level. I experienced the importance of intercultural connections. My participation in the conference and, more broadly my understanding of China, was enriched with the perspectives, advice, and guidance of my fellow delegates. 

During the competition, the arguments made by other model economies reflected the range of conditions that exist in the community. Each delegate used this research to propose his or her own unique programs to raise prosperity in areas of priority for their country. While consensus on select issues was sometimes challenging, the diversity of thought most often strengthened the solutions. I also benefited in the intervals between competitive rounds, when delegates shared with me advice and feedback on how to speak, address the chair and strategically use tea break to gather support for my proposals. 

In the final full day of the conference, my friends shared Yangzhou’s history with me. We explored the art district and the beginning of the ancient Grand Canal that stretches as far as Beijing. More than the history, I learned how these events and innovations shaped the China of today; how it spurred economic development and thereby shaped the society of the region. With the insights of my friends, I did more than seeing the craftsmanship and value of an incredible array of art forms. I learned about their symbols, significance.

One other important thing I walked away with was awe for the talent that exists in youth diplomacy. The skills of the delegates at this conference, particularly those on display at the finals, gave me an exciting glimpse into the developing leaders of the future, China and beyond. 

I consider myself to be very lucky to have represented ACYA at this year’s MODEL APEC; what I learned, the friends I made and the skills I have started on the journey to developing made this experience invaluable. If you have the opportunity, and the interest, I highly recommend taking part in 2018!