{
  "title": "ChAFTA poses worker safety risks",
  "author": "ACYA",
  "date": "2017-09-07T01:46:27+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Worker Image",
      "type": "image",
      "src": "Worker-Image.jpeg"
    }
  ]
}
Economists and business people alike have been hailing praise for the China Australia Free Trade Agreement (ChAFTA) since it was ratified in late 2015. The free trade agreement is set to reduce tariffs on Australian exports to China as a way of trade liberalisation designed to benefit both countries. Essentially under the agreement, it has become cheaper for Australian businesses to sell in China and cheaper for Chinese consumers to buy Australian products.

<!--more-->

It appears to be a win win situation for both sides, as ChAFTA is economically bridging the two nations together. But there is some concern for unqualified Chinese workers having their rights exploited on Australian soil, which puts their wellbeing and that of others at risk.

A clause within ChAFTA allows the Australian government to grant an extra 5000 working holiday visas to Chinese nationals (457 Visa). The Work and Holiday Agreement section is designed to boost Chinese tourism within Australia, especially in rural areas. However, there has been some exploitation in the form of unqualified labourers working in unsafe conditions not up to Australian standards. The majority of these are construction workers without the proper certifications required under Australian law. Not only is this undermining the integrity of Australian workplace laws, but it is creating unnecessary health and safety risks.

There are also some workers that are earning less than $10 per hour. Australia is renowned for having a respectfully high minimum wage by world standards, but this clause is failing to protect the basic rights of these workers.

However, it is important to note that there are few of these instances. In fact, the number of Chinese nationals attaining a working visa has declined. During the first year of ChAFTA, 457 visas to Chinese nationals fell by 19.2%, which is 9% more than the average decline of other countries. ChAFTA critics can now rest easy as the agreement has not ‘opened the flood gates’ of imported labour, which was argued would take local jobs. It should also be noted that the 457 visa already requires labour market testing to ensure that this does not occur.

ChAFTA has brought extensive benefits to the Australian China relationship. It has given Australian businesses across multiple industries a clear advantage in the Chinese marketplace, bringing significant revenue into Australia. Since negotiations ended two years ago and the gradual implementation began, several Australian industries have seen exponential growth in China. From January to March 2016 Australian wine grew by 60% when the tariffs dropped from 14% to 8.4%. In that same three-month period, milk powder and fresh cherry imports more than doubled. Also, contrary to popular belief, ChAFTA has not exacerbated China’s overcapacity of coal. In fact, the agreement has demonstrated great benefits for Australian coal producers and Chinese consumers.

By no means was ChAFTA a poor policy decision. It has given Australian exports an undeniable advantage within the Chinese marketplace, especially against strong competitors such as France. But it is important that Australia does not forfeit its national laws in favour of ChAFTA. There is no doubt that the agreement can give these extra visa provisions whilst still respecting Australian working standards. By allowing these instances to fly under the radar, we are doing the China Australia relationship a disservice.

By Zoe Niell