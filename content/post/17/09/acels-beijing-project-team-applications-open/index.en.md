{
  "title": "ACELS Beijing Project Team Applications Open ",
  "author": "ACYA",
  "date": "2017-09-27T06:21:24+00:00",
  "image": "",
  "categories": [
    "ACELS",
    "Opportunity"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association"
  ],
  "resources": []
}
ACYA is pleased to announce that in 2018 the China iteration of the <a href="http://www.acya.org.au/acels/">Australia-China Emerging Leaders Summit</a> (ACELS) will once again be held in Beijing! We are now calling for applications to be on the project team to deliver the pre-eminent youth conference supported by the Australia China Council. This is an exciting opportunity to develop your event management skills and networks.

<!--more-->

Positions on the Project Team include

- [Project Manager](http://www.acya.org.au/job/acels-project-manager)
- [Operations Manager](http://www.acya.org.au/job/acels-operations-manager)
- [Delegate Manager](http://www.acya.org.au/job/acels-delegate-manager)
- [Logistics Manager](http://www.acya.org.au/job/acels-logistics-manager)
</ul>

Submit your resume and a CV to <mailto:gm_china@acya.org.au> by midnight China Standard Time Friday *6th October* 2017. Check out the video of our previous ACELS Beijing.

[embed]https://youtu.be/w1fOo5dIEeQ[/embed]

> Being Delegate Manager for ACELS Melbourne 2017 was a chance for me to develop my professional skills, make new contacts and be a part of the National ACYA team. It proved that good things do come from hard work — the fantastic feedback we had from our delegates really made those hours of Skype meetings worth it!

> I got to work not only with a great team but with professionals from the Aus-China space in Melbourne and beyond. Being part of the Project Team goes way beyond something you can put on your resume, it’s a fantastic way to get more involved with ACYA and explore new ideas and opportunities.

> -- Bridie Allen, ACELS Melbourne 2017 Delegate Manager

<!-- Extra Blockquotes
<blockquote>
    As the Logistics Manager for ACELS Sydney 2016, I had a great opportunity to develop my skills in project management and form invaluable networks with conference delegates and members of the National Executive that I still rely on today in my current role at ACYA.
The role also gave me extensive overall access to the conference in that I was able to help “run the show” but also remain involved in the substantive proceedings such as listening to guest lectures and attending workshops.
Ultimately, being able to contribute positively to something as special as ACELS was a very rewarding experience, and I would certainly recommend it to anyone looking to further their involvement with ACYA.

<footer>— Courtney Lor, ACELS Sydney 2016 Logistics Manager</footer></blockquote>
<blockquote>
    Project managing a conference as ambitious as ACELS is one of the best opportunities to concomitantly enrich your professional development and engage in Aus-China relations.
In my run as Project Manager in 2015, I was able to test my strengths in project development, leadership, budgeting, and managing key partnerships. Yet the pinnacle of the experience was at the end of the conference, when I sat down to dinner with a table of delegates from across the country who, just three days prior, had never met one another.
It was a thoroughly rewarding payoff to witness a group of highly passionate leaders who were energised by newly-formed friendships, and knowing I played a part in making that happen.

<footer>— Samuel Johnson, ACELS Sydney 2015 Project Manager</footer></blockquote>
-->