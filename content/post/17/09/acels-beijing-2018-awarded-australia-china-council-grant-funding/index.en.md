{
  "title": "ACELS Beijing 2018 Awarded Australia-China Council Grant Funding",
  "author": "ACYA",
  "date": "2017-09-03T14:27:08+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Australia-China Council",
    "Australia-China Youth Association"
  ],
  "resources": []
}
The Australia-China Youth Association (ACYA) is pleased to announce that the <a href="http://dfat.gov.au/people-to-people/foundations-councils-institutes/australia-china-council/Pages/australia-china-council.aspx">Australia-China Council (ACC)</a> will be providing a grant of $20,000 to ACYA. These funds will support the next 2018 <a href="http://www.acya.org.au/acels/" target="_blank" rel="noopener">Australia-China Emerging Leaders Summit (ACELS)</a> to be held in Beijing in February 2018.

<!--more-->

[embed]https://www.youtube.com/watch?v=w1fOo5dIEeQhttp://[/embed]

The ACC was established in 1972 by the Commonwealth government with the overall goal of facilitating closer person-to-person links between Australia and the People’s Republic of China – a goal that ACYA similarly strives to achieve. Its main activities are to provide cross-sectoral bilateral expertise and policy advice to the government and to support various projects and research centers deemed valuable to the long-term strengthening of Australia-China relations.

This year is particularly important for Australia-China relations generally, being the 45th anniversary of the formal establishment of diplomatic dialogue between the two countries in 1972. The ACC was borne out of discussions that followed and was properly instituted by the government in 1978, which will make next year the 40th anniversary of its founding.

Through its annual competitive grants program, the ACC awards grants to applicants who have successfully demonstrated innovative proposals related to the ACC’s strategic priority areas of Arts and Culture, Education and Economic Diplomacy in China. For the latest 2017-2018 round, the ACC delivered a total of $800,000 in funding to 34 projects across Australia.

ACELS is ACYA’s largest professional development initiative, bringing together current and future young leaders to partake in a 3-day program to engage in topics ranging from business to sociocultural issues in the Australia-China space. This year’s ACELS Beijing 2017 was held from 17-19 February and featured a number of important keynote speakers, networking evenings and skills-based workshops.

Contact:

Chloe Dempsey
China General Manager
gm_china@acya.org.au