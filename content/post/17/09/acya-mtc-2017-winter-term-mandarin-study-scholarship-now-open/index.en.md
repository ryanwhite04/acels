{
  "title": "ACYA-MTC 2017 Winter Term Mandarin Study Scholarship Now Open",
  "author": "April Liu",
  "date": "2017-09-18T10:18:44+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [
    "Taiwan"
  ],
  "organisations": [
    "Australia-China Youth Association",
    "Mandarin Training Centre",
    "National Taiwan Normal University"
  ],
  "resources": [
    {
      "title": "Mandarin Training Centre Logo",
      "type": "image",
      "src": "course-banner-1.jpg"
    }
  ]
}
ACYA is offering 3 scholarships to study at the National Taiwan Normal University Mandarin Training Centre (NTNU MTC) during their Winter Term (Dec 1, 2017 - Feb 27, 2018).

This is a fantastic opportunity for ACYA members to continue their Chinese language and culture studies in one of Taiwan’s most prestigious educational institutions. MTC has drawn participants from around the world, including former Australian Prime Minister Kevin Rudd.

<!--more-->

For Jermaine Werror, a 2016 ACYA-MTC scholarship recipient, one of the advantages of studying at MTC was cultural immersion - 

<blockquote>The experience significantly assisted and accelerated my learning by allowing me to practice my Chinese every day and understand a bit more about Chinese culture and history.</blockquote>

The scholarship is for the 2017 Winter Term with a waived tuition fee and will be awarded to serious students with an excellent academic record, and a demonstrated interest in Mandarin Chinese study. 

For more information, go to <a href="http://mtc.ntnu.edu.tw/eng/">http://mtc.ntnu.edu.tw/eng/</a>

<h2>To apply</h2>
Please email the application form, additional materials listed in the application form, a copy of your resume and cover letter outlining why you should be selected as an MTC scholar and how the program will benefit you to <strong><a href="mailto:education@acya.org.au">education@acya.org.au</a> before 5 pm, October 13, 2017</strong>. If you are part of an ACYA chapter, please state the chapter you are a member of.
Download the application form <a href="http://www.mtc.ntnu.edu.tw/upload_files/student/NewMTCScholarship.pdf">here</a>. 
 
<h2>About the MTC</h2>
<a href="http://www.mtc.ntnu.edu.tw">The Mandarin Training Center (MTC)</a>, established in 1956, is affiliated with the National Taiwan Normal University (NTNU) and is Taiwan’s oldest and best-known Chinese language institute. In cooperation with the Graduate Institute of Teaching Chinese as a Foreign/Second Language, the Graduate Institute of International Sinology Studies, the Department of Applied Chinese Languages and Literature, and the Department of Chinese Language and Culture for International Students at NTNU, the MTC is recognised as one of the world’s leading research and teaching institutions.

For more information visit their <a href="https://www.facebook.com/mtc.ntnu?fref=ts">Facebook page</a>.

 General enquiries about ACYA scholarships should be directed to ACYA National Education Director, April Liu, at <a href="education@acya.org.au">education@acya.org.au</a>.