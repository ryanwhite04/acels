{
  "title": "Diversity Fan Engagement Internship",
  "author": "ACYA",
  "date": "2017-04-08T05:19:59+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [
    "Casual",
    "Communications",
    "Marketing"
  ],
  "locations": [
    "Melbourne"
  ],
  "organisations": [
    "Melbourne Football Club"
  ],
  "resources": []
}
1-2 days per week depending on requirements
<ul>
 	<li>Department: Consumer Business</li>
 	<li>Reports To: Manager - Consumer Business and Engagement</li>
 	<li>Direct Reports: NIL</li>
 	<li>Location: MCG</li>
 	<li>Start and End Date: 1st May - 30th September</li>
 	<li>Remuneration: Contract will be part time fixed term for 5 months at market rates</li>
</ul>
<!--more-->
<h2>Position</h2>
1. Position Summary/Objective
<ul>
 	<li>This role is responsible for assisting with the engagement and fan growth elements of the Melbourne Football Club, particularly through the Chinese community.</li>
</ul>
2. Key Areas of Responsibility
<ul>
 	<li>Assist with the recruitment of students for the Student Engagement program</li>
 	<li>Assist with delivering the Student Engagement program</li>
 	<li>Assist in the development and delivery of key Chinese activations</li>
 	<li>Develop and deliver content for the Melbourne Football Club WeChat account</li>
 	<li>Assist with the development of online content specific to the Chinese market on behalf of the Melbourne Football Club</li>
 	<li>Assist in developing and delivering all engagement strategies for Chinese nationals that come to Melbourne</li>
 	<li>Work with the marketing and community departments to maximise data collection opportunities</li>
 	<li>Assist with implementing engagement activities across new and existing markets</li>
 	<li>Demonstrate the MFC values in day to day work</li>
</ul>
3. Competencies, Qualifications, Experience and Key Attributes
<ul>
 	<li>Experience and high level of proficiency with the Microsoft Office Suite especially Word, Excel and Powerpoint</li>
 	<li>Excellent organisational skills with the ability to multi task, prioritise and meet deadlines</li>
 	<li>Proficiency with WeChat official accounts</li>
 	<li>Good numeracy and analytical skills</li>
 	<li>Developed communication and interpersonal skills, both English and Chinese</li>
 	<li>Professional proficiency in Chinese Mandarin preferable (speaking and written)</li>
 	<li>Self-motivated with a commitment to achieving results</li>
 	<li>Ability to multi task, prioritise and display effective time management skills</li>
 	<li>Ability to work autonomously and as part of a team with a positive and proactive work ethic</li>
</ul>
4. Additional Requirements
<ul>
 	<li>Position offered will be subject to a Working With Children's Check.</li>
</ul>
To apply, please send applications to the ACYA Careers Director (James Tang - <a href="mailto:careers@acya.org.au">careers@acya.org.au</a>).
Please include a CV and cover letter addressed to Jane McGough - Manager, Melbourne Football Club outlining your motivations and suitability for the role.

Applications close on Friday 21st April 2017.