{
  "title": "Dashan Chats with ACYA",
  "author": "ACYA",
  "date": "2017-04-10T11:47:23+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [
    "Interview"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Dashan2006",
      "type": "image",
      "src": "Dashan2006-e1491826809111.jpg"
    }
  ]
}
<em>ACYA members Charlie Lyons Jones and Scott Gigante chat to Dashan - the 'most famous foreigner in China'.</em>

ACYA: I would like to ask you one question... “Da” (大 big) is an adjective and “Dashan” (大山 big mountain) doesn't really sound like someone’s name, how did you come up with it?

Dashan: Dashan is my stage name. When I first started learning Chinese, my teacher gave me a regular but bookish Chinese name called “Lu Shiwei” (路世伟). But later on in 1988, when I was studying abroad at Peking University, I took part in a sketch on a TV show at a chance. The name of my character in the sketch was “Dashan”. “Dashan” is one of the hottest words in the eighties, it's similar to the internet slang words nowadays, it was the slang word at that time. I found the name “Dashan” pretty funny, and since it's a joke, the audience remembered the name.

<!--more-->

ACYA: I heard you went to Beijing to study and within months you were speaking with a Beijing accent on CCTV. What’s your secret of learning Chinese?

Dashan: There's no such thing as a secret. I had learnt Chinese for 4 years at University of Toronto before I set off to Peking University. It is true that I participated in a TV show within 2 months in Beijing. My Beijing accent wasn't so strong at that time. I was still speaking with an English accent, but I didn't have problems to make everyday conversation. After living in Beijing for a period of time, I naturally picked up the Beijing accent under the linguistic environment as well as some dialect languages. There is a difference between the Beijing dialect and Mandarin, that we need to learn it slowly. Learning a language is a very long process. You have to practise and make use of it every day, and adopt it gradually to become your own language.

ACYA: Thanks for your advice. After appearing on the CCTV New Year's Gala, you were learning crosstalk with Jiang Kun (姜昆老师). You are doing a lot of the Western-style stand-up comedy nowadays. Which one is your most enjoyed and favorite performance over the past 30 years?

Dashan: I actually like performing comedy shows. Whether it is stand-up or crosstalk, they both need to interact with the audience directly. For performing in a sketch or a drama, we need to put ourselves into the role before going on the stage. We need the genuine acting skills in order to make the audience enjoy the show. However, I'm talking directly to the audience in stand-up and crosstalk performance, and get the feedback from the audience directly. The audience doesn't speak when I'm speaking on the stage as a performer but their laughing and applause is the way they interact with the performance. This kind of conversation has a stronger interaction. This is the same for both stand-up and crosstalk, which I very much enjoy. When we get on the stage, we'd greet the audience directly by saying “Hello, my name is..., I'd like to tell you a story...”. We interact directly with the audience rather than simply acting in front of the audience in drama performance. This is exactly why I like performing stand-up comedy.

ACYA: What are the differences between Chinese and Australian humour?

Dashan: In my opinion, there's a huge mistake. I think no matter in China or in Western countries like Australia, there is cultural difference. Cultural difference can be found everywhere in different cities and provinces within China. I recognise that humour is a common feeling of humanity. Every culture has its own way and own content of humour, and there's also difference in some habits. However there's no much difference in terms of the sense of humour. The things Chinese find funny, Australians could also find funny. People find it hard to understand or appreciate sometimes, because of the language barrier, and also some content only exists in a specific culture. I believe that humour itself is universal.

ACYA: Do you think humour is international?

Dashan: I think it is. It seems to me that this is a common basic function of a human being, and also a big difference to tell between humans and animals. According to our understanding of modern science, animals do not have any sense of humour. Some advanced animals, like monkeys, may have a little sense of humour, but the rest doesn't. For a human being, this is very common and basic. It is similar to music that music has different types and styles, and music is in all cultures in the world. It is not difficult to enjoy music from a different culture. However, since the humour of a comedy is highly related to the languages, there is a strong language barrier. But I don't think that would pose a huge difference in the sense of humour.

ACYA: To enter a new culture, such as Chinese culture, what do you have to do?

Dashan: As I see it, we need to find out the common point. This is actually giving me a headache. People like me who work cross culturally always focus on the cultural differences when we talk about different cultures. It seems there is nothing in common apart from the difference. Why is that? It is because it'd be meaningless if we are looking at the common points of different cultures. In order to enter a new culture, the best way is to find out the common points, or some habits or values that we can share together. I don't support the idea of “overcoming the differences”, instead, we can actually embrace the differences. It would be boring and meaningless without the differences. It is nice to appreciate the differences when different cultures meet one another. For example, your eating habits are different to ours. Don't try to avoid it, we can actually take the initiative to try it. It is not about toleration or acceptance, but taking the initiative to aspire after the differences, and to embrace and appreciate them. Cultural difference is not an obstacle, I think it is all about our attitude.

ACYA: Is there anything you are still not yet used to living in China?

Dashan: I don't think so, since I have lived in mainland China for a long time. This is the first time for me coming to Australia, there are certainly some things that I'm not used to. I like experiencing different cultures worldwide, and I don't think there is a habit I can't get used to. When working in a cross-cultural environment, I think the sticking point is about one's ability to accept differences and diversity instead of the cultural differences. It really depends on you, whether you have the passion and ability to accept different cultures and habits.

ACYA: You’re the most well-known foreigner in China, and in the past you’ve said you wanted to promote stand-up comedy in China. However, you're taking your show “Dashan” on a world tour to Australia, New Zealand and other countries. Why are you doing so?

Dashan: I always perform in China at the same time. I'm performing in Beijing, Shanghai and Shenzhen this year. In fact, I just performed in Chongqing 10 days ago. It is a show telling my experience in between Chinese and Western cultures, it can be performed in China domestic area as well as in the Chinese communities overseas. Melbourne is an ideal place because there are a lot of Chinese in the community, and there's also an amazing International Comedy Festival going on now. I think this is a very good opportunity for me. This morning I've received an invitation from a Finnish friend I met at Shanghai World Expo via my website. He is back to Finland now and he said he would like to invite me with my show to Finland. I thought to myself, I haven't been to Finland yet and I believe that there are a lot of Chinese over there. Why can't we go there and make a show? Therefore, after I finish my tour in Australia, I can strive myself a chance to perform my “Dashan Live” show in northern Europe.

ACYA: What’s the meaning behind the title of your show “Dashan kan Dashan” (大山侃大山)? What can an ACYA member looking to see the show expect?

Dashan: As the name implies, “Dashan kan Dashan” is basically my own story. The audience is familiar with the public image of “Dashan” through TV shows. With the live comedy show, I'd like to introduce the image of “Dashan” behind the scenes and my personal experience in reality. The show is essentially my biography; it begins with the time I started learning Chinese and got engaged with performing on Chinese TV show. It tells the story of how I became a famous public figure and finally a comedian.

ACYA: Thank you for taking the time to chat to us. Do you have any final words for our members at ACYA?

Dashan: Thank you for all your support, ACYA! I'm Dashan from Canada. I'm coming to Melbourne with my stand-up comedy “Dashan Live” (大山侃大山). Sydney will be my next stop. Please follow me on my Wechat “dashan_kan_dashan” for more details. Thank you very much!

<em>Translated by Joanne Tse.</em>