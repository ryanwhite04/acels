{
  "title": "English Content Media Editor",
  "author": "ACYA",
  "date": "2017-04-07T05:29:14+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [
    "Casual",
    "Communications",
    "Marketing"
  ],
  "locations": [
    "Australia"
  ],
  "organisations": [
    "Educating Girls of Rural China"
  ],
  "resources": []
}
Educating Girls of Rural China (EGRC) is a Canadian charity founded in 2005.
It is dedicated to providing high school and university educations to impoverished young women from rural regions of Western China through financial sponsorship, personal support and mentorship.
EGRC operates at a grassroots level in poverty-stricken rural regions of Western China.
It has provided financial sponsorship and personal support to 698 girls and young women with their high school and university education, boasting a graduation rate of over 99 percent.
EGRC conducts various programs helping female students education who are living below the poverty line.
Associated with high school and university level respectively including tuition, accommodation, books, basic meals and living expenses.

More information can be found <a href="http://www.egrc.ca/">here</a>.<!--more-->
<h2>The Role</h2>
EGRC are looking for volunteers to help with the editing of their English Media content.
The candidates would be familiar with Wechat and able to edit posts at short notice.
Chinese proficiency would be highly regarded but not essential.
This role is a rewarding volunteer opportunity to help support a worthy cause while developing useful professional skills.

Please send your resume and a short cover letter to James Tang ACYA Careers Director - <a href="mailto:careers@acya.org.au">careers@acya.org.au</a> outlining your interest and motivation for the role by 18th May 2017.