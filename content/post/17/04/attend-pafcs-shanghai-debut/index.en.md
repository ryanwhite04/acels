{
  "title": "Attend PAFC's Shanghai debut",
  "author": "ACYA",
  "date": "2017-04-03T03:56:52+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "AFL Shanghai 2017",
      "type": "image",
      "src": "Shanghai-2017.jpg"
    }
  ]
}
ACYA has secured some exclusive free tickets for our members to attend Port Adelaide Football Club's (PAFC) inaugural premiership round AFL match in Shanghai. Tickets are now sold out so this could be your last chance to attend this historic event!

<!--more-->

PAFC is a proud supporter of ACYA. More information about our partnership can be found <a href="http://acya.org.au/2016/07/acya-port-adelaide-partnership-announcement/">here</a>.

The match will take place Sunday 14 May at Jiangwan Sports Centre, 1.20pm Shanghai time/ 2.50pm Adelaide time.

If you want to go into the running to access free tickets, tell us in 50 words or less what you think Port Adelaide bringing AFL to China means for Aus-China relations.

[ninja_form id=6]