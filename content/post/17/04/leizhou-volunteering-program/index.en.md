{
  "title": "Leizhou Volunteering Program",
  "author": "ACYA",
  "date": "2017-04-27T20:19:37+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Waiver",
      "type": "application",
      "src": "Waiver.pdf"
    },
    {
      "title": "2016 Leizhou Trip Volunteer, Michelly Ramli with her students",
      "type": "image",
      "src": "image1.jpg"
    },
    {
      "title": "2016 Leizhou Trip Volunteer, Michelly Ramli’s English Class",
      "type": "image",
      "src": "image2.jpg"
    },
    {
      "title": "Leizhou Volunteering Waiver",
      "type": "application",
      "src": "Leizhou-Volunteering-Waiver.pdf"
    }
  ]
}
ACYA, in conjunction with the Leizhou College Students Volunteer Association (LZCVA), is pleased to be opening applications for our 2017 Volunteering trip in Leizhou, China.
This will be a wonderful opportunity to teach English to middle school students and to experience life in regional China.
The trip will take place from the 15th to 25th July 2017, and applications are open to anyone with a passion for sharing Australian culture, the Australia-China relationship and teaching English to children in rural China.
Please fill the following to be considered for the trip:

<!--more-->

<ol>
  <li><a href="https://goo.gl/forms/WhXDMohGBSdoft7A2">Application form</a></li>
  <li><a href="http://www.acya.org.au/wp-content/uploads/2017/04/Leizhou-Volunteering-Waiver.pdf">Waiver</a></li>
</ol>

<img class="alignnone wp-image-25921" src="http://www.acya.org.au/wp-content/uploads/2017/04/image1-300x150.jpg" alt="" width="427" height="215" />

<b>Applications close on 9th of June 2017 at 7PM (AEST).</b>
You will be contacted following the closing date.
General enquiries about 2017 Leizhou Volunteering Trip should be directed to the ACYA National Education Director April Liu at <a href="mailto:education@acya.org.au">education@acya.org.au</a>.

Hope to see you in Leizhou!

<i>"It was definitely different from my previous China experiences.
Staying in an old-style home with limited modern luxuries was definitely an experience worth having, as it allowed me to gain an insight into the lifestyle of people who live in less developed cities.
It also made me truly appreciate the conveniences we may take for granted in Australia, such as hot water and sanitary bathrooms.
Meeting all the people there was definitely the highlight for me.
They were so nice and supportive and were really interesting too.
The students were also a lot of fun."</i>

- Lewis Hong, 2016 ACYA Leizhou Volunteer