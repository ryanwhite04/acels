{
  "title": "ACYA AGM: Sunday, 26th February 2017",
  "author": "ACYA",
  "date": "2017-01-28T06:18:10+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "AGM",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
The Australia China Youth Association (ACYA) is excited to announce details of the upcoming Annual General Meeting (AGM) for 2017.

Attendance at the AGM is open to all of the National Executive, Chapter Executives, and ACYA members in both countries.

<!--more-->

The principal aims of the AGM are fourfold:

1) To provide full operating, management, and financial reports of the 2016 National Executive Committee’s undertakings to members.

2) To approve proposed changes to the ACYA Constitution splitting the Media &amp; IT Portfolio into two separate portfolios, the Media Portfolio and the IT Portfolio

3) To elect the members of the 2017 National Executive Committee. Positions open for election include: <em>President, Managing Director, National Secretary, National Treasurer, General Manager - Australia, General Manager - China, General Manager - Portfolios, Alumni Engagement Manager</em>

4) To take in applications for members of the 2017 National Directors. Positions open for application in 2017 include: <em>Education Director, People-to-People Exchange Director, Careers Director, Publications Director, Business Development Director, Translations Director, Media Director, IT Director </em>(please note: if proposed changes to the ACYA Constitution are not approved, applicants for both Media and IT directorships will be considered for the extant Media &amp; IT directorship.)

In addition, applications for a number of supporting roles will be accepted, the appointment of which will be completed following the AGM. These roles include: <em>Portfolio Manager Secretary, Australia Manager Secretary, AusraliaBites Editor, ChinaBites Editor, Australia-China Perspectives Blog Editor, Careers Officer, Translations Officer</em>.

As the only apolitical NFP run for members, by members working to actively foster a transnational community of youth Australians and Chinese interested in further understanding each other’s countries, attendance at the AGM is the perfect opportunity for members to participate in, and be informed of the full scope of 2016’s activities, as well as the strategic direction to be charted in 2017.

The attendance of members is valued and highly encouraged. To attend, visit the <a href="http://www.acya.org.au/join-acya/"><em>Join ACYA</em></a> page for registration instructions or <a href="http://www.acya.org.au/chapters/">contact your local chapter president</a> to join a group attendance meeting.

<strong>The AGM will be held on Sunday, the 26<sup>th</sup> of February, 2017 at 10am China Standard Time / 1pm Australia Eastern Daylight Time. Applications for all roles must be submitted by 11:59pm AEDT on Friday, 17<sup>th</sup> of February, 2017.</strong>

<strong>Details for registering for the AGM and Position Descriptions of the National Executive Committee can be found on the </strong><a href="http://www.acya.org.au/join-acya/"><strong><em>Join ACYA</em></strong></a><strong> page of the ACYA website. A full agenda will be distributed to all registered attendees one week prior to the commencement of the meeting.</strong>