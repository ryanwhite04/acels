{
  "title": "Foreign Policy White Paper: Call for Submissions",
  "author": "ACYA",
  "date": "2017-01-12T02:38:09+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS FPWhitePaper Poster",
      "type": "application",
      "src": "ACELS-FPWhitePaper-Poster.pdf"
    },
    {
      "title": "Screen Shot 2017-01-12 at 13.27.53",
      "type": "image",
      "src": "Screen-Shot-2017-01-12-at-13.27.53-e1484188401551.png"
    },
    {
      "title": "Screen Shot 2017-01-12 at 13.27.31",
      "type": "image",
      "src": "Screen-Shot-2017-01-12-at-13.27.31-e1484188446885.png"
    },
    {
      "title": "ACELS Foreign Policy White Paper Call for Submissions",
      "type": "image",
      "src": "Foreign-Paper-Call.png"
    }
  ]
}
The Australia China Youth Association (ACYA) is proud to announce collaboration between its signature event, the Australia China Emerging Leaders Summit (ACELS), and the Australian Embassy Beijing.

<!--more-->

<img class="aligncenter wp-image-24775" src="http://www.acya.org.au/wp-content/uploads/2017/01/Screen-Shot-2017-01-12-at-13.27.53-e1484188401551.png" alt="ACELS Foreign Policy White Paper Submissions" width="568" height="161" />
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column">

The Australian Government is preparing a Foreign Policy White Paper to guide Australia’s international engagement over the next ten years. To make sure the Foreign Policy White Paper reflects the views, interests and talents of all Australians, the Australian Embassy Beijing invites ACYA members to make submissions to the ACELS - Foreign Policy White Paper Submissions in the lead-up to the Australia China Emerging Leaders Summit.

The White Paper will build on the Government’s foreign policy achievements over the last three years and will outline Australia’s most important principles and interests for engaging with the world and working with allies and partners in the decade ahead. The White Paper will include analysis of international opportunities and challenges facing Australia and the Indo-Pacific region as a result of these shifts and will guide its foreign policy and diplomatic efforts.

Through study, internships, work and people-to-people links, ACYA Members are actively engaged with the effects of Australia’s foreign policy in the region. ACYA and The Australian Embassy Beijing will select three outstanding submissions to be featured and discussed by panel experts at the Australia China Emerging Leaders Summit policy workshop. Finalists will also be published on ACYA and Embassy media platforms.

<strong>Step 1:</strong> Read <a href="http://dfat.gov.au/whitepaper/wp-content/uploads/2016/11/foreign-policy-white-paper-call-for-submissions.pdf" target="_blank" rel="noopener noreferrer">Call for Submissions: Foreign Policy White Paper</a>, and familiarize yourself with the six key topics on page 3 (national interests, diverse interests, international organisations, economic opportunity, security and transnational challenges, assets and capabilities). For inspiration and ideas, please follow the discussion #FPWhitePaper on social media.

<strong>Step 2:</strong> Write submission of up to 800 words addressing one of the six outlined topics. Clearly outline how you think Australia should engage with the world in the decade to come. Submissions are encouraged to focus on Australia’s engagement with East Asia. <strong>Panellists will be looking for your own ideas and suggestions, as opposed to secondary research.</strong>

<strong>Step 3:</strong> Submit to <a href="mailto:education@acya.org.au" target="_blank" rel="noopener noreferrer">education@acya.org.au</a>. Deadline for the ACELS - Foreign Policy White Paper Submissions will close at midnight on 14 Feb 2017. All submissions will be forwarded to the Foreign Policy White Paper Taskforce, unless applicants specify otherwise.

<strong>Step 4:</strong> Applicants of the top three submissions, as selected by ACYA and panellists of the ACELS policy workshop, will be notified by email by 17 Feb 2016.

<strong>Step 5:</strong> The top three submissions will be announced and discussed at the ACELS policy workshop on 17 Feb 2016. These submissions will then be shared on ACYA and Embassy media platforms.

Please send any enquires to <a href="mailto:education@acya.org.au" target="_blank" rel="noopener noreferrer">education@acya.org.au</a>. To learn more about the Foreign Policy White Paper, please visit <a href="http://dfat.gov.au/whitepaper" target="_blank" rel="noopener noreferrer">dfat.gov.au/whitepaper</a> or join the conversation on social media #FPWhitePaper.

<img class="aligncenter wp-image-24776" src="http://www.acya.org.au/wp-content/uploads/2017/01/Screen-Shot-2017-01-12-at-13.27.31-e1484188446885.png" alt="Australian Embassy China, Australia China Youth Association, Australia Global Alumni, Australia China Emerging Leaders Summit" width="568" height="171" />

</div>
</div>
</div>