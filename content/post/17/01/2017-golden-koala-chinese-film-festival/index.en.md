{
  "title": "ACYA Hosts Golden Koala Chinese Film Festival for 2017",
  "author": "ACYA",
  "date": "2017-01-19T14:04:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Golden Koala Chinese Film Festival Event Program",
      "type": "application",
      "src": "Golden-Koala-Chinese-Film-Festival-Event-Program.pdf"
    },
    {
      "title": "film_festival_banner_corrected",
      "type": "image",
      "src": "film_festival_banner_corrected.jpg"
    },
    {
      "title": "film_festival_banner_corrected",
      "type": "image",
      "src": "film_festival_banner_corrected-1-e1484834503757.jpg"
    }
  ]
}
<h2></h2>
ACYA is proud to announce that we will be hosting the 2017 <a href="http://www.cff.org.au" target="_blank" rel="noopener noreferrer">Golden Koala Chinese Film Festival (GKCFF)</a> for its 7th edition. GKCFF is Australia's peak Chinese film festival, and aims to promote Chinese films and boost Australia's understanding of Chinese culture. It brings to Australia the best Chinese language films this season, showcasing new, up-and-coming directors. Tickets are free, all films are subtitled in both English and Chinese, and all members of the community are encouraged to come along.

<!--more-->

After a successful 2016 hosted by our sister organisation, the <a href="http://acypi.org.au" target="_blank" rel="noopener noreferrer">Australia-China Young Professionals Initiative</a>, ACYA is honoured to take the reins of this excellent initiative in Sydney, Melbourne, Adelaide, Canberra and Brisbane, while the Perth event will continue to be hosted by ACYPI. The festival will open in Sydney on the 2nd of February and close in Canberra on the 27th of February.

You can follow the links to meet the <a href="http://www.acya.org.au/p2p/golden-koala-chinese-film-festival/project-team/">project team</a> and read the <a href="http://www.acya.org.au/wp-content/uploads/2017/01/Golden-Koala-Chinese-Film-Festival-Event-Program.pdf" target="_blank" rel="noopener noreferrer">full event program</a>. We look forward to seeing you there!
<h2>Festival Schedule</h2>
<table>
<tbody>
<tr>
<th style="text-align: left;" width="75">City</th>
<th style="text-align: left;" width="155">Dates</th>
<th style="text-align: left;" width="295">Venue</th>
<th style="text-align: left;" width="75">Event Link</th>
<th style="text-align: left;" width="80">Tickets Link</th>
</tr>
<tr>
<td>Sydney</td>
<td>02/02/17 - 07/02/17</td>
<td>UNSW Central Lecture Block 4</td>
<td><a href="https://www.facebook.com/events/1376142745737329/" target="_blank" rel="noopener noreferrer">Facebook</a></td>
<td><a href="http://acyaunsw.eventbrite.com/" target="_blank" rel="noopener noreferrer">Eventbrite</a></td>
</tr>
<tr>
<td>Melbourne</td>
<td>08/02/17 - 10/02/17</td>
<td>RMIT School of Media &amp; Communications</td>
<td><a href="https://www.facebook.com/events/1299571723441853/" target="_blank" rel="noopener noreferrer">Facebook</a></td>
<td><a href="http://gkcff-2017.eventbrite.com" target="_blank" rel="noopener noreferrer">Eventbrite</a></td>
</tr>
<tr>
<td>Perth</td>
<td>13/02/17 - 17/02/17</td>
<td>State Theatre Centre of Western Australia</td>
<td><a href="https://www.facebook.com/goldenkoalaperth" target="_blank" rel="noopener noreferrer">Facebook</a></td>
<td><a href="https://www.eventbrite.com.au/o/co-organised-by-the-australia-china-young-professionals-initiative-in-perth-9644515311" target="_blank" rel="noopener noreferrer">Eventbrite</a></td>
</tr>
<tr>
<td>Brisbane</td>
<td>15/02/17 - 17/02/17</td>
<td>Griffith University Film School</td>
<td><a href="https://www.facebook.com/events/373241453044964/" target="_blank" rel="noopener noreferrer">Facebook</a></td>
<td><a href="https://www.eventbrite.com.au/o/australia-china-youth-association-griffith-12622916190" target="_blank" rel="noopener noreferrer">Eventbrite</a></td>
</tr>
<tr>
<td>Adelaide</td>
<td>18/02/17 - 21/02/17</td>
<td>Adelaide University Cinema</td>
<td><a href="https://www.facebook.com/events/963639147102366/" target="_blank" rel="noopener noreferrer">Facebook</a></td>
<td><a href="https://www.eventbrite.com/o/australia-china-youth-association-adelaide-12556986893" target="_blank" rel="noopener noreferrer">Eventbrite</a></td>
</tr>
<tr>
<td>Canberra</td>
<td>24/02/17 - 27/02/17</td>
<td>ANU Molonglo Theatre</td>
<td><a href="https://m.facebook.com/events/1739214989730603/">Facebook</a></td>
<td><a href="https://www.eventbrite.com.au/o/acya-golden-koala-chinese-film-festival-canberra-12744634294">Eventbrite</a></td>
</tr>
</tbody>
</table>
<h2>Film Trailers</h2>
► Heukseok Kids《黑石迷儿》
https://youtu.be/OqKnBT1Nu70

► The Song of Cotton《盛先生的花》
https://youtu.be/KTL9PXrs5-A

► Detective Chinatown《唐人街探案》
https://youtu.be/7BYfv6hnXi0

► The Bride《尸忆》
https://youtu.be/oYGmcqKPGqo

► Packages From Daddy《心灵时钟》
https://youtu.be/kSutJNZWvRY

► The Summer Is Gone《八月》
https://youtu.be/m_43X8gzB5g

► The Dog《狗》
https://youtu.be/-Y52TkuiV6w

► Front Cover《封面有男天》
https://youtu.be/Yd200BoZSPQ

► Robbery《老笠》
https://youtu.be/ssslJ_xRzZk