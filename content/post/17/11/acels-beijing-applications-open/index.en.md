{
  "title": "ACELS Beijing Applications Open",
  "author": "Liam Kearney",
  "date": "2017-11-28T03:05:34+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association"
  ],
  "resources": [
    {
      "title": "ACELS Beijing Flyer",
      "type": "image",
      "src": "ACELS-Beijing-Flyer.png"
    },
    {
      "title": "ACELS Beijing FAQ",
      "type": "application",
      "src": "ACELS-Beijing-FAQ.pdf"
    }
  ]
}
ACYA is pleased to announce that the Australia China Emerging Leaders Summit (ACELS) will be returning to Beijing from 22-25 February 2018.

[ACELS Beijing 2018]((http://www.acya.org.au/acels/beijing-2018/) will be guided by the theme: Broadening the Bilateral - Embracing a New Asian Paradigm – reflecting the unparalleled opportunities and new challenges that come with a region undergoing massive transformation.

Apply [here](https://acelsbeijing2018.typeform.com/to/Bo1SrA) before **15 December 2017 (11:59pm AEST)** if you would like to attend.

<!--more-->

Before applying, it is essential that you read the [ACELS Beijing Delegate FAQ](http://www.acya.org.au/wp-content/uploads/2017/11/ACELS-Beijing-FAQ.pdf).

If you have any queries please contact the ACELS Beijing 2018 Project Team at <mailto:acels@acya.org.au>