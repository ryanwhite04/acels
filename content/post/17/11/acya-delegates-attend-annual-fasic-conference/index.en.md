{
  "title": "ACYA Delegates attend annual FASIC Conference",
  "author": "ACYA",
  "date": "2017-11-14T16:31:15+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [
    "China",
    "Jiangsu",
    "Xuzhou"
  ],
  "organisations": [
    "Foundation for Australian Studies in China"
  ],
  "resources": [
    {
      "title": "Jesse Glass",
      "type": "image",
      "src": "Jesse-Glass.jpg"
    },
    {
      "title": "Greg McCarthy",
      "type": "image",
      "src": "Greg-McCarthy.jpg"
    },
    {
      "title": "Kevin Hobgood Brown",
      "type": "image",
      "src": "Kevin-Hobgood-Brown.jpg"
    }
  ]
}
The Foundation for Australian Studies in China and the Australian Embassy Beijing recently sponsored 10 delegates selected by ACYA (5 from ACYA and 5 from the New Colombo Plan) to attend the fifth annual Foundation for Australian Studies in China Conference in Xuzhou, Jiangsu from 1-3 November 2017.

The conference was convened by Professor Greg McCarthy, BHP Billiton Chair of the Australian Studies Centre at Peking University (PKUASC), the Foundation for Australian Studies in China (FASIC), and gracefully hosted by Jiangsu Normal University.

![ACYA delegates speak with FASIC Chairman Kevin Hobgood Brown AM](http://www.acya.org.au/wp-content/uploads/2018/03/Kevin-Hobgood-Brown.jpg "ACYA delegates speak with FASIC Chairman Kevin Hobgood Brown AM")

<!--more-->

Applicants were selected based on their response to one of two questions, which revolved around the central theme of the conference, ‘transnational connectivity’. 

What impact does transnational connectivity have on Australia-China relations?
What is the relevance of transnational connectivity in the current world?

Piero Craney, New Colombo Plan 2017 Scholar and current Tsinghua University student was one of the scholars selected to attend. Piero’s response provides an insight into the standard of applications received. 

> An understanding of transnational connectivity and its impact on the relationship between Australia and China necessitates a reflection of our shared past, especially given that this year marks the 45th anniversary of diplomatic relations between the two counties. Our past, however, extends beyond the negotiation meeting rooms of previous leaders and includes neglected histories such as the role played by Indigenous rights activists in the October 1972 trip to Beijing, and the first inter-marriages of Chinese and Australians during the 20th century Gold Rush. These neglected parts in Australian-Chinese history should be re-examined as they show that our relationship with China is historically deep and rich. A greater understanding of our complete shared history with China is also important because our past reflects and explains the current relationship we have with China and aids us in shaping our future relations.

> Australia-China relations are not only shaped by our past but by current attitudes and connections of Australian and Chinese people. A bilateral relationship may be formalised by treaties and trade agreements, but it is the people-to-people links and cultural exchanges that underpin the success of such relationship. The opportunities for the exchange of ideas and cultures cannot be pushed by government but driven by the desire and curiosity of everyday Australians and Chinese –to learn more and engage with each other. Transnational connectivity is therefore transforming the ‘tyranny of distance’ anxiety to an appraisal of the ‘benefits of proximity’.

![ACYA delegates including New Colombo Plan Scholars with BHP Billiton Chair of Australian Studies at Peking University, Professor Greg McCarthy](http://www.acya.org.au/wp-content/uploads/2018/03/Greg-McCarthy.jpg "ACYA delegates including New Colombo Plan Scholars with BHP Billiton Chair of Australian Studies at Peking University, Professor Greg McCarthy")

Cameron Hunter, Westpac Bicentennial Foundation Asian Exchange alumni and current Renmin University student, was one of the ACYA members in attendance at the conference.

> The FASIC Conference brought together a group of accomplished speakers and engaged delegates who shared thought-provoking, well-balanced and constructive ideas on the depth and character of Australia-China relations. Taking place in historical Xuzhou, over three days delegates were able to gain insight into multiple aspects of our bilateral relationship including discussing issues such as education, international law, immigration, geopolitics and language. The packed programme included several delectable banquets, an array of cultural performances and the chance to explore Xuzhou’s Han Dynasty collection of terracotta soldiers.

> There were many fantastic opportunities to network and socialise with other delegates, hosts and speakers throughout a stimulating and friendly conference. I particularly enjoyed hearing from Priya Chacko from the University of Adelaide talk about regulatory measures in the Belt and Road Initiative and James Oswald’s expertise around ecological civilisation terminology in Chinese policy. The conference allowed me to both further develop my research interests and discover new avenues of cultural exchange.

![ACYA Delegate and Chengdu Chapter President Jesse Glass](http://www.acya.org.au/wp-content/uploads/2018/03/Jesse-Glass.jpg "ACYA Delegate and Chengdu Chapter President Jesse Glass")

ACYA is proud of its longstanding relationship with FASIC, which provides a valuable medium through which the bilateral relationship can develop in an environment of academic understanding and inquiry.