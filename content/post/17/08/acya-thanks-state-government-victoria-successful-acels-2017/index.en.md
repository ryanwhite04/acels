{
  "title": "ACYA Thanks the State Government of Victoria for a successful ACELS 2017",
  "author": "ACYA",
  "date": "2017-08-18T07:32:34+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Media Release",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [
    "Australia",
    "Melbourne",
    "Victoria"
  ],
  "organisations": [
    "Trade Victoria"
  ],
  "resources": [
    {
      "title": "Tvic4",
      "type": "image",
      "src": "Tvic4.jpg"
    },
    {
      "title": "TVic3",
      "type": "image",
      "src": "TVic3.jpg"
    },
    {
      "title": "Tvic",
      "type": "image",
      "src": "Tvic.jpg"
    },
    {
      "title": "Trade Victoria Speaker at ACELS",
      "type": "image",
      "src": "TVic2-e1505099753363.jpg"
    }
  ]
}
Following a successful <a href="http://www.acya.org.au/acels/">Australia-China Emerging Leaders Summit</a> (ACELS) 2017 in Melbourne from 27-30 July, the Australia-China Youth Association (ACYA) would like to express its appreciation for the generous support of the event provided by Trade Victoria division at the State Government of Victoria.

<!--more-->

<a href="http://trade.vic.gov.au/">Trade Victoria</a> is a division of the Department of Economic Development, Jobs Transport and Resources within the State Government of Victoria. Trade Victoria – including the global network of 20 Victorian Government Business Offices (VGBOs) – support local businesses to expand into international markets, build strong trading relationships and capitalise on export opportunities. 
 
China is Victoria’s largest trading partner, with two-way merchandise trade totaling $22.8 billion in 2016. Combined with a buzzing Chinese population in Melbourne, the city was an ideal backdrop to bring together young leaders in Australia-China relations. 

As a major sponsor this year, Victorian Government China specialists participated in various events during the program, including panel discussions, seminars, and networking events. In addition, three Victorian Government Hamer Scholarship alumni formed part of the ACELS delegation. 

The ACELS is one of ACYA’s key initiatives for youth development. Centred around the theme “Exploring the Cultural Dimensions of the Bilateral Relationship”, the 3-day intensive program explored current cultural issues in the Australia-China social, political and economic space through a series of networking events, panel discussions, interactive forums, and workshops. 
 
This year, over 70 students and young professionals from around Australia participated in the Summit.
 
Contact:
<ul>
<li>Roger Lee</li>
<li>Australia General Manager</li>
<li>gm_aus@acya.org.au</li>
</ul>