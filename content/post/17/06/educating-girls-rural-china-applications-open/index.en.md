{
  "title": "Educating Girls of Rural China Applications Open",
  "author": "ACYA",
  "date": "2017-06-07T07:47:08+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [
    "China"
  ],
  "organisations": [
    "Educating Girls of Rural China"
  ],
  "resources": [
    {
      "title": "EGRC",
      "type": "image",
      "src": "EGRC.jpg"
    }
  ]
}
In partnership with Educating Girls of Rural China, ACYA is very pleased to announce that an opportunity has arisen for two volunteers to participate in a one week English teaching program in Gansu or in Guizhou this coming Summer holiday from 3<sup>rd</sup> - 8<sup>th</sup> of August, 2017. 

<a href="//egrc.ca">Educating Girls of Rural China</a> (EGRC) is an organisation dedicated to providing high school and university education to promising but disadvantaged girls from rural regions in Western China. For girls from these regions in China, their opportunities are often hindered by both financial hardship and a traditional preference favouring men in the community.
 
<!--more-->

<img src="http://www.acya.org.au/wp-content/uploads/2017/05/ERGC-1-300x225.jpg" alt="" width="300" height="225" class="alignnone size-medium wp-image-26303" />

The gift of education has the power to transform the lives of these young women confronted with social barriers and financial hardships, who will in turn change their lives of those in their community. As a volunteer English teacher, you will have the opportunity to participate in the fantastic work of EGRC, making a direct and positive impact on the future of these young girls. 

<h2>A successful volunteer applicant should be</h2>
<ul>
<li>Students or young professionals</li>
<li>Have demonstrated leadership in their community</li>
<li>Dedicated to the empowerment of women through education</li>
<li>Be culturally aware and sensitive</li>
</ul>

In order to apply, please send your CV and a cover letter detailing why you’d like to take part in this program and how you meet the selection criteria to <a href="mailto:education@acya.org.au">education@acya.org.au</a>

Applications close on 5pm (AEST), 25<sup>th</sup> June, 2017. 

"Educating Girls of Rural China makes a real difference in the lives of young women who would otherwise be left behind and forgotten. It also helps to identify and support the future leaders that China needs. What impresses me most about the organisation is its patient attention to each individual girl through every step of her advanced education."
- David Mulroney, Former Canadian Ambassador to China