{
  "title": "ACYA announces ShineWing Australia as ACELS Sponsor",
  "author": "ACYA",
  "date": "2017-06-18T03:49:04+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
ACYA is excited to announce <a href="http://www.shinewing.com.au">ShineWing Australia</a> as a major sponsor for the Australia-China Emerging Leaders Summit (ACELS), held in <a href="http://www.acya.org.au/acels/melbourne-2017/">Melbourne</a> 27-30 July 2017.

<!--more-->

ACELS is a signature bi-annual initiative of the Australia-China Youth Association (ACYA), this year in Australia bringing together 70 emerging leaders from around the country to partake in a 4-day professional development program. The Summit will deliver an intensive program focusing on the notion of “Connecting Through Culture”. This will feature seminars and panel discussions with prominent figures in the bilateral relationship, professional development workshops as well as networking opportunities. 

"ACYA is incredibly grateful for ShineWing Australia’s generous funding of the Summit’s events," said Georgia Sands, ACYA's National President. "We are excited to work with ShineWing Australia to present the Summit this year, the first time ACELS will be held in Melbourne, which presents an opportunity to expose the delegates to a range of new speakers and ideas throughout the program." As part of the Summit program, ACYA and ShineWing will be delivering the ShineWing ACELS Gala Dinner, at which delegates will be invited to hear from a prestigious keynote speaker.

With 80 years of operation in the Australian business landscape and more than 30 Partners in Australia, ShineWing Australia is a leading international accounting firm that has built up a strong reputation in providing high-quality assurance, business advisory, corporate finance and wealth management services to their clients. The ShineWing Australia member alliance with ShineWing International gives their clients access to ShineWing China – the largest indigenous Chinese domestic accounting practice, creating opportunities in specialist industries that provide local knowledge and real connections.

<h2>Contact</h2>
<ul>
<li>Samantha Jonscher</li>
<li><a href="mailto:media@acya.org.au">media@acya.org.au</a></li>
<li><a href="tel:+61420244612">(+61) 420244612</a></li>
</ul>