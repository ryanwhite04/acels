{
  "title": "ACYA to Partner with the State Government of Victoria for ACELS Melbourne 2017",
  "author": "ACYA",
  "date": "2017-06-27T06:01:46+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Trade Victoria"
  ],
  "resources": []
}
The Australia-China Youth Association (ACYA) has announced it will be partnering with the Victorian Government for the 2017 <a href="http://www.acya.org.au/acels/">Australia-China Emerging Leaders Summit (ACELS)</a>.

The bi-annual Summit is an initiative of the ACYA and will bring together more than 70 emerging leaders from around Australia for a 4-day professional development program.

Participants will attend seminars, panel discussions and professional development workshops centring on the Summit’s theme of ‘Connecting through Culture’.

<!--more-->

National President of the ACYA Georgia Sands said that the support of the Victorian Government through <a href="http://trade.vic.gov.au/">Trade Victoria</a> would help deliver an expanded Summit program.

<blockquote>We are thrilled to welcome the Victorian Government as the name sponsor for ACELS Melbourne and look forward to a full and exciting program for 2017.<cite>Ms Sands</cite></blockquote>
 
The ACYA and Victorian Government will also provide opportunities for alumni of the Hamer Scholarship program to participate in activities throughout the Summit.

The Hamer Scholarship Program provides opportunities for Victorian professionals to undergo intensive language study at partner universities across Asia, including China.

Hamer Scholars improve their language skills, gain the first-hand experience of the local culture, and develop business and people-to-people networks.

More than 250 Victorians have participated in the program since it began in 2012.

<a href="http://trade.vic.gov.au/">Trade Victoria</a> sits within the Department of Economic Development, Jobs, Transport and Resources and includes a global network of 20 Victorian Government trade and investment offices.

The offices are located in key markets including Asia, Europe, the Americas and the Middle East, giving Victorian businesses the information and support for global expansion. 
 
<h2>Contact</h2>
<ul>
<li>Samantha Jonscher</li>
<li><a href="mailto:media@acya.org.au">media@acya.org.au</a></li>
<li><a href="tel:+61420244612">(+61) 420244612</a></li>
</ul>