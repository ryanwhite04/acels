{
  "title": "Leizhou Volunteering Applications Open",
  "author": "April Liu",
  "date": "2017-12-17T13:00:07+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized",
    "Volunteering"
  ],
  "tags": [],
  "locations": [
    "Leizhou"
  ],
  "organisations": [
    "Leizhou College Students Volunteer Association"
  ],
  "resources": [
    {
      "title": "2017 Leizhou Summer Camp",
      "type": "image",
      "src": "2017-Leizhou-Summer-Camp.jpg"
    }
  ]
}
<p>Have you got any New Year’s Resolution? What about joining ACYA’s very first volunteering program in 2018 ?! With the successful collaboration with Leizhou College Students Volunteer Association (LZCVA) on its 2017 English Summer Camp, ACYA is excited to be opening applications for the 2018 Leizhou Volunteering Trip - English Winter Camp.</p>

<!--more-->

<p>This is a wonderful opportunity to teach English to middle school students and gain knowledge and understanding about education in rural China! You will also be hosted by local Chinese families/peers and experience authentic Chinese regional life.</p>

<p>The trip will take place from the 25th Jan to 4th Feb 2018, and applications are open to anyone with a passion for sharing Australian culture, the Australia-China relationship, and teaching English to children in rural China.</p>

<p><b>Applications close at 5 pm (AEST) on the 29th of December, 2017.</b></p>

<p>Please send your CV and a cover letter detailing why you’d like to take part in this program to ACYA National Education Director, April Liu, at <a href="mailto:education@acya.org.au">education@acya.org.au</a> or if you have any general enquiries about the trip.
You will be contacted following the closing date.
Hope to see you in Leizhou in 2018!</p>

<blockquote>
<p>In the end, everyone has their own objectives in participating in a volunteer program but whether it be learning Chinese, learning and assimilating into Chinese culture, forging lifelong friendships across the seas or even teaching a few more people about what Australia is really about - I think this trip has satisfied all of that, and everything beyond.</p>
<footer><p>Nancy Chan, 2017 ACYA Leizhou Volunteer</p></footer>
</blockquote>

<blockquote>
<p>Trying new things such as teaching English abroad and meeting new people who have become like family to me, my Leizhou volunteering experience was beyond amazing. I cannot wait to see what is in store for next year’s volunteers, it will be an unforgettable experience no doubt.</p><footer><p>Connor O'Brien, 2017 ACYA Leizhou Volunteer</p></footer>
</blockquote>
