{
  "title": "ACELS Beijing 2018 Applications Extended ",
  "author": "Liam Kearney",
  "date": "2017-12-13T09:38:45+08:00",
  "image": "",
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS BJG18 Banner FB",
      "type": "image",
      "src": "ACELS-BJG18-Banner-FB.png"
    }
  ]
}
Applications for the Australia China Emerging leaders Summit (ACELS) Beijing 2018 have been extended until **Friday 15th December 11:59pm AEST.**

![ACELS Beijing 2018 Banner](http://www.acya.org.au/wp-content/uploads/2017/12/ACELS-BJG18-Banner-FB-300x169.png)

Get your application in now to be part of ACYA's signature initiative - an unforgettable four days of presentations, workshops, panels, cultural events and networking with industry pioneers, government, leaders and future leaders in this bilateral space.

<!--more-->

The theme, *Broadening the Bilateral: Embracing a New Asian Paradigm*, will explore a region in the midst of unprecedented changes spanning tech, trade and politics to education, arts and media.

ACELS aims to equip delegates with the necessary tools to enhance their understanding of and navigate future challenges and opportunities in the Australia-China space. It is a unique platform to unite, develop and enrich a new generation of culturally and regionally literate youth.