{
  "title": "Alan McGuinness",
  "author": "Dawn Qiu",
  "date": "2017-12-27T13:15:52+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Alumni Spotlight",
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Alan McGuinness",
      "type": "image",
      "src": "Alan-McGuinness.jpg"
    }
  ]
}
It's time for our Alumni to shine in the limelight! This time Dawn chats with Alan McGuinness, our previous National Communications Director. Read on to find out about his experience in China and how he pursued his career path to work in the government sector today.

<!--more-->

- ## What was your graduation year?
  I graduated from my undergraduate studies (UTS) in 2009, and from my postgraduate studies (Macquarie) in 2013.

- ## Did you get a chance to take on any leadership roles within ACYA?
  I was a member of the ACYA Communications Committee in 2012, supporting the National Communications Director (the very talented Joel Wing-Lun). I was then fortunate to succeed Joel as National Communications Director the following year.

- ## What or who sparked your original interest in China?
  It was a rather circuitous route for me. In high school, I had a close group of friends that introduced me to films and music from Greater China, which started my love affair with broader Chinese popular culture. I also always had an interest in history, previously mainly focused on Rome and Greece. So I was keen to learn a lot more about Chinese history, both ancient and modern, and how it intersected with other great civilisations. Finally, my partner is Malaysian-Chinese-Australian, so Mandarin came into my life that way.
 	
- ## What has been your experience of living and studying in China?
  It has been a very positive experience. China has a habit of pulling me back every few years - from my exchange year in Hangzhou as a student, to my work in Shanghai in 2010, and now my current stint in Beijing since early 2016. It's completely cliche, but the nature and pace of change never cease to amaze me. No two visits or periods living here are the same - you can feel that dynamism and drive in the air (along with the occasional bout of air pollution, of course). Being witness to all of that is a privilege, and the experience is never static.
 	
- ## Could you tell us about your career path leading up to today?
  Sure thing. I did a Combined Bachelor of Arts at UTS, majoring in Public Communications and International Studies, which involved an exchange year at Zhejiang University in Hangzhou (2007). After graduation, I worked for the communications consultancy Burson-Marsteller, primarily for clients in the technology, telecommunications, finance and energy spaces. In 2010 I took on a role at the Shanghai World Expo as part of the Australian Pavilion team - a once in a lifetime experience. After that, I came back to Burson-Marsteller part-time and completed my Masters in Ancient History at Macquarie University. Upon graduation in 2013, I moved to Canberra as part of the graduate cohort for the then-Department of Resources, Energy and Tourism, where I did rotations across all three of those policy areas. I then worked in international resources and energy policy, followed by nuclear regulatory policy, within the Department of Industry, Innovation and Science. In February 2016 I moved to Beijing with my partner and began working at the Australian Embassy in Beijing. At the Embassy I've had roles in both the Industry, Innovation and Science team and the Public Affairs team. I'm currently the locally engaged manager for Industry, Innovation and Science.
 	
- ## What does your current work at the Embassy involve?
  It's a challenging and engaging role. I look after the small local team and support our two Counsellors from the Department of Industry, Innovation and Science. As the name implies, we cover policy areas spanning industry, innovation, science and research, but beyond that we're also responsible for areas including resources, energy commodities and the development of Northern Australia. That means on any given day, our team could be working on aspects as diverse as gas policy, science collaboration, the digital economy, and coal and iron ore trade and investment.
 	
- ## What influenced you to pursue a career in the Australia-China space?
  The vibrancy and importance of the relationship, as well as my personal experiences studying and living in China. I wanted the chance to grow my knowledge of the issues, and actually work on China-related policy. It has given me the opportunity to see aspects of the relationship up close, and to meet and interact with an ever-wider group of Chinese stakeholders and industry experts. As an added bonus, it has meant that I've been able to see more of the country itself.
 	
- ## What's one thing you love about your job?
  As noted in my earlier answer, no two days are the same. It's incredibly intellectually stimulating.
 	
- ## Did you imagine this would be your career? What would first-year you think of what you are doing now?
  I certainly did not! The first-year me simply wouldn't believe it - he would assume I'd be working in the private sector as a communications consultant. He would think it's exciting that I'm working at the Beijing Embassy, but not necessarily have an accurate picture of what that work entails.
 	
- ## Do you have any advice for students and recent graduates looking to work in your field?
  Absolutely - mainly that there's never only one way to get into this kind of work. Australian Government graduate programs across the board can offer opportunities, even if these are not immediately apparent. You don't have to become a graduate of the Department of Foreign Affairs and Trade to work on international policy, including China. Many Government portfolios do important work with and about China, have international teams, and sometimes also overseas positions. There are also locally engaged, contract positions at the Embassy and Consulates around China - keep an eye out for these opportunities, which are always posted online. Also consider the Australian Trade and Investment Commission (Austrade), and some of the excellent think tanks such as China Policy.
 	
- ## What was your time in ACYA like? What were the most valuable things you got out of being part of ACYA?
  It was a very engaging time, and taught me a lot about working with a group of incredible volunteers from diverse backgrounds. I was so impressed by the dedication and creativity coming out of the National Executive and the growing number of chapters across Australia and China. It was valuable meeting the individuals contributing their time and expertise to the success of the organisation, and watching it continue to grow and go from strength to strength. I also picked up valuable skills in remotely managing teams, and creating effective communication materials to showcase the membership.
 	
- ## What's your best memory from your involvement in ACYA?
  It's difficult to limit it to just one memory. So a few quick ones: engaging in lengthy, good natured Skype strategy and update sessions with my fellow National Executive members; the feeling when we finished the latest edition of our ACYA monthly newsletter, particularly when it contained an awesome interview and event coverage; and successfully collaborating with our sister organisations the Australia-China Young Professionals Initiative and the Australia-China Youth Dialogue (where I also went on to volunteer for several years).