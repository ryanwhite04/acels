{
  "title": "Jimmy Zeng",
  "author": "Dawn Qiu",
  "date": "2017-12-23T11:33:56+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Alumni Spotlight",
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Jimmy Zeng",
      "type": "image",
      "src": "Jimmy-Zeng.jpg"
    }
  ]
}
This week Hayden chats with previous ACYA President Jimmy Zeng, who discusses his interest and appreciation of Chinese culture, his experience in ACYA as well as current role at Shell.

<!--more-->

- ## When did you graduate?
  2013

- ## What has been your experience of living and studying in China?
  Varied. I’ve travelled to quite a number of different places in China. I can’t say I’ve lived in China long enough to form an opinion of what it’s like to actually settle down for an extended period of time, and my study experience is focused exclusively on my exchange semester in Hong Kong.
  
  Nevertheless, I do have a few specific things about these experiences I enjoy sharing.

  1. China is confronting to someone who has grown up in a largely “Western” socioeconomic environment. This can range from the behaviour of locals to the lack of regulations in certain aspects of life that a citizen of the Developed World would take for granted. Keeping an open mind is critical to get past this.
  2. China to a foreigner exerts what I’d describe as an inexorable gravity. This has pros and cons. It attracts some extraordinarily talented and focused people who are eager to learn about China, lives in China, and develop themselves in the process. Many people in this group come to China partly for professional reasons.But it also has a tendency to attract those who come to China because of the sheer fact that China gets talked about so often. I’d like to make it clear that this isn’t a negative perspective; not everyone has a fully formed idea of what they want out of an experience.This very binary set of outcomes provides a very rich opportunity to develop new perspectives about people, society, culture, and their relationships between each other, and I think China is even more bewildering for this ability to attract both wanderers and go-getters. In my time spent travelling to other countries, I have found this experience less pronounced, compared to China.
  3. China has an economic gravity that is so large it can be difficult to comprehend. Every time I thought I had grasped the essence of this, or come to some robust understanding, I would learn another fact that widened this perspective even further. It is both shocking, and eye-opening to observe the phenomenon that is the Chinese economy (whether this economy is in good or bad shape, I leave to the professionals to judge). I learned from this experience how to frame big problems, in big contextual frameworks. It’s been a very helpful skill if you want to call it that.

- ## Could you tell us about your career path leading up to today?
  Well, that’s certainly a haphazard topic for me to address. I think the notion of a path for most people is linear and one-directional, so on this account, I’m an anomaly.

  I started out studying Engineering and IT at UQ, after a period in secondary education tinged with scientific curiosity and, I admit, a rather questioning attitude to socioeconomic issues. This is the problem with an overzealously curious mind.

  I quickly found I didn’t enjoy IT and tried substituting it with Science to supplement the Engineering degree. It turns out I liked that even less, and eventually decided to give Economics a try because it sounded interesting. During this period I worked as a waiter, a tutor, an intern for the Brisbane Transport company which managed the Brisbane public bus fleet, learned a little about consulting at an Engineering Consultancy, and grew to enjoy my Electrical Engineering and Economics studies more over time.

  I fell into my current role in Oil & Gas more out of luck and circumstance than any real planning. I liked trying to understand issues relating to Energy Technology, Economics, and Policy. I completed a full-time summer internship at Shell’s Geelong Refinery in Victoria over the course of a summer where I also wrote my Honours Thesis. A job offer came shortly after, and here I am.

- ## What does your current work at Shell involve?
  My longer explanations whenever someone asks me this question tends to lose my audience a little due to the technical nature of the work, so I’ll keep this brief.

  In short, I provide engineering support for the measurement, analysis, and automatic control of various physical parameters that are required to operate oil & gas facilities. I draw from a variety of scientific and business knowledge in order to achieve this. It’s good fun.

- ## What influenced you to pursue a career in engineering?
  I’m the kind of person that likes to ask how things work. Somewhere along the way, Engineering sounded like a good fit based on what I knew about myself. I definitely didn’t grow up wanting to be an Engineer – there were no Engineering influences in my life up until university.

- ## What’s one thing you love about your job?
  The complexity and the travel. Each new problem I’m asked to solve is different in nature to the others, be it technical, managerial, or people-related. My current role also allows me some insight into contractual, commercial, and regulatory issues as well, which I’m quite grateful for. The travel keeps me moving and seeing new places. Again, I’ve been very grateful for this.

- ## Did you imagine this would be your career?
  Not at all. I don’t tend to believe that it’s possible to imagine a career. I’m quite sceptical of anyone who tells me they have a definitive career in mind since it is an intangible and personal concept. It’s one thing to be able to visualise a tangible outcome you want from a problem in the future, it’s another thing to think about your own life.

- ## What would first year you think of what you are doing now?
  Well, that’s definitely not what I had in mind.

- ## Do you have any advice for students and recent graduates looking to work in your field?
  It’s a tough industry (but then again, so are many other industries). If you’re attracted to this industry because of the headline salary figures, I’d advise looking elsewhere. I’ve seen far too many people “do it for the money” and they either get too comfortable or begin to question why they’re doing it after a few years. I’ve been working for 3 years in oil & gas now, and whilst I won’t rule out the possibility of future career changes, I’m very happy with the position I’m in now (all things considered).

  There’s a social aspect to working in oil & gas that I think doesn’t get enough perspective in mainstream media and popular conversation. 97% of the world’s cars still depend on gasoline, a crude oil derivative, and natural gas is a far cleaner burning fuel for electricity generation and industrial applications than coal or oil products. There is a lot about modern life that depends on these two chemical compounds, and I think it is easy to convince oneself that all the environmental and ecological problems of the world would be solved by a miracle switch to renewable sources. This is not a very realistic position, but it is also the public opinion. For students and recent graduates who would feel deeply uncomfortable having the “sins of the world heaped on your feet”, it might be less psychologically stressful to considering an alternative career.

- ## What was your time in ACYA like?
  It was four of the most fulfilling years of my life, and I still look back on it very fondly. I doubt I’ll ever forget those years. I was given the opportunity to develop myself personally and professionally in previously unexpected ways (all positive in the end!), and I’ve met some truly incredible young people. It’s been a very humbling experience.

- ## What were the most valuable things you got out of ACYA?
  I think it might be better for me to reframe this a little if you’ll excuse the liberty. I don’t think I ever got anything valuable “out of ACYA”.
  I spent four years putting things “into ACYA”. New events, new initiatives, motivating people, learning how to think operationally, managerially, and strategically so that ACYA could grow as an organisation and develop both the people in it and the people who we counted as our community.

  So I’d like to say that the most valuable thing anyone can do in an NGO, is to put effort into it. If you’re looking to get something out of it other than an opportunity to learn and develop skills and abilities you might not already have, whilst contributing to a community of people from both Australia and China, perhaps your time would be better spent elsewhere.
  I have always seen the greatest value one can give and get out of ACYA to come from a position of community participation.

- ## Did you get a chance to take on any leadership roles in ACYA?
  Yes. I spent my first year as the Sponsorship Director for the University of Queensland Chapter, then the next year as its Chapter President.
  I joined the National Executive the year after as Australia Manager, followed by a year as National President.

- ## What benefits did you get from it?
  I think ACYA got all the benefits out of me!

- ## What’s your best memory of your involvement in ACYA?
  I have two.

  In my year as National President, ACYA achieved the previously unimaginable and hosted a youth conference in Shanghai. In hindsight, it seems that this absolutely thrilling development helped set the stage for ACYA’s continued growth that I am seeing today. It was a truly proud and emotional moment for me to be in Shanghai opening that conference.

  The second memory that comes to mind is the day I stepped down as National President. I left a farewell message, if you will, on Facebook, and received a rather unexpected reception to it. I hadn’t realised how far ACYA had come in four years until the likes and the positive comments stopped coming in. It still makes me feel great to think about it.
