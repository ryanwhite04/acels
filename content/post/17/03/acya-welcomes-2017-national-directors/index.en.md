{
  "title": "ACYA Welcomes 2017 National Directors",
  "author": "ACYA",
  "date": "2017-03-09T11:38:14+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "AGM",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
After receiving a record number of competitive applications, the 2017 ACYA National Executive Committee is excited to welcome the 2017 ACYA National Directors to their appointed positions:
<ul>
 	<li><strong>Careers Director:</strong> James Tang</li>
 	<li><strong>P2P Director:</strong> Cormac Power</li>
 	<li><strong>Education Director:</strong> April Liu</li>
 	<li><strong>Business Development Director:</strong> Tom Linnette</li>
 	<li><strong>Translations Director:</strong> Joanne Tse</li>
 	<li><strong>Media Director:</strong> Samantha Jonscher</li>
 	<li><strong>Publications Director:</strong> Charlie Lyons-Jones</li>
 	<li><strong>IT Director:</strong> Ryan White</li>
</ul>
<!--more-->

A selection panel was formed from the recently elected National Executive Committee, and the applicants’ online interviews and written applications were assessed on the basis of the following criteria:
<ul>
 	<li>Demonstrated experience managing organisational partnerships and stakeholder relations;</li>
 	<li>Experience managing across cultures, especially Aus-China;</li>
 	<li>Comfort with and knowledge of the role/time commitment;</li>
 	<li>Attitude &amp; personality (willingness to learn &amp; cooperate with a team); and</li>
 	<li>Potential benefit of the role to the candidate.</li>
</ul>
ACYA General Manager - Portfolios and panel chair, Jacinta Keast, expressed her pleasure to see such a high standard of applications. "I'm very excited to work with such a talented, passionate group of people. I'm confident that ACYA in 2017 is going to go from strength to strength in our work in across all of the National Portfolios!"

Scott Gigante, the ACYA General Manager - Portfolios for 2016, expressed his gratitude to the 2016 National Directors. "Over the course of 2016, we have expanded partnerships in business, education, government and the arts. ACYA's web, email and social media publications throughout the year shed light on the genuine people-to-people engagement occurring between young people in the Australia-China space, and the National Portfolios continued to provide excellent resources for our chapters to flourish in both Australia and China. Many thanks to our 2016 National Directors who poured their hearts and souls into their work, and best of luck to the incoming team for 2017."

Congratulations to the New Directors and thank you to all applicants for the ACYA passion evident in their applications. We are confident in the strong 2017 National Team and look forward to working with our Chapters to maximise opportunities for all members in the Year of the Rooster!