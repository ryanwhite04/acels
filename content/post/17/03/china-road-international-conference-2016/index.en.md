{
  "title": "China Road International Conference 2016",
  "author": "ACYA",
  "date": "2017-03-06T02:32:29+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_9884",
      "type": "image",
      "src": "IMG_9884.jpg"
    },
    {
      "title": "IMG_9883",
      "type": "image",
      "src": "IMG_9883.jpg"
    },
    {
      "title": "IMG_9898",
      "type": "image",
      "src": "IMG_9898.jpg"
    },
    {
      "title": "IMG_9907",
      "type": "image",
      "src": "IMG_9907.jpg"
    }
  ]
}
<em>ACYA National Education Officer Declan Fry recently spoke to Kate Kalinova, a delegate at the 2016 China Road International Conference. </em>

The China Road conference, sponsored by the Chinese Academy of Social Sciences and the University of Newcastle, saw speakers and delegates from China and around the world giving panel and paper presentations on the concept of the ‘China Road’ from a range of perspectives, including philosophy, Marxism, economics, politics, society, education, culture, different forms of democracy, and international relations in the Asian Century.

<!--more-->

<a href="http://www.acya.org.au/wp-content/uploads/2017/03/IMG_9884.jpg"><img class="size-medium wp-image-25228 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2017/03/IMG_9884-300x225.jpg" alt="" width="300" height="225" /></a>

Kate is an honours student in International Relations at the University of New South Wales and was an English Teacher in China with the AIESEC Global Citizen program. She began to study Mandarin during a one year long academic exchange in Spain, continued her studies in UNSW, and completed a three-month language program at National Taiwan Normal University.

In this interview, she discusses the process of studying Chinese, China’s place in the 21<sup>st</sup> century, and how she began her journey to learn more about China.

<strong><em>Declan:</em> <em>How do you feel China’s culture and history interact with its growing power and geostrategic significance? Is it possible for scholars to combine the two in their work and research?</em></strong>

<strong>Kate:</strong> Well it’s probably hard to avoid drawing connections between the ‘Rising China’ of today and the mythical aura of the ancient Chinese civilization, since discussions of China tend to be permeated with notions of Confucianism, legacies of the ‘tribute system’, and so on - all of which are tied not only to China, but also to its relation with its neighbours.

However, I do not think that history and culture is restricted only to starry-eyed academics that spend their days puzzling over ancient Chinese manuscripts.

And indeed, carefully avoiding the trap of ‘Chinese exceptionalism’, I would argue that since <em>all</em> nations are shaped by their culture and history, it is not only possible for scholars to combine the study of cultural and material (i.e. geostrategic) factors, but vital.

Of course, as an aside, we should keep in mind that since all scholarly inquiry must limit itself to pursuing a particular strand of complexity, it is understandable and expected that some scholars focus on, let’s say, geography, and others trace the linguistic evolution of a nation’s language(s).

However, in regards to China there is one particular point that I feel is of striking importance. When I first became interested in China one of my professors told me that ‘You can never understand China unless you go there’, and indeed the more I study the more I feel a strong resonance with this notion. I think that there are certain things that can never be learnt second-hand, whether in books or academic journals - even if you attempt to supplement this with things from popular culture (such as watching Chinese drama or listening to Chinese music), or interactions with the Chinese diaspora abroad.

So I think that any academic (or individual) who is deeply interested in China should live there and experience its culture and history <em>from within</em> – which is something that is quite noticeable among ‘China experts’ within academic, diplomatic, <em>and</em> business sectors, and a good point to take note of for all those budding ‘China enthusiasts’ out there.

<strong><em>Declan:</em> <em>You’ve studied Mandarin both inside and outside China – in Spain, at UNSW, and most recently in Taiwan. What were the benefits and disadvantages of studying a language in three different countries? Did you learn different things about the language from seeing it in different contexts?</em></strong>

<strong>Kate:</strong> (laughs). Well, apart from the fact that studying Mandarin in Spain was a bit of a paradox, I can indeed say that there were noticeable differences I experienced among the three environments.

First of all, teaching style is something that was instantly clear as a key factor. Whilst in Spain I was in a very, let’s say, ‘relaxed’ environment where the expectation was to learn 250 characters after a year of study, and in Sydney the pace was significantly faster - at UNSW you covered 500 characters <em>in one semester</em>.

The point here is a very interesting one, because I think what contributes to these different approaches and expectations is not only the university culture (to generalise we might say it is more relaxed in a provincial city in Spain as opposed to a highly competitive, on an international scale, institution in Australia) but also the presence of the studied language in daily life.

So, in Zaragoza there were maybe three Chinese restaurants in the whole city and you might expect to see a few Asian faces on the street every day, but it was far from a common sight. The reality in Sydney is strikingly different, and indeed the classes offered by the Chinese Language faculty were split to cater for ‘Background’ and ‘Non-Background’ students.

This brings me to the second point I want to emphasise – the importance of studying a language in its spoken environment.

This is of course something of a ‘truth universally acknowledged’ and indeed, when I was studying Chinese in Taiwan, one of the chapters in the textbook actually discussed why Taiwan was a particularly good place to study – and the <em>huanjing</em> (cultural environment) factor, alongside the classic ‘preserved traditional Chinese culture’ argument, was clearly emphasised!

<strong><em>Declan:</em> <em>So which did you prefer?</em></strong>

<strong>Kate:</strong> Clichés aside, actually I think ‘in-country’ is indeed hands down the best way to go – and that’s exactly why I plan to go to China to continue my language studies rather than pursuing them in Australia (although, admittedly, Australia is probably a better candidate than Spain).

My study in Taiwan also gave me a lot of insight into the particularities of studying Mandarin in different places within the ‘Greater China’ space. So, first of all, anyone trying to decide where to study Mandarin should consider the ‘characters question’. What I mean is of course that while Taiwan and Hong Kong continue to use traditional characters, the Mainland has switched to a simplified system. So, apart from the debate regarding the level of difficulty (which I think is not so significant – as, in my opinion, one can grow accustomed to either system after a month or so of study) it is very important to think about your future aspirations and goals.

In other words, as a businessman, unless you have a particularly strong desire to focus on Hong Kong, traditional characters are probably not the way to go and will not be of particular use. Now, on the other hand, if your interest lies in linguistics, anthropology, culture, or a related discipline, you might decide that traditional Chinese is where your passion leads you.

I, of course, am speaking very generally, as everyone has their own reason for studying Chinese and will need to consider a vast array of factors in making a decision.

Moving on, the second point that is important relates to spoken Mandarin. Now, although we have established that learning Mandarin in China is the best way to go, dialects and, more broadly, the style of speaking has a role to play too. So, whilst Taiwan was a great place to study and practice speaking/listening to Mandarin, I have become used to the ‘Taiwanese way’ and therefore find accents from Northern China difficult to understand. Furthermore, since Taiwanese people, at least in Taipei, seem more relaxed about the ‘tones’ (or are used to listening to foreigners fail at the tones), I have developed a terrible habit of expecting people to understand me without using the correct tone – something that became very much apparent when I tried to ask for directions in Beijing during a short visit in January.

Of course, these are all particularities of language study – the ‘little things’ rather than the ‘big problems’ – so I want to emphasise that, so long as you work hard, no matter where you study you will be able to attain a high level of proficiency. Furthermore, there are numerous ‘tricks’ to overcome the accent problem – such as watching dramas or films of a particular region, finding friends to practice with, and so on.

These (boring) practicalities aside, I think what we should also keep in mind that China is an incredibly diverse country, and the place you choose to study will give you a glimpse of a particular facet of China – all of which are equally fascinating and exciting! So, perhaps you can say that whilst the practicalities might lead you to choosing a place, it is the place that will shape the evolution of your particular ‘China story’.

<strong><em>Declan:</em> <em>You’ve said elsewhere that you feel that, as the international relevance of the Asia-Pacific continues to increase, China will play a significant role in shaping international relations in economic, political and cultural spheres. How do you believe it will do this? How does this fit in with political and cultural relationships in the wider Asia-Pacific space?</em></strong>

<strong>Kate:</strong> Well this question touches on some sensitive discussion points, especially in an Australian context, so it is a little difficult to think of exactly the right way to frame my opinion.

Certainly it’s widely acknowledged that we are living in something called the ‘Asian’ or ‘Asia-Pacific’ Century, and most of us can agree that China’s economic rise holds deep significance for the region. However, that is, in a way, where the certainty ends and the debate takes over – ‘Can China rise peacefully?’; ‘Will the stability of the region come to an end?’; ‘Will China challenge the existing order?’ are things most of us have heard before.

I think this last question is probably the most relevant to what you asked- since, really, if we wish to talk of China’s political and cultural influence in the region, the topic of norms cannot be avoided. So the debate over ‘Asian values’, the legacy of Confucianism and so on and so forth is, inevitably, tied to whether China’s rise will challenge the ‘Western system’ that is already in place – globally and, to some (debatable) degree, within the Asia-Pacific.

It seems to me that what we have seen increasingly in the discussion is the notion that there is a shift in China’s political influence: from ‘rule-taker’ to ‘rule-maker’ (with the decrease in the prevalence of the ‘China threat’/‘rule-breaker’ rhetoric). This suggests that indeed economic changes in the region might become tied to more fundamental socio-cultural changes in the coming decades.

Yet it remains true that there is what one may call the ‘elephant-in-the-room’: the role of the United States as a regional and global power.

I am hesitant to comment on how this will impact on political and cultural relationships in the wider Asia-Pacific space, since I think that Asia is characterised by its long history of regional interconnections, not just among the so-called ‘major-players’ but among all nations. Thus, without knowing these complex details, especially in regards to internal dynamics, is hard for an outsider, especially one with no claim to ‘expert’ status, to say how China’s increasing influence will shape the region in the coming years. While economics matter, history is not a factor one can avoid.

So, overall, I think that whilst China’s role will indeed be ‘significant’, what is more interesting is considering not how China influences the ‘region’, but rather how a multitude of regional actors (state and non-state) interact with each other, all moving within a complex web of narratives, history, culture, economics, globalisation, climate change and, of course, politics, to mould a ‘new’ Asia-Pacific.

<strong><em>Declan:</em> <em>You’ve very passionate about China at present – were there any events in childhood or high school that led you toward your current enthusiasm and interest for the country? Or do you believe a particular experience or circumstance, or aspect of your personality, set you on this path?</em></strong>

<strong>Kate:</strong> Well, although I’m someone who has always been interested in foreign languages and cultures – which is indeed what led me to study International Relations and Spanish in university - my ‘China story’ is something that can be traced to its beginnings in 2014.

It started out quite innocently, as I picked a subject called ‘Regional Dynamics’ in university, driven by a desire to ‘fill my knowledge gap’ regarding Asia, which I had not studied for the first two years of my degree (incidentally, in the same semester, I chose a ‘Middle East’ subject with the same intent).

However, my professor piqued my curiosity with their discussion of Chinese philosophy, worldview, values, and aspirations and as I delved into researching the topic of Chinese civilisation and its relationship with Taiwan for an essay I became more and more captivated by the story of the Middle Kingdom.

That was only the beginning.

I plunged into Chinese drama, film, music, took some basic Chinese language lessons from my friends, and dabbled in reading English translations of Chinese classics such as the<em> Romance of the Three Kingdoms </em>and <em>Dream of Red Mansions.</em>

Now, alongside all these developments I was preparing for my year-long exchange in Spain (set to start in September 2014) and pursuing a vague goal of preparing for further post-grad study in Latin America. So because I wanted to experience something of the ‘developing world’ that was at the backdrop of my future plans and, also, keen to get a ‘glimpse’ of this fascinating country – China- I signed up for an AIESEC Global Citizen exchange program to teach English in China for 5 weeks as a stop-over on the way to Europe.

It was, as you may have guessed, a stop-over that would dramatically change my future plans and aspirations. What I discovered, upon arriving in Beijing, was that the China I had read about, the China I had <em>imagined</em>, wasn’t really there.

How could this be? After all, I had never been to China before, and what I expected to find was therefore something with no real-life foundation.

Of course, the story didn’t end there (otherwise, how could I be as engaged with the Australia-China space as I am?).

For indeed there was another, very real China, that made me fall in love.

Although there were many things I hated – such as the absence of blue sky and the ‘post-card tourism’ that permeated many places of ancient culture – once I overcame the disillusionment that occurs when dreams and reality clash I realised that the reality was more fascinating, exciting, and potent than I could ever have expected.

This is why I think that anyone who is truly interested in understanding and exploring another country, language, or culture must travel and live there - and not as a tourist, but as someone who interacts with locals and gets a glimpse of the day-to-day lifestyle that lies beneath the veneer of sight-seeing and dimply-lit museums.

So, to return to your question, I think that while I had numerous ‘latent’ factors – such as an interest in foreign languages, history, culture, and art - that facilitated the blossoming of my fascination with China, the key event that translated my <em>interest</em> into <em>passion</em> was spending time within China (which is why I plan to return there after completing my Honours).

<strong><em>Declan:</em> <em>Do you feel there are any concrete steps either Australia or China could be taking to improve their cross-cultural relationship and understanding of each other?</em></strong>

<strong>Kate: </strong>That’s a pretty hard question to answer, since a lot is being done already to promote these links.

I guess there are two strands to this problem – first, getting people interested in the other country (the ‘hook’ if you will); and second, helping them to pursue that interest (something we may call the ‘opportunity factor’).

In my opinion I think that education is definitely a vital pillar – so the 2012 <em>Australia in the Asian Century</em> White Paper and the general push for ‘Asia literacy’ is a great initiative, especially alongside attempts to deepen the engagement at a primary school level though groups such as the Engaging China Project. And similarly, Australian study centres within China and the promotion of exchange partnerships among Australian and Chinese universities are a great way of building cultural understanding and awareness through people to people ties.

However, I think that something both Australia and China can improve on is the use of ‘soft power’ or popular culture. The three great pedestal examples of countries that excel at this are, of course, the United States, Japan, and South Korea. And although China is clearly aware of this factor and is promoting music, film, and building a network of Confucius Institutes, it is something that does not yet match China’s economic influence, and is therefore likely to grow in the coming decades.

In regards to the second point, I definitely think that ACYA is a great initiative and that promoting relations though student exchange is a great way to go – perhaps it could be broadened to include more professional opportunities such as internships? One of the topics that often comes up in conversation is the difficulty of adapting to the business culture abroad, so I think having more internships (especially a structure that facilitates them) will be a great way of building culture-savvy young professionals.

Sure, there are already opportunities out there – ACYA promotes AusTrade and AustCham internships, AIESEC also offers internships as well as volunteering programs, and, generally speaking, there are plenty of others options out there for young people to make use of, such as CRCC Asia, or the highly competitive New Colombo Plan Scholarship – but I think that when you compare it to student exchange and language class opportunities the gap is quite wide.

I think the key point is improving the accessibility of opportunities that people can make use of – so while at this stage internships are not impossible, I think that often the cost and safety factors play a big role in discouraging people from applying. So, as someone who has relied greatly on the university framework, I think that universities are well placed to address this gap, especially since a lot of them are increasingly Asia-orientated and are set upon pursuing a ‘China strategy’ (with the same ‘global’ outlook being evident within China).

<strong><em>Declan: Thank you so much for talking with us Kate.</em></strong>

<strong>Kate: </strong>Thank you.