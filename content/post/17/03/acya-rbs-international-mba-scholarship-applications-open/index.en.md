{
  "title": "ACYA-RBS International MBA Scholarship Applications Open",
  "author": "ACYA",
  "date": "2017-03-01T01:45:22+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "p7784181a522399786-ss",
      "type": "image",
      "src": "p7784181a522399786-ss.jpg"
    }
  ]
}
The Australia-China Youth Association (ACYA) and <a href="http://mbaen.rbs.org.cn/">The Renmin University of China Business School (RBS)</a> is proud to announce that applications for the 2017 ACYA-RBS International MBA Scholarship are now open!

The ACYA-RBS International MBA Scholarship began in 2015 as ACYA's first scholarship program in mainland China. The Scholarship will provide an ACYA member with a full scholarship for the 2-year degree.

<!--more-->

You can learn more about the scholarship by reading the <a href="http://www.acya.org.au/2014/10/interview-shaun-harkness-inaugural-recipient-acya-rmb-imba-scholarship/">Australia-China Perspectives Blog interview</a> with the scholarship’s inaugural recipient, Shaun Harkness.

Through applying for this scholarship, applicants will also be considered for government and city based scholarships on offer at RUC. RBS International MBA students also have the opportunity to undertake exchange with multiple other prestigious universities around the world!

Interested applicants should read the application requirements <a href="http://www.acya.org.au/education/scholarships/acya-ruc-imba-scholarship/">here</a> and submit their application to <a href="mailto:education@acya.org.au">education@acya.org.au</a> by <strong>March 31</strong>.

For more information on the scholarship and the International MBA program please visit <a href="http://mbaen.rbs.org.cn/">http://mbaen.rbs.org.cn/</a>. General enquiries about ACYA scholarships should be directed to the ACYA National Education Director Chloe Dempsey (<a href="mailto:education@acya.org.au">education@acya.org.au</a>).

ACYA once again reiterates its delight at being able to provide this opportunity to our members and we wish you the best of luck in your applications!