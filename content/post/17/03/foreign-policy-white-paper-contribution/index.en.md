{
  "title": "Foreign Policy White Paper - Outstanding Contribution from ACELS Beijing",
  "author": "ACYA",
  "date": "2017-03-10T09:44:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS Foreign Policy Panel",
      "type": "image",
      "src": "IMG_2130.jpg"
    },
    {
      "title": "ACYA-White-Paper-Submission",
      "type": "application",
      "src": "ACYA-White-Paper-Submission.pdf"
    }
  ]
}
In the lead-up to the recent <a href="/acels">Australia China Emerging Leaders Summit</a> in Beijing, ACYA invited submissions from its members and attendees of the Summit in regards to the Australian Government’s <a href="http://dfat.gov.au/whitepaper/index.html">Foreign Policy White Paper</a>.

<!--more-->

These submissions were considered and vetted by ACYA and the Australian Embassy Beijing before being slated for discussion during a special policy workshop session at ACELS with special guests including Gerald Thompson, Deputy Head of Mission to China at the Australian Embassy, and David Kelly, Director of Research at China Policy.

These chosen submissions were further publicly submitted by ACYA to the White Paper public policy consultations. One submission particularly stood out and considering its author, who was recently appointed ACYA’s National Publications Director, we would like to share it publicly because of its quality and innovative approach to foreign policy in the region. Congratulations Charlie Lyons Jones and welcome to the team!

You can read Charlie's submission <a href="http://www.acya.org.au/wp-content/uploads/2017/03/ACYA-White-Paper-Submission.pdf" target="_blank" rel="noopener noreferrer">here</a>.

<strong>About the Foreign Policy White Paper</strong>

The Australian Government is developing a Foreign Policy White Paper – a comprehensive framework to guide our international engagement over the next five to ten years. This will be Australia’s first foreign policy White Paper since 2003.

The international environment has changed profoundly over the past 13 years. At the same time, Australia’s engagement with the world has continued to expand.

We need to ensure our international engagement continues to deliver prosperity and security for all Australians. The White Paper will provide a roadmap for advancing and protecting Australia’s international interests and define how we engage with the world in the years ahead.