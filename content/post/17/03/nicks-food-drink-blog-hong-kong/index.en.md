{
  "title": "Nick's Food and Drink Blog- Hong Kong",
  "author": "ACYA",
  "date": "2017-03-06T02:38:23+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Picture3",
      "type": "image",
      "src": "Picture3.png"
    },
    {
      "title": "Picture2",
      "type": "image",
      "src": "Picture2.png"
    },
    {
      "title": "Picture1",
      "type": "image",
      "src": "Picture1.png"
    }
  ]
}
I'm about to commit a cardinal sin.  Whereas my last two blog posts have featured places to eat and drink in Sydney, I've decided, in lieu of my visit there last month, to venture beyond the seas to Hong Kong in this latest of posts.

<!--more-->

Despite living in Hong Kong for some four years, I'm very much a visitor to Hong Kong.  Regrettably, I was one of those 'third culture' kids who attended an international school and had parents who were expatriate workers; in my case, a father who was, boringly, with P&amp;O, the British shipping firm.

Going back to Hong Kong is always a thrill.  I try to go back once a year, but every time no matter how often I return, I still feel very much a tourist.  I'm constantly struck by that wonderful cliché of 'East meets West', where the markets of Fa Yuen Street cut onto the Sino-<em>Hebridean</em> Argyle Street, and where the Hong Kong Police Force still somehow manages to muster a marching pipe band.

I doubt any of our ACYA Hong Kong friends would need my suggestions as to where to go, but I thought I'd share some of my experiences, perhaps for the benefit of our Australian based ACYA members; for those who might head to Hong Kong in the near future.

<strong>Where did I go to eat?</strong>

<u>Kau Kee (</u><u>九記牛腩)</u>

<em>21 Gough St, Central, Hong Kong</em>

When I directed my taxi driver to "Kau Kee", he wished me luck; it'd been almost a decade since he last went, and everyone knows the patience, or rather lack of, a Hong Kong taxi driver has.  As we pulled up, I saw that interminable line that I always see, curving around Gough Street, composed of that familiar mix of locals and tourists; some having come directly from the airport with their luggage.  My taxi driver says "it's a good bowl of noodle, but it sure isn't worth the wait".  I begged to differ.

Kau Kee has been a fixture in Hong Kong since the 1930's, serving their signature beef brisket noodle soup to generations of Hong Kong people.  Indeed, my mother lived around the corner of Wellington Street, and she recalls Kau Kee noodles with fond familiarity.

<em>What to expect: </em>A thirty to forty-five minute wait.  In typical Hong Kong style, service is tepid, and you'll probably be seated next to complete strangers, unless you are, unadvisedly, travelling in a pack.

<a href="http://www.acya.org.au/wp-content/uploads/2017/03/Picture1.png"><img class="alignnone size-medium wp-image-25237" src="http://www.acya.org.au/wp-content/uploads/2017/03/Picture1-225x300.png" alt="" width="225" height="300" /></a>

<em>What to order: </em>Beef brisket e-fu noodles (上湯牛腩伊麵), and a cold Ovaltine for the adventurous.

<a href="http://www.acya.org.au/wp-content/uploads/2017/03/Picture2.png"><img class="alignnone size-medium wp-image-25236" src="http://www.acya.org.au/wp-content/uploads/2017/03/Picture2-300x300.png" alt="" width="300" height="300" /></a>

<strong>Where did I go to drink?</strong>

<u>Foxglove</u>

<em>Printing House, 2/F, 6 Duddell St, Central, Hong Kong</em>

As you could probably tell from my first post, I'm all for speakeasies.  Those well-hidden bars tucked away in alleyways have taken off all over the world; and Foxglove is that quintessential speakeasy.  To the untrained eye, it's disguised as an umbrella shop; its namesake a homage to the English umbrella firm, Fox.  It's a rather bizarre sensation, expecting a martini with friends, only to end up in an umbrella shop.

One enters the 'shop' and approaches the cashier, where an attendant usually looks at you with that sly smile of a cunning prankster.  For the uninitiated, you either look gleefully excited, dreadfully confused, or a mixture of both.  Sooner or later, you notice the umbrella stand in front of the cashier, and upon pulling one of the umbrella handles, a door, previously a display cabinet, slides wide open.

The bar itself is reminiscent of a well-bedecked plane cabin.  A rotating fan on the wall mimics a spinning turbine.  Drinks are incredible and the whisky selection is first class.  If you're a gallivanting tourist like I was, I'd recommend the 1987 Laphroaig 'Silver Seal' 16 year old; there are only 190 bottles in the world!

Of course, a bar would be incomplete without music.  On that score, you can most certainly count on Foxglove to 'deliver the goods'.  There are usually live jazz gigs every night.  Unpretentious and smooth, it's definitely a comforting tonic for a long day of touristy wandering.

So there you have it, Hong Kong, a city so dear to me, summarised into two food and drink suggestions. If you have your suggestions, why not leave a comment below?

<a href="http://www.acya.org.au/wp-content/uploads/2017/03/Picture3.png"><img class="alignnone size-medium wp-image-25235" src="http://www.acya.org.au/wp-content/uploads/2017/03/Picture3-300x300.png" alt="" width="300" height="300" /></a>

Nick Yuen is a third year Arts/Law student, and the views expressed here are solely his own.