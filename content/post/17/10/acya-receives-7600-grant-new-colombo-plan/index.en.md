{
  "title": "ACYA Receives $7,600 Grant Under The New Colombo Plan",
  "author": "ACYA",
  "date": "2017-10-06T10:10:10+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "image1",
      "type": "image",
      "src": "image1.jpg"
    }
  ]
}
The Australia-China Youth Association (ACYA) is pleased to announce that the Australian Embassy Beijing, as part of the New Colombo Plan (NCP), has provided a grant of $7,600 (35,500RMB) to help support a variety of ACYA’s initiatives in China.

<!--more-->

The grant has supported four projects. The first was the ACYA Volunteering trip in Leizhou, China which took place from the 15th-25th July. The trip supported selected ACYA members and NCP students to travel to Leizhou to create closer interpersonal connections between Australia and China. Volunteers stayed with youths in the community and taught English classes to high school students, being immersed in the local culture of rural China, whilst promoting Australian culture.
<blockquote>In the end, everyone has their own objectives in participating in a volunteer program but whether it be learning Chinese, learning and assimilating into Chinese culture, forging lifelong friendships across the seas or even teaching a few more people about what Australia is really about- I think this trip has satisfied all of that, and everything beyond.

<footer>Nancy Chan, who was a volunteer on the trip

</footer></blockquote>
The grants are also supporting ACYA’s regional chapters to put together guides to their cities for prospective Australian students and New Colombo Plan scholarship and grant recipients. “The cities of Wuhan, Chengdu and Guangzhou all offer excellent academic and immersion opportunities that challenge Australian students to engage with another side of China outside Beijing and Shanghai”, said Chloe Dempsey, ACYA’s China General Manager, who previously spent six months in regional Sichuan.

Other projects include an ongoing program of collaboration between the ACYA and Educating Girls of Rural China, as well as support for tours to art galleries in Beijing.

The New Colombo Plan is a major initiative of the Australian Government that aims to facilitate greater understanding between countries in the Indo-Pacific region. As such, ACYA is incredibly grateful for the government’s ongoing support of the organisation’s activities.
<h2>Contact</h2>
Chloe Dempsey

China General Manager

<a href="mailto:gm_china@acya.org.au">gm_china@acya.org.au</a>