{
  "title": "ACYA and China Australia Millennial Project partner to create AusChina leadership funnel ",
  "author": "ACYA",
  "date": "2017-10-10T06:42:27+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Australia-China Youth Association",
    "China Australia Millennial Project"
  ],
  "resources": [
    {
      "title": "CAMP Logo",
      "type": "image",
      "src": "CAMP-Logo.png"
    },
    {
      "title": "CAMP_LOGO_Horizontal_fullcolour",
      "type": "image",
      "src": "CAMP_LOGO_Horizontal_fullcolour.png"
    }
  ]
}
<i>Addressing lack of Asia-readiness within Australia’s business community</i>

<img class="alignnone size-medium wp-image-26783" src="http://www.acya.org.au/wp-content/uploads/2017/10/CAMP_LOGO_Horizontal_fullcolour-300x121.png" alt="" width="300" height="121" />

<p>The <a href="http://www.acya.org.au">Australia-China Youth Association</a> (ACYA) and the <a href="http://www.australiachina.org/">China Australia Millennial Project</a> (CAMP) have announced a partnership aimed at building the capacity of Australian CEOs and business leaders to engage with Asia, through the deepening of essential cross-border connections and expertise.
</p>

<!--more-->

<p>
A recent PWC ‘Match Fit’ survey found that up to 90% of ASX 200 companies were not Asia ready, so the partnership is another progressive step forward by today’s youth, according to CAMP CEO Andrea Myles.
</p>

<blockquote><p>With both China and Japan as our first and third largest trading partners, and the mining boom behind us, the future of Australia relies on minds, not mines. Australia's brightest millennials know that having 90% of the Australian ASX 200 with no Asia experience simply isn't good enough.</p>

<p>This is why over 5000 Australians are heading to China each year to gain the skill sets, networks and confidence, needed to succeed in the 21st century. We are seeing millennials not only be ready for leadership tomorrow but they are actually outstripping the leadership capability of boards of today, are interested in disruptive business models and keen to shake things up. For anyone under 40 wanting to fast-track their career, China is a fantastic horse to bet on and CAMP is an efficient way to get there.</p>
<footer> - Andrew Myles</footer>
</blockquote>
<br/>

<p>With a network of 19 Australian chapters and 8 Chinese, ACYA is looking to the partnership to reconnect Australian-educated Chinese talent with the business and professional landscape in Australia, while CAMP is seeking to spread its vision of connecting 100,000 Australians with the new opportunities in China by 2025.</p>
<p>According to ACYA’s President, Georgia Sands, the partnership is a perfect match to help progress bilateral people-to-people relationships and career networks amongst emerging leaders.</p>

<blockquote>University students and young professionals across Australia and China look to ACYA as a bilateral community and a central hub of opportunities they can tap into throughout university and as they transition to early career professionals. We are particularly excited about this partnership expanding opportunities for our members and alumni to engage with China as one of Asia’s innovation centres.

<footer>- Georgia Sands</footer>
</blockquote>
<br/>

<p>CAMP is driven to strengthen the Australia-China relationship, through its award-winning cross-border 100-day innovation program that unites dynamic young Australian and Chinese leaders and provides them with the tools to address real-world global challenges.</p>

<p>"We’re intent on changing Australia, relationship by relationship,” continues Myles. “Our goal is to connect 100,000 Australians with new opportunities in China by 2025, so they can harness these opportunities. The China Pavilion is another channel for us to share our message around the impact that the seismic changes in China are going to have for us all.</p>

<p>Find more information on how to access the opportunities for ACYA members from this partnership <a href="http://www.acya.org.au/education/acya-x-camp/">here</a>.</p>

<h2>About CAMP</h2>
<p>Heralded as part of the solution to ‘future-proofing’ Australia, CAMP is equipping the next generation of Australian entrepreneurs with the Asia proficiency and networks required to navigate the country’s most important economic relationship – that with China.</p>
<p>The award-winning cross-border innovation program unites dynamic young leaders who are passionate about business, social innovation and strengthening the Australia-China relationship, and provides them with the tools to address real-world global challenges.</p>
<p>CAMPx2017 was proudly powered by Swisse Wellness, Alibaba, UTS, EY, China Eastern Airlines, City of Sydney, 8Hotels and the federal government’s Department of Foreign Affairs and Trade via the Australia-China Council.</p>
<!-- <p><strong>Applications are now open for the CAMPx2018 program and scholarships <a href="http://www.australiachina.org/what-is-camp-2018/">here</a>.</strong></p> -->

<h2>About ACYA</h2> 
<p>ACYA unites a community of over 6000 members through a network of chapters across Australia and China. This community is focussed on a number of events, projects and initiatives under the 3 ‘pillars’ of Careers, Education and People-to-People Exchange. A number of unique internship, scholarship and volunteering opportunities make ACYA a ‘one stop shop’ for youth interested in the Australia-China relationship.</p>
<p>Every year, ACYA hosts the Australia-China Emerging Leaders Summit (ACELS) in Australia and China. The Summit is designed to connect and up-skill emerging leaders, preparing them for continued engagement with the challenges and opportunities of one of Australia’s most important bilateral relationships. In 2017, ACELS was sponsored by ShineWing Australia, the State Government of Victoria and the Australia-China Council.</p>
<p>Applications will be opening soon for ACELS Beijing, which will be held in late February 2018</p>