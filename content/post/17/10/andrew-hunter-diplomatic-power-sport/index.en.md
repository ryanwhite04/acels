{
  "title": "Andrew Hunter on the Diplomatic Power of Sport",
  "author": "ACYA",
  "date": "2017-10-21T09:43:31+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<em>This year Port Adelaide and the Gold Coast Suns made history by playing the first Australian Football League match in Shanghai, China. ACYA's Cormac Power spoke with Andrew Hunter, Port Adelaide's China Manager, about the power of sports diplomacy.    </em>

<!--more-->

<b>ACYA: What has your career been like thus far?</b>

Andrew: My career thus far has been largely enjoyable. I think the thing that I have taken out of my career is that it is important to try, where possible, as far as possible, to do something that you enjoy. More importantly you need to do something about which you are passionate. I’ve had great fortune in the last four years to be involved in internationally focused roles that, whether they be commercial or political, require a deep engagement with people from different cultures and different countries. I’ve enjoyed that very much and think that it’s a great privilege when you can actually work in an area about which you are passionate and interested. In short, my career has been rewarding.

<b>ACYA: Are there any lessons you can share with us from Port Adelaide’s experience engaging with AFL in China?</b>

Andrew: We understood that AFL was unknown in China. The great lesson from that is, in retrospect, we developed a reasonable strategy. Our first step was to develop a greater understanding of who we were and what we did.  The importance of using language changed the way we described the sport which made conversations a lot easier. The way that we talked about sport as a cultural expression, was important as we understood that Australian Football would simply be one of many sports unless we found that point of difference. We found it in its centrality to Australian culture, and we knew that there was a strong interest in Australia. We tried to explain the sport as being a cultural carrier. If someone was interested in Australia, they should know about Australian Football as well. I think that to sell a product, people had to understand it and see its value, it was a certainly a strong learning that we had.

<b>ACYA: What do you see as the role of sports diplomacy broadly in international relations?</b>

Andrew: I think sport has been underused in international relations. There are some great examples of where sport has being used for breakthrough diplomacy. Certainly the United States and Cuba, on both sides, there was an understanding over a long period that baseball could potentially be used as ‘breakthrough diplomacy’. There were certain examples where they’d taken some modest steps towards a normalisation of that relationship through baseball. The Baltimore Orioles played against the Cuban national team in 1999 which was an initiative of Bill Clinton’s. Jimmy Carter, after he retired as President, went to Cuba and did the first pitch in an All Stars’ game. Later, when relationships were ultimately normalised, they used a baseball game that Obama attended in Havana to demonstrate that there was some commonality between the two countries.

I think sport can be used in a much broader way, it doesn’t always have to be between two countries that don’t have an existing relationship. It doesn’t have to be for a diplomatic breakthough, it can be used to have greater intercultural understanding between people on a people-to-people level. It’s very important to find those areas of mutual passion and sport is a popular human endeavour.

I think sport can be used to achieve commercial outcomes as well, not just through the industry itself. Look at the example of ‘Ping Pong diplomacy’, which everyone knows about, in 1971. A couple of years before that the Chinese table tennis team actually travelled to Japan, the Asian Table Tennis Championships were held in Nagoya in 1970. The Chinese team travelled with a number of Chinese businesses and they approached, talked to and had forums with the Japanese business community. At the time in Japan I think they were really wrestling with their existing pro-American relationship. The business community in Japan was very keen for the country to normalise relationships with China, obviously because it provided a big market, and China and Japan have had a very natural cultural enmeshment which has evolved over centuries. Ping Pong diplomacy was famously used for the breakthrough diplomacy between the United States and China but before that even, the Chinese had understood that through sport it was a comfortable and unthreatening way to get to know people on a number of levels –including people to people and business-to-business. Tanaka Kakuei went about normalising relationships between Japan and China even before the more famous Nixon Mao and Kissinger and Zhou Enlai events.

I think sports diplomacy is underused at the moment but there’s great potential for it to serve a diplomatic purpose.

<b>ACYA: What is it that makes it different from other forms of diplomacy?</b>

Andrew: Sport has been politicised; it has been used to try and achieve unsavoury outcomes. It has not always been that pure method of exchange but generally-speaking, sport can be something that is not threatening to another culture. It’s not like language, which has in the past been used as a form of cultural imperialism. I don’t think sport can play that role. Sport is largely unthreatening and, to use a cliché, it does put people on an even playing field. There is no real dominant culture in sport; some countries may perform better than others but, once you get on the field, everyone is equal. In that sense, unlike language and unlike other forms of cultural exchange it does create equality. That is where its potential lies.

<b>ACYA: What future untapped opportunities are there for Australia and China when it comes to sports diplomacy?</b>

Andrew: Looking at what we’re doing in sports diplomacy in terms of AFL, I think that whilst for a number of reasons it’s complicated and challenging, because there’s limited or no understanding of Australian Football in China, it is a quintessentially Australian Sport. So it has the advantage of being an Australian expression and something we can share with the Chinese who might be interested in Australia more broadly. There are other sports however, where there is a shared interest in the sport in Australia and China. Basketball, for example, is quite popular in both countries.

There are some good opportunities that can be explored further. Certainly on an individual level there can be attachment to certain athletes so I think individual athletes also have a great opportunity to be ambassadors as well and represent their cultures in a positive light. An interest in an individual athlete could easily lead to an interest in their culture. Ian Thorpe, for example, had an interest in Japanese culture and the Japanese enjoyed the fact that a famous Australian athlete had a certain affinity for their culture. I think those things are important and certainly they can be developed going both way between Australia and China.

<b>ACYA: How do you think government can better facilitate cultural exchange through sport? Are we doing enough?</b>

Andrew: I don’t think we are doing enough in terms of cultural diplomacy or sports diplomacy. A part of the issue is that we need an understanding from governments that that sport can play a real role and I think that might take some time to develop. In Australia there is a very strong economistic approach to foreign policy. They see this sense of economic diplomacy and how we can use foreign policy to further our economic interests. The Sports Diplomacy Strategy 2015-2018 in some ways reflects that; we are looking at how we can use sport to achieve commercial outcomes. Diplomacy for me is broader than that and perhaps we can let the commerce take care of itself once we’ve developed a stronger intercultural understanding and trust between the peoples as a foundation for other endeavours and exchanges, including trade and investment.

I think the first step is a greater understanding of sport’s potential. Government funding and support always helps, and we need more funding for cultural and sports diplomacy. Australia, compared to other countries, generally underfunds these areas. European countries, Japan and China have vast sums of money that they devote to public diplomacy, including cultural diplomacy, which is generally considers as a sub-set. But probably on both sides, the greatest impediment is that this fascination with public diplomacy, which is very unidirectional at the moment, is all about the projection of culture and greater affection on the other side. It doesn’t really speak of a two-way and a genuine exchange. There needs to be a fundamental shift in thinking that foreign policy is more than economic policy. Whilst you always want other countries to think well and understand you, perhaps the best way of going about it is to first try to develop a greater understanding and affection of the other countries as well.

The idea of reciprocity should be central to foreign policy, areas such as public diplomacy, sports diplomacy, and cultural diplomacy. These foreign policy areas should have a stronger focus on beneficial outcomes. From that point we can build a stronger foundation for more accessible relationships between the people of both countries.