{
  "title": "FASIC NCP ACYA Participate Competition",
  "author": "ACYA",
  "date": "2017-10-10T06:13:18+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "FASIC and NCP",
      "type": "image",
      "src": "FASIC-and-NCP.png"
    }
  ]
}
Jointly supported by the Australian Embassy in Beijing and the Foundation for Australian Studies in China (FASIC), ten lucky New Colombo Plan students and Australia China Youth Association (ACYD) members will be sponsored to attend the <a href="http://china.embassy.gov.au/bjng/Studycenter.html">Fifth FASIC Conference 2017</a>, from 1-3 November 2017 at Jiangsu Normal University in Xuzhou.
Participants provided a 300-word comment on the theme of one of the below topics in relation to the Australia-China youth space (in English). 

What impact does transnational connectivity have on Australia-China relations?
What is the relevance of transnational connectivity in the current world?
Essays should be sent to <a href="mailto:gm_china@acya.org.au">gm_china@acya.org.au</a> by October 17th. The result will be released on October 20th on Australia Global Alumni: China’s LinkedIn page and the winners will be notified via email.