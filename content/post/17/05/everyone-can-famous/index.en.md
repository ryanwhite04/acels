{
  "title": "How you, me, and everyone can be famous",
  "author": "Garry Ho",
  "date": "2017-05-11T07:09:49+00:00",
  "image": "",
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
While Western technology companies gain momentum in a shift to broadcasting real-time social media, most noticeably seen in Facebook and Instagram's livestreaming functionalities, you may be surprised to know how popular it is in China already. This internet sector, having barely existed three years ago, produced revenues of more than 30 billion yuan ($4.3 billion US) in 2016 and is predicted by investment bank China Renaissance Securities to more than triple by 2020. Just to add perspective, China's 2016 box office ticket sales were $5.8 billion US.

<!--more-->

So you might ask, how is watching your smartphone even comparable to the cinemas? In many ways, livestreaming has come to occupy a niche most appropriately, albeit awkwardly, described as: on-demand-user-catered-frivolous entertainment. If that wasn't baffling enough, try watching the content. From eating noodles to appease a few hundred bored viewers, dedicated broadcasters perform requests ranging from: "eat a maggot" to "wear a pink wig, red lipstick, sparkly mermaid tail and a bra while dancing". It may seem unmerciful, but these broadcasters, many who see livestreaming as an intermediate path to achieving a traditional celebrity status, earn lucrative wages up to 80 000 Yuan ($11 000 US) per month - an extraordinary figure given the seemingly simple toolkit of just a smartphone and an Internet connection. But to capture, maintain and grow a loyal fan-base (from the 344 million netizens who used livestreaming applications in 2016), a skillset encompassing dancing, singing, personable conversation and humour is oftentimes barely enough.

And extraordinary lengths are taken to expand that 'toolkit', as Jing Qi, a part time presenter on streaming platform *Huajiao* can attest to. After an ordeal of rhinoplasty and facial fat injections that left her feeling "worse than dead", Qi believed her increased attractiveness would incentivise more viewership, the majority of which are single men. Consequently, it isn't surprising that they are often colloquially labelled *diaosi* ("losers").

The business premise lies in viewers purchasing virtual gifts to send as tokens of appreciation to their favoured broadcasters, who in turn are incentivised to create content catered to their viewers' interest. These gifts may be virtual roses, sports cars or villas ranging from $0.5 to 140 US, and are redeemable in part, for cash. Essentially monetising real time interaction, the livestreaming industry has grown such that agencies recruit talented broadcasters to perform on dozens of live-streaming platforms. In its Beijing office, dozens of <i>Three Minute TV's</i> female anchors work in small computer-facing booths - singing, flirting and chatting with fans to earn more virtual gifts. The cash redeemed from these gifts is then split between the platform, the agency and the anchor. Deng Jian, chairman of one such agency named Three Minute TV, described his business as a 'militarised' production machine to feed the livestreaming industry.

More than what many would consider another vehicle for corporate greed however, the livestreaming platform plays an important role in generating income in lower-tier cities. Often equipped with fast broadband connection but limited employment opportunity, popular applications such as <i>Inke, Momo</i> and <i>Uplive</i> allow individuals an alternative to the saturated university graduate market and an opportunity to develop fun social relationships whilst earning compensation. In a country as geographically diverse as China, men and (mostly) women in more isolated cities are increasingly embracing the opportunity to become 'internet celebrities' in their own right.

*Having recently realised that primary school weekend classes were more than just another parental nag, Garry plans to (re)learn Chinese in the next two years so that he can properly order any dish from a restaurant menu. He is currently majoring in Economics and studies a Bachelor of Liberal Arts and Science.*