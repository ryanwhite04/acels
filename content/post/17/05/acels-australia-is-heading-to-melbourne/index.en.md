{
  "title": "ACELS Australia is heading to Melbourne",
  "author": "ACYA",
  "date": "2017-05-01T11:11:53+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
The Australia-China Youth Australia (ACYA) has announced that the Australian Chapter of the Australia China Emerging Leaders Summit (ACELS) will be hosted this year in Melbourne from 27-30 July.

<!--more-->

Over 70 budding young leaders from all corners of the country are expected to partake in this 4-day intensive program. As ACYA’s signature initiative, the program aims to provide opportunities for youths active in the Australia-China relationship to strengthen the networks of and enhance their professional development. Centered around the theme “Exploring the Cultural Dimensions of the Bilateral Relationship”, events will explore economic and cultural issues in the Australia-China space, both domestically and internationally. Key aspects of the program will include networking nights, panel discussions, interactive forums and scavenger hunts.

Earlier this year the ACELS was held in Beijing 17-19 February in collaboration with Model APEC (Asia-Pacific Economic Cooperation). The Summit brought together over 50 delegates to discuss a range of topics including technology innovation, the Australian Government’s upcoming White Paper, and the challenges of rural education. Supported by the Australian Embassy, the Summit brought delegates through Beijing’s hutong to its most prestigious universities and engaged guests and speakers from the highest levels of government, business, and media.

[embed]https://www.youtube.com/watch?v=w1fOo5dIEeQ[/embed]

Established in 2008, ACYA aims to foster transnational ties between youths in Australia and China across business, cultural, government and academic fields. Through its 22 local chapters and the National Executive Committee, ACYA offers internship and career opportunities, releases publications and hosts networking nights and conferences, both self-administered and as partnerships.

More information regarding applications and program details are <a href="http://www.acya.org.au/acels/" target="_blank" rel="noopener noreferrer">here</a>

Contact:

Roger lee
Australia General Manager
gm_aus@acya.org.au