{
  "title": "Sam Mugford",
  "author": "Hayden Wilkinson",
  "date": "2017-05-26T08:14:53+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Alumni Spotlight",
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "[:en]Sam Mugford in Shanghai[:zh]慕俊杰在上海[:tw]慕俊杰在上海[:]",
      "type": "image",
      "src": "unknown.png"
    }
  ]
}
As part of a new initiative, a spotlight will be put on all the cool things ACYA alumni have been doing to advance the Australia-China relationship. To kick things off, Hayden Wilkinson interviewed Sam Mugford.

<!--more-->

## What was your graduation year?

I graduated in 2015.

## Of which ACYA chapters were you a member?

Shanghai and Adelaide.

## What or who sparked your original interest in China?

I studied Chinese in primary school and high school. I continued to study Chinese at university and its become a life-long interest.

## What has been your experience of living and studying in China?

I first went to China back in 2010 in my second year of university. I went with a family friend who has businesses in Ningbo and Beijing. We traveled together for two weeks and I still remember the amazing feeling of traveling in a country which is completely different to home. The different food, smells and culture all fascinated me. I went back again and again, and ended up living in Shanghai, Beijing and Jinan between 2012-2014.

In that time, I made lifelong friends, Chinese, Australian and foreigners, and learnt a lot about myself as a person whilst learning Chinese and about China. When people asked me about my experience when I got home, I told them that living in China is like watching a daily fireworks show: every experience is fascinating.

## Could you tell us about your career path leading up to today?

I've worked as a consultant, as a public servant and now as a policy analyst.

## What does your current work at C|T involve?

I assist with policy analysis for private and political campaigns.

## What influenced you to pursue a career in policy?

I studied law and international relations as well as Chinese at university. Whilst in China, I studied Chinese law and political economy, which is where my interest in public policy grew from.

## What's one thing you love about your job?

I get to research different and interesting areas of policy.

## Did you imagine this would be your career? What would first-year you think of what you are doing now?

In my first year of university I would never have imagined I'd be working where I am now. I think first-year Sam would think what I'm doing is pretty cool, I always wanted to do something that was "different". I guess studying Chinese was just an extension of that!

## Do you have any advice for students and recent graduates looking to work in your field?

Be honest with yourself and the people you are pitching to about what you're interested in and what you can offer them. If you are interested in politics and public policy then it could be the right field for you.

## What was your time in ACYA like? What were the most valuable things you got out of being part of ACYA?

I loved ACYA. It was exciting and interesting, because I was able to meet and interact with people, Chinese and Australian, who had similar interests to me.

By far the most valuable thing about being part of ACYA was the friends I have made and the people I met. ACYA members all have an incredible sense of community and friendship, even after they have moved on from the organisation.

## Did you get a chance to take on any leadership roles within ACYA? If so, what benefits did you get from it?

I was President in Shanghai and Adelaide. It taught me about working between cultures, bringing people together and being a leader, all useful skills to learn that university doesn't necessarily give you.

## What's your best memory from your involvement in ACYA?

Our first ACYA event in Shanghai was amazing. I have attached two photos below of me and my friend Renbin speaking on the night that sums it up pretty well.

We were all friends with a bar manager called Jackie who managed a high-class bar called Le Sun Chine on Huashan Road near Xuhui. He said we could have our event there at a very cheap price. He even invented a new cocktail for the night, named after our Chinese speaking former-Prime Minister, called the "陆克文". We had about 40 people come, and we played Chinese/Australian themed quizzes and games.

It was a lot of fun, but the reason it is my best memory was because I had some of my best friends there with me. ACYA helped bring these people into my life and it is something I will always be thankful for.