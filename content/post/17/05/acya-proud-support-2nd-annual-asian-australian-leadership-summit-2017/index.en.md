{
  "title": "ACYA is proud to support The 2nd Annual Asian Australian Leadership Summit 2017",
  "author": "ACYA",
  "date": "2017-05-30T01:51:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [
    "Communications"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA - Asian Australian Leadership Summit",
      "type": "image",
      "src": "ACYA-Asian-Australian-Leadership-Summit.png"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2017/05/ACYA-Asian-Australian-Leadership-Summit.png">ACYA</a> is proud to support The 2nd Annual Asian Australian Leadership Summit 2017.

Across two days on the 18<sup>th</sup> and 19<sup>th</sup> of July  at Rydges World Square, Sydney, the summit will tackle the theme: “Overcoming Adversity, Seizing Opportunity".

<!--more-->

As we launch into the Asian Century, there is still a clear underrepresentation of Asian Australian professionals at executive leadership levels.
<a href="http://bit.ly/2oO6cQX" target="_blank" rel="noopener noreferrer" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://bit.ly/2oO6cQX&amp;source=gmail&amp;ust=1496194441085000&amp;usg=AFQjCNHXU8PqdBUNaQdwRGTtvY-JEaIIbA">The 2nd Annual Asian Australian Leadership Summit</a> is Australia’s only dedicated professional development event specifically designed to develop Asian Australian leaders.
Through anecdotal case studies from inspiring Asian Australian leaders and expert advice from diversity and cross-cultural specialists, this event will equip Asian Australian leaders with the tools they need to overcome the barriers on the path to leadership.

Presented by Liquid Learning Group, the Summit is the only high-profile event in Australia that is specifically dedicated to the professional development of Asian-Australians.
This marks an important step towards addressing significant underrepresentation in the country’s senior executive levels, especially in the “Asian” Century.
As such, ACYA is delighted to partake in the hosting of an event that stimulates valuable discussion of such issues, as well as foster cross-cultural literacy and career confidence amongst existing and up-and-coming Asian-Australian leaders.

<strong>Explore</strong>
<ul>
 	<li>Hear inspirational success stories from Asian Australian leaders</li>
 	<li>Develop cross-cultural competence and confidence</li>
 	<li>Broker powerful relationships to promote career progression</li>
 	<li>Become an authentic and innovative leader</li>
</ul>
<strong>Featured speakers include</strong>
<ul>
 	<li>Tien-Ti Mak Innovation Partner and Chief Technology Officer <strong>Australia Post</strong></li>
 	<li>Yung Ngo State General Manager (NSW,ACT,VIC,TAS) and Chair of the Cultural Diversity Leadership Employee Action Group <strong>Westpac</strong></li>
 	<li>Hui Cheng Tan Head of Financial Planning and Analysis <strong>Microsoft</strong></li>
 	<li>Ken Woo Partner <strong>PwC</strong></li>
 	<li>Grace Bacon Head of International Segment, ANZ Private <strong>ANZ</strong></li>
 	<li>Leon Doyle Partner and Head of Experience Design <strong>Deloitte</strong></li>
</ul>
<b><a href="http://bit.ly/2oO6cQX" target="_blank" rel="noopener noreferrer" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://bit.ly/2oO6cQX&amp;source=gmail&amp;ust=1496194441085000&amp;usg=AFQjCNHXU8PqdBUNaQdwRGTtvY-JEaIIbA">Download the full programme here</a></b>

ACYA members receive an exclusive<strong> 10% discount</strong> off the standard rates with the booking code Q4.

Contact Liquid Learning : email us at <a href="mailto:registration@liquidlearning.com.au" target="_blank" rel="noopener noreferrer">registration@liquidlearning.com.au</a> or Call us on: <a href="tel:+61%202%208239%209711" target="_blank" rel="noopener noreferrer">+61 2 8239 9711</a>