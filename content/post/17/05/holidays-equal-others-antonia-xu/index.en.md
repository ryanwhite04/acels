{
  "title": "When some holidays are more equal than others | Antonia Xu",
  "author": "Sophia Sunzou",
  "date": "2017-05-26T07:51:53+00:00",
  "image": "",
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
I never really know what to do with myself during Christian holidays.  I feel a little like Johnny English as I weave through the plastic pine trees and gold tinsel that seems to permeate every Sydney street during Christmas, or like a fraud when I buy Easter eggs not because I'm hiding them for children to find, but because I only had 3 dollars and would get more chocolate for my dollar than if I had bought it in a rectangle.

<!--more-->

For me, growing up in a Chinese household meant that the bubbly festive season feelings tended to be overshadowed by mostly inconvenience and sometimes a shadow of annoyance.  I can't get a coffee from my usual coffee shop in the morning, and if I want to grab lunch during the day, I'd need to look up in advance for opening hours that no one seems to bother with updating.  On these seemingly arbitrary days, and despite our trials and tribulations in searching for operating services, my mum and I try to get the family together to replicate some semblance of a holiday.  But without the tradition or any sort of biblical familiarity, I can't help but shake the feeling that when I sit down in front of the mountains of roast duck, seafood, and steaming bowls of rice, it's all for show.

It's not a sad feeling by a long shot, but just distinctly incongruous with the general feelings one expects a holiday to entail.  As I sit in the library trying to complete a university assignment on Easter Saturday, I'm mostly irritated that nothing is open, and that everyone has disappeared to celebrate and rest for a reason that I don't really understand.

It's also a little hilarious to see how hypocritical my cynical apathy is when Chinese holidays roll around.  "春节" or Chinese New Year is such a big deal that often it's referred to as simply "过节", which translates literally to just "festival celebration".  Everyone knows what you're talking about and where my parents are from in 东北, everyone in the family embarks on the day long monolithic task of making thousands of dumplings to feed everyone.  At the unspoken deadline of 12 am, everyone demolishes everything over the course of approximately 5 whole minutes. There are also arbitrary traditions, like putting money in random dumplings which was probably the most unhygienic, dangerous thing to do, but it meant that someone that night would be irrationally happy they almost swallowed a shard of copper because it meant they'd have good luck and fortune for the rest of the year.  I only spent a handful of New Years with my family in China nearly a decade and a half ago, and the memories of exact events have faded and worn around the edges.  But even now, every 春节, I still carry a very nostalgic, melancholic memory of warmth and love and the colour red.

However, living in Sydney for most of my life, I get an equally incongruous feeling when during 春节, life outside of home is business as usual.  Nothing is closed, and if I didn't talk to my mum, it's just another day out of 365.  For a lot of friends, the only exposure they'd had to 春节 was the annual overpriced and commercialised "Night Noodle Market" erected in Hyde Park.

It's funny that despite having two sets of celebrations, I feel like I can't scratch the celebration itch.  Something is always missing, whether it be the formal day off work or the unspoken traditions that bring people together.  My mother often laments that this is the classical experience for any Chinese immigrant, and as an ABC, until recently I had not even realised that one could have both on the same day.

Traditions are slow to change, and I do not doubt that they are exceptionally important.  But Australia is much more dynamic and heterogeneous than it once was.  With these changes we also need public holidays and celebrations that are reflective of the people celebrating them.  Amidst the controversies surrounding whether the origins of Australia Day are a legitimate cause to celebrate, and a growing secular population that doesn't necessarily believe in Easter bunnies anymore, perhaps we should be turning our minds to celebrating aspects of Australia that Australians now are most proud of, so that we can feel a part of and regain a sense of ownership towards the holidays that we celebrate.

<em>Antonia Xu grew up in Sydney in a household that seems to have been lifted straight out of Jilin. After 20 years of confusion and frustration, she is finally realising now how special it is have a secret Chinese life to return to everyday. Here her favourite past time is trying to figure out how to explain privilege in mandarin so she can explain to her mother why she doesn't own 3 properties yet. Outside of her abode, Antonia studies a Bachelor of Science and Law.</em>