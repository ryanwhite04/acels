{
  "title": "Global Mobility in Shanghai",
  "author": "ACYA",
  "date": "2017-02-16T01:15:01+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Uni5",
      "type": "image",
      "src": "Uni5.jpg"
    }
  ]
}
David Shaw is an ACYA alumnus who now works in Westpac’s Shanghai office as a credit analyst and has half a decade of experience in China. Joe Zhan, an Adelaide University alumnus, lectures at Shanghai University of Finance and works with exchange students heading to Australia.

<!--more-->

Australian expatriates in Shanghai represent the PRC’s further opening its doors to the world and people making the effort to knock. Global mobility is the phrase used to describe this trend, what was once lingo of HR peons is now the buzzword of the world economy. Specifically, it’s the movement of individuals across borders, cultures, languages and ways of thinking, meaning the labour talent pool is more flexible than ever. Although Shanghai has been regarded as a cosmopolitan city, its Chinese roots are still present every day and it’s social, linguistic and commercial idiosyncrasies confuse many expatriates today. ACYA Shanghai sat down with two Australian ones at Xnode in Jing’an, where Austrade now incubates promising startups to grow in the Chinese market. The clear message of the night sent to the audience was that although being globally mobile incurred a steep cultural learning curve, the process is mutual.

A descriptor for the hectic and noisy nature of Chinese restaurants can also be used to style much of Shanghai. For much of the city’s sophistication and rich history there is still a sense of controlled chaos when walking around the streets. It’s certainly a stark contrast from the seemingly docile streets of Sydney but everything works, as Deng Xiaoping once said ‘it doesn’t matter what colour the cat is, so long as it catches mice.’ Back in Jing’an, David when asked about his first impressions when he studied mandarin her responded:

“You’re required to have not just a solid work ethic relating to your studies but also to push yourself to immerse yourself. You’ll never be counted as a local, but at least you’re somewhat fluent in their lifestyle.”

It hints at a much broader issue in global mobility, particularly in China, where does one set their identity? As a foreigner, or as assimilated? The answer is forcibly in the middle, because there’s a large reserve army of labour behind you, expats need to offer expertise not just in local customs but that of the west. Besides, when little kids at train stations point at you and holler “妈妈,外国人!” (‘mother, foreigner!’), you know that western part of you can never be separated.

In a way, global mobility induces an attitude of <em>chabuduo</em> or ‘close enough’ in mandarin slang. Australians need to be Chinese enough to understand their surrounds whilst retaining enough ‘westernness’ to distinguish him/herself. Keenly though this process doesn’t only happen for the individuals entering a new country, the employers also take on the task. Joe at ACYA articulated his experience by saying:

“For many westerners learning mandarin isn’t necessary when working in Shanghai, although those who do succeed generally are. Chinese offices sometimes use western recruits as a way to not only translate the mannerisms of foreign businesses but also as a symbol of how <em>globalized</em> they want to appear.”

Evidently then Shanghai exploit the fruits of global mobility to dually expand their worldly business experience, but to some extent a PR image to the same meaning. Business culture has shifted dramatically in China over the last two decades, global mobility is amongst a dozen major factors continuing it’s ‘opening up’. For instance 关系 (guanxi) or ‘a culture of relationships and favours’ once dominated the operations of Chinese businesses, banquet dinners and bottles of liquor for clients were the currency of choice. However, with greater global scrutiny Xi Jingping enacted the biggest anti-corruption purge in recent memory. Which in David’s experience, left some organizations terrified,

‘Typically during the Mid-Autumn festival fellow businesses and customers would send each other mooncake vouchers as a sign of good will. But in 2016 all the vouchers we sent out were returned to us, something that happened to lots of businesses because they were afraid of being accused of anything.’

Arguably in Australia there’s a business culture similar to this amongst many firms, albeit on a lower scale, but between the two examples the power of cultural diffusion into China is evident. In conclusion we’re seeing how global mobility now translates to global adaptability.

<em>This piece was written by Kyle Salkeld for the Australia-China Perspectives blog. Kyle is a University of Sydney student currently studying at Fudan University, Shanghai.</em>