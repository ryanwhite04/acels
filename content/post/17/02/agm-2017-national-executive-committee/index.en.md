{
  "title": "AGM: ACYA Announces 2017 National Executive Committee",
  "author": "ACYA",
  "date": "2017-02-26T21:38:36+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "AGM",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Post-AGM media release photo",
      "type": "image",
      "src": "Post-AGM-media-release-photo.jpg"
    },
    {
      "title": "Post-AGM media release photo",
      "type": "image",
      "src": "Post-AGM-media-release-photo-1-e1488144950428.jpg"
    }
  ]
}
The Australia-China Youth Association (ACYA) is pleased to announce the conclusion of the 2017 Annual General Meeting (AGM). The AGM was held on February 26, 2017. National Committee Executives, Chapter Committee Executives and interested members gathered in more than eight cities to hear annual reports from the 2016 National Executive Committee and to vote for the 2017 National Executive Committee.

<!--more-->

With a record attendance at the AGM and record field of candidates for the eight elected positions, the 2017 National Executive Committee is a truly excellent selection of young leaders in the Australia-China space, showcasing experience in leadership, project management, and understanding of the Australia-China Youth Association’s operations in Australia and China.

The 2017 ACYA National President is Georgia Sands. Georgia has been a member of ACYA for the last four years, during which she has been Chapter President in both Griffith University and Hong Kong Chapters, and a member of the National Executive Committee in 2015 as General Manager – Portfolios. Georgia is an alumna of the Prime Minister’s Endeavour Award Scholarship and has recently begun work as a Law Graduate at Allens.

Georgia will be joined by Josephine Macmillan as 2017 Managing Director. Josephine joined ACYA in 2016 as the National People-to-People Director, managing the Association’s relationship with numerous sporting clubs and managing the 2017 Golden Koala Chinese Film Festival. Josephine is a student of Arts and Laws at the University of Queensland and is currently on exchange at Peking University.

The 2017 ACYA National Executive Committee will include:
<ul>
 	<li><strong>National President </strong>– Georgia Sands</li>
 	<li><strong>Managing Director </strong>– Josephine MacMillan</li>
 	<li><strong>National Secretary </strong>– Ryan Cunningham</li>
 	<li><strong>National Treasurer –</strong> Sean Teh</li>
 	<li><strong>General Manager – Portfolios</strong>– Jacinta Keast</li>
 	<li><strong>General Manager – Australia </strong>– Roger Lee</li>
 	<li><strong>General Manager – China </strong>– Chloe Dempsey</li>
 	<li><strong>Alumni Engagement Manager </strong>– Hayden Wilkinson</li>
</ul>
David Douglas, the outgoing National President acknowledged the contributions of the 2016 National Executive Committee and thanked them for their dedication and commitment to ACYA over the past year. Without the commitment of these hard-working volunteers, ACYA would not have been able to experience the success seen throughout 2016.

David expressed his praise for the achievements of the Australia-China Youth Association in 2016. "ACYA has again experienced tremendous growth over the past year, not only in terms of size, but also in terms of the quality of events and opportunities offered to members. I thank the 2016 Committee for their contributions, congratulate the 2017 Committee on their election and wish them all the best for the coming year."

To join the 2017 National Executive team, visit our <a href="http://www.acya.org.au/join-acya/">recruitment page</a> to see which roles are currently hiring.