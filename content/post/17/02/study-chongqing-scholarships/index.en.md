{
  "title": "Study in Chongqing Scholarships for Victorian Students",
  "author": "ACYA",
  "date": "2017-02-11T01:46:51+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Application Form",
      "type": "application",
      "src": "Chongqing-Scholarship-Application-Form.pdf"
    },
    {
      "title": "Chongqing Industry Polytechnic",
      "type": "image",
      "src": "ChongQinng-Polytechnic.jpg"
    },
    {
      "title": "Chongqing Industry Polytechnic",
      "type": "image",
      "src": "ChongQinng-Polytechnic-1.jpg"
    }
  ]
}
<h2>Background</h2>
Located in south west China, Chongqing is rapidly becoming one of the most economically important cities in China. Chongqing and Australia have been long term partners. This relationship was further strengthened in 2002 through the largest educational aid project: the Australian China Chongqing Vocational Education and Training Project. Through this close relationship the <a href="http://www.cqedu.net/">Chongqing Education Commission</a> has given authorisation for <a href="http://www.cqipc.net/">Chongqing Industry Polytechnic</a> to offer 10 full Study in Chongqing Scholarships for Victorian students.

<!--more-->
<h2>Scholarships</h2>
The scholarships on offer for 2017 are 10 study abroad scholarships (3-6 month program, total subsidies of RMB 10,000)

This covers the tuition fee, accomodation and basic insurance in China for the period of 3-6 months studies.
<h3>Eligibility and conditions</h3>
To be eligible, applicants must be residents of Victoria. Student should apply for the Chinese student visa by themselves. Chongqing Industry Polytechnic College will provide required paperwork.
<h3>Application process</h3>
Applicants are required to submit the <a href="http://www.acya.org.au/wp-content/uploads/2017/02/Chongqing-Scholarship-Application-Form.pdf">scholarship application form</a> and required documents to Chongqing Industry Polytechnic (Please refer to the below contact details). Successful candidate will be enrolled in Chongqing Industry Polytechnic.
<h3>Documents Required:</h3>
<ol>
 	<li><a href="http://www.acya.org.au/wp-content/uploads/2017/02/Chongqing-Scholarship-Application-Form.pdf">Application Form</a></li>
 	<li>Personal statement specifying why you wish to study in Chongqing (within 300 words)</li>
 	<li>Curriculum Vitae</li>
 	<li>The photocopies of the highest diploma and transcripts.</li>
 	<li>Photocopy of the passport</li>
</ol>
<h3>Application Submission</h3>
The deadline for applications is 15 April 2017. Applications will be assessed by the Chongqing Industry Polytechnic. Applicants will be notified of the outcome by end of May 2017.

Please submit your application (in a unified PDF document) to

Chongqing Industry Polytechnic College
Ms. Liu Xin
Program Officer
Email: <a href="mailto:ikylq1023@foxmail.com">ikylq1023@foxmail.com</a>
Mobile: <a href="tel:+8613527416583">(+86) 135 2741 6583</a>
Address: No. 1000, Taoyuan Avenue, Yubei District, Chongqing

If you have any enquiries, you may also contact:

Victoria Government Business Office in Chengdu
Mr. Terry Wang
Education Service Manager, Victorian Government Business Office, Chengdu
Email: <a href="mailto:Terry.Wang@ecodev.vic.gov.au">Terry.Wang@ecodev.vic.gov.au</a>
Phone: <a href="tel:+862865118178">(+86) 28 6511 8178</a>