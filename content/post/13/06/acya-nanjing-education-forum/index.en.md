{
  "title": "<!--:en-->ACYA Nanjing Education Forum<!--:--><!--:zh-->ACYA Nanjing Education Forum<!--:-->",
  "author": "ACYA",
  "date": "2013-06-13T11:14:04+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->Around 20 people came out to the ACYA Nanjing Education Forum at Nanjing University on 7 June 2013. The forum provided information on tertiary education to Chinese students considering studying in Australia. Speeches were followed by snacks and an informal chat. Thanks to Leah Bramhill and Chelsea Zhou for their event organisation, and Laurie Harris and Caterina Chen for speaking on the night.<!--:--><!--:zh-->Around 20 people came out to the ACYA Nanjing Education Forum at Nanjing University on 7 June 2013. The forum provided information on tertiary education to Chinese students considering studying in Australia. Speeches were followed by snacks and an informal chat. Thanks to Leah Bramhill and Chelsea Zhou for their event organisation, and Laurie Harris and Caterina Chen for speaking on the night.<!--:-->