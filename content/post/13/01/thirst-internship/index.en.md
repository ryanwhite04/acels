{
  "title": "Thirst Internship",
  "author": "ACYA",
  "date": "2013-01-10T04:47:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [
    "Full Time"
  ],
  "locations": [
    "China",
    "Shanghai"
  ],
  "organisations": [
    "Thirst"
  ],
  "resources": []
}
Thirst is a water conservation and education NGO, which seeks to educate and raise awareness about the water crisis in schools and universities across China.
An initiative of the World Economic Forum, Thirst educate students about "Virtual Water", the hidden water consumed in the manufacturing of a product or good, and seeks to inform the next generation of global consumers to make "water-wise" consumer choices and to more generally reduce their water consumption.
Having reached over 100,000 students with our WE Water Experience presentation, and a further 20,000 students through large-scale activities and events, our dedicated team want to expand their operations.
To do this, we need your support!
<!--more-->
<h2>Position Description</h2>
A Thirst intern is highly motivated and cares about the environment.
A Thirst Intern is aware of global issues, politics and trade-relations.
Thirst interns inspire and educate Chinese Youth to reduce their water consumption and change their consumer choices.

Generally interns will assist with one or more of the following three departments
<ul>
 	<li>Education</li>
 	<li>Online and Social Media</li>
 	<li>Thirst Clubs</li>
</ul>
<h2>Duties</h2>
<ul>
 	<li>Deliver We Water Experience presentation</li>
 	<li>Deliver Thirst Clubs activates</li>
 	<li>Contact schools and organize presentations and activities</li>
 	<li>Create and promote Club activities i.e. events</li>
 	<li>Engage with Thirst's online audience vie different social media</li>
 	<li>Conduct research</li>
 	<li>Assist with weekly, monthly, quarterly and annually reports</li>
</ul>
<h2>Selection Criteria</h2>
<ul>
 	<li style="list-style-type: none">
<ul>
 	<li>Fluency in written and oral English (regardless of native language)</li>
 	<li>Chinese language competence highly regarded but not essential</li>
 	<li>Proficient skills and experience in Social Media</li>
 	<li>Proficient skills in MS Office</li>
 	<li>Motivated self-starter able to balance multiple projects</li>
 	<li>Good understanding of education market and consumers in China</li>
 	<li>Outgoing with excellent interpersonal and communication skills</li>
</ul>
</li>
</ul>
<h2>Provided</h2>
<ul>
 	<li style="list-style-type: none">
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li>Mentoring of the intern and practical exposure to the sustainable water issue and international Nongovernmental Organisation experience in China.</li>
 	<li>If interning for less than 3 months the intern will receive no stipend, if over 3 months then Thirst can accommodate a stipend of 70RMB per day</li>
 	<li>Thirst will also provide an invitation letter for the intern to receive an M class visa (if needed)</li>
</ul>
</li>
</ul>
</li>
</ul>
<h2>Additional Information</h2>
<ul>
 	<li style="list-style-type: none">
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li><a href="http://www.acya.org.au/wp-content/uploads/2015/09/Our-Thirsty-Story-LoRes-1.pdf">Our Thirsty Story</a></li>
 	<li><a href="http://www.acya.org.au/wp-content/uploads/2015/09/The-Water-Crisis-Fast-Facts-1.pdf">The Water Crisis</a></li>
 	<li><a href="http://www.acya.org.au/wp-content/uploads/2015/09/7-Deserts-7-Continents-1-Reason-LoRes-1.pdf">7 Deserts 7 Continents 1 Reason</a></li>
</ul>
</li>
</ul>
</li>
</ul>
Internships typically last 3-6 months.
The working days are from 9am to 6pm at the Thirst office in Beijing or Shanghai.

If you are interested in this internship please write a one page cover letter explaining why you are a suitable candidate and include with your resume to <a href="mailto:careers@acya.org.au">careers@acya.org.au</a>.