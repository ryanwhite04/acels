{
  "title": "Greenpeace Sustainable Finance Campaigner",
  "author": "ACYA",
  "date": "2013-01-24T10:44:05+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="page" title="Page 1">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: 11pt; font-family: 'Helvetica,Bold';">East Asia </span></p>
</div>
</div>
<div class="layoutArea">
<div class="column">
<p><span style="font-size: 12pt; font-family: 'Arial,Bold';">JOB DESCRIPTION </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">POSITION TITLE (ENGLISH): </span><span style="font-size: 11pt; font-family: Arial;">Sustainable Finance Campaigner </span><span style="font-size: 11pt; font-family: 'Arial,Bold';">POSITION TITLE (CHINESE): </span><span style="font-size: 11pt;">绿色金融项目主任 </span><span style="font-size: 11pt; font-family: 'Arial,Bold';">DEPARTMENT</span><span style="font-size: 11pt; font-family: Arial;">: Program Department<br /> </span><span style="font-size: 11pt; font-family: 'Arial,Bold';">REPORTS TO: </span><span style="font-size: 11pt; font-family: Arial;">Head of Sustainable Finance Program </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">GRADE: </span><span style="font-size: 11pt; font-family: Arial;">4 </span><span style="font-size: 11pt; font-family: 'Arial,Bold';">LOCATION: </span><span style="font-size: 11pt; font-family: Arial;">Beijing </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">PURPOSE OF POSITION: </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">The Sustainable Finance Campaigner is responsible for the development and implementation of campaign strategies, proposals and policy for engaging the investment and corporate finance communities in winning Greenpeace campaigns. The position is located in Beijing or Hong Kong. While the primary area focus is the financial market and business sector in China (including Hong Kong), there will be strong links to Greenpeace international campaigns and financial markets outside of China. </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">MAJOR RESPONSIBILITIES: </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">1. Develop and implement strategies to change corporate behaviors, in conjunction with Issue Campaign Teams, to support our ongoing campaigns in various business sectors. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">2. Actively engage corporations, their investors, shareholders and stakeholders to draw attention to environmental and social responsibilities of these corporations. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">3. Actively represent Greenpeace in the finance sector &amp; investment communities, in public and business forums, and the business-related media. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">4. Foster public disclosure of environmental impacts by corporations and investors. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">5. Empower investors and shareholders to bring in environmental and sustainability standards to improve the performance, practices and policies of corporations. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">6. Provides expertise with respect to corporate finance. Keeps abreast of issues related to corporate social responsibilities and business behavior in relation to the environment. Conducts research and investigation. Analyzes new developments in support of issue campaign objectives and goals.</span></p>
</div>
</div>
</div>
<div class="page" title="Page 2">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: 11pt; font-family: Arial;">7. Develops and implements effective plans and strategies to draw attention from finance stakeholders to ecological abuse. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">8. Researches and analyzes a wide range of specialized material on campaign issues and strategies. Maybe required to do media work. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">9. Promotes support within Greenpeace to advance campaign goals. Organizes activities and other public events. Makes presentations and responds to public speaking requests. Initiates communications with strategically targeted groups. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">10. Responds to public inquiries about campaign issues and other matters. Prepares materials and publications. Attends and/or organizes conferences. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">11. Performs other relevant duties as assigned by the Program Director. </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">REQUIREMENTS: </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Italic';">SKILLS REQUIREMENTS </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Organizational and Time Management Skills: to address fluctuating priorities; to accommodate office and on-site campaign work. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Leadership and Judgment Skills: to exhibit confidence and provide direction in planning, organizing and coordinating campaign activities in a given specialized area. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Planning Skills and Flexibility: to develop new approaches and advance campaign goals and objectives. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Interpersonal, Negotiating and Communications Skills: to work within teams; to facilitate public speaking engagements and interactions with other groups and interest sectors; to gain new allies and promote coalition work; to deal with the media. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Research and Analytical Skills: to evaluate information, plan strategies and anticipate ramifications. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Tact and Enthusiasm: to function effectively within a highly charged atmosphere, under public scrutiny. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Computer Skills: to utilize word-processing and telecommunications capabilities; to develop written materials on campaign issues and strategies. </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Italic';">KNOWLEDGE REQUIREMENTS </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of the overall goals and intent of Greenpeace (including policies, guidelines, programs, campaigns and project activities): to lead campaign activities in support of the&nbsp;</span><span style="font-family: Arial; font-size: 11pt;">world's largest environmental protection group.</span></p>
</div>
</div>
</div>
<div class="page" title="Page 3">
<div class="layoutArea">
<div class="column">
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of environment/ecology issues in general: to provide specialized campaign expertise. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of the business sector and investment communities: to provide leadership and expertise in engaging these targets. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of the media including responsibilities, equipment, techniques, policies, practices, scope, audience, tactics, structure, impact: to use the media to further campaign objectives; to increase awareness and gain public support; to communicate with relevant industry and government. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of campaign techniques, strategies and practices: to provide key leadership and assume the role of campaign specialist with respect to a critical area of ecological concern. </span></p>
<p><span style="font-size: 11pt; font-family: 'Arial,Bold';">QUALIFICATIONS: </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Bachelor's degree (masters preferred) in business management, finance, environment, law or public policy or related field. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">At least three years of relevant experience. </span></p>
<p><span style="font-size: 11pt; font-family: Arial;">Knowledge of environmental/sustainability issues and an understanding of corporations (including corporate reporting tools), the socially responsible investment community, and the environmental movement.</span></p>
</div>
</div>
</div>