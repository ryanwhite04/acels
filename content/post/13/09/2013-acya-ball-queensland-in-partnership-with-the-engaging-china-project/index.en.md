{
  "title": "<!--:en-->2013 ACYA Ball delivers glamour, raises funds for Gansu earthquake appeal in partnership with ECP QLD<!--:-->",
  "author": "ACYA",
  "date": "2013-09-27T07:35:44+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "1235447_10151873615361131_1433022083_n",
      "type": "image",
      "src": "1235447_10151873615361131_1433022083_n.jpg"
    },
    {
      "title": "1229934_10151873615001131_1821919574_n",
      "type": "image",
      "src": "1229934_10151873615001131_1821919574_n.jpg"
    },
    {
      "title": "1234776_10151873614076131_1185899411_n",
      "type": "image",
      "src": "1234776_10151873614076131_1185899411_n.jpg"
    },
    {
      "title": "1236457_10151872322275102_949392455_n",
      "type": "image",
      "src": "1236457_10151872322275102_949392455_n.jpg"
    },
    {
      "title": "1240074_10151873616576131_1633313619_n",
      "type": "image",
      "src": "1240074_10151873616576131_1633313619_n.jpg"
    },
    {
      "title": "acya",
      "type": "image",
      "src": "acya.jpg"
    }
  ]
}
<!--:en--><a href="http://www.acya.org.au/wp-content/uploads/2013/09/1235447_10151873615361131_1433022083_n.jpg"><img class="size-full wp-image-1590 aligncenter" alt="1235447_10151873615361131_1433022083_n" src="http://www.acya.org.au/wp-content/uploads/2013/09/1235447_10151873615361131_1433022083_n.jpg" width="720" height="480" /></a>

On Saturday the 21<sup>st</sup> of September, ACYA Chapters at the University of Queensland (UQ) and Queensland University of Technology (QUT) hosted the highly successful 2013 ACYA Ball at the Marriott Hotel Brisbane.

Bringing together 100 ACYA members and 20 VIPs, ACYA Ball continues to thrive as the flagship annual network<a name="_GoBack"></a>ing event celebrating ACYA’s successes in Queensland.

Chapter executives had the honour of hosting a group of diverse and distinguished professionals including Dr. Max Lu, Deputy Vice Chancellor for Research at UQ and recently awarded the title of Queensland Great by Premier Campbell Newman; Mr. Bill O’Chee, former Australian Senator and the first Chinese-Australian Member of Parliament; and Mr. Geoffrey Ewing, President of the Australian Institute of International Affairs Queensland. In addition, VIP representatives from PricewaterhouseCoopers, Auskin Group, UQ Business School, Chinalco and CNOOC showed their generous support.

In partnership with Engaging China Project (ECP) Queensland, a rewarding prize raffle was also held on the night to raise funds for young victims of the July earthquake in Gansu, one of the poorest provinces in China. The earthquake caused not only a tragic loss of life but also affected 360 schools and severely disrupted the education of over ten thousand students in the region. Ten local businesses and organisations donated prizes to the raffle and Ball attendees generously contributed nearly $1,000 in proceeds. This money will be used to support the reconstruction effort in Gansu supported by the China Youth Development Foundation. ACYA and ECP would like to thank these organisations for their much appreciated contributions to a meaningful and enjoyable evening.

Here is a selection of photos from the night:

<a href="http://www.acya.org.au/wp-content/uploads/2013/09/1229934_10151873615001131_1821919574_n.jpg"><img class="alignnone size-full wp-image-1591" alt="1229934_10151873615001131_1821919574_n" src="http://www.acya.org.au/wp-content/uploads/2013/09/1229934_10151873615001131_1821919574_n.jpg" width="720" height="480" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/09/1234776_10151873614076131_1185899411_n.jpg"><img class="alignnone size-full wp-image-1592" alt="1234776_10151873614076131_1185899411_n" src="http://www.acya.org.au/wp-content/uploads/2013/09/1234776_10151873614076131_1185899411_n.jpg" width="720" height="480" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/09/1236457_10151872322275102_949392455_n.jpg"><img class="alignnone size-full wp-image-1593" alt="1236457_10151872322275102_949392455_n" src="http://www.acya.org.au/wp-content/uploads/2013/09/1236457_10151872322275102_949392455_n.jpg" width="720" height="480" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/09/1240074_10151873616576131_1633313619_n.jpg"><img class="alignnone size-full wp-image-1594" alt="1240074_10151873616576131_1633313619_n" src="http://www.acya.org.au/wp-content/uploads/2013/09/1240074_10151873616576131_1633313619_n.jpg" width="720" height="480" /></a><!--:-->