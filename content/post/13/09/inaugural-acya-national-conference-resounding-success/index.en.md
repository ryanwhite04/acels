{
  "title": "<!--:en-->Inaugural ACYA National Conference resounding success<!--:--><!--:zh-->ACYA全国大会圆满落幕<!--:-->",
  "author": "ACYA",
  "date": "2013-09-19T11:13:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "1175318_10153210743075121_1583470273_n",
      "type": "image",
      "src": "1175318_10153210743075121_1583470273_n.jpg"
    },
    {
      "title": "acya",
      "type": "image",
      "src": "acya1.jpg"
    }
  ]
}
<!--:en--><a href="http://www.acya.org.au/wp-content/uploads/2013/09/acya1.jpg"><img class="size-full wp-image-1728 aligncenter" alt="acya" src="http://www.acya.org.au/wp-content/uploads/2013/09/acya1.jpg" width="618" height="358" /></a>

ACYA kicked off its first annual National Conference in style on Friday 23 August, bringing together 58 delegates from the National Executive and ACYA Chapters from Australia and China at the University of Queensland (UQ).

The conference officially commenced with a formal evening at Jade Buddha’s Shadow Lounge in the heart of Brisbane. Australia’s first Chinese-Australian Senator, <a href="http://en.wikipedia.org/wiki/Bill_O'Chee" target="_blank">Mr Bill O’Chee</a>, delivered an insightful presentation on Australia-China relations. Saturday’s highlights included presentations from <a href="http://www.linkedin.com/pub/charmian-campbell/b/6b/3a9" target="_blank">Charmian Campbell</a> on creating winning teams within not-for-profit organisations and <a href="http://www.acbc.com.au/deploycontrol/files/upload/acbc_vic_MichaelKbio_4June%202012.pdf" target="_blank">Michael Komesaroff</a>, who spoke on management with Chinese characteristics.

Delegates engaged in energetic workshops on ACYA's growth, organisational structure, internal management and ability to deliver value to members while brainstorming new projects and opportunities for enhanced interstate collaboration. These were ably led by Executive members Tom Williams, Jimmy Zeng, Sophia Chen and Philip Liu. Training sessions on project management and execution, leadership, effective communication, relationship management and the effective use of funds followed, along with fruitful roundtable discussions on a range of topics.<!--:--><!--:zh--><p>ACYA第一届全国大会于8月23日星期五成功召开，58名来自<wbr />全国管理委员会和ACYA中澳两国各分会的参会代表齐聚<a href="http://www.uq.edu.au/" target="_blank">昆士兰大<wbr />学</a> (UQ)、共议ACYA的未来发展。</p>
<p>本届全国大会的开幕晚宴在位于布里斯班市中心的Jade Buddha内的<a href="http://www.shadowlounge.com.au/" target="_blank">Shadow Lounge</a>举行。澳大利亚首位华裔联邦参议员<a href="http://zh.wikipedia.org/wiki/%E9%98%BF%E5%BF%97" target="_blank">比尔阿志</a>在晚宴上<wbr />就中澳关系发表了富有深度的讲话。当晚的其他焦点活动还包括：<a href="http://www.linkedin.com/pub/charmian-campbell/b/6b/3a9" target="_blank">C<wbr />harmian Campbell</a>关于如何在非营利组织中培养成<wbr />功团队的讲话、以及<a href="http://www.acbc.com.au/deploycontrol/files/upload/acbc_vic_MichaelKbio_4June%202012.pdf" target="_blank">Michael Komesaroff</a>关于如何在中国文化环境中进行管理活动的讲<wbr />话。</p>
<p>在全国大会上，参会代表们积极参与了关于ACYA未来发展、<wbr />组织结构规划、内部管理、成员福利等主题的一系列研讨活动。<wbr />全国大会的各项研讨在全国管理委员会成员<a href="http://www.acya.org.au/Profiles/Thomas%20Williams.pdf" target="_blank">卫涛</a>、<a href="http://www.linkedin.com/in/jimmyzeng" target="_blank">曾瑞祥</a>、<a href="http://www.linkedin.com/profile/view?id=165922436" target="_blank">陈桐</a>和<a href="http://www.linkedin.com/pub/philip-liu/b/336/933" target="_blank">刘<wbr />乐</a>的带领下下积极有效地展开。在这些活动中，<wbr />代表们通过头脑风暴，提出了许多在未来提升ACYA各分会间合作<wbr />的计划和机遇。代表们还参加了包括项目管理实行、领导力培养、<wbr />沟通技巧、关系管理、<wbr />资金运用等主题的培训活动和一系列涉及不同主题的圆桌讨论活动。</p>
<p>全国大会的闭幕日当天晴空万里，代表们共同参加了一次由ACYA<wbr />昆士兰大学分会主办的烧烤餐会。<wbr />这次愉快的聚餐活动为这个充实而收获颇丰的会议周末画上了圆满的<wbr />句号。全国大会后再出发的ACYA将会是一个更具凝聚力、<wbr />更积极向上、更有活力的组织。</p>
<p>ACYA在此感谢所有前来布里斯班的参会代表们和所有来自ACY<wbr />A昆士兰分会的大会志愿者们。ACYA在此感谢所有赞助方和战略<wbr />伙伴，是他们长期以来的支持使得2013年度ACYA全国大会得<wbr />以成功举办。您可以在<a href="http://www.acya.org.au/" target="_blank">ACYA官方网站</a>浏览所有赞助方和战略伙伴<wbr />的信息。</p>
<p>我们期待2014年度ACYA 全国大会的再次召开——与您相约2014年再见！</p><!--:-->