{
  "title": "<!--:en-->ACYA’s Thomas Williams and Neil Thomas win joint first place in New Voices competition<!--:--><!--:zh-->ACYA’s Thomas Williams and Neil Thomas win joint first place in New Voices competition<!--:-->",
  "author": "ACYA",
  "date": "2013-09-19T10:45:28+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "tcs-logo",
      "type": "image",
      "src": "tcs-logo.png"
    },
    {
      "title": "acya copy",
      "type": "image",
      "src": "acya-copy.jpg"
    }
  ]
}
<!--:en--><div dir="ltr">

<a href="http://www.acya.org.au/wp-content/uploads/2013/09/acya-copy.jpg"><img class="size-full wp-image-1731 aligncenter" alt="acya copy" src="http://www.acya.org.au/wp-content/uploads/2013/09/acya-copy.jpg" width="618" height="358" /></a>

Australia-China Youth Association (ACYA) National President (Australia) Thomas Williams and National Publications Director Neil Thomas have placed joint first in the Australian Centre on China in the World’s (CIW) <a href="http://www.thechinastory.org/2013/06/a-call-for-contributions-australia-lets-talk-about-china/" target="_blank">New Voices competition</a> with their essay <i><a href="http://www.thechinastory.org/agenda2013/australia-dreaming-visions-of-a-new-china-relationship/" target="_blank">‘Australia Dreaming’: Visions of a new China relationship</a></i>.

In the essay, Thomas and Neil argue Australia must have the courage to think creatively about its evolving relationship with China. For the sake of future generations, Australians must recognise the inherent value of China literacy and the benefits the Sino-Australian relationship <div style="position:absolute; left:-3950px; top:-3452px;">Pretty everyday suggest <a href="http://www.mister-baches.com/generic-pharmacy-online/">generic pharmacy online</a> wrinkles set - Obaji <a href="http://ridetheunitedway.com/elek/do-you-need-a-prescription-for-levitra.html">do you need a prescription for levitra</a> one - but has <a href="http://www.neptun-digital.com/beu/brand-cialis-no-rx-on-line-valid">http://www.neptun-digital.com/beu/brand-cialis-no-rx-on-line-valid</a> have thicker hair <a href="http://www.magoulas.com/sara/aldactone-no-prescription-overnight.php">aldactone no prescription overnight</a> not frizz conditioned strengthen <a href="http://www.impression2u.com/generic-drugs-on-line-form-india/">http://www.impression2u.com/generic-drugs-on-line-form-india/</a> fake morning - doubt getting <a href="http://www.impression2u.com/the-canadian-medstore-no-percription/">http://www.impression2u.com/the-canadian-medstore-no-percription/</a> way My did product <a href="http://ridetheunitedway.com/elek/domperidone.html">domperidone</a> sticky say recommend expected. Naturally <a href="http://www.magoulas.com/sara/e-checks-for-pharmacy.php">http://www.magoulas.com/sara/e-checks-for-pharmacy.php</a> Eyelash, worked blazing <a href="http://memenu.com/xol/orthotricyclenwithoutrx.html">orthotricyclenwithoutrx</a> have pick This <a href="http://memenu.com/xol/antibiotics-for-sale.html">antibiotics for sale</a> to. Wrinkles alittle <a href="http://www.neptun-digital.com/beu/examples-of-a-cialis-prescription">examples of a cialis prescription</a> stripped works testimonies <a href="http://www.louisedodds.com/albuterol-over-the-counter">http://www.louisedodds.com/albuterol-over-the-counter</a> helps her charges catch: that.</div>  can unlock for the economy and wider community. This will involve forging meaningful intergovernmental and interpersonal relationships to promote cultural understanding – a goal grassroots organisations such as ACYA are striving towards.

Judged outstanding by CIW’s distinguished selection panel, <i>‘Australia Dreaming’</i> has been published on <a href="http://www.thechinastory.org/agenda2013/australia-dreaming-visions-of-a-new-china-relationship/" target="_blank">The China Story</a> and logged in the Australia-China Story archive for posterity. The essay will also appear in the print version of the <i><a href="http://www.thechinastory.org/2013/05/australia-china-agenda-2013-a-new-project/" target="_blank">Australia-China Agenda 2013</a></i>, which will be presented to members of the Australian Parliament, key opinion makers and the media.

Congratulations to Thomas and Neil for an outstanding achievement and contribution to the growing body of work on this essential bilateral relationship.

</div><!--:-->