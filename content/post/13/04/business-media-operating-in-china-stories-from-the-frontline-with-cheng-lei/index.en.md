{
  "title": "Business Media Operating in China: Stories from the Frontline with Cheng Lei",
  "author": "ACYA",
  "date": "2013-04-18T14:02:20+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Cheng Lei",
      "type": "image",
      "src": "cheng.jpg"
    }
  ]
}
<!--:en--><span style="line-height: 1.3em;">ACYA presents the first Beijing Forecast event for this semester. ACYA has invited respected CCTV news journalist and anchor Cheng Lei from BizAsia to give a talk on business media operations in China. She will also discuss her professional insights on Australia-China business relations.</span>
<h2><span style="line-height: 1.3em;">Profile</span></h2>
<span style="line-height: 1.3em;">Cheng Lei began her finance career in Australia, holding accountant and business analyst roles with Cadbury Schweppes and ExxonMobil. She returned to China in 2001 as a business analyst for a Sino-Australian logistics joint venture.</span>

<span style="line-height: 1.3em;">From 2002, Cheng Lei began reporting on China's business and economic issues. She has conducted one-on-one interviews with CEOs and political figures, producing financial stories and daily market reports. She has also spent nine years with CNBC as their China correspondent. </span>
<h2><span style="line-height: 1.3em;">Event Details</span></h2>
<span style="line-height: 1.3em;">RSVP is required as spots are limited. So p</span><span style="line-height: 1.3em;">lease email </span><a style="line-height: 1.3em;" href="mailto:president_BJ@acya.org.au">Mert</a><span style="line-height: 1.3em;">.</span>

<span style="line-height: 1.3em;">Time and Date: 7pm Thursday 25th of April </span>

<span style="line-height: 1.3em;">Location: 11.5.302 Xingfuyincun , Chaoyang District </span>

<span style="line-height: 1.3em;">潮阳区，幸福一村 11.5.302 </span>

<a href="http://goo.gl/maps/K3uEZ">Map view</a>

<span style="line-height: 1.3em;">Directions: Opposite Workers Stadium, in front of Purple Haze Thai Restaurant </span>

<span style="line-height: 1.3em;">If you have any problems please contact either Mert or Victoria on 18612385205 or 18600820913</span><!--:--><!--:zh-->&nbsp;

ACYA 举行本季第一届北京预测会议。我们邀请到中国中央电视台商业亚洲节目的权威记者及主播陈蕾就商业媒体如何在中国运作发表讲话。她也对了澳中商业关系发展发表了专业的观点。

&nbsp;

<b>简介：</b><b></b>

陈蕾的金融职业生涯始于澳洲，她曾作为澳洲吉百利史威士和艾克森美孚两家公司的会计师和业务分析师。2001年，她以澳中合资企业的业务分析师的身份回到中国。

2002年起，她开始报导中国商业和经济新闻。她曾面对面采访过数位公司的CEO和政治人物，并发布金融热点问题和每日市场的报告。她也曾在CNBC做过９年的中国通信记者。

&nbsp;

<b>会议信息：</b><b></b>

<b>座位有限，订座请电邮</b>Mert

会议时间：４月２５号晚上７点

会议地点：　朝阳区幸福一村 11.5.302

<span style="text-decoration: underline;">查看地图</span>

位置：　工人体育场对面，紫雾泰国餐馆前。

如有疑问，请联系Mert（18612385205）, Victoria（18600820913）<!--:-->