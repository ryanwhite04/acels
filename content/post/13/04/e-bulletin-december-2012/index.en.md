{
  "title": "E-Bulletin December 2012",
  "author": "ACYA",
  "date": "2013-04-01T17:02:35+10:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en--><!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: left;"><a style="color: #336699; text-decoration: underline; font-weight: normal;" href="http://www.acya.org.au/"><img style="height: 257px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; border: 0; line-height: 100%; outline: none; text-decoration: none;" alt="ACYA E-Bulletin December 2012" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/image_13550389857041355038994.jpg" width="600" height="257" border="0" /></a></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h1 class="h1" style="color: #202020; display: block; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="font-size: 18px;"><span style="color: #ff0000;">Nominations for 2013 ACYA National Executive Now Open</span></span></h1>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/"><img style="width: 160px; height: 145px; margin-right: 10px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo_1_small.png" width="160" height="145" align="left" border="0" /></a>Are you passionate about Australia-China relations? Would you like to join the premier Australia-China youth engagement initiative? Then nominate for the 2013 National Executive.

Nominations close <strong>December 21</strong>. Visit the ACYA <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/p2p/news-announcements/307-nominations-for-2013-acya-national-executive-now-open" target="_blank">website</a> or contact <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:jeffrey.sheehy@acya.org.au">jeffrey.sheehy@acya.org.au</a> for more details.

</div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">Australia-China Young Professionals Initiative</span></span></strong></span></h4>
&nbsp;
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;">

The Australia-China Young Professionals Initiative and the Boston Consulting Group hosted Australian Stock Exchange CEO<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/2012/11/13/melbourne-australian-financial-markets-in-the-asian-century-a-discussion-with-elmer-funke-kupper/" target="_blank"> Elmer Funke Kupper</a> in Melbourne on December 4. Elmer discussed the current state of Australia’s economy and regulatory environment as well as the prospects for Australian financial markets in the Asian Century.

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acypi.org.au"><img style="line-height: 100%; outline-style: none; outline-color: invert; outline-width: medium; width: 245px; display: inline; margin-bottom: 5px; height: 182px; margin-left: 5px; text-decoration: none; border: 0; outline: none;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYPI_Beijing_Daniel_Andrews_1.jpg" width="245" height="182" align="right" border="0" /></a>ACYPI Beijing and the China Australia Chamber of Commerce jointly hosted a breakfast with the Victorian Leader of the Opposition, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/2012/12/02/beijing-breakfast-with-the-hon-daniel-andrews-mp-opposition-leader-victoria/" target="_blank">Daniel Andrews</a> on December 7.

On his first trip to China, Daniel Andrews shared his views on the Australia-China relationship and the future direction of opportunities for Australians in China and the growing importance of cross-cultural dialogue.

</div>
<div id="cke_pastebin" style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"></div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…</span></span></span></h4>
<span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Gareth Durrant</strong></span></span>
<strong style="font-family: arial, helvetica, sans-serif; color: #ff0000;">Team Leader, Youth Empowerment Against HIV/AIDS</strong>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;">
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"></div>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/GarethDurrant"><img style="line-height: 100%; margin: 5px 10px 5px 5px; outline-style: none; outline-color: invert; outline-width: medium; width: 200px; display: inline; height: 160px; text-decoration: none; border: 0; outline: none;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Gareth_Durrant.jpg" width="200" height="160" align="left" border="0" /></a></div>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"><strong style="line-height: 150%;"><span>You're a graduate of the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.ntu.edu.tw/engv4/" target="_blank">National Taiwan University</a> in Taipei. How did that happen?</span></strong></div>
<span>I went to Taiwan as an exchange student and ended up studying in Beijing the year after. I eventually decided to do my undergraduate at NTU because I wanted to graduate from a Chinese speaking university. It didn't make sense to study Chinese in Australia, when living there and studying as an international student was an option. My grades were terrible, and it was a hard slog, but I would recommend it anyone.</span>

<strong><span>How did you end up working in sexual health?</span></strong>
<span>Last year when I was still living in Perth and working in mining, I got involved with the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.waaids.com/" target="_blank">Western Australian AIDS Council</a>. My job as an outreach officer was to engage with men at 'beats', public places where men cruise for sex, to raise awareness around safe sex. Naturally I found it more interesting than selling steel to Chinese clients and decided to pursue it</span>.

<strong>Can you tell us about your work with <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.redaware.org.au/" target="_blank">YEAH</a>?</strong>
<span>I am the Melbourne Team Leader for the sexual health peer education program. Basically, I rally the troops to participate in sexual health workshops in schools and the wider community as well as liaise with Victorian based stakeholders. You'd be surprised how many local businesses, community organisations and local government staffers I have spoken to about <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://en.wikipedia.org/wiki/Chlamydia_infection" target="_blank">Chlamydia</a>.</span>

<em>Gareth is an <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acyd.org.au/" target="_blank">Australia-China Youth Dialogue 2012</a> deligate and an ambassador for the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au/" target="_blank">Engaging China Project</a>.</em>

</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: Arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial, helvetica, sans-serif; color: #ff0000; font-size: large;">Engaging China Project</span></h4>
<span><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au/"><img style="line-height: 100%; outline-style: none; outline-color: invert; outline-width: medium; width: 220px; display: inline; margin-bottom: 5px; height: 163px; margin-left: 7px; text-decoration: none; border: 0px; outline: none;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/531538_405116929545718_1431023725_n.jpg" width="220" height="163" align="right" border="0" /></a>The Engaging China Project (ECP) is excited that Mandarin is one of four priority languages for the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.org.au/index.php/en/p2p/news-announcements/300-acya-group-media-release-australia-in-the-asian-century-white-paper" target="_blank">Asian Century</a> and look forward to an increased opportunity to make a difference. ECP contextualises China for a high school audience by exposing students and teachers to young Asia-literate university students and young professionals who share their experiences and map out the opportunities that the Asian Century holds.</span>

ECP is seeking a new national treasurer to facilitate our developing in 2013. Please send all enquires and expressions of interest to <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:sebastian.jurd@acya.org.au">sebastian.jurd@acya.org.au</a>. To find out more visit our <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au" target="_blank">website</a> and follow us on <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/EngagingChinaProject?fref=ts" target="_blank">Facebook</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://twitter.com/EngagingChina" target="_blank">Twitter</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: Arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;">Internships</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://english.caixin.com/" target="_blank">Caixin Media</a>, one of the most internationally respected and influential media outlets in China, is offering an internship opportunity exclusively for ACYA members. For more information contact: <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:sam.wall@acya.org.au">sam.wall@acya.org.au</a> and visit the ACYA <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/careers/featured-opportunities/319-caixin-acya-internship" target="_blank">website</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-family: Arial; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large; line-height: 34px; -webkit-text-size-adjust: none;">Connect with ACYA Group</span></h4>
Twitter:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya"><img style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" width="75" height="73" align="right" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya">@a_cya</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://twitter.com/EngagingChina">@EngagingChina</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/ACYDialogue">@ACYDialogue</a>

Facebook:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/"><img style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" width="75" height="75" align="right" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">ACYA National</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/EngagingChinaProject?fref=ts">Engaging China Project</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts">ACYD</a>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/#!/a_cya">follow on Twitter</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">friend on Facebook</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=6619424542&amp;e=b9ce0f9afe">forward to a friend</a></div></td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://transasialawyers.com/" target="_blank"><img style="margin: 2px; width: 250px; height: 122px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="TransAsia Lawyers" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" width="250" height="122" align="none" border="0" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://chinainstitute.anu.edu.au/" target="_blank"><img style="margin: 2px; width: 250px; height: 85px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" width="250" height="85" align="none" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.crccasia.com/"><img style="margin: 2px; width: 250px; height: 95px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia_1_crop.jpg" width="250" height="95" align="none" border="0" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"></div>
</div></td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">Header photograph - Stilt-walker, Lanzhou, Chinese New Year 2011, by <strong>Joel Wing-Lun</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">

<span style="color: #2f4f4f;"><em>Copyright © 2012 ACYA, All rights reserved.</em></span>

</div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;">

<span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img title="MailChimp Email Marketing" alt="Email Marketing Powered by MailChimp" src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" /></a> </span></span></span>

</div>
</div></td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage1.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=6619424542">unsubscribe from this list</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage1.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe">update subscription preferences</a></div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  --><!--:--><!--:zh--><!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: left;"><a href="http://www.acya.org.au/" style="color: #336699; text-decoration: underline; font-weight: normal;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/image_13550389857041355038994.jpg" border="0" alt="ACYA E-Bulletin December 2012" width="600" height="257" style="height: 257px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; border: 0; line-height: 100%; outline: none; text-decoration: none;" /></a></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h1 class="h1" style="color: #202020; display: block; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="font-size: 18px;"><span style="color: #ff0000;">Nominations for 2013 ACYA National Executive Now Open</span></span></h1>
<a href="http://www.acya.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo_1_small.png" border="0" width="160" height="145" align="left" style="width: 160px; height: 145px; margin-right: 10px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a>Are you passionate about Australia-China relations? Would you like to join the premier Australia-China youth engagement initiative? Then nominate for the 2013 National Executive.<br /> <br /> Nominations close <strong>December 21</strong>. Visit the ACYA <a href="index.php/en/p2p/news-announcements/307-nominations-for-2013-acya-national-executive-now-open" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a> or contact <a href="mailto:jeffrey.sheehy@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">jeffrey.sheehy@acya.org.au</a> for more details.</div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">Australia-China Young Professionals Initiative</span></span></strong></span></h4>
<br />
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;">The Australia-China Young Professionals Initiative and the Boston Consulting Group hosted Australian Stock Exchange CEO<a href="http://www.acypi.org.au/2012/11/13/melbourne-australian-financial-markets-in-the-asian-century-a-discussion-with-elmer-funke-kupper/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"> Elmer Funke Kupper</a> in Melbourne on December 4. Elmer discussed the current state of Australia’s economy and regulatory environment as well as the prospects for Australian financial markets in the Asian Century. <br /> <br /> <a href="http://acypi.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYPI_Beijing_Daniel_Andrews_1.jpg" border="0" width="245" height="182" align="right" style="line-height: 100%; outline-style: none; outline-color: invert; outline-width: medium; width: 245px; display: inline; margin-bottom: 5px; height: 182px; margin-left: 5px; text-decoration: none; border: 0; outline: none;" /></a>ACYPI Beijing and the China Australia Chamber of Commerce jointly hosted a breakfast with the Victorian Leader of the Opposition, <a href="http://www.acypi.org.au/2012/12/02/beijing-breakfast-with-the-hon-daniel-andrews-mp-opposition-leader-victoria/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Daniel Andrews</a> on December 7.<br /> <br /> On his first trip to China, Daniel Andrews shared his views on the Australia-China relationship and the future direction of opportunities for Australians in China and the growing importance of cross-cultural dialogue.</div>
<div id="cke_pastebin" style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"> </div>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…</span></span></span></h4>
<span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Gareth Durrant</strong></span></span><br /> <strong style="font-family: arial, helvetica, sans-serif; color: #ff0000;">Team Leader, Youth Empowerment Against HIV/AIDS</strong>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;">
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"> </div>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"><a href="https://twitter.com/GarethDurrant" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Gareth_Durrant.jpg" border="0" width="200" height="160" align="left" style="line-height: 100%; margin: 5px 10px 5px 5px; outline-style: none; outline-color: invert; outline-width: medium; width: 200px; display: inline; height: 160px; text-decoration: none; border: 0; outline: none;" /></a></div>
<div style="text-align: left; line-height: 150%; font-family: Arial; color: #505050; font-size: 14px;"><strong style="line-height: 150%;"><span>You're a graduate of the <a href="http://www.ntu.edu.tw/engv4/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">National Taiwan University</a> in Taipei. How did that happen?</span></strong></div>
<span>I went to Taiwan as an exchange student and ended up studying in Beijing the year after. I eventually decided to do my undergraduate at NTU because I wanted to graduate from a Chinese speaking university. It didn't make sense to study Chinese in Australia, when living there and studying as an international student was an option. My grades were terrible, and it was a hard slog, but I would recommend it anyone.</span><br /> <br /> <strong><span>How did you end up working in sexual health?</span></strong><br /> <span>Last year when I was still living in Perth and working in mining, I got involved with the <a href="http://www.waaids.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Western Australian AIDS Council</a>. My job as an outreach officer was to engage with men at 'beats', public places where men cruise for sex, to raise awareness around safe sex. Naturally I found it more interesting than selling steel to Chinese clients and decided to pursue it</span>.<br /> <br /> <strong>Can you tell us about your work with <a href="http://www.redaware.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">YEAH</a>?</strong><br /> <span>I am the Melbourne Team Leader for the sexual health peer education program. Basically, I rally the troops to participate in sexual health workshops in schools and the wider community as well as liaise with Victorian based stakeholders. You'd be surprised how many local businesses, community organisations and local government staffers I have spoken to about <a href="http://en.wikipedia.org/wiki/Chlamydia_infection" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Chlamydia</a>.</span><br /> <br /> <em>Gareth is an <a href="http://acyd.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Australia-China Youth Dialogue 2012</a> deligate and an ambassador for the <a href="http://www.engagingchinaproject.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a>.</em></div>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: Arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial, helvetica, sans-serif; color: #ff0000; font-size: large;">Engaging China Project</span></h4>
<span><a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/531538_405116929545718_1431023725_n.jpg" border="0" width="220" height="163" align="right" style="line-height: 100%; outline-style: none; outline-color: invert; outline-width: medium; width: 220px; display: inline; margin-bottom: 5px; height: 163px; margin-left: 7px; text-decoration: none; border: 0px; outline: none;" /></a>The Engaging China Project (ECP) is excited that Mandarin is one of four priority languages for the <a href="http://acya.org.au/index.php/en/p2p/news-announcements/300-acya-group-media-release-australia-in-the-asian-century-white-paper" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Asian Century</a> and look forward to an increased opportunity to make a difference. ECP contextualises China for a high school audience by exposing students and teachers to young Asia-literate university students and young professionals who share their experiences and map out the opportunities that the Asian Century holds.<br /> <br /> ECP is seeking a new national treasurer to facilitate our developing in 2013. Please send all enquires and expressions of interest to <a href="mailto:sebastian.jurd@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">sebastian.jurd@acya.org.au</a>. To find out more visit our <a href="http://www.engagingchinaproject.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a> and follow us on <a href="http://www.facebook.com/EngagingChinaProject?fref=ts" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a> and <a href="http://twitter.com/EngagingChina" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a>.</span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: Arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;">Internships</span></h4>
<a href="http://english.caixin.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Caixin Media</a>, one of the most internationally respected and influential media outlets in China, is offering an internship opportunity exclusively for ACYA members. For more information contact: <a href="mailto:sam.wall@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">sam.wall@acya.org.au</a> and visit the ACYA <a href="index.php/en/careers/featured-opportunities/319-caixin-acya-internship" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a>.<br />   <br /> <span><a href="http://www.pricecao.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Price Cao</a>, a foreign law firm in Beijing, is looking for Business Development and Administrative Intern. Visit the ACYA <a href="index.php/en/careers/featured-opportunities/321-price-cao-is-looking-for-a-business-development-and-administrative-intern" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a> for more details.</span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-family: Arial; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large; line-height: 34px; -webkit-text-size-adjust: none;">Connect with ACYA Group</span></h4>
Twitter:<a href="https://twitter.com/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" border="0" width="75" height="73" align="right" style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="https://twitter.com/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">@a_cya</a><br /> <a href="http://twitter.com/EngagingChina" style="color: #336699; font-weight: normal; text-decoration: underline;">@EngagingChina</a><br /> <a href="https://twitter.com/ACYDialogue" style="color: #336699; font-weight: normal; text-decoration: underline;">@ACYDialogue</a> <br /> <br /> Facebook:<a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" border="0" width="75" height="75" align="right" style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA National</a><br /> <a href="http://www.facebook.com/EngagingChinaProject?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a><br /> <a href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYD</a></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">follow on Twitter</a> | <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">friend on Facebook</a> | <a href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&id=6619424542&e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">forward to a friend</a> </div>
</td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://transasialawyers.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" border="0" alt="TransAsia Lawyers" width="250" height="122" align="none" style="margin: 2px; width: 250px; height: 122px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><a href="http://chinainstitute.anu.edu.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" border="0" width="250" height="85" align="none" style="margin: 2px; width: 250px; height: 85px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.crccasia.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia_1_crop.jpg" border="0" width="250" height="95" align="none" style="margin: 2px; width: 250px; height: 95px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"> </div>
</div>
</td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">Header photograph - Stilt-walker, Lanzhou, Chinese New Year 2011, by <strong>Joel Wing-Lun</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <br /> <span style="color: #2f4f4f;"><em>Copyright © 2012 ACYA, All rights reserved.</em></span><br />  </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><br /> <br /> <br /> <br /> <br /> <span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&utm_medium=email&utm_campaign=monkey_rewards&aid=db2f0ea3a68f632de3088eada&afl=1"><img src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" alt="Email Marketing Powered by MailChimp" title="MailChimp Email Marketing" /></a> </span></span></span></div>
<br />    </div>
</td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="http://acya.us1.list-manage1.com/unsubscribe?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe&c=6619424542" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="http://acya.us1.list-manage1.com/profile?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> </div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  --><!--:-->