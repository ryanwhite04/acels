{
  "title": "ACYA is looking for Community Liaison Officers",
  "author": "ACYA",
  "date": "2013-04-22T06:26:50+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA WA",
      "type": "image",
      "src": "ACYA_WA.jpg"
    }
  ]
}
<!--:en-->The ACYA is now looking for Community Liason Officers, to liaise with international student offices and market to international students.

The role requires a solid commitment of 5-7 hours per week. Support and guidance will be constant from the national executive.

The role also offers the chance to make new friends and learn about great things happening around the country and in China.

Initially, we will aim to have two people fill this role: one Chinese, one Australian.
<h1><span style="font-size: 13px;">Please contact Max at: max.parasol@acya.org.au by April 30rd.</span></h1><!--:--><!--:zh-->“澳中青年联合会”招聘2位社区联络官 我们需要您与国际学生办公室保持联系，并且向国际学生推广“澳中青年联合会”。

我们需要您每周固定工作5-7个小时。“澳中青年联合会”全国行政委员会会为你提供协助和指导。

我们会为您提供一个结交新朋友并且了解发生在澳洲和中国大事的平台。 我们需要2位联络官；一位中国人，一位澳洲人。

Please contact Max at: max.parasol@acya.org.au by April 30rd.<!--:-->