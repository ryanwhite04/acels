{
  "title": "Schwarzman Scholarships",
  "author": "ACYA",
  "date": "2013-04-27T00:27:52+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "rsz_1acya_scharzman_large",
      "type": "image",
      "src": "rsz_1acya_scharzman_large.jpg"
    },
    {
      "title": "rsz_1rsz_acya_scharzman_photo",
      "type": "image",
      "src": "rsz_1rsz_acya_scharzman_photo.jpg"
    }
  ]
}
<!--:en-->ACYA China President, Victoria Kung, a Masters candidate at Tsinghua University, was recently invited to attend the announcement ceremony for the Tsinghua University Schwarzman Scholarship Program.

After the ceremony, Ms Kung was introduced to Former Prime Minister and Foreign Minister Kevin Rudd by the Australian Ambassador to China, Frances Adamson. Ambassador Adamson introduced Ms Kung as a representative of the “future leaders of this generation”. Mr Rudd expressed his support for ACYA and congratulated ACYA on its achievements in the Australia-China youth space.

Mr Rudd was at the ceremony as a member of the Honorary Board of Advisors for the Schwarzman Scholars Program, some other Honorary advisors including: Nicolas Sarkozy, Tony Blair, Condoleezza Rice, Henry Kissinger, among many other influential former politicians, diplomats, and businesspeople.

ACYA China President, Victoria Kung commented, “this is great news for young Australians interested in being part of the Asian Century – by applying for the elite Schwarzman Scholarships at Tsinghua University they will have an unparalleled opportunity to meet and become future world leaders.“

<a href="/images/Documents/ACYA%20Media%20Release%20-%20Schwarzman%20Scholarship.pdf">Full media release available</a>.<!--:--><!--:zh--><h2><span style="font-size: 13px;"> “澳中青年联合会（ACYA）”中国主席贡爱玲是清华大学硕士学生，她最近应邀参加了清华大学“苏世民奖学金计划”启动仪式。</span></h2>
启动仪式后，贡爱玲小姐受到了澳洲前任总理和前任外交部长陆克文的接见。澳洲大使称贡爱玲小姐是“未来领导人”的代表。陆克文表达了对ACYA的支持同时也对于ACYA在促进中澳两国青年交流取得的成果深表祝贺。

陆克文先生以“苏世民奖学金项目”荣誉董事会顾问的身份参加了启动仪式。其他与会的荣誉顾问还包括：法国前总统尼古拉·萨尔科齐, 英国前首相托尼·布莱尔, 美国前国务卿康多莉扎·赖斯和亨利·基辛格博士，其中许多是具有高度影响力的政治人物，外交官以及商界人士。

中国ACYA主席贡爱玲表示：“这对于参与到“亚洲世纪”的澳洲青年来说是个好消息，他们可以申请清华大学的“<b>苏世民</b>奖学金”，以后也有机与各国的领导人会面，并且有机会成为未来世界的领导人。

<a href="/images/Documents/ACYA%20Media%20Release%20-%20Schwarzman%20Scholarship.pdf">Full media release available</a>.

<img style="display: block; margin-left: auto; margin-right: auto;" alt="acya scharzman large" src="images/acya_scharzman_large.jpg" width="600" height="800" /><!--:-->