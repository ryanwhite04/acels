{
  "title": "E-Bulletin August 2012",
  "author": "ACYA",
  "date": "2013-04-01T16:58:01+10:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: left;"><a href="http://www.acya.org.au/" style="color: #336699; text-decoration: underline; font-weight: normal;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Your_Text_Here1344353342.jpg" border="0" alt="ACYA E-Bulletin August 2012" width="600" height="350" style="height: 350px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; border: 0; line-height: 100%; outline: none; text-decoration: none;" /></a></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h1 class="h1" style="color: #202020; display: block; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="font-size: 18px;"><span style="color: #ff0000;">Australia-China Youth Dialogue Call for Applications</span></span></h1>
<a href="http://acyd.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/40_years_anniversary_logo.JPG" border="0" width="200" height="180" align="left" style="width: 200px; height: 180px; margin: 3px 5px 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a>The 2012 <a href="http://acyd.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Australia-China Youth Dialogue</a> will bring 30 future leaders from Australia and China to Beijing and Chengdu from 19-24 October to discuss, debate and analyse the issues that will shape Sino-Australian relations in coming decades. In this year, the 40th anniversary of diplomatic relations between Australia and China, the theme of the ACYD is: At 40 there are no doubts: next steps in Australia-China relations.<br /> <br /> Applications for the 2012 Australia-China Youth Dialogue close on <strong>31 August </strong><strong>for </strong><strong>Australian delegates</strong> and <strong>15 September</strong><strong> for </strong><strong>Chinese delegates.</strong> ACYD delegates must be Australian or Chinese and between the ages of 18 and 35 on 24 October, 2012. Visit <a href="http://acyd.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">acyd.org.au</a> for more information.</div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial,helvetica,sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…</span></span></span></h4>
<span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Carol Danmajyid (Danma)<a href="http://teamdanma.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/carold.png" border="0" width="125" height="156" align="right" style="width: 125px; height: 156px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" /></a><br /> Communications Student at the University of Technology, Sydney, and ACYD Alumna</strong></span></span><br /> <br /> <span style="font-family: arial,helvetica,sans-serif;"><strong>Where did you grow up?</strong><br />  <br /> I grew up in Bdechen (Daiqian) Village, Tianzhu County, <a href="http://en.wikipedia.org/wiki/Gansu" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Gansu Province</a>. Most villagers in Bdechen are pastoralists. Every March, villagers move from their winter encampment to a spring pasture and then to a summer pasture at the beginning of June. Women mostly work at home while men go out herding or leave the village to find work. Older villagers speak fluent Tibetan and young people often speak a mix of Tibetan and Chinese.<br />  <br /> Bdechen villagers are Tibetan Buddhists. Many people, especially older people, worship the village <a href="http://en.wikipedia.org/wiki/Stupa" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">stupa</a> by circumambulating it everyday, and every household has their own shrine, or <em>mchod khang</em>. Villagers still sing traditional songs. Older men, especially, like to sing <a href="http://en.wikipedia.org/wiki/Epic_of_King_Gesar" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">King Gesar’s songs</a>, and tell folk stories and riddles to young people.<br />  <br /> <strong>How has life changed in your village?</strong><br />  <br /> In the past, we didn't have highways, TVs or cars, and were quite isolated from the rest of world. Now, modernisation is challenging traditional customs. Young people watch TV and listen to the radio instead of listening to folksongs and stories from elders. It is now quite rare to hear folksongs at weddings and other ceremonies.<br />  <br /> <strong>Why did you decide to come to Australia?</strong><br />  <br /> I heard much about Australia when I was working with several Australians at a local NGO in <a href="http://en.wikipedia.org/wiki/Yunnan" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Yunnan</a>. I was especially fascinated to hear about the <a href="http://en.wikipedia.org/wiki/Blue_Mountains_(New_South_Wales)" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Blue Mountains</a>, Australia’s <a href="http://en.wikipedia.org/wiki/Indigenous_Australians" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">indigenous people</a>, beaches and universities. I decided to apply to study in Australia so I could experience what I had heard from my friends.<br />  <br /> <strong>What are your plans for the future?</strong><br />  <br /> I would like to go back to China after my degree, and continue to work with local NGOs to make positive changes. I hope to become an expert on sustainable development.<br /> <br /> For more on Danma, please visit her <a href="http://teamdanma.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a>.</span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">ACYA Journal Deadline now 20 August</span></span></strong></span><br /> <br /> <span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="http://www.acya.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo_1_.gif" border="0" width="150" height="136" align="left" style="width: 150px; height: 136px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px 7px 5px 5px; border: 0;" /></a></span></span></h4>
<span style="font-family: arial,helvetica,sans-serif;">The deadline for submissions to the ACYA Journal has been extended to <strong>11:59pm Monday 20 August</strong>. The ACYA Journal will be bilingual, peer-reviewed, and offering cash prizes of up to $500! We welcome submissions of academic essays, opinion pieces, and creative work in either English or Chinese. The Journal will be launched at the <a href="http://acyd.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">2012 ACYD</a> in October. Click for more details in <a href="http://acya.org.au/PDF/Call%20for%20Submissions%20-%20ACYA%20Journal.pdf" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">English</a> and <a href="http://acya.org.au/PDF/Call%20for%20Submissions%20%28CN%29.pdf" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">中文</a>.</span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Engaging China Project</span></span></span></strong><br /> <br /> <span style="font-family: arial,helvetica,sans-serif;">The <a href="http://www.engagingchinaproject.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a> (ECP) visited three NSW schools in July, with trips to Canterbury Girls, Killarney Heights High School, Sydney Boys High and Scots College lined up for August.<br />  <br /> We are seeking a National Deputy Director as well as enthusiastic ECP ambassadors. Please send all enquires to National Communications Manager <a href="mailto:sebastian.jurd@acya.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">sebastian.jurd@acya.org.au</a>. To find out more visit our website <a href="http://www.engagingchinaproject.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">www.engagingchinaproject.org.au</a> and follow us on <a href="http://www.facebook.com/EngagingChinaProject" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a> and <a href="https://twitter.com/EngagingChina" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a>.<br /> <br /> <img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/531538_405116929545718_1431023725_n.jpg" border="0" width="200" height="149" align="none" style="width: 200px; height: 149px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /> <img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/428377_390440641013347_1842183963_n.jpg" border="0" width="200" height="149" align="none" style="width: 200px; height: 149px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /> </span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">ACYA@UQ Welcome Picnic, Karaoke and “Great Aussie Barbie”</span></span></strong></h4>
<span style="font-family: arial,helvetica,sans-serif;"><a href="http://www.facebook.com/groups/uqacya/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA@UQ</a> is holding a welcome picnic at UQ Lakes on <strong>August 10</strong>, Karaoke in Brisbane's CBD on <strong>August 11</strong>, and an incredibly </span><span style="font-family: arial,helvetica,sans-serif;">intense 键</span><span style="font-family: arial, helvetica, sans-serif;">子 competition at </span><span style="font-family: arial, helvetica, sans-serif;">UQ's Great Court in <strong>Week 4</strong>. We are also holding a "Great Aussie Barbie" on <strong>August 25</strong> to welcome UQ’s 1500 new international students and introduce Aussie lingo and culture. For more info contact </span><a href="mailto:rachael.ross@acya.org.au" target="_blank" style="font-family: arial, helvetica, sans-serif; color: #336699; font-weight: normal; text-decoration: underline;">rachael.ross@acya.org.au</a><span style="font-family: arial, helvetica, sans-serif;">.</span></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">ACYA Sydney Uni Semester 2 Welcome Party</span></span></span></strong></h4>
<a href="http://www.facebook.com/pages/Australia-China-Youth-Association-Sydney-Uni/186676451368009" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA Sydney Uni</a> is holding a party on <strong>Wednesday, 15 August</strong>, for new and existing members with games, trivia and pizza! See the <a href="http://www.facebook.com/events/412736162106218/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">event page</a> for more details.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Mandarin Corner</span></span></span></strong></h4>
Finding people who are enthusiastic about practicing Mandarin can be hard. Finding the time can be even harder. Join like-minded young professionals for a series of Mandarin Corner events in Sydney. Our next meeting will be <strong>Friday, 17 August at 6pm</strong> in <a href="http://www.jetbarcaffe.com/JET_BAR_CAFFE.html" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Jet Cafe</a> (QVB). Email <a href="mailto:rachel.caine@student.unsw.edu.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Rachel Caine</a> or <a href="mailto:roy.yu@dlapiper.com" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Roy Yu</a> for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial,helvetica,sans-serif;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">Internships</span></span></strong></span></h4>
Applications for the <a href="http://www.austcham.org/resources/austcham-china-scholarship-program" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">2013 AustCham China Scholarship</a> close <strong>19th August 2012</strong> at <strong>11:30pm Beijing Time</strong>. Visit the <a href="http://www.austcham.org/resources/austcham-china-scholarship-programme/apply-now" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a> for more information about how to apply.<br /> <br /><hr /><br /> <a href="http://transasialawyers.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">TransAsia Lawyers</a> offers three month internships in Beijing or Shanghai exclusively for ACYA members on a rolling basis. Interns will be working in one of the leading law firms licensed to practice in the PRC and get direct work experience with a firm that operates in the Chinese and international legal systems.<br />  <br /> Visit the <a href="index.php/en/careers/featured-opportunities/241-acya-tranasia-laywers-internships" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> or contact <a href="mailto:david.davies@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">david.davies@acya.org.au</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">follow on Twitter</a> | <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">friend on Facebook</a> | <a href="http://us1.forward-to-friend1.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=9e91898de8&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">forward to a friend</a> </div>
</td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://transasialawyers.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" border="0" alt="TransAsia Lawyers" width="250" height="122" align="none" style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><a href="http://chinainstitute.anu.edu.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" border="0" width="250" height="85" align="none" style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.crccasia.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" border="0" width="250" height="175" align="none" style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"> </div>
</div>
</td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <span style="color: #2f4f4f;">Header photograph - An Australian Pub in Suzhou by </span><strong class="gmail_sendername" style="color: #222222; font-family: arial, sans-serif; font-size: 12.499999046325684px; line-height: normal; -webkit-text-size-adjust: auto;">Nicola Boyle</strong></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <br /> <span style="color: #2f4f4f;"><em>Copyright © 2012 ACYA, All rights reserved.</em></span><br />  </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><br /> <br /> <br /> <br /> <br /> <span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" alt="Email Marketing Powered by MailChimp" title="MailChimp Email Marketing" /></a> </span></span></span></div>
<br />    </div>
</td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=9e91898de8" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> </div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  -->