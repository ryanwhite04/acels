{
  "title": "ACYA Newsletter March 2013",
  "author": "ACYA",
  "date": "2013-04-01T17:03:57+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "image_13626623154631362662349",
      "type": "image",
      "src": "image_13626623154631362662349.jpg"
    }
  ]
}
<!--:en--><!-- // Begin Template Header  -->
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="font-size: 18px; color: #ff0000; font-family: arial, helvetica, sans-serif; font-weight: bold; line-height: 22px; -webkit-text-size-adjust: auto;">Welcome</span>

March is already upon us and we have another bumper month ahead for ACYA Group, with ACYPI events locked in for Sydney and Melbourne, Engaging China Project hosting an event on the Gold Coast, and new ACYA chapters starting up at UTS and UniSA. So go ahead and get stuck into this month's ACYA e-bulletin, featuring an interview with ACYA@ANU President Geraint Schmidt, coverage of O-Week festivities at home and abroad, and career opportunities with Wiseman and Extension &amp; Lettuce. ACYA Publications are also looking for new committee members and helping hands - so be sure to get in touch and get involved!

We look forward to seeing you again in April.

祝大家天天开心,

The ACYA Group team.

</div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="margin: 0px 0px 10px; text-align: left; color: #202020; line-height: 100%; font-family: arial; font-size: 22px; font-weight: bold; display: block; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial,helvetica,sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…Geraint Schmidt
Chapter President
ACYA@ANU</span></span></span></h4>
<div style="text-align: left; color: #505050; line-height: 150%; font-family: Arial; font-size: 14px;">
<div style="text-align: left; color: #505050; line-height: 150%; font-family: Arial; font-size: 14px;">

<strong><img style="width: 250px; height: 259px; line-height: 100%; text-decoration: none; display: inline; outline: none; margin: 5px 10px 5px 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/geraint1.jpg" width="250" height="259" align="left" border="0" />How did you get involved with Sino-Australian youth engagement / ACYA?</strong>
My passion for China and Sino-Australian youth engagement was born out of my travels in China in 2011. I unequivocally fell in love with the country, the language, the food, the people, the landscapes, the diversity, and I was inspired by my experiences to want to influence Australians, especially youth, to see the opportunities that engaging with China provides. After feeling frustrated by the lifestyle of a high school boarding house, China rekindled in me a desire to learn and explore, and if anyone became at least somewhat engaged with China after hearing me wax lyrical about it I would be overjoyed.

<strong>What keeps you interested in China?</strong>
Very simply: the desire to go back, and this time with a greater understanding of the language, the people, the issues and the culture. I look forward to the day that I step into China again with all the time in the world to explore both places I visited and loved as well as places I missed out on, preferably on a bicycle.

<strong>What exciting plans do you have up your sleeve for ACYA@ANU in 2013?</strong>
Fundamentally, the ACYA@ANU executive is focused on maintaining a high quality weekly language exchange and on implementing a potential mentoring system where domestic and Chinese-speaking students can form one-on-one relationships to practice the languages they are least confident with. We are planning a film night in the next couple of weeks, a Hot Pot/Mahjong night over the frigidly cold Canberra winter, and an inter-society ball with other Asia-related societies in Semester Two.

<strong>What is your philosophy as ACYA@ANU President?</strong>
We at ACYA@ANU are trying to facilitate interest and engagement between the nationalities and cultures of the Asia-Pacific. We can offer a relatively peaceful space for international students to engage with each other away from the tensions that might exist in their home countries, and I think facilitating these sorts of engagements is what societies like ACYA should be all about. I’m therefore pushing this year for tighter ties between ACYA@ANU and the other societies around campus.

</div>
</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">Australia-China Young Professionals Initiative</span></span></strong></span></h4>
<strong><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/"><img style="width: 250px; height: 177px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYPI2.jpg" width="250" height="177" align="right" border="0" /></a>ACYPI NSW</strong> in conjunction with the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acbc.com.au/" target="_self">Australia-China Business Council</a> invites you to drinks and a discussion on the art of building successful working relationships between Australia and China – <strong>‘The Art of Guanxi: Doing Business in China’</strong>. The event will feature leading China experts <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.rsadvisory.com/people/index.html">Kevin Hobgood-Brown</a>, Managing Director, Riverstone Advisory, and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://advance.org/mr-barry-plews/">Barry Plews</a>, Creative Producer and Dramaturge of Reckless Moments.

<strong>Time:</strong> 6:30pm
<strong>Date:</strong> Tuesday 19<sup>th</sup> March 2013
<strong>Venue:</strong> Shanghai Room, Level 13, The Gateway, 1 Macquarie Place, Sydney
<strong>RSVP by Friday 15 March to:</strong> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acypi.eventbrite.com/" target="_blank">http://acypi.eventbrite.com</a>

<strong>ACYPI Victoria</strong> and the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.aiia.asn.au/" target="_self">Australian Institute of International Affairs</a> also invite you to a discussion with <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://au.linkedin.com/pub/colin-heseltine/30/606/832">Colin Heseltine</a>. Colin's career with the Australian Department of Foreign Affairs and Trade spanned over 40 years. During this time, Colin worked extensively in the Asian region as head of the Asia Pacific Economic Cooperation (APEC) Secretariat and ambassador to the Republic of Korea.

<strong>Time:</strong> 6:00pm
<strong>Date:</strong> Wednesday 3<sup>rd</sup> April 2013
<strong>Venue:</strong> Dyason House, 124 Jolimont Road, East Melbourne
<strong>Please visit <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/" target="_blank">http://www.acypi.org.au/</a> where RSVP details and information regarding the new ACYPI Perth Chapter will be released shortly.</strong>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Engaging China Project</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au/"><img style="width: 250px; height: 333px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ECPeeps_Perth.jpg" width="250" height="333" align="left" border="0" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au/" target="_self">ECP</a> has started 2013 with a bang! All around the country we celebrated Chinese New Year with our local communities. From Perth to the Gold Coast we've been working hard to develop our networks with schools and universities and of course ACYA! We partnered with ACYA on several O-week stalls and our newest ECP Ambassadors are excited to get out into schools, tell their China story and inspire the next generation. Now that school has resumed, ECP will be delivering our Term 1 presentations this month. For more information or to join in the fun please email <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:info@engagingchinaproject.org.au" target="_blank">info@engagingchinaproject.org.au</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; display: block; font-family: arial; font-weight: bold; margin: 0px 0px 10px; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large;">Get Involved in ACYA Publications! <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/p2p/news-announcements/331-acya-publications-call-for-applications-2013"><img style="width: 250px; height: 239px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAJournalLOGO.jpg" width="250" height="239" align="right" border="0" /></a></span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/publications">ACYA National Publications</a> is calling for applications for Sub-Editor positions on the 2013 <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/publications/acya-journal">ACYA Journal of Australia-China Affairs</a> Sub-Committee and the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/publications/opportunities-guide">ACYA Opportunities Guide</a> Sub-Committee, as well as regular Contributors to our unique <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/publications/australia-bites">AustraliaBites</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/publications/china-bites">ChinaBites</a> current affairs and pop culture updates. For more details on how to apply, please visit the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/p2p/news-announcements/331-acya-publications-call-for-applications-2013">ACYA website</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>New Chapters - UTS &amp; UniSA!</strong></span>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="width: 200px; height: 175px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px 8px 3px 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo2.jpg" width="200" height="175" align="left" border="0" /></a>This year a new ACYA Chapter will be starting up at <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uts.edu.au/">UTS</a>! Sydney University, UNSW and Macquarie already have ACYA chapters up and running and it’s about time that UTS joins in the fun!

It’s well known that UTS has a huge Chinese student population that is growing every year, yet there is still no ACYA presence to take advantage of this. So to get things started we need members, members, members! Everyone is talking about how Australia is entering the Asian Century so what better time to join than now?

Everyone is welcome, even if you don’t speak a word of Chinese. Join us at the new <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/147655622061721/">ACYA@UTS Facebook group</a> - it may just be the best thing you do today.

Please contact ACYA@UTS Chapter President <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:president_uts@acya.org.au">Matthew Warr</a> for more details or simply add him on <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/warry86">Facebook</a>.

<hr />

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/153400261484948/?fref=ts"><img style="width: 150px; height: 212px; margin: 5px 8px 5px 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UNISA.jpg" width="150" height="212" align="left" border="0" /></a>The new ACYA@<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.unisa.edu.au/" target="_self">UniSA</a> chapter aims to introduce Chinese culture to Australian youth who are interested in China and vice versa. By delivering a Mandarin course, traditional festival party and other social events, we hope to encourage cross-cultural understanding and different cultural experiences at UniSA.

We are going to have a welcome party working together with ACYA@UniAdelaide and the Chinese Culture Club UniSA in the coming weeks. Feel free to come and join us, you are very welcome!

We will post up-to-date information on our new <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/153400261484948/">Facebook group</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://unione.unilife.edu.au/Clubs/Club.aspx?CID=511">website</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">'Work, Study and Engage with China' - Information Seminar &amp; Networking Event</span></span></span></strong></h4>
<strong>18:00 - 20:00<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://workstudyandengagewithchina.eventbrite.com.au/"><img style="width: 250px; height: 354px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ECP_Logo_Concepts_06_single.jpg" width="250" height="354" align="right" border="0" /></a></strong>
<strong>Friday 22 March 2013</strong>
<strong><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.griffith.edu.au/" target="_self">Griffith University</a> Gold Coast, International Building (G52), Lecture Theatre 1.21</strong>
<strong>Free</strong>

Hosted by <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://paulrackemann.com.au/">Paul Rackemann</a><strong>, </strong>this vibrant seminar and networking event will feature speakers including Engaging China Project’s own <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://au.linkedin.com/pub/alexis-wagner/41/a39/9a1">Alexis Wagner</a> as well as <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://au.linkedin.com/pub/susie-douglas/35/997/5a1">Susie Douglas</a>, Associate Professor Shaofang Liu of Griffith University and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://au.linkedin.com/in/luika">Luika Bankson</a>.

RSVP today by clicking <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://workstudyandengagewithchina.eventbrite.com.au/">here</a>.

This event has been made possible by ACYA Group initiatives <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.engagingchinaproject.org.au/">Engaging China Project</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/">ACYA</a>, in addition to the kind support of the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.griffith.edu.au/tourism-confucius-institute">Griffith University Tourism Confucius Institute</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.crccasia.com/">CRCC Asia</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">ACYA O-Weeks Kick Off across Australia &amp; China</span></span></span></strong>

It's been a busy and exciting time for ACYA's chapters across Australia and China, with university O-Weeks heralding the start of the new semester. Below are some choice shots from ACYA@UniWA, ACYA@UQ, ACYA@USYD and ACYA@Nanjing!

<strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="width: 300px; height: 225px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_WA.JPG" width="300" height="225" align="none" border="0" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="height: 200px; line-height: 18px; width: 300px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UQ.JPG" width="300" height="200" align="none" border="0" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="width: 300px; height: 168px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UQ2.JPG" width="300" height="168" align="none" border="0" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="width: 300px; height: 225px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAUSYD1.jpg" width="300" height="225" align="none" border="0" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="index.php/en/"><img style="height: 376px; line-height: 18px; width: 250px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_NANJING.JPG" width="250" height="376" align="none" border="0" /></a> </span></span></span></strong>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-weight: bold; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ANU Language &amp; Thinking Survey - Seeking Participants</span></h4>
If you are Chinese, have obtained Australian citizenship or residency and are fluent in both Mandarin and English, please consider taking part in the following ANU language and thinking survey by <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://anupsych.us.qualtrics.com/SE/?SID=SV_7OgepHX5ViHV0HP">clicking here</a>. You’ll receive a detailed report about yourself for your efforts! Contact <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:corie.lin@anu.edu.au">Corie Lin</a> for more information.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="margin: 0px 0px 10px; text-align: left; color: #202020; line-height: 100%; font-family: arial; font-size: 22px; font-weight: bold; display: block; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Intership Opportunity - Wiseman</span></span></span></h4>
<strong>Great opportunity to gain valuable work experience in the education community</strong>

Wiseman Summer Internship Programme (WSIP) is looking for passionate interns to work with an outstanding international team on e-learning in Hong Kong. Don’t miss out this great overseas English educational opportunity. For more information, visit <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.wiseman.com.hk/site/about/interns/" target="_blank">http://www.wiseman.com.hk/site/about/interns/</a>.

To apply or seek more information, please contact <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:clement.chung@wiseman.com.hk">Clement Chung</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">International Marketing Executive Wanted - Extension &amp; Lettuce</span></h4>
If you are a young professional looking for a challenge, write business level Mandarin and English and are a qualified, passionate marketer, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://lettuce.com.au/">Extension &amp; Lettuce</a> would like to hear from you.

Based in Extension &amp; Lettuce’s funky South Melbourne office, the International Marketing Executive will be championing their most ambitious online project to date as well as assisting their clients devise and implement international marketing strategies.

Criteria:

• 2 years experience
• Asian marketing knowledge
• Strong multilingual communicator (Mandarin is a must)
• Australian &amp; international online marketing experience
• Interest in the property industry

Applications should be sent to Jon Ellis at <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:jon@extensionco.com">jon@extensionco.com</a> or 200 Wells Street South Melbourne VIC 3205.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-family: Arial; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large; line-height: 34px; -webkit-text-size-adjust: none;">Connect with ACYA Group</span></h4>
Twitter:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya"><img style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" width="75" height="73" align="right" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya">@a_cya</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://twitter.com/EngagingChina">@EngagingChina</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/ACYDialogue">@ACYDialogue</a>

Facebook:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/"><img style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" width="75" height="75" align="right" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">ACYA National</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/EngagingChinaProject?fref=ts">Engaging China Project</a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts">ACYD</a>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/#!/a_cya">follow on Twitter</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">friend on Facebook</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=dc00bfe8c9&amp;e=b9ce0f9afe">forward to a friend</a></div></td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://transasialawyers.com/" target="_blank"><img style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="TransAsia Lawyers" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" width="250" height="122" align="none" border="0" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://chinainstitute.anu.edu.au/" target="_blank"><img style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" width="250" height="85" align="none" border="0" /></a>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.crccasia.com/"><img style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" width="250" height="175" align="none" border="0" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"></div>
</div></td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">Header photograph - Monks on solemn parade, taken by <strong>Nicola Boyle.</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">

<span style="color: #2f4f4f;"><em>Copyright © 2013 ACYA, All rights reserved.</em></span>

</div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;">

<span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img title="MailChimp Email Marketing" alt="Email Marketing Powered by MailChimp" src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" /></a> </span></span></span>

</div>
</div></td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=dc00bfe8c9">unsubscribe from this list</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe">update subscription preferences</a></div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  --><!--:--><!--:zh--><!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: center;"><a href="http://www.acya.org.au/" style="color: #336699; text-decoration: underline; font-weight: normal;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/image_13626623154631362662349.jpg" border="0" alt="ACYA E-Bulletin November 2012" width="600" height="233" style="height: 233px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; margin-left: auto; margin-right: auto; border: 0; line-height: 100%; outline: none; text-decoration: none;" /></a></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><span style="font-size: 18px; color: #ff0000; font-family: arial, helvetica, sans-serif; font-weight: bold; line-height: 22px; -webkit-text-size-adjust: auto;">Welcome</span><br /> <br /> March is already upon us and we have another bumper month ahead for ACYA Group, with ACYPI events locked in for Sydney and Melbourne, Engaging China Project hosting an event on the Gold Coast, and new ACYA chapters starting up at UTS and UniSA. So go ahead and get stuck into this month's ACYA e-bulletin, featuring an interview with ACYA@ANU President Geraint Schmidt, coverage of O-Week festivities at home and abroad, and career opportunities with Wiseman and Extension & Lettuce. ACYA Publications are also looking for new committee members and helping hands - so be sure to get in touch and get involved!<br /> <br /> We look forward to seeing you again in April. <br /> <br /> 祝大家天天开心,<br /> <br /> The ACYA Group team.</div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="margin: 0px 0px 10px; text-align: left; color: #202020; line-height: 100%; font-family: arial; font-size: 22px; font-weight: bold; display: block; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial,helvetica,sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…Geraint Schmidt<br /> Chapter President<br /> ACYA@ANU</span></span></span></h4>
<div style="text-align: left; color: #505050; line-height: 150%; font-family: Arial; font-size: 14px;">
<div style="text-align: left; color: #505050; line-height: 150%; font-family: Arial; font-size: 14px;"><br /> <strong><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/geraint1.jpg" border="0" width="250" height="259" align="left" style="width: 250px; height: 259px; line-height: 100%; text-decoration: none; display: inline; outline: none; margin: 5px 10px 5px 5px; border: 0;" />How did you get involved with Sino-Australian youth engagement / ACYA?</strong><br /> My passion for China and Sino-Australian youth engagement was born out of my travels in China in 2011. I unequivocally fell in love with the country, the language, the food, the people, the landscapes, the diversity, and I was inspired by my experiences to want to influence Australians, especially youth, to see the opportunities that engaging with China provides. After feeling frustrated by the lifestyle of a high school boarding house, China rekindled in me a desire to learn and explore, and if anyone became at least somewhat engaged with China after hearing me wax lyrical about it I would be overjoyed.<br /> <br /> <strong>What keeps you interested in China?</strong><br /> Very simply: the desire to go back, and this time with a greater understanding of the language, the people, the issues and the culture. I look forward to the day that I step into China again with all the time in the world to explore both places I visited and loved as well as places I missed out on, preferably on a bicycle.<br /> <br /> <strong>What exciting plans do you have up your sleeve for ACYA@ANU in 2013?</strong><br /> Fundamentally, the ACYA@ANU executive is focused on maintaining a high quality weekly language exchange and on implementing a potential mentoring system where domestic and Chinese-speaking students can form one-on-one relationships to practice the languages they are least confident with. We are planning a film night in the next couple of weeks, a Hot Pot/Mahjong night over the frigidly cold Canberra winter, and an inter-society ball with other Asia-related societies in Semester Two.<br /> <br /> <strong>What is your philosophy as ACYA@ANU President?</strong><br /> We at ACYA@ANU are trying to facilitate interest and engagement between the nationalities and cultures of the Asia-Pacific. We can offer a relatively peaceful space for international students to engage with each other away from the tensions that might exist in their home countries, and I think facilitating these sorts of engagements is what societies like ACYA should be all about. I’m therefore pushing this year for tighter ties between ACYA@ANU and the other societies around campus.</div>
</div>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">Australia-China Young Professionals Initiative</span></span></strong></span></h4>
<br /> <strong><a href="http://www.acypi.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYPI2.jpg" border="0" width="250" height="177" align="right" style="width: 250px; height: 177px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a>ACYPI NSW</strong> in conjunction with the <a href="http://www.acbc.com.au/" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">Australia-China Business Council</a> invites you to drinks and a discussion on the art of building successful working relationships between Australia and China – <strong>‘The Art of Guanxi: Doing Business in China’</strong>. The event will feature leading China experts <a href="http://www.rsadvisory.com/people/index.html" style="color: #336699; font-weight: normal; text-decoration: underline;">Kevin Hobgood-Brown</a>, Managing Director, Riverstone Advisory, and <a href="http://advance.org/mr-barry-plews/" style="color: #336699; font-weight: normal; text-decoration: underline;">Barry Plews</a>, Creative Producer and Dramaturge of Reckless Moments.<br /> <br /> <strong>Time:</strong> 6:30pm<br /> <strong>Date:</strong> Tuesday 19<sup>th</sup> March 2013<br /> <strong>Venue:</strong> Shanghai Room, Level 13, The Gateway, 1 Macquarie Place, Sydney<br /> <strong>RSVP by Friday 15 March to:</strong> <a href="http://acypi.eventbrite.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">http://acypi.eventbrite.com</a><br />  <br /> <strong>ACYPI Victoria</strong> and the <a href="http://www.aiia.asn.au/" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">Australian Institute of International Affairs</a> also invite you to a discussion with <a href="http://au.linkedin.com/pub/colin-heseltine/30/606/832" style="color: #336699; font-weight: normal; text-decoration: underline;">Colin Heseltine</a>. Colin's career with the Australian Department of Foreign Affairs and Trade spanned over 40 years. During this time, Colin worked extensively in the Asian region as head of the Asia Pacific Economic Cooperation (APEC) Secretariat and ambassador to the Republic of Korea.<br /> <br /> <strong>Time:</strong> 6:00pm<br /> <strong>Date:</strong> Wednesday 3<sup>rd</sup> April 2013<br /> <strong>Venue:</strong> Dyason House, 124 Jolimont Road, East Melbourne<br /> <strong>Please visit <a href="http://www.acypi.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">http://www.acypi.org.au/</a> where RSVP details and information regarding the new ACYPI Perth Chapter will be released shortly.</strong></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Engaging China Project</span></h4>
<a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ECPeeps_Perth.jpg" border="0" width="250" height="333" align="left" style="width: 250px; height: 333px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><a href="http://www.engagingchinaproject.org.au/" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">ECP</a> has started 2013 with a bang! All around the country we celebrated Chinese New Year with our local communities. From Perth to the Gold Coast we've been working hard to develop our networks with schools and universities and of course ACYA! We partnered with ACYA on several O-week stalls and our newest ECP Ambassadors are excited to get out into schools, tell their China story and inspire the next generation. Now that school has resumed, ECP will be delivering our Term 1 presentations this month. For more information or to join in the fun please email <a href="mailto:info@engagingchinaproject.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">info@engagingchinaproject.org.au</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; display: block; font-family: arial; font-weight: bold; margin: 0px 0px 10px; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large;">Get Involved in ACYA Publications! <a href="index.php/en/p2p/news-announcements/331-acya-publications-call-for-applications-2013" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAJournalLOGO.jpg" border="0" width="250" height="239" align="right" style="width: 250px; height: 239px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></span></h4>
<a href="index.php/en/publications" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA National Publications</a> is calling for applications for Sub-Editor positions on the 2013 <a href="index.php/en/publications/acya-journal" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA Journal of Australia-China Affairs</a> Sub-Committee and the <a href="index.php/en/publications/opportunities-guide" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA Opportunities Guide</a> Sub-Committee, as well as regular Contributors to our unique <a href="index.php/en/publications/australia-bites" style="color: #336699; font-weight: normal; text-decoration: underline;">AustraliaBites</a> and <a href="index.php/en/publications/china-bites" style="color: #336699; font-weight: normal; text-decoration: underline;">ChinaBites</a> current affairs and pop culture updates. For more details on how to apply, please visit the <a href="index.php/en/p2p/news-announcements/331-acya-publications-call-for-applications-2013" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>New Chapters - UTS & UniSA!</strong></span><br /> <br /> <a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo2.jpg" border="0" width="200" height="175" align="left" style="width: 200px; height: 175px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px 8px 3px 5px; border: 0;" /></a>This year a new ACYA Chapter will be starting up at <a href="http://www.uts.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">UTS</a>! Sydney University, UNSW and Macquarie already have ACYA chapters up and running and it’s about time that UTS joins in the fun! <br /> <br /> It’s well known that UTS has a huge Chinese student population that is growing every year, yet there is still no ACYA presence to take advantage of this. So to get things started we need members, members, members! Everyone is talking about how Australia is entering the Asian Century so what better time to join than now?<br /> <br /> Everyone is welcome, even if you don’t speak a word of Chinese. Join us at the new <a href="http://www.facebook.com/groups/147655622061721/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA@UTS Facebook group</a> - it may just be the best thing you do today.<br /> <br /> Please contact ACYA@UTS Chapter President <a href="mailto:president_uts@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">Matthew Warr</a> for more details or simply add him on <a href="http://www.facebook.com/warry86" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a>.<br /><hr />
<p><a href="http://www.facebook.com/groups/153400261484948/?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UNISA.jpg" border="0" width="150" height="212" align="left" style="width: 150px; height: 212px; margin: 5px 8px 5px 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a>The new ACYA@<a href="http://www.unisa.edu.au/" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">UniSA</a> chapter aims to introduce Chinese culture to Australian youth who are interested in China and vice versa. By delivering a Mandarin course, traditional festival party and other social events, we hope to encourage cross-cultural understanding and different cultural experiences at UniSA.<br /> <br /> We are going to have a welcome party working together with ACYA@UniAdelaide and the Chinese Culture Club UniSA in the coming weeks. Feel free to come and join us, you are very welcome!<br /> <br /> We will post up-to-date information on our new <a href="http://www.facebook.com/groups/153400261484948/" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook group</a> and <a href="http://unione.unilife.edu.au/Clubs/Club.aspx?CID=511" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a>.</p>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">'Work, Study and Engage with China' - Information Seminar & Networking Event</span></span></span></strong></h4>
<strong>18:00 - 20:00<a href="http://workstudyandengagewithchina.eventbrite.com.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ECP_Logo_Concepts_06_single.jpg" border="0" width="250" height="354" align="right" style="width: 250px; height: 354px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></strong><br /> <strong>Friday 22 March 2013</strong><br /> <strong><a href="http://www.griffith.edu.au/" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">Griffith University</a> Gold Coast, International Building (G52), Lecture Theatre 1.21</strong><br /> <strong>Free</strong><br />  <br /> Hosted by <a href="http://paulrackemann.com.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Paul Rackemann</a><strong>, </strong>this vibrant seminar and networking event will feature speakers including Engaging China Project’s own <a href="http://au.linkedin.com/pub/alexis-wagner/41/a39/9a1" style="color: #336699; font-weight: normal; text-decoration: underline;">Alexis Wagner</a> as well as <a href="http://au.linkedin.com/pub/susie-douglas/35/997/5a1" style="color: #336699; font-weight: normal; text-decoration: underline;">Susie Douglas</a>, Associate Professor Shaofang Liu of Griffith University and <a href="http://au.linkedin.com/in/luika" style="color: #336699; font-weight: normal; text-decoration: underline;">Luika Bankson</a>.<br />  <br /> RSVP today by clicking <a href="http://workstudyandengagewithchina.eventbrite.com.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">here</a>.<br /> <br /> This event has been made possible by ACYA Group initiatives <a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a> and <a href="http://www.acya.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA</a>, in addition to the kind support of the <a href="http://www.griffith.edu.au/tourism-confucius-institute" style="color: #336699; font-weight: normal; text-decoration: underline;">Griffith University Tourism Confucius Institute</a> and <a href="http://www.crccasia.com/" style="color: #336699; font-weight: normal; text-decoration: underline;">CRCC Asia</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">ACYA O-Weeks Kick Off across Australia & China</span></span></span></strong><br /> <br /> It's been a busy and exciting time for ACYA's chapters across Australia and China, with university O-Weeks heralding the start of the new semester. Below are some choice shots from ACYA@UniWA, ACYA@UQ, ACYA@USYD and ACYA@Nanjing!<br /> <br /> <strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_WA.JPG" border="0" width="300" height="225" align="none" style="width: 300px; height: 225px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UQ.JPG" border="0" width="300" height="200" align="none" style="height: 200px; line-height: 18px; width: 300px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_UQ2.JPG" border="0" width="300" height="168" align="none" style="width: 300px; height: 168px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" /></a> <a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAUSYD1.jpg" border="0" width="300" height="225" align="none" style="width: 300px; height: 225px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></span></span></span></strong><strong style="color: #202020; font-family: Arial; font-size: 22px; line-height: 22px;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_NANJING.JPG" border="0" width="250" height="376" align="none" style="height: 376px; line-height: 18px; width: 250px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" /></a> </span></span></span></strong><br /> </div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-weight: bold; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ANU Language & Thinking Survey - Seeking Participants</span></h4>
If you are Chinese, have obtained Australian citizenship or residency and are fluent in both Mandarin and English, please consider taking part in the following ANU language and thinking survey by <a href="https://anupsych.us.qualtrics.com/SE/?SID=SV_7OgepHX5ViHV0HP" style="color: #336699; font-weight: normal; text-decoration: underline;">clicking here</a>. You’ll receive a detailed report about yourself for your efforts! Contact <a href="mailto:corie.lin@anu.edu.au" style="color: #336699; font-weight: normal; text-decoration: underline;">Corie Lin</a> for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="margin: 0px 0px 10px; text-align: left; color: #202020; line-height: 100%; font-family: arial; font-size: 22px; font-weight: bold; display: block; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Intership Opportunity - Wiseman</span></span></span></h4>
<strong>Great opportunity to gain valuable work experience in the education community</strong><br />  <br /> Wiseman Summer Internship Programme (WSIP) is looking for passionate interns to work with an outstanding international team on e-learning in Hong Kong. Don’t miss out this great overseas English educational opportunity. For more information, visit <a href="http://www.wiseman.com.hk/site/about/interns/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">http://www.wiseman.com.hk/site/about/interns/</a>.<br /> <br /> To apply or seek more information, please contact <a href="mailto:clement.chung@wiseman.com.hk" style="color: #336699; font-weight: normal; text-decoration: underline;">Clement Chung</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">International Marketing Executive Wanted - Extension & Lettuce</span></h4>
If you are a young professional looking for a challenge, write business level Mandarin and English and are a qualified, passionate marketer, <a href="http://lettuce.com.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Extension & Lettuce</a> would like to hear from you.<br />  <br /> Based in Extension & Lettuce’s funky South Melbourne office, the International Marketing Executive will be championing their most ambitious online project to date as well as assisting their clients devise and implement international marketing strategies.<br />  <br /> Criteria:<br />  <br /> • 2 years experience<br /> • Asian marketing knowledge<br /> • Strong multilingual communicator (Mandarin is a must)<br /> • Australian & international online marketing experience<br /> • Interest in the property industry<br />  <br /> Applications should be sent to Jon Ellis at <a href="mailto:jon@extensionco.com" style="color: #336699; font-weight: normal; text-decoration: underline;">jon@extensionco.com</a> or 200 Wells Street South Melbourne VIC 3205.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-family: Arial; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large; line-height: 34px; -webkit-text-size-adjust: none;">Connect with ACYA Group</span></h4>
Twitter:<a href="https://twitter.com/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" border="0" width="75" height="73" align="right" style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="https://twitter.com/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">@a_cya</a><br /> <a href="http://twitter.com/EngagingChina" style="color: #336699; font-weight: normal; text-decoration: underline;">@EngagingChina</a><br /> <a href="https://twitter.com/ACYDialogue" style="color: #336699; font-weight: normal; text-decoration: underline;">@ACYDialogue</a> <br /> <br /> Facebook:<a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" border="0" width="75" height="75" align="right" style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA National</a><br /> <a href="http://www.facebook.com/EngagingChinaProject?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a><br /> <a href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYD</a></div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">follow on Twitter</a> | <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">friend on Facebook</a> | <a href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&id=dc00bfe8c9&e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">forward to a friend</a> </div>
</td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://transasialawyers.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" border="0" alt="TransAsia Lawyers" width="250" height="122" align="none" style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><a href="http://chinainstitute.anu.edu.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" border="0" width="250" height="85" align="none" style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.crccasia.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" border="0" width="250" height="175" align="none" style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"> </div>
</div>
</td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <span style="color: #2f4f4f;">Header photograph - Monks on solemn parade, taken by <strong>Nicola Boyle.</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <br /> <span style="color: #2f4f4f;"><em>Copyright © 2013 ACYA, All rights reserved.</em></span><br />  </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><br /> <br /> <br /> <br /> <br /> <span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&utm_medium=email&utm_campaign=monkey_rewards&aid=db2f0ea3a68f632de3088eada&afl=1"><img src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" alt="Email Marketing Powered by MailChimp" title="MailChimp Email Marketing" /></a> </span></span></span></div>
<br />    </div>
</td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe&c=dc00bfe8c9" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> </div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  --><!--:-->