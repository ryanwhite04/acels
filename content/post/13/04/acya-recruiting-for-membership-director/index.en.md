{
  "title": "ACYA recruiting for Membership Director",
  "author": "ACYA",
  "date": "2013-04-09T13:56:33+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>ACYA is seeking to build on recent membership growth and hit the 10,000 members mark by the end of 2013. As such, a new volunteer position is available on the ACYA National Exec: ACYA Membership Director.</p>
<p>The role would involve:</p>
<ul>
<li>Designing and implementing membership growth strategies to hit the 10,000 member target;</li>
<li>Maintaining a highly active presence on ACYA's social media platforms, i.e.Facebook, Twitter and Weibo; and</li>
<li>Working with ACYA's Communications, P2P, and Publications teams in managing ACYA's membership database.</li>
</ul>
<p>Expected time commitment:</p>
<ul>
<li>4-5 hours a week;</li>
<li>3-weekly 1 hour National Exec Meeting attendance. </li>
</ul>
<p>To apply, please send your resume (max. 2 pages) and a brief covering letter (max. 1 page) touching on why you would make a perfect ACYA Membership Director to Alan McGuinness <a href="mailto:atcommunications@acya.org.au" target="_blank">atcommunications@acya.org.au</a>by 5pm Monday, 15 April 2013.</p>