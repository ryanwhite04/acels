{
  "title": "Julia Gillard China Visit",
  "author": "ACYA",
  "date": "2013-04-16T16:26:13+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p><img src="images/IMG_5884.JPG" width="480" height="359" alt="IMG 5884" style="vertical-align: middle;" />
</p>
<p></p>ACYA warmly welcomes Julia Gillard's announcement of an annual leaders summit between the Australian Prime Minister and Chinese Premier.</p>
<p>ACYA China President Victoria Kung represented ACYA at the AustCham-ACBC-MOFCOM luncheon in Beijing and eight ACYA delegates attended the Dinner to Celebrate 40 Years of Achievement in Australia-China Education, Science, and Research in celebration of the announcement. “All efforts aimed at adding deep ballast to the Australia-China bilateral relationship are fundamentally important to both Australia's and China's future. The announcement of the annual leaders summit is most heartening!” Victoria said.</p>
<p>ACYA is honoured to have taken part in these events and believes that the annual bilateral leaders’ summit is a crucial, if long overdue, step towards a move comprehensive Australia-China engagement.</p>
</p>
<p><span style="line-height: 1.3em;"><a href="/images/Documents/ACYA%20Media%20release.pdf">Full media release</a>.</span>
</p>
<p><span style="line-height: 1.3em;"><img src="images/rsz_img_5850.jpg" width="480" height="640" alt="rsz img 5850" style="vertical-align: middle;" /></span>
</p>