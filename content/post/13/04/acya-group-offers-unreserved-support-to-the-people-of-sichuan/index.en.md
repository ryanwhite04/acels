{
  "title": "ACYA Group offers unreserved support to the people of Sichuan",
  "author": "ACYA",
  "date": "2013-04-24T12:18:39+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Sichuan",
      "type": "image",
      "src": "map1.jpg"
    }
  ]
}
<!--:en-->The Australia-China Youth Association Group (ACYA Group) would like to offer its sincere condolences and unreserved support to the people of Sichuan Province who have suffered unimaginable losses in the wake of the recent earthquake of Saturday, April 20.

It is devastatingly cruel that the people of Sichuan should have to endure yet another natural calamity so soon after the horrific events of 2008.

Our thoughts go out to the brave residents and rescue teams who continue to selflessly put themselves at risk in order to save others in Longmen, Lushan, Ya’an and other badly affected areas. We wish them all the best in these extremely trying times.

<em>The ACYA Group consists of the Australia-China Youth Association, Australia-China Youth Dialogue, Australia-China Young Professionals Initiative and the Engaging China Project </em><!--:--><!--:zh--><h3><span style="font-size: 13px;">澳中青年联合组织(ACYA Group) 向在2013年4月20日四川省芦山地震中遇难的群众和抢险救灾中牺牲的战士表示深切哀悼。ACYA组织将全力支持和帮助受灾群众战胜自然灾害，重建美好家园。</span></h3>
四川省在经历了2008年汶川地震后，又再次遭受此次芦山地震，ACYA组织在此对龙门、芦山、雅安及其他受灾地区中勇敢的群众和救援人员表示真诚的同情、慰问和支持。在这最艰难的时刻中，我们祝福他们平安、顺利。

<i>澳中青年联合组织成员包括：澳中青年联合会(Australia-China Youth Association)、澳中青年论坛(Australia-China Youth Dialogue)、中澳青年精英领袖团（Australia-China Young Professionals Initiative)、 关注中国计划(Engaging China Project)</i><!--:-->