{
  "title": "E-Bulletin July 2012",
  "author": "ACYA",
  "date": "2013-04-01T16:55:55+10:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: left;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/image_13418287418791341828747.jpg" border="0" alt="" width="600" height="203" style="height: 203px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; border: 0; line-height: 100%; outline: none; text-decoration: none;" /></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h1 class="h1" style="color: #202020; display: block; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="font-size: 18px;"><span style="color: #ff0000;"><strong><span style="background-color: #ffffff; color: #ff0000; font-size: 18px; -webkit-text-size-adjust: none;">5 minutes with...</span></strong></span></span></h1>
<strong><span style="color: #ff0000;">James Hudson<img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/1._James_Hudson_photo.jpg" border="0" alt="Jacob Taylor" width="109" height="156" align="right" style="width: 109px; height: 156px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /><br /> East Asia Adviser,</span></strong><br /> <strong><span style="color: #ff0000;">CSIRO</span></strong><br /> <br /> <strong>Where did you grow up?</strong><br /> From the age of five I grew up in Lismore, a regional city on the far north coast of New South Wales. I also spent just a year in Ottawa, Canada when I was 16 when my parents were on a teacher's exchange program.<br /> <br /> <strong>What is your day job?</strong><br /> I work as the East Asia Adviser at the Commonwealth Scientific and Industrial Research Organisation (CSIRO), Australia’s national science research agency. <br /> <br /> <strong>How did you develop an interest in China?</strong><br /> When I was in my early teens my parents ran English as a Second Language (ESL) programs for pre-service teachers from Hong Kong and Japan here in Australia. We had one student from Hong Kong, Glenda, who stayed with us for about six weeks as my parents were unable to find a host family for her. I had a fascination with the tiny characters she used to write on postcards to send back to her family and, with an immense amount of patience on her part, she taught me a few. There is not a lot about China that doesn't interest me: the food, the music, the people, the language but, perhaps most of all, it's really the diversity that keeps me so fascinated in China. You could spend a lifetime exploring the deepest corners of China and still not even begin to understand the depth, complexity and diversity of its history, culture and people. <br /> <br /> <strong>How do you stay engaged in Australia-China relations?</strong><br /> In my role at CSIRO, I oversee the organisation's scientific engagement with the east Asian region.  This involves maintaining oversight of a large amount of interaction between CSIRO and Asia, hosting senior officials, participating in Government negotiations, identifying Asian partners to aid CSIRO's commercial activities, and keeping up to date with key policy developments in neighbouring countries and how they may impact our collaborations and interactions with Asia. I travel to Asia, mostly China, Korea and Japan, to meet with partners or accompany senior CSIRO staff two or three times a year.<br /> <br /> <strong>What advice would you give to someone just starting in ACYA?</strong><br /> I think ACYA is a brilliant organisation. I don't think people realise the power of organisations run by younger people. So, my first piece of advice is get involved with organisations that are lead by young people. My second piece of advice is to focus on building your network within the Australian community engaged with China. It is a surprisingly small community and getting to know the 'who's who in the zoo' sooner rather than later will be of benefit later on. Finally, I would tell younger people just starting off at ACYA to take the initiative to explore new horizons. There are many opportunities emerging from Australia’s bilateral relationship with China, so take the initiative to follow your passions and discover new ways to take this engagement to the next level.</div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="background-color: #ffffff; color: #ff0000; font-size: 18px; -webkit-text-size-adjust: none;">Andrea Myles and Henry Makeham feature in Asialink Next Generation series</span></h4>
<p style="line-height: 21px; background-color: #ffffff; font-family: arial; color: #505050; font-size: 14px; -webkit-text-size-adjust: none;">Engaging China Project's National Director <strong>Andrea Myles</strong> and ACYD founder <strong>Henry Makeham</strong> have featured as presenters at Asialink's Next Generation series on the Fifth Generation of Chinese leadership. Andrea participated in the <a href="http://cms.unimelb.edu.au/asialink/calendar/events/next_generation_series_-_the_fifth_generation_chinas_next_leaders_sydney" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Sydney event</a> along with Fairfax journalist John Garnaut, reporter Christian Edwards and Chinese Scholar John Lee. Henry was a panel member at the <a href="http://cms.unimelb.edu.au/asialink/calendar/events/next_generation_series_-_the_fifth_generation_chinas_next_leaders_melbourne" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Melbourne event</a> which included media commentator Jeremy Goldkorn and Age correspondent Peter Cai.<br /> .</p>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><span style="color: #ff0000;">Engaging China Project and ACYD successful for ACC Grants!</span></span></h4>
The <strong>Engaging China Project</strong> (ECP) and <strong>Australian China Youth Dialogue</strong> (ACYD) were successful in their applications for grants from the Australian China Council. The ECP is to receive $12,500 to develop its China engagement intitiative in Australia schools, while the ACYD will receive $25,000 for their annual bilateral dialogue. Well done to all involved! </div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><strong><span style="color: #ff0000; font-size: large;">Hamer Scholarship</span></strong><br /> <br /> The Victorian Government Department of Business and Innovation is providing 50 scholarships for Victorian students studying Chinese lanaguge in China. Applications for these scholarships open July 23 and close August 23. The scholarships are for study commencing February 2013 in China. For more information please view the <a href="http://www.dbi.vic.gov.au/projects-and-inititatives/the-hamer-scholarships" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Hamer Scholarship</a> webpage or contact the Hamer Scholarship Management Team at <a href="mailto:hamerscholarships@dbi.vic.gov.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">hamerscholarships@dbi.vic.gov.au</a> or +61 3 9651 8018.<br />  </div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><span style="color: #ff0000;">ACYA Journal of Australia-China Affairs - Call for Submissions</span></span></h4>
<span><a href="http://acya.org.au/PDF/Call%20for%20Submissions%20-%20ACYA%20Journal.pdf" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYA_logo.png" border="0" alt="ACYA Journal" width="139" height="127" align="right" style="width: 139px; height: 127px; margin-bottom: 3px; margin-left: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></span>This year the ACYA Journal will be <strong>bilingual</strong>, <strong>peer-reviewed</strong>, and offering <strong>cash prizes</strong> of up to $500! We are now calling for submissions of academic essays, opinion pieces, and creative work in either <strong><a href="http://acya.org.au/PDF/Call%20for%20Submissions%20-%20ACYA%20Journal.pdf" style="color: #336699; font-weight: normal; text-decoration: underline;">English</a></strong><strong> or <a href="http://acya.org.au/PDF/Call%20for%20Submissions%20-%20ACYA%20Journal.pdf" style="color: #336699; font-weight: normal; text-decoration: underline;">Chinese</a></strong>. Entries close 31 July and the Journal will be launched at the 2012 ACYD in October.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><span style="color: #ff0000;">ACYA Online</span></span></h4>
ACYA’s online presence is continuing to grow. In addition to our <a href="http://www.acya.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">new website</a>, which we are continuing to upgrade, and our national <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook page</a>, ACYA now has a team of Australian and Chinese students posting to our official <a href="http://weibo.com/acya/profile?leftnav=1&amp;wvr=3.6" style="color: #336699; font-weight: normal; text-decoration: underline;">Weibo</a> account and a renewed presence on <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a>. We have also launched <a href="http://www.facebook.com/groups/415660751790528/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA@Beijing</a>, where you can keep up to date with the latest happenings in the Chinese capital. </div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px; color: #ff0000; line-height: 100%;">ACYD applications are now open!</span></h4>
Applications for the third Australia-China Youth Dialogue are now open! The 2012 ACYD, in partnership with the Chinese University Media Union and All China Youth Federation, will bring 30 future leaders from Australia and China to <strong>Beijing</strong> and <strong>Chengdu</strong> from 19-24 October to discuss, debate and analyse the issues that will shape Sino-Australian relations in coming decades. In this year, the 40th anniversary of diplomatic relations between Australia and China, the theme of the ACYD is: <em>At 40 there are no doubts: next steps in Australia-China relations</em>. <br /> <br /> Details on how to apply are available on the <a href="http:// www.acyd.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYD</a> website. To attend the ACYD you must be Australian or Chinese and between the ages of 18 and 35 on 24 October, 2012. <br /> <br /> <strong>Qantas confirmed as a major partner for the 2012 ACYD</strong><br /> Qantas has confirmed it will continue to be a major partner of the ACYD. Qantas has supported the ACYD each year since the inaugural Dialogue in 2010. Delegates flying from Australia should watch out for the feature article on the ACYD in Qantas' inflight magazine.<br /> <br /> <strong>Australia-China Council (DFAT) confirmed as a major partner for the 2012 ACYD</strong><br /> The ACC has confirmed generous financial support for the 2012 ACYD. The ACC has supported the ACYD each year since the inaugural Dialogue in 2010. We are also very pleased to announce that two members of the ACC board: Chris Clark and Kevin Hobgood-brown, will be addressing the Dialogue in Beijing. <br /> <br /> <strong>ACYD on Twitter</strong><br /> Follow us on <a href="http:// http//www.twitter.com/ACYDialogue" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a> for regular updates, <br /> </div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<div style="text-align: left; line-height: 150%; font-family: arial; color: #505050; font-size: 14px;">
<h1 class="null" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 34px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;"><span style="font-size: 18px;">Engaging China Project</span></span></h1>
</div>
The <a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a> (ECP) is excited to announce the appointment of <strong>Caryn Tan</strong> as the new ECP State Manager for Western Australia. ECP's NSW chapter has completed its first school engagements featuring four ambassadors presenting to over 200 students. The ECP which is less than one year old now has state chapters in NSW, Victoria, ACT, WA and QLD!<br /> <br /> Visit the <a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">ECP website</a> or contact the National Communications Manager <a href="mailto:sebastian.jurd@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">sebastian.jurd@acya.org.au</a> for more information. Follow ECP on <a href="http://www.facebook.com/EngagingChinaProject" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a> and <a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Fwww.engagingchinaproject.org.au%2F&amp;region=follow_link&amp;screen_name=engagingchina&amp;source=followbutton&amp;variant=2.0" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; display: block; font-family: arial; font-size: 22px; font-weight: bold; margin: 0px 0px 10px; color: #202020; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;">Chinese International Student Liaisons</span></h4>
ACYA is seeking Chinese International Student Liaisons in cities across Australia to assist newly arrived Chinese students adjust to life in Australia. We also hope to develop a list of Frequently Asked Questions (FAQs) for Chinese students coming to Australia. Please contact <a href="mailto:sam.wall@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">sam.wall@acya.org.au</a>, in English or Chinese, for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000;">Internships</span></h4>
The East Asia Program at the <a href="http://www.lowyinstitute.org/" style="color: #336699; font-weight: normal; text-decoration: underline;">Lowy Institute for International Policy</a> seeks interns with a keen interest in international relations, and China’s foreign and security policy in particular. Interns will produce English language summaries Chinese-language foreign policy articles,must be based in Sydney and be prepared to work a minimum of 3 days per week.<br /> <br /> Please send a CV, a writing sample in English, and a brief explanation describing your motivation for applying to Ed Kus at <a href="mailto:ekus@lowyinstitute.org" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ekus@lowyinstitute.org</a><br /><hr /><br /> <a href="http://transasialawyers.com/" style="color: #336699; font-weight: normal; text-decoration: underline;">TransAsia Lawyers</a> offers three month internships in Beijing or Shanghai exclusively for ACYA members on a rolling basis. Applications are now open for internships beginning in September. Interns will be working in one of the leading law firms licensed to practice in the PRC and get direct work experience in working in a firm that operates in Chinese and international legal systems.<br /> <br /> Visit the <a href="index.php/en/careers/internships/241-acya-tranasia-laywers-internships" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> or contact <a href="mailto:david.davies@acya.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">david.davies@acya.org.au</a>.<br /><hr /><br /> <a href="http://policycn.com" style="color: #336699; font-weight: normal; text-decoration: underline;"><strong>China Policy</strong></a> provides expert analysis on contemporary China, seeking to build and sustain China knowledge through its innovative ChinaBase information systems. China Policy welcomes interns with advanced research, Chinese language or IT skills for two to six months on a part-time of full-time basis.<br /> <br /> Visit the <a href="index.php/en/careers/internships/164-china-policy" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> or contact <a href="mailto:amanda.rasmussen@policycn.com" style="color: #336699; font-weight: normal; text-decoration: underline;">amanda.rasmussen@policycn.com</a> for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000;">Taiwan Fellowship</span></h4>
The Ministry of Foreign Affairs (MOFA) Taiwan is providing fellowships foreign experts and scholars wishing to conduct advanced research on Taiwan, cross-strait relations, mainland China, Asia –Pacific region and Chinese studies at universities or academic institutions in Taiwan.<br /> <br /> Assistance includes:<br /> - Monthly stipend of NT$ 60,000 (professors, associate professors, research fellows and associate research fellows); or NT$ 50,000 (assistant professors, assistant research fellows, post-doctoral researchers, doctoral candidates, doctoral students and other recommended candidates)<br /> - Accident and medical insurance<br /> - Economy-class return flight<br /> Applications close July 15 and are to be posted to the Taipei Representative Office in the U.K. at 50 Grosvenor Gardens, London SW1W 0EB.<br /> <br /> More information can be found on the <a href="http://www.roc-taiwan.org/uk/ct.asp?xItem=273441&amp;ctNode=932&amp;mp=132" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Taiwan Fellowship</a> webpage, further questions can be directed to Olivia Saunders eduuk3@btconnect.com.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">follow on Twitter</a> | <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">friend on Facebook</a> | <a href="http://us1.forward-to-friend1.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=3356f7abf8&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">forward to a friend</a> </div>
</td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://transasialawyers.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" border="0" alt="TransAsia Lawyers" width="250" height="122" style="width: 250px; height: 122px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://chinainstitute.anu.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/img_sponsor_anu.jpg" border="0" alt="ANU China Institute" width="250" height="64" style="width: 250px; height: 64px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a></div>
</div>
</td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <span style="color: #2f4f4f;">Header photograph by <a href="http://www.facebook.com/groups/uqacya/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA@</a><a href="http://www.facebook.com/groups/uqacya/" style="color: #336699; font-weight: normal; text-decoration: underline;">UQ</a></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <br /> <span style="color: #2f4f4f;"><em>Copyright © 2012 ACYA, All rights reserved.</em></span><br />  </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><br /> <br /> <br /> <br /> <br /> <span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" alt="Email Marketing Powered by MailChimp" title="MailChimp Email Marketing" /></a> </span></span></span></div>
<br />    </div>
</td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=3356f7abf8" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> </div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  -->