{
  "title": "E-Bulletin October 2012",
  "author": "ACYA",
  "date": "2013-04-01T16:59:51+10:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!-- // Begin Template Header  -->
<table id="templateHeader" style="background-color: #000000; border-bottom: 0; padding: 0px;" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle;"><!-- // Begin Module: Standard Header Image  -->
<div style="text-align: center;"><a href="http://www.acya.org.au/" style="color: #336699; text-decoration: underline; font-weight: normal;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/image_13489896059421348989618.jpg" border="0" alt="ACYA E-Bulletin August 2012" width="600" height="233" style="height: 233px; width: 600px; margin: 0; padding: 0; display: block; margin-top: 0; margin-bottom: 0; margin-left: auto; margin-right: auto; border: 0; line-height: 100%; outline: none; text-decoration: none;" /></a></div>
<!-- // End Module: Standard Header Image  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Header  --><!-- // Begin Template Body  -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h1 class="h1" style="color: #202020; display: block; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="font-size: 18px;"><span style="color: #ff0000;">Australia-China Youth Dialogue</span></span></h1>
<a href="http://acyd.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/acyd_2010dialogue_web.jpg" border="0" width="224" height="150" align="left" style="width: 224px; height: 150px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a>On the eve of the release of the Australian Government’s <a href="http://asiancentury.dpmc.gov.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">White Paper on Australia in the Asian Century</a>, and with the 18th National People’s Congress only <a href="http://news.xinhuanet.com/english/china/2012-09/28/c_131880076.htm" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">weeks away</a>, 30 of China and Australia’s best and brightest future leaders will convene in Beijing and Chengdu for the 3<sup>rd</sup> Australia China Youth Dialogue (ACYD).<br />  <br /> The ACYD team has been humbled by the caliber of applicants, who reflect the growing depth and diversity of bilateral engagement between Australia and China. We would like to thank all applicants for supporting the ACYD. Australian delegates have now been finalised and will be announced shortly on the ACYD website <a href="http://www.acyd.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">www.acyd.org.au</a>, with Chinese delegates to be announced shortly. <br />  <br /> Former PM, the Hon. <a href="http://www.alp.org.au/federal-government/labor-people/kevin-rudd/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Kevin Rudd MP</a> and the Australian Ambassador to China, HE Ms <a href="http://www.dfat.gov.au/homs/cn.html" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Frances Adamson</a> are now confirmed to join the <a href="http://acyd.org.au/2012-acyd-speaker-list/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">distinguished speakers</a> at ACYD 2012.<br />  <br /> Follow us on <a href="http://www.twitter.com/ACYDialogue" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a> for updates during the event <strong>19-24 October</strong>.</div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content  --> <!-- // Begin Module: Left Image with Content  -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-family: arial,helvetica,sans-serif;"><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…</span></span></span></h4>
<span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Dominic Meagher</strong></span></span><br /> <strong style="font-family: arial, helvetica, sans-serif; color: #ff0000;">Director, Australia-China Youth Dialogue</strong><br />
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><br /> <strong>What sparked your interest in China and Australia-China relations? </strong><a href="http://au.linkedin.com/pub/dominic-meagher/5/405/963" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Dominic_Headshot_Final.png" border="0" width="150" height="199" align="right" style="height: 199px; line-height: 16px; width: 150px; display: inline; margin: 5px; border: 0; outline: none; text-decoration: none;" /></a>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"> </div>
Interning at the Asia-Australia Institute in 2001 was definitely one of the early sparks, but the biggest thing was graduating with honors in international relations but without international experience. China appealed to me because it is such a fascinating and diverse country. Once you start paying attention it's impossible not to be drawn in. <br /> <br /> <strong>How did you become involved with ACYD?</strong><br /> <br /> I met <a href="http://www.linkedin.com/pub/henry-f-makeham-%E5%AD%9F%E7%A7%89%E8%9E%8D/b/966/a74" style="color: #336699; font-weight: normal; text-decoration: underline;">Henry Makeham</a> in a cafe in <a href="http://en.wikipedia.org/wiki/Wudaokou" style="color: #336699; font-weight: normal; text-decoration: underline;">Wudaokou</a> in 2008. I was wearing my <a href="http://www.anu.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">ANU</a> hoodie so he bustled me up and we became good mates. I was project manager of the <a href="http://www.crawford.anu.edu.au/research_units/china/" style="color: #336699; font-weight: normal; text-decoration: underline;">China Economy Program</a> at ANU so when Henry told me about <a href="http://acyd.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYD</a> in 2010 I agreed to consult on the agenda. The next year he convinced me to apply as a delegate and it was one of the best things I’ve ever done. I still keep in touch with many of the delegates and they were some of the first people I contacted when I returned to Beijing this year. <br /> <br /> <strong>What are you most looking forward to about ACYD 2012? </strong><br /> <br /> Definitely meeting all the delegates. We had so many great applications, it was really hard to choose just 15 from each country, but the group we've got is going to be fantastic. It's humbling to see all the incredible things people have done.</div>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">ACYPI Moves West!</span></span></strong></span></h4>
<a href="http://www.acypi.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYpi_logo.png" border="0" width="102" height="170" align="left" style="width: 102px; height: 170px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" /></a>The <a href="http://www.acypi.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Australia-China Young Professionals Initiative</a> (ACYPI) is proud to announce its upcoming chapter in <a href="http://en.wikipedia.org/wiki/Perth" style="color: #336699; font-weight: normal; text-decoration: underline;">Perth</a>, Western Australia!  ACYPI Perth will serve as a platform to engage with Sino-Australian issues that impact <a href="http://en.wikipedia.org/wiki/Western_Australia" style="color: #336699; font-weight: normal; text-decoration: underline;">Western Australia</a>, and we are seeking young professionals in Perth to help establish the chapter. Send us an email at <a href="mailto:perth@acypi.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">perth@acypi.org.au</a> or visit our website for more information and to sign up to our mailing list: <a href="http://www.acypi.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">www.acypi.org.au</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">China's Leadership Handover 2012: The Verdict </span></h4>
<br />
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><a href="http://www.kerry-brown.co.uk/" style="font-family: Arial; font-size: 14px; line-height: 21px; color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/drkerrybrown1.jpg" border="0" width="96" height="150" align="right" style="height: 150px; line-height: 16px; width: 96px; display: inline; margin-right: 5px; margin-left: 5px; border: 0; outline: none; text-decoration: none;" /></a></div>
<a href="http://www.facebook.com/groups/145918808810437/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYPI Sydney</a> is hosting "<strong>China's Leadership Handover 2012 - The Verdict</strong>", featuring <a href="http://www.kerry-brown.co.uk/" style="color: #336699; font-weight: normal; text-decoration: underline;">Dr Kerry Brown</a>, newly appointed Executive Director of the <a href="http://sydney.edu.au/china_studies_centre/" style="color: #336699; font-weight: normal; text-decoration: underline;">China Studies Centre</a> at the <a href="http://sydney.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">University  </a>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"> </div>
<a href="http://sydney.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">of Sydney</a>. The event will be held at <strong>6pm </strong>on<strong> 22 November</strong>, at the offices of the <a href="http://www.acbc.com.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Australia China Business Council</a>. Please sign up to the ACYPI mailing list on our website to be informed when and where you can register: <a href="http://www.acypi.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">www.acypi.org.au</a>.</div>
</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">“Home”, Sydney an International City</span></span></strong></h4>
<a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA</a> and the <a href="http://www.cityofsydney.nsw.gov.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">City of Sydney</a> are producing <em><strong>"Home", Sydney an International City</strong></em>, bringing together international and local students and the broader community in a series of fun, youth-lead events to raise awareness of cultural similarities. We are fortunate to have a team of truly committed and experienced individuals on board for a project that brings together <a href="http://www.uts.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">UTS</a>, <a href="http://www.unsw.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">UNSW</a> and <a href="http://sydney.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">USYD</a>!<br /> <br /> ·  <strong>Jamie Yu</strong> - Executive Project Manager<br /> ·  <strong>Greg Mikkelsen -</strong> Events and Logistics Manager<br /> ·  <strong>Nini Zeng</strong> - Communications &amp; Marketing Manager<br /> ·  <strong>Aimee Yi</strong> - Community Outreach Manager<br /> ·  <strong>Bowany Pugh</strong> - Volunteer Manager<br /> ·  <strong>Justin Zou -</strong> Sponsorship Manager<br /> ·  <strong>Lulu Shen</strong> from <strong>City of Sydney</strong><br /> ·  <strong>Thomson Ch’ng</strong> from <strong><a href="http://cisa.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Council of International Students Australia</a> (CISA)</strong><br /> <br /> Contact <a href="mailto:bowanyp@yahoo.com.au" style="color: #336699; font-weight: normal; text-decoration: underline;">bowanyp@yahoo.com.au</a> to get involved!</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Engaging China Project</span></span></span></strong><br /> <br /> <span style="font-family: arial,helvetica,sans-serif;"><a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ECP_Logo_Concepts_06_single.1.jpg" border="0" width="138" height="200" align="left" style="width: 138px; height: 200px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 0px 5px 5px; border: 0;" /></a></span>Whether you are gaining leadership experience, work experience, networks or friends, active participation in the <a href="http://acya.org.au/index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA</a> network will help you build your skills to become a leader of the future. Together we are building the <a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Engaging China Project</a> to reach out to thousands of high school students, developing networks within the <a href="http://www.acypi.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYPI</a>, designing events in ACYA and creating a leading Australia-China dialogue.<br /> <br /> The ECP is <strong>seeking a National Deputy Director</strong> as well as <strong>enthusiastic ambassadors</strong> to continue our growth and development. Please visit our website <a href="http://www.engagingchinaproject.org.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">www.engagingchinaproject.org.au</a> and follow us on <a href="http://www.facebook.com/EngagingChinaProject?fref=ts" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a> and <a href="https://twitter.com/EngagingChina" style="color: #336699; font-weight: normal; text-decoration: underline;">Twitter</a> or contact National Communications Manager <strong>Sebastian Jurd</strong> at <a href="mailto:sebastian.jurd@acya.org.au" style="color: #336699; font-weight: normal; text-decoration: underline;">sebastian.jurd@acya.org.au</a> for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">CRCC Asia Summer Internship Program</span></span></span></strong></h4>
CRCC Asia is a proud sponsor of ACYA and offers internship opportunities across a wide range of industries in both Shanghai and Beijing. We provide you with a guaranteed internship in an industry of your choice as well as accommodation, weekly Mandarin lessons, business visa support and much more.<br /> <br /> Visit our <a href="http://www.crccasia.com/internships/china-intern-program/" style="color: #336699; font-weight: normal; text-decoration: underline;">China Internship Website</a> for more information OR watch a <a href="http://www.youtube.com/watch?v=OCQSlmK9jnU&amp;feature=player_detailpage" style="color: #336699; font-weight: normal; text-decoration: underline;">video</a> of our July 2012 Interns and see what their experience was like!<br /> <a href="http://crccasia.com/apply" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">Apply here</a> by <strong>October 18th</strong> for an internship start date of <strong>November 29<sup>th</sup></strong>.<br /> <br /> <strong><a href="http://au.linkedin.com/in/jakekelder" style="color: #336699; font-weight: normal; text-decoration: underline;">Jake Kelder</a> is a 3rd year Finance and Law student and President of ACYA at the <a href="http://www.qut.edu.au/" style="color: #336699; font-weight: normal; text-decoration: underline;">Queensland University of Technology</a>. He interned with CRCC in 2011:</strong><br /> <br /> “My goal as a university student is to gain worthwhile and commercially valuable experience in the finance industry before I graduate. In this way, the placement that I received through CRCC Asia laid great foundations for me. As it turns out, after my placement in Shanghai I was able to secure an internship with one of the ‘Big 4’ accounting firms. It has also opened my eyes to the opportunities that exist in China and has increased my value to employers.”<br /> <br /> See the full discussion on our <a href="http://acya.org.au/index.php/en/p2p/news-announcements/292-crcc-asia-summer-internship-programs" style="color: #336699; font-weight: normal; text-decoration: underline;">website</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-weight: bold; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ACYA Chapter Video Competition</span></h4>
<a href="index.php/en/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACYAlogo_1_.gif" border="0" width="140" height="127" align="right" style="width: 140px; height: 127px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin-right: 5px; margin-bottom: 5px; margin-left: 7px; border: 0;" /></a>Do you have a camera, a smartphone and some spare time this weekend? Then enter <strong>ACYA’s Chapter Video Competition</strong>!<br />  <br /> Make a short video highlighting the best parts of your university, local ACYA chapter, campus, city and country. Upload your entry to <a href="http://www.youtube.com/" style="color: #336699; font-weight: normal; text-decoration: underline;">YouTube</a> and send a link through to <a href="mailto:daniel.paterson@acya.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">daniel.paterson@acya.org.au</a> along with your name and the chapter you belong to. <br />  <br /> The winning entry will gain a <strong>$300 prize bounty</strong> for a chapter event of their choice. Get creative to make sure your chapter wins! All entries due in by <strong>November 1</strong>, winners announced <strong>November 2</strong>.<br />  <br /> For further details see <a href="http://acya.org.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">acya.org.au</a> or follow us on <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">Facebook</a>.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Contribute to ACYA Publications!</span></span></span></strong></h4>
Opportunities exist on an ongoing basis with <a href="http://acya.org.au/index.php/en/publications" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA Publications</a> to become a contributor or editor for our <a href="index.php/en/publications/australia-bites" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">AustraliaBites</a> and <a href="index.php/en/publications/china-bites" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ChinaBites</a> publications - monthly updates sent to members and published on the <a href="http://acya.org.au/index.php/en/publications" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> that provide offbeat insights into Australian and Chinese current affairs, pop culture, and modern language. Please email <a href="mailto:neil.thomas@acya.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">neil.thomas@acya.org.au</a> for more information.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><strong style="font-family: arial;"><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">Internships</span></span></span></strong></h4>
<p><a href="http://acya.us1.list-manage1.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=b40edcca8c&amp;e=5074be372c" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">TransAsia Lawyers</a> offers<strong> three month </strong>internships in Beijing or Shanghai exclusively for ACYA members on a rolling basis. Interns will be working in one of the leading law firms licensed to practice in the PRC and get direct work experience at a firm that operates in Chinese and international legal systems.<br /> Visit the <a href="http://acya.us1.list-manage2.com/track/click?u=db2f0ea3a68f632de3088eada&amp;id=42765ecc0f&amp;e=5074be372c" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> or contact <a href="mailto:david.davies@acya.org.au" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;">david.davies@acya.org.au</a>.</p>
<hr /><a href="http://www.argylehotels.com/en/argylegroup/" style="color: #336699; font-weight: normal; text-decoration: underline;">The Argyle Group</a> is an Australian Hotel Group that operates in Australia and China. Argyle Group is currently seeking interns in their hotel operations team. The position is open to all Australian citizens, will be fully paid and will run for at least <strong>6 months</strong>. To apply, or for more information, visit the <a href="index.php/en/careers/featured-opportunities/289-the-argyle-group-in-china-are-seeking-4-operations-interns" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a>.<br /><hr /><a href="http://www.austcham.org/" style="color: #336699; font-weight: normal; text-decoration: underline;">AustCham Beijing</a> is seeking a passionate intern with a strong policy and/or international relations background for the Chamber's Advocacy and Government Relations Department. The internship aims to offer valuable experience and exposure to the workings of Chinese and Australian governments. The internship will run for <strong>3 - 6 months</strong> and will be unpaid (operational expenses will be covered). Visit the <a href="index.php/en/careers/featured-opportunities/283-austcham-beijing-internship-3-to-6-months" style="color: #336699; font-weight: normal; text-decoration: underline;">ACYA website</a> or contact <a href="mailto:jj.chen@austcham.org" style="color: #336699; font-weight: normal; text-decoration: underline;">JJ Chen</a> the Operations Manager of AustCham Beijing for more details.</div>
</td>
<td style="border-collapse: collapse;" align="center" valign="top"><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" alt="" style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content  --> <!-- // Begin Module: Right Image with Content  --> <!-- // End Module: Right Image with Content  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body  --><!-- // Begin Template Footer  -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer  -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="https://twitter.com/#!/a_cya" style="color: #336699; font-weight: normal; text-decoration: underline;">follow on Twitter</a> | <a href="http://www.facebook.com/groups/ACYAnational/" style="color: #336699; font-weight: normal; text-decoration: underline;">friend on Facebook</a> | <a href="http://us1.forward-to-friend1.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=c588bf2b29&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">forward to a friend</a> </div>
</td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"> </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a href="http://transasialawyers.com/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" border="0" alt="TransAsia Lawyers" width="250" height="122" align="none" style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><a href="http://chinainstitute.anu.edu.au/" target="_blank" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" border="0" width="250" height="85" align="none" style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" /></a><br /> <a href="http://www.crccasia.com/" style="color: #336699; font-weight: normal; text-decoration: underline;"><img src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" border="0" width="250" height="175" align="none" style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"> </div>
</div>
</td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <span style="color: #2f4f4f;">Header photograph - Artwork from MOCA Shanghai, taken by <strong>Alan McGuinness.</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><br /> <br /> <span style="color: #2f4f4f;"><em>Copyright © 2012 ACYA, All rights reserved.</em></span><br />  </div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><br /> <br /> <br /> <br /> <br /> <span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" alt="Email Marketing Powered by MailChimp" title="MailChimp Email Marketing" /></a> </span></span></span></div>
<br />    </div>
</td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=c588bf2b29" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> </div>
</td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer  -->