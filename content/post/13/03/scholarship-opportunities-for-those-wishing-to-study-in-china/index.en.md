{
  "title": "Scholarship Opportunities for those wishing to study in China",
  "author": "ACYA",
  "date": "2013-03-26T21:11:48+10:00",
  "image": "",
  "categories": [],
  "tags": [
    "Education"
  ],
  "locations": [],
  "organisations": [],
  "resources": []
}
Always wanted to study in the Middle Kingdom on scholarship?
Do not miss out!

Here are two exciting opportunities are now open to University students for travel to China:
Chinese Government Scholarships for Study in China Short-Term Scholarships Scheme for Foreign Teachers of the Chinese Language Both schemes are offered by the Chinese Ministry of Education, and administered by the China Scholarship Council.
Students in various disciplines of science, technology, agriculture, medicine, economics, law, management, education, history, liberal arts and philosophy may apply.

Register and apply online at http://laihua.csc.edu.cn then forward a copy of your application to David Boyd in the International Portfolio at david.boyd@sydney.edu.au Applications close 10 April 2013