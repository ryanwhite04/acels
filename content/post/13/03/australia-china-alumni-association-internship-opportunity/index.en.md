{
  "title": "Australia China Alumni Association Internship Opportunity",
  "author": "ACYA",
  "date": "2013-03-26T11:48:45+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<h2>Positions:</h2>
<p><span style="text-align: center; line-height: 1.3em;">One position available, part time/full time (negotiable) in Beijing</span></p>
<p>The position can be commenced immediately, and the intern must commit to a <span style="line-height: 1.3em;">minimum of three months.</span></p>
<h2>About Australia China Alumni Association</h2>
<p>The Australia China Alumni Association (ACAA) is an inclusive organisation, open to <span style="line-height: 1.3em;">all China-based alumni of Australian universities. The ACAA serves as a high-profile </span><span style="line-height: 1.3em;">umbrella alumni organisation with the aims of promoting Australian education via </span><span style="line-height: 1.3em;">the success of alumni, and supporting alumni in China through business and social </span><span style="line-height: 1.3em;">networking. With offices in Beijing, Shanghai and Guangzhou, ACAA is a rapidly-</span><span style="line-height: 1.3em;">growing and sustainable, high-profile organisation.</span></p>
<p>The ACAA is proud to have the Australian Ambassador to China, HE Frances <span style="line-height: 1.3em;">Adamson, serve as its Patron.</span></p>
<p>The ACAA is funded by thirty of Australia's universities and is supported by the <span style="line-height: 1.3em;">Australian Government through Australian Education International, Austrade and the </span><span style="line-height: 1.3em;">Department of Foreign Affairs and Trade. We thank our partners for their generous </span><span style="line-height: 1.3em;">and ongoing support, which is helping to create Australia's biggest overseas alumni </span><span style="line-height: 1.3em;">network which is now the largest Australia China people to people network.</span></p>
<p style="text-align: center;"><a href="http://www.austchinaalumni.org/">Read more on our website</a></p>
<h2>Position description:</h2>
<p>The successful candidate will work closely with their supervisor on a range of <span style="line-height: 1.3em;">projects mainly related to event management, social media and alumni development. </span><span style="line-height: 1.3em;">They will also assist with the day-to-day running of the Association. There will </span><span style="line-height: 1.3em;">opportunities to assist with ACAA events during the internship, as well as participate </span><span style="line-height: 1.3em;">in meetings, venue inspections etc. Other duties will include:</span></p>
<ul>
<li><span style="line-height: 1.3em;">Providing effective administrative and event management support</span></li>
<li><span style="line-height: 1.3em;">Assisting with improving the integrity of the ACAA’s online database of alumni </span><span style="line-height: 1.3em;">details;</span></li>
<li><span style="line-height: 1.3em;">Assisting with website updates (posting event information, arranging for </span><span style="line-height: 1.3em;">translations);</span></li>
<li><span style="line-height: 1.3em;">Helping prepare for the “Australia China Alumni Awards”</span></li>
<li><span style="line-height: 1.3em;">Preparing newsletter content for the ACAA’s e-newsletter and uploading </span><span style="line-height: 1.3em;">relevant news to the ACAA website and social media platforms</span></li>
</ul>
<h2>Selection criteria:</h2>
<ul>
<li><span style="line-height: 1.3em;">Substantial progress towards a degree in marketing, communications or </span><span style="line-height: 1.3em;">another relevant discipline;</span></li>
<li><span style="line-height: 1.3em;">Excellent writing skills are essential;</span></li>
<li><span style="line-height: 1.3em;">Native Chinese language ability and high proficiency in English is preferred;</span></li>
<li><span style="line-height: 1.3em;">Experience working with websites, content management systems and email</span></li>
<li><span style="line-height: 1.3em;">marketing systems is highly desirable;</span></li>
<li><span style="line-height: 1.3em;">Experience with Chinese Social Media platforms desirable (e.g. Weibo, </span><span style="line-height: 1.3em;">RenRenWang, KaiXinWang, etc.) is desirable;</span></li>
<li><span style="line-height: 1.3em;">Event management experience is desirable;</span></li>
<li><span style="line-height: 1.3em;">Desktop publishing experience is highly desirable;</span></li>
<li><span style="line-height: 1.3em;">Demonstrated ability to adapt quickly to a new environment and a different </span><span style="line-height: 1.3em;">working environment;</span></li>
<li><span style="line-height: 1.3em;">Positive attitude, reliable, hard working.</span></li>
</ul>
<h2>Required Documentation for Application</h2>
<ul>
<li><span style="line-height: 1.3em;">1 page cover letter</span></li>
<li><span style="line-height: 1.3em;">Maximum 2 page CV</span></li>
</ul>
<p>*All documents should be in English</p>
<p> </p>