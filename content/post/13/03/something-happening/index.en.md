{
  "title": "Meet & Greet at the Red Room - Recap",
  "author": "ACYA",
  "date": "2013-03-10T03:06:03+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->UQ started the year in fine form with a bar tab at UQ's campus bar: Red Room. Over 60 attendees got together for food, drink and a merry kickoff to 2013's engagement in the Aus-China space. 干杯!<!--:--><!--:zh--><p>UQ started the year in fine form with a bar tab at UQ's campus bar: Red Room. Over 60 attendees got together for food, drink and a merry kickoff to 2013's engagement in the Aus-China space. 干杯!</p>
<p> </p>
<p><img src="images/Chapters/UQ/rsz_2013-03-07_16.jpg" width="480" height="360" alt="rsz 2013-03-07 16" style="display: block; margin-left: auto; margin-right: auto;" /></p><!--:-->