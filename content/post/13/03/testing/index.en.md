{
  "title": "Testing",
  "author": "ACYA",
  "date": "2013-03-10T07:22:38+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p> </p>
<div id="centercontent" style="background-color: transparent; border: 0px; margin: 0px; padding: 10px; outline: 0px; vertical-align: baseline; width: 520px; float: left; overflow: hidden; color: #555555; font-family: Helvetica, Arial, sans-serif; font-size: 13px; line-height: 16px;">
<div class="clearpad" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 3px; outline: 0px; vertical-align: baseline;">
<div class="item-page" style="border: 0px; font-style: inherit; margin: 0px; padding: 7px; outline: 0px; vertical-align: baseline; position: relative;">
<p style="margin-top: 0px; margin-bottom: 10px;">Want to get involved with ACYA at an international level?</p>
<p style="margin-top: 0px; margin-bottom: 10px;">ACYA Publications is calling for applications to join the Sub-Committee for the 2013<a href="index.php/en/publications/acya-journal" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">ACYA Journal of Australia-China Affairs</a>, as well as for the Sub-Committee of the new<a href="index.php/en/publications/opportunities-guide" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">ACYA Opportunities Guide</a> to come out later in the year. We are also seeking regular Contributors for our <a href="index.php/en/publications/australia-bites" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">AustraliaBites</a> and <a href="index.php/en/publications/china-bites" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">ChinaBites</a> newsletters on Australian and Chinese current affairs, pop culture, and contemporary language. Please see the respective Call for Applications documents below for more details and how to apply:</p>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;"><a href="images/Documents/ACYA_Call_for_Applications_-_ACYA_Journal_2013.pdf" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">Call for Applications - ACYA Journal of Australia-China Affairs Sub-Committee 2013</a></div>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;"><a href="images/Documents/ACYA_Call_for_Applications_-_Opportunities_Guide_2013.pdf" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">Call for Applications - ACYA Opportunities Guide Sub-Committee 2013</a><a href="images/Documents/ACYA_Call_for_Applications_-_ACYA_Journal_2013.pdf" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;"></a></div>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;"><a href="images/Documents/ACYA_Call_for_Applications_-_ACYA_AustraliaBites_2013.pdf" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">Call for Applications - ACYA AustraliaBites Contributors</a></div>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;"> </div>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;">If you have any questions regarding the Call for Applications please email Neil Thomas, ACYA National Publications Director, on <a href="mailto:publications@acya.org.au?subject=ACYA%20Publications%20Call%20for%20Applications%202013" style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline; color: #a52a2a;">publications@acya.org.au</a>.</div>
<div style="background-color: transparent; border: 0px; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; vertical-align: baseline;"> </div>
</div>
</div>
</div>
<p> </p>