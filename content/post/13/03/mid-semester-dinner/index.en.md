{
  "title": "Mid-semester Dinner",
  "author": "ACYA",
  "date": "2013-03-28T15:18:44+10:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "acya 2",
      "type": "image",
      "src": "acya-2.jpg"
    }
  ]
}
<!--:en--><p style="text-align: justify;"><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">ACYA’s Hong Kong chapter hosted a mid-semester dinner gathering on Monday night at a Thai restaurant near the Hong Kong University campus. </span><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">Delicious pad thai, sweet soufflés for dessert and a great bunch of ACYA-ites came together in what was an excellent evening. It was a good opportunity for the community to have a comprehensive catch up – the slow service allowed lots of time for lively conversation!</span><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">Stay tuned for ACYA HK’s next gathering, which will involve food, drink, fun and sunshine on a boat in Victoria Harbour. Details forthcoming………</span></p>
<span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;"> </span>

<span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;"> </span><!--:--><!--:zh--><p style="text-align: justify;"><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">ACYA’s Hong Kong chapter hosted a mid-semester dinner gathering on Monday night at a Thai restaurant near the Hong Kong University campus.&nbsp;</span><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">Delicious pad thai, sweet soufflés for dessert and a great bunch of ACYA-ites came together in what was an excellent evening. It was a good opportunity for the community to have a comprehensive catch up – the slow service allowed lots of time for lively conversation!</span><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><br style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;" /><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">Stay tuned for ACYA HK’s next gathering, which will involve food, drink, fun and sunshine on a boat in Victoria Harbour. Details forthcoming………</span></p>
<p style="text-align: justify;"><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;"><br /><img src="images/acya.jpg" alt="" width="338" height="225" style="display: block; margin-left: auto; margin-right: auto;" /></span></p>
<p><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;"><img src="images/acya 2.jpg" alt="" width="339" height="226" style="display: block; margin-left: auto; margin-right: auto;" /></span></p>
<p><span style="color: #333333; font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 12.727272033691406px; line-height: 15.454545021057129px;">&nbsp;</span></p><!--:-->