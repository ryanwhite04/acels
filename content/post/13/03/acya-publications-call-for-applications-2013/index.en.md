{
  "title": "ACYA Publications Call for Applications 2013",
  "author": "ACYA",
  "date": "2013-03-04T06:10:23+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Want to get involved with ACYA at an international level?

ACYA Publications is calling for applications to join the Sub-Committee for the 2013 <a href="index.php/en/publications/acya-journal">ACYA Journal of Australia-China Affairs</a>, as well as for the Sub-Committee of the new <a href="index.php/en/publications/opportunities-guide">ACYA Opportunities Guide</a> to come out later in the year.

We are also seeking regular Contributors for our <a href="index.php/en/publications/australia-bites">AustraliaBites</a> and <a href="index.php/en/publications/china-bites">ChinaBites</a> newsletters on Australian and Chinese current affairs, pop culture, and contemporary language.

Please see the respective Call for Applications documents below for more details and how to apply:
  <a href="images/Documents/ACYA_Call_for_Applications_-_ACYA_Journal_2013.pdf">Call for Applications - ACYA Journal of Australia-China Affairs Sub-Committee 2013</a>
  <a href="images/Documents/ACYA_Call_for_Applications_-_Opportunities_Guide_2013.pdf">Call for Applications - ACYA Opportunities Guide Sub-Committee 2013</a>
  <a href="images/Documents/ACYA_Call_for_Applications_-_ACYA_AustraliaBites_2013.pdf">Call for Applications - ACYA AustraliaBites Contributors</a>

If you have any questions regarding the Call for Applications please email Neil Thomas, ACYA National Publications Director, at <a href="mailto:publications@acya.org.au">publications@acya.org.au</a>.