{
  "title": "<!--:en-->ACYA-ACAA Christmas Party<!--:--><!--:zh-->ACYA-ACAA Christmas Party<!--:-->",
  "author": "ACYA",
  "date": "2013-03-10T08:58:32+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Christmas",
      "type": "image",
      "src": "Image1.jpg"
    }
  ]
}
<!--:en-->ACYA Nanjing celebrated Christmas dinner with members of Australia China Alumni Association Nanjing Chapter at La Villa Bar and Restaurant on Saturday 15th December. The night included a delicious roast dinner, drinks and door prizes.

<a href="http://www.acya.org.au/wp-content/uploads/2013/07/Image1.jpg"><img class="aligncenter size-full wp-image-1009" alt="Christmas" src="http://www.acya.org.au/wp-content/uploads/2013/07/Image1.jpg" width="3648" height="2736" /></a><!--:--><!--:zh-->ACYA Nanjing celebrated Christmas dinner with members of Australia China Alumni Association Nanjing Chapter at La Villa Bar and Restaurant on Saturday 15th December. The night included a delicious roast dinner, drinks and door prizes.

<a href="http://www.acya.org.au/wp-content/uploads/2013/07/Image1.jpg"><img class="aligncenter size-full wp-image-1009" alt="Christmas" src="http://www.acya.org.au/wp-content/uploads/2013/07/Image1.jpg" width="3648" height="2736" /></a><!--:-->