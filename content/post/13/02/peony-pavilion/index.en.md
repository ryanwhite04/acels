{
  "title": "Peony Pavilion",
  "author": "ACYA",
  "date": "2013-02-22T07:59:59+10:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p>ACYA Members have been offered a group ticket discount of 30% for the <em>Peony Pavilion</em> playing at the Palais Theatre in St Kilda. Please contact Sushan at <a href="mailto:sushan.s@live.com" target="_blank" style="color: #1155cc;">sushan.s@live.com</a> for more details.</p>
<p><strong style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">Time</strong><span style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">: Feb 26th, 27th 2013</span></p>
<p><strong style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">Venue</strong><span style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">: The Palais Theatre Lower Esplanade, St Kilda VIC3182</span></p>
<div dir="ltr">
<p><strong style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">Price</strong><span style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">:$39 $69 $99 $169</span></p>
<p dir="ltr"><span style="color: #222222; font-size: 13px; font-family: arial,sans-serif;">For detailed information, please go check our offical ticket sale website on ticket master: </span><span style="font-size: 13px; font-family: arial,sans-serif; color: #ff0000;"><span style="text-decoration: underline;"><a href="http://www.ticketmaster.com.au/The-Peony-Pavilion-tickets/artist/1800925" target="_blank" style="color: #1155cc;">http://www.ticketmaster.com.au/The-Peony-Pavilion-tickets/artist/1800925</a></span></span><span style="font-size: 13px; font-family: arial,sans-serif; color: #ff0000;"> <br /></span></p>
</div>
<p>See flyer <a href="http://www.brendonbody.com/peony.html">here</a>.</p>
<p>{youtube}https://www.youtube.com/watch?v=-qnZMVDHl3g{/youtube}</p>
<p><object width="640" height="360"><param name="movie" value="http://www.youtube.com/v/-qnZMVDHl3g?hl=en_GB&amp;version=3&amp;rel=0" /><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /></object></p>