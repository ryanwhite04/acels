{
  "title": "<!--:en-->ACYA Newsletter October 2013<!--:-->",
  "author": "ACYA",
  "date": "2013-11-21T03:33:35+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->Welcome.

October is already upon us – how the year has flown by!

In the latest issue you can read wrap ups of the 2013 Australia-China Youth Dialogue, ACYA’s participation in the ‘Australia’s China, China’s Australia’ FASIC Conference in Beijing and the glamorous 2013 ACYA Ball. Also included are an interview with Hanyu Qiao finalist Jack Fisher, the latest from the Australia-China Young Professionals Initiative and contributions from ACYA Chapters all across Australia. Be sure to check out a special Sydney China Business Forum offer for ACYA members and our piece on the ACAA Alumni Awards.

See you all again in November.

The ACYA Bulletin Team
<h3><a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=4bdd39ca39">E-Bulletin October 2013</a></h3><!--:--><!--:zh--><strong>欢迎辞</strong>

今年的时光也一如既往地飞逝着：10月已经到来了！

在本期的最新时讯中，您将会阅读到：2013年中澳青年对话落幕; ACYA成员参加由中国澳大利亚研究基金会(FASIC)在北京举办的“澳大利亚的中国、中国的澳大利亚”会议活动；2013年ACYA舞会相关报道；独家专访“汉语桥”大赛决赛选手鲱杰克；中澳青年精英领袖团的最新消息；还有ACYA澳大利亚各分会的相关报道。本期时讯中还有专向ACYA成员开放的悉尼中国商业论坛的相关机会、以及澳中同学联谊会杰出校友奖的信息，您决不能错过！

11月再见！

ACYA团队
<h3><a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=4bdd39ca39">E-Bulletin October 2013</a></h3><!--:-->