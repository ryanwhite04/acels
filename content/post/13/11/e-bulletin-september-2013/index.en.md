{
  "title": "<!--:en-->ACYA Newsletter September 2013<!--:-->",
  "author": "ACYA",
  "date": "2013-11-21T03:32:24+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->Welcome

Welcome everyone! We have a bumper issue in store for you this month, with wrap ups of our hugely successful ACYA National Conference, HOME 2013 and Leizhou volunteer trip. You can read our interview with Alistair Bayley, the first Aussie to take out the Hanyu Qiao crown, as well as an exclusive sneak peek at what to expect from the 2013 Australia-China Youth Dialogue with Fiona Lawrie. ACYA members based in Beijing should also strongly consider applying for an internship with Geoff Raby &amp; Associates – full details below!

To venture below the tip of the iceberg, read on and enjoy. See you all again in October!

The ACYA E-Bulletin team
<h3><a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=f7900a65a0&amp;e=b9ce0f9afe">E-Bulletin September 2013</a></h3><!--:--><!--:zh--><strong>欢迎辞</strong>
欢迎您阅读本期时讯！本月我们将为你带来超足量的信息：ACYA全国大会、2013 HOME计划和雷州志愿者计划都取得了令人瞩目的成功；第一位获得“汉语桥”大赛冠军的澳洲选手贝乐泰的采访；Fiona Lawrie带来即将召开的2013年中澳青年对话的预告；为身在北京的ACYA成员提供的超宝贵Geoff Raby &amp; Associates公司的实习机会等等——欲知详情请继续阅读！

祝您本月的阅读体验一如既往的愉快！十月再见！

ACYA时讯团队
<h3><a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=f7900a65a0&amp;e=b9ce0f9afe">E-Bulletin September 2013</a></h3><!--:-->