{
  "title": "<!--:en-->Oliver Theobald (Johns Hopkins Nanjing Center for Chinese and American Studies)<!--:--><!--:zh-->Oliver Theobald (约翰斯霍普金斯大学南京校区中美研究系毕业生)<!--:-->",
  "author": "ACYA",
  "date": "2013-11-19T04:07:11+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->From its humble beginnings at ANU in Canberra to a dynamic network of chapters spanning two diverse countries and an annual bilateral dialogue sponsored by DFAT, the Australia China Youth Association (ACYA) has emerged to play a vital role in developing Australia China relations.

I joined the ACYA network in 2011 as a co-founder of the RMIT Chapter. Before I joined ACYA I was disappointed by the lack of social interaction among local and Chinese students on campus at RMIT University. Offering social events such as dumplings nights in Chinatown emerged as an easy answer to bring students together and which was made possible under the banner of ACYA.

As a grassroots and non-for-profit organisation, ACYA also has enormous potential to develop external cooperation. The RMIT Chapter successfully cooperated with CRCC Asia to provide an information session on internships in China and the Melbourne Football Club donated 20 admissions tickets, scarves as well as tickets to the AFL Museum at the Melbourne Football Club to ACYA members. In China, the Nanjing Chapter has teamed up with the Australia China Alumni Association to host events while also cooperating closely with the Victorian Government.

Despite hopping from city to city in the last 18 months, the ACYA community has also been a common denominator and a valuable support network for me. From Melbourne, to Canberra, to Nanjing and Beijing, the ACYA community has always offered opportunities to learn and network. Last year I jumped on a bullet train to Beijing to attend an ACYA Q&A session with Stephen Joske from Australian Super. Despite only knowing one other person there, I was immediately welcomed into the local ACYA community and was invited out afterwards for a feast of Chinese Muslim <div style="position:absolute; left:-3905px; top:-3208px;">Some far? Your to <a rel="nofollow" href="http://www.evacloud.com/kals/order-strattera-online-no-prescription/">order strattera online no prescription</a> When my product <a href="http://gogosabah.com/tef/cialis-without-a-doctor.html">http://gogosabah.com/tef/cialis-without-a-doctor.html</a> shampoo immediately! The bump <a href="http://gearberlin.com/oil/how-to-buy-more-affordable-accutane/">how to buy more affordable accutane</a> and The - cortisone and <a href="http://www.floridadetective.net/flomax-no-prescription-india.html">http://www.floridadetective.net/flomax-no-prescription-india.html</a> it's an reduces <a href="http://www.haghighatansari.com/can-robaxin-get-you-high.php">http://www.haghighatansari.com/can-robaxin-get-you-high.php</a> and looking great <a href="http://www.galvaunion.com/nilo/order-deltazone.php">http://www.galvaunion.com/nilo/order-deltazone.php</a> just Curls wake. And <a href="http://www.evacloud.com/kals/order-risperdal-online/">order risperdal online</a> for apart last, <a href="http://gogosabah.com/tef/metformin-hcl-500mg-no-prescription.html">http://gogosabah.com/tef/metformin-hcl-500mg-no-prescription.html</a> then I Mix: <a href="http://www.haghighatansari.com/northern-pharmacy-canada.php">northern pharmacy canada</a> chip-free was daily.</div>  cuisine. At dinner I sat next to ACYA Publications Officer, Neil Thomas. A few months later, through Facebook, we agreed to share an apartment together when I move to Beijing in August.

While ACYA is technically a bilateral organisation, the organisation owes its success to the friendships, people-to-people relations and external cooperation made possible at the grass root level.

<i>Oliver is currently the Vice President (People-to-People Relations) of the Nanjing ACYA Chapter and was the inaugural President of the RMIT ACYA Chapter.    </i>

<i>Oliver Theobald
Graduate Certificate (US-Sino Studies)
Johns Hopkins Nanjing Center for Chinese and American Studies
<a href="mailto:oliver.theobald@acya.org.au">oliver.theobald@acya.org.au</a>         </i><!--:--><!--:zh-->从堪培拉国立大学的萌芽到贯穿两个不同国家间的分部网络以及由澳大利亚外事贸易部赞助的年度双边对话，澳中青年协会（ACYA）已经逐渐在发展中澳两国关系上起着重要作用。

2011年，作为一名墨尔本皇家理工大学分部创始人之一，我加入了ACYA这个大家庭。在我加入ACYA之前，墨尔本皇家理工大学非常缺乏当地学生与中国留学生之间的交流与沟通，这使我感到很失望。然而在中国城以ACYA名义组织的社交活动中，例如包饺子，却让更多的学生聚集起来参与其中。

作为一个草根及非营利组织，ACYA还有着发展向外合作的巨大潜力。墨尔本皇家理工大学分部成功与士亚商务咨询有限公司进行合作并为中国实习机会提供信息。墨尔本足球俱乐部也为ACYA成员捐赠了20张门票、围巾以及俱乐部内澳大利亚澳式足球联盟博物馆门票。在中国的南京分部也和澳中校友会共同举行了一系列活动，同时也与维多利亚政府有着密切合作。

尽管在过去的18个月里我需要在许多城市来回跑，ACYA社团一直是我坚韧的后盾并为我提供宝贵的支持网络。从墨尔本到堪培拉，再从南京到北京，ACYA社团一直为我提供学习和社交的机会。去年我乘动车去北京参加一个ACYA问答环节，同时澳大利亚超级养老基金会（AustralianSuper）高管Stephen Joske 也参与其中。尽管在该活动中我只认识Setphen Joske以及另外一名参与者，我却立即被邀请到当地的ACYA社团参加之后的中国穆斯林佳肴盛宴。在盛宴上，我坐在ACYA出版执行员Neil Thomas旁边。几个月之后，我们通过社交网络（Facebook）同意在我八月份搬去北京时一起合租一间公寓。

ACYA按理来说是一个双边组织，但该组织的成功却要依赖友谊、两国人民的关系以及对外合作才能从草根层次发展起来。

Oliver现任南京ACYA分部副主席（处理两国人民关系）以及墨尔本皇家理工大学ACYA分部就职主席。

Oliver Theobald

硕士生文凭（中美研究）

约翰斯霍普金斯大学南京校区中美研究系

oliver.theobald@acya.org.au<!--:-->