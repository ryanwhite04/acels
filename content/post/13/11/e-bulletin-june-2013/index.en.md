{
  "title": "ACYA Newsletter June 2013",
  "author": "ACYA",
  "date": "2013-11-21T03:26:02+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
June is already upon us and it’s been another busy and fruitful month for ACYA Group. In our second bilingual edition you’ll find our interview with Zhou Jiamiao, ACYA’s representative to the 2013 Global Emerging Voices program, as well as the latest news from our chapters and initiatives, including the newly minted ACYA chapter at the University of Newcastle.

Be sure to get your applications in for the 2013 Australia-China Youth Dialogue, to be held in Canberra and Perth, and also fill out ACYPI’s survey gathering young professionals’ views on the Sino-Australian relationship if you have a spare second.

Enjoy the month ahead!

All the best from the ACYA Group team.