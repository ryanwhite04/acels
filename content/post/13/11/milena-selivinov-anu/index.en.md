{
  "title": "<!--:en-->Milena Selivinov (ANU)<!--:-->",
  "author": "ACYA",
  "date": "2013-11-19T04:03:47+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "3",
      "type": "image",
      "src": "3.jpg"
    },
    {
      "title": "4",
      "type": "image",
      "src": "4.jpg"
    }
  ]
}
<!--:en--><h4>How has ACYA enhanced your life in your local community?</h4>
Being involved in ACYA has definitively had an impact on my perception of the student community and my role in it. I have realised that people look forward to getting together for events with people that may be different to their usual social circle. Curiosity to step out of a comfort zone is not uncommon. Through ACYA one not only makes new friends but also engages in dialogue that often challenges one’s assumptions and beliefs. However it’s not only about finding out about differences. There is actually a lot more in common between the Chinese and Australian students than I once thought. For example, before getting involved in ACYA, I have often subscribed to the prejudice that Chinese students are all about studying, and this meant that I would often try to really get to know those Chinese students I could engage with. But I was relying on the assumption without actually interrogating it. This is probably what a lot of new students of an Asian language without an Asian cultural background feel. By participating in ACYA events one realizes that it’s quite the opposite! It is true that Chinese culture is quite different to its Western counterparts but personally this has made me want to but to know more about these similarities and differences.
The events that we organize through ACYA are a great occasion to get out of the routine and create something that is not for one’s own benefit but mainly for others’. When doing this as a team all the efforts couldn’t be repaid in a better way than by the gratitude of the participants.
To meet the requests of ACYA members by listening to what they would like to see happening, gives the ACYA@ANU team great goals. From this I believe it is a responsibility to deliver and be a community of students that is heard and respected.
I firmly believe that ACYA offers a true university life experience to the committee and the members. By connecting people from different cultural and disciplinary backgrounds we appreciate the student diversity on campus. The creation and organization of new events is all possible thanks to the energy of the committee and the members’ support, where, within the context of Australian and Chinese cultural exchange, friendship is fostered.<!--:-->