{
  "title": "<!--:en-->Liuissa Zhen (University of Queensland)<!--:-->",
  "author": "ACYA",
  "date": "2013-11-18T04:14:34+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "2",
      "type": "image",
      "src": "2.jpg"
    }
  ]
}
<!--:en-->Liuissa Zhen
Psychology
University of Queensland
<a href="mailto:lulu.zhen@gmail.com">lulu.zhen@gmail.com</a>

How has ACYA enhanced my life and/or the local community?

Where do I even begin? I’m an Australian born Chinese (ABC) and like
many others, it’s not the easiest to find someone like you. Did you
ever feel lonely because no one else had the same hair colour, could
understand your culture, or even brought similar packed lunches as you
to school? I know I did. I was the only Chinese person in my grade,
scratch that, I was the only person of colour in my grade all
throughout primary! So when it came to university I was ecstatic when
I realised that THERE WERE OTHER PEOPLE LIKE ME! WE COULD COMPLAIN
ABOUT OUR PARENTS TOGETHER! WE COULD COMPLAIN ABOUT UNACCEPTABLE
CULTURAL DEVIATIONS! My world seemed that much brighter! And that's
where ACYA came in, because they provided this safe haven away from
those who could not comprehend why I wanted to learn Chinese over the
weekend or could stomach the idea of bird's nest soup. They were the
ones who encouraged not only international students to engage with the
local students who could barely speak a phrase of Chinese, but also
encouraged Australian students to actively engage with Chinese
students to learn about the rich culture and history of the Middle
Kingdom. So ACYA isn’t just about me, it’s about my fellow students
being able to enjoy each other’s company and culture in a
multicultural, judgement free zone.<!--:-->