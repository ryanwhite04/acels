{
  "title": "ACYA Group heralded in Asian Century China Strategy, supports rollout of New Colombo Plan",
  "author": "ACYA",
  "date": "2013-10-25T05:21:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "492286-new-colombo-steering-group",
      "type": "image",
      "src": "492286-new-colombo-steering-group.jpg"
    },
    {
      "title": "china_strategy",
      "type": "image",
      "src": "china_strategy.jpg"
    },
    {
      "title": "chinacountrystrat_ecp",
      "type": "image",
      "src": "chinacountrystrat_ecp.jpg"
    },
    {
      "title": "ACYA2",
      "type": "image",
      "src": "ACYA2.jpg"
    }
  ]
}
In June 2013, in response to the Department Foreign Affairs and Trade (DFAT) call for submissions to their Asian Century Country Strategies, the Australia-China Youth Association (ACYA) made a submission to the China Country Strategy.

<!--more-->

The point of the submission was to lay out in clear and concrete terms what ACYA believes are essential focus areas for DFAT when moving forward in the Australia-China relationship.

ACYA is pleased that in the release of DFAT’s report on the Asian Century China Strategy a key ACYA initiative, the Engaging China Project (ECP), has been featured as a privileged case study.

Taking pride of place on the official website and on page 7 of the strategy document, ECP receives high praise: “They inspire and mentor school students to learn Chinese language and to engage with China in creative ways across the Australian curriculum.”

Other ACYA initiatives including the Australia-China Youth Dialogue and Australia-China Young Professionals Initiative are also recognised on page 12 of the strategy as organisations that will “connect future champions of the bilateral relationship, and increase the flow of ideas and people between our countries.”

Congratulations to everyone across the ACYA Group portfolio for achieving this well-deserved recognition.

ACYA also looks forward to the Australian Government’s rollout of the New Colombo Plan as it moves from a pilot phase in 2014 to full commencement in 2015.
ACYA National President (Australia), Thomas Williams, is a member of the New Colombo Plan Steering Committee and will ensure that the voices of young people from Australia and China are brought to the table as the plan develops further.

The Asian Century China Strategy can be downloaded <a href="http://www.dfat.gov.au/issues/asian-century/china/">here</a>.
For more information on the New Colombo Plan, visit the <a href="http://www.dfat.gov.au/new-colombo-plan/">official website</a>.

<img class="alignnone size-full wp-image-1675" src="http://www.acya.org.au/wp-content/uploads/2013/10/china_strategy.jpg" alt="china_strategy" width="438" height="626" />
<img class="alignnone size-full wp-image-1676" src="http://www.acya.org.au/wp-content/uploads/2013/10/chinacountrystrat_ecp.jpg" alt="chinacountrystrat_ecp" width="1042" height="482" />