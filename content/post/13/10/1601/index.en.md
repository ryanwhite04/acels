{
  "title": "<!--:en-->Australia China Youth Association @ UNSW Executive Testimonials 2013<!--:-->",
  "author": "ACYA",
  "date": "2013-10-03T04:08:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA Logo",
      "type": "image",
      "src": "ACYA-Logo-GOOD.jpg"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.15 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.15-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.26 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.26-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.35 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.35-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.41 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.41-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.49 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.49-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.54 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.54-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.15.59 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.15.59-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.16.04 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.16.04-PM.png"
    },
    {
      "title": "Screen Shot 2013-10-03 at 2.16.09 PM",
      "type": "image",
      "src": "Screen-Shot-2013-10-03-at-2.16.09-PM.png"
    },
    {
      "title": "acya",
      "type": "image",
      "src": "acya2.jpg"
    }
  ]
}
<!--:en--><a href="http://www.acya.org.au/wp-content/uploads/2013/10/acya2.jpg"><img class="size-full wp-image-1723 aligncenter" alt="acya" src="http://www.acya.org.au/wp-content/uploads/2013/10/acya2.jpg" width="618" height="358" /></a>

Below are the testimonials for each of the ACYA executives. Have a read of their testimonials so that you can personally gather the experiences of what it is like to be an ACYA executive. The executive is willing to discuss any issues you have regarding the positions, their personal experiences and what it is like to be an executive of ACYA at UNSW.

Being an executive of a large, expanding society requires a lot of commitment, motivation and effort in order to move the society forward and allow it to expand. It is important that you have a strong understanding of the different roles and for you to choose the best ones for yourself and that you are willing to sacrifice your free time (or even study time), to help better the society.

On an extra note, you may need to attend a few national conferences around Australia to discuss issues amongst all of the other ACYA chapters.
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.15-PM.png"><img class="size-medium wp-image-1604 alignleft" alt="Screen Shot 2013-10-03 at 2.15.15 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.15-PM-224x300.png" width="224" height="300" /></a>Aimee Yi (易梦萦) - President</h1>
<div title="Page 1">

I am very proud to be the President (Australia) of ACYA @ UNSW, for so many reasons. Most significantly, I get the opportunity to work with and drive an amazing team of very committed and motivated young people who all share the common goal of bridging the gap between Australian and Chinese students. Through this, we are part of a much larger cause of building closer ties between Australia and China. This is something I am also personally committed to.

For me, being President means to strive towards three important ideals: representation, motivation, and support. The President represents ACYA @ UNSW to external organisations and institutions, as well as the wider community. This means reflecting the image of ACYA accurately, by proliferating its purpose and goals. In order to do so, the President must motivate the team, and allow each executive to be involved and take ownership of his or her roles. Finally, the President is someone that needs to be supportive, approachable and understanding of a diverse range of people. Most importantly, the President must be committed and place ACYA first.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.26-PM.png"><img class="size-full wp-image-1605 alignleft" alt="Screen Shot 2013-10-03 at 2.15.26 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.26-PM.png" width="244" height="300" /></a>Yongzhen Chen (陳永臻) - Secretary</h1>
<div title="Page 1">

What I have found great about working with a part of the ACYA @ UNSW executive team was due to the fact that the society had a bilateral focus. It was not just exclusive to Chinese people, but in fact, non-Chinese people were welcome as well. I had the pleasure of being able to host events such as the International Students Careers workshop, hosted by Zookal, as well as socials such as the joint ACYA dinner and the Mahjong night. I also enjoyed helping out with the combined Project Hope and ACYA Amazing Race in semester 1, which generated HUGE success for both societies.

Being a part of the ACYA team also enabled me to develop my leadership skills and be able to immerse myself with people of different thinking styles. It also allowed me to be a part of a multicultural society that aims to foster a greater understanding between the issues that arise between Chinese and Australian people. It also allowed me to turn a “dead society” into a more active society, which was the crucial objective for this year.

As the secretary of ACYA, I had a very important role. This consisted of recruiting people for O-week, making sure event attendance was more than satisfactory and including the usual taking down minutes of meetings and doing the paperwork required. For those who are considering applying for the Secretary position, this is an important position of the society and you will NEED to make sure you are diligent and committed to your role. You may potentially need to cut into your study time in order to fulfil the society’s needs. Being able to speak in Mandarin and/or Cantonese is essential, but not necessary.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.35-PM.png"><img class="size-full wp-image-1606 alignleft" alt="Screen Shot 2013-10-03 at 2.15.35 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.35-PM.png" width="200" height="300" /></a>Adam Elliott (亚当) - Treasurer</h1>
<div title="Page 1">

For me, the highlight of being part of the ACYA team is the shared goal and focus on bilateral engagement between Australian and Chinese students. Our exec team in 2013 was full of great people with different perspectives who were all able to work effectively to produce group decisions. It is always rewarding to see our successful events such as our Games Night and career workshops which were a result of hard work and dedication over weeks of organising.

The role of Treasurer involves monitoring the cash flows of the society during the year. This requires the ability to stay organised and keep a record of the cost of events, ARC grants and the payment and reimbursement of relevant people for each event. For some simple events this may seem straight forward, however, the payment process for large events involving a range of different payment methods can often make it hard to keep track. In addition, the Treasurer should also contribute to the planning and execution of all events as the exec team needs to have full cooperation to continue creating value for students.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.41-PM.png"><img class="size-full wp-image-1607 alignleft" alt="Screen Shot 2013-10-03 at 2.15.41 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.41-PM.png" width="200" height="300" /></a>Yasar Chowdhury - ARC Delegate</h1>
<div title="Page 1">

During my time as an Executive member of ACYA, I’ve had the pleasure to work with, and meet, a variety of people who have offered me a range of unique insights into cross-cultural relationships between China and Australia. Our presence on, and outside of, campus has increased rapidly, and I’ve enjoyed being a part of events such as Games Night and the International Night Markets, meeting many new people and attaining a sense of belonging as a member of ACYA. I look forward to our future events, and I hope to see you there!

My role of Arc Delegate requires me to maintain the good relationship between ACYA and Arc @ UNSW, who are responsible for allocating club funding and regulations. This mandates a thorough understanding of Arc policies and a strict adherence to punctuality and deadlines. Compared to some other executive roles, the Arc Delegate’s executive capacity is more “behind-the-scenes”, and less conspicuous. Due to my executive role not being readily apparent to most members, this allows me to be more active as a regular member of the society at society events, as most of my work happens after events.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.49-PM.png"><img class="size-full wp-image-1608 alignleft" alt="Screen Shot 2013-10-03 at 2.15.49 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.49-PM.png" width="200" height="190" /></a>Fangmei (May) Chen - VP (Careers)</h1>
<div title="Page 1">

For general routine, I attend the fortnight meetings, put forward suggestions towards how to attract international students and meet their needs. As a native Chinese as well as an international student, I understand the thoughts and behaviour of Chinese students and take my responsibility to build connections between the society and Chinese students.

Currently my role is the career VP as well as the international student representative of ACYA. My job involves seeking career related opportunities for international students, and representing interests of international students. I’m also contributing my language skill and do some literal work such as translating and writing marketing materials. When there is going to be activities for international students, I take part in marketing and come to the activities to promote and coordinate.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.54-PM.png"><img class="size-full wp-image-1609 alignleft" alt="Screen Shot 2013-10-03 at 2.15.54 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.54-PM.png" width="200" height="260" /></a>Zhen Wang (王臻达) - VP (Education)</h1>
<div title="Page 1">

It is a great honour to be elected the vice president of education for ACYA and while there are commitments and obligations with the role, it also provides a great and unique opportunity for learning and self-development, as well as meeting great people and gaining new friends. In my one year and half of service as the education VP I have gained brand new insights about myself and made some unforgettable experiences and friends. One recent highlight that comes to mind is the ACYA national conference which brought together over 60 delegates from all over Australia, each with their own interesting story to share.

As the vice president of education, the role will consist of organising events with educational merits for ACYA during the semester and also liaising and actively supporting with the sister organisation Engaging China Project(ECP). To actively support ECP, the education VP must provide UNSW students as ambassadors to ECP presentation in high schools, with the aim of spreading Chinese culture and language to young students. The UNSW ambassadors does not need to be member of ACYA, although it is preferred, and the VP themselves can also volunteer as an ambassador and it will be counted as a normal student in terms of contributions to the ECP. Of course, as a student society, the actual tasks performed by the VP of education can be varied and beyond the job description, case in point, I have also functioned as a translator and helped in the organising and running of events not related to education in my service.

</div>
<div title="Page 1"></div>
<p style="text-align: center;">____________________________________________________________________________________</p>
<img class="alignleft" alt="Screen Shot 2013-10-03 at 2.15.59 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.15.59-PM.png" width="200" height="200 class=" />
<h1>William Chan (陳偉霖) - VP (P2P)</h1>
<div title="Page 1">

ACYA is one of the more unique societies that you can expect to encounter within UNSW. It is a society that integrates the concept of ‘east meets west’ in which it facilitates this process by providing a platform for cultural exchange between Chinese international students who have a high degree of interest in Australian culture, and domestic and international students, both of and not of Chinese descent, who have a strong interest in Chinese culture. In this cultural exchange process, the three pillars which form the basis of ACYA consists of careers (which is tailored for students planning to establish their career in China and Australia), education (for those who would like to enhance their knowledge in Chinese and Australian culture) and the social pillar (for those who are looking for a means to meet new people and to have a good time).

As the Vice President of the social aspect of ACYA, I have found this position to be quite rewarding, both in terms of the skills and experiences that were obtained throughout the whole duration, but also the people that I had the opportunity to meet. The two essential key ingredients which would contribute significantly to this aspect are event management and social networking. It is important that you reserve quite a lot of time from your school work and other forms of commitment, as it is quite demanding, that is, the more time you invest in organizing an event, the grander and more memorable it will be. With regards to social networking, it is vital to build rapport and liaise with other societies to not only maximize the amount of attendees for your events, as a greater number results in a more successful event, but also, it provides an opportunity to collaborate with other societies to generate a flow of ideas when designing an event. If everything runs smoothly, the whole of UNSW will know your name!

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.16.04-PM.png"><img class="size-full wp-image-1612 alignleft" alt="Screen Shot 2013-10-03 at 2.16.04 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.16.04-PM.png" width="200" height="280" /></a>Kelvin Ren (任继禹) - Sponsorship</h1>
<div title="Page 1">

To be a part of ACYA UNSW is to be a part of something unique. It’s not quite a cultural club like CSA (Chinese Students Association), yet not a professional one like UNIT (University Network of Investing and Trading). We hit a niche sector of students and organisations, yet we have great potential. ACYA exists in different universities across Australia all promoting similar goals and desires.

Handling sponsorship for ACYA can be difficult. Most of the times you’ll be trying to find support for specific events rather for the society in general. Most of the times you’ll be working with other executives in planning out events for ACYA. But there truly is no limit. The role of the sponsorship director is to find external support, in any form, not just financial. You may find yourself working alongside executives from other chapters in finding major sponsors or working alone in approaching restaurants and small firms. What’s truly great about this position however is that if you are someone who is really interested in entering a field involving Australia-China relationships, being Sponsorship Director gives you front row seats in meeting different businesses and building your own connections. While you should always put ACYA first, as what you do impacts the society’s image, at the end of the day, you will also benefit. So the more effort you put into this position, the greater the satisfaction and reward.

As Sponsorship Director, it is important that you have confidence in yourself. This is a role that requires you to do a lot of talking with older people that have years of experience. You need to be approachability and friendly, as well as know exactly what you want. Being bi-lingual is a bonus, but it is not necessary.

</div>
<p style="text-align: center;">____________________________________________________________________________________</p>

<h1><a href="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.16.09-PM.png"><img class="size-full wp-image-1613 alignleft" alt="Screen Shot 2013-10-03 at 2.16.09 PM" src="http://www.acya.org.au/wp-content/uploads/2013/10/Screen-Shot-2013-10-03-at-2.16.09-PM.png" width="200" height="280" /></a>Haoxiang (Howie) Fei (费昊翔) - Communications</h1>
<div title="Page 1">

I really enjoy working as an executive for ACYA UNSW because I have the opportunity to work together with a great team of people to achieve our stated goal of being a platform for cross-cultural engagement. We have been very active this semester in holding a variety of events for our members, and if you didn’t attend any of them, you’re missing out! It’s been great to not only see our members having fun and to see our hard work paying off, but to also enjoy the events myself. However, being the Communications Director has also meant that I’ve had to market events seemingly non-stop, so sorry for all the emails!

As a Communications Director, there are some skills which you need to possess and which you will further develop in the role. Any executive role requires good teamwork and leadership skills to communicate well with the rest of the exec team, to fulfil your assigned tasks in a timely manner, and to be proactive in putting forward ideas. It’s especially important to have good written communication skills for this role. As a bonus, English – Chinese bilingual skills are highly useful as we do have a lot of members who are Chinese international students. And lastly, enthusiasm and passion for what ACYA is about will always make a difference!

</div><!--:-->