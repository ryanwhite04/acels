{
  "title": "<!--:en-->ACYA Panel lights up ‘Australia’s China, China’s Australia’ Conference, Beijing<!--:--><!--:zh-->ACYA小组闪耀“澳洲的中国，中国的澳洲”会议，北京<!--:-->",
  "author": "ACYA",
  "date": "2013-10-04T04:29:18+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_0308 (1)",
      "type": "image",
      "src": "IMG_0308-1.jpg"
    },
    {
      "title": "IMG_0283",
      "type": "image",
      "src": "IMG_0283.jpg"
    },
    {
      "title": "IMG_0312",
      "type": "image",
      "src": "IMG_0312.jpg"
    },
    {
      "title": "IMG_0310 (1)",
      "type": "image",
      "src": "IMG_0310-1.jpg"
    },
    {
      "title": "IMG_0307",
      "type": "image",
      "src": "IMG_0307.jpg"
    },
    {
      "title": "IMG_0302 (1)",
      "type": "image",
      "src": "IMG_0302-1.jpg"
    },
    {
      "title": "acya",
      "type": "image",
      "src": "acya1.jpg"
    }
  ]
}
<!--:en--><div>

<span style="font-family: tahoma, sans-serif;">ACYA members recently appeared alongside renowned figures such as Hugh White and <i>The Australian</i>’s Paul Kelly at the 'Australia's China, China's Australia: Past, Present &amp; Future' conference in Beijing. The conference was convened by Professor David Walker, the inaugural BHP Billiton Chair of Australian Studies at Peking University (PKU-ASC), in cooperation with the Foundation for Australian Studies in China (FASIC). </span>

<span style="font-family: tahoma, sans-serif;">ACYA panellists Elliott Clutterbuck, Sophie Smith, Oliver Theobald and Neil Thomas rounded off the two-day conference with the entertaining and well-received 'Our China Dream: Personal Perspectives from Beyond the Blackboard' session that focused on their on-the-ground experiences and perspectives as Australian students in China.</span>

<span style="font-family: tahoma, sans-serif;">The panel discussion was hosted by prominent media personality Geraldine Doogue, who ventured probing questions on topics such as study experiences in China, language acquisition, embarrassing cross-cultural misunderstandings, and even transnational dating! The ACYA panel was well received for its honest, interesting, and very personal angle on the discussion of contemporary Australia-China relations.</span>

<span style="font-family: tahoma, sans-serif;">The inaugural conference of the revamped Australian Studies Centre at Peking University was hailed a remarkable success thanks to the hard work of Creative Asia Founder and Australia-China Young Professionals Initiative Sydney Co-Director, Hannah Skrzynski. ACYA members and PKU-ASC interns James Campbell, Robbie Harris, Michael McGregor and Joel Wing-Lun were also involved in the organisation and behind-the-scenes operations of the two day event.</span>

<span style="font-family: tahoma, sans-serif;">Overall, ACYA was thrilled to have been invited to participate in the conference and is immensely grateful for the continued support and encouragement of Professor David Walker. ACYA looks forward to working closely with the PKU-ASC and FASIC for the remainder of 2013 and into the future.</span>

<span style="font-family: tahoma, sans-serif;"><a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0308-1.jpg"><img class="alignnone size-full wp-image-1618" alt="IMG_0308 (1)" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0308-1.jpg" width="649" height="485" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0302-1.jpg"><img class="alignnone size-full wp-image-1623" alt="IMG_0302 (1)" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0302-1.jpg" width="649" height="485" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0307.jpg"><img class="alignnone size-full wp-image-1622" alt="IMG_0307" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0307.jpg" width="649" height="485" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0310-1.jpg"><img class="alignnone size-full wp-image-1621" alt="IMG_0310 (1)" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0310-1.jpg" width="649" height="485" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0312.jpg"><img class="alignnone size-full wp-image-1620" alt="IMG_0312" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0312.jpg" width="649" height="485" /></a> <a href="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0283.jpg"><img class="alignnone size-full wp-image-1619" alt="IMG_0283" src="http://www.acya.org.au/wp-content/uploads/2013/10/IMG_0283.jpg" width="649" height="485" /></a> </span>

</div><!--:--><!--:zh-->ACYA成员近日和著名人士修·怀特（Hugh White）以及《澳大利亚人报》的保罗·凯利（Paul Kelly）在北京出席了“澳洲的中国，中国的澳洲：过去，现在和未来” 会议。此次会议由首位北京大学必和必拓澳大利亚研究中心教授大卫·沃克（David Walker）和中国澳大利亚研究基金会共同举办。

ACYA小组成员埃利奥·特克拉特巴克（Elliott Clutterbuck），索菲·史密斯（Sophie Smith），奥利弗·西奥博尔德（Oliver Theobald）和牛犇（Neil Thomas）以在一场有趣的“我们的中国梦：超越讲义的个人观点”讨论环节的出色表现圆满完成了此次为期两天的会议。他们在这一讨论环节中以在中国留学的澳洲学生的视角分享了他们的亲身经历和观点看法，获得了极好的反响。

小组讨论是由著名的媒体人士杰拉尔丁·杜格（Geraldine Doogue）主持的。他提出的讨论话题包括在中国的学习经历，语言的学习，尴尬的跨文化误解，甚至跨国约会！ACYA小组在对当今中澳关系的讨论中凭借真诚、有趣、并具有个人看法的回答获得了热烈的欢迎。

由于创新亚洲的创始人、中澳青年专业人士倡议活动悉尼的共同主任汉娜·克拉辛斯基（Hannah Skrzynski）的辛勤努力，此次由重整后的北京大学澳洲研究中心首次举办的会议取得了巨大的成功。

ACYA成员、同时也是北京大学澳洲研究中心实习生的詹姆斯·坎贝尔（James Campbell），罗比·哈里斯（Robbie Harris），迈克尔·麦克格雷戈（Michael McGregor）以及Joel Wing-Lun也参与了这一为期两天的活动的幕后组织工作。

总体来说，ACYA非常激动能被邀请参加这一会议。同时我们也对大卫·沃克（David Walker）教授对我们一直以来的支持和鼓励表示无限感激。 ACYA期待在2013年剩下的时光以及未来能与北京大学澳洲研究中心和中国澳大利亚研究基金会有更加密切的合作。<!--:-->