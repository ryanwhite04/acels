{
  "title": "<!--:en-->Nanjing ACYA Executive Position Nominations Now Open<!--:--><!--:zh-->Nanjing ACYA Executive Position Nominations Now Open<!--:-->",
  "author": "ACYA",
  "date": "2013-07-03T11:40:15+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->The Nanjing Australia China Youth Association Chapter was established in September 2012 and in just over nine months it has established itself as one of the most proactive chapters within the ACYA network and a leader in mainland China.

As the first generation of ACYA Nanjing volunteers return home to Australia or continue other pursuits in China there is now an opportunity for new enthusiastic volunteers to be involved with developing the Chapter’s presence and growth strategy.

Volunteering with the ACYA Nanjing Chapter offers a valuable experience to practice leadership skills, to develop a personal network in China, contribute to advancing Australian-China relations, and to develop China specific skills and experience, including Chinese social media and the Chinese style of ‘networking’.

The outgoing chapter president, Steve Carton, will be able to provide support and assistance (he will remain in Nanjing), as will Chelsea Zhou, a Nanjing native who is a very resourceful member of the team.
<h2>Application Details</h2>
<ul>
	<li>Positions available: President, Vice President, Communications Manager, Secretary/Treasurer</li>
	<li>Applications for other specialised positions are also welcome, ie Graphic Design, Careers Officer etc</li>
	<li>Optimum age of applicants is 19-30 years of age</li>
	<li>To apply please email a 1-2 page resume and 1 page cover letter to<a href="mailto:steve.carton@acya.org.au">steve.carton@acya.org.au</a></li>
	<li>Applications close August 1. Both successful and unsuccessful candidates will be notified by mid August</li>
</ul>

<h2>Chapter President</h2>
<h3>Criteria:</h3>
<ul>
	<li>Strong time management, communication &amp; managerial skills</li>
	<li>Preparedness to oversee/facilitate a range of different initiatives</li>
	<li>Excellent networking skills</li>
	<li>Sound knowledge of members’ interests and expectations</li>
	<li>Ability to plan chapter’s current and future position (exec formation, handover, etc)</li>
</ul>
<h3>Core Duties and Responsibilities:</h3>
Managing the overall development of ACYA in Nanjing including:
<ul>
	<li>Chapter development</li>
	<li>Developing relationships within local community</li>
	<li>Assessing opportunities for sponsorships and partnerships</li>
	<li>Facilitating, leading, encouraging the work of other exec members</li>
	<li>Ensuring workload is spread among team with clear delegation and support among exec portfolios</li>
	<li>Represent ACYA at meetings with sponsors/stakeholders and (where necessary) in media</li>
	<li>Contributing to all ACYA meetings (national and chapter)</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Leadership experience</li>
	<li>Experience with ACYA preferred (not essential)</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Cross-cultural fluency (Mandarin preferred not essential)</li>
	<li>Ability manage/facilitate as appropriate</li>
	<li>Confident spokesperson</li>
</ul>
Time Commitment: 5 hours average per week.
Length of Time Held Position: 6 – 12 months (longer period preferred) or 1 – 2 semesters

<h2>VICE PRESIDENT</h2>

<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing Chapter social events</li>
	<li>Assisting the Chapter President with the overall development of ACYA in Nanjing and representing ACYA Nanjing at external events in the absence of the Chapter President</li>
	<li>Managing membership sign up events once or twice a semester</li>
	<li>Developing and managing external Chapter relations</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Leadership experience</li>
	<li>Event management skills</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Cross-cultural fluency (Mandarin preferred not essential)</li>
	<li>Flexible and adaptable to assuming new tasks</li>
	<li>Strong communication skills</li>
</ul>
Time Commitment: 4 hours average per week.

Length of Time Held Position: 6 – 12 months or 1 – 2 semesters

<h2>Communications Manager</h2>

<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing the Chapter’s overall social media presence (Facebook, Weibo and Wechat), including delegation of tasks to Chelsea Zhou (ACYA Nanjing communication officer) or any other potential volunteers within the Communications department.</li>
	<li>Preparing and distributing a monthly e-bullentin</li>
	<li>Updating information in regard to Nanjing events on the ACYA website</li>
	<li>Maintaining close communication with the National ACYA Communications Manager</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Proficient with or willing to learn how to use Mailchimp (e-bulletin software)</li>
	<li>Proficient with or willing to learn how to navigate Chinese social media</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Strong time management and communication skills</li>
</ul>
Time Commitment: 5 hours average per week.
Length of Time Held Position: 6 – 12 months or 1 – 2 semesters
<h2>Secretary/Treasurer</h2>
<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing the Chapter’s administrative tasks, including passwords, executive email addresses and printing name cards</li>
	<li>Note taking at executive meetings</li>
	<li>Managing the Chapter’s funds</li>
	<li>Managing funding applications from the national ACYA body</li>
	<li>Assessing opportunities for potential sponsorships</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Proficient with Microsoft Excel</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Strong time management and organisational skills</li>
</ul>
Time Commitment: 3 hours average per week.
Length of Time Held Position: 6 – 12 months or 1 – 2 semesters<!--:--><!--:zh-->The Nanjing Australia China Youth Association Chapter was established in September 2012 and in just over nine months it has established itself as one of the most proactive chapters within the ACYA network and a leader in mainland China.

As the first generation of ACYA Nanjing volunteers return home to Australia or continue other pursuits in China there is now an opportunity for new enthusiastic volunteers to be involved with developing the Chapter’s presence and growth strategy.

Volunteering with the ACYA Nanjing Chapter offers a valuable experience to practice leadership skills, to develop a personal network in China, contribute to advancing Australian-China relations, and to develop China specific skills and experience, including Chinese social media and the Chinese style of ‘networking’.

The outgoing chapter president, Steve Carton, will be able to provide support and assistance (he will remain in Nanjing), as will Chelsea Zhou, a Nanjing native who is a very resourceful member of the team.
<h2>Application Details</h2>
<ul>
	<li>Positions available: President, Vice President, Communications Manager, Secretary/Treasurer</li>
	<li>Applications for other specialised positions are also welcome, ie Graphic Design, Careers Officer etc</li>
	<li>Optimum age of applicants is 19-30 years of age</li>
	<li>To apply please email a 1-2 page resume and 1 page cover letter to<a href="mailto:steve.carton@acya.org.au">steve.carton@acya.org.au</a></li>
	<li>Applications close August 1. Both successful and unsuccessful candidates will be notified by mid August</li>
</ul>

<h2>Chapter President</h2>
<h3>Criteria:</h3>
<ul>
	<li>Strong time management, communication &amp; managerial skills</li>
	<li>Preparedness to oversee/facilitate a range of different initiatives</li>
	<li>Excellent networking skills</li>
	<li>Sound knowledge of members’ interests and expectations</li>
	<li>Ability to plan chapter’s current and future position (exec formation, handover, etc)</li>
</ul>
<h3>Core Duties and Responsibilities:</h3>
Managing the overall development of ACYA in Nanjing including:
<ul>
	<li>Chapter development</li>
	<li>Developing relationships within local community</li>
	<li>Assessing opportunities for sponsorships and partnerships</li>
	<li>Facilitating, leading, encouraging the work of other exec members</li>
	<li>Ensuring workload is spread among team with clear delegation and support among exec portfolios</li>
	<li>Represent ACYA at meetings with sponsors/stakeholders and (where necessary) in media</li>
	<li>Contributing to all ACYA meetings (national and chapter)</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Leadership experience</li>
	<li>Experience with ACYA preferred (not essential)</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Cross-cultural fluency (Mandarin preferred not essential)</li>
	<li>Ability manage/facilitate as appropriate</li>
	<li>Confident spokesperson</li>
</ul>
Time Commitment: 5 hours average per week.
Length of Time Held Position: 6 – 12 months (longer period preferred) or 1 – 2 semesters

<h2>VICE PRESIDENT</h2>

<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing Chapter social events</li>
	<li>Assisting the Chapter President with the overall development of ACYA in Nanjing and representing ACYA Nanjing at external events in the absence of the Chapter President</li>
	<li>Managing membership sign up events once or twice a semester</li>
	<li>Developing and managing external Chapter relations</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Leadership experience</li>
	<li>Event management skills</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Cross-cultural fluency (Mandarin preferred not essential)</li>
	<li>Flexible and adaptable to assuming new tasks</li>
	<li>Strong communication skills</li>
</ul>
Time Commitment: 4 hours average per week.

Length of Time Held Position: 6 – 12 months or 1 – 2 semesters

<h2>Communications Manager</h2>

<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing the Chapter’s overall social media presence (Facebook, Weibo and Wechat), including delegation of tasks to Chelsea Zhou (ACYA Nanjing communication officer) or any other potential volunteers within the Communications department.</li>
	<li>Preparing and distributing a monthly e-bullentin</li>
	<li>Updating information in regard to Nanjing events on the ACYA website</li>
	<li>Maintaining close communication with the National ACYA Communications Manager</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Proficient with or willing to learn how to use Mailchimp (e-bulletin software)</li>
	<li>Proficient with or willing to learn how to navigate Chinese social media</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Strong time management and communication skills</li>
</ul>
Time Commitment: 5 hours average per week.
Length of Time Held Position: 6 – 12 months or 1 – 2 semesters
<h2>Secretary/Treasurer</h2>
<h3>Core Duties and Responsibilities:</h3>
<ul>
	<li>Managing the Chapter’s administrative tasks, including passwords, executive email addresses and printing name cards</li>
	<li>Note taking at executive meetings</li>
	<li>Managing the Chapter’s funds</li>
	<li>Managing funding applications from the national ACYA body</li>
	<li>Assessing opportunities for potential sponsorships</li>
	<li>Attending local Chapter events wherever possible</li>
</ul>
<h3>Expectations:</h3>
<ul>
	<li>Proficient with Microsoft Excel</li>
	<li>Proven interest in and commitment to Sino-Australian relations</li>
	<li>Prepared to commit time and energy</li>
	<li>Strong time management and organisational skills</li>
</ul>
Time Commitment: 3 hours average per week.
Length of Time Held Position: 6 – 12 months or 1 – 2 semesters<!--:-->