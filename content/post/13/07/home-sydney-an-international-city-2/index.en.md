{
  "title": "<!--:en-->Home, Sydney an International City<!--:--><!--:zh-->悉尼，国际化的“家园”<!--:-->",
  "author": "ACYA",
  "date": "2013-07-13T06:25:15+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "HOMEsydneyshanghai (5)",
      "type": "image",
      "src": "HOMEsydneyshanghai-5.jpg"
    },
    {
      "title": "HOMEverticaldv",
      "type": "image",
      "src": "HOMEverticaldv.jpg"
    }
  ]
}
<!--:en-->“HOME” 2013, organised along the theme of “Sydney an International City”, is the keynote event of the year for the Australia-China Youth Association (ACYA) in NSW. It is a large-scale cross-cultural festival, designed to bring together international students, local Australian students, and the broader community to celebrate the Sydney’s diversity. The event aims to encourage cross-cultural exchange and showcase the many exciting opportunities within the Australia-China space.
<h2>Event Details</h2>
The event features a series of fun activities, interactive exhibitions, captivating visual displays, festive performances, and engaging careers and educational workshops. HOME will take a fresh approach to engaging  the local community in Australia-China exchange, and is truly a one-stop-shop for everything Australia-China related.
<b>When: </b>10.00am - 5.00pm
<b>Where:</b> Lower Town Hall 483 George Street Druitt St Entrance (Opp Queen Victoria Building) Sydney NSW 2000
<b>Cost: </b>Free
<b>Registration: </b><a href="http://acya.ticketleap.com/home2013/">http://acya.ticketleap.com/home2013/</a>
<b>Questions? </b><a href="http://info.home@acya.org.au/">info.home@acya.org.au</a>
<b>Volunteer?</b> <a href="mailto:volunteer.home@acya.org.au">volunteer.home@acya.org.au</a>
<h2>MISSION STATEMENT</h2>
<blockquote>“A Multicultural Day - At ACYA we believe what brings people together is stronger than what drives them apart"</blockquote>
HOME aims to connect and reconnect people with the place and the community we call or hope to call our HOME. The HOME project brings a diverse cross-section of local and international communities together for a series of fun and fresh activities. Supported by educational, social, cultural and professional pillars, ACYA hopes the HOME project will become a platform to provide community work experience for international students, germinate formal and informal mentoring relationships between senior business and communities and young Australians and Chinese, ,and showcase to key stakeholders and the public the possibilities and opportunities that can arise from an ongoing Australia-China conversation and mutual understanding.<!--:--><!--:zh-->2013 “家园” 活动是以“悉尼，国际化的家园”为主题的大型综合文化活动，由澳中青年联合会（ACYA）主办，面向留学生，澳洲本地学生以及广大对中澳关系及文化有兴趣的人群，旨在促进跨文化交流，传播文化多样性，展现中澳两国的发展机会。
活动详情：
活动包括艺术展览，荧屏演示，歌舞表演，辩论演讲，职业教育咨询等精彩互动环节。在充分展示中澳文化之余，”家园”将采取全新的方式，鼓励观众参与到这场有趣的文化盛宴。
时间： 早10：00至晚5：00
地点： Lower Town Hall， 483 George Street & Druitt St Entrance (Queen Victoria Building的对面) Sydney NSW 2000
门票：免费
登记：http://acya.ticketleap.com/home2013/
咨询： info.home@acya.org.au
志愿者： volunteer.home@acya.org.au
 
使命宣言
“多元文化的节日”－ ACYA相信团结的力量”
“家园”活动旨在为两国青年提供一个互相交流的“家”或者“希望”的平台。
“家园”让本地学生和留学生互相交流并且参加一系列新鲜有趣的活动。ACYA涵盖了教育，社会，文化和职业领域，我们希望“家园”活动会成为一个平台,能够帮助留学生获得社会工作经验, 建立企业与澳中青年的联系，并且向主要参与方和社会展示如何从中澳两国双边对话与了解中获得新的机遇
<!--:-->