{
  "title": "<!--:en-->ACYA 串儿B.Q<!--:--><!--:zh-->ACYA “烤串儿”聚餐活动<!--:-->",
  "author": "ACYA",
  "date": "2013-07-27T11:12:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "BBQ",
      "type": "image",
      "src": "rsz_img_4191.jpg"
    }
  ]
}
<!--:en-->Cork hats on and tongs at the ready! ACYA Person-to-person exchange at its best. Last Monday night (8/07/2013) saw ACYA serving up authentic Aussie sausages to the fine people of Doufuchi Hutong in Gulou, Beijing. The inaugural ‘ACYA 串儿B.Q’ was held under the shadow of the Bell Tower as the sunset on another hot and humid Summer day in the ‘Jing.

A great turn out of students from the ACYA @USYD Chapter currently studying in Beijing, local Beida students, ACYA @Beijing Chapter President Mert and ACYA National Exec including Victoria (China President), Mike (National Secretary) and Sophie (Membership Director).

Big thanks to the Laoban of the local chuanr restaurant that allowed us to cook our snags on their barbie! Sausages were well received by the locals with overtures of “好吃, 好吃!” all round.

The ‘ACYA 串儿B.Q’ is a great example of ACYA’s work on the frontline of Australia-China cultural exchange.
<a href="http://www.acya.org.au/wp-content/uploads/2013/07/rsz_img_4191.jpg"><img class="aligncenter size-full wp-image-1073" alt="BBQ" src="http://www.acya.org.au/wp-content/uploads/2013/07/rsz_img_4191.jpg" width="600" height="400" /></a>
<!--:--><!--:zh-->戴上户外猎人帽，准备开始烧烤啦！ACYA这次的人文交流别开生面。2013年7月8日晚上，ACYA在北京鼓楼豆腐池胡同为当地居民烹饪了澳洲香肠大餐。为了躲避7月炎夏，我们于傍晚在鼓楼的阴凉下开始了 “ACYA串儿B.Q”。

参加本次活动的成员包括目前在北京学习的ACYA悉尼分会的学生、来自北京大学的中国学生，ACYA北京分会主席Mert Erkul、ACYA全国管理委员会成员<a href="http://www.linkedin.com/profile/view?id=112704780&amp;authType=NAME_SEARCH&amp;authToken=hEEJ&amp;locale=en_US&amp;srchid=21fdfb9d-d1e1-4722-b754-532d91658db5-0&amp;srchindex=1&amp;srchtotal=9&amp;goback=%2Efps_PBCK_*1_Victoria_Kung_*1_*1_*1_*1_*2_*1_Y_*1_*1_*1_false_1_R_*1_*51_*1_*5">贡爱玲</a>（中国地区主席）、<a href="http://www.linkedin.com/in/michaelmcgregor1">Michael McGregor</a>（全国总秘书）和苏飞飞（全国会员事务主管）。

非常感谢当地“串儿烧烤”餐馆老板的帮助！当地居民们也觉得我们澳式烧烤非常好吃！

ACYA “串儿烧烤”聚餐活动是一次很成功的中澳文化交流活动。<!--:-->