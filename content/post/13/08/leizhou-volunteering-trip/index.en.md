{
  "title": "My experience on the Leizou Volunteering Trip",
  "author": "ACYA",
  "date": "2013-08-24T02:07:58+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Untitled1",
      "type": "image",
      "src": "Untitled1.png"
    },
    {
      "title": "Untitled2",
      "type": "image",
      "src": "Untitled2.png"
    },
    {
      "title": "Untitled3",
      "type": "image",
      "src": "Untitled3.png"
    },
    {
      "title": "Untitled4",
      "type": "image",
      "src": "Untitled4.png"
    },
    {
      "title": "acya copy",
      "type": "image",
      "src": "acya-copy.jpg"
    }
  ]
}
When I heard that ACYA had organised a volunteering trip to teach English to Chinese middle school students in a place called Leizhou my initial thought was, “Where?!”

Despite being similar in size to Perth, the Guangdong city of Leizhou, with 1.7 million residents, is by Chinese standards considered a small backwater. Foreign presence here is non-existent -- it doesn’t even rate a mention in Lonely Planet. And yet upon arrival there I discovered that the city, and the peninsula which shares its name, is rich in history and culture, with a unique dialect, interesting architecture, and even a decent beach! Locals like to boast that it is mainland China’s southern-most point, however they can be slightly more taciturn when their custom of eating dog is brought up.

I was part of a delegate of four ACYA members called in to teach authentic colloquial English as part of “Crazy Spoken Language Summer Camp”, an initiative arranged by the Leizhou College Students Volunteer Association (LZCVA). The aim of the programme was to encourage students to come out of their shells, in term-time not always possible under the rigid structure of the Chinese education system. With 130 participants aged between thirteen and seventeen, we certainly had our work cut out for us, especially considering our limited teaching experience.

<a href="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled1.png"><img class="aligncenter size-full wp-image-1511" src="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled1.png" alt="Untitled1" width="521" height="347" /></a>

<a href="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled2.png"><img class="aligncenter size-full wp-image-1512" src="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled2.png" alt="Untitled2" width="488" height="325" /></a>

Each of us was given a class of about 30 students and a few hours of classroom time per day, and what we did with that time was basically up to us, as long as we got the students actively engaged and speaking English. Given the varying levels of language proficiency within each class, and the crippling shyness of many students, this was often a challenge; the phrase “Have a go” become a sort of mantra in my classroom. Many of the people we were teaching had never come into contact with a foreigner before, and a significant part of class time was spent explaining bizarre rituals such as “Christmas” and “surfing”. We ended up playing a lot of language games, as they allowed for maximum participation and got creative juices flowing. Perhaps the greatest difficulty lay in getting the students to understand my accent. In Chinese schools English is generally taught by Chinese people with Chinese accents, and so most students are never exposed to a genuine native accent, much less a relatively obscure Australian one. On the first day, after being told to slow my speaking down several times, I finally reached a level the students could understand at about one fifth of my usual talking speed. In spite of this initial frustration, it was deeply satisfying for all involved when recognition of what I was saying finally dawned on the faces before me.

<a href="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled3.png"><img class="aligncenter size-full wp-image-1513" src="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled3.png" alt="Untitled3" width="502" height="335" /></a>

Our final day with the students was spent rehearsing for, and then performing in, a group singing competition. All four classes chose an English-language song, and we helped them to both learn the lyrics and understand the meaning (I never realised the profound subtext in Westlife’s <i>My Love </i>until I had to translate it into Chinese!) I glowed with pride seeing my kids up on stage pronouncing <i>th</i>’s and <i>v</i>’s with clarity and confidence. The tables were turned, though, when, due to overwhelming audience request, another ACYA member and I were goaded into doing a duet performance of the Mandarin pop hit “因为爱情” <i>(Because of Love). </i>By the time judging was finished, it was obvious that winning or losing had become irrelevant (and I say this even though my students won): as each class fervently screamed “We love you!” to their respective English teachers, we realised that our presence at this summer camp had had a real impact on their lives. We were all extremely moved by the immense number of gifts we received, ranging from an antique vase to a homemade calligraphy scroll to a handwoven key-ring. Most moving of all, I think, was that when approaching me for a final photo together, almost all of my students expressed a newfound desire to apply themselves to continue studying English. These teenagers have been struggling to learn the global <i>lingua franca </i>since primary school, as a way of standing out in a nation of over one billion, and showing them the language in its living, non-textbook form gave them some much-needed motivation.

<a href="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled4.png"><img class="aligncenter size-full wp-image-1514" src="http://www.acya.org.au/wp-content/uploads/2013/08/Untitled4.png" alt="Untitled4" width="549" height="331" /></a>

The infrequency of their coming means that foreign visitors to Leizhou can expect red carpet treatment. All four of us were put in home-stays, allowing us to experience local customs like sleeping on beds with wooden mattresses (surprisingly comfortable) and to enjoy the communal warmth of living with four generations under one roof. The regional government also made us feel very welcome; we were treated to a delicious banquet of local fare and a crazy karaoke night at the finest hotel in town by a committee member of the city’s CCP Youth League. LZCVA also organised some great activities including a barbecue and, at our request, a trip to the nearby beach. This was a great chance for us to share some Aussie culture with our Chinese counterparts, many of whom had never learnt to swim. By the time we left Leizhou, the number of new WeChat contacts in our phones reflected the fact that, despite a short stay, we’d made some pretty deep bonds with a smart, fun and proactive group of young Chinese people, and in doing so had laid the foundations for a bright future of cooperation between ACYA and LZCVA. I hope that next year this program is continued and expanded, as it not only allowed us, as foreigners, to glimpse a part of the ‘real China’ far from the tourist trail and first-tier cities, but also to contribute to such a community in a meaningful way. By sustaining this contribution, the effect on the students of Leizhou No. 1 Middle School will be that much greater. Maybe one day, armed with the confidence afforded by a grasp of the English language, one of them will go on to achieve great things on the international stage, and put this ‘small town’ on the map.