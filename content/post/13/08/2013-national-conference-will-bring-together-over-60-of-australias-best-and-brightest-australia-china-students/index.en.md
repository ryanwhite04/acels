{
  "title": "<!--:en-->2013 National Conference will bring together over 60 of Australia’s best and brightest Australia-China students<!--:-->",
  "author": "ACYA",
  "date": "2013-08-23T14:42:31+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "National Conference",
      "type": "image",
      "src": "1175575_10151790372283334_1114187834_n-e1384744802198.jpg"
    },
    {
      "title": "1",
      "type": "image",
      "src": "1.jpg"
    }
  ]
}
<!--:en-->ACYA is proud to be hosting the 2013 National Conference this weekend in Brisbane, Australia.  Between 23<sup>rd</sup> and 26<sup>th</sup> August ACYA will bring together over sixty of the brightest domestic and international students from ACYA Chapters at over fourteen Australian universities to discuss and learn about Sino-Australia affairs.

ACYA Australia President Tom Williams said “hosted on campus at the University of Queensland by the ACYA UQ Chapter, the 2013 National Conference is a unique event that will see a number of Australia-China experts deliver presentations to delegates, including Bill O’Chee who is opening proceedings”.

“Our aim is to bring together the best students to generate ideas, discussion and debate about where they see the Australia-China relationship going and how ACYA fits into this framework”.

Over the course of the three days ACYA will provide a range of training, speaking and social activities to build stronger relations between Australian and Chinese students and provide a development platform for our future leaders.

At the conclusion of this event, conference delegates will depart with a renewed understanding of what it is like to be a part of Australia’s only youth run non-profit organisation aimed at building Australia-China relations.

Special acknowledgement must also go to the Australia China Council and the Australian National University, for without their financial support this event would not be possible.

For media enquiries relating to the 2013 National Conference please contact:

Jimmy Zeng (president_UQ@acya.org.au)
<script type="mce-text/javascript" src="http://widgets.twimg.com/j/2/widget.js">// <![CDATA[
<span id="mce_marker" data-mce-type="bookmark"></span><span id="__caret">_</span>
// ]]></script><script type="mce-text/javascript">// <![CDATA[
new TWTR.Widget({   version: 2,   type: 'search',   search: '#ACYA.NC',   rpp: 30,   interval: 5000,   title: 'ACYA National Conference',   subject: 'Updated Live',   width: 200,   height: 300,   theme: {     shell: {       background: '#8ec1da',       color: '#ffffff'     },     tweets: {       background: '#ffffff',       color: '#000000',       links: '#1985b5'     }   },   features: {     scrollbar: true,     loop: true,     live: true,     hashtags: true,     timestamp: true,     avatars: true,     toptweets: true,     behavior: 'all'   } }).render().start();
// ]]></script><!--:-->