{
  "title": "<!--:en-->ACYA Enjoys Australia-China Council Grant Success<!--:--><!--:zh-->ACYA获澳中理事会拨款赞助<!--:-->",
  "author": "ACYA",
  "date": "2013-08-21T20:56:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACC",
      "type": "image",
      "src": "image001-1.jpg"
    },
    {
      "title": "2",
      "type": "image",
      "src": "2.jpg"
    }
  ]
}
<!--:en-->Australia-China Youth Association (ACYA) Group initiatives the Australia-China Youth Association (ACYA), Australia-China Youth Dialogue (ACYD) and Engaging China Project (ECP) have been successful in securing grant funding from the Australia-China Council (ACC) to continue their dynamic work fostering meaningful engagement between young people from Australia and China.
ACYA Group would like to thank the ACC Board and the Australian Government Department of Foreign Affairs and Trade for recognising our contribution to the Australia-China youth space. ACC’s support will allow ACYA to grow its presence across Australian and Chinese universities, encourage ECP to send more young China Ambassadors into Australian high schools and contribute to the 2013 ACYD, which will bring together 30 outstanding delegates from Australia and China to discuss issues of key importance to the bilateral relationship from 24-29 September in Canberra and Melbourne.
For a full list of successful 2013-14 ACC grant recipients, please visit <a href="http://www.acya.org.au/2013/08/16/acya-enjoys-australia-china-council-grant-success/www.dfat.gov.au/acc/grants/successful-recipients.html">www.dfat.gov.au/acc/grants/successful-recipients.html</a>.
<h3>About ACYA Group</h3>
ACYA Group’s mission is to inspire Australians and Chinese to work together in realising their vision of a more prosperous, sustainable and interconnected world in the Asian Century. By providing meaningful and frequent engagement between young Australians and Chinese, empowering motivated young people to successfully implement new initiatives, and engaging established leaders in a range of fields from both countries, ACYA Group promotes greater cooperation and engagement between Australia and China and empowers young people who share our vision.
ACYA Group consists of four initiatives: the Australia-China Youth Association (ACYA); Australia-China Youth Dialogue (ACYD); Australia-China Young Professionals Initiative (ACYPI); and Engaging China Project (ECP). For more information, please visit <a href="http://www.acya.org.au/2013/08/16/acya-enjoys-australia-china-council-grant-success/www.acya.org.au/australia-china-youth-association-group-acya-group">www.acya.org.au/australia-china-youth-association-group-acya-group</a>.
<h3>About the Australia-China Council</h3>
The Australia-China Council (ACC) was established by the Australian Government in 1978 to promote mutual understanding and foster people-to-people relations between Australia and China. The function of the Council is to make recommendations to the Australian Government through the Minister for Foreign Affairs on strengthening the Australia-China relationship in ways that support Australia’s foreign and trade policy interests.
For more information, please visit <a href="http://www.dfat.gov.au/acc">www.dfat.gov.au/acc</a>.<!--:--><!--:zh-->“中澳青年联合组织”（ACYA Group）创办的中澳青年联合会（ACYA）、中澳青年对话（ACYD）、以及关注中国计划（ECP）成功地获得了澳大利亚中国理事会（ACC）的拨款赞助，从而使我们能够继续活跃在加强中澳青年间深层次的交流互动的最前线。
ACYA衷心感谢澳中理事会董事成员与澳大利亚政府外交与贸易部对我们在中澳青年交流空间所做出的努力的认可。在澳大利亚中国理事会的支持下，ACYA在中国和澳洲的众多高校的出镜率将大大增加，并且使我们的“关注中国计划”能够派送更多的年轻的中国使者到澳洲的各个高中，同时也将促进2013年的中澳青年对话（ACYD）活动；本次“对话”将于9月24至29日在堪培拉和墨尔本举行，届时30位来自中国和澳洲的杰出代表将对中澳双边关系中的重要话题进行探讨。
欲查看完整版的2013-2014年度澳大利亚中国理事会拨款赞助对象的名单，请访问 <a href="http://www.acya.org.au/2013/08/16/acya-enjoys-australia-china-council-grant-success/www.dfat.gov.au/acc/grants/successful-recipients.html">www.dfat.gov.au/acc/grants/successful-recipients.html</a>。
<h3>关于“中澳青年联合组织”</h3>
“中澳青年联合组织”（ACYA Group）的宗旨是促进中国与澳洲青年彼此间的合作，从而实现他们共同的愿景，即在亚洲世纪中打造一个更加繁荣、持久、并且相互联系的世界。通过为中澳青年提供不间断的充满意义的交流与互动、让蓄势待发的年轻人能够成功地将梦想付诸实践、以及促进在两国诸多领域各有建树的领导者们的积极参与，我们志在加强中澳两国间更加广泛与深入的交流与合作，同时尽心帮助与我们有着相同愿景的年轻一代。
“中澳青年联合组织”的四大分支机构分别为：中澳青年联合会（ACYA）、中澳青年对话（ACYD）、中澳青年精英领袖团（ACYPI）、以及关注中国计划（ECP）。欲了解更多信息，请访问 <a href="http://www.acya.org.au/2013/08/16/acya-enjoys-australia-china-council-grant-success/www.acya.org.au/australia-china-youth-association-group-acya-group">www.acya.org.au/australia-china-youth-association-group-acya-group</a>.
<h3>About the Australia-China Council</h3>
The Australia-China Council (ACC) was established by the Australian Government in 1978 to promote mutual understanding and foster people-to-people relations between Australia and China. The function of the Council is to make recommendations to the Australian Government through the Minister for Foreign Affairs on strengthening the Australia-China relationship in ways that support Australia’s foreign and trade policy interests.
For more information, please visit <a href="http://www.dfat.gov.au/acc">www.dfat.gov.au/acc</a>.<!--:-->