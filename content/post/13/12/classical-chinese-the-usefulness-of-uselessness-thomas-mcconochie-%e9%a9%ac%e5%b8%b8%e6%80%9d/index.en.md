{
  "title": "Classical Chinese: The Usefulness of Uselessness - Thomas McConochie 马常思",
  "author": "ACYA",
  "date": "2013-12-14T07:41:37+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Classical Chinese Article",
      "type": "application",
      "src": "Classical-Chinese-Article.docx"
    },
    {
      "title": "chinese-traditional-characters",
      "type": "image",
      "src": "chinese-traditional-characters-e1387006719408.jpg"
    }
  ]
}
<p style="text-align: center;"><a href="http://www.acya.org.au/wp-content/uploads/2013/12/chinese-traditional-characters.jpg"><img class=" wp-image-1832 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2013/12/chinese-traditional-characters.jpg" alt="chinese-traditional-characters" width="512" height="344" /></a></p>
<i>This article was awarded the ACYA Journal Opinion Article Prize (English).</i>

<i>Thomas McConochie has studied Mandarin in Australia and Taiwan</i><i>.</i><i> He was awarded a Bachelor of Asian Studies (Chinese) from the Australian National University (ANU) in 2006 and a Master of Arts (East Asian Studies) from ANU in 2012. He commenced a PhD at the University of New South Wales in 2013. His research interests include; Zhuangzi </i>庄子,<i> Daoism, classical Chinese thought, mysticism and the philosophy of religion</i><i>.</i><i> </i><i>Thomas is also </i><i>NAATI</i><i> accredited</i><i> Chinese-to-</i><i>English translator and has been working as a translator since 2012.</i>

Classical Chinese (i.e. literary Chinese) is a good subject to take for students majoring in Mandarin Chinese or any other Chinese language at university. Being able to understand literary Chinese gives one access to more than 2000 years of Chinese literature. Through reading classical Chinese literature, students can come to appreciate Chinese philosophy, poetry and storytelling as it was originally expressed, without translation into modern languages such as Mandarin. Thus people learning Mandarin would do well to study classical Chinese.

For non-native speakers of modern Mandarin, learning classical Chinese is especially helpful with regards to more formal vocabulary and sentence patterns that have basically been ‘transferred’ from classical Chinese into vernacular Mandarin. For example, the construction ‘以…为…’, meaning ‘to treat (something) as (something in particular)’, which often appears in formal and especially written modern Chinese. Or the passive construction ‘为…所…’, meaning ‘to be (past-participle verb [at least in English]) by (the active party)’, which is occasionally used in modern Mandarin but appears frequently in classical Chinese. Having an understanding of classical Chinese also helps students understand the meaning of many Chinese ‘idioms’ (<i>chengyu </i>成语) by making them familiar with the more complex vocabulary used in <i>chengyu </i>and capable of reading <i>diangu </i>典故, the ancient tales from which <i>chengyu</i> are derived.

Some of the most commonly used <i>chengyu</i>,<i> </i>such as <i>shou zhu dai tu</i>守株待兔 (‘standing by a stump waiting for more hares to come and dash themselves against it’ – to wait idly for opportunities at the expense of one’s work), and even phrases that are not strictly <i>chengyu</i>,<i> </i>such as <i>wan de buyilehu</i> 玩得不亦乐乎 (having an awfully good time), <i>san si er hou xing</i><i> </i>三思而后行 (think again and again before acting) and <i>ji lai zhi ze an zhi</i><i> </i>既来之则安之 (you have come this far so may as well stick with it), have a <i>diangu </i>or have been modified from their original usages in philosophical works written using classical Chinese.<a title="" href="#_ftn1">[1]</a> Therefore there are many reasons why people learning Mandarin as a second language should want to learn and reap the benefits of knowing classical Chinese.

Unfortunately, many students of Mandarin are not interested in learning classical Chinese. I have made this observation from teaching undergraduate students at the Australian National University (ANU) who are undertaking Mandarin as part of a double major.<a title="" href="#_ftn2">[2]</a> When I tutored first-year ANU students in elementary Mandarin in 2012, I encouraged my over sixty students to take classical Chinese classes from their second year. Most were not very attracted to the idea. They felt discouraged by how ‘hard’ classical Chinese seemed to them. They reasoned that they were only seeking to gain an ‘adequate’, ‘acceptable’, ‘passable’ or ‘functional’ command of modern Mandarin, which is ‘already hard enough’, and so there would be no point in attempting to learn the ‘even harder’ and much ‘less useful’ classical Chinese. Students further complained that not only is the vocabulary, grammar and content of texts written in classical Chinese harder than what they are willing to learn (though this point was not one they made explicitly), but having to learn full-form traditional Chinese characters makes the task that much harder still.

I have observed a degree of flippancy amongst university students whom I have tutored in Mandarin. These students reason that only modern Chinese written in simplified characters is worth knowing. I make a point of asking every one of my students why they are learning Mandarin, and the answer I often hear is ‘because it would be good to be able to speak some Chinese when I go to China on business’ or something similar to this effect.<a title="" href="#_ftn3">[3]</a> These students do not seem to understand that they can only gain a high level of proficiency in a foreign language if they actively use that language to learn the culture, customs, history, literature and expressive means of its people. One can never become truly proficient in a non-native language by merely using it as some kind of novel device for ‘getting by’ or for the superficial facilitation of business relations between companies A and B. I believe that such utilitarian goals should not be part of the motivation for undertaking a language major as a component of a bachelor’s degree. My experiences tutoring Mandarin left me wondering what it would take to get students with a Mandarin component of an Asian Studies major to want to attain a deeper level of proficiency in Mandarin and an understanding of Chinese culture. I have observed and tried to understand some of the issues causing the current situation in Chinese studies amongst beginner students.

There is a fallacy of relevance that exists amongst Chinese and non-Chinese people alike that ought to be dispelled: that Mandarin, or any other Chinese language, is ‘hard’ for a non-Asian person to learn. This fallacy has some evidently racist elements: a person’s skin colour has no bearing on his/her ability to learn any language. Rather it is the level of remoteness of relation between a person’s native language and the language he/she seeks to learn that most strongly influences whether the target language is initially ‘easy’ or ‘hard’. For example, Mandarin and English are very remote. The phonology and grammar are quite different and the writing systems are totally different. I am a native speaker of English, yet I have acquired some proficiency in Mandarin, which is far remoter from my mother tongue than, say, French. Yet the French language is much less intelligible to me than Mandarin, and when I hear people speaking French I often cannot even tell where words begin and end, whereas I do not have this difficulty with Mandarin and other Chinese languages such as Cantonese and Taiwanese. Hence, Chinese languages, even those which I cannot speak and do not understand,<a title="" href="#_ftn4">[4]</a> are clearer to me than a language more closely related to my native English. This goes against what I have observed to be people’s ‘common sense’, which holds that Asian languages like Mandarin are ‘hard’ and that European languages like French are ‘easy’. Ultimately, if a person wants to learn a foreign language well, he/she will put in the effort of studying, practicing and applying it in real life.

Speaking of ‘real life’, I can raise a pertinent example of when being able to understand classical Chinese was relevant to my own life in Taiwan. The brief exchange below occurred between my orchestra-mate and I whilst we were eating ‘lunchboxes’ (<i>biandang</i>便当) before a performance at the National Concert Hall of Taiwan in 2011:

Friend:             你看得懂这种中文吗？
Do you understand this kind of Chinese?

Me:                 我大概看得懂。
I basically do.

Friend:             看不懂没关系；我也看不懂。大部分台湾人都看不懂这种中文。
It’s okay if you don’t: I don’t understand it. Most Taiwanese people don’t understand this kind of Chinese.

The kind of Chinese that, according to my friend, most Taiwanese do not understand, was a maxim printed on the <i>biandang</i> chopstick packets pertaining to ‘having (literally: eating) the right kind of medicine’ (<i>shi dui yao</i>食对药).

I surmise several things about the classical Chinese on the chopstick packet. Firstly, the <i>biandang </i>shop owners decided to make chopstick packets with a quote written in classical Chinese – which most Taiwanese people apparently cannot understand – to manufacture an aura of learnedness in order to help their business stand out from its competitors, impressing current customers and attracting new ones. Secondly, the maxim pertained to health and wellbeing, implying that the <i>biandang </i>shop owners care about their customers’ health and that their product<i> </i>is healthy. This is a fallacy of equivocation. The <i>biandang </i>shop owners imply that their product is ‘the right kind of medicine’ even though <i>biandang </i>is not medicine. Without being able to understand classical Chinese I would not have been able to comprehend or even read what was written on the chopstick packet, let alone make any conclusions about the meaning it represented. Hence, the ‘usefulness of uselessness’ served me well on this occasion.

In conclusion, I have been frustrated by university students’ dismissal of classical Chinese as ‘irrelevant’ to their degrees, or just ‘too hard’ or ‘uninteresting’. But perhaps we are just coming from different perspectives, and maybe I am just eccentric in my appreciation of the ‘usefulness of uselessness’ (<i>wu yong zhi yong </i>无用之用) – in learning classical Chinese. Students just think there is no money in it. However, in the translation work that I have been undertaking, I regularly need to be able to translate from both Mandarin and classical Chinese into English. Therefore, financial benefits can accrue from being one of the few people to learn classical Chinese.

In recent years, the Australian government has been toting policies and predictions about the ‘Asian century’ and its intentions to increase ‘Asia literacy’ – whatever that means.<a title="" href="#_ftn5">[5]</a> However, as long as misguided attitudes persist about how ‘hard’ it is for non-Asian people to learn ‘difficult’ languages such as Mandarin, there can be no real ‘Asia literacy’ amongst Australians, or at least not as I understand the term; with a strong emphasis on language, writing and culture. I think that those of us involved in teaching tertiary Chinese need to do more to dispel the misguided notions that turn students off from studying different languages and cultures. Furthermore, we need to encourage students to embrace classical Chinese as a direct way of learning about Chinese philosophy, history, language and culture. My ruminations on a chopstick packet are a novel example of this ‘usefulness of uselessness’. As Zhuangzi 庄子suggests to his friend Huizi 惠子, who did not understand the value of useless things, it is perhaps not unlike the enjoyment taken in floating merrily downstream on a big useless gourd.<a title="" href="#_ftn6">[6]</a>
<div>
<div>

<a title="" href="#_ftnref2">[2]</a> Most undergraduate students taking Mandarin at ANU are doing so as part of a double major. Combined majors in Law &amp; Asian Studies (Chinese) and Economics &amp; Asian Studies (Chinese) are quite popular. However, I have observed that most such students tend to have a ‘primary major’, such as law, and a ‘secondary major’ that is less important to them, usually Chinese studies and Mandarin.

</div>
<div>

<a title="" href="#_ftnref3">[3]</a> Though I have also taught a number of students whose motivations for learning Mandarin were much less utilitarian.

</div>
<div>

<a title="" href="#_ftnref4">[4]</a> It is entirely possible for people to understand a language well but have limited proficiency in speaking it. Such is the case of Taiwanese (Southern Min) and Mandarin in Taiwan. On numerous occasions I have witnessed conversations between Taiwanese people where one party (usually an elderly person) speaks in Taiwanese and the other party (usually a younger person) speaks in Mandarin. Even though they speak in different languages, each party has a sufficient understanding of the language the other speaks such that effective communication can occur.

</div>
<div>

<a title="" href="#_ftnref5">[5]</a> For a scholarly inquiry into this vagueness, see Kirrilee Hughes’ forthcoming PhD thesis: <i>Australia's Asia Literacy: Knowledge to Serve What Ends?</i>,<i> </i>Australian National University, 2013.<i> </i>

</div>
<div>

<a title="" href="#_ftnref6">[6]</a>  This story occurs towards the end of the first chapter of <i>Zhuangzi</i>: 《庄子〈逍遥游〉》.

</div>
</div>