{
  "title": "ACYA Newsletter May 2013",
  "author": "ACYA",
  "date": "2013-05-14T20:03:16+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "image_13674925071291367492555",
      "type": "image",
      "src": "image_13674925071291367492555.jpg"
    },
    {
      "title": "ebulletin",
      "type": "image",
      "src": "ebulletin.jpg"
    }
  ]
}
<!--:en--><!-- // Begin Template Header \ -->
<!-- // End Template Header \ -->

<!-- // Begin Template Body \ -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="font-size: 18px; color: #ff0000; font-family: arial, helvetica, sans-serif; font-weight: bold; line-height: 22px; -webkit-text-size-adjust: auto;">Welcome</span>

Welcome to the very first bilingual edition of the ACYA Group monthly e-bulletin! A big thank you to our new resident Translator, Chelsea Zhou.

This month we would like to offer its sincere condolences and unreserved support to the people of Sichuan Province who have suffered unimaginable losses in the wake of the recent earthquake of Saturday, April 20.

It is devastatingly cruel that the people of Sichuan should have to endure yet another natural calamity so soon after the horrific events of 2008.

Our thoughts go out to the brave residents and rescue teams who continue to selflessly put themselves at risk in order to save others in Longmen, Lushan, Ya’an and other badly affected areas. We wish them all the best in these extremely trying times.

The ACYA Group team.

<hr />

<strong>NOTE:</strong> ACYA National has recently consolidated our electronic mailing lists. In the course of this necessary process, it is possible that this E-Bulletin has been sent to ACYA Members who have previously unsubscribed. The ACYA <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/29-australia-bites" target="_self">AustraliaBites</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/30-china-bites" target="_self">ChinaBites</a> mailing lists have also had to be consolidated, and so it is similarly possible that they will also be sent to ACYA Members who have previously unsubscribed. Note that unsubscribing from either AustraliaBites or ChinaBites will now cause you to be unsubscribed from the other publication also. Please accept our sincerest apologies for any inconvenience caused. We appreciate your support.

<strong>提醒</strong>：近期，ACYA针对我们的联系人电子邮件系统进行了强化。因此，一些在过去注销了收件人身份的ACYA成员可能也会收到这封时讯。针对<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/29-australia-bites" target="_self">AusraliaBites</a>和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/30-china-bites" target="_self">ChinaBites</a>的联系人系统的强化也将在近期展开。相应的，在过去注销了收件人身份的ACYA成员也可能会收到这些邮件。另外提醒您，现在，在注销AustraliaBits和ChinaBites的同时，您也将注销针对任何其他ACYA邮件的收件人身份。我们对此深表歉意，并非常感谢您的合作与支持。

</div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content \ -->

<!-- // Begin Module: Left Image with Content \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="font-family: tahoma,verdana,segoe,sans-serif;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">5 minutes with…</span></span></strong></span>

Anna Liao

ACYA@USyd China President
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<strong><img style="width: 191px; height: 179px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/anna_liao.jpg" width="191" height="179" align="left" />How did you get involved with Sino-Australian youth engagement / ACYA?</strong>

What got me intrigued in the first place is my family background. Growing up outside of China yet with a Chinese background has allowed me to see many fascinating aspects of the Chinese and the Australian culture. Because of my parent’s work, I had the opportunity to experience different cultures amongst different countries. It was not until during my JD degree that I came across ACYA’s USyd Chapter.  I was very impressed by ACYA’s devoted effort in helping international students from China adapt to and enjoy Australian culture while assisting Australian students to learn about and partake in Chinese culture.

<strong>What can we expect from the <em>“HOME”, Sydney an International City</em> project? How long will the project run, and when does it kick off?</strong>

The “HOME” project is designed to promote Sino-Australian youth engagement in two unique ways. First, the project provides enjoyable and meaningful activities and events for both the international student body and the local community. The project focuses on career development, education and awareness, and last but not least social and cross-culture engagement. It is organised by students, both international and local, who can obtain valuable experiences from the planning to the operational stage of the project, working together towards a common goal.

<strong>What exciting plans do you have up your sleeve for ACYA@USyd in 2013?</strong>

This year is an extremely exciting one for <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.USYD/?fref=ts" target="_self">ACYA@USyd</a>. We have over two hundred subscribers and a very active and passionate executive team. Each one of our executive is working hard to develop and enact attractive events to both promote the Australia-China relationship and provide services for the young Chinese and Australians who are interested in cross-culture engagement. Some of the upcoming events include an Aussie outback BBQ, language trade, international student careers resume workshop, interview skills workshop, careers hunting workshop, international exchange to China information session, and much more.

</div>
</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">"HOME", Sydney an International City</span></span></strong></span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/281-home-sydney-an-international-city"><img style="width: 600px; height: 135px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/home_sydney2.jpg" width="600" height="135" align="left" /></a>

This year <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.USYD/?fref=ts">ACYA@USyd</a>, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.UTS/?fref=ts">UTS</a>, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyaunsw/?fref=ts">UNSW</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyamq/?fref=ts">MQ</a> are collaborating with the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityofsydney.nsw.gov.au/">City of Sydney</a> and many other prominent sponsors to present <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-annsouncements/281-home-sydney-an-international-city">“HOME”, Sydney an International City</a>. The project provides a meaningful platform to engage the local and international community in Sydney, in a day of fun events and activities created by young people, for young people. Events will include social mixers, an educational roundtable, a Q&amp;A on topical issues, keynote speakers, career opportunities in China, tailored workshops and cultural displays of food, art and fashion.

<strong>The recruitment process for “HOME” is now closed.</strong> We would like to thank everyone for the support our project has received and for the overwhelming volume of applications.

If you have missed out this opportunity but would like to get in touch to talk about your ideas, possible involvement, or anything else, please email <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:careers@acya.org.au" target="_blank">careers@acya.org.au</a>. We would love to hear from you!

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Austrade-ACYA Taipei Internship Opportunity</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/featured-opportunities/360-australian-office-taipei-internship-opportunity-2013"><img style="width: 190px; height: 125px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/austrade2.jpg" width="190" height="125" align="right" /></a>The <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.australia.org.tw/tpei/home.html">Australian Office Taipei</a>, Commercial Section, is seeking applications from Australian citizens for a 2013 summer internship in Taipei. The internship will run from June to September (four months). Start and end dates are flexible. The intern will be hired for a period of 12 - 16 weeks, working a five-day week on a full-time basis.

Applications must include the following:

1. Copy of candidate’s resume

2. An outline of why you would like to work as an intern for Austrade in Taiwan (strictly no more than one page clearly written in Arial, 12 point)

3. Advice of your preferred start and end dates

Please send your application to: <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:careers@acya.org.au">careers@acya.org.au</a> by <strong>COB Friday 10 May</strong>. For more information, please click <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/featured-opportunities/360-australian-office-taipei-internship-opportunity-2013" target="_self">here</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; display: block; font-family: arial; font-weight: bold; margin: 0px 0px 10px; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-size: large;">ACYA's Victoria Kung at the Schwarzman Scholarship Tsinghua launch</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/359-schwarzman-scholarships"><img style="width: 260px; height: 217px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/vicky_rudd.jpg" width="260" height="217" align="left" /></a>ACYA China President, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://cn.linkedin.com/pub/victoria-kung/31/8b2/810">Victoria Kung</a>, was recently invited to attend the announcement ceremony for the Tsinghua University <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/359-schwarzman-scholarships">Schwarzman Scholarship Program</a>.

After the ceremony, Ms Kung was introduced to Former Prime Minister and Foreign Minister <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.kevinruddmp.com/">Kevin Rudd</a> by the Australian Ambassador to China, Ms <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.dfat.gov.au/homs/cn.html">Frances Adamson</a>. Ambassador Adamson introduced Ms Kung as a representative of the “future leaders of this generation”. Mr Rudd expressed his support for ACYA and congratulated us on our achievements in the Australia-China youth space.

Check out the full media release <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/images/Documents/ACYA%20Media%20Release%20-%20Schwarzman%20Scholarship.pdf">here</a>!

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Australia-China Young Professionals Initiative</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/2013/04/25/canberra-investing-in-the-asian-century/"><img style="width: 250px; height: 250px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Scott_Gardiner_300x300.jpg" width="250" height="250" align="right" /></a>As the premier platform for young professionals interested in the Sino-Australian relationship, ACYPI is continually looking for people interested in developing the Australian-China landscape. The Perth, Beijing and Shanghai chapters are now taking expressions of interest. Please contact <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:enquiries@acypi.org.au" target="_blank">enquiries@acypi.org.au</a> with a short personal description if you’re interested.

The newly formed <strong>ACYPI Perth</strong> chapter looks forward to hosting its inaugural event in late May. <strong>ACYPI Shanghai</strong> will also be holding an event on 22 May in collaboration with Austcham Shanghai and ACAA. Please visit <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/" target="_blank">www.acypi.org.au</a> for further details for both cities’ events in coming weeks.

<strong><em>From Mao suits to Metersbonwe, luxury bling and cash register Ka'ching! A presentation and panel discussion on fashion in China</em></strong>

<strong>ACYPI</strong> <strong>Beijing</strong> is holding its first event for 2013 early this month. China is fast becoming the biggest fashion story of the century and saviour to an industry ailing in many other parts of the world. Daily news, blogs and other media are rife with information about every Coco, Louis, Ralph, Hugo and Giorgio trying to get their sewing needles into Chinese wallets, yet there is a lack of accurate coverage from within China.

<strong>Time &amp; Date: 7-9:30pm, 9 May 2013</strong>

Venue: <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityweekend.com.cn/beijing/listings/nightlife/bars/has/mesh/">Mesh</a>, Opposite House, San Li Tun Village, Beijing

<strong>RSVP:</strong> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acypibj.eventbrite.com/" target="_blank">http://acypibj.eventbrite.com/</a>

<strong style="line-height: 150%;"><em>Investing in the Asian Century</em></strong>

On 1 May, <strong>ACYPI Canberra</strong> collaborated with <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.mallesons.com/">King &amp; Wood Mallesons</a> (KWM) to welcome guest speaker <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.mallesons.com/people/sgardin">Scott Gardiner</a>, Head of KWM’s China Outbound Investments team and Managing Partner of Energy Resources and Projects (China). Scott discussed the challenges of doing business in China and relayed his personal insights into how Australian businesses can overcome cultural barriers to succeeding in Chinese markets. He also reviewed the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://asiancentury.dpmc.gov.au/" target="_self">Australia in the Asian Century white paper</a> and debated with attendees the main issues for Australian trade policy. ACYPI thanks Scott and King &amp; Wood Mallesons for helping to coordinate such a successful evening.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ACYA@HK out &amp; about</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyahk/?fref=ts">ACYA@HK</a> is hosting a Wine Tasting Event at the beautiful <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nathanhotel.com/en/dinner_cellar.html">Nathan Cellar</a> on <strong>Friday, 10 May from 9pm</strong> until late! Whether you enjoy a glass of red or white, join us for the wine buffet. If you don’t like wine, there are always discussions about Australia-China relations to keep you going!

Cost: $188HKD + 10% (Wine Buffet/All you can drink wine). Payment to be made at the venue.

To RSVP/find out more, click <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/events/162213953939120/">here</a>.

<hr />

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyahk/?fref=ts"><img style="width: 240px; height: 217px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/hk_boat.jpg" width="240" height="217" align="left" /></a>ACYA@HK’s inaugural junk boat ride was a sweeping success. Celebrated on Saturday 27 April, we had a large turnout for a day of fun, food and plenty of the expected boating madness!

Leaving from Central Pier we sailed around the harbour and made our way to various spots on <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://en.wikipedia.org/wiki/Lamma_Island">Lamma Island</a>. Swimming, relaxing and swapping stories was the order of the day, with a catered lunch enjoyed on board the boat. There was plenty of time for everyone to get to know each other as well as allowing some to show off their diving skills from the top deck of the boat. A great time was had by all!

Students from the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.hku.hk/">University of Hong Kong</a>, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.polyu.edu.hk/cpa/polyu/index.php">Hong Kong Polytechnic University</a> and a number of others from around town participated in this event, marking a great day for the young ACYA@HK chapter.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><b>Weibo Watcher looking for Contributors to join the family</b></span>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.weibowatcher.com/"><img style="width: 213px; height: 145px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Become_a_weibo_watcher1.jpg" width="213" height="145" align="right" /></a>Weibo Watcher is a newly formed internet forum that aims to translate the hottest and most interesting Weibo content into English so that the English speaking world can learn what their Chinese counterparts are talking about and thinking! Weibo Watcher does not aim to editorialise or insert the opinion of the contributor into any content - only to translate the original post and some of the most interesting comments - after that it is up to our viewers to add their two cents to the conversation.

Weibo Watcher is hoping to build a team of international contributors to join the current family in creating story content for the site. This is a volunteer, freelance position. If you have some spare time and want to contribute, we'd love to have your content! If you're busy for a while, you won't hear us complaining!

If you'd like to learn more visit <a style="color: #336699; font-weight: normal; text-decoration: underline;" title="www.weibowatcher.com" href="http://www.weibowatcher.com/">www.weibowatcher.com</a> or email <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:applications@weibowatcher.com">applications@weibowatcher.com</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; font-weight: bold; display: block; margin: 0px 0px 10px; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ACYA@UQ - Entrepreneurship in China</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/uqresearchers/researcher/xud.html"><img style="width: 180px; height: 236px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/drxu.jpg" width="180" height="236" align="left" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/acya.uq?fref=ts" target="_self">ACYA@UQ</a> was honoured to have <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/uqresearchers/researcher/xud.html">Dr. Dongming Xu</a>, Senior Lecturer in Business Information Systems at the UQ Business School speak on the 3rd of May about entrepreneurship and entrepreneurs in China. Though much credit is given to the government, infrastructure developments and foreign investment for China's economic success, the contributions of private sector development can often be overlooked. Dr. Xu recounted the success stories of <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://news.alibaba.com/specials/aboutalibaba/aligroup/index.html">Alibaba</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.baidu.com/">Baidu</a> as examples of China’s entrepreneurial success, as well as a summary of the current state of entrepreneurship in China. The event was sponsored by the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/confucius/">Confucius Institute UQ</a> and forms the first of a series of similar Guest Speaker events for 2013.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; text-align: left; margin: 0px 0px 10px; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ACYA makes positive contribution to City of Sydney's Living Harmony festival</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/361-chinese-cultural-days-2013"><img style="width: 250px; height: 225px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/cityofsydharmony.jpg" width="250" height="225" align="right" /></a>This year, ACYA sat on the Steering Committee for <em><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/361-chinese-cultural-days-2013" target="_self">Chinese Cultural Days 2013</a></em>, an annual two day festival that forms part of City of Sydney's <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://whatson.cityofsydney.nsw.gov.au/events/16715-living-in-harmony-festival-2013">Living in Harmony</a>. As the only youth-oriented association on the committee, over 40 ACYA volunteers made a very positive contribution to the festival and continued ACYA’s proud tradition of pioneering Sino-Australian community engagement. The City of Sydney issued a Certificate of Appreciation in recognition of ACYA’s stellar involvement.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;">ACYA in the media!</span></span></span></strong></h4>
It’s been a huge month for ACYA in the media, with quotes from ACYA Australia President <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/images/Profiles/Thomas%20Williams.pdf" target="_self">Tom Williams</a> on <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nzweek.com/business/news-analysis-gillard-wins-praises-for-china-trip-59906/">NZ Week</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://news.xinhuanet.com/english/indepth/2013-04/16/c_132313403.htm">Xinhua</a> surrounding Prime Minister Gillard’s recent trip to China, and a profile of ACYA@HK co-founder Elise Giles in the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.frasercoastchronicle.com.au/news/monto-girl-east-west-together/1846744/">Fraser Coast Chronicle</a>.

To see ACYA’s media release on the PM’s recent China visit, visit the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/355-julia-gillard-china-visit">ACYA website</a>.

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nzweek.com/business/news-analysis-gillard-wins-praises-for-china-trip-59906/"><img style="width: 230px; height: 90px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/nzweek_logo.png" width="230" height="90" align="left" /></a>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 22px;"><b>Job Opportunities - World of Suzie Wong's</b></span></span>

Interested in working in one of Beijing’s most prestigious and lavish clubs? Want to gain work experience in a Chinese hospitality work environment? The <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.clubsuziewong.com/">World of Suzie Wong’s Club</a> in Beijing is now looking for part-time and full-time foreign staff to cater to their foreign clientele. Positions available include, but are not limited to, bartender, bar manager, doorman/door hostess, and sales and marketing. A great opportunity for improving Mandarin language skills, and greater immersion in Chinese culture!

If interested please contact David Han on +86 1361 139 0948 or <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:mail@clubsuziewong.com">mail@clubsuziewong.com</a>. For more information, please visit the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/employers-profiles">ACYA website</a>.

<strong>Please note you must be currently living in Beijing to apply.</strong>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-weight: bold; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Applications now being accepted - German Chancellor Fellowship</span></h4>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/german-chancellor-fellowship.html"><img style="width: 203px; height: 109px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/german_chancellor.jpg" width="203" height="109" align="left" /></a>The <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/home.html">Alexander von Humboldt Foundation</a>'s German Chancellor Fellowship Program is for university graduates from the US, Russia, China, Brazil and India with an interest in international issues and demonstrated leadership potential. It provides fellows the opportunity to spend one year in Germany, where they will network with other prospective leaders from abroad and explore new solutions to the global issues of our times. Fellows also conduct independent projects at their host institutions.

For eligibility criteria and how to apply, please <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/german-chancellor-fellowship.html">click here</a>. Applications are due by <strong>15 September 2013</strong>.

</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Missed a previous issue of ACYA e-bulletin?</span></h4>
Look no further! Check out our archive of past editions on the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/e-bulletin">ACYA website</a>.

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="color: #ff0000;"><span style="font-size: 18px;"><strong><span style="font-size: 24px;">Connect with ACYA Group / 联系我们</span></strong></span></span>

Twitter:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya"><img style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" width="75" height="73" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya">@a_cya</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://twitter.com/EngagingChina">@EngagingChina</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/ACYDialogue">@ACYDialogue</a>

Facebook:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/"><img style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" width="75" height="75" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">ACYA National</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/EngagingChinaProject?fref=ts">Engaging China Project</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts">ACYD</a>

LinkedIn:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-association"><img style="height: 75px; line-height: 16px; color: #0000ee; width: 75px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/LinkedIn_logo.png" width="75" height="75" align="right" /></a>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-association" target="_self">ACYA</a></div>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/groups/AustraliaChina-Young-Professionals-Initiative-ACYPI-3744368" target="_self">ACYPI</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-dialogue" target="_self">ACYD</a>

Chinese Social Media:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://e.weibo.com/acya"><img style="width: 75px; height: 60px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/weibo.png" width="75" height="60" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://e.weibo.com/acya" target="_self">Weibo</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://page.renren.com/600745607?checked=true" target="_self">Renren</a>

</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content \ -->

<!-- // Begin Module: Right Image with Content \ -->

<!-- // End Module: Right Image with Content \ --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body \ -->

<!-- // Begin Template Footer \ -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/#!/a_cya">follow on Twitter</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">friend on Facebook</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&amp;id=213c903a93&amp;e=b9ce0f9afe">forward to a friend</a></div></td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://transasialawyers.com/" target="_blank"><img style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="TransAsia Lawyers" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" width="250" height="122" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://chinainstitute.anu.edu.au/" target="_blank"><img style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" width="250" height="85" align="none" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.crccasia.com/"><img style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" width="250" height="175" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityofsydney.nsw.gov.au/"><img style="width: 250px; height: 84px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/home_sydney.png" width="250" height="84" align="none" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.austcham.org/"><img style="width: 180px; height: 79px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/austchamlogo_1_.jpg" width="180" height="79" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.austchinaalumni.org/"><img style="width: 217px; height: 180px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACAA_logo_optimised_for_the.jpg" width="217" height="180" align="none" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"></div>
</div></td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">

<span style="color: #2f4f4f;">Header photograph - A purveyor of sauces, by <strong>Nicola Boyle.</strong></span>

</div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">

<span style="color: #2f4f4f;"><em>Copyright © 2013 ACYA, All rights reserved.</em></span>

</div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;">

<span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&amp;utm_medium=email&amp;utm_campaign=monkey_rewards&amp;aid=db2f0ea3a68f632de3088eada&amp;afl=1"><img title="MailChimp Email Marketing" alt="Email Marketing Powered by MailChimp" src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" /></a> </span></span></span>

</div>
</div></td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe&amp;c=213c903a93">unsubscribe from this list</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&amp;id=6e67c7a2c2&amp;e=b9ce0f9afe">update subscription preferences</a></div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer \ --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer \ --><!--:--><!--:zh--> 

<!-- // End Template Header \ -->

<!-- // Begin Template Body \ -->
<table id="templateBody" width="600" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="bodyContent" style="border-collapse: collapse; background-color: #ffffff;" valign="top"><!-- // Begin Module: Standard Content \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<strong style="line-height: 150%;">欢迎您！</strong>
欢迎您开始阅读澳中青年联合组织(ACYA Group)的第一期双语版时讯！多谢我们ACYA的新官方时讯翻译，周杨。</div>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<span style="line-height: 150%;">在本月时讯的开篇，澳中青年联合组织向在2013年4月20日四川省芦山地震中遇难的群众和抢险救灾中牺牲的战士表示深切哀悼。ACYA组织将全力支持和帮助受灾群众战胜自然灾害，重建美好家园。</span></div>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
四川省在经历了2008年汶川地震后，又再次遭受此次芦山地震，ACYA组织在此对龙门、芦山、雅安及其他受灾地区中勇敢的群众和救援人员表示真诚的同情、慰问和支持。在这最艰难的时刻中，我们祝福他们平安、顺利。
祝大家天天开心,
The ACYA Group team.

<hr />

<strong></strong>
<strong>提醒</strong>：近期，ACYA针对我们的联系人电子邮件系统进行了强化。因此，一些在过去注销了收件人身份的ACYA成员可能也会收到这封时讯。针对<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/29-australia-bites" target="_self">AusraliaBites</a>和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/30-china-bites" target="_self">ChinaBites</a>的联系人系统的强化也将在近期展开。相应的，在过去注销了收件人身份的ACYA成员也可能会收到这些邮件。另外提醒您，现在，在注销AustraliaBits和ChinaBites的同时，您也将注销针对任何其他ACYA邮件的收件人身份。我们对此深表歉意，并非常感谢您的合作与支持。

</div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Content \ -->

<!-- // Begin Module: Left Image with Content \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><span style="font-family: tahoma,verdana,segoe,sans-serif;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">
</span></span></strong><strong style="line-height: 150%;"><span style="color: #ff0000;"><span style="font-size: 18px;">5分钟了解...</span></span></strong><strong><span style="color: #ff0000;"><span style="font-size: 18px;">

Anna Liao
</span></span></strong><span style="font-family: tahoma,verdana,segoe,sans-serif;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;">ACYA悉尼大学分会</span></span><span style="color: #ff0000;"><span style="font-size: 18px;">主</span></span></strong></span><span style="color: #ff0000;"><strong><span style="font-size: 18px;">席(中国)</span></strong></span><strong><span style="color: #ff0000;"><span style="font-size: 18px;">
</span></span></strong></span>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><em id="__mceDel"> <strong><img style="width: 191px; height: 179px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/anna_liao.jpg" width="191" height="179" align="left" /></strong>


<strong style="line-height: 150%;">你是如何成为澳中青年联合会的成员的？</strong>

我的家庭背景是让我对澳中青年关系感兴趣的第一原因。我并不在中国长大，但我却又拥有中国文化的背景，这让我可以从与众不同的方面了解澳中两国的文化。由于我父母的工作，我在成长中有许多机会去体验不同国家的不同文化。我是在攻读法律学士学位的时候才与悉尼大学的ACYA分会结缘的。ACYA的工作给我留下了深刻的印象：ACYA致力于帮助来自中国的国际学生适应在澳大利亚的生活、并帮助他们融入、享受澳大利亚的文化；同时ACYA也协助澳大利亚的学生了解和亲身体验中国的文化。</em></div>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><em id="__mceDel">
<strong>可以向我们介绍一</strong><strong>下</strong><strong>“</strong><strong><em>HOME”, Sydney an International City</em></strong><strong>这个计划吗？它是如何运作的？将从什么时候开始实行？</strong>

“Home”计划旨在从两个方面促进澳中青年关系。首先，“Home”计划将在国际学生团体和当地社区中展开一系列有趣又有意义的活动。“Home”计划专注于职业发展、教育和交流意识，同时也关注社会与跨文化的交流。“Home”计划的组织者既有国际学生，也有澳大利亚本地的学生，他们可以从项目活动的计划和实行中获取宝贵的经验，同时可以为了相同的目标紧密合作、共同努力。

<strong>你为</strong><strong>ACYA</strong><strong>悉尼大学分会的</strong><strong>2013</strong><strong>年准备了什么特别的惊喜？</strong>

2013年对于<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.USYD/?fref=ts" target="_self">ACYA悉尼大学分会</a>来说，将会是非常激动人心的一年。我们拥有超过200个成员和一个充满了激情与活力的管理团队。管理团队中的每一个成员都在为开展富有吸引力的活动而努力工作着，我们希望能够更好的促进澳中青年关系、并向两国对跨文化交流有兴趣的青年提供更好的服务与协助。这一年我们将开展的活动包括以下几个：澳洲烧烤会、求职研讨会、中国交换生计划介绍会等等，还有许多其他活动。</em></div>
</div>
 

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="font-size: 18px;"><strong><span style="color: #ff0000;"><span style="font-family: arial, helvetica, sans-serif;">"HOME", Sydney an International City</span></span></strong></span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/281-home-sydney-an-international-city"><img style="width: 600px; height: 135px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/home_sydney2.jpg" width="600" height="135" align="left" /></a>















This year <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.USYD/?fref=ts">ACYA@USyd</a>, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.UTS/?fref=ts">UTS</a>, <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyaunsw/?fref=ts">UNSW</a> and <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyamq/?fref=ts">MQ</a> are collaborating with the <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityofsydney.nsw.gov.au/">City of Sydney</a> and many other prominent sponsors to present <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-annsouncements/281-home-sydney-an-international-city">“HOME”, Sydney an International City</a>. The project provides a meaningful platform to engage the local and international community in Sydney, in a day of fun events and activities created by young people, for young people. Events will include social mixers, an educational roundtable, a Q&A on topical issues, keynote speakers, career opportunities in China, tailored workshops and cultural displays of food, art and fashion.



<strong>The recruitment process for “HOME” is now closed.</strong> We would like to thank everyone for the support our project has received and for the overwhelming volume of applications.

If you have missed out this opportunity but would like to get in touch to talk about your ideas, possible involvement, or anything else, please email <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:careers@acya.org.au" target="_blank">careers@acya.org.au</a>. We would love to hear from you!



今年，<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.USYD/?fref=ts">ACYA悉尼大学分会</a> 、<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/ACYA.UTS/?fref=ts">悉尼科技大学</a> 、<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyaunsw/?fref=ts">新南威尔士大学</a> 和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyamq/?fref=ts">麦考瑞大学</a>将和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityofsydney.nsw.gov.au/">悉尼市</a>及一系列赞助方合作，开展一项名为<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-annsouncements/281-home-sydney-an-international-city">“HOME”, Sydney an International City</a>的计划。该计划将为在悉尼的本地和国际团体提供一个促进交流的平台，年轻人将在其中自己策划和实行一系列针对年轻人自身的、充满趣味的活动与项目。这些活动与项目包括社交活动、教育圆桌论坛、热点话题问答会、名人演讲、中国求职机会研讨、定制研讨会和食物、艺术、时尚文化展览会等等。



<strong>“HOME”</strong><strong>计划的报名程序至今已经截止。</strong>我们收到了大量的报名申请，在此向大家对本计划的支持表示感谢。如果您错过了本次报名的机会，但希望能继续和我们保持联系并与我们交流您的创意或任何信息，请发电子邮件至<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:careers@acya.org.au" target="_blank">careers@acya.org.au</a>。我们十分期待和您联络！

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="color: #202020; display: block; font-family: Arial; font-size: 22px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">Austrade-ACYA Taipei Internship Opportunity</span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/featured-opportunities/360-australian-office-taipei-internship-opportunity-2013"><img style="width: 190px; height: 125px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/austrade2.jpg" width="190" height="125" align="right" /></a><strong>澳大利</strong><strong>亚贸</strong><strong>易委</strong><strong>员</strong><strong>会台北</strong><strong>实习</strong><strong>生</strong><strong>计划</strong>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.australia.org.tw/tpei/home.html"><strong>澳洲台北办事处</strong></a> 的商业部门现正寻找一名能够在2013年夏天于台北担任实习生工作的澳大利亚公民。这一实习工作将从2013年6月开始，到2013年9月结束，历时共4个月。实习的开始与结束日期可调整。实习生的雇用时间为12到16周，一周的工作时间为5天全勤。

申请者必须提交以下信息：
<ol>
	<li><span style="line-height: 150%;">申请者的简历</span></li>
	<li><span style="line-height: 150%;">简单陈述为何您希望成为澳大利亚贸易委员会在台北的实习生（篇幅必须限制在一页纸内，格式请采用12号大小的Arial字体）</span></li>
	<li><span style="line-height: 150%;">您希望的实习开始与结束日期</span></li>
</ol>
请在<strong>5月10日星</strong>期五之前将您的申请发送至：<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:careers@acya.org.au" target="_blank">careers@acya.org.au</a>。如需更多信息，请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/featured-opportunities/360-australian-office-taipei-internship-opportunity-2013">这里</a>。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; display: block; font-family: arial; font-weight: bold; margin: 0px 0px 10px; color: #202020; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong>ACYA</strong><strong>成员</strong><strong>Victoria Kung</strong><strong>参加清华大学苏世民学者项目启动仪式</strong></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/359-schwarzman-scholarships"><img style="width: 260px; height: 217px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/vicky_rudd.jpg" width="260" height="217" align="left" /></a><span style="line-height: 150%;">ACYA中国地区主席</span><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://cn.linkedin.com/pub/victoria-kung/31/8b2/810">Victoria Kung</a><span style="line-height: 150%;"> 近日受邀参加了清华大学</span><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/359-schwarzman-scholarships">苏世民学者项目</a><span style="line-height: 150%;">启动仪式。</span>
启动仪式后，澳大利亚驻中国大使<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.dfat.gov.au/homs/cn.html">Frances Adamson</a>向澳大利亚前任总理及外交部长<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.kevinruddmp.com/">陆克文</a>介绍了Kung。Adamson大使将Kung介绍为“青年一代的未来领袖”。陆克文先生向ACYA表达了他的支持，并对我们在澳中青年交流领域获得的成功表示了祝贺。

更多相关媒体报道请看<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/images/Documents/ACYA%20Media%20Release%20-%20Schwarzman%20Scholarship.pdf">这里</a>。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong>中澳青年精英领袖团</strong><strong>(ACYPI)</strong></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/2013/04/25/canberra-investing-in-the-asian-century/"><img style="width: 250px; height: 250px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Scott_Gardiner_300x300.jpg" width="250" height="250" align="right" /></a><span style="line-height: 150%;">作为一个为 青年提供交流平台的领袖组织，ACYPI一直在寻找对促进澳中关系感兴趣的成员。ACYPI的珀斯、北京和上海分会现正有空缺职位。如果您对我们感兴趣，请联系</span><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:enquiries@acypi.org.au" target="_blank">enquiries@acypi.org.au</a><span style="line-height: 150%;">并提供一份简短的个人简介。</span>

新成立的ACYPI珀斯分会将会在五月下旬组办启动仪式。ACYPI上海分会将会 与Austcham上海和澳中同学联谊会合作，在5月22日举行活动。请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/" target="_blank">www.acypi.org.au</a>了解接下来几周两个城市分会活动的更多信息。

<strong>从中山装到美特斯邦威、奢侈品之光和源源不断的钞票！关于时尚在中国的演讲和讨论会</strong>

<strong>ACYPI</strong><strong>北京分会</strong>将在本月初举办2013年第一次的活动。中国正在快速的成长为本世纪最大的时尚消费国度，并成为了时尚这一与世界各地紧密联系的产业的拯救者。每日新闻、博客和媒体上充斥着各种关于香奈儿、路易威登、拉尔夫劳伦、雨果和阿玛尼的信息，品牌设计师们都忙着将他们缝制衣裳的针也戳进中国人的钱包里。但在外国时尚信息不断进入中国的同时，中国内部对时尚态度的信息却甚少准确的被报道和了解。

<strong>时间：</strong><strong>2013</strong><strong>年</strong><strong>5</strong><strong>月</strong><strong>9</strong><strong>日晚</strong><strong>7</strong><strong>点到</strong><strong>9</strong><strong>点半</strong>

<strong>地点：北京市三里屯</strong><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityweekend.com.cn/beijing/listings/nightlife/bars/has/mesh/"><strong>Mesh</strong></a><strong>, Opposite House</strong>

<strong>RSVP:</strong> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acypibj.eventbrite.com/" target="_blank">http://acypibj.eventbrite.com/</a>

<strong>在亚洲世纪投资</strong>

5月1日，<strong>ACYPI堪培拉分会</strong>与<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.mallesons.com/">King & Wood Mallesons</a> (KWM)一起欢迎了KWM中国投资团队主管、中国能源计划管理合伙人<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.mallesons.com/people/sgardin">Scott Gardiner</a>作为客座嘉宾进行演讲。Scott讨论了在中国开展商业活动的挑战，并分享了他个人对于澳大利亚商业机构如果克服文化壁垒、在中国市场获得成功的观点。他还对<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://asiancentury.dpmc.gov.au/sites/default/files/white-paper/translations/pm_press_release_aacwp_chinese.pdf" target="_self">《亚洲世纪中的澳大利亚》白皮书</a>进行了评价，并与在场听众针对澳大利亚贸易政策进行了辩论。ACYPI对Scott和King & Wood Mellesons帮助此次活动成功做出的贡献表示感谢。

请关注我们的<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acypi.org.au/">网站</a>和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/groups/AustraliaChina-Young-Professionals-Initiative-ACYPI-3744368">LinkedIn账号</a>以持续了解ACYPI的一切动向。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;">ACYA@HK out & about</span></h4>
<strong>ACYA</strong><strong>香港分会最新资讯</strong>

ACYA香港分会将会在五月10日星期五于迷人的<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nathanhotel.com/en/dinner_cellar.html">弥敦酒窖</a>举办品酒会。品酒会将从晚上9点开始，一直持续到夜间！无论您想要尝的那杯的是红葡萄酒还是白葡萄酒，都请加入我们的品酒自助餐会！即便您对品酒不甚感兴趣，我们也将提供您讨论澳中关系的机会，让您的晚上充满乐趣！

预算：$188HKD + 10% (包括品酒自助餐和无限量酒水)。请在品酒会现场付款。RSVP和了解更多信息，请看<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/events/162213953939120/">这里</a>。

<hr />

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/groups/acyahk/?fref=ts"><img style="width: 240px; height: 217px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/hk_boat.jpg" width="240" height="217" align="left" /></a><span style="line-height: 150%;">ACYA香港分会于4月27日星期六举办的中国帆船活动取得了巨大的成功。我们在美食、帆船和其他各种快乐的活动中渡过了充满乐趣的一天！</span>

从中环码头出发，我们乘坐帆船沿港口航行至<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://en.wikipedia.org/wiki/Lamma_Island">南丫岛</a>上的各个景点。当天的主题活动包括游泳、放松和交换有趣的故事。在船上我们还享用了丰盛的现场烹调午餐。每一位参与者都拥有充足的时间去了解其他人，有一些成员甚至还利用帆船的顶层甲板向大家展示了自己的跳水技巧。每个人都渡过了最愉快的时间！

来自<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.hku.hk/">香港大学</a>、<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.polyu.edu.hk/cpa/polyu/index.php">香港理工大学</a>和香港其他学校的学生们参与了本次活动，让ACYA香港分会的这一天成为了青年的节日。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><strong>Weibo Watcher</strong><strong>寻找新成员</strong>



<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.weibowatcher.com/"><img style="width: 213px; height: 145px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/Become_a_weibo_watcher1.jpg" width="213" height="145" align="right" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.weibowatcher.com/">Weibo Watcher</a> 是一个新成立的在线论坛，旨在将微博上最新鲜和热门的内容翻译成英语，让英语读者了解当下在中国人们感兴趣的话题。Weibo Watcher并不针对微博进行编辑或加入任何观点——仅仅对原始微博的内容和对此内容的在线有趣评论进行翻译——对这些微博的评价将由后续的读者们做出。

Weibo Watcher希望来自世界各地的成员能够加入现有的团队，并帮助丰富网站已有的内容。这将是一个志愿者性质的工作，成员可自由选择工作时间和地点。如果您有空余的时间并愿意加入我们，我们将十分期待与您合作！即便您现下没有时间帮助我们，我们也绝不会对您有意见！

如果想了解更多信息，请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" title="www.weibowatcher.com" href="http://www.weibowatcher.com/">www.weibowatcher.com</a>或发送电子邮件至<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:applications@weibowatcher.com">applications@weibowatcher.com</a>。</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; font-weight: bold; display: block; margin: 0px 0px 10px; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>ACYA</strong><strong>昆士兰大学分会</strong><strong>——</strong><strong>中国创业</strong></span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/uqresearchers/researcher/xud.html"><img style="width: 180px; height: 236px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/drxu.jpg" width="180" height="236" align="left" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://www.facebook.com/acya.uq?fref=ts">ACYA昆士兰大学分会</a>荣幸地邀请到昆士兰大学商学院商业信息中心的高级讲师<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/uqresearchers/researcher/xud.html">Dongming Xu博士</a>，在5月3日就中国的创业和创业者进行了演讲。尽管中国的政府、基础设施建设和外国投资为中国的经济成功贡献了大部分的力量，私人领域的贡献也不该被忽视。Xu博士引用了<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://news.alibaba.com/specials/aboutalibaba/aligroup/index.html">阿里巴巴</a>和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.baidu.com/">百度</a>的成功作为中国创业成功的例子，并对目前中国的创业进行了总结。本次活动引领了2013年将开展的一系列客座嘉宾演讲活动，由<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.uq.edu.au/confucius/">昆士兰大学孔子学院</a>赞助。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; text-align: left; margin: 0px 0px 10px; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>ACYA</strong><strong>为悉尼市和谐生活节做出贡</strong><strong>献</strong></span></h4>
<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/361-chinese-cultural-days-2013"><img style="width: 250px; height: 225px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/cityofsydharmony.jpg" width="250" height="225" align="right" /></a> 今年，ACYA成为了悉尼<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/361-chinese-cultural-days-2013" target="_blank">2013年中国文化节</a>筹备委员会的成员。中国文化节每年举行一次，一次历时两天，是悉尼市<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://whatson.cityofsydney.nsw.gov.au/events/16715-living-in-harmony-festival-2013" target="_blank">和谐生活节</a>的重要组成部分。ACYA作为筹备委员会中唯一一个青年联合组织，向该节日提供了超过40名ACYA志愿者，延续了ACYA引以为豪的传统：不断促进青年在澳中社区中的参与度与交流活动。悉尼市对ACYA的参与和协助表示认可，并向我们颁发了感谢证书。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-family: arial; color: #202020; font-size: 22px; font-weight: bold; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><strong><span style="color: #ff0000;"><span style="font-size: 18px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>媒体中的</strong><strong>ACYA</strong><strong>！</strong></span></span></span></strong></h4>
本月对于ACYA来说，是与媒体紧密相关的重要一月。ACYA澳大利亚主席<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/images/Profiles/Thomas%20Williams.pdf" target="_self">卫涛</a>关于澳大利亚总理吉拉德近日访华活动的评论被<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nzweek.com/business/news-analysis-gillard-wins-praises-for-china-trip-59906/">新西兰周报</a>和<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://news.xinhuanet.com/english/indepth/2013-04/16/c_132313403.htm">新华社</a>在报道中引用，<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.frasercoastchronicle.com.au/news/monto-girl-east-west-together/1846744/">弗雷泽海岸记事报</a>则刊载了ACYA香港分会创立人Elise Giles的介绍。

了解更多ACYA关于吉拉德总理访华的媒体报道，请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/p2p/news-announcements/355-julia-gillard-china-visit">ACYA网站</a>。

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.nzweek.com/business/news-analysis-gillard-wins-praises-for-china-trip-59906/"><img style="width: 230px; height: 90px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/nzweek_logo.png" width="230" height="90" align="left" /></a>

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">

<span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 22px;"><b><strong>工作机会</strong><strong>——</strong><strong>苏丝黄的世界</strong></b></span></span>

有兴趣在北京最高级奢华的会员工作吗？希望在中国友好的工作环境中获得宝贵经验吗？北京<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.clubsuziewong.com/">苏丝黄的世界</a>会所现正寻找兼职与全职的外籍员工 。职位包括但不仅限于以下：酒保、酒吧经理、迎宾、市场营销。将提供极佳的中文练习机会并可以帮助您更好的了解中国文化！

如果您有兴趣，请联系David Han, 电话号码+86 1361 139 0948，电子邮箱<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="mailto:mail@clubsuziewong.com">mail@clubsuziewong.com</a>。了解更多信息请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/careers/employers-profiles">ACYA网站</a> 。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="text-align: left; line-height: 100%; margin: 0px 0px 10px; display: block; font-weight: bold; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>报名申请现已开始</strong><strong>——</strong><strong>联邦德国总理奖学金项目</strong></span></h4>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/german-chancellor-fellowship.html"><img style="width: 203px; height: 109px; line-height: 100%; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/german_chancellor.jpg" width="203" height="109" align="left" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/home.html">Alexander von Humboldt 基金会</a>的联邦德国总理奖学金项目主要面向美国、俄罗斯、中国、巴西和印度对国际问题和领导力有兴趣和能力的大学毕业生 。奖学金获得者将有机会在德国生活一年，与来自各国的未来领袖们联系交流并探讨当下全球问题的解决方案。奖学金获得者也将在各自于德国访问的机构中进行独立的项目活动。

了解申请资格与申请方法，请访问<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.humboldt-foundation.de/web/german-chancellor-fellowship.html">这里</a>。报名申请截止日期为<strong>2013年9月15日</strong>。</div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;">
<h4 class="h4" style="display: block; font-weight: bold; line-height: 100%; margin: 0px 0px 10px; text-align: left; color: #202020; font-family: Arial; font-size: 22px; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0;"><span style="color: #ff0000; font-family: arial, helvetica, sans-serif; font-size: large;"><strong>错过了往期的</strong><strong>ACYA</strong><strong>时讯怎么办？</strong></span></h4>
不要担心！请在<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.acya.org.au/index.php/en/publications/e-bulletin">ACYA网站</a>的文档中寻找往期时讯。

</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
<tbody>
<tr>
<td style="border-collapse: collapse;" valign="top">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><span style="color: #ff0000;"><span style="font-size: 18px;"><strong><span style="font-size: 24px;">Connect with ACYA Group / 联系我们</span></strong></span></span>



Twitter:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya"><img style="width: 75px; height: 73px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/twitter_logo.jpg" width="75" height="73" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/a_cya">@a_cya</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://twitter.com/EngagingChina">@EngagingChina</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/ACYDialogue">@ACYDialogue</a>



Facebook:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/"><img style="width: 75px; height: 75px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/facebook_logo.jpg" width="75" height="75" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">ACYA National</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/EngagingChinaProject?fref=ts">Engaging China Project</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/pages/Australia-China-Youth-Dialogue/129486543768784?fref=ts">ACYD</a>



LinkedIn:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-association"><img style="height: 75px; line-height: 16px; color: #0000ee; width: 75px; outline: none; text-decoration: none; display: inline; margin: 5px; border: 0;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/LinkedIn_logo.png" width="75" height="75" align="right" /></a>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-association" target="_self">ACYA</a></div>
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/groups/AustraliaChina-Young-Professionals-Initiative-ACYPI-3744368" target="_self">ACYPI</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.linkedin.com/company/australia-china-youth-dialogue" target="_self">ACYD</a>



Chinese Social Media:<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://e.weibo.com/acya"><img style="width: 75px; height: 60px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/weibo.png" width="75" height="60" align="right" /></a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://e.weibo.com/acya" target="_self">Weibo</a>

<a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://page.renren.com/600745607?checked=true" target="_self">Renren</a></div>
</div></td>
<td style="border-collapse: collapse;" align="center" valign="top"><img style="margin: 0; padding: 0; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/transparent.gif" border="0" /></td>
</tr>
</tbody>
</table>
<!-- // End Module: Left Image with Content \ -->

<!-- // Begin Module: Right Image with Content \ -->

<!-- // End Module: Right Image with Content \ --></td>
</tr>
</tbody>
</table>
<!-- // End Template Body \ -->

<!-- // Begin Template Footer \ -->
<table id="templateFooter" style="background-color: #ffffff; border-top: 0;" width="600" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="footerContent" style="border-collapse: collapse;" valign="top"><!-- // Begin Module: Standard Footer \ -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td id="social" style="border-collapse: collapse; background-color: #fafafa; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="https://twitter.com/#!/a_cya">follow on Twitter</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.facebook.com/groups/ACYAnational/">friend on Facebook</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://us1.forward-to-friend.com/forward?u=db2f0ea3a68f632de3088eada&id=213c903a93&e=b9ce0f9afe">forward to a friend</a></div></td>
</tr>
<tr>
<td style="border-collapse: collapse;" valign="top" width="350">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"><span style="color: #2f4f4f;">ACYA is proudly supported by:</span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;"></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://transasialawyers.com/" target="_blank"><img style="width: 250px; height: 122px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="TransAsia Lawyers" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/transasia.jpg" width="250" height="122" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://chinainstitute.anu.edu.au/" target="_blank"><img style="width: 250px; height: 85px; margin: 2px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ANU_LOGO_RGB_50mm.jpg" width="250" height="85" align="none" /></a> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.crccasia.com/"><img style="height: 175px; line-height: 12px; width: 250px; margin: 2px; border: 0; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/CRCC_Asia.1.jpg" width="250" height="175" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.cityofsydney.nsw.gov.au/"><img style="width: 250px; height: 84px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/home_sydney.png" width="250" height="84" align="none" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.austcham.org/"><img style="width: 180px; height: 79px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/austchamlogo_1_.jpg" width="180" height="79" align="none" /></a><a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://www.austchinaalumni.org/"><img style="width: 217px; height: 180px; margin: 5px; border: 0; line-height: 100%; outline: none; text-decoration: none; display: inline;" alt="" src="http://gallery.mailchimp.com/db2f0ea3a68f632de3088eada/images/ACAA_logo_optimised_for_the.jpg" width="217" height="180" align="none" /></a></div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;"></div>
</div></td>
<td id="monkeyRewards" style="border-collapse: collapse;" valign="top" width="190">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">

<span style="color: #2f4f4f;">Header photograph - A purveyor of sauces, by <strong>Nicola Boyle.</strong></span></div>
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;">



<span style="color: #2f4f4f;"><em>Copyright © 2013 ACYA, All rights reserved.</em></span>

</div>
<div style="text-align: center; color: #707070; font-family: Arial; font-size: 12px; line-height: 125%;">









<span style="font-size: 12px;"><span style="color: #2f4f4f;"><span style="color: #2f4f4f;"> <a href="http://www.mailchimp.com/monkey-rewards/?utm_source=freemium_newsletter&utm_medium=email&utm_campaign=monkey_rewards&aid=db2f0ea3a68f632de3088eada&afl=1"><img title="MailChimp Email Marketing" alt="Email Marketing Powered by MailChimp" src="http://gallery.mailchimp.com/089443193dd93823f3fed78b4/images/banner1.gif" border="0" /></a> </span></span></span></div>
</div></td>
</tr>
<tr>
<td id="utility" style="border-collapse: collapse; background-color: #ffffff; border: 0;" colspan="2" valign="middle">
<div style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"> <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/unsubscribe?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe&c=213c903a93">unsubscribe from this list</a> | <a style="color: #336699; font-weight: normal; text-decoration: underline;" href="http://acya.us1.list-manage.com/profile?u=db2f0ea3a68f632de3088eada&id=6e67c7a2c2&e=b9ce0f9afe">update subscription preferences</a></div></td>
</tr>
</tbody>
</table>
<!-- // End Module: Standard Footer \ --></td>
</tr>
</tbody>
</table>
<!-- // End Template Footer \ --><!--:-->