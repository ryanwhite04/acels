---
title: ACELS Brisbane 2018 Review
author: Kimberleigh Bates
date: 2018-08-12T08:05:29+08:00
categories:
  - ACELS
  - Newsletter
tags:
  - ACELS Brisbane 2018
locations:
  - Australia
  - Brisbane
  - Queensland
featured_image: Banners.jpg
---

[ACELS Brisbane 2018](/acels/brisbane-2018) was a highly successful Summit, bringing people together from all over Australia and China to discuss the future of the Australia-China relationship and the ongoing work of so many dedicated ACYA members.

<!--more-->

Delegates heard from professional speakers in the Australia-China space and participated in seminars and workshops on a variety of issues including the role of youth, transnational education, language learning, innovation, art and cultural exchange, trade and investment, foreign policy, and cross-cultural leadership.

For a lot of delegates attending ACELS was an opportunity to expand their personal and professional networks, better understand ACYA and learn among their peers.

> ACELS is a wonderful opportunity to meet like-minded individuals and discuss topics that we share a deep passion for.
> -- Connor O'Brien, President of ACYA Griffith University

> I would like to extend my gratitude to the project team for organising a diverse and successful event and I hope to attend more ACYA and ACELS events in the future.
> -- Justin Ji, Careers Director with ACYA QUT

ACYA runs ACELS twice a year, in Australia and in China.

> I would encourage any young person with a passion for learning more about the Australia-China space to apply to be an ACELS delegate.
> -- Rex Tion, President of the ACYA Curtin University