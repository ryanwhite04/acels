---
title: Cynthia Yuan
author: Courtney Lor
date: 2018-08-05T20:00:58+08:00
featured_image: Cynthia Yuan.jpg
categories:
- Alumni Spotlight
- Newsletter
---

In this Alumni Spotlight interview, Courtney Lor chats with Cynthia Yuan, the ACYA President of the Macquarie Chapter in 2015 and National Secretary in 2016. She was also the Operations Manager for ACELS Sydney 2015 and studied a Bachelor of Laws with a Bachelor of Commerce at Macquarie University.

<!--more-->

## What do you do for work currently?

I'm a Utilities Policy Analyst at KPMG where I advise government, regulators, and industry on the regulatory framework governing Australia's energy sector. Outside of this, I'm currently a legal volunteer in Environmental Justice Australia (EJA)'s Climate and Finance division.

## What would you consider has been the highlight of your time with ACYA?

The lifelong friendships and networks I have built. My close circle of friends that I have now and the people I am inspired by on a daily basis I would not have met had it not been for my involvement in ACYA.

## In your view, what role does ACYA have to play in the Australia-China youth space?

It encourages the future leaders of our generation to think outside the box and push boundaries. It exposes them to a culture outside of our immediate, and often insular, bubble and connects us with like-minded individuals who inspire us to be better.

## What do you think is currently the biggest challenge to the Australia-China relationship?

I think our biggest challenge involves figuring out how we position ourselves in the strategic conflict between America, our major ally, and China, our major trade partner, as America's strategic engagement in Asia dwindles.

I think considerable damage has been inflicted on our relationship with China by recent government policies. These policy decisions have undermined years of trust-building during our previous pivot towards China and the initiatives which arose out of this period, including the Australia-China Free Trade Agreement. The tightening of security measures against China can be seen through limitation of the mortgage eligibility of Chinese investors with the big Australia banks, FIRB blocking Chinese bids on critical Australian infrastructure, and most recently Huawei being banned from Australia's 5G wireless networks. Moving forwards, Australia will need to carefully balance its interests in protecting it national security against its interests in maintaining China's trust.