{
  "title": "Foundation for Australian Studies in China (FASIC) Internship Program",
  "author": "ACYA",
  "date": "2018-08-01T08:25:41+08:00",
  "featured_image": "Foundation for Australian Studies in China.jpg",
  "categories": [
    "Opportunity"
  ],
  "organisations": [
    "Foundation for Australian Studies in China (FASIC)"
  ]
}


Applications for 2018 Spring Semester are now open and will close on **17 August**.

<!--more-->

## The FASIC Internship Program

An essential part of the mission of the Foundation for Australian Studies in China is to encourage the next generation of Chinese youth to develop an interest in Australia as a country and subject of academic inquiry, as well as to support the endeavours of young Australians studying and working in China.

To this end, the Centre has partnered with the Australia-China Youth Association (ACYA) to create a unique Internship Program that enables both Australian and Chinese students to gain valuable practical academic experience working at the Australian Studies Centre on campus at Peking University.

<!--more-->

Interns at the Peking University Australian Studies Centre are provided with the opportunity to pursue an Australia-China research project of their own design under the supervision of the BHP Billiton Chair of Australian Studies Professor Gregory McCarthy (from February 2016). The research project can take the form of a conference presentation, short paper, or any other form of research approved by the Chair.

Other duties of interns may include managing the social media presence of the Centre, writing news and blog content for the Centre website, planning and running the annual keynote academic conference, organising roundtable events for Australian and Chinese students and academics in Beijing, and assisting the Chair with the implementation of the Australian Studies academic curriculum at Peking University and other Centre duties.

The duration of an internship will be three months. Interns are expected to make a commitment of three days per week, with one day at the Centre itself and two days on the research project. This may also involve tutoring local Chinese students at the University. The internship is aimed at those already or about to be based in Beijing, as the Centre is unable to provide visa assistance. Interns will receive a modest monthly honorarium.

## Selection Criteria

<ul>
 	<li>Currently studying or recently graduated with a tertiary degree;</li>
 	<li>Demonstrated interest in and knowledge of an aspect of Australia-China relations</li>
 	<li>Ability to conduct research independently and work with academics and other interns on ad-hoc projects;</li>
 	<li>Reliable organisational skills;</li>
 	<li>Excellent English language competency; and,</li>
 	<li>Chinese language ability not necessary but an advantage.</li>
</ul>

## How to Apply

Applications should include a **CV and a one-page cover letter** outlining the candidate's motivation for applying and a brief research project proposal outline. Check out our resume guide [here](http://www.acya.org.au/careers/careers-guides/resume-cover-letter-writing/) prior to submission. Internships will commence/conclude in line with Peking University semester dates as follows:

## Application timeline: PKU Autumn Semester 2018

<ul>
 	<li>Applications open: now</li>
 	<li>Applications close: 17 August 2018</li>
 	<li>Applicants notified: 20 August 2018</li>
 	<li>Internship period: 10 September 2018 <span class="aBn"><span class="aQJ">to 14 January 2019</span></span></li>
 	<li>Hours: part-time approximately 2-3 days per week.</li>
</ul>

Please email your complete application or further queries with the subject **FASIC Internship** to careers director Alana Kirby at <mailto:careers@acya.org.au> Good luck!