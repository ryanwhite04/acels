{
  "title": "ACELS Beijing Media Release",
  "author": "Liam Kearney",
  "date": "2018-03-09T03:37:12+08:00",
  "image": "",
  "categories": [
    "ACELS",
    "Media Release"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association"
  ],
  "resources": []
}
From February 22 to 25, the Australia-China Emerging Leaders Summit (ACELS) returned to Beijing supported by a $20,000 grant from the Australia-China Council.
Over four days, 70 delegates (half from China, half from Australia) gathered for an intensive series of panel discussions, workshops, interactive activities and networking events under the theme: **"Broadening the Bilateral: Embracing a New Asian Paradigm"**.

For a full, interactive report on ACELS Beijing 2018, please click [here](https://spark.adobe.com/page/lH2aLK5G1KNtU/).

> The people-to-people connections between Australia and China are the real strength of the relationship … ACELS is a particularly important part of that because we need young leaders to have full exposure to each other in China and Australia to keep improving and expanding these networks.
>
> -- Jan Adams, Australia's Ambassador to the People's Republic of China

![first image](https://spark.adobe.com/page/lH2aLK5G1KNtU/images/28ec3c6a-e1d5-40e2-af68-93250d523322.jpg)

<!--more-->

Capturing the scope of unprecedented change occurring in the Asia-Pacific, the theme was further divided into number of key areas which formed the basis for the Summit content. Critical issues — namely, geopolitics, trade and Investment; environment and clean energy; philanthropy and social welfare; innovation and youth entrepreneurship; and arts, culture and soft power — were explored in depth in a multilateral, forward-looking framework.

![second image](https://spark.adobe.com/page/lH2aLK5G1KNtU/images/11758247-5aa5-4a1f-8084-e04e520e8131.jpg)

The networking evening at King & Wood Mallesons as well as the Gala Dinner at TRB Copper gave delegates a platform to connect and interact with senior leaders in government, industry and academia across the bilateral space. Most importantly, close interactions over four days provided the delegates with ample opportunity to forge lasting friendships with each other.

> These past few days have opened my eyes to the many aspects of the unique relationship between Australia and China. I feel inspired to continue learning about and contributing to this bilateral relationship.
>
> -- Jingjing Ge, ACELS BJ18 Delegate

![third image](https://spark.adobe.com/page/lH2aLK5G1KNtU/images/3007a032-2b8c-476f-bb9f-2de1dd2de474.jpg)

Feedback from delegates, senior government figures and top-level industry representatives was overwhelmingly positive. Stakeholders emphasised the value of ACELS in bringing together the next generation of leaders in the Australia-China space, continuing important and topical discussions, and providing a vehicle for youth to share ideas and seize opportunities for further personal and professional development. 

> We recognise the valuable work that ACYA  is doing in China to provide a platform for engaging the future leaders in this space.
>
> -- Vaughn Barber, AustCham Beijing Chair, Global Chair, KPMG Global China Practice