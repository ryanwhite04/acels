{
  "title": "National Committee Positions Open",
  "author": "ACYA",
  "date": "2018-03-30T14:02:48+00:00",
  "image": "",
  "categories": [
    "Opportunity",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Australia-China Youth Association"
  ],
  "resources": []
}
The National Executive is looking for new team members to contribute to the various initiatives of ACYA, the only youth administered NGO in Australia-China affairs. 

As ACYA continues to grow beyond its 6000+ membership base, it will require the support, passion and enthusiasm of its officers to continue fostering its vibrant transnational community.

With the variety of National Executive Portfolios, becoming an Officer is also a fantastic way to hone personal and professional skills in a cross-cultural environment. Thus, we strongly encourage applicants from all backgrounds to apply! Further, applicants are welcome and indeed encouraged to apply for multiple roles; if you do so, please state your order of preference among the roles you have applied for.

More information can be found in the position descriptions below
 
- [Alumni Officer](www.acya.org.au/positions/alumni-officer)
- [Careers Officer](www.acya.org.au/positions/careers-officer)
- [China Secretary](www.acya.org.au/positions/china-secretary)
- [Education Officer](www.acya.org.au/positions/education-officer)
- [Information Technology Officer](www.acya.org.au/positions/it-officer)
- [Media Officer](www.acya.org.au/positions/media-officer)
- [Translations Officer](www.acya.org.au/positions/translations-officer)
- [Partnerships Officer](www.acya.org.au/positions/partnerships-officer)
- [People to People Officer](www.acya.org.au/positions/p2p-officer)
- [Publications Officer](www.acya.org.au/positions/publications-officer)

We look forward to receiving your applications before April 6th, 2018! 