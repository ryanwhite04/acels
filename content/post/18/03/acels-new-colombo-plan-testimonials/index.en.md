{
  "title": "ACELS New Colombo Plan Testimonials",
  "author": "Liam Kearney",
  "date": "2018-03-01T07:39:33+08:00",
  "image": "",
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association"
  ],
  "resources": []
}

> ## The Hon. Julie Bishop MP, Minister for Foreign Affairs
> <img alt="Julie Bishop" src="http://www.acya.org.au/wp-content/uploads/2018/03/julie-150x150.jpg" height="150" width="150" align="left" style="margin-right: 1em"></img> Events like the Australia-China Emerging Leaders Summit (ACELS), which unite passionate and talented youth in the Australia-China space, are essential to nurture the future of the bilateral relationship.
> 
> Strengthening people-to-people connections and enhancing youth engagement is at the heart of the New Colombo Plan, which is supporting more than 4,700 Australian undergraduates to live, study and work in China.

<!--more-->

---

> ## Jan Adams, AO PSM, Australia’s Ambassador to the People’s Republic of China
> <img alt="Jan Adams" src="http://www.acya.org.au/wp-content/uploads/2018/03/jan-adams-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> The people-to-people connections between Australia and China are the real strength of the relationship... ACELS is a particularly important part of that, because we need young leaders to have full exposure to each other in China and Australia to keep improving and expanding these networks.

---

> ## Chloe Dempsey - China Scholar 2016, ACYA China General Manager 2017
> <img alt="Chloe Dempsey" src="http://www.acya.org.au/wp-content/uploads/2018/03/chloe-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> Upon returning from two months interning in the Taiwanese wilderness for a national park in 2015, I flew directly into Sydney to attend ACELS for the first time. I was blown away by the diversity of young people brought together under the banner of the Australia-China relationship.

> Our packed schedule exposed us to some of the most interesting and intellectual thinkers in the bilateral relationship, and gave us the opportunity to get to know each other well through hands-on workshops and activities around Sydney. I am still very close to many of the friends I made at that first Summit.

> Similarly, having first arrived in China at the beginning of what was to be a ‘trip’ lasting over two years, I attended the ACELS in Shanghai and there could have been no better introduction to the opportunities and excitement that awaited me. Getting a taste of what was to come as a young person actively invested in our bilateral relationship was very unique - especially in just three days.

> It was a whirlwind overseeing ACELS Beijing 2018 but one that makes me very proud and positive about the future of the Australia-China relationship. While the delegation’s diversity and individual achievements were incredibly impressive, for me personally, working with the team that put in tireless hours to make it happen was an excellent exercise in teamwork, cross-cultural learning and skills-building in itself.

> ## Liam Kearney - China Fellow 2017, ACELS Beijing 2018 Project Manager
> <img alt="Liam Kearney" src="http://www.acya.org.au/wp-content/uploads/2018/03/liam-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> Over the last six years ACELS has grown into the largest summit for youth passionate about the Australia-China space, and is ACYA's signature initiative. Twelve New Colombo Plan scholars attended ACELS Beijing 2018 from across three cohorts (2016, 2017 & 2018), representing three regional host locations (Mainland China, Taiwan and Hong Kong).  

> This year the summit brought together 70 extraordinary delegates (half from China, half from Australia) for four days of intensive panels, workshops, networking events and cultural and social activities. The ACELS BJ18 delegation comprised university students and young professionals from 20 Chinese provinces and regions, and 6 Australian states and territories - studying and working in fields across the spectrum of bilateral engagement.  

> At its core, ACELS provides a platform to build connections and forge the friendships that will drive future partnership and cooperation between our two countries. On behalf of ACYA I would like to express my gratitude for the strong financial and in-kind support received from the Australia-China Council, the Australian Embassy in Beijing and the New Colombo Plan that made ACELS Beijing 2018 possible.

---

> ## Robert Banks - China Scholar 2017
> <img alt="Robert Banks" src="http://www.acya.org.au/wp-content/uploads/2018/03/robbanks-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> ACELS provides a great opportunity for Chinese and Australian students to build both professional and personal relationships whilst building knowledge across a variety of issues in the Sino-Australian space. I first attended ACELS in 2017, when I initially arrived in China to begin my NCP experience.  

> Arriving fresh, I had no professional or personal contacts within China and had only briefly met the other NCP students based in China.  

> During the conference I established professional relationships which directly led to internships that I have completed during my NCP experience. It also allowed me to have a ready-made personal network of existing NCP students in China as well as Chinese delegates that could help me through the process of adjusting to living in China. 

> I jumped at the opportunity to attend this year’s ACELS to further build my network and meet like-minded people from both China and Australia. The discussions regrading technology and venture capital in China and Australia was of particular interest to me as this is a field I intend to enter following graduation. No other experience I’ve had during my NCP program has provided the opportunity to have frank and in-depth discussions about these areas, and to have direct contact with leading figures in these fields.

---

> ## Zoe Fitzgerald - Hong Kong Fellow 2018
> <img alt="Zoe Fitzgerald" src="http://www.acya.org.au/wp-content/uploads/2018/03/zoe-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> I had an incredible experience at ACELS Beijing 2018. This was the first time that I have ever been involved with ACYA, and from the start everyone was so welcoming and friendly. During ACELS, I was able to engage deeply in the Australia-China space by meeting and working with peers from both nations.  

> I got so much out of ACELS; not only have I left the conference with a renewed understanding and appreciation of the bilateral relationship, but I have also made lifelong friends. The highlight for me was being able to learn from the diverse range of speakers that were on the panels each day.  

> ACELS has been incredibly valuable for me as a scholar and ties into the goals of the New Colombo Plan. Being able to network with diverse individuals from both Australia and China allowed me to develop the interpersonal and cross-cultural networks that are the bedrock of the NCP programme. I have also gained a more nuanced understanding of the Australia-China relationship, which I will use to inform and strengthen my NCP experience. I feel so lucky to have had the opportunity to attend such a fantastic conference, and highly recommend the experience to NCP scholars in the future.

---

> ## Joseph Percy - China Scholar 2018
> <img alt="Joseph Percy" src="http://www.acya.org.au/wp-content/uploads/2018/03/joseph-150x150.png" height="150" width="150" align="left" style="margin-right: 1em"></img> ACELS is an amazing forum for connecting youth engaged in the Australia-China space to each other and to leaders of industry. For me, the central highlight was the depth of professional and cultural exchanges that allowed me to form friendships and connections that will last for years to come.  

> The diverse array of backgrounds from which the participants came meant that every exchange was rich and insightful. ACELS truly presents a unique opportunity to form lasting relationships with youth and leaders in industries ranging from government to the tech world and even to the art world.  

> As a recipient of a New Colombo Plan Scholarship – a program designed to build connections in the Indo-Pacific – I found that ACELS complements the goals of the NCP by providing a fantastic opportunity to form deep culturally-aware relationships across a vast array of industries. I could not recommend ACELS more highly for anyone interested in strengthening the relationship between Australia and China.

---

![New Colombo Plan scholars at the ACELS Beijing 2018 Gala Dinner.](http://www.acya.org.au/wp-content/uploads/2018/03/5A1DDBCB-7BE8-49A3-BFE8-189680D87C00-1024x757.jpg "New Colombo Plan scholars at the ACELS Beijing 2018 Gala Dinner.") __New Colombo Plan scholars at the ACELS Beijing 2018 Gala Dinner__.
