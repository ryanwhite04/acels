{
  "title": "City Guides",
  "author": "ACYA",
  "date": "2018-03-21T06:27:19+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
The majority of Australian students coming to China will spend their time in the academic hubs of Beijing and Shanghai. While Beijing and Shanghai offer exposure to China’s top universities, it is a mistake to believe that they are the only ‘big cities’ of China. Cities in China are often categorised into ‘tiers’ dependent upon their gross domestic product, political administration level and population. By such markers, China has up to 15 first-tier cities-think of them each as a Sydney or Melbourne!

https://youtu.be/jbz0JL0sxV8

<!--more-->

For Australian students, studying in cities outside of Beijing and Shanghai offer unique opportunities to engage with a China, which is arguably more ‘authentic’, less explored and allows for greater learning of language and culture. 
ACYA has chapters across China, and has a number of projects which offer Australian students the opportunity to engage with a less-explored part of China such as the Leizhou trip, EGRC initiatives and the annual FASIC conference.

Considering this, the Australian Embassy Beijing, in conjunction with New Colombo Plan, supported ACYA to showcase some of their regional chapters. For those considering studying in China these guides and videos provide an insight into what life is like for the average student in the bustling metropolises outside of Beijing and Shanghai. From Cantonese Guangzhou to spicy Chengdu to wise Wuhan to romantic Nanjing, we hope that these guides will encourage more Australian students to embrace a more alternative study destination.

---

## Guangzhou

[Guide to Guangzhou](http://www.acya.org.au/wp-content/uploads/2018/03/28-Jan-Guangzhou.compressed.pdf)

https://youtu.be/j72oEYnSsMw

---

## Nanjing

[Guide to Nanjing](http://www.acya.org.au/wp-content/uploads/2018/03/26-Jan-Nanjing.pdf)

https://youtu.be/z1HlYakhKcU

---

## Chengdu

[Guide to Chengdu](http://www.acya.org.au/wp-content/uploads/2018/03/5-Feb-Chengdu.pdf)

https://youtu.be/jXoZkuCXte8

---

## Wuhan

[Guide to Wuhan](http://www.acya.org.au/wp-content/uploads/2018/03/26-Jan-Wuhan.compressed.pdf)

https://youtu.be/gvNM542sOqs