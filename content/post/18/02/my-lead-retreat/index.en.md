{
  "title": "ACYA x EGRC My Lead Retreat",
  "author": "Yifan Wang",
  "date": "2018-02-13T12:49:48+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Youth Association",
    "Educating Girls of Rural China"
  ],
  "resources": [
    {
      "title": "Baker and Spice Dinner",
      "type": "image",
      "src": "Baker-and-Spice-Dinner.jpg"
    },
    {
      "title": "Baking",
      "type": "image",
      "src": "Baking.jpg"
    },
    {
      "title": "Cultural Shock Discussion",
      "type": "image",
      "src": "Cultural-Shock-Discussion.jpg"
    },
    {
      "title": "Inauguration",
      "type": "image",
      "src": "Inauguration.jpg"
    },
    {
      "title": "Shijia Hutong Museum",
      "type": "image",
      "src": "Shijia-Hutong-Museum.jpg"
    },
    {
      "title": "Yoga",
      "type": "image",
      "src": "Yoga.jpg"
    }
  ]
}
On the weekend of 2 and 3 December 2017, the Australia-China Youth Association (ACYA) in collaboration with Educating Girls of Rural China (EGRC), hosted the inaugural My Lead retreat in Beijing.

[embed]https://youtu.be/ROUkDGcOIT4[/embed]

<!--more-->

The retreat allowed 5 ACYA girls currently undertaking exchange studies in either Beijing or Shanghai to meet with 8 EGRC girls, born in Gansu, but now studying or working in cities across China.

The retreat kicked off on Saturday morning with an introduction from the project leader, Joanna Yue. She highlighted the main objectives of the retreat which included cultural sharing, language exchange and most importantly, friendship building. An opening icebreaker activity saw the silence broken by the girls' enthusiasm to know more about each other.

![Baker and Spice Dinner](http://www.acya.org.au/wp-content/uploads/2018/02/Baker-and-Spice-Dinner-300x200.jpg "Baker and Spice Dinner")

After brunch at the Hatchery, the girls had animated discussions around the shared bike phenomenon in China, first impressions of the country, and other cultural shocks and differences between the perspectives of Australians and Chinese. The girls then had a Hutong tour led by a local school boy at Beijing's first Hutong Museum, Shijia Hutong, which allowed the girls to discuss ancient Qing history and family courtyard establishments. On Saturday afternoon, the conversation traversed topics of family, leisure activities, education, relationships, self-awareness and job interviews. The retreat provided a unique and intimate forum for young women from different worlds to find commonality and appreciate their different strengths as they exchanged insights, experience and advice with one another.

![Baking](http://www.acya.org.au/wp-content/uploads/2018/02/Baking-300x200.jpg "Baking")

As Crystal Chang, one of the EGRC girls said, it provided girls a good platform to practice English and learn to communicate with each other, and it also had a huge impact on their vision, values and life. A good day cannot end without something delicious so the girls concluded the first day by dining at a local Beijing Hotpot restaurant.

Early on Sunday morning, a yoga session, led by multi-talented Emily Dunn, allowed the girls to refresh their minds and learn different ways of centering and relaxing themselves such as meditation and deep breathing. This was followed by cookie baking class which allowed the girls to bond over a Western tradition and practical yet artistic activity. For many of the EGRC girls, it was their first time to try Western style baking. Later in the day, at 706 Youth Space, ACYA member and New Colombo Plan scholar, Rebecca Jensen, taught girls to write “Landay poems”, short couplets of Afghanistan origin that wittily reveal truths on themes such as home, love and gender roles.

![Cultural Shock Discussion](http://www.acya.org.au/wp-content/uploads/2018/02/Cultural-Shock-Discussion-300x200.jpg "Cultural Shock Discussion")

Emmelyn Wu, one of the ACYA girls, reflected on a memorable moment writing poems with the EGRC girls

> Despite being introduced to the concept only a few minutes before, and writing in a language other than their mother tongue, I found the sentiment and words expressed by some of the girls in their pieces empowering and thought-provoking, and it reminded me just how powerful the tools of education and communication can be.

![Inauguration](http://www.acya.org.au/wp-content/uploads/2018/02/Inauguration-300x200.jpg "Inauguration")

The entourage of young women then traveled with ACYA Beijing Chapter members to visit the Inside Out Gallery, where they enjoyed an eye-opening contemporary art tour. Then at the final dinner, it was a privilege to have Jackie Yun, the co-founder of the successful Wagas Group, share her own experience of studying and doing business in China. She encouraged girls to not wait but take action, learn from failures, set a goal and be self-disciplined and most importantly to believe in themselves. It was an extremely empowering speech. Jackie and Wagas were very generous to treat the girls to a feast of their fresh and healthy cuisine, which fit in well with a theme of health and self care running through the retreat.

![Shijia Hutong Museum](http://www.acya.org.au/wp-content/uploads/2018/02/Shijia-Hutong-Museum-300x200.jpg "Shijia Hutong Museum")

The retreat would not have happened without the generous support of The Australian Embassy, the New Colombo Plan and Wagas, the logistical excellence of Joanna Yue, the backstage facilitation of Yifan Wang, the expert embassy liaison of ACYA China Manager on the ground, Chloe Dempsey and the special moments captured by photographer Luke Pegrum. Thanks also to the team behind the scenes led by ACYA Managing Director Josephine Macmillan, who designed the concept, produced the retreat plan and workshop materials, worked with EGRC to launch the retreat. Thanks to ACYA’s Education Director April Liu, and her officers Sandy Qiao, Sunny Huang. ACYA looks forward to continuing its special partnership with EGRC into 2018.

![Yoga](http://www.acya.org.au/wp-content/uploads/2018/02/Yoga-300x200.jpg "Yoga")

Photographer: Luke Pegrum