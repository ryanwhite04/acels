{
  "title": "ACYA Announces 2018 AGM Results",
  "author": "ACYA",
  "date": "2018-02-05T03:32:45+00:00",
  "image": "",
  "categories": [
    "Media Release",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
The Australia-China Youth Association Inc. (ACYA) held its 2018 AGM on 4 February 2018. Attended by National Executive Committee members, Chapter Presidents, and members from across Australia and China, the AGM provided an opportunity to recap ACYA’s initiatives and achievements in the past year and elect a National Executive Committee for 2018.

<!--more-->

See the 2017 Annual Reports in the links below:

- [Australia Manager](http://www.acya.org.au/wp-content/uploads/2018/02/Australia-Manager.pdf)
- [China Manager](http://www.acya.org.au/wp-content/uploads/2018/02/China-Manager.pdf)
- [National Treasurer](http://www.acya.org.au/wp-content/uploads/2018/02/National-Treasurer.pdf)
- [National President](http://www.acya.org.au/wp-content/uploads/2018/02/National-President.pdf)
- [Managing Director](http://www.acya.org.au/wp-content/uploads/2018/02/Managing-Director.pdf)

The applicants of the 2018 National Executive Committee came from a range of academic backgrounds and possessed a variety of work and extra-curricular experiences. This reflected ACYA’s role as a leading youth organisation run by and for talented young Australians and Chinese.

The incoming National President is Roger Lee. This year marks his fifth year of involvement with ACYA where he has served in a variety of roles including:

- Macquarie Chapter President (2014),
- BYLW Logistics & Catering Manager (2014),
- Business Development Officer (2015–16),
- and most recently, Australia Manager (2017).

> During my involvement with ACYA, I have witnessed many amazing milestones. Accordingly, I’m excited to work alongside the 2018 National Committee to build on the successes of past Executives and see the organisation continue to flourish!
> --<cite>Roger Lee, ACYA National President 2018</cite>

Roger will be joined by Tony Gu as Managing Director. He is a student of Asian Studies and International Relations at the Australian National University. In 2017, Tony not only served as the Executive Director of the Golden Koala Film Festival in Canberra but also as ANU Chapter President, which was awarded “Best Club 2017” at their university.

ACYA is pleased to announce the other members of the National Executive Committee for 2018:

- National President - Roger Lee
- Managing Director - Tony Gu
- National Secretary - Nancy Chen
- National Treasurer - Yifan Wang
- Operations Manager - Jennie Hu
- Australia Manager - Anthea Lai
- China Manager - Liam Kearney
- Alumni Manager - Dawn Qiu

> The record number of applicants for the 2018 National Executive Committee and high attendance at this year’s AGM are testament to the hard work of the 2017 Committee and the enduring interest in the Australia-China relationship at the youth level. I congratulate the members of the 2018 Committee on their election. Having worked with many of them in my time in ACYA, I look forward to the suite of existing and new projects, events and initiatives they will implement in the coming year.
> --<cite>Georgia Sands, ACYA National President 2017</cite>