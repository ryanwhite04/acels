{
  "title": "ACELS Exploring the many facets of the bilateral relationship",
  "author": "ACYA",
  "date": "2018-02-21T08:47:34+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS",
    "Media Release"
  ],
  "tags": [],
  "locations": [
    "Beijing",
    "China"
  ],
  "organisations": [
    "Australia-China Council",
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association",
    "ShineWing Australia",
    "Trade Victoria"
  ],
  "resources": [
    {
      "title": "ACELS Melbourne Laneway Feature",
      "type": "image",
      "src": "ACELS-Melbourne-Laneway-Feature-e1519202348332.jpeg"
    },
    {
      "title": "ACELS Melbourne Laneway Graffiti",
      "type": "image",
      "src": "ACELS-Melbourne-Laneway-Graffiti.jpeg"
    }
  ]
}
With ACELS Beijing just around the corner, ACYA is excited to announce the release of the ACELS Melbourne 2017 Publication! By providing an overview of the aims of such Summits, as well as insights from the various lectures and workshops, this timely Publication provides a taste of what to expect at ACELS Beijing 2018.

> Events like ACELS, which unite passionate and talented youth in the Australia-China space, are essential to nurture the future of the bilateral relationship.
> -- <cite>The Hon Julie Bishop MP, Minister for Foreign Affairs</cite>

![ACELS Melbourne Laneway Graffiti](http://www.acya.org.au/wp-content/uploads/2018/02/ACELS-Melbourne-Laneway-Graffiti-300x225.jpeg "ACELS Melbourne Laneway Graffiti")

<!--more-->

The themes of both Summits reflect important turning points in the bilateral relationship. In light of the 45th anniversary of diplomatic relations, ACELS Melbourne focused on the theme of Connecting Through Culture. Here, delegates were encouraged to take stock of the many aspects of Australia-China relations such as artistic exchange, sports diplomacy, and new tourism initiatives. Reflecting new challenges and unparalleled opportunities that come with massive transformation in the region, ACELS Beijing will be guided by the theme of Broadening the Bilateral: Embracing a New Asian Paradigm.

> The analytical and passionate discussion allowed delegates to expand their understanding of the Australia-China relationship and share their own experiences.
> -- <cite>Ciara Morris, ACYA USyd Chapter President and Publications Officer (2017)</cite>

Another highlight of ACELS is the calibre of its delegates. Melbourne marked the first time ACELS was able to host a delegation of seventy from various academic backgrounds. Similarly, the upcoming Beijing Summit comprises students and young professionals from over 20 Chinese provinces and regions, and six Australian states and territories. Their backgrounds are equally diverse: ranging from optoelectronics, renewable energy, and consumer psychology to clinical medicine, finance, and conflict studies.

Following the successes of ACELS Melbourne, the Beijing Summit promises to once again provide a platform to unite and engage a new generation of young Australian and Chinese leaders.

ACYA would like to acknowledge the generous support of its partners, without whom ACELS would not be possible:

- Melbourne
  - [The State Government of Victoria](http://trade.vic.gov.au/)
  - [ShineWing Australia](http://www.shinewing.com.au/)
- Beijing
  - [Australia-China Council](http://dfat.gov.au/people-to-people/foundations-councils-institutes/australia-china-council/Pages/australia-china-council.aspx)