{
  "title": "Shanghai Teaching Opportunity",
  "author": "ACYA",
  "date": "2018-04-19T17:07:45+08:00",
  "image": "",
  "categories": [
    "Opportunity"
  ],
  "tags": [],
  "locations": [
    "China",
    "Shanghai"
  ],
  "organisations": [
    "Stepping Stones"
  ],
  "resources": [
    {
      "title": "Stepping Stones",
      "type": "image",
      "src": "Stepping-Stones.png"
    }
  ]
}
Are you an enthusiastic person who have been thinking about giving rather than receiving in 2018?

ACYA, in conjunction with [Stepping Stones](http://steppingstoneschina.net) is pleased to be opening applications for our Shanghai Teaching Program. This will be a wonderful opportunity to teach English to migrant children in Shanghai and to work with like-minded volunteers from around the world who also want to make a difference!

There are 2 programs available, the Videolink program, which will be taught online to a class, and the Classroom teaching program, which will be taught in a classroom in Shanghai.

<!--more-->

The program will be conducted from April until July 2018. The **Minimum Volunteering Commitment** is once a week for a full school term with a minimum of 8 weeks teaching.

There is no previous teaching experience required. An online orientation and training session will be provided by Stepping Stones. This will cover information on migrant children in China, migrant education, lesson planning, teaching tips and classroom management techniques.

## Videolink

During the videolink program, while working from your home or office to teach English, you will also be supported by one on-site classroom teacher. Please ensure to have a strong and reliable internet connect during the lessons and a high proficiency in using Microsoft PowerPoint.

## Classroom

Our Classroom Teaching Program will be conducted any day from Monday to Sunday. You will be placed in convenient migrant schools and community centers in and around Shanghai.

## Applications

Applications close on **29th of April 2018 at 11:59PM AEST**. You will be contacted following the closing date. General enquiries about the 2018 Shanghai Classroom Teaching Program should be directed to the ACYA National Education Director, Sandy Qiao, education@acya.org.au.

Please fill out the following Application form to be considered:

[ninja_form id=8]