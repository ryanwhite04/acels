{
  "title": "ACYA Announces 2018 AGM",
  "author": "ACYA",
  "date": "2018-01-11T01:45:43+00:00",
  "image": "",
  "categories": [
    "Media Release",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Australia-China Youth Association"
  ],
  "resources": []
}
The Australia-China Youth Association (“ACYA”) is excited to announce details of the upcoming 2018 Annual General Meeting (“AGM”):

- &#x1f4c6;	10:00am CST / 1:00pm AEDT on **Sunday 4 February 2018**
- &#x1f4cd;	Attendance will be via **linked conference call** (registration link below)
- &#x1f465;	All National and Chapter Executives and ACYA members are welcome!

To reduce the number of connections, ACYA members in the same city or chapter are encouraged to gather in a communal location and share a computer to join the call.
To join your local gathering, [contact your local chapter president](http://www.acya.org.au/chapters/) and register your attendance below.

<!--more-->

## Why attend the AGM

As the only apolitical non profit run for members, by members working to actively foster a transnational community of Australian and Chinese youth,
attendance at the AGM is the perfect opportunity for members to participate in and be informed of the full scope of ACYA’s achievements in 2017,
as well as the strategic direction to be charted in 2018.

The principal aims of the AGM are:

- To provide full operating, management, and financial reports of the 2017 National Executive Committee’s undertakings to members.
- To approve proposed changes to the ACYA Constitution and governance structure
- To elect the members of the 2018 National Executive Committee.

A full agenda will be distributed to all registered attendees prior to the commencement of the AGM

## ACYA National Executive Elections

A key function of the AGM is to elect a new executive.
All current ACYA National Executive Committee members, National Directors, and Chapter Presidents are eligible to vote.
All registered attendees will be sent applicant and voting information along with the AGM agenda one week prior to the commencement of the meeting.

### Elected Positions (i.e., voted for at the AGM)
- [National President](http://acya.org.au/positions/national-president)
- [Managing Director](http://acya.org.au/positions/managing-director)
- [National Secretary](http://acya.org.au/positions/national-secretary)
- [National Treasurer](http://acya.org.au/positions/national-treasurer)
- [Australia Manager](http://acya.org.au/positions/australia-manager)
- [China Manager](http://acya.org.au/positions/china-manager)
- [Portfolio Manager](http://acya.org.au/positions/portfolio-manager)
- [Alumni Engagement Manager](http://acya.org.au/positions/alumni-engagement-manager)

### Appointed Positions (i.e., selected by the incoming National Executive)
- [Careers Director](http://acya.org.au/positions/careers-director)
- [Education Director](http://acya.org.au/positions/education-director)
- [People-to-People (P2P) Director](http://acya.org.au/positions/people-to-people-director)
- [Business Development Director](http://acya.org.au/positions/business-development-director)
- [Publications Director](http://acya.org.au/positions/publications-director)
- [Translations Director](http://acya.org.au/positions/translations-director)
- [Media Director](http://acya.org.au/positions/media-director)
- [IT Director](http://acya.org.au/positions/it-director)

Various other roles will be open for application following the AGM and may include:

- Portfolio Manager Secretary
- Australia and China Manager Secretaries
- Project Officers
- Translations Officers

## How to Apply

All applications should be sent by email, with a CV and cover letter, to the ACYA Returning Officer [elections@acya.org.au](mailto:elections@acya.org.au) by **11.59pm CST on Thursday 1st February 2018**.
Applicants are welcome and indeed encouraged to apply for multiple roles; if you do so, please state your order of preference among the roles you have applied for.
As ACYA continues to grow beyond its 6000+ membership base spread across 25 Chapters in Australia and China, it will look to the passion, ability and enthusiasm of its executives to lead the only youth-administered NGO in Australia-China affairs that is run by members, for members.
We strongly encourage applicants from all backgrounds to apply and look forward to seeing you at the AGM.

## Register to Attend the 2018 AGM

To reduce the number of connections, ACYA members in the same city are encouraged to gather in a communal location and share a computer to join the call. To join your local gathering, contact your local chapter president and register your attendance below:
Fields marked with an * are required
If you would like to take part in our event, please fill in your details in this Event Registration Form below and you will be automatically registered.