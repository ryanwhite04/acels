{
  "title": "ShineWing Australia: ACELS Brisbane 2018 Partner",
  "author": "ACYA",
  "date": "2018-07-23T08:27:45+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "ShineWing Australia"
  ],
  "resources": [
    {
      "title": "07-24_ShineWing",
      "type": "image",
      "src": "07-24_ShineWing.jpg"
    }
  ]
}
ACYA is excited to announce [ShineWing Australia](http://www.shinewing.com.au/) as a partner of [ACELS Brisbane 2018](/acels/brisbane-2018). 

Over four days, the Brisbane Summit will explore the skills and knowledge necessary to navigate this dynamic bilateral relationship. As part of the programme, ACYA and ShineWing will deliver the ShineWing ACELS Gala Dinner where delegates will be invited to celebrate new insights and friendships.   

<!--more-->

> Following a successful partnership at last year’s Melbourne Summit, ACYA is delighted to partner with ShineWing Australia once again.
> 
> We are grateful for ShineWing Australia’s generous funding, and I look forward to continued collaboration in future!
> -- Roger Lee, ACYA National President.

![ShineWing Australia](https://i1.wp.com/www.acya.org.au/wp-content/uploads/2017/06/ShineWing.png?zoom=1.399999976158142&w=525)

With 80 years of operation in the Australian business landscape, ShineWing Australia is a leading international accounting firm with a strong reputation in providing high-quality assurance, business advisory, corporate finance and wealth management services to their clients. Further, 

The ShineWing Australia member alliance with ShineWing International gives their clients access to ShineWing China – the largest indigenous Chinese domestic accounting practice, creating opportunities in specialist industries that provide local knowledge and real connections.
