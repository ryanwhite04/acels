{
  "title": "Alipay in Australia ",
  "author": "Jenny Malloy",
  "date": "2018-07-29T20:00:33+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Alipay",
      "type": "image",
      "src": "Alipay.png"
    }
  ]
}
Alipay has succeeded in China because it foresaw the future of payment and business to consumer interaction, but does it translate as well in the Australian context?

<!--more-->

Alipay was launched by Alibaba in 2004 in China as an online payment method for Chinese consumers to pay securely on Alibaba’s eCommerce sites. Since then, it has grown to become a USD$50 billion-dollar company, with almost 70% of all online payments in China being made via Alipay. 

The success of Alipay resides in its ability to fill a much needed area of Chinese consumerism, safe online payments. Alibaba identified that Chinese consumers were doubtful of the security of online payment in the context of weak consumer protection laws and regulations. Thus, Alibaba introduced safety into the online payment equation with Alipay’s guarantee for transaction by not releasing payment to the seller until the consumer expresses satisfaction over their purchase. This disruptive innovation in the form of a classic back scratcher solution exploited the desire for safe online transactions in an environment where state-owned financial institutions were slow to respond to a consumer desire for mobile payment.

In Australia, Alipay has become the bridge for more convenient and secure transactions for Chinese tourists and international students by allowing them to use their preferred method of payment. Furthermore, Alipay in Australia has the additional features of currency conversion and a “discovery map” to allow Alipay users to identify stores that allow them to pay using Alipay. With an increasing number of Australians and other foreigners beginning to use Alipay while they are in China, there may be need for an evolution of paying platforms such as PayPal to account for this.