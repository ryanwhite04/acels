---
title: The University of Queensland - ACELS Brisbane 2018 Partner
author: ACYA
date: 2018-07-27T13:14:27+08:00
featured_image: Confucius Institute.png
categories:
  - ACELS
locations:
  - Australia
  - Brisbane
  - Queensland
organisations:
  - Confucius Institute
  - The University of Queensland
---

ACYA would like to acknowledge [The University of Queensland](www.uq.edu.au/) (UQ) as [ACELS Brisbane 2018](/acels/brisbane-2018) Partner.

<!--more-->

The University of Queensland (UQ) is located in St Lucia, Brisbane, and hosts the biggest ACYA Chapter in Australia, with over 600 members. UQ is a research-focused institution and a university in the world’s top 50 and UQ's innovations have made and continue to make genuinely life-changing advances.

As an ACELS Brisbane 2018 Partner, the [Confucius Institute of UQ](confucius-institute.centre.uq.edu.au/) has funded the trip for our prestigious keynote speaker, Professor Chen Hong, Director of the Australian Studies Centre at East China Normal University. ACYA is highly appreciative of the support provided by UQ and the Confucius Institute toward the Summit.

<img src="Chen Hong.jpg" style="width: 160px" alt="Chen Hong" title="Chen Hong" />