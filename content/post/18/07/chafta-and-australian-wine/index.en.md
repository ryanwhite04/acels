{
  "title": "ChAFTA and Australian Wine",
  "author": "Edward Satchell",
  "date": "2018-07-22T20:00:58+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ChAFTA and Australian Wine",
      "type": "image",
      "src": "ChAFTA-and-Australian-Wine.jpg"
    }
  ]
}
Amidst the ongoing diplomatic frostiness between Australia and China, this year Australian wine exports surpassed the value point of $1 billion.

The above growth in the Australian wine market flows from the benefits of the China-Australia Free Trade Agreement (ChAFTA) which entered into effect on 20 December 2015 and will see tariffs of 14-20% on Australian wine imports eliminated by 2019. The benefits of free trade and a developing Chinese wine market have the potential to benefit many wine producing regions. 

<!--more-->

The strong Chinese preference for red wines has helped, shiraz and cabernet sauvignon variants make up the bulk of Australian exports. This contrasts to the Australian domestic market which has a strong balance between red and white, with sauvignon blanc the most popular wine in the Australian domestic market.

Whilst free trade has helped Australian exports, France still has the upper hand. Australia is the second largest exporter of wine to China after France. French wine has an elite reputation in China compared to the newer world varieties. This seems related to the Chinese consumer’s sentimental and romantic attitude towards wine. For example, wine exported to China is much more likely to use corks rather than screw tops. 

While Australia may pride itself on having the best wine in the world (Australian wine consumption is 75% Australian produced) there is still a way to go before the Chinese market concurs. 