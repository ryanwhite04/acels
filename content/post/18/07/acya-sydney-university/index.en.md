{
  "title": "ACYA Sydney University",
  "author": "Melanie Raymond",
  "date": "2018-07-08T20:00:27+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Newsletter"
  ],
  "tags": [
    "Chapter Focus"
  ],
  "locations": [
    "Australia",
    "New South Wales",
    "Sydney"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "Chapter Focus - ACYA Sydney University",
      "type": "image",
      "src": "Chapter-Focus-ACYA-Sydney-University.png"
    },
    {
      "title": "ACYA Sydney University",
      "type": "image",
      "src": "ACYA-Sydney-University.jpg"
    }
  ]
}
ACYA Sydney University’s ‘Speed-Friending/Language Exchange’ event was by far one of our most successful events this semester, not just in terms of turnout, but also the value it provided to our membership. It essentially followed the same structure as Speed-Dating, an equal amount of people sitting on either side of the table, with each person talking to their opposite counterpart for 5 minutes. 

<!--more-->

The idea of the event was designed to address, in part, a reluctance of our new members to open up and socialise at our previous events. We wanted to fix this. 

By facilitating discussion with a conversation starter sheet, creating a one-on-one platform for discussion, and strict time pressure, attendees interacted and gradually became more and more comfortable with each other. Attendees were also encouraged to speak in the language they were learning, for example, one person speaking in Mandarin, and the other responding in English. 

Ultimately, the event resulted in people exchanging WeChat and Facebook details to stay in contact with new friends and practice their language skills later in the semester. What a successful event!