{
  "title": "King & Wood Mallesons: ACELS Brisbane 2018 Supporter ",
  "author": "ACYA",
  "date": "2018-07-25T22:06:58+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "King &amp; Wood Mallesons"
  ],
  "resources": [
    {
      "title": "07-26_KWM",
      "type": "image",
      "src": "07-26_KWM.jpg"
    },
    {
      "title": "KWM",
      "type": "image",
      "src": "KWM.png"
    }
  ]
}
ACYA would like to acknowledge the generous support of [King & Wood Mallesons](http://www.kwm.com/en/) (Brisbane) in sponsoring the [Australia-China Emerging Leaders](/acels/brisbane-2018) Networking Evening on 26 July 2018. 

<!--more-->

At this event, the delegation — 70 students and young professionals engaged in Australia-China relations — will be hosted at KWM’s Brisbane Offices to mingle with leaders in the legal, business and government spheres. Delegates will also hear from a KWM Partner who will officially open ACELS BNE18.

ACYA is grateful to KWM for their long-standing support of ACELS in both China and Australia, and we look forward to a continuing relationship.

<img src="http://www.acya.org.au/wp-content/uploads/KWM-300x83.png" alt="" width="300" height="83" class="alignleft size-medium wp-image-27908" />

Recognised as one of the world’s most innovative law firms, King & Wood Mallesons offers a different perspective to commercial thinking and the client experience. With access to a global platform, a team of over 2000 lawyers in 27 locations around the world works with clients to help them understand local challenges, navigate through regional complexity, and to find commercial solutions that deliver a competitive advantage for our clients.