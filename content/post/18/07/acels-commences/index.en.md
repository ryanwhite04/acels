{
  "title": "ACELS Commences",
  "author": "ACYA",
  "date": "2018-07-26T22:16:16+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Australia",
    "Brisbane",
    "Queensland"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "Networking Dinner",
      "type": "image",
      "src": "Networking-Dinner.jpg"
    },
    {
      "title": "Amazing Race 2",
      "type": "image",
      "src": "Amazing-Race-2.jpg"
    },
    {
      "title": "Amazing Race",
      "type": "image",
      "src": "Amazing-Race.jpg"
    }
  ]
}
ACELS was off to a racing start today with the delegates discovering both Brisbane and each other before ending with a classy canape filled Networking dinner.

<!--more-->

![Amazing Race 2](/wp-content/uploads/Amazing-Race-2.jpg)

The Delegates checked-in to Nomads Brisbane Hotel around noon and barely had time to settle in before embarking on the first event, Discover Brisbane, where they competed to see which group could capture the best photos at various tourist attractions around the city such as the Cathedral of St Stephen and Queensland Parliament.

![Amazing Race](/wp-content/uploads/Amazing-Race.jpg)

The Networking Night at Waterfront Place was kindly hosted later that night by Leading law firm [King & Wood Mallesons](www.kwm.com/en/).

The delegates, dressed in their most formal attire, were able to socialize with representatives from the Queensland Treasury, DLA Piper, Westpac, and of course King & Wood Mallesons.

![Networking Dinner](/wp-content/uploads/Networking-Dinner.jpg)

> I'm eager to see if the rest of the summit can match today's energy and pace.
> -- Nancy Chen, ACELS Delegate

