{
  "title": "Wei Wei",
  "author": "Dawn Qiu",
  "date": "2018-05-02T20:28:12+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Alumni Spotlight",
    "Newsletter"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Wei Wei",
      "type": "image",
      "src": "Wei-Wei.jpg"
    }
  ]
}
Our next Alumni Spotlight participant is Wei Wei! Wei Wei is currently in her final year of study at UQ and was ACYA's previous Language and Translations Director. She reflects on her extensive internship experiences, her passion to assist the disadvantaged and her ongoing interest in promoting strong cross cultural understanding between Australia and China.

<!--more-->

## What was your graduation year?

I will graduate in the end of this year, December 2018.
 
## Which ACYA chapters were you a member of?

I was involved in the University of Queensland Chapter during the second year of my degree, in 2014. Then I began being involved in the National Translation Portfolio in 2015, before become the Language & Translations Director in the end of the year.
 
## What or who sparked your original interest in China? / What influenced you to pursue a career in the Australia-China space?

I’m Taiwanese-Australian and being an ethnic Chinese, I have always held a lot of pride over my culture and heritage. Moving to Australia at age 11 was difficult and I really wish the general Australia public has more understanding of us! Having majored in Chinese and English Translation & Interpreting, I was also hoping to sharpen my skills, and there seemed to be no better ways to combine all these aspirations by translation articles that promote cross-cultural understanding.
 
## What has been your experience of living and studying in China?

I have not studied in China but I have had a 2 month legal internship in Zhuhai, Guangdong. Bilingual ability and an international outlook are much valued skills. 
 
## Could you tell us about your career path leading up to today?

I am currently doing the final year of studying of my Bachelor of Laws / Arts degree in UQ. While I currently don’t have a paid job, I have had a lot of experiences doing internship and volunteering in many countries. I am hoping to lead a career in policy, particularly in DFAT as that would allow me utilize my linguistic skills and interests in international relations. But ultimately any career in policies that allow me to serve the community to improve human rights would be very meaningful to me.
 
## What does your current work involve? / What's one thing you love about your job?

I am currently involved in a few volunteering projects in addition to full time studying. One of them is being part of the Salvos Legal Humanitarian (subset of the Salvation Army) volunteer team and we assist unrepresented asylum seekers in court. The other one is a program run by St. Vincent de Paul, we pay weekly visits to a local refugee family to help them settle into Australia. I have been doing this latter project for so long that I also help to train incoming volunteers. You see, I am always interested in working with people from different cultural backgrounds! Also I find a lot of meaning in helping people to navigate their way in settling here, because I have gone through the difficult journey myself.
 
## Did you imagine this would be your career? What would first-year you think of what you are doing now?

I started university hoping to work in human rights so I haven’t deviated from my dream at all. It’s just that now I am even more ambitious and want to work on the larger picture of improving the society by contributing to the making of better laws and policies, instead of helping disadvantaged clients, one person and one family at a time. Also, there is nothing stopping me from also dabbing in human rights while working on policy – I have been doing a lot of pro bono legal work for disadvantaged members of the community and I plan to continue to do this after graduation. 
 
## Do you have any advice for students and recent graduates looking to work in your field? / What career or uni advice can you give current ACYA members? 

Law is a bit over-glorified and even though the same advice was fed to me without me having taken it very seriously when I was younger, I want to say it again. The large bulk of the work I have encountered so far is quite boring. Without a bigger vision it’s difficult to get through the small mundane details. Also, the competitiveness in the industry needs to be well-understood. Mental health issues have gained a lot of awareness in recent years, which is a good thing, because a lot of legal professionals and law students suffer from them. 
 
## What was your time in ACYA like? What were the most valuable things you got out of being part of ACYA? / What's your best memory from your involvement in ACYA?

Apart from gaining skills, it has got to be the friends and connections I have made. Although I have formally left ACYA for a year now, I am still good friends with a lot of people. I am also grateful for some of the opportunities ACYA has provided me, like interpreting for Jessica Rudd, entrepreneur and daughter of Australia’s former Prime Minister!
 
## Did you get a chance to take on any leadership roles within ACYA? If so, what benefits did you get from it?

I held the role of the National Language & Translations Director for approximately 18 months and was managing by far the largest team at the National level, supervising up to 20 Translation Officers. It was a fantastic opportunity for me to develop my leadership skills. I took this privileged position very seriously, dedicating around 10 to 15 hours each week while balancing full time studying, other volunteering activities, and even going on exchange and doing extensive travelling! If that didn’t teach me time management and prioritization skills, I don’t know what would. 
Since I am such a perfectionist I think my team felt quite pressured at times, but I led by example by being so dedicated and giving my 120%. I really did my best by giving each and every member individual attention and the room to grow and mature as translators. When I finally stepped down in the end of my term, I knew I had learnt so much about myself and have become confident of my ability to lead and inspire.
 
## What internships did you complete and what were they like?

A comprehensive answer for this question would be looooong! So I will just pick a few more important and meaningful internships I have done. I haven’t done any internship through ACYA but the experience I have gained in ACYA probably played an important part of landing me those opportunities. Many organisations ask applicants to demonstrate cultural awareness and sensitivity, so having worked in a cross-cultural organisation like ACYA was perfect.

For one semester, I interned in the Refugee And Immigration Legal Services (RAILS), the only community legal centre in Queensland that is dedicated to helping refugees and asylum seekers. I loved my experience there as it allowed me to help some extremely vulnerable people, learn their stories, and be inspired by their resilience.

Last year I also did a clinical placement in an NGO in Thailand called BABSEACLE, dedicated to raising legal awareness in the community. My colleagues and I designed materials to teach complicated legal information to people in simple, digestible and memorable ways. My cultural awareness allowed me to bring fresh perspectives onto the table that would probably otherwise be overlooked and I was very proud of that.

The translation and team-work skills I have gained from ACYA gave me the confidence to apply for a prestigious volunteering position, doing full time translation and interpreting on an UN-affiliated NGO cruise, Peace Boat, which pays for our board, food and travel. I was absolutely beyond myself when I found out that I was accepted! For 15 weeks I travelled the world on a cruise interpreting for public lectures about human rights, sustainability, and international relations. The workload was more than demanding but the experience was simply invaluable!