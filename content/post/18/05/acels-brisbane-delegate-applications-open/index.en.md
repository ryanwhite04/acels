{
  "title": "ACELS Brisbane Delegate Applications Open",
  "author": "ACYA",
  "date": "2018-05-28T16:35:39+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Australia",
    "Brisbane",
    "Queensland"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS Beijing Delegate Applications Open",
      "type": "image",
      "src": "ACELS-Beijing-Delegate-Applications-Open.jpg"
    },
    {
      "title": "Delegate Application Form",
      "type": "application",
      "src": "Delegate-Application-Form.pdf"
    }
  ]
}
The Australia-China Emerging Leaders Summit [ACELS](/acels) is heading to **[Brisbane](/acels/brisbane-2018)**! The Summit will be held on **26–29 July 2018** and will follow the theme of *Navigating the Contemporary Australia-China Relationship*. The Summit will cover a wide range of topics and bring together some of the brightest young minds engaged in the bilateral relationship.

[Click here](https://goo.gl/forms/cXBAgfKf0E2Iurq72) to apply, or if the Google Form doesn't work for you, fill out [this PDF Application form](/wp-content/uploads/Delegate-Application-Form.pdf) and email it to <mailto:acels@acya.org.au>.

Please note, successful delegates will be required to pay a Summit Fee (full amount TBC) - this will cover accommodation and most meals. Transport to and from Brisbane is at the delegate's own cost. 

Applications close on **Saturday 9th June 2018 at 11:59pm AEST**. If you have additional questions about the Summit, please read through the [FAQ](/acels/brisbane-2018/faq) or contact the Project Team at <acels@acya.org.au>.