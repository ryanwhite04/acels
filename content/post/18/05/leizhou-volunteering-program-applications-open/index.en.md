{
  "title": "Leizhou Volunteering Program Applications Open",
  "author": "Sandy Qiao",
  "date": "2018-05-08T02:29:58+08:00",
  "image": "",
  "categories": [
    "Opportunity",
    "Volunteering"
  ],
  "tags": [],
  "locations": [
    "China",
    "Guangdong",
    "Leizhou",
    "Zhejiang"
  ],
  "organisations": [
    "Leizhou College Students Volunteer Association"
  ],
  "resources": [
    {
      "title": "2016 Leizhou Trip Volunteer, Michelly Ramli with her students",
      "type": "image",
      "src": "2016-Leizhou-Trip-Volunteer-Michelly-Ramli-with-her-students.png"
    },
    {
      "title": "2017 Leizhou Trip Students were raising hands to answer questions in volunteer Cecilia ‘s English class",
      "type": "image",
      "src": "2017-Leizhou-Trip-Students-were-raising-hands-to-answer-questions-in-volunteer-Cecilia-‘s-English-class.png"
    },
    {
      "title": "2017 Leizhou Trip Volunteer, Zezan and Cecilia with their students",
      "type": "image",
      "src": "2017-Leizhou-Trip-Volunteer-Zezan-and-Cecilia-with-their-students.png"
    }
  ]
}
ACYA, in conjunction with the Leizhou College Students Volunteer Association (LZCVA), is pleased to be opening applications for our 2018 Volunteering trip in Leizhou, China. The theme for this year is *Happy Growth*. This will be a wonderful opportunity to teach English to rural middle school students and to experience life in regional China.

<!--more-->

[caption id="attachment_27717" width="300"]<img src="http://www.acya.org.au/wp-content/uploads/2018/05/2016-Leizhou-Trip-Volunteer-Michelly-Ramli-with-her-students-300x150.png" alt="2016 Leizhou Trip Volunteer, Michelly Ramli with her students" width="300" height="150" class="size-medium wp-image-27717" /> 2016 Leizhou Trip Volunteer, Michelly Ramli with her students[/caption]

The trip will take place from approximately the **15-26th July 2018**, and applications are open to anyone with a passion for sharing Australian culture, the Australia-China relationship, and teaching English to children in rural China.

[caption id="attachment_27718" width="300"]<img src="http://www.acya.org.au/wp-content/uploads/2018/05/2017-Leizhou-Trip-Students-were-raising-hands-to-answer-questions-in-volunteer-Cecilia-‘s-English-class-300x225.png" alt="2017 Leizhou Trip Students were raising hands to answer questions in volunteer Cecilia ‘s English class" width="300" height="225" class="size-medium wp-image-27718" /> 2017 Leizhou Trip Students were raising hands to answer questions in volunteer Cecilia ‘s English class[/caption]

> I am very happy that we did the Leizhou Teaching Volunteering program. It was a very memorable and unique experience to see how normal people in China live and study, especially when our host family would take us out to see Leizhou. We had very fun kids to teach and it was very heart warming to teach them.
I highly recommend anybody with an interest in understanding the real China to apply.”  
  -- Zezan Tam, 2017 ACYA Leizhou Volunteer.

[caption id="attachment_27719" width="300"]<img src="http://www.acya.org.au/wp-content/uploads/2018/05/2017-Leizhou-Trip-Volunteer-Zezan-and-Cecilia-with-their-students-300x225.png" alt="2017 Leizhou Trip Volunteer, Zezan and Cecilia with their students" width="300" height="225" class="size-medium wp-image-27719" /> 2017 Leizhou Trip Volunteer, Zezan and Cecilia with their students[/caption]

> It was definitely different from my previous China experiences. Staying in an old-style home with limited modern luxuries was definitely an experience worth having, as it allowed me to gain an insight into the lifestyle of people who live in less developed cities. It also made me truly appreciate the conveniences we may take for granted in Australia, such as hot water and sanitary bathrooms.
Meeting all the people there was definitely the highlight for me. They were so nice and supportive and were really interesting too. The students were also a lot of fun.” 
  -- Lewis Hong, 2016 ACYA Leizhou Volunteer.

In order to apply for the program, please fill out and submit the following form before **5pm 28th of May 2018**. You will be contacted following the closing date and if successful you will be sent a legal waiver to sign and return.

General enquiries should be directed to Sandy Qiao, Education Director, at <mailto:education@acya.org.au>.

[contact-form to="education@acya.org.au" subject="Leizhou Trip Application"]
  [contact-field label="Given Name" type="name" required="1"]
  [contact-field label="Family Name" type="name" required="1"]
  [contact-field label="Email" type="email" required="1"]
  [contact-field label="Date of Birth" type="date" required="1"]
  [contact-field label="Mobile Number" type="text" required="1"]
  [contact-field label="Address (Include country)" type="text" required="1"]
  [contact-field label="Wechat" type="text"]
  [contact-field label="Skype" type="text"]
  [contact-field label="Passport Number" type="text" required="1"]
  [contact-field label="ACYA Chapter" type="text"]
  [contact-field label="Nationality" type="checkbox-multiple" required="1" options="Australia National,Chinese National,Other"]
  [contact-field label="Chinese Language Proficiency" type="checkbox-multiple" required="1" options="Beginner,Intermediate,Advanced,Native"]
  [contact-field label="English Language Proficiency" type="checkbox-multiple" required="1" options="Beginner,Intermediate,Advanced,Native"]
  [contact-field label="Interested in blogging/ tweeting about your experience?" type="checkbox"]
  [contact-field label="Interested in providing a photographic essay of your experiences?" type="checkbox"]
  [contact-field label="Interested in writing an excerpt for ACYA Publications/ Communications?" type="checkbox"]
  [contact-field label="Do you have any commitments around 14th-20th July?" type="checkbox"]
  [contact-field label="What is the likelihood of your other commitments impacting your participation? (e.g no leave from work, winter intensive classes, previous plans etc) " type="textarea" required="1"]
[/contact-form]