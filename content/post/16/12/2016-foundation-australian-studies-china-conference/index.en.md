{
  "title": "2016 Foundation for Australian Studies in China Conference",
  "author": "ACYA",
  "date": "2016-12-29T07:00:48+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "img_1416",
      "type": "image",
      "src": "IMG_1416.jpg"
    }
  ]
}
<em>The Foundation for Australian Studies in China and the Australian Embassy Beijing recently sponsored 4 delegates selected by ACYA Education (2 from ACYA and 2 from the New Colombo Plan) to attend the fourth annual Foundation for Australian Studies in China Conference. After a particularly impressive round of applications was received, FASIC went on to sponsor an additional two ACYA delegates to attend the conference. The successful recipients were Chelsea Jacka, Megan Wee, Linda Ma, Ellie Wyatt, Georgia Kalyniuk, Mitchell Davies &amp; Alexander Trauth-Goik. </em>

<!--more-->

The 4th annual FASIC Australia Studies in China Conference, convened by Professor Greg McCarthy, BHP Billiton Chair of the Australian Studies Centre at Peking University(PKUASC), the Foundation for Australian Studies in China (FASIC), and gracefully hosted by the Centre for Australian Studies and the Centre for Oceanian Studies at Sun Yat-sen University, invited high-quality discussion on the topic of Sustainability: Social and Environmental Issues over the course of three days from 16- 18 November 2016.

This challenging theme, open to a broad range of interpretations in the context of Australia-China studies was met with great enthusiasm and innovation, transcending beyond sustainability of the Australia-China relationship itself, and encouraging deeper consideration of comparative approaches to dealing with social and environmental issues. Scholars from across the globe joined their Chinese and Australian counterparts, making the event a truly international gathering, consolidating a diverse range of perspectives and ideas.

Early in the conference, Professor Kent Anderson emphasised the importance of synthesising these ideas to develop more comprehensive studies in the China-Australia sphere, however the professor also highlighed a deficiency in the current level of collaboration between Chinese and Australian scholars, with evidence of low co-publication figures.This influenced me to consider what we could do as young scholars studying in China to facilitate further co-publication in the future. Other lectures which I found to be particularly stimulating include those by Professor Ghil'ad Zuckermann and Professor Lester-Irabinna Rigney who both addressed indigenous language revival and minority issues in Australia. Their Chinese counterpart, Professor Chang Chenguang, outlined similar issues concerning endangered languages in China, higlighting a potential area for comparative studies and cooperation to ensure the survival of these linguistic capabilities and the unique identities they embody. Finally, as a student of international relations, the presentations by Dr Alison Broinowski and Professor Pan Chengxin concerning Australia's foreign policy debate caught my interest, with Dr Broinowski indicating that Australia would be forced to make a choice between China and the United States soon, due to major changes in the region.

I sincerely appreciate the opportunity afforded to me as an ACYA student delegate, providing me with a chance to participate in such a thought- provoking conference and to discuss the Australia-China context, not only with leading researchers in the field, but also with my fellow ACYA student delegates, who share similar interests, experiences and aspirations as myself. It is my hope that such a valuable experience can be replicated for the 5th FASIC Conference, to provide greater opportunities for young scholars to expand their knowledge and develop their ideas.

<em>Written by Chelsea Jacka for the Australia-China Perspectives blog.</em>