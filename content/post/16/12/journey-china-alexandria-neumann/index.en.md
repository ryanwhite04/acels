{
  "title": "My journey to China: Alexandria Neumann",
  "author": "ACYA",
  "date": "2016-12-15T02:11:14+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "img_4361",
      "type": "image",
      "src": "IMG_4361.jpg"
    },
    {
      "title": "img_4615",
      "type": "image",
      "src": "IMG_4615.jpg"
    },
    {
      "title": "img_4563",
      "type": "image",
      "src": "IMG_4563.jpg"
    }
  ]
}
<em>Supported by the Australian Government New Colombo Plan, Griffith University student Alexandria Neumann shares insights on her recent experiences in China. From interning at the Peking University Australian Studies Centre to attending the 4<sup>th</sup> FASIC Conference on Sustainability, Social and Environmental Issues in Guangzhou, Alexandria's experience highlights the diversity of opportunities afforded to undergraduate students through the New Colombo Plan.</em>

<!--more-->

<a href="http://www.acya.org.au/wp-content/uploads/2016/12/IMG_4361.jpg" rel="attachment wp-att-24742"><img class="alignnone size-medium wp-image-24742 alignleft" src="http://www.acya.org.au/wp-content/uploads/2016/12/IMG_4361-225x300.jpg" alt="img_4361" width="225" height="300" /></a>

In the world that we live in today young people have never been more supported and encouraged to develop their skills, engage in activities akin to their interests and expand their knowledge of anything on which they set their mind. With the plethora of opportunities available, I have been privileged to participate in a program that will open my eyes to new perspectives and perhaps also greatly enrich my Australian university experiences more than I could have imagined. From late October 2016 to January 2017 through the New Colombo Plan initiative I will have the opportunity to immerse myself in Chinese culture through studying Chinese language at Beijing Foreign Studies University. This amazing opportunity will assist in fostering relationships between my home University, Griffith University, and Peking University.

Growing up as a young child in Australia, I was introduced to Chinese culture, food and customs through my grandmother who is Hong Kong Chinese. This atmosphere kindled my interest in learning Chinese in high school and I have continued to grow in knowledge and fluency throughout my law degree at Griffith University. The addition of Chinese to my law degree encouraged me to begin to understand the Australia-China relationship and the mannerisms of the partnership. As I became very interested in Chinese culture and improving my language I took every opportunity I got to travel to China. I had the privilege in participating in the Asia Future Fellows Program whereby Griffith University students underwent a cultural exchange with students from Peking University. It was eye opening to foster friendships and learn more about the differences and similarities Chinese and Australian people share in their lifestyle, government and culture. This was especially evident through growing in friendship with the Peking University students, as it initiated discussions surrounding university life, the pressures of study, societal systems and what they hope to accomplish. Attending the Australian Studies Conference at Peking University the following year and participating in a cultural awareness tour of China allowed me to see different aspects of Chinese culture I had never seen before by travelling to numerous cities including Xi’an, Datong and Pingyao. Thus, when I was given the opportunity to study in China for a semester and work with Peking University, I could not say no.

When I recently arrived in Beijing I was both excited and nervous. This was the first time I had travelled independently to a completely different country where everything was so fascinating and strange to me. Settling into my university dormitory and beginning my Chinese classes was a whole new experience, I can definitely say I am obtaining a full Chinese cultural experience living at Beijing Foreign Studies University. The room I am staying in is not what I expected, as it is probably half the size of my bedroom back in Australia even though I am sharing the room with my lovely Japanese roommate, fitted with a simple desk, limited closet space, and the thinnest mattress I have ever slept on. Using shared squat toilets every day is also something I am definitely not used to. After a month, I am happy to say I have grown accustomed to my dwelling and has given me an appreciation of what I have in Australia and a newfound admiration to those Chinese students who live in these conditions for their whole degree. Attending classes has been an equally interesting experience, as no English is used at all. While this has definitely assisted in improving my Chinese level, it was a struggle at the beginning to constantly listening and speaking in Chinese, let alone writing Chinese characters. Meeting all my students in my class whom come from different countries and diverse backgrounds has also been really interesting, as I have been able to foster a lot of new friendships with people who share the same passion of Chinese with me. I realised I am not only learning about Chinese culture, but also a plethora of others.

The unique and wonderful experiences does not stop at Beijing Foreign Studies University, as three weeks after arriving to Beijing I was able to commence my internship at Peking University. On the first day as I stepped into Peking University for the first time since I arrived I was full of excitement (after braving the congested Beijing subway system and failing to find the right exit several times). It was there I met Professor Greg McCarthy, his assistant Howard Wong and the other interns. Although I felt the least experienced being the youngest, I was warmly welcomed and my capacity to contribute and expand on the forum relating to Australia-China relations was realised.

The Peking University Australian Studies Centre is committed to maintaining and investing in the Australia-China relationship through research, conference facilitation and soft-power communication. The forums produced by the Australian Studies Centre ultimately assists to strengthen the growth of this relationship. From the beginning of my internship it was clear that the work completed by the centre achieves this. I am currently assisting Professor Greg McCarthy’s Australian studies class where Chinese students obtain a unique perspective on contemporary Australia by focusing on the viewpoint of Australia’s leaders. Through critical assessment of different sources, the students can gain a snapshot into Australian culture and society. As an intern I assist with facilitating discussions and providing perspectives as an Australian on the issues talked about. I find it very interesting listening to the ideas espoused by the Chinese students and deliberating with them on certain issues. The Chinese student’s unique mentality surrounding these issues is interesting as they possess a different way of thinking and privilege some ideals over others. As it has become evident to me, the Chinese value honour and privilege the wellbeing of society over individuals. For example, the notions they possessed surrounding the handing down of a case that convicted a Chinese-Australian for manslaughter were different to mine, as the fact it was committed under immense pressure was not an excuse according to the Chinese students. Discussing these societal issues I believe is very important as those soft-power links can be created which can contribute to the relations shared between Australia and China.

Quite recently I had the pleasure to attend the 4<sup>th</sup> FASIC Conference on Sustainability, Social and Environmental Issues at Sun Yat-Sen University in Guangzhou. It was an amazing opportunity not only to expand my knowledge on a variety of issues surrounding the Australia China relationship, but also to meet knowledgeable people within this area. Guangzhou provided a lovely atmosphere with its warm weather (a stark contrast to the current snowy weather and gusty winds in Beijing), peaceful surroundings experienced during afternoon walks by the lake and delectable food that consisted largely of delicious seafood. However, having the opportunity to experience another culture was one thing, but providing assistance at this conference and working with the other interns definitely added to my repertoire of experiences working with others, such as organising buses for the attendees to return to the airport. Without having this position as an intern, an opportunity like this would have definitely been out of reach.

Furthermore, as an intern I will have the opportunity to pursue an independent research topic of my own choosing. Being given this platform to contribute through my own voice to the area of Australia and China is indeed exciting. As environmental sustainability is a pertinent issue effecting the world today, and another one of my interest areas, I have decided under the supervision of the Australian Studies Centre to conduct research concerning the current climate change situation in both Australia and China. The Paris Conference was just held last year and marked a significant stance in combating climate change due to the willingness of numerous countries, including Australia and China, to initiate action in relation to decreasing greenhouse gases. The Paris Conference treaty set targets and also proposed that all signatory nations initiate policies in relation to the targets imposed. Thus, my research will encompass the policies implemented by both Australia and China and assess them.

As I begin my journey in China, it is very exciting to know that I will not only be learning about China and their own unique culture and customs, but I will also possess the capacity to expand my knowledge concerning the Australia China relationship, a relationship that is very close to me. Working at the Peking University Australian Studies Centre will ultimately allow me to achieve both these aims, as well as presenting me with international experience with a diverse range of people which will provide me with a solid and valuable foundation to propel me into my chosen future career in Law and Australian-Chinese relations.

<em>This piece was written by Alexandria Neumann for the Australia-China Perspectives Blog.</em>