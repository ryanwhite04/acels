{
  "title": "2016 ACYA-ACRI Internship Commences",
  "author": "ACYA",
  "date": "2016-02-11T04:21:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACRI_StuartFuller_064",
      "type": "image",
      "src": "ACRI_StuartFuller_064.jpg"
    }
  ]
}
[caption id="attachment_23641" align="aligncenter" width="300"]<img src="http://www.acya.org.au/wp-content/uploads/2016/02/ACRI_StuartFuller_064-300x207.jpg" alt="Stuart Fuller of Law Firm King+Wood Mallesons with Bob Carr at UTS ACRI seminar in Sydney. Photo Jeremy Piper" width="300" height="207" class="size-medium wp-image-23641" /> Stuart Fuller of Law Firm King+Wood Mallesons with Bob Carr at UTS ACRI seminar in Sydney. Photo Jeremy Piper[/caption]

The Australia-China Relations Institute (ACRI) at the University of Technology Sydney has welcomed two interns from the joint ACYA-ACRI Internship Program. ACYA is proud to partner with ACRI for this internship program, which supports students with an economics background to undertake a collaborative research project in Australia’s only think-tank devoted to the bilateral relationship, with a focus on economics and business. 
Cecilia Ren and Marilyn Pan, the 2015 ACYA-ACRI interns, have commenced an Australia-China focused economics research under the supervision of Professor James Laurenceson, a leading authority on China’s economy at the ACRI.
Cecilia is researching the potential worth of Australian exports to China in 2025 across all sectors, and will be co-authoring a research report with Professor Laurenceson and an opinion-editorial piece.
Cecilia’s project, entitled ‘The level and pattern of Australia’s exports to China in 2025’, foretells the impact of China’s middle class on agricultural and services markets in a post-resource boom era and will involve the use of rigorous benchmarks.
Marilyn plans to explore the spillover effects of China’s macroeconomic policies on Australia, in conjunction with Professor Fabrizio Carmignani at Griffith University and Professor Laurenceson, to produce a co-authored academic journal article.
Her research project, entitled ‘The macroeconomic impact on Australia of output and policy shocks in China’, will bypass assumptions and anecdotes by using econometric methods to study the impact of China’s policies.
In addition to these collaborative research projects, the new ACYA-ACRI interns will assist Professor Laurenceson with his articles for The Australian Financial Review, in conducting other research and the organisation of ACRI’s regular events program.
We wish them all the very best for their internship!

More information about this joint internship program can be found here: http://www.acya.org.au/careers/acri-internship/.

