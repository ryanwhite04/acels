{
  "title": "ACELS Shanghai Media Release",
  "author": "ACYA",
  "date": "2016-02-17T00:37:11+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS",
    "Media Release"
  ],
  "tags": [],
  "locations": [
    "Shanghai"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS-Webpage-banner",
      "type": "image",
      "src": "ACELS-Webpage-banner.png"
    }
  ]
}
In 2016, the Australia China Emerging Leaders Summit (ACELS) will be hosted in Shanghai from the 19th to the 21st of February. This will be the first time ACELS has been held in China.

ACELS is an exciting collaboration between the Australia-China Youth Association (ACYA) and the brightest minds in the Australian-Chinese space.

<!--more-->

As a forum that targets youth development, ACELS seeks to facilitate the sharing of ideas amongst delegates on the opportunities and challenges of the Australia-China relationship.

Speakers at the Summit come from diverse backgrounds with expertise in different fields, but all with a common interest in Australia’s bilateral relationship with China.

Delegates will have many opportunities to engage and network with speakers and other key guests during workshops, networking and social events.

ACELS 2016 is funded by a $25,000 grant from the Department of Foreign Affairs under the Australia China Council Grants Program in 2015.

ACELS’ sponsors from last year include: the Australia-China Council, Wines By: Geoff Hardy, the Australia China Business Council and the University of Sydney’s China Studies Centre.
