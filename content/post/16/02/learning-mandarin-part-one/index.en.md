{
  "title": "Learning Mandarin Blog Series Part One: Learning How to Learn Chinese",
  "author": "ACYA",
  "date": "2016-02-03T08:37:27+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "images",
      "type": "image",
      "src": "images.jpeg"
    },
    {
      "title": "k4",
      "type": "image",
      "src": "k4.jpg"
    }
  ]
}
<i>“Every battle is won before it is fought.”
Sun Zi, the Art of War </i><a href="#f1">[1]</a>
<p style="text-align: justify;">As a tennis novice, more than once, I have found myself standing at the baseline, racquet in position, only to find the ball whizz past my ear in an ace shot.</p>
<p style="text-align: justify;">I had a similar experience when I first started to learn Chinese. Like many foreigners, I was shocked by and unprepared for the velocity of the information hurtling at me in the learning environment. This blog series is designed to provide a strategy for anybody learning or thinking about learning Chinese. Or rather, it is designed to help you formulate your own strategy for learning; to learn how to learn.</p>
<p style="text-align: justify;">I studied a Diploma of Languages in Chinese alongside my arts degree at Melbourne University. The Diploma of Languages in Chinese is offered by many Australian universities and means that you can study Chinese alongside any other degree. University classes in Chinese are particularly rigorous, but class sizes can be large and it may be difficult to get personalised feedback to your learning.</p>
<p style="text-align: justify;">This blog series is not just intended for university students, however, and studying at a Confucius Institute, Tafe or a private school of languages may have the advantages of offering smaller class sizes or a less onerous study schedule than a university.</p>
<p style="text-align: justify;">But before embarking on the study of Chinese, it is worth being aware of some of the unique challenges the language presents.</p>
<p style="text-align: justify;">The written script of Chinese consists of thousands of unique characters rather than an alphabet. Arguably, the characters are the single most difficult aspect of learning Chinese. David Moser, in the article <a href="http://pinyin.info/readings/texts/moser.html">'Why Chinese Is So Damn Hard'</a> painfully acknowledges the certain beauty and fascination of characters owing to their long and complex history, but laments the length of time it takes the learner before they come to appear as being more than random squiggles – and for more experienced learners, the length of time it takes to read a novel or newspaper in Chinese.</p>
<p style="text-align: justify;">Additionally, Chinese is a tonal language, and there are many homonyms with different tones. Chinese requires a finer tuning of the ears and voice to subtle differences than most other languages. This is humorously illustrated in the <a href="http://www.yellowbridge.com/onlinelit/stonelion.php">"Lion-Eating Poet"</a> poem, which is an entire poem consisting only of words pronounced “shi” but with different tones.</p>
<p style="text-align: justify;">These things shouldn’t scare you away from learning Chinese. If you go about it in the right way, working “smart” and not just hard, it is possible to reach an excellent standard of Chinese. And there are a myriad of reasons to study Chinese which easily compensates for the drawbacks.</p>
<p style="text-align: justify;">Firstly, learning Chinese gives you unique insight into Chinese culture, enabling you to engage with Chinese people in their native tongue and build rapport and friendships. For example, one of my earliest lessons in Chinese included learning how to ask someone to dinner, the lesson revealing that in Chinese culture one person will usually pay for the meal and this person will often be given a special place at the table. The wording of the common phrase (我请客) exposed subtle cultural differences and provided an understanding and appreciation of the history and people of China in a way that is not accessible without knowing Chinese. Such cultural idiosyncrasies make the language fun and exciting to learn.</p>
<p style="text-align: justify;">Learning Chinese can also be an important career investment. The Chinese language will play a vital part in Australia’s economic future. Already China is Australia’s biggest trading partner, with exports and imports of goods totalling <a href="https://dfat.gov.au/trade/resources/Documents/chin.pdf">$142 billion</a> in 2014. As for service industries, the Chinese are Australia’s largest source of <a href="http://www.smh.com.au/business/markets/australia-new-zealand-in-arms-race-for-chinese-tourist-dollars-20150929-gjxpz1.html">tourism spending</a> and there are more students from China <a href="http://www.iie.org/Research-and-Publications/Open-Doors/Data/International-Students/Leading-Places-of-Origin/2013-15">studying in Australia</a> than from any other country.</p>
<p style="text-align: justify;">In spite of the many benefits of studying Chinese, it is common for beginner students to struggle and decide they just cannot keep up with Chinese alongside other commitments. I myself had friends who decided to drop out after their first year of language study, asserting that it “wasn’t for them”. This is a pity, as if you can make it past the first hurdles and learn, say, 200 characters, Chinese subsequently becomes easier and more enjoyable. Learning the numbers from one to ten can take a lot of work, but once your brain gets used to the process and the sounds become more familiar, you can acquire larger and larger amounts of characters with more and more ease.</p>
<p style="text-align: justify;">What is really crucial is to know what to expect. You need to have a set of well-developed study techniques and habits. With these in place, the proverbial battle is already half won, and you will see your learning journey as being a process of continual refinement and improvement rather than bafflement and stress. In the coming series of posts I will share how to plan your language study, improve your memory and change your habits to enhance your Chinese learning experience.</p>
<a name="f1"></a>1. This is a popular paraphrase of the following aphorism: 夫未战而庙算胜者，得算多也；未战而庙算不胜者，得算少也. The general who wins the battle makes many calculations in his temple before the battle is fought. The general who loses makes but few calculations beforehand.

<i>Written by Jesse Turland. Jesse is completing the Bachelor of Arts at the University of Melbourne with majors in Asian Studies and Arabic. He is also studying a Diploma of Languages in Chinese. He has studied abroad in Nanjing, China.</i>