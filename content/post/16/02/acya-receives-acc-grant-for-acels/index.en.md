{
  "title": "ACYA receives ACC grant for ACELS",
  "author": "ACYA",
  "date": "2016-02-09T18:51:23+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [],
  "organisations": [
    "Australia-China Council",
    "Australia-China Emerging Leaders Summit",
    "Australia-China Youth Association"
  ],
  "resources": []
}
In 2015, ACYA received a $25,000 grant from the Department of Foreign Affairs as part of the [Australia China Council Grants Program](http://www.dfat.gov.au/acc/). This competitive Program aims to broaden and strengthen Australia-China relations in education, business, culture and the arts. Alongside ACYA, other 2015–16 grant recipients include the Sydney Symphony Orchestra, the National Library of Australia, Australia China Alumni Association, and more.     

The funds received by ACYA will be used to support the costs associated with the Australia China Emerging Leaders’ Summit (ACELS) – a flagship initiative for youth development in the Australia-China space. ACELS Delegates participated in an intensive schedule of forums, workshops, a varied speaker program, and networking sessions to better understand their role in shaping bilateral relations in the 21st century.

ACYA is excited to have the support of the Australia China Council in developing and watching the next generation of leaders emerge in this exciting Australia-China space.