{
  "title": "Learning Mandarin Blog Series Part Three: How to Train your Memory",
  "author": "ACYA",
  "date": "2016-02-18T03:26:31+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Screen Shot 2016-02-18 at 2.00.22 PM",
      "type": "image",
      "src": "Screen-Shot-2016-02-18-at-2.00.22-PM.png"
    },
    {
      "title": "Screen Shot 2016-02-18 at 1.52.14 PM",
      "type": "image",
      "src": "Screen-Shot-2016-02-18-at-1.52.14-PM.png"
    },
    {
      "title": "Screen Shot 2016-02-18 at 2.01.45 PM",
      "type": "image",
      "src": "Screen-Shot-2016-02-18-at-2.01.45-PM.png"
    },
    {
      "title": "Screen Shot 2016-02-18 at 2.15.50 PM",
      "type": "image",
      "src": "Screen-Shot-2016-02-18-at-2.15.50-PM.png"
    },
    {
      "title": "Screen Shot 2016-02-18 at 2.20.36 PM",
      "type": "image",
      "src": "Screen-Shot-2016-02-18-at-2.20.36-PM.png"
    }
  ]
}
<p style="text-align: justify;"><a href="http://www.amazon.com/Moonwalking-Einstein-Science-Remembering-Everything/dp/0143120530">Moonwalking with Einstein: The Art and Science of Remembering Everything</a> is a gem of a popular science writing book. In it, the author Joshua Foer recounts his surprising journey to winning the United States Memory Championship. Along the way, with wit and panache, he explores scientifically the way the human brain records and recalls memories.</p>
<p style="text-align: justify;">The key message of Moonwalking with Einstein is that memory capacity is not fixed or innate. It can be expanded and enhanced. It all depends on how you go about memorisation. This is one of the most important things for learners of Chinese to realise. What you need is an elegant and effective system.</p>
<p style="text-align: justify;">Memory normally decays along the <a href="http://sidsavara.com/personal-productivity/the-ebbinghaus-curve-of-forgetting">“curve of forgetting”</a>. The principle of this curve is that there is a steep decline in memory with respect to time. A student who relies on simply copying out a list of words is unlikely to retain them for very long. Chinese characters are a particularly abstract entity to memorise, so the curve of forgetting is initially very steep.</p>
<p style="text-align: justify;">The good news is that memorisation efficiency can be dramatically increased through improved techniques of memory representation and repetition.</p>
<p style="text-align: justify;">The title of Foer’s book 'Moonwalking with Einstein' comes from the fact that memory representation can be enhanced by the use of zany mnemonic devices. As part of the “speed cards” event in the USA Memory Championship, Foer needed to memorise the order of a randomly shuffled deck. To prepare for this, he developed his own mnemonic system for memorising cards. Michael Jackson moonwalking represented the king of hearts while Albert Einstein smoking a pipe was the three of diamonds. Though he viewed the deck for less than a minute, he was able to memorise its order by tying the 52 mnemonic images attached to the individual cards into a story . The item that was abstract and uninteresting (the card) was attached to something that was vivid and humorous (the mnemonic image). This is the core of a process known as elaborative encoding, and it goes to show why memorisation is not just a discipline, but also an art requiring creativity and imagination.</p>
<p style="text-align: justify;">How does one go about elaborative encoding when it comes to Chinese?</p>
<a href="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.00.22-PM.png" rel="attachment wp-att-23680"><img class="aligncenter size-full wp-image-23680" src="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.00.22-PM.png" alt="Screen Shot 2016-02-18 at 2.00.22 PM" width="521" height="125" /></a>
<p style="text-align: justify;">The above is taken from the excellent book <a href="http://www.amazon.com/Tuttle-Learning-Chinese-Characters-Revolutionary/dp/080483816X">Learning Chinese Characters</a> by Alison and Lawrence Matthews. It is a mnemonic for the character 机jī, used for “machine” or “device”.</p>
<p style="text-align: justify;">The first step is deconstruction. Just as Foer did not attempt to memorise the deck of cards as a single block, neither should you memorise a complex character as a single unit. It is more efficient to see the character as a combination of components. The next step is meaningful assembly. Create a memorable context in which the two components and the definition word appear together. This is what Alison and Laurence Matthews have done for “machine”.</p>
<p style="text-align: justify;">All serious learners of Chinese use elaborative encoding. There are many products available to help you, as a beginner, to develop your own praxis as you learn characters. The books <a href="http://www.amazon.com/Remembering-Simplified-Hanzi-Meaning-Characters/dp/0824833236">Remembering the Hanzi</a> by James Heisig, <a href="http://www.amazon.com/Tuttle-Learning-Chinese-Characters-Revolutionary/dp/080483816X">Learning Chinese Characters</a> by Alison and Laurence Matthews, and the cutely titled <a href="http://www.amazon.com/Chineasy-New-Way-Read-Chinese/dp/0062332090">Chineasy</a> by ShaoLan Hsueh are all worthwhile investments. The flashcard apps <a href="http://www.skritter.com/home">Skritter</a> and <a href="http://www.memrise.com/home/">MemRise</a> have features that allow users to incorporate mnemonics in their revision of characters.</p>
<p style="text-align: justify;">Whilst it can be interesting to learn these clever mnemonics, the most memorable will be ones of your own creation. For example, my mnemonic for the character 让 (pinyin: ràng, meaning: “allow”) involves picturing a student writing words (讠the radical for “words”) on the blackboard above (上 pinyin: shàng, meaning: above) the height that the teacher can reach. The teacher tells him “not allowed!”</p>
讠+ 上 =让

[caption id="attachment_23685" align="aligncenter" width="335"]<a href="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.15.50-PM.png" rel="attachment wp-att-23685"><img class="size-full wp-image-23685" src="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.15.50-PM.png" alt="Illustration by James Bravender-Coyle" width="335" height="278" /></a> Illustration by James Bravender-Coyle[/caption]
<p style="text-align: justify;">In formulating this mnemonic, I called to mind a particular teacher who, let’s say, did not have the natural build of a basketballer, and this vivid and humorous association makes the image particularly memorable to me.</p>
<p style="text-align: justify;">Flashcard programs like Skritter, <a href="http://ankisrs.net/">Anki</a> and MemRise contain space-based repetition algorithms to optimise the memory renewal process. Space-based repetition is different to “drill and kill” rote learning in that it involves reviewing material only when necessary. The beauty of these systems is that they keep track precisely of the user’s performance and assign only those characters than need to be reviewed, so that no character is overlearned or under learned.</p>
<p style="text-align: justify;">Confucius said “men’s natures are alike, it is their habits that carry them far apart" (性相近习相远). Although the previous post discussed planning and self-discipline, a few more words are warranted specifically about how to develop the habit of memorising and reviewing characters.</p>
<p style="text-align: justify;">The author <a href="http://www.amazon.com/gp/product/081298160X?ie=UTF8&amp;tag=randohouseinc6399-20&amp;linkCode=as2&amp;camp=1789&amp;creative=9325&amp;creativeASIN=081298160X">Charles Duhigg</a> has described the common properties of all human habits: a cue, a routine, a reward, connected in a self-reinforcing loop. This image illustrates my own habit loop for studying Chinese. Boarding my train to university in the morning is a regular and well-timed daily cue for the routine of using Skritter. The app provides a regular reward: an upward-sloping graph representing my progress motivates me to keep up the practice.</p>
<a href="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.20.36-PM.png" rel="attachment wp-att-23686"><img class="aligncenter size-full wp-image-23686" src="http://www.acya.org.au/wp-content/uploads/2016/02/Screen-Shot-2016-02-18-at-2.20.36-PM.png" alt="Screen Shot 2016-02-18 at 2.20.36 PM" width="316" height="174" /></a>
<p style="text-align: justify;">If you are struggling to embed the habit of memorising and reviewing vocabulary, consider your own habit loop. Are you cued to study regularly, and when your mind is fresh? Do you allow yourself to feel a sense of satisfaction at the small wins along the way? For advice on how to create a strong habit, consider a <a href="http://charlesduhigg.com/wp-content/uploads/2014/04/Flowchart-How-to-Create-a-Habit.pdf">flowchart</a><strong> created by Duhigg based on his book.</strong></p>
<p style="text-align: justify;">It is common for students to feel that they just can’t handle Chinese characters. Those who try these memory techniques, however, will be surprised to discover just how well their memory can work and how enjoyable the study of Mandarin can be.</p>
<i>Written by Jesse Turland. Jesse is completing the Bachelor of Arts at the University of Melbourne with majors in Asian Studies and Arabic. He is also studying a Diploma of Languages in Chinese. He has studied abroad in Nanjing, China.</i>

&nbsp;