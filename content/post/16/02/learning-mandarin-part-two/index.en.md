{
  "title": "Learning Mandarin Blog Series Part Two: The Importance of Planning Ahead",
  "author": "ACYA",
  "date": "2016-02-12T00:29:56+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<p style="text-align: justify;">Do you remember those TV ads for Industry Super Funds? They would entreat viewers to “compare the pair” of workers and their superannuation schemes. I don’t get a lot of excitement from talking about retirement planning, but let’s talk about comparative learning strategies.</p>
<p style="text-align: justify;">We’ll have two students. Same age. Same goal (learn Chinese!) Same level of prior exposure to the language (zero). Compare the pair.</p>
<p style="text-align: justify;">Student 1 makes a deliberate point to acquire the course outline and textbook before classes start. She marks down important dates in a calendar, and thumbs through the relevant textbook chapters to preview each week’s lessons in advance.</p>
<p style="text-align: justify;">Student 2 enrols in class without acquiring the course outline and textbook prior to her first lesson. She comes to class late and doesn’t expect this semester to be too demanding. This mindset means she is not troubled that the university bookstore has sold out of textbooks.</p>
<p style="text-align: justify;">Student 1 studies the weekly vocabulary with a flashcard app on her phone. She then blanks out the pinyin transcriptions of the chapter texts, so as to force herself to read the characters directly. When writing role plays, which is a common extension exercise in the tutorial hour, she drafts them in characters and uses a variety of new vocabulary and expressions.</p>
<p style="text-align: justify;">Student 2 does not use a flashcard system, but writes out the wordlists for each chapter until she feels she has them “down pat”. This enables her to grasp the simplest characters, but she struggles with the more complex ones (she can remember 听 but not 新). In the tutorial hour, she scribbles out her role plays in pinyin rather than characters and only uses the most basic phrases and vocabulary.</p>
<p style="text-align: justify;">In the final exam at the end of semester, Student 1 walks into the hall with confidence and determination. She knows she did well on the other major pieces of assessment. Upon opening the written exam paper she experiences a warm feeling of familiarity born of months of proactive learning.</p>
<p style="text-align: justify;">Student 2 walks into the exam hall with dread. The sunny days of the start of semester are gone and the exam season has approached. She knows she did poorly on the listening and oral tests. The last week has been spent on intense, coffee-fuelled revision in a bid to ensure that she can merely pass the exam and the subject.</p>
<p style="text-align: justify;">Clearly, there is a lifetime of difference between the two approaches!</p>
<p style="text-align: justify;">What’s tragic is that many students do actually rely on the ad hoc approach of student 2. This is not to pass judgement; I used to be one of them. I did very poorly in my first semester of Chinese—determined to enjoy my first semester of university a little too much. The irony is that the latter approach actually requires more time in the end. Although Student 1 puts in more hours at the beginning of the semester, Student 2 has to play catch up at the end. Languages, unlike other study, require consistent exposure and practice, and cannot be successfully learnt by the “cramming” style of many university students.</p>
<p style="text-align: justify;">Chinese lessons require a proactive approach because they are cumulative. They build one on another. The characters introduced in week 1 will be referred to in the texts for week 12 at the same time as new ones are being introduced. The best approach is captured by the maxim “an ounce of prevention is worth a pound of cure” (or in Chinese: 预防胜于治疗!) and that prevention consists of previewing and reviewing lesson content.</p>
<p style="text-align: justify;">The good news is that once you get past the first stages, you will start to pick up on patterns. After learning the first 100 characters or so, you will recognise common radicals, subcomponents of characters that give a clue to their meaning (e.g. The radical for “people” (亻) is found in 体 (body) and 住 (live)). Similarly, you will come to recognise patters of tones and will get faster at picking them up the more you are exposed to spoken Chinese.</p>
<p style="text-align: justify;">More good news is that an understanding of memorisation techniques and building good habits will go a long way in streamlining the learning process. These things will be addressed in subsequent posts. There are also many free resources and tools that can assist, and these likewise will be addressed. Still, all of these things require you to first adopt a proactive approach to learning that anticipates challenges.</p>
<i>Written by Jesse Turland. Jesse is completing the Bachelor of Arts at the University of Melbourne with majors in Asian Studies and Arabic. He is also studying a Diploma of Languages in Chinese. He has studied abroad in Nanjing, China.</i>

&nbsp;