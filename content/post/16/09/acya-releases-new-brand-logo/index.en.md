{
  "title": "ACYA Releases New Brand and Logo",
  "author": "ACYA",
  "date": "2016-09-12T08:43:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "acya-logo-stacked-1",
      "type": "image",
      "src": "ACYA-Logo-Stacked-1.png"
    },
    {
      "title": "acya-logo-horizontal",
      "type": "image",
      "src": "ACYA-Logo-Horizontal.png"
    }
  ]
}
The Australia-China Youth Association is proud to announce the release of our new brand and logo. The new logo represents ACYA's diverse base of both Australian and Chinese members, whilst still maintaining the look and feel of ACYA's previous brand and logo.

<!--more-->
<h2>A Logo to Represent ACYA Members</h2>
The logo was designed to introduce an Australian element to the original ACYA logo. While still based on the 和 character, which represents peace and harmony, the inclusion of the Commonwealth Star inside the right-hand side of the 和 represents ACYA's Australian members, and the commitment to a harmonious coming together of China and Australia.

和 is a common Chinese character with numerous meanings. It is most commonly pronounced <em>hé</em>. Used by itself, the character has the meaning of <em>and</em>. When used in compound words such as 和平 (<em>hépíng,</em> <em>peace</em>) and 和睦 (<em>hémù, </em><em>harmony</em>), 和 carries the meaning of <i>kind</i>, <em>peaceful</em> and <em>harmonious</em>.

The Commonwealth Star, also known as the Federation Star, has one point representing each of the six original states of the Commonwealth of Australia, and one point representing the Northern Territory, Australian Capital Territory and any other future states of Australia. It is found in the bottom left corner of the Australian flag.
<h2>The Result of a Comprehensive Design Process</h2>
The new brand and logo are the result of hard work by ACYA's <a href="http://www.acya.org.au/join-acya-2016/" target="_blank" rel="noopener noreferrer">design team</a>. Through a comprehensive consultation process, the new logo was successfully trialled at the Australia-China Emerging Leaders' Summits in both <a href="http://www.acya.org.au/wp-content/uploads/2016/05/03-30_ACELSPublication-1-1.pdf" target="_blank" rel="noopener noreferrer">Shanghai</a> and <a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Enriching-a-New-Generation-of-Australia-China-Literate-Youth.pdf" target="_blank" rel="noopener noreferrer">Sydney</a> this year. We are proud of our new brand and logo and would like to thank everyone involved in its creation.

To find out more about the new ACYA brand and logo, contact Xin Na Zeng, our National Media, IT &amp; Design Director <a href="mailto:media@acya.org.au" target="_blank" rel="noopener noreferrer">here</a>.