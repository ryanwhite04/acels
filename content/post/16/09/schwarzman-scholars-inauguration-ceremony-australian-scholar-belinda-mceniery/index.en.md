{
  "title": "Schwarzman Scholars Inauguration Ceremony | Australian Scholar Belinda McEniery",
  "author": "ACYA",
  "date": "2016-09-08T12:11:22+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "14303931_10210879244722381_1283233393_o",
      "type": "image",
      "src": "14303931_10210879244722381_1283233393_o.jpg"
    },
    {
      "title": "14284981_10210879244642379_1827728304_o",
      "type": "image",
      "src": "14284981_10210879244642379_1827728304_o.jpg"
    },
    {
      "title": "14247746_10210879244682380_291018950_o",
      "type": "image",
      "src": "14247746_10210879244682380_291018950_o.jpg"
    }
  ]
}
Schwarzman Scholars, a fully-funded year in China for the world’s most promising young leaders, is accepting applications until September 15, 2016. Anchored in an innovative 11-month professional Master’s Degree in Global Affairs at Beijing’s prestigious Tsinghua University, the Schwarzman Scholars experience includes unparalleled learning opportunities in and outside the classroom through extensive leadership training, a network of senior mentors, practical training/internship opportunities, and travel seminars around China. Schwarzman Scholars is designed to prepare leaders for a world where China plays a key role. Scholars will learn from world-class faculty and guest speakers about the emergence of China as an economic and political force through a dynamic core curriculum and concentrations in public policy; international studies; or business and economics.

We caught up with Belinda McEniery, recent recipient of the Schwarzman Scholarship, on her experience in the program.

<!--more-->

<strong>What were you most looking forward to about moving into Schwarzman College?</strong>

Definitely meeting people! There are 110 Scholars from around the world now living at Schwarzman College and while, all similar ages and at a similar stage in life, we will bring a diverse range of history and experience. From my experiences at international conferences, other participants (Scholars in our case) are always a great source of inspiration and education. There is also a Schwarzman Scholars Team on the ground in Beijing who have been working to make the first year a huge success - we have already met some of the academic faculty, had leadership training and started Mandarin language courses. We will also take a trip to the Great Wall for two nights this week - I can't wait to see what else they've got planned. Another benefit of the program is the suite of Advisory Board Members and Speaker Series - giving Scholars great exposure to those already active in global leadership. The Schwarzman College building is also absolutely amazing and has been designed to facilitate both formal and informal discussions between Scholars themselves, lecturers and guests with facilities including the auditorium, International Conference Centre and, importantly, the pub.

<a href="http://www.acya.org.au/wp-content/uploads/2016/09/14284981_10210879244642379_1827728304_o.jpg" rel="attachment wp-att-24280"><img class="aligncenter wp-image-24280" src="http://www.acya.org.au/wp-content/uploads/2016/09/14284981_10210879244642379_1827728304_o-300x225.jpg" alt="Schwarzman College" width="385" height="289" /></a>
<p style="text-align: center;"><em>Schwarzman College</em></p>
<strong>Had you been in touch with any other scholars before the programme? What was it like to meet so many highly accomplished people in such a short space of time?</strong>

We were all put in touch with each other following the formal announcement of Scholars in January so we have had a significant amount of interaction with each other across social media platforms including Facebook and WeChat. Everyone was very keen to begin discussions including ideas for the programme, travel plans and also current affairs topics. Meeting everyone in person over the last week has been so interesting (and a little exhausting)! We have all been getting involved in activities such as sports, discussion and debate clubs which has made remembering 109 other names and backgrounds a lot easier! It has also been very interesting to see the differing familiarity and experiences Scholars have had in China, which is not a requirement for the programme - we have some Scholars who have never been to China, others who have never left.

<a href="http://www.acya.org.au/wp-content/uploads/2016/09/14303931_10210879244722381_1283233393_o.jpg" rel="attachment wp-att-24279"><img class=" wp-image-24279 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/09/14303931_10210879244722381_1283233393_o-300x225.jpg" alt="14303931_10210879244722381_1283233393_o" width="387" height="290" /></a>
<p style="text-align: center;"><em>Schwarzman Scholars attend the Inauguration Ceremony</em></p>
<strong>What aspects of the Schwarzman experience do you anticipate will have the biggest impact on you?</strong>

Overall, it is definitely the opportunity to spend a year living in China - learning the language, getting exposure to the culture and travelling as much as possible. In terms of aspects it is very hard to choose! I hope that the Practical Training Programme (similar to an internship) at a Beijing-based institution will give me an insight into industries that I may wish to go into during my career and allow me to meet a variety of people living in Beijing. I am also really looking forward to having faculty live in the College with us including Larry Summers and Niall Ferguson (a documentary of his was one of the reasons I decided to study economics in undergrad). They will not only teach courses but also eat meals and have informal conversations with us on a daily basis.

<a href="http://www.acya.org.au/wp-content/uploads/2016/09/14247746_10210879244682380_291018950_o.jpg" rel="attachment wp-att-24281"><img class=" wp-image-24281 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/09/14247746_10210879244682380_291018950_o-225x300.jpg" alt="14247746_10210879244682380_291018950_o" width="282" height="376" /></a>
<p style="text-align: center;"><em>Tsinghua University Graduate Inauguration Ceremony</em></p>
<p style="text-align: left;"><em>Applications for the Schwarzman Scholars Program close Thursday 15 September.</em></p>
<p style="text-align: left;"><em>This interview was conducted by Siobhan O'Sullivan, Editor of the Australia-China Perspectives blog.</em></p>