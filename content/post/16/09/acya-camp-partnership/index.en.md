{
  "title": "ACYA Partner with CAMP to Support Young Chinese and Australian Leaders",
  "author": "ACYA",
  "date": "2016-09-27T08:51:48+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "camp-acya-web-banner-01",
      "type": "image",
      "src": "camp-acya-web-banner-01.png"
    }
  ]
}
ACYA is proud to announce our partnership with the <a href="http://www.australiachina.org" target="_blank" rel="noopener noreferrer">China-Asia Millenial Project (CAMP)</a>, an award-winning bilateral program for young leaders and innovators. The CAMP 2017 Program is a 100 day experience for 150 people under 35 made up of 3 components; a 5-day face-to-face summit in China, a 90-day online program and finally a 5-day face-to-face summit in Sydney.

ACYA members can <strong>save $100</strong> from the registration fee by using the promotional code "ACYA" when registering for CAMP.

<!--more-->
<h2>About CAMP</h2>
Calling all millennials with a passion for innovation and social change, the China Australia Millennial Project (CAMP) is on the hunt for young leaders and innovators to join our award-winning bilateral program in 2017. CAMP 2017 is now happening in China AND Australia! Over the 100 days, participants co-create new businesses and business solutions, level-up their professional skills and networks and forge deep friendships. CAMPers are globally connected, hyper-skilled and will change the world.
<h2>The 21st Century Toolkit</h2>
<em>Advanced intercultural collaboration, innovation and entrepreneurship.</em>

The program incorporates three distinct frameworks across the learning experience for every participant:
<div>
<ol>
 	<li><strong>INTERCULTURAL LEADERSHIP</strong> – Build the skills needed to forge high performing teams located in Australia and China.</li>
 	<li><strong>INNOVATION</strong> – via Stanford University’s d.school’s design thinking methodology.</li>
 	<li><strong>ENTREPRENEURSHIP</strong> – A blend of lean startup methodology and project management, experience what it takes to start something amazing.</li>
</ol>
<h2>Register Now!</h2>
Early bird applications are open until the 1st of October. Early bird applicants receive a further <strong>12% off</strong> the registration price if paid before the 31st of October. The "ACYA" promotion code entitles applicants to a <strong>$100 discount</strong> off<strong> </strong>the application price, regardless of their early bird status. <a href="http://www.australiachina.org/apply2017/" target="_blank" rel="noopener noreferrer">Apply today</a>!

</div>