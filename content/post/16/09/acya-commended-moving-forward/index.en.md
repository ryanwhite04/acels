{
  "title": "ACYA Commended in Moving Forward: Issues in Canada-China Relations",
  "author": "ACYA",
  "date": "2016-09-09T11:24:56+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "movingforward",
      "type": "image",
      "src": "movingforward.png"
    },
    {
      "title": "movingforwardcrop",
      "type": "image",
      "src": "movingforwardcrop.png"
    },
    {
      "title": "movingforwardcrop",
      "type": "image",
      "src": "movingforwardcrop-1.png"
    }
  ]
}
<h2>An Overview of Canada-China Relations</h2>
<em><a href="http://munkschool.utoronto.ca/ai/files/2016/02/Moving_Forward_Farooq_McKnight.pdf#page=105" target="_blank" rel="noopener noreferrer">Moving Forward: Issues in Canada-China Relations</a></em> is a collaborative analysis into the past, present and future of Canada-China relations. <em>Moving Forward</em> was collated and published by the <a href="http://munkschool.utoronto.ca/ai/" target="_blank" rel="noopener noreferrer">Asian Institute at the Munk School of Global Affairs</a> and the <a href="http://corn.groups.politics.utoronto.ca/" target="_blank" rel="noopener noreferrer">China Open Research Network</a> at the University of Toronto. It offers a "prospective blueprint for reviving and then deepening Canada-China relations", ranging from security and economy to soft diplomacy.

<!--more-->
<h3>ACYA Commended for Comprehensive Model of Engagement</h3>
<em>Moving Forward</em> spoke to ACYA Co-Founder Henry Makeham as part of a survey into the various models of youth engagement with China in English-speaking countries. The report discusses the activities and achievements of the ACYA Group, which includes the Australia-China Youth Dialogue, the Australia-China Young Professionals Initiative and the Engaging China Project. The authors state that "ACYA offers the most comprehensive model for youth engagement of the organisations surveyed", and recommend that Canada facilitate the creation of a youth organisation which emulates the ACYA model.

You can read the entire report <a href="http://munkschool.utoronto.ca/ai/files/2016/02/Moving_Forward_Farooq_McKnight.pdf#page=105" target="_blank" rel="noopener noreferrer">here</a>. We encourage Canadian youth to get involved with the initiative recommended in <em>Moving Forward</em>, and look forward to collaborating with such an organisation in the future!