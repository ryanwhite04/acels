{
  "title": "National Taiwan Normal University’s Mandarin Training Centre | Australian Scholar Jermaine Werror",
  "author": "ACYA",
  "date": "2016-09-21T04:06:51+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "picture1",
      "type": "image",
      "src": "Picture1.png"
    },
    {
      "title": "picture2",
      "type": "image",
      "src": "Picture2.png"
    }
  ]
}
Jermaine Werror is a previous recipient of the ACYA-MTC scholarship, which is now open for applications, closing October 12. For those who are interested in studying and developing their Chinese language skills, the ACYA -MTC scholarship offers the opportunity to live and practice Mandarin in Taipei city. The Mandarin Training Centre at the National Taiwan Normal University is a well-known institution that offers intensive study programs and has attracted students from all over the world, including former Australian Prime Minister Kevin Rudd. Jermaine shared his experiences as a scholar with us.

<!--more-->

<strong> What motivated you to major in Chinese and to study the language?</strong>

I chose to study Chinese because it is becoming an increasingly important language in today’s day and age. China is a key player and contributor to economic growth in Australia and Chinese (Mandarin) is the most widely spoken language in the world today. The ability to communicate in Chinese is an invaluable skill to have in business and in any profession and provides a competitive advantage to both graduates and professionals alike.

<strong> Were there any challenges or surprises living in Taiwan? How does it compare to studying in Queensland? </strong>

I did not find anything challenging or surprising about living in Taiwan. Taipei was incredibly easy and convenient to navigate and move around and I enjoyed living there. A key advantage of studying in Taiwan was the opportunity I had to completely immerse myself in the culture. The experience significantly assisted and accelerated my learning by allowing me to practice my Chinese everyday and understand a bit more about Chinese culture and history.

<strong> What was a typical day during the program like? Did the course help you build on your language skills? </strong>

I was enrolled in regular classes, which involved 2 hours of class and at least 1 hour of private study in the library (or alternatively, a cultural class i.e. calligraphy) every day. The class sizes were small and highly interactive giving each student adequate opportunity to engage in the learning process. The teachers were incredibly friendly, helpful and experienced too. The course helped me build on my language skills and gave me the confidence to communicate in Chinese with native speakers.

<strong> Finally - do you have any advice/ suggestions for future MTC attendees? (Application, learning, living related or otherwise!) </strong>

Firstly, I would advise future MTC attendees to familiarize themselves with traditional Chinese characters before attending MTC. Initially, I found it difficult to adapt to traditional characters. Since attending MTC however, I have developed a preference for traditional over simplified characters and I continue to use traditional characters today. Secondly, I would encourage students to embrace the entire experience and enjoy it to the fullest extent possible. Taiwan is an absolutely amazing place. Whether you’re an adventurer, food lover or shopaholic, Taiwan has just about everything for everyone.

<a href="http://www.acya.org.au/wp-content/uploads/2016/09/Picture2.png" rel="attachment wp-att-24336"><img class="size-medium wp-image-24336 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/09/Picture2-300x300.png" alt="picture2" width="300" height="300" /></a>

If you are interested in an immersive Mandarin learning experience, please download and fill out the application here: <a href="http://www.acya.org.au/education/scholarships/acya-mtc-scholarship/">http://www.acya.org.au/education/scholarships/acya-mtc-scholarship/</a>

This interview was conducted by Michelle Lu for the Australia-China Perspectives Blog.