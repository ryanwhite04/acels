{
  "title": "ACELS Shanghai 2016 Wraps Up",
  "author": "ACYA",
  "date": "2016-03-16T10:11:20+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Shanghai"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS 2016 Shanghai",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0286.jpg"
    }
  ]
}
Over an intensive weekend on the 19th to 21st of February, 2016, 45 enthusiastic youth delegates gathered together in Shanghai for the 2016 Australia-China Emerging Leaders Summit (ACELS). The first time the Australia-China Youth Association (ACYA) has hosted a youth conference in China, focusing on professional development and generating new project ideas to advance the bilateral relationship.

<!--more-->

As one of ACYA’s key initiatives for youth development, ACELS Shanghai exposed delegates to a rigorous schedule of high-level speaker sessions, peer discussions, networking events and concept development workshops. Presenters included Mr. Graeme Meehan, Australian Consul-General in Shanghai; Michelle Garnaut, CEO of M Group of Restaurants; Andrew Hogg, Regional General Manager (North Asia), Tourism Australia; Craig Aldous, Managing Director, Elders China; Steve Willett, Managing Director, Lend Lease Greater China Construction, and Danny Armstrong, CEO, NAB China. 

The delegates were representatives from the Australian Federal Government’s New Colombo Plan, Westpac Asian Exchange Scholars, Chinese Government Scholarship holders, Victorian Sichuan Government Scholarship holders, and young professionals from the ACYA membership network, in addition to other university students. 

Michelle Howie, New Colombo Plan Scholar commented on her [blog post](https://michelletominhye.wordpress.com/2016/02/21/australia-china-emerging-leaders-summit-shanghai-2015/) about ACELS,

> Over one whirlwind weekend I sat alongside fellow delegates from Sydney, Perth, Beijing, Nanjing and more. It was a mix of Aussie students who have studied abroad in Greater China, Chinese returned exchange students from Australia, and other youths with interest in bilateral business and language.

Key discussion points ranged from the exciting and varied opportunities available to Australian students in China, to the difficulties encountered by both country’s youth in successfully integrating into the other host country and gaining an in-depth social and cultural experience. These were further explored in workshops that gave delegates the opportunity to express their own thoughts and opinions regarding the Australia-China relationship. The key ideas were then developed into a tangible project via a structured and targeted process facilitated by the ACELS Project Team.

ACYA would like to acknowledge the support given to ACELS Shanghai by the [Australia China Council](http://dfat.gov.au/people-to-people/foundations-councils-institutes/australia-china-council/pages/australia-china-council.aspx), the M Group of Restaurants and the Department of Foreign Affairs and Trade (DFAT) in Beijing. ACYA is also grateful to the speakers and professional representatives from Herbert Smith Freehills, Qantas, EY Advisory China, Westpac, Hassell, Telstra, the New Colombo Plan Secretariat, Austrade, HNA Aviation and CPA Australia.

Students and young professionals interested in attending future Emerging Leaders’ Summits should check the ACYA website homepage and ACYA social media for updates on how to apply.

**Organisations interested in partnering with ACYA** to deliver the 2016-17 Emerging Leaders’ Summits can contact the National Business Development Director at <mailto:partnerships@acya.org.au>.

*With 24 chapters across China and Australia collectively forming a 6000+ strong cross-border community, ACYA is the sole NGO at the forefront of connecting like-minded Chinese and Australian youths who aspire towards building stronger ties between both nations. ACYA seeks to promote long-lasting relationships without borders, and provide opportunities from both sides to learn and mutually benefit under this unique bilateralism.*