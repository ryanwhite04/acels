{
  "title": "Post-AGM Media Release",
  "author": "ACYA",
  "date": "2016-03-04T09:27:10+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "AGM",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "AGM-General-Meeting-2016",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0596.jpg"
    }
  ]
}
The Australia-China Youth Association (ACYA) is pleased to announce the conclusion of the Annual General Meeting (AGM). The AGM was held on February 28, 2016. National Committee Executives, Chapter Committee Executives and interested members gathered for annual reports on management, operations, and finances of the organisation, as well as to vote for the 2016 National Executive Committee.

The 2016 National Executive Committee is comprised of passionate, committed, and talented individuals, together bringing a wealth of Australia-China experience to ACYA’s initiatives, projects and operations in the coming year. An exciting year awaits.

The 2016 ACYA National President is David Douglas. David was Managing Director in 2015 and previously led the University of Melbourne Chapter as Chapter President for two years. He will be commencing as a law graduate at Herbert Smith Freehills in early 2016.

David will be joined by Victor Luo as 2016 Managing Director. Victor was a key member of the Project Team for the 2015 Australia-China Emerging Leaders Summit in Sydney and was previously Chapter President of the University of New South Wales (UNSW) Chapter.

The other members joining the 2016 National Executive Committee include: Cynthia Yuan as National Secretary, Jonathon Glindemann as National Treasurer, Scott Gigante as Portfolio Manager, William Zhao as Australia Manager and Theo Stapleton as China Manager.

Jimmy Zeng, outgoing National President acknowledged the contributions of the 2015 National Executive Committee in executing a number of key initiatives such as the Australia-China Emerging Leaders Summit in both Sydney and Shanghai, an expanded scope of partnerships, and a marked improvement in the quality of products and services provided by ACYA to its members. 

Jimmy commented: “The Australia-China Youth Association’s membership, events, publications, and partnerships continue to grow in terms of scale and quality. It is the unique youth brand, bilateral network, and diverse talent pool that make this possible. The continued growth of this network and the connections it fosters will ultimately underpin the future of the bilateral relationship.”

