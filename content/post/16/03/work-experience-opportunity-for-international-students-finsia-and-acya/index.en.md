{
  "title": "Work Experience Opportunity for International Students: Finsia and ACYA",
  "author": "ACYA",
  "date": "2016-03-09T10:43:11+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Work Experience Opportunity",
      "type": "image",
      "src": "c3f08dfd-9dec-4066-940f-fe2a3162b68a.png"
    },
    {
      "title": "c3f08dfd-9dec-4066-940f-fe2a3162b68a",
      "type": "image",
      "src": "c3f08dfd-9dec-4066-940f-fe2a3162b68a-1.png"
    },
    {
      "title": "Work Experience Opportunity for International Students",
      "type": "image",
      "src": "c3f08dfd-9dec-4066-940f-fe2a3162b68a-2.png"
    }
  ]
}
ACYA & and Finsia collaborate to provide work experience for international students

The Australia-China Youth Association (ACYA) and the Financial Services Institute of Australasia (Finsia) are joining forces in 2016 to offer a Work Experience oopportunity exclusively for Chinese international students in Sydney. 

Finsia is the peak professional membership body representing the entire financial services industry.  With a membership that includes CEOs of major banks, executives from over half the ASX 200 and today and tomorrow’s leading fintech entrepreneurs, Finsia provides members unparalleled opportunities to get ahead in the world of finance.

As part of the partnership with ACYA, four Sydney-based international students will have the opportunity to contribute to the development and implementation of Finsia’s international student programs .Participants will also receive a complimentary Finsia student membership for 12 months.

Participants will be identified through a competitive selection process. Applications close at 11.59pm Australian Eastern Daylight Time (AEDT) on 13 March, 2016.

More information on the Work Experience Program and how to apply can be found here: http://www.acya.org.au/careers/finsia-work-experience/. 