{
  "title": "Learning Mandarin Blog Series Part Four: Resources for the Learning Journey",
  "author": "ACYA",
  "date": "2016-03-02T08:13:06+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Unknown",
      "type": "image",
      "src": "Unknown.jpeg"
    },
    {
      "title": "mandarin_2003767c",
      "type": "image",
      "src": "mandarin_2003767c.jpg"
    }
  ]
}
<p style="text-align: justify;">The final component to mastering the practice of Mandarin is access to quality resources. In this last post I share some resources that have helped me improve my Mandarin skills. As a more advanced learner now, some of these resources would have been extremely handy had I found them sooner.</p>
<b>Apps</b>
<p style="text-align: justify;"><a href="https://www.pleco.com">Pleco</a> is the premier dictionary app for Chinese. You can translate both ways: Chinese to English and English to Chinese. As well as the English definition, for each Chinese word there is a recording of a native speaker pronouncing it, a diagram showing the stroke order, and a replication of the character in its traditional form. Many words also have sample sentences illustrating their usage. If you add the ‘Chinese – Handwriting’ keyboard to your smartphone, you can draw in unknown characters to determine their meaning.</p>
<p style="text-align: justify;"><a href="http://www.skritter.com/home">Skritter</a> is the only flashcard app designed specifically for learning Chinese. Like all flashcard apps, it contains a space-based repetition algorithm, but Skritter is unique in that it has algorithms simultaneously tracking your ability to recall the pronunciation, tone and definition of a character as well as how to draw it. Also unlike any other app, it allows you to actually draw the characters on your phone screen and grades your performance automatically (although you can override this grading if you need to). Skritter requires a subscription account and the prices can be seen on the website.</p>
<p style="text-align: justify;">Both Pleco and Skritter are worthwhile acquisitions for beginner students.</p>
<b>Web Resources</b>
<p style="text-align: justify;"><a href="http://www.slow-chinese.com/">Slow Chinese</a> is a free podcast for learners focusing on contemporary issues but using simple vocabulary and very clear diction. It covers contemporary Chinese media topics and is particularly useful for improving one’s listening abilities. This podcast is more suitable for advanced learners.</p>
<p style="text-align: justify;"><a href="http://www.thechairmansbao.com/">The Chairman’s Bao</a> is a simplified online newspaper for learners, with entries tagged according to HSK ability level. The HSK (Hanyu Shuiping Kaoshi) is the test of Chinese proficiency, and the easiest articles on the site are marked HSK3.</p>
<p style="text-align: justify;"><a href="http://www.hackingchinese.com/">Hacking Chinese</a> and <a href="https://www.chineseboost.com/blog/">Chinese Boost</a> are two blogs started by students of Chinese looking at enhanced study methods. As you can tell from this series, I am a big fan of advice on learning how to learn, as it can act like a bicycle gear to deliver greater results with the same amount of effort. Both of these sites are highly recommended for all levels.</p>
<p style="text-align: justify;"><a href="http://openlanguage.com/">Open Language</a> is a subscription website that contains Chinese podcasts categorised into six difficulty levels from beginner to advanced. Accompanying these podcast lessons are dialogue breakdowns, vocabulary lists, expansion and grammar exercises, and tests such as dictation practice and multiple-choice quizzes.</p>
<b>Books</b>
<p style="text-align: justify;">The specialist bookstore <a href="http://www.chinabooks.com.au/index.cfm">China Books</a> has stores in the CBDs of Melbourne and Sydney. There is value in consulting different textbooks, grammar books and graded readers.</p>
<p style="text-align: justify;">Packed with useful tips as well as fascinating trivia, <a href="http://www.amazon.com/The-Chinese-Language-History-Current/dp/0804838534">The Chinese Language: Its History and Current Usage</a> by Daniel Kane is an excellent read. Kane is a former Australian diplomat and current professor of Chinese at Macquarie University. This book is a concise and readable description of how some of the idiosyncrasies which make Chinese difficult to learners came into existence. This knowledge will give you a better grasp of the nuances of modern Chinese and help you to develop more intelligent learning strategies.</p>
<p style="text-align: justify;">Graded Readers: The <a href="http://www.chinesebreeze.net/">Chinese Breeze</a> and <a href="http://mandarincompanion.com/">Mandarin Companion</a> series contain short stories using only the most basic characters, useful for beginners. The <a href="http://www.cypressbooks.com/proddetail.php?prod=9787802004153&amp;cat=">Graded Chinese Reader</a> series published by Sinolingua contains more advanced stories which provide an insight into contemporary Chinese society and life. These readers are available from China Books.</p>
<p style="text-align: justify;">If you visit China, it can also be helpful to look in the children’s book section of supermarkets such as Family Mart. Here you can often find books that have both pinyin and characters- an interesting and easy way to learn characters.</p>
<i>Written by Jesse Turland. Jesse is completing the Bachelor of Arts at the University of Melbourne with majors in Asian Studies and Arabic. He is also studying a Diploma of Languages in Chinese. He has studied abroad in Nanjing, China.</i>

&nbsp;