{
  "title": "In conversation with: Zhong Chen, a Chinese-Australian artist exhibiting at Red Sea Gallery in Brisbane",
  "author": "ACYA",
  "date": "2016-03-21T13:26:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Zhong Chen",
      "type": "image",
      "src": "Chen.jpg.505x585_q85.jpg"
    }
  ]
}
Zhong Chen is a Chinese-Australian artist who has built a name for himself here in Australia, exhibiting nearly non-stop for 20 years in Melbourne and Sydney. His first exhibition in Brisbane will be showing his Gongfu (Kungfu) series at <a href="http://redseagallery.com.au">Red Sea Gallery</a>, ending on April 3, 2016.

<strong>I want to hear about your story right from the beginning. I understand you came over from China at the age of 19 and moved to Melbourne. What was that experience like, coming over as a 19-year-old?</strong>

I arrived in Australia in 1989 before Tiananmen Square, just when I was turning 20. I came here for language. Actually I just wanted to study abroad! I graduated and got a big scholarship to go to England for postgrad studies. I came back from London and since then, in 2000, became a so-called ‘practice artist’, making a living off art.

<strong>What initially prompted you to come over to Australia?</strong>

I told my grandfather, “I want to study art.” He just booked me a place! My grandfather helped with the booking actually. I didn’t make the booking myself. I knew nothing then. I indicated to him I want to study further in art.

<strong>And he chose Australia?</strong>

Yeah! We have an uncle, he is American and lives in California and always goes to Las Vegas and gambles. My grandfather was worried about that so he sent me to study in Australia.

<strong>It was a very fateful decision on your grandfather’s part to choose for you to go to Australia! So when did you find out or discover that you wanted to be an artist?</strong>

Probably very young, around 13. In China, there wasn’t much going on growing on up in the late 70s/early 80s. I went to a sort of training school in my hometown and discovered calligraphy and Chinese painting. From there, I just kept chasing this question – ‘what is art?’

<strong>And what was Australia like when you arrived in 1989? What did Australian people think of Chinese people then?</strong>

I went to Adelaide to study after Melbourne. Adelaide was pretty quiet then compared to now. But Australians, generally, I think are pretty welcoming. My English at that time was not very good at all, so I couldn’t go many places. During my whole 20 years in Australia, I feel Australians are pretty friendly and I don’t often feel like an outsider. I feel welcomed anywhere in Australia!

<strong>Could you find any good Chinese food in the 80s and 90s in Australia?!</strong>

In Melbourne, definitely! But Adelaide didn’t have much nice Chinese food at that time. Melbourne has always had some good Chinese restaurants.

<strong>Why did you choose to go to Adelaide to study?</strong>

The reason is I just had a friend who helped me enrol there. I couldn’t tell the difference before landing between Melbourne and Adelaide! In the 80s, China didn’t have Internet or anything so I had no idea how Australian cities were different. It turns out there’s a big difference between the Adelaide Entertainment Centre and the Sydney Opera House! Before I came to Australia, all I knew of Australia was the image of the Opera House.

<strong>How did you develop yourself artistically and your particular style as an artist?</strong>

My style has been developed over a long period – from undergraduate school in Australia to London and then coming back to Australia again. It’s changed around 3, 4 or 5 times. But in general, my style has a Chinese meaning. When I came in the early 90s I was tracing the self-identity… the question about me being a migrant in Australia… It was mainly questions of identity at that time… Chinese-Australian, Australian-Chinese, that sort of thing. That has always been a theme in my work. My Pixilation Series and now the Gongfu Series, they all have some kind of connection with China.

<strong>You have exhibited in China, and you also mentioned Hong Kong and Singapore earlier. How do people react to your work in China?</strong>

I have shown in Beijing. It’s quite a large, dynamic city. For reaction, I don’t think it’s as strong as the one I receive in Australia. Maybe, we are a smaller society here, not that many gallery openings on at any one time. Also, I have consistently showed here for so many years now.

<strong>So you’ve been able to build up a name for yourself.</strong>

Yeah, I don’t think showing once or twice in China is making a name there… it’s a drop in the ocean! Beijing has 100 or more exhibitions opening at any one time. The art world is quite different in China to in Australia. I’m not in a rush to say ‘I want to conquer the world’ with my art or something like that!

<strong>You said the reaction to your work in Australia has been quite strong. What do you mean by that?</strong>

It’s been received well. The Portrait Series and Pixel Series did very well. In Chinese society there isn’t that much recognition, but in New South Wales and Victoria, even in South Australia and Western Australia, my work is quite well known now, especially for the Pixel Series and the later Gongfu Series. Since 2000, I’ve had no trouble showing or selling or anything! It’s been quite a smooth ride. I’ve always had good support from the general public, especially the Australian people. Not so much from Asians, but that’s slowly changing.

<strong>Do you think that’s because of how Asian or Chinese people perceive art and what is good art?</strong>

I think Australians and people in Western society generally, from a very young age, have an eye for buying art. China is changing. Now there are more and more Chinese people coming to look at my art even in Australia. It’s the latest fashion in China! Everything is changing very fast in Chinese society.

<strong>I think it makes it a very exciting place for an artist, particularly for modern art – it’s exciting and it’s always changing and you never know what people will like from one moment to the next!</strong>

It’s good to have more people having an eye for art. It’s a cultural change. For my work, I’ve always been doing what I want.

<strong>You’ve never changed it for China.</strong>

It’s never needed to change for the market. The market so far has been following my work, in Australia at least. So that’s what’s really good about practice art. Not many people can do that!

<strong>And I want to hear about your experience entering the <a href="http://www.artgallery.nsw.gov.au/prizes/archibald/">Archibald Prize</a> twice.</strong>

That would be 7 years ago? 2007? I was a Finalist three times and once as a Runner Up. Two was my <a href="http://www.artgallery.nsw.gov.au/prizes/archibald/2011/28908/">self-portrait</a>; one was a portrait of Nicholas Harding. It was a good experience for helping me change from my ‘pixel’ series or style to portrait style. At that time, from 2000 to 2007, I had been doing a series of portraits of Chinese women pixelated. It had been quite demanding. All the shows were on that series! A few years later I didn’t really want to be doing that series anymore. The Archibald made me confident to give up the Pixel Series and do something else. I had painted the same series for 7 years straight and I forced myself to stop. If I continue, people will just keep buying it!

<strong>Can you talk to me about your inspiration for your latest series, the one about Gongfu, that’s showing at Red Sea Gallery?</strong>

The background is… it’s a kind of free and expressionist background. I did a lot of painting by hand, tried to be as free as possible. Bruce Lee is an example of westernised Chinese culture. I thought Gongfu is pretty strong in how I grew up in the 70s and 80s, watching Hong Kong movies. Gongfu has a big connection with both Chinese and Western culture. The introduction of Asian culture, for pop culture, I thought was Bruce Lee. Some of the earliest knowledge of Western people about Asia was through kung fu. That’s the reason I chose the subject, to play around with that idea. I tried to use it like a childhood painting or ‘mucking around’. That’s the concept for the latest paintings.

<strong>How interesting! I’m very excited to go see the exhibition myself. What do you think about using art as a kind of cultural exchange, or almost a kind of diplomacy between Australia and China?</strong>

That’s what I’ve been doing in Melbourne. I have an art exhibition space in Melbourne – 402 Art Space. In the last couple of exhibitions I’ve chosen Chinese art to bring to Australia and also show Australian art. American pop art or Buddhist art – strong culture usually translates into strong art. China has a four or five thousand-year history of culture. What Chinese artists are grappling with now is how to represent those thousands of years of culture, while embracing Western ideas of art. The East-West mix is still happening. It’s a new exciting kind of art that’s being born this century!

There are about 50,000 artists trying to practice in Beijing alone. I only do my bit. Myself, I have a unique position, having learnt some basic Chinese art techniques in the 80s. It’s a good thing, I think, that I didn’t put my foot in too deep. I was born and raised there though. In my blood, I have a little bit of Chinese in me. But I’ve also been educated here. I accept a lot of Western culture. I’ve found I can mix that together in my art and try to come up with something that represents me. I’m free to paint what I want! I don’t know one day what will become of art, but I just practice in the moment. There weren’t many people like me in the 80s who could continue practicing art. Some Chinese artists went to France, some to America, a few to Australia. There was only a handful that were able to continue practicing.

<strong>So you feel very privileged to be able to continue to practice, I’m sure.</strong>

Indeed.

<strong>What makes you excited about art at the moment in China and Australia? Which young artists are you following?</strong>

There are so many fantastic artists from Australia and China right now! We have just held exhibitions at my gallery in Melbourne with some great young artists and right now I am interviewing artists for the next exhibition. Most of them just graduated a few years ago, or maybe even one year ago, from university and they are so talented! They are mainly doing installation and media work. It’s exciting, different, very playful. Everything can be art now – that’s exciting! Next time you’re coming to Melbourne you’ll have to come and see – I’ll let you know when the next show is coming up.

<em>This interview was conducted by Georgia Sands, 2015 ACYA Portfolio Manager and final year Law/Arts student.</em>

&nbsp;