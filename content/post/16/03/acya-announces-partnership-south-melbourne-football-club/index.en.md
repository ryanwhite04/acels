{
  "title": "ACYA Announces Partnership with South Melbourne Football Club",
  "author": "ACYA",
  "date": "2016-03-02T12:35:24+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA SMFC Partnership",
      "type": "image",
      "src": "ACYA-SMFC-Partnership.jpg"
    }
  ]
}
ACYA is pleased to announce it has entered into a partnership agreement with the South Melbourne Football Club (SFMC). SFMC is one of the oldest and most successful soccer clubs in Australia and has previously represented Australia in the FIFA Club World Championships. 

As part of this new partnership, ACYA members will be able to register for free 3 game memberships and ACYA will also be running a number of events for our members in conjunction with the SMFC. 

This is an exciting new partnership and details will be available soon on how ACYA members can redeem their free SMFC membership as well as on upcoming events. 

South Melbourne FC Operations Manager George Kouroumalis was very welcoming of the new partner, commenting "we would like to welcome ACYA and all of its members into the SMFC Family."

ACYA Managing Director David Douglas was also very excited about the partnership: “SMFC is a fantastic community organisation that ACYA is looking forward to working closely with. Football is a very popular sport in China, and working with SMFC gives our members, and especially our international student members from China the chance to get involved in the sport in Australia."
