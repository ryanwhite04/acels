{
  "title": "ACYA-MTC Scholarship 2017 Now Open",
  "author": "ACYA",
  "date": "2016-10-03T07:45:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "taiwan-photo",
      "type": "image",
      "src": "Taiwan-Photo.jpg"
    },
    {
      "title": "Taiwan Scholarship Photo",
      "type": "image",
      "src": "MTC-Taiwan-Scholarship-Photo.jpg"
    }
  ]
}
Interested in studying in the same language program that Kevin Rudd graduated from? ACYA has three scholarships offered in conjunction with the Mandarin Training Centre (MTC) in Taipei that are currently open.

<!--more-->

ACYA is proud to continue its relationship with Taiwan Normal University's MTC which is renowned through Greater China for the prestige and excellence of its teaching. Previous recipients of the scholarship have all praised both the studies they undertook and the experience they gained from living in Taiwan temporarily. Previous recipient Pei Yoong stated, "MTC is one of the best places to study Chinese, due to the small class sizes, highly experienced teachers, and the variety of classes offered as well as supplementary classes that allow you to not only learn the language but Taiwanese culture as well." To read about another previous scholar's experiences, please click <a href="http://www.acya.org.au/2016/09/national-taiwan-normal-universitys-mandarin-training-centre-australian-scholar-jermaine-werror/">here</a>.

ACYA's Taipei chapter runs regular intimate events including picnics, hikes and Chinese corners. Scholarship recipients are encouraged to become involved with ACYA and take advantage of Taipei's vibrant culture and opportunities. The deadline for applications is October 12. For more information about how to apply, please click <a href="http://www.acya.org.au/education/scholarships/acya-mtc-scholarship/">here</a>.