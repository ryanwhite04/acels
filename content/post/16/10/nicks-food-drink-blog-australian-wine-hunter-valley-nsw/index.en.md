{
  "title": "Nick's Food and Drink Blog - Australian Wine (Hunter Valley, NSW)",
  "author": "ACYA",
  "date": "2016-10-11T01:13:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "picture1",
      "type": "image",
      "src": "Picture1.png"
    }
  ]
}
When I think of France, I think of romance, cheese, and wine.  Bad romance, but decent cheese, and decent wine. Equally, when I think of China, I think of good food, but disgusting wine (酒).  Anyone who professes love for baijiu (白酒) is either lying or probably a baijiu salesman. But despite what I think of baijiu, China is still indisputably its foremost producer.  Similarly,  no one does scotch better than the Scots.  And when it comes to wine, a glass of Chablis or a bottle of Bordeaux is simply without equal.  Or is it?

<!--more-->

As France struggles to satiate the demand for its wines, especially from middle-class China, people have sought to find alternatives.  And they have, for good reason, turned to Australia. Australia, in my opinion, makes a jolly good drop.  And I am not alone in thinking that.  Wine exports are said to have grown to $2.1 billion per year, with 50 per cent of our highest-priced being consumed by China!

So where do we begin?  Well, let's turn to somewhere close to home, at least for me; the Hunter Valley.

The Hunter Valley is arguably the best producer of Semillons or Shirazes!  At least, that's what I think.  Of course, in regard to the latter, my South Australian friends might very well disagree.  So long as my Semillon is crisp, preferably served with a dozen Sydney rock oysters, I'm happy.

But then again, there's no need to be a connoisseur or oenophile to enjoy a good Hunter wine: yes, there really is a word to describe wine lovers.  A good bottle of wine, is a good bottle of wine; as axiomatic and cliché as that sounds.

So, for the uninitiated, in what appear to be a quagmire of sorts, let me give you my take on the Hunter Valley.

<strong>Where is the Hunter Valley?</strong>

The Hunter Valley is but a stone's throw away from Sydney; roughly two hours north.  Take a car, and enjoy the drive!  Along the way, I'd definitely recommend stopping off on the Central Coast.  Saunter along the beaches of Avoca, have a pint of beer and some fish and chips, and be on your way.  As you continue driving along the Pacific Highway, and as rocks and cliffs transform into woodland and endless pastures, you know you've reached 'wine country'.

<strong>What is there to do?</strong>

If wine's not your thing - not a problem.  From cheese, to chocolate, golf to horse riding, the Hunter has you covered.

Looking to get a few likes on that token Instagram or WeChat photo?  Look no further than the Wollombi Valley and its heritage-listed village.  It still retains many of its old convict structures!

Alternatively, if you're looking to ply yourself with chocolate and cheese, have a drive around Pokolbin.  There are plenty of cheese shops that would be more than obliged to offer you a generous sample.  Having said that, guilt and lack of self-restraint always get the better of me, and I end up bringing back a tonne of cheese back home!

And if you're staying overnight, why not enjoy a nice dinner somewhere?  But where?  Well, it seems odd to say, but surprisingly, the Hunter is home to many an award winning restaurant.  As someone who loves all things French, Bistro Molines offers some of the heartiest food in town.  However, Chinese food hasn't quite found its way up into wine country yet, so I'd steer well away from any Chinese (餐馆)!

<strong>Where do we go for a drink?</strong>

Where do I even begin?  Why don't I let you discover that for yourself?  Take a car, join a bus tour, explore the region!  However, everyone has their personal favourites, and I'll leave you with some of my own.

<u>Tempus Two</u>

Everyone has to visit Tempus Two just to see it's incredibly designed cellar door!

<u>Tyrell's</u>

The Tyrell family have been making wines in the Hunter for almost 200 years!  They pride themselves on making some of the best Semillons and Shirazes in the world.

<u>Brokenwood</u>

A relatively new winery, Brokenwood's tasting rooms are certainly worth a visit.  Give their 'Graveyard' Shiraz a try, despite the name, I'm sure you won't be disappointed.  Look at how happy I look!

So there you have it, a brief summary of the Hunter Valley.  I hope you get to travel up there some time, and if ever you do, let me know, and we can exchange tasting notes!

<em>Nick Yuen is a third year Arts/Law student, and the views expressed here are solely his own.</em>
<p class="p1"></p>