{
  "title": "ACYA-AustCham Beijing Internship Announced",
  "author": "ACYA",
  "date": "2016-01-26T23:27:44+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "4585714037_35a5f77b11_o",
      "type": "image",
      "src": "4585714037_35a5f77b11_o-e1454456494598.jpg"
    },
    {
      "title": "HNA Logo",
      "type": "image",
      "src": "HNA-Logo-e1474029358441.jpg"
    }
  ]
}
The Australia China Youth Association (ACYA) is excited to start 2016 by announcing the new and improved ACYA-AustCham Beijing Internship, a result of our close cooperation with the China-Australia Chamber of Commerce (AustCham) in Beijing.  

AustCham Beijing has been working with the Australian and Chinese governments since 1996 to promote broader business relationships between the two nations through advocacy programs and business events. 

Through the Memorandum of Understanding signed between ACYA and AustCham Beijing, the 12-week, full-time internship will involve interns working in a team to assist Australian companies that are doing business in China. 

It will provide several opportunities to interact with accomplished Australian business leaders in China. 

Interns will be able to develop their skills in industry relations, communication, project management, market research, event management, government relations and other key business areas. 

AustCham Beijing will cover all work expenses and will provide a modest stipend to the successful interns, but interns will not be given a salary. 

Applicants should have a bachelor’s degree or be in their final year of study with an interest in the Australia-China relationship.  

Don’t miss out on this rare opportunity to work and live in China’s capital city! More information about the internship can be found here: http://www.acya.org.au/careers/acya-austcham-beijing-internship/ 

<b>Quotes attributable to AustCham Beijing</b> 

"AustCham Beijing has a well-regarded internship program which now gives priority to those in the ACYA network to apply for an internship. AustCham Beijing provides regular internship opportunities, on a rolling basis, for interns to work in the Beijing office over a 3-month period. With the aim of creating greater opportunities for the young and emerging business leaders in the ACYA network, AustCham Beijing look forward to welcoming more smart, innovative and ambitious talent to the AustCham Beijing team. AustCham Beijing and ACYA’s MoU will develop greater talent through internship opportunities with Australia’s peak industry body in China." 
