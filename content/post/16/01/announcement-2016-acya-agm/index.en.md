{
  "title": "Announcement: 2016 ACYA AGM",
  "author": "ACYA",
  "date": "2016-01-31T14:09:50+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Join ACYA in 2016 (1)",
      "type": "image",
      "src": "Join-ACYA-in-2016-1-e1454496419450.png"
    },
    {
      "title": "11845152_506559556165693_7685287538555620469_o",
      "type": "image",
      "src": "11845152_506559556165693_7685287538555620469_o.jpg"
    }
  ]
}
It is with great excitement that the Australia China Youth Association (ACYA) would like to announce the date of, and accompanying details for the organisation's Annual General Meeting (AGM) for 2016.

The principal aims of the AGM are threefold:

1) To provide full operating, management, and financial reports of the 2015 National Executive Committee’s undertakings to members.

2) To elect the members of the 2016 National Executive Committee. Positions open for election include: <strong>National President, Managing Director, National Secretary, National Treasurer, Australia Manager, China Manager, Portfolio Manager, Alumni Manager.</strong>

3) To take in applications for members of the 2016 National Directors. Positions open for application in 2016 include: <strong>Publications Director, Media & IT Director, Careers Director, Education Director and People-to-People Director.</strong>

As the only apolitical NFP run for members, by members working to actively foster a transnational community of youth Australians and Chinese interested in further understanding each other’s countries, attendance at the AGM is the perfect opportunity for members to participate in, and be informed of the full scope of 2015’s activities, as well as the strategic direction to be charted in 2016.

ACYA always values, and encourages the attendance and participation of its members on this annual occasion. Attendance at the Annual General Meeting is open to all of the National Executive, Chapter Executives, and ACYA members in both countries.  

The AGM will be held on Sunday, the <strong>28th of February, 2016 at 10am China Standard Time / 1pm Australia Eastern Daylight Time.</strong>

Details for Executive Position Descriptions, and Joining Instructions for connecting to the AGM can be found on the <strong>Join ACYA page</strong> of the ACYA website. A full agenda will be distributed prior to the commencement of the meeting.

