{
  "title": "In conversation with Jessica Rudd, founder of e-commerce platform Jessica's Suitcase",
  "author": "ACYA",
  "date": "2016-01-14T00:58:17+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "fd6f932b-24cc-47ca-8992-65d6938749a2",
      "type": "image",
      "src": "fd6f932b-24cc-47ca-8992-65d6938749a2.jpg"
    },
    {
      "title": "Jessica Rudd",
      "type": "image",
      "src": "jessia-1.jpg"
    }
  ]
}
<p style="text-align: left;"><a href="http://www.acya.org.au/wp-content/uploads/2016/01/jessia-1.jpg" rel="attachment wp-att-23905"><img class="aligncenter wp-image-23905 size-medium" src="http://www.acya.org.au/wp-content/uploads/2016/01/jessia-1-200x300.jpg" alt="Jessica Rudd, founder of Jessica's Suitcase" width="200" height="300" /></a></p>
<p style="text-align: left;">Jessica Rudd has recently ventured into Chinese e-commerce industry through her online store, Jessica’s Suitcase (澳宝包 àobǎobāo in Chinese, accessible via <a href="https://jessicassuitcase.tmall.hk">Tmall</a> or <a href="http://jessicassuitcase.jd.hk">JD.com</a>). Recently, ACYA's <a href="http://www.acya.org.au/publications/china-bites/">ChinaBites</a> team were lucky enough to speak with Jessica Rudd about her career, the workings of her business, and her experiences in the Chinese online market.</p>
<p style="text-align: left;">Prior to launching Jessica’s Suitcase, Jessica's career has been wide and varied, including roles as a media and intellectual property lawyer, a 2007 federal campaigner with her father (former Prime Minister Kevin Rudd), a PR and crisis management consultant, and author of chick-lit novels <a href="https://www.textpublishing.com.au/books/campaign-ruby">Campaign Ruby</a> and <a href="https://www.textpublishing.com.au/books/ruby-blues">Ruby Blues.</a> She studied a double degree in law and politics at Griffith University.</p>

<h4 style="text-align: left;"><b>How are you distinguishing yourself and Jessica’s Suitcase in the Chinese market?</b></h4>
<p style="text-align: left;">I’m encouraging mums to trust their instincts. Being a new mum can be such a noisy time, where people are constantly giving you advice, telling you what to do, and that they know best for your child. When I lived in Beijing, there were a number of occasions when strangers would come up to me on the street, and say “Oh, what a beautiful baby!” Then they’d look at me and go, “Why isn’t she wearing any socks?!” It was this whole accusatory tone of telling mums what to do. I wanted to be the positive voice saying, “Actually, your instincts are pretty good, why not just follow them?” and block everyone else out. Ultimately, mums have their kids’ best interests at heart and all of those instincts are propelled by love.</p>

<h4 style="text-align: left;"><b>How do you feel about becoming a popular Chinese mummy blogger?</b></h4>
<p style="text-align: left;">I’m blogging for 辣妈宝典 on JD.com (làmā bǎodiǎn). They operate on a range of different platforms, and 辣妈宝典 is their online community for parents. I love blogging about mum issues; it’s a lot of fun. I’ve learned a lot from the feedback I receive from other mums as well. If there’s one thing that Chinese mums do well, it’s dishing out advice as much as taking it. Blogging is just as much about me learning from them as them learning from me.</p>

<h4 style="text-align: left;"><b>Have you ever struggled with the cultural barrier?</b></h4>
<p style="text-align: left;">I’ve grown up with Chinese culture in my life and spent five years in China, so I haven’t found much of a cultural barrier. However, there were some things I didn’t understand. For example, I didn’t know Chinese mums were drinking baby formula! I had no idea. It’s something that took me completely by surprise. Nor did I know that food for babies starts with egg yolk, and then moves on to other solids. There were many situations that I had come to expect were the normal way of doing things, but there are actually a million kinds of normal. I think many Westerners don’t realise there’s no such thing as the Chinese way of doing things. It’s culturally and linguistically diverse, and this has resulted in many different ways of doing things within China.</p>

<h4 style="text-align: left;"><b>What’s your long-term vision for Jessica’s Suitcase?</b></h4>
<p style="text-align: left;">We want to earn the trust of the Chinese customer. We see ourselves as being an authentic voice for Australia in the Chinese market, bringing high quality Australian products that I either would use or currently use – I won’t sell anything that I don’t use. I think that’s what sets us apart. The long-term vision is to keep building that trust and to bring these wonderful, high quality, clean, green Australian products to as many parts to China as we possibly can. We started the business on major platforms Tmall Global and JD.com, and we’re really happy to be there.</p>

<h4 style="text-align: left;"><b>As Alibaba’s Tmall and JD.com are competitors, have you noticed any major differences between these two platforms?</b></h4>
<p style="text-align: left;">They’ve both got their strengths. Alibaba is brilliant at what they do and the current market leader. They’re competitive and a great place to start a store. I actually had the fortune of meeting Jack Ma and he was really inspiring for me. He’s a boy from Hangzhou who’s created this business that has lifted so many people out of poverty in China and enabled everybody to participate in a China-wide, if not global, market. It’s hugely exciting to be part of this vision.</p>
<p style="text-align: left;">I think the key difference with JD.com is that Tmall is fully integrated with WeChat, which is a large part of business, social and family life in China. For us, it’s important to use WeChat as a tool and listen to feedback, and also talk to other mums through 辣妈宝典 and other social media platforms. It’s about reaching as many people as possible and we’re happy to achieve this through both Tmall and JD.com.</p>

<h4 style="text-align: left;"><b>The opportunity to meet Jack Ma for business sounds incredibly exciting. Is this a typical working day in the life of Jessica Rudd?</b></h4>
<p style="text-align: left;">Not at all! On that day, I felt like a massive fan girl. I was following what Jack’s been doing for some time and when we met in person, I was able to ask him questions about the strength of the business. It was pretty extraordinary to receive advice from him.</p>
<p style="text-align: left;">The average day for me involves running my business from home. My little girl is around a lot, and when she’s not at kindergarten, she helps Mummy do her work, sitting on my lap while I’m on my laptop. One of the wonderful things about online retail, particularly in selling to mums and bubs, is that I know exactly where they’re at. A lot of mums are at home with their child and they kill time by shopping online or going on social media. It’s what a lot of us are doing and I relate to that because that’s my working day as well – I’m just on the selling side.</p>

<h4 style="text-align: left;"><b>What’s been one of the most important things you’ve learned from starting Jessica’s Suitcase?</b></h4>
<p style="text-align: left;">Again, it comes down to building trust. When you’re asking somebody to buy a product from you, they can’t touch, smell or shake the product - all the things you do when you go into a bricks-and-mortar shop. They need to trust the person who’s selling to them, and I place a huge emphasis on earning that trust. Within our team, we do everything we can to meet the needs of our customers. We want to deliver intact, high quality products on time, and we don’t vouch for anything we don’t believe in.</p>

<h4 style="text-align: left;"><b>Your career has been really diverse to date. You’ve worked as a lawyer, author, PR consultant. Have these experiences helped launch your own business?</b></h4>
<p style="text-align: left;">I think they have. Throughout my life, the common thread is storytelling. As a lawyer, you’re expected to tell a story through giving advice. In politics, you’re telling the story of what’s going on, either your story or the Opposition’s story. In PR, I was telling approximately five stories in different ways to media, and then I went into real storytelling as a novelist. Here, what I’m doing is telling the story of these wonderful products. It’s essentially Australia’s stories. We have something pretty wonderful to offer and I’m really relishing telling that story.</p>

<h4 style="text-align: left;"><b>Any favourite products from Jessica’s Suitcase that you’d like to recommend?</b></h4>
<p style="text-align: left;">I love all the products, it’s like asking to pick my favourite child! But there’s a really great brand called Organic Bub, they make certified organic baby food and do a formula range, specifically goat’s milk formula. I like the FLATOUTbears toys, made from Australian sheepskin. They look slightly like UGG boots but they’re completely flat. My daughter loves those and I’m hoping our next baby will love them too. I also really like this certified organic coconut oil that I’ve been selling, by Absolute Organic. I use this coconut oil for everything – for cooking, on my skin, on mozzy bites for my daughter. So there you go, that’s my top three at the moment.</p>


<hr />
<p style="text-align: left;">This interview was conducted by Olivia Gao for the December 2015 issue of ChinaBites. Read the original version <a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=9e01da626a&amp;e=0d2083691e">here</a>.</p>