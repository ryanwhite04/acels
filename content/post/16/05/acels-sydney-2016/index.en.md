{
  "title": "Applications Open for ACELS Sydney 2016",
  "author": "ACYA",
  "date": "2016-05-30T13:45:58+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/05/image-1.png" rel="attachment wp-att-23990"><img class="aligncenter wp-image-23990" src="http://www.acya.org.au/wp-content/uploads/2016/05/image-1.png" alt="ACELS Sydney 2016" width="500" height="333" /></a>

ACYA is pleased to announce that delegate applications for the 2016 Australia-China Emerging Leaders Summit (ACELS) are open. The 2016 ACELS Conference will be held in Sydney from the 29-31 July 2016.

ACELS is ACYA’s premier flagship initiative event aimed to bring together emerging young leaders in the Australia-China bilateral relationship since the first time it was held in 2013. The 2016 Conference will deliver an intensive program filled with workshops, speaker networking events and social function events designed to foster professional development, provide networking opportunities, up-skill and further strengthen networks between the next generation of leaders.

Over 250 delegates have participated in previous ACELS Conferences and ACYA looks forward to welcoming the next group of emerging young leaders to Sydney in late July.

Apply here: <a href="http://goo.gl/forms/SNqx0QWdp7NJw90E2" target="_blank">http://goo.gl/forms/SNqx0QWdp7NJw90E2</a>

Applications are open until 11:59pm AEST 15 June 2016