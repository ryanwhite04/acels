{
  "title": "Directors Welcome",
  "author": "ACYA",
  "date": "2016-05-21T14:00:56+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "image",
      "type": "image",
      "src": "image.png"
    }
  ]
}
ACYA’s National Executive team has recently appointed the Portfolio Directors for 2016. The eight new directors come from a diversity of backgrounds and experiences with a common interest in deepening the relationship between Australia and China.

<strong>Careers Director: Annie Zhou</strong>
The Careers Director for this year, Annie Zhou, is in her third year Law and Political Economics student at the Australia National University. Her passion for Australia-China relations started when she was growing up in Beijing and she continued to study Mandarin at the Beijing Language and Culture University. She looks forward to expanding ACYA's Careers Portal in order to cater to a diverse range of interests.

<strong>People-to-People Director: Josephine Macmillan</strong>
Josephine Macmillan is the incoming People-to-People Director. She is currently completing degrees in Arts and Law at the University of Queensland. She interned at the Queensland Art Gallery where her interest in cultural diplomacy and its role in the Australia-China bilateral relationship developed. In her role as People-to-People Director, she looks forward to having the opportunity to visit and engage personally with several ACYA branches.

<strong>Education Director: Chloe Dempsey</strong>
ACYA’s new Education Director, Chloe Dempsey, is currently studying Law at Tsinghua University in Beijing under the Westpac Asia Exchange Scholarship. After spending five months in Sichuan, Chloe has been a passionate advocate for the importance of youth in the Australia-China relationship. Chloe believes ACYA’s strengths are in the diversity, connectedness and mobility of its members.

<strong>Publications Director: Olivia Gao</strong>
Olivia is the new Publications Director. She is currently a Marketing Executive for Campbell Arnott's in Sydney and has been involved with Australia-China Youth Association for over two years, becoming the Editor of ChinaBites in 2015. Highlights of her role as Editor included interviewing high-profile members of the Australia-China community, including Jessica Rudd and Andrew Hogg. Olivia's interest in China grew significantly when she moved to Beijing for her gap year, in order to study Mandarin Chinese at Tsinghua and Peking Universities. She speaks, reads and writes fluent Mandarin Chinese and has intermediate skills in French.

<strong>Media &amp; IT Director: Xin Na Zeng</strong>
ACYA’s latest Media &amp; IT Director is Xin Na Zeng. She is currently completing a Bachelor of Commerce (International) degree at the University of New South Wales. Xin Na joined ACYA in 2013, and whilst undertaking a one-year exchange to Shanghai Jiao Tong University in 2015, she served as the Marketing Director of ACYA’s Shanghai Chapter.

<strong>Business Development Director: Ryan Cunningham</strong>
Ryan Cunningham, a Commerce and Languages student at the University of Adelaide, is this year’s Business Development Director. Ryan first got involved with ACYA whilst on exchange at Tsinghua University where he studied for one year on a Confucius Institute Scholarship in 2014. Ryan's business interests include the study of cross-cultural mergers and the creation of shared value within Asia. With a passion for the arts, as well as language and culture, he hopes to bring a new direction to Business Development, in hoping to establish meaningful relationships for ACYA in both Australia and China.

<strong>Translations Director: Wei Wei Shih</strong>
Wei Wei Shih is the Translations Director for 2016. An Arts and Law student, she is majoring in Chinese Translation &amp; Interpreting, and Peace &amp; Conflict Studies. Having interned and worked in law firms with international client focus both in China and Australia, Wei Wei enjoys being able to contribute culturally and professionally to the strengthening ties between Australia and Greater China. Wei Wei is passionate about activism and volunteering. She works with refugees both in her capacity as a law student and as a community member.

<strong>Alumni Engagement Director: Jani Song</strong>
This year’s Alumni Engagement Director is Jani Song, the co-founder and Creative Director of the Perth-based creative consulting firm Yu Consultants. She served as ACYA’s Person-to-Person Vice President at the University of Western Australia in 2014 and the UWA Chapter President in 2015. Under the new position of Alumni Director, she hopes to use her experience to build engaging initiatives that will facilitate mentorship, partnerships and the cross-pollination of ideas and culture in the Australia-China space.