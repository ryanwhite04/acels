{
  "title": "Invitation to Youth Development Seminar in Taiwan: Age of Globalisation",
  "author": "ACYA",
  "date": "2016-05-21T14:22:58+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Taiwan Photo",
      "type": "image",
      "src": "Taiwan-Photo-1.jpg"
    }
  ]
}
The Youth Development Administration of the Ministry of Education in Taiwan invites Australian citizens, aged 18-25, to participate in a ‘Seminar on Youth Participation and International Humanitarianism in the Age of Globalisation’. This event will take place in Taipei from the 11th to the 18th October, 2016. All costs (including insurance, meals, accommodation etc) are covered except for delegates airfares which they have to pay themselves.

The Summit will involve travelling to Kaosiung and Tainan for tours and then to Taipei for tours and the Summit. The Summit will focus on the topics of how youth can generate positive action on Climate Change, reduce wealth inequality and finally, harness the power of social media in youth affairs and to have a positive impact. The Australian delegation will be presenting and debating these topics will delegations from many other countries with the aim of producing declarations of action by the end of the Summit.

Eligibility criteria are:
- Be aged between 18-25
- Be an Australian citizen
- Be residing in Australia
- Have an interest in Youth Affairs
- Be available from Oct 11-18 inclusive

Applicants should send a brief statement confirming they meet the eligibility criteria and outlining their past experience in youth affairs, volunteering, any relevant work experience, their academic qualifications and current occupation/degree and also any other relevant information to president@acya.org.au by the 24th of May.