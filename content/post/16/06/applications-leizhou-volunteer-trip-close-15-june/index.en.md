{
  "title": "Interested in volunteering and the Aus-China relationship? Applications for the Leizhou Volunteer Trip close on 15 June",
  "author": "ACYA",
  "date": "2016-06-11T04:15:29+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "3a592a51de4fe95a78b62f9342cd09f",
      "type": "image",
      "src": "3a592a51de4fe95a78b62f9342cd09f.jpg"
    },
    {
      "title": "4970d45c5bf60a9d2dc3b43a914e6d4",
      "type": "image",
      "src": "4970d45c5bf60a9d2dc3b43a914e6d4.jpg"
    },
    {
      "title": "20150726_074943_Richtone(HDR)",
      "type": "image",
      "src": "20150726_074943_RichtoneHDR.jpg"
    }
  ]
}
Last year, Tom Linette and Nick Harding volunteered with the Leizhou program. Since 2013, ACYA has sent volunteers to take a week in July to teach English to Leizhou high school students, allowing a glimpse into regional China, while contributing to the local community and promoting Australian culture. It's a great opportunity to interact with Chinese learners of English and experience life as a local.

If you have a passion for volunteering and the Aus-China relationship, we'd love for you to <a href="https://docs.google.com/forms/d/13CfdRs2JYS7I2dreY6wnJvLapqedu3DMeFm8XMpueLs/viewform?c=0&amp;w=1">apply here</a> for this year's intake. Applications close 15 June 2016

<a href="http://www.acya.org.au/wp-content/uploads/2016/06/3a592a51de4fe95a78b62f9342cd09f.jpg" rel="attachment wp-att-24034"><img class="size-medium wp-image-24034 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/3a592a51de4fe95a78b62f9342cd09f-300x225.jpg" alt="3a592a51de4fe95a78b62f9342cd09f" width="300" height="225" /></a>

&nbsp;

<strong>Why did you go to Leizhou?</strong>
<em>Nick:</em> How could you say no to spending a couple of weeks in China, in a city you’ve never heard of, living with a local family? Great opportunity to see a place in China you would never visit, and spend time seeing how locals go about their lives.

<em>Tom</em>: I went on the Leizhou trip because the opportunity to get involved in teaching students in rural parts of China was something that really grabbed my attention. The opportunity to spend time in a small Chinese city is not one that often comes around and so the chance seemed too good to pass up.

<strong><a href="http://www.acya.org.au/wp-content/uploads/2016/06/4970d45c5bf60a9d2dc3b43a914e6d4.jpg" rel="attachment wp-att-24035"><img class="size-medium wp-image-24035 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/4970d45c5bf60a9d2dc3b43a914e6d4-300x225.jpg" alt="4970d45c5bf60a9d2dc3b43a914e6d4" width="300" height="225" /></a></strong>

<strong>How did your trip improve your language or cultural skills?</strong>
<em>Nick:</em> Cultural skills – meeting Chinese uni students outside of the big coastal cities was pretty interesting and gave a good insight into Chinese mindsets/the education system.

<em>Tom:</em> The Leizhou trip by far the best cultural experience I have ever had. It gives an insight into parts of China that are otherwise very difficult to experience. Being far removed from the glitz and glamour of first tier cities you are able to see how a working class community functions in China. You are able to experience the real struggle of living in a country with 2.6 billion people and gain a better understanding of how hard people must work to make their way in the world.

That being said, the people I met in Leizhou were some of the most generous and happy people that I have had the privilege of meeting. The host family that I stayed with were all extremely nice and generously provided each meal for us.

Culturally I learned so much in Leizhou and my language also improved through my stay with the host family that only spoke some Mandarin and Leizhou dialect.

<strong>How would you describe your teaching responsibilities?</strong>
<em>Nick: </em>Pretty much free reign over what you want to teach. We took an approach of trying to have as much fun with the kids as we could, while introducing them to aspects of Australia. There was usually a Chinese uni student paired with us to translate instructions if we needed it, but we tried to stick to things basic enough not to require it too much.

<em>Tom</em>: Teaching in Leizhou was certainly a character building experience, we were truly thrown into the deep end in terms of teaching. Each volunteer took a class of around 60 students and was responsible for the preparation of all lessons. But rest assured that teaching these children is such an incredible experience. I had previous teaching experience to this trip but had never experienced children with so much motivation to learn. The whole class would hang off every word that was said and constantly try their best to get as much of each lesson as they could. I really can’t express how grateful I am that I had the privilege to teach kids like these, it really did put into perspective how much opportunity there is in the world to make a positive difference and I hope that is a lesson that I will never forget.

<strong>How do you feel you contributed to Leizhou(as a school or community) during your time there?</strong>
<em>Nick</em>: Getting to know the Chinese university students and exposing them to English from a native speaker was hopefully pretty helpful to a lot of them. For a lot of the kids, it was pretty exciting having foreigners coming to teach at the camp - when I was taken down to where they were signing kids up, there was a marked uptake in enrolment.

<a href="http://www.acya.org.au/wp-content/uploads/2016/06/20150726_074943_RichtoneHDR.jpg" rel="attachment wp-att-24036"><img class="size-medium wp-image-24036 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/20150726_074943_RichtoneHDR-300x169.jpg" alt="20150726_074943_Richtone(HDR)" width="300" height="169" /></a>

<strong>What did your social life involve while in Leizhou?</strong>
<em>Nick: </em>Hanging out with the other uni students. After classes finished the whole place became a big leisure centre, so we’d usually play badminton, Chinese hacky-sack, basketball etc with students/teachers/people from the community. Leizhou was surprisingly lively at night, so there was lots to do/walk around at night. Obviously karaoke on the last night was lots of fun.

<em>Tom</em>: Social life in Leizhou was a whole lot of fun, we experienced all kinds of things from going to dinner with the local government officials to riding around town on hipster bicycles. We were even taken into the country side for a picnic on some farm land where there were water buffalo roaming around freely. But perhaps my favourite social experience of the trip was playing sports after school with parents and students. This seemed to be a social norm in Leizhou and was one of the nicest cultural practices that I have seen. We played a mixture of things from Jianzi to badminton and table tennis. Overall the social experiences and the friends that I made in Leizhou was the cherry on the cake of a truly amazing experience.

<strong>What was a highlight of your volunteering experience?</strong>
<em>Nick:</em> Sitting on top of a Ming Dynasty tower early on a Sunday morning, watching a group of women doing tai chi and chatting to a uni student from Leizhou about the pressure of the Chinese education system and from her parents was pretty special. That and riding a fixie around and every second motorbike going past slowing down to have a chat.

<hr />

This interview was conducted by Chloe Dempsey.

&nbsp;