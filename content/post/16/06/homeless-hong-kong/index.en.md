{
  "title": "ACYA volunteers to help the homeless in Hong Kong",
  "author": "ACYA",
  "date": "2016-06-25T14:50:08+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "13390998_717543378348220_1602025238_n",
      "type": "image",
      "src": "13390998_717543378348220_1602025238_n.jpg"
    },
    {
      "title": "13407370_717543405014884_1747491039_n",
      "type": "image",
      "src": "13407370_717543405014884_1747491039_n.jpg"
    }
  ]
}
The ACYA Hong Kong team recently organised 17 students and young professionals to hand out over 100 bags of food supplies to the homeless community of Hong Kong.

Blog editor Siobhan O’Sullivan had the pleasure of interviewing Isla and Michael to discuss the event. Isla Munro is a Fashion student and Hong Kong New Colombo Plan Scholar studying at Hong Kong Polytechnic University. Michael Shanahan is Marketing and Business student on exchange from the University of South Australia to the Chinese University of Hong Kong.

<a href="http://www.acya.org.au/wp-content/uploads/2016/06/13390998_717543378348220_1602025238_n.jpg" rel="attachment wp-att-24083"><img class="wp-image-24083 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/13390998_717543378348220_1602025238_n-300x203.jpg" alt="13390998_717543378348220_1602025238_n" width="341" height="231" /></a>

<strong>How visible is the issue of homelessness in Hong Kong, and was there any specific experience that inspired you to organize this event?</strong>

Isla: The ACYA HK Education team decided to focus their efforts on volunteer work for a few reasons. The main reason being that the divide between rich and poor is very obvious in Hong Kong. On one side of the city you have massive hundred-story buildings and flashy rooftop bars. Yet within just a few minutes’ drive you find hundreds of homeless people sleeping underneath office and apartment buildings at any one time. Because of this divide, I think it has become very easy for expats and perhaps to a certain extent even locals to be blinded by the flashy lights of Hong Kong and forget what is really happening as a result of this massive gap in wealth. The issue of homelessness varies depending on where you visit in HK. To really see the extent of the issue, one needs to take a walk around less developed areas of Hong Kong such as Shim Sha Po or Jordan at nighttime.

<strong>Did you have local contacts or collaborate with any other organisations to run this event?</strong>

Isla: ACYA HK reached out to another non-for-profit community organization called ImpactHK. ImpactHK run weekly volunteer missions around the streets of Hong Kong to hand out essential items such as food, clothing, toiletries and books. It’s something that anyone living or visiting in Hong Kong can get involved in, just head over to The Guest Room’s Facebook page. It was great to run our volunteer night in collaboration with other people with experience and knowledge of where the areas were that needed our help the most.

<strong>How did the day run and how did the ACYA members feel at the event’s conclusion?</strong>

Michael: At first the group met at a designated supermarket where we bought a collection of different essential goods to be divided into bags. Once we had sorted out the bags we had a bit of a run-down given to us about where we would be going and what we would be expecting from the night from Jeff, the legend who runs The Guest Room. We started our walk through some parklands and then ventured to the underpass of Nam Cheong. Along the way we witnessed some pretty confronting sites as we handed out the bags.

For most of us the evening was a real wake-up call about how often we complained about our trivial lifestyle dilemmas. Being exposed to such gratitude from those whom we helped hit a cord in us all on the night and it certainly provided us a special insight into the other side of this city that prides itself on the lavish lifestyles lead by those in the fast-lane. Perhaps overall what was most impressionable from the night on our eager group of adolescents was the feeling of helping someone in need, it truly is a feeling like no other.

<strong>As a foreigner in Hong Kong, what are the challenges associated with getting involved with charities or volunteer work?</strong>

Michael: There is an ever-growing need for help in Hong Kong at present and as a foreigner you can easily make a difference – even in the smallest of ways – if you make the effort to seek the opportunities that are out there. It’s such a diverse city, with many expats hailing from countries across the globe which has truly made the country something of a cultural hub. Keeping that in mind, as a foreigner in Hong Kong this means there is always someone around the corner in a similar situation to you and there are many networks of people who are both local and international to Hong Kong running activities for those in need.

<strong>Is there anything you would recommend to young students or professionals arriving in Hong Kong for the first time and wanting to get involved in the community?</strong>

Michael: Just go for it! A trusty Google search goes a long way when you’re looking for opportunities. Most charitable or volunteer organisations run blogs and social media pages so they are readily available. Contacting your host university or employer here in Hong Kong is also a really great idea if you wanted some insights as they often have affiliations too and can point you in the right direction.

This interview was conducted by Siobhan O'Sullivan, Editor of the Australia-China Perspectives blog.

&nbsp;