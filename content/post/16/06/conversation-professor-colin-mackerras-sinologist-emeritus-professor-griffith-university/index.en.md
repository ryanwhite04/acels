{
  "title": "In conversation with Professor Colin Mackerras, Sinologist and Emeritus Professor of Griffith University",
  "author": "ACYA",
  "date": "2016-06-07T09:30:39+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "In Focus",
      "type": "image",
      "src": "In-Focus.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/06/In-Focus.jpg" rel="attachment wp-att-24025"><img class="size-medium wp-image-24025 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/In-Focus-300x200.jpg" alt="In Focus" width="300" height="200" /></a>

This month, ChinaBites spoke with Professor Colin Mackerras, an Australian Sinologist and Emeritus Professor of Griffith University. Professor Mackerras took an M.A. degree at the University of Cambridge before embarking on an academic career in China, beginning at the Beijing Foreign Studies University in 1964. He was also Chair Professor and Research Scholar at the Australian National University where he obtained his PhD. Professor Mackerras’ has had an accomplished research and teaching career with a specialisation in Chinese ethnic minorities, China-Australia relations and Chinese arts.

<strong>When did you first become interested in China and for what reason?</strong>
I first became interested in China in the late 1950’s when my mother suggested me I should apply for an Asian studies scholarship, which was being advertised by the Menzies government, which was trying to establish more links with Asia after WW2. The Menzies government set up a small section on Asian studies, at a place called the Canberra University College which was then a branch of the Melbourne University. I applied for one of these scholarships and was successful. In those days there were not many people studying Asian studies. I was one of the first Australians in the field. After that, while I was doing my graduate degree, I became interested in Chinese history and in particular, the Tang Dynasty.

<strong>What was life in China like in the 1960s as an Australian academic?</strong>
It was very ideological in comparison to now. The majority of the population was very poor compared to Australia’s. We couldn’t get a very close relationship with our colleagues or our students, although I did establish relationships with the teachers over the years, which last to this day. There was a cultural barrier and furthermore, the academics needed permission to invite us to their houses. We were never allowed to leave Beijing without permission. The first time I was allowed to travel unaccompanied, was during the Cultural Revolution when I was told that I had to stop teaching, so I went to Inner Mongolia. I didn’t have a colleague with me because they were engaged in revolution and frankly, I think they wanted me out of the way. During the Cultural Revolution I also went to a Buddhist centre in the mountains of Jiangxi province, which had been converted into a centre for Chairman Mao’s works.

<strong>How has the world of academia changed in China?</strong>
In those days it was much more ideological. Also nowadays there is more of a focus on research than teaching. Academics with PhDs were a small minority in those days. Most Chinese thought the aim of university was to defend Chairman Mao in a social manner. In the 1950s there was a lot of work done on ethnic minorities through ethnology. Academics did field trips to note down and find out facts about ethnic minorities, however there was little analysis done in the way research as was conducted in the West.

<strong>How has the sentiment towards Australia changed?</strong>
In those days, the Chinese people thought that Australia was the running dog of capitalist American imperialism. They thought Australia was a bit like a colony of the US. My interpretation is that Australia was very heavily influenced by the Americans. Yes we did follow the Americans into the war in Vietnam and Korea, however there were some distinctions. For example, we traded with China in the early 1960s when the US refused to do so. In particular a substantial wheat trade. For example, if you bought anything from Hong Kong, you had to have a certificate of certification that it did not originate from Mainland China before you were allowed to bring it into the US. When I first obtained an Australian passport I had to have it registered with the Chinese officials as back in that day, it had a clause that specified that the passport was not valid for the People’s Republic of China.

<strong>How do you seek to navigate potentially politically sensitive topics in your research?</strong>
I am quite well disposed towards China, however that does not mean that I am going to not say what I think. If I think something is going to be very controversial in Australia, I will not write on it, same goes for China. I don’t go out of my way to offend or provoke people wherever I am.

<strong>What makes China unique in regards to the situation of its ethnic minorities?</strong>
The fact that one ethnic group is so dominant in terms of population but not in area, the history of China in the last few hundred years and the expanse of its borders. A lot of these factors could be said about Russia, however the ethnic Russians comprise of a much smaller population of the whole of Russia than the Han do of China. Furthermore, Chinese policy on ethnic minorities is derived from the Soviet Union. Compared to Australia, where people may be identified by what language they speak at home, in China there has been a historically constituted community of people having a common language, economic life, territory and a common psychological makeup which expresses itself in a common culture.

<hr />

<em>This interview was conducted by Aaron Zee for the May 2016 edition of ChinaBites. Read the original <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=da404d026d&amp;e=9cd1615cba">here</a>.</em>