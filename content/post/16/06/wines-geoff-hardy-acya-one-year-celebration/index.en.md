{
  "title": "Wines by Geoff Hardy and ACYA: One Year Celebration",
  "author": "ACYA",
  "date": "2016-06-28T09:13:47+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Wines by Geoff Hardy at ACYA NSW’s Careers Banquet",
      "type": "image",
      "src": "Wines-by-Geoff-Hardy-at-ACYA-NSW’s-Careers-Banquet.jpg"
    },
    {
      "title": "Clinking glasses with a ‘干杯 (gānbēi)’ at ACYA NSW’s Careers Banquet",
      "type": "image",
      "src": "Clinking-glasses-with-a-‘干杯-gānbēi’-at-ACYA-NSW’s-Careers-Banquet.jpg"
    }
  ]
}
July will mark a year of cooperation between the Australia-China Youth Association and our wine partner, Wines by Geoff Hardy. A family-owned collection of wineries across the Adelaide Hills and McLaren Vale in South Australia, Wines by Geoff Hardy has been two-time winner of Winestate Magazine’s coveted ‘Australian Winery of the Year’, and has achieved the company’s best ever results in the CWSA Best Value 2016.

<img src="http://www.acya.org.au/wp-content/uploads/2016/06/360截图20160628143841513.jpg" alt="Wines" width="580" height="400" class="alignleft size-full wp-image-24096" />
<em>Wines by Geoff Hardy on display at the Monash Careers Evening, 2016</em>

Also in early 2016, the company achieved its best ever results in the China Wine and Spirits Awards (Best Value), with several Double Gold and Gold Medals. ACYA is proud to support the team at Wines by Geoff Hardy’s excellence in China.

<img src="http://www.acya.org.au/wp-content/uploads/2016/06/ACYA-Adelaide-visiting.jpg" alt="ACYA Adelaide visiting" width="580" height="580" class="alignleft size-full wp-image-24097" />
<em>ACYA Adelaide visiting K1 Cellar door, 2015</em>

Throughout the past year, Wines by Geoff Hardy has supplied their award winning wines to more than ten major events around Australia, including our national Australia-China Emerging Leaders Summit (ACELS) and other combined state-based events. The wines lent their flavours perfectly to both Chinese dinners and networking evenings across Sydney, Melbourne, Brisbane, Canberra, Adelaide and Perth. 

We thank Wines by Geoff Hardy for their continued support and look forward to our cooperation in the future!

<img src="http://www.acya.org.au/wp-content/uploads/2016/06/Wines-by-Geoff-Hardy-at-ACYA-NSW’s-Careers-Banquet.jpg" alt="Wines by Geoff Hardy at ACYA NSW’s Careers Banquet" width="580" height="400" class="alignleft size-full wp-image-24099" />
<em>Wines by Geoff Hardy at ACYA NSW’s Careers Banquet, 2016</em>

<img src="http://www.acya.org.au/wp-content/uploads/2016/06/Clinking-glasses-with-a-‘干杯-gānbēi’-at-ACYA-NSW’s-Careers-Banquet.jpg" alt="Clinking glasses with a ‘干杯 (gānbēi)’ at ACYA NSW’s Careers Banquet" width="580" height="400" class="alignleft size-full wp-image-24100" />
<em>Clinking glasses with a ‘干杯 (gānbēi)’ at ACYA NSW’s Careers Banquet, 2016</em>


For more information on Wines by Geoff Hardy or if you want to purchase some of their single estate premium and other wines, please see their website!
http://www.winesbygeoffhardy.com.au/

