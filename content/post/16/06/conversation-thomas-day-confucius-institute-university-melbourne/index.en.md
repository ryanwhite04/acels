{
  "title": "Think Confucius, expat living, and footy don't go together? You need to meet Thomas Day",
  "author": "ACYA",
  "date": "2016-06-06T09:31:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Thomas Day",
      "type": "image",
      "src": "Thomas-Day.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2016/06/Thomas-Day.jpg" rel="attachment wp-att-24028"><img class="size-medium wp-image-24028 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/06/Thomas-Day-300x300.jpg" alt="Thomas Day" width="300" height="300" /></a>

We caught up with Thomas Day, the ‎External Relations and Projects Manager at Confucius Institute at the University of Melbourne. Thomas is a Chinese speaker who has broad experience in the Greater China region where he has worked, lived and studied for over five years. His in-depth understanding of the business, political and cultural ties that exist between Australia and China comes through his previous roles at the Australia China Alumni Association, Australia China Business Council Victorian Branch, Austrade Shanghai and the Australian Pavilion at the 2010 Shanghai World Expo.

<strong>Can you tell us about your work at the Confucius Institute?</strong>
I manage our business programs which seek to enhance Victorian business’ China capabilities mainly through language and cross-cultural training. We have some great language clients from the state and local government as well as a number of corporates who are engaged with business with China. I also work closely with my colleagues from Asialink Business, where we jointly deliver a number of China-focused cross-cultural training modules. In addition, I'm lucky enough to organise our Student and Young Professional Study Tour to China, where participants partake in cultural activities, briefings and visits in Beijing, Nanjing and Shanghai, as well as Chinese language and culture short-course at Nanjing University.

<strong>What advice would you give to students and young professionals who want to embark on a career in the Australia-China space?</strong>
There is so much activity in the Australia-China space at the moment and you need to be across a lot of the key trends in the market as everything changes at a rapid pace in China. Organisations such the Australia China Youth Association (ACYA), Australia-China Young Professionals Initiative (ACYPI) are a great way to learn about some of the key issues in the bilateral relationship. They also offer a great opportunity to meet with a large cohort of peers interested in and engaged in the Australia-China space. Asialink and the Australia China Business Council run a number of great events and programs as well.

I also think spending time on the ground in China is essential to understand the changing nature of the Chinese economy and society. It's important to build and develop your own set of networks in China for personal and professional purposes. I would look at the variety of study, work and travel options out there. Asia Options does a great job at highlighting many of these.

<strong>You played footy in China. Do you see AFL taking off there?</strong>
I loved playing footy in China and have had the fortune of representing the Beijing Bombers, Shanghai Tigers and China Reds where I met a fantastic group of guys, many of whom I still see regularly today. Whilst we had a few local guys who would play with us in Beijing and Shanghai I know the game has taken off in Guangzhou, where they have over 50 locals playing regularly in the Southern China AFL League. The captain of Team China, Chen Shaoliang, who plays in this league has recently been signed by Port Adelaide on an international scholarship and hopefully he makes the big league! Whilst I don’t think AFL will suddenly become the game of choice in China there is a lot of interesting movement in the corporate world with companies such as China Southern Airlines, TCL and Huawei all sponsoring AFL teams. I think that will be the interesting space to watch over the coming years.

<strong>What was your favourite experience as a student and expat in China?</strong>
Living in China gives you rare access to government and business leaders as well a large cohort of China focused expatriates. Every night of the week, there would be an awesome event to attend and you would constantly bump into talented people doing interesting things. I remember one day where I met with a group of talented Chinese graduates from Australian universities in the morning, the Australian Prime Minister in the afternoon and then partied with Saudi Princes (and their bodyguards) at night!

<hr />

<em>This interview was conducted by Caitlin Hardy for the <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=814441737c&amp;e=9cd1615cba">May 2016</a> edition of AustraliaBites.</em>