{
  "title": "Dancing aunties, dumplings and megacities: My internship with Austrade Shanghai",
  "author": "ACYA",
  "date": "2016-08-06T11:47:22+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "thumb_Shanghai I love SH_1024",
      "type": "image",
      "src": "thumb_Shanghai-I-love-SH_1024.jpg"
    },
    {
      "title": "thumb_Shanghai yu gardens_1024",
      "type": "image",
      "src": "thumb_Shanghai-yu-gardens_1024.jpg"
    },
    {
      "title": "thumb_Shanghai street food_1024",
      "type": "image",
      "src": "thumb_Shanghai-street-food_1024.jpg"
    },
    {
      "title": "Shanghai_Luisa Cools",
      "type": "image",
      "src": "Screen-Shot-2016-08-06-at-19.56.59.png"
    }
  ]
}
One of our ACYA members, <a href="https://www.linkedin.com/in/luisacools">Luisa Cools</a>, recently participated in a two-month internship with Austrade in their Shanghai office. She gives us the lowdown on her time in China, from working on the 'Landing Pad' innovation hub initiative that connects Australian start-ups with the Chinese market to navigating daily life in Shanghai and seeking out the very best <em>xiaolongbao</em> the city had to offer!

<!--more-->

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/thumb_Shanghai-I-love-SH_1024.jpg" rel="attachment wp-att-24183"><img class="wp-image-24183 size-medium aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/thumb_Shanghai-I-love-SH_1024-300x169.jpg" alt="thumb_Shanghai I love SH_1024" width="300" height="169" /></a>

With locations in more than 40 countries and with more than 10 offices in Greater China alone, I was lucky enough to be placed with Austrade’s largest offshore office: Shanghai. The office is right in the bustling centre of the city on Nanjing Road West, in the same building as the Australian Consulate, and most of the state government trade and investment offices. There are more than 30 staff in the office, a mix of both locally-engaged employees and Australia-based Austrade staff. The team is wonderful, fun, hardworking and above all, passionate about the Australia-China relationship.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/thumb_Shanghai-yu-gardens_1024.jpg" rel="attachment wp-att-24184"><img class="aligncenter wp-image-24184 size-medium" src="http://www.acya.org.au/wp-content/uploads/2016/08/thumb_Shanghai-yu-gardens_1024-300x169.jpg" alt="thumb_Shanghai yu gardens_1024" width="300" height="169" /></a>

I arrived just after the wrap-up of one of Australia’s largest overseas trade missions to date, the Australia Week in China (AWIC) 2016, which saw a delegation of more than 1,000 head to Shanghai for a series of events, workshops, site visits and lectures. I was so thankful for the welcoming and open embrace from all of the staff at Austrade.

I was fortunate enough to work on the most exciting project while at Austrade: establishing an innovation hub in Shanghai for Australian start-ups to launch their products in China. This ‘Landing Pad’ initiative is one element of the Turnbull Government’s global NISA strategy and was so exciting to be a part of! I was able to sit in on various internal and client meetings, attend site visits and get an insight in to the day-to-day functions of a government agency and their relationship with other departments and ministries – all of which was incredibly valuable experience.

However, the internship is just one part of the learning experience. More than any other place in China, I feel that Shanghai really represents the growth and chaotic rise of the Chinese economy and the country’s modernisation. While Shanghai is definitely a melting pot of Chinese and foreign, modern and old, rich and poor, Chinese culture is still very present in daily life. Shanghai is famous for Xiaolongbao dumplings (which was all my diet consisted of while I was there), and you couldn’t escape the fake goods hagglers, the dancing aunties jamming out to their music in inconvenient and obstructive public places, the groups of old folks singing Cultural Revolution era songs and the young and wealthy flitting in and out of the brand new shopping malls. People I ran in to on the street loved it when they found that I could speak Chinese and I improved my vocabulary so much in use in daily life.

My internship was two months and it left me wishing that I had been there for six months or more! I highly recommend anyone with an interest in China to apply for the Austrade internships as there is nothing more valuable than in-country experience, seeing China from the inside-out, and placing Australia’s global role in a broader perspective.