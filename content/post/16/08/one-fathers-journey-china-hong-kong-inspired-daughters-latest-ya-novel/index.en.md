{
  "title": "How one father's journey from mainland China to Hong Kong inspired his daughter's latest YA novel, 'Freedom Swimmer'",
  "author": "ACYA",
  "date": "2016-08-25T00:00:36+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "WaiChimAuthorProfile2",
      "type": "image",
      "src": "WaiChimAuthorProfile2.jpg"
    },
    {
      "title": "FreedomSwimmer_COVER5_Front",
      "type": "image",
      "src": "FreedomSwimmer_COVER5_Front.jpg"
    }
  ]
}
<strong>Your new novel, <em>Freedom Swimmer</em>, is based on the true event of your father’s swim from Communist China to Hong Kong when he was 15 years old. How much of the novel was a realistic reflection of his journey?</strong>

My father did lose his parents at the age of 15 and made the journey to Hong Kong when he was about 19 years old. He and a friend from the village spent about a year talking about the trip before finally making the journey on 1 September in 1973.

<!--more-->

It's always interesting to try and distinguish the line between real life and fiction, especially when the real life events inspire fictional recounts. To be completely honest, while my father was exceptionally helpful in providing some of the colour and the detail about the landscape, environment and the tone of the story and a lot of the background on what was happening with the cadres and the famine, most of the actual 'actions' that happen through the story are fictional accounts.

On the other hand, the events could be real. When I tried asking my father if he had any special female friends or potential love interests back when he was in the village, he glanced nervously at my mother and loudly declared "Of course not!".

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/FreedomSwimmer_COVER5_Front.jpg" rel="attachment wp-att-24238"><img class="aligncenter wp-image-24238" src="http://www.acya.org.au/wp-content/uploads/2016/08/FreedomSwimmer_COVER5_Front-655x1024.jpg" alt="FreedomSwimmer_COVER5_Front" width="200" height="313" /></a>
<strong>What inspired you to write a novel about this particular period in China? </strong>
It was largely inspired by the release of <em>Tombstone: The Untold Story of Mao's Great Famine</em>. I saw this and then read <em>Mao's Great Famine</em> and realised, "Wait a minute, my father lived through this." He never really talked about his childhood when I was growing up and gave pretty vague answers whenever I asked. I figured he wasn't really comfortable with talking about it. It wasn't until I really immersed myself in the research did I fully understand why. My father was a bit apprehensive at first but I think writing this book proved to be a great benefit to our relationship because I finally had some common ground to discuss the matters with him. I had the right questions and could understand more about where he was coming from, or as much as any American with first world problems can relate to such tragedies.

<strong>You've previously authored a number of children’s books, including the <em>Chook Chook</em> series. Why write a young adult/adult novel this time?</strong>
I have always loved YA and had always wanted to write a young adult novel. I have a few early drafts of YA manuscripts that failed and it took me awhile to find the right story to tell. However, at the end of the day, as many writers find, our stories are dictated by our characters. Where the final work ends up on the children's to adult spectrum is largely independent of the story that the characters set out to tell.

<strong>As someone who was raised in New York City and currently lives in Sydney, what drives your decision to set your books in China?</strong>
I'm a huge supporter of diverse stories, especially for children. I remember being a little girl and loving any book that came out that had an Asian face as the main character. I still tend to gravitate towards Asian stories I think because I largely relate to those experiences. And because I am a first-generation Chinese-American, I wanted to understand a little bit more about my own culture and heritage and especially some of the recent historical context. Writing my books allowed me to do so.

<strong>In comparison to Asian Australian culture, Asian American culture seems to have gained more public ground in the United States — there are dedicated university courses, publications, TV shows, even parenting books (hello Tiger Mom!). After living in Sydney for five years, what are your thoughts on any similarities or differences?</strong>
I am so super glad you asked this question! This was one of the first things that struck me when I came to Australia. Because I'm from New York, my childhood was kind of unique when it came to culture and identity. My high school was actually 52% Asian so the Asian-American experience was the majority and really common. I remember always feeling a bit alienated by my peer groups because I never really had a strong fondness for bubble tea (I still don't!).

Asian culture seems, in some ways, to be inherently ingrained in Australian culture, with every country town seemingly having a local Thai joint and many of the most popular metro restaurants with Asian-inspired dishes, the Asian- Australian identity has not yet emerged as strongly in media and pop culture. This is the American hypothesising (or bullshitting!) so please take it with a grain of salt, but it may be that there is so much discussion around establishing the Australian identity itself and distinguishing it from British or American culture, that sub-divisions become lost in the conversation.

But I think it's changing and it's largely being lead a lot by the movements in the US and overseas. Certainly in the children's book world, diversity has become a hot topic in the US and I'm definitely seeing it being championed here in Australia as well. So with these discussions, I'm confident that these broader identity conversations will follow. I hope so anyway!

<strong>Last, do you have any words of advice for our readers interested in pursuing a creative writing career?</strong>
Work really, really, really hard. Be exceptionally kind and humble. Be passionate. Be obsessive. Be emotional. Be ready to fail. But then have the confidence in yourself that you can always do better. Because you are amazing!

<em>This interview was conducted by Olivia Gao.</em>

&nbsp;