{
  "title": "Think your Chinese skills are pretty good? The Chinese Bridge Competition is for you",
  "author": "ACYA",
  "date": "2016-08-20T22:59:32+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "group 3",
      "type": "image",
      "src": "group-3.jpg"
    },
    {
      "title": "1",
      "type": "image",
      "src": "1.jpg"
    },
    {
      "title": "group",
      "type": "image",
      "src": "group.jpg"
    },
    {
      "title": "group 2",
      "type": "image",
      "src": "group-2.jpg"
    },
    {
      "title": "3",
      "type": "image",
      "src": "3.jpg"
    }
  ]
}
The Chinese Bridge Competition is a language proficiency competition held for foreign university students in Changsha, China. It is a platform that builds a communication bridge between young people of China and of other countries. This year, 146 university students from 108 countries came together to put their Chinese cultural knowledge and language skills to the ultimate test! I was fortunate to participate in this large-scale international competition after placing first in the preliminary rounds (held at your local university). At that point I didn’t know what I was in for, but I can now say it was one hell of a journey.

<!--more-->

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/group-2.jpg" rel="attachment wp-att-24224"><img class="size-medium wp-image-24224 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/group-2-300x188.jpg" alt="group 2" width="300" height="188" /></a>

After a 14-hour flight from Brisbane to Beijing I landed just before midnight in a hotel lobby filled with cameras and volunteers shuffling in groups of students as they arrived. As you can imagine we were all quite ready for bed, but we were placed straight in front of the cameras and told to briefly introduce ourselves (I hope they don’t use that footage).

With just a few hours of sleep, we were off bright and early the next day to travel to one of China’s most historical sites, the Forbidden City. In the scorching Beijing summer heat, we wandered around the palace, enjoying the marvellous architecture, filming every now and then and of course frequently stopping to allow Chinese tourists to take photos with us. The day after that was another early start as we travelled to the Great Wall of China. My third time on the Great Wall and it still never ceases to amaze me.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/group-3.jpg" rel="attachment wp-att-24221"><img class="size-medium wp-image-24221 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/group-3-300x188.jpg" alt="group 3" width="300" height="188" /></a>

After the sightseeing in Beijing concluded, it was straight on a plane to Changsha for the competition to begin the following day (what we had all been waiting for). The competition was held over a jam-packed two days as contestants fought it out for a spot in the top 30.

In order of region, each contestant presented a 90 second speech from memory, with the option to present either ‘My Dreams for a Bright Future’ or ‘A gift for the Chinese Bridge Competition.’ I choose the latter, and brought along a cute stuffed sheep to discuss the growing ties between both China-Australia, especially through trade (maybe a little boring for Chinese television). With the bright lights, numerous cameras and the sound of the timer counting you down, it was quite stressful. For those that did do well, they just got up there, made the audience laugh and had fun.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/1.jpg" rel="attachment wp-att-24222"><img class="size-medium wp-image-24222 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/1-300x200.jpg" alt="1" width="300" height="200" /></a>

The speech was immediately followed by an ad hoc cultural question from the judging panel. Questions ranged from “What’s your favourite Chinese cuisine?” to “List the most famous four towers in China.” As soon as the question is asked a 50 seconds countdown begins for you to provide an adequate answer.

Then there was a quick change to prepare for the cultural performance, the most exciting part. There was everything from stand-up comedy, acrobatics, martial arts to Chinese folk arts, traditional musical instruments, traditional dance and Chinese calligraphy. The judges mostly enjoyed performances that ‘wowed’ the audience. Some of these included: a beautiful piece performed on a large golden harp whilst singing a soft Chinese melody. Also a crazy acrobatic slash martial arts performance called Shaolin Kung Fu rope dart (绳表) to a loud upbeat Chinese backing track.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/3.jpg" rel="attachment wp-att-24225"><img class="size-medium wp-image-24225 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/3-200x300.jpg" alt="3" width="200" height="300" /></a>

It’s not over yet! After all the presentations were concluded we were given one day to prepare for the Cultural exam. If the pressure to pass the exam wasn’t enough, we were placed in a room like you would see on a real gaming show. Cameras facing you from every angle as you struggled to answer the cultural question in the 5 seconds they allowed (great for TV, not so much for the contestants).

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/group.jpg" rel="attachment wp-att-24223"><img class="size-medium wp-image-24223 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/group-300x200.jpg" alt="group" width="300" height="200" /></a>

Once the top 30 was selected they were quickly separated from the rest and whisked away for more filming. For those of us who did not make it into the top 30, fortunately the pressure was off, the cameras disappeared, and we could start to enjoy the city of Changsha. At the end of the 12 days it was hard to say goodbye to the many new friends made. I met so many wonderful students, some as far as Ireland and as close as New Zealand. It may have been fast paced and demanding, but it was an experience I will never forget.

I highly recommend entering this competition whether you’re already fluent or just starting out. It is a fantastic opportunity to improve your Chinese language skills and meet likeminded individuals from all over the world. The entire trip is covered by the Confucius Institute and all contestants that make it to the semi-finals in China receive a one semester scholarship to study at a Chinese University of your choice!

If you have any questions regarding the competition you can contact me on WeChat: melodievalk Good luck!

<em>Melodie Valk is a final year Business Management/Arts student at the University of Queensland.</em>