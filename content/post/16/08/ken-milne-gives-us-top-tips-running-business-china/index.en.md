{
  "title": "Ken Milne shares his top tips on running a business in China",
  "author": "ACYA",
  "date": "2016-08-06T05:21:48+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "1d04670f-c9c1-4600-bea3-c0f0f174d832",
      "type": "image",
      "src": "1d04670f-c9c1-4600-bea3-c0f0f174d832.jpg"
    }
  ]
}
This month, we interviewed New Zealand businessman Ken Milne about his experience of working and conducting business in China. Eleven years ago, Ken arrived in Shanghai intending to stay for a six-month work placement, however, he decided to stay a while longer when business opportunities arose. With a professional background in dairy production and sales to Asia, Ken decided to start his own business with a focus on ice-cream distribution, food machinery and packaging.

<!--more-->

<strong>What are your top three tips for running a successful business in China?</strong>
Number one, think local and act local. Generally you can’t come in as an arrogant foreign company, thinking you know more than they do because the reality is you don’t. So if you localise, your market is mainland Chinese people, not foreigners living in China. Then you have to be able to tailor your business and your sales to fit in.

Number two, hire some very dependable local staff and assistants. Those staff and assistants must see a future plan, or some future for themselves in your business. Because most Chinese still want to further themselves financially and experience-wise, so they are motivated to learn as much as they can. If you want to retain them, you need to reward them and incentivise them.

Number three, don’t trust anyone. Think twice about everyone’s motivation and don’t take everything at face value because you are never really sure exactly who you’re dealing with or what they want from you.

<strong>What do you think is the major difference in business culture between China and the West?</strong>
I guess guanxi is always important to Chinese business and it’s difficult for Western people to come in and establish good, trusting relationships. It takes some time. In China, you want to be a customer of Chinese suppliers and you have to deal with them because you have no one else to deal with. However, it takes a while to filter out the good from the bad.

Generally, I still find that Chinese will tell you only what they think you should know. They are not dishonest, but they don’t tell you the whole truth. They might say, "we can’t finish this job today" but they don’t say "we can’t finish this job today because the carton boxes never arrived in time".

<strong>You came to China for business more than 10 years ago. What advantages do you think China has as an emerging economy?</strong>
In the 1980s, when Deng Xiaoping opened China for business, China was given an extraordinary step up because they were gifted with the latest technology at the very beginning of their development. They didn’t start with the 1950s and 1960s machinery; they just started with CNC machines, robots, complete production line from Germany. They didn’t have to invent those things themselves. From that base, China has been growing extremely fast.

In the 1980s, when the Americans and Germans were coming and bringing their systems and so on, these companies were able to not only reinvent the wheel, but start with the established processes. Since then, because of foreign education, young Chinese will go on to Yale and Harvard and Oxford and everywhere else to study, and when they come back and work in these kinds of companies, they’ll bring back overseas experience. They can also do things very well, with Chinese language, Chinese philosophy, Chinese understanding, as compared to foreigners. A laowai is always a laowai, a Chinese is always a Chinese. These returned graduates are available at a much lower salary than foreigners on an expat package.

<strong>How does localisation work in China? Is there a pattern you can follow?</strong>
France is pretty much the same now as it was 30 years ago and will be pretty much the same in another 30 years. However, China changes every single year, so the pace here is really hard to keep up with. I spoke to a man today who deals with a famous restaurant in Shanghai. When they visited third-tier cities, they said it was like Shanghai 10 years ago. Now Shanghainese understand raw salad, they understand healthy drinks, and they understand Western food. But you go somewhere like, say Wuhan, and you have to teach Wuhan people it’s okay to have the raw, green stuff on your plate and it doesn’t have to be cooked. And you have to study the marketing all over again. For the workload involved, operating in a third-tier city tends to be less lucrative, as market prices are half those of Shanghai. As such, the profit margin is small. So they might as well just open another 10 stores in Shanghai than open 10 stores in second-tier cities. What works in Shanghai may not necessarily work elsewhere.

<em>Interview has been edited for length and clarity. This interview was conducted by Maddie (Ruiming) Ma for the <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=19204ed0dd&amp;e=9cd1615cba">July 2016 edition of ChinaBites</a>.</em>

&nbsp;