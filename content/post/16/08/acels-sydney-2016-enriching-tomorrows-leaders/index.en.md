{
  "title": "ACELS Sydney 2016: Enriching Tomorrow's Leaders",
  "author": "ACYA",
  "date": "2016-08-22T01:21:45+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS16-68",
      "type": "image",
      "src": "ACELS16-68.jpg"
    },
    {
      "title": "ACELS Enriching a New Generation of Australia-China Literate Youth",
      "type": "application",
      "src": "ACELS-Enriching-a-New-Generation-of-Australia-China-Literate-Youth.pdf"
    }
  ]
}
ACYA is pleased to present the Sydney 2016 Edition of <a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Enriching-a-New-Generation-of-Australia-China-Literate-Youth.pdf" target="_blank" rel="noopener noreferrer">ACELS: Enriching a New Generation of Australia-China Literate Youth</a>. The 6th edition of the Australia-China Emerging Leaders Summit was held in Sydney from the 29th to the 31st of July, with 60 delegates attending from cities across Australia and China. The delegates attended keynote speeches, contributed to panel discussions and networked with business leaders over an intense three-day program.

With support from major sponsor Schwarzman Scholars, venue partners the University of Sydney's China Studies Centre and the Westpac Bicentennial Foundation, and official wine sponsor Wine by Geoff Hardy, ACELS Sydney 2016 provided insights and opportunities for all present. <a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Enriching-a-New-Generation-of-Australia-China-Literate-Youth.pdf" target="_blank" rel="noopener noreferrer">Click here</a> to r<span style="font-weight: 400;">ead more about the key events and meet some of the delegates who attended ACELS Sydney 2016!</span>