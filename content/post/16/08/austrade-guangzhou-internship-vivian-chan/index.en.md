{
  "title": "Diary of my internship with Austrade in Guangzhou ",
  "author": "ACYA",
  "date": "2016-08-31T07:33:37+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Picture1",
      "type": "image",
      "src": "Picture1.png"
    },
    {
      "title": "Picture2",
      "type": "image",
      "src": "Picture2.png"
    },
    {
      "title": "Picture3",
      "type": "image",
      "src": "Picture3.png"
    },
    {
      "title": "Picture4",
      "type": "image",
      "src": "Picture4.png"
    },
    {
      "title": "Picture5",
      "type": "image",
      "src": "Picture5.png"
    },
    {
      "title": "Picture6",
      "type": "image",
      "src": "Picture6.png"
    },
    {
      "title": "Picture7",
      "type": "image",
      "src": "Picture7.png"
    }
  ]
}
I stood looking at the light green pool of water before me, marvelling at the enormous impact this small – almost unnoticeable if it wasn’t now reconstructed as part of a museum – body of water contributed to the modern history of China. Several of these pools exist, but this was one of the only ones left protected for today’s museum-goers. 177 years ago, Lin Zexu, an Imperial Qing official, destroyed more than one thousand long tons of illicit opium confiscated in the Pearl River Delta region, triggering the First Opium War, which led to Hong Kong becoming a British concession... (the rest is history).

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/Picture1.png" rel="attachment wp-att-24253"><img class="wp-image-24253 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/Picture1-300x224.png" alt="Picture1" width="349" height="260" /></a>

<span style="font-weight: 400;">This museum is located in Humen, Dongguan, a small town now known for its enormous manufacturing industry, even being named ‘China’s factory to the world’, despite recent economic downturns which saw the migration of factories to lower-cost locations in South East Asia. Today, almost two centuries after the turbulent historical events that took place here, I visited Humen as part of my internship with the Australian Trade and Investment Commission (Austrade), en route to attending meetings and an industry seminar about the recent China-Australia Free Trade Agreement given by the Trade Commissioner (Guangzhou).</span>

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/Picture3.png" rel="attachment wp-att-24255"><img class="wp-image-24255 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/Picture3-224x300.png" alt="Picture3" width="214" height="287" /></a>

<span style="font-weight: 400;">And this was exactly what I loved about the internship in the Austrade Guangzhou office. Guangzhou is located in the Pearl River Delta region of South China, less than three hours away from Hong Kong, Macau and Shenzhen on super comfortable trains. Over the past ten years, Guangzhou city has become a metropolitan commercial hub of its own. The Pearl River New Town area in which the Austrade office is located is almost always bustling – even close to midnight. Guangzhou gave me a unique impression as it combines the old and the new, with advanced usage of e-commerce channels like WeChat, one can ‘like’ restaurants and companies’ pages to get free deals and discounts – in almost every single restaurant I’ve been to. For someone who has spent half her life living in Hong Kong and visiting Guangzhou before any of these developments, and half her life living in Canberra, this internship was indeed an eye-opener.</span>

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/Picture5.png" rel="attachment wp-att-24257"><img class="wp-image-24257 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/Picture5-300x225.png" alt="Picture5" width="332" height="249" /></a>

<span style="font-weight: 400;">Although my internship involved participating in day-to-day Austrade activities, such as scheduling meetings and providing event support, I also had many opportunities, thanks to the generosity of the Guangzhou team, to explore Guangzhou’s culture and business environment as part of my work. Austrade has a large, but close-knit network. It was easy to communicate with someone on the other side of China, or on the other side of the world. There are also many training and team-building opportunities. Since everyone who worked there all have a can-do, proactive and supportive attitude, I had no trouble settling into the working environment. As I undertook the internship during the 2016 election period, I was even invited to give an update on #auspol in the office and discuss the situation with my colleagues. Because I was thrown into the work of the organisation on the first day of my internship, I had the opportunity to practice my Cantonese and Mandarin almost immediately by communicating to local Austrade staff, and local businesses. Imagine the fun (and fear!) of having to organise catering on WeChat, in Mandarin, on the first day of the job! On that note, when you’re in South China, you do get brownie points if you can speak Cantonese!</span>

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/Picture6.png" rel="attachment wp-att-24258"><img class="wp-image-24258 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/Picture6-300x267.png" alt="Picture6" width="314" height="279" /></a>

<span style="font-weight: 400;">Everyday, despite the humidity and heat (I resisted the temptation of bringing a tiny electric fan around, which seemed to have become quite the fashion statement at the time), there was something new to marvel at: an inexpensive but scrumptious bowl of Japanese ramen, delicious and traditional dim sum at yum cha (this is the region where </span><i><span style="font-weight: 400;">yum cha </span></i><span style="font-weight: 400;">came from!), being able to visit Foshan (think kung fu and dragon dance) within two hours, affordable afternoon tea during a karaoke session with your friends, trying on </span><i><span style="font-weight: 400;">qipao </span></i><span style="font-weight: 400;">on historical shopping strips, boutique bookshops with cafés that belong in a Melbourne laneway…I can go on and on.</span>
<p style="text-align: center;"><a href="http://www.acya.org.au/wp-content/uploads/2016/08/Picture7.png" rel="attachment wp-att-24259"><img class=" wp-image-24259 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2016/08/Picture7-300x225.png" alt="Picture7" width="345" height="259" /></a></p>
<span style="font-weight: 400;">I am extremely lucky to have picked Austrade Guangzhou for my internship. Through the ACYA application system and depending on the needs of Austrade, applicants could pick from up to six locations in which Austrade has offices in China. Twelve months ago I was anxiously awaiting a phone call from the Guangzhou team to conduct a phone interview with me, and test my Cantonese and Mandarin skills. Today, I can happily say to anyone considering applying to this internship through ACYA: don’t miss it for the world! This could be a massive step on your way to become a China-expert in Australia or an Australia-expert in China. Either way, there is no better time than now to consider working in the China-Australia economic and policy space.  </span>
<p style="text-align: left;"><i><span style="font-weight: 400;">About the author: Vivian Chan is a fourth-year student of Law (Honours)/International Relations at the Australian National University. She completed a short internship in Austrade Guangzhou in June-July this year. She is currently participating in a short internship at a Canberra-based advisory firm on pro bono, China-related assignments and working as a retail assistant manager. She can be contacted on </span></i><a href="mailto:wh.vivian.chan@gmail.com"><i><span style="font-weight: 400;">wh.vivian.chan@gmail.com</span></i></a> <i><span style="font-weight: 400;">for inquiries about the post or the internship application process.</span></i></p>