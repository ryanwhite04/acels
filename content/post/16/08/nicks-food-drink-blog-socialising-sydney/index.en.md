{
  "title": "Nick's Food and Drink Blog – Socialising in Sydney",
  "author": "ACYA",
  "date": "2016-08-18T05:37:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "13956899_10210027898672563_1377800941_n",
      "type": "image",
      "src": "13956899_10210027898672563_1377800941_n.jpg"
    }
  ]
}
Last week marked the end of the 2016 Australia-China Emerging Leaders Summit (ACELS).  With delegates coming from an array of professions and studies, and even all the way from China and Hong Kong, I thought it'd be appropriate to write about some of my favourite food and drink "haunts" in Sydney - this blog might even benefit some of our friends interstate!

<!--more-->

I've no doubt the delegates participated in a wide range of networking events, drinks and the like, but how many of them were aware of what ordinary 'Sydneysiders' do for fun; where they go?  Whilst I can't speak for all 'Sydneysiders', here's my take.

Most 'Sydneysiders', like any Aussie, enjoy a night out with friends, eating and drinking.  Quite often, you can find me at the pub digging into a chicken parmi (short for parmigiana; a cheese-laden chicken schnitzel), and 'downing' a beer with my mates.  I think this makes for a quintessentially Australian experience.  I imagine this is similar to what is done in China; hot-pot (火鍋) and beers with friends.  On other days, perhaps more elegant and refined days, I'd be with a small group of friends at a bar.  Although admittedly, I do sometimes wish for more karaoke (唱K) with friends; shamelessly singing the songs of Cold Chisel and John Farnham (if you don't know who they are, you should look them up: they're as Aussie as they come).

<strong><em>Nick, what's the difference between a pub and a bar? </em></strong>

Well, pubs started out as "public houses", and have now become an inherited and much loved suburban establishment from Britain.  Initially rowdy and disreputable, they've now become the place to grab a cheap drink and hearty meal, without having to venture into the city.  Conversely, bars, like China and Hong Kong, follow that universal definition: cocktails (shaken not stirred?) and spirits (maybe not baijiu (白酒)), usually enjoyed with good company in (more) elegant surrounds.

<strong><em>Nick, what do you order at your local pub?</em></strong>

Well, you can't go wrong with a steak, or a chicken schnitzel: served with a side of chips, they're worthy of abandoning any month-long die (or in my case, usually a day-long diet), and exercise regime. No matter where you are in Sydney, I'm sure there's a decent local pub; nothing too fancy, but I'm sure she'll do.  If you're ever up my way, I'd be happy to have a beer or two at my "local"; the Greengate in Killara.

But, you probably didn't read this blog-post for recommendations in Killara.  So, here's a pub you visit if ever you find yourself in Sydney.<strong> <a href="http://www.acya.org.au/wp-content/uploads/2016/08/13956899_10210027898672563_1377800941_n.jpg" rel="attachment wp-att-24216">
</a></strong>

<strong>The Hero of Waterloo: <em>81 Lower Fort Street, Millers Point</em></strong>

I know of no other pub that houses both a ghost, and a secret tunnel leading to Sydney Harbour.  The ghost is rumoured to be that of Anne Kirkman's, pushed down the stairs by her husband in 1849.  The secret tunnel, which allegedly ran from the pub to the harbour, operated in order to smuggle rum into the then rowdy and unruly colony of New South Wales.  Open for lunch and dinner seven days a week, and located in Sydney's historic Rocks area, why wouldn't you give this pub a visit?

<em>Nearby things to do: </em>Why not catch a play at the Sydney Theatre Company?<strong><em> </em></strong>

And a good bar?

<strong>The Baxter Inn: <em>156 Clarence Street, Sydney</em></strong>

Tucked away in a basement, that is in turn tucked away in an alleyway, is a bar that houses more than 800 different whiskies.  For a peaty (smoky) whisky, ask for a Kilchoman Machir Bay, or alternatively, have a chat to one of the bartenders: they are some of the most knowledgeable and interesting characters you'll come across! If whisky's not your thing, don't worry, they also make a mean cocktail!  Designed like a prohibition-esque speakeasy, with smooth jazz all night, you don't need to enjoy drinking to have a blast; in fact, finding the bar itself is enough of a thrill.

Nick is a third year Law/Arts student at the University of Sydney and amateur sinologist with interest especially in the Australia-China space.