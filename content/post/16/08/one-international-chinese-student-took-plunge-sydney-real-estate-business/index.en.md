{
  "title": "How one international Chinese student took the plunge into the Sydney real estate business",
  "author": "ACYA",
  "date": "2016-08-03T05:01:18+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "d5e9551e-5076-4957-84f2-6aeadb3d45cd",
      "type": "image",
      "src": "d5e9551e-5076-4957-84f2-6aeadb3d45cd.png"
    }
  ]
}
This month, we chatted with a recently graduated Chinese student-cum-entrepreneur Shengnan Qian (Nannan), who is currently the director of a real estate company based in Sydney. Unlike other graduated international students, Nannan decided to start her own business, instead of looking for a permanent position in Australia or back in China. We discovered her secrets to success in starting her successful business in this interview.

<!--more-->

<strong>As a former Chinese international student, what made you start your own business? Tell us about what you do?</strong>
I had been studying international relations and finance for more than 6 years in Australia.  After I graduated from USYD, I got a precious opportunity to join a real-estate company - this inspired me to pursue this path. What I believe is that the first person who opens a door for you into a new industry is important. I got an investment opportunity due to the expansion of a real-estate agency company in Sydney. One of the shareholders was the first person who brought me into the real estate industry and let me became one of them.

At the same time, I noticed that the Australian real-estate market was substantial and continuously growing, which was the same as the market in China around 2005. People, especially Chinese, could see and smell the opportunity for growth in the Australian real-estate industry, and how it was not a crowded space yet. That was the moment I decided to start my own business, by taking the mortgage and borrowing finances to go out on my own. I would say it is suffering while enjoying.

<strong>What’s a typical day of work for you as a business newbie?</strong>
My day usually starts with a morning meeting with the sales manager and the property manager to understand and review both our sales and clients’ performance. Constantly striving for improvements to our work process and results to better meet client’s expectations is our aim.  After that, we have our training sessions, to study current and future property projects. It is important to keep learning and stand in front of other agencies. We also run tele-conferences with our sales team based in Shenzhen, China to bring new projects to them and answer their questions. Sometimes, I also have meetings with developers and peers to maintain a good relationship of cooperation and mutual understanding. Besides those schedules, I have to look after appointments with clients to ensure we continue to meet their expectations.

<strong>How did you find yourself doing what you’re doing now?</strong>
I entered the field as a general manager straight away - something I had never done before. After a long period of learning, pressure and challenges,  I found myself  with improved skills suited for managing, planning and communicating with people. Being the youngest executive in the company, I was proud.

<strong>What was the biggest challenge you faced when you started your own business?  </strong>
I will say the biggest challenge was  the responsibility of my role now. I became the boss on day one, and my staff needed my leadership - despite being new to the field as well, and only recently having finished my studies.  Some people did not believe I could handle this position and some of them did not even want to work with me at the very beginning. I had to let everyone see my ability and strengths instead of focusing on my age and lack of experience. After six months’ of hard work, the company’s seasonal profit went up 7.3%, and I can say I have proved myself!

<strong>What do you want to suggest to those students who keen to start their own businesses?</strong>
In my understanding, business is about teamwork. If you want to start your own business, find a valuable and similarly minded parter as your most important task. It is important also to always be confident, no matter how hard and how long it takes to solve problems. Bring your willpower and tell yourself there is no way you want to go backwards.

<em>This interview was conducted for the <a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=bc9f24dd52&amp;e=9cd1615cba">July edition of AustraliaBites</a>. </em>

&nbsp;