{
  "title": "ACELS Sydney 2016 wraps up",
  "author": "ACYA",
  "date": "2016-08-13T05:10:55+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "ACELS"
  ],
  "tags": [],
  "locations": [
    "Australia",
    "New South Wales",
    "Sydney"
  ],
  "organisations": [],
  "resources": [
    {
      "title": "(ACELS Blog) Photo 3",
      "type": "image",
      "src": "ACELS-Blog-Photo-3.jpg"
    },
    {
      "title": "(ACELS Blog) Photo 9",
      "type": "image",
      "src": "ACELS-Blog-Photo-9.jpg"
    },
    {
      "title": "(ACELS Blog) Photo 5",
      "type": "image",
      "src": "ACELS-Blog-Photo-5.jpg"
    },
    {
      "title": "(ACELS Blog) Photo 10",
      "type": "image",
      "src": "ACELS-Blog-Photo-10.jpg"
    },
    {
      "title": "(ACELS Blog) Photo 1",
      "type": "image",
      "src": "ACELS-Blog-Photo-1.jpg"
    },
    {
      "title": "(ACELS Blog) Photo 4",
      "type": "image",
      "src": "ACELS-Blog-Photo-4.jpg"
    }
  ]
}
The Australia China Emerging Leaders Summit, ACELS, is unequivocally the premier workshop for young leaders looking to grow within the Australia China space. This year's ACELS is the second iteration of the event and was held in Sydney in late July. Sixty delegates met in Sydney to “listen, learn, critique and discuss everything Australia China” in an intensive three-day conference combined with professional development and networking opportunities. The Summit's purpose is to build the personal networks and leadership capabilities of ACYA members and leaders from the broader Australia-China network.

<!--more-->

So, what kind of opportunities does ACELS present to young students and professionals who want to lead in Aus-China space? Asia Options was invited by ACYA to cover the Sydney Summit.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-1.jpg" rel="attachment wp-att-24203"><img class="aligncenter wp-image-24203" src="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-1-1024x576.jpg" alt="(ACELS Blog) Photo 1" width="500" height="281" /></a>

## Day 1

The evening's Keynote Address was by Cathy Monro, former China Liaison for the Australian Olympic Committee to the 2008 Beijing Olympics. Cathy set the tone for the weekend, urging delegates to step out of their cultural comfort zones in order to develop nuanced understanding of other cultures.

## Day 2

Saturday was the first and longest day for the ACELS delegates, comprising six formal presentations with eight speakers, an afternoon of fruitful ACYA chapter planning and an evening of networking.

Matthew Benjamin, of Asia Recon, and Peter Cai, from the Lowy Institute, covered prodigious ground in their morning seminar <em>Entering the Chinese Market: Challenges and Opportunities for Start-Ups</em>. Matthew and Peter both struck upon a common theme, emphasising that delegates should develop trust and relationships in order to plot a safe course through the business world in either Australia or China.

> Social norms in China are layered and change from industry to industry and region to region. You need to invest in relationships to see your way through.
>
> -- Matthew Benjamin

Professor Jeffrey Riegel, Director of the Chinese Studies Centre of the University of Sydney, then delivered a seminar on his career in Australian and American academia. He provided a historical context for contemporary Chinese business culture, summed together in four truisms: China is old, China is big and complex, China is diverse, and China is different.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-4.jpg" rel="attachment wp-att-24204"><img class="aligncenter wp-image-24204" src="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-4-1024x576.jpg" alt="(ACELS Blog) Photo 4" width="500" height="281" /></a>

Last was Dai Le, CEO and founder of DAWN, presenting on <em>Cultural and Gender Diversity within Innovation</em>. Dai spoke from almost three decades of experience pushing for diverse leadership in Australia. She encouraged delegates to recognise the Bamboo Ceiling is a real barrier for Asian leaders in many industries. Dai pointed to fields like fintech, ICT and online media to show the success of fields that accepted diverse talent, and talent goes when it isn't recognised by 'traditional' leadership structures.

That evening, delegates made their way to the Sofitel Sydney for the evening's function – a panel discussion and networking with representatives from PwC, Alibaba Australia and Hainan Airlines.

## Day 3
ACELS challenged delegates in new ways and revealed the depth of experience, skills and creativity of the ACYA network. Teams of delegates were put through their paces in event management and project management workshops.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-5.jpg" rel="attachment wp-att-24201"><img class="aligncenter wp-image-24201" src="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-5-1024x576.jpg" alt="(ACELS Blog) Photo 5" width="500" height="281" /></a>

Afterwards, each chapter presented to fellow delegates their events, plans and goals for the year.

David Kral, ACYA Nanjing, shared how the Nanjing chapter is both a landing pad for Australians studying and working in Nanjing, and also increasingly a first port of call for local Chinese students who are looking at living in Australia. The ACYA VIC BLC, now in its second year, is a language competition that celebrates the Chinese-English bilingual skills of Victorian tertiary students.

Meanwhile, select delegates were interviewing for the HNA Group 2017 Global Talent Management Program. The group found no shortage of Asia-literate delegates interested in its program - many delegates reported that the interview was a great extra opportunity presented by ACELS.

ACELS culminated with the anticipated presentation by the Hon. Bob Carr, now director of the Australia-China Relations Institute at the University of Technology Sydney. Bob presented delegates with a fantastically rich summary of the Australia-China relationship.

Over two and a half days, ACELS squeezed in a great variety of events and workshops, with unprecedented chances to hear and be heard by foremost Australia China leaders. As delegates bid each other farewell on Sunday afternoon, the overwhelming feeling was that ACELS had given delegates tools and connections to start out on their own path of becoming Australia-China leaders.

<a href="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-10.jpg" rel="attachment wp-att-24202"><img class="aligncenter wp-image-24202" src="http://www.acya.org.au/wp-content/uploads/2016/08/ACELS-Blog-Photo-10-1024x578.jpg" alt="(ACELS Blog) Photo 10" width="500" height="282" /></a>

*This blog post was written by Will Breedon, Greater China Correspondent at [Asia Options](http://www.asiaoptions.org/).*