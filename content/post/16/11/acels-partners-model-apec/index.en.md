{
  "title": "ACELS Partners with Model APEC",
  "author": "ACYA",
  "date": "2016-11-20T00:16:33+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "beijing",
      "type": "image",
      "src": "beijing.jpg"
    }
  ]
}
ACYA is pleased to announce that the <a href="http://www.acya.org.au/acels/acels-beijing-2017/" target="_blank" rel="noopener noreferrer">2017 Australia-China Emerging Leaders Summit</a> will be hosted in association with Model APEC, a Beijing-based organisation which aims to promote young leaders' involvement with <a href="http://www.apec.org/" target="_blank" rel="noopener noreferrer">APEC (Asia-Pacific Economic Cooperation)</a>. Model APEC has a wealth of experience in hosting high-quality conferences and ACYA is very excited to work together to provide our best summit yet. A high-quality delegation of Model APEC members attended ACELS Sydney 2016, and we are looking forward to building this relationship going forward.

<!--more-->
<h2>ACELS Applications Closing Soon</h2>
ACELS Beijing applications close in just five days! Applications are open to all members and friends of ACYA, and we look forward to reading yours today. <a href="http://www.acya.org.au/acels/acels-beijing-2017/">Click here</a> to view the application form.
<h2>Who are Model APEC?</h2>
The Model APEC is designed to engage young leaders in APEC. It provides a platform to unite, develop and enrich the skills of next generation of the Asia-Pacific, and get them to respond to closer regional cooperation of the Asia-Pacific.

A five-day activity was held in August 2016 in Beijing, China. It invited students from APEC member economies to step into the shoes of APEC officials/ministers and discuss relative issues from their knowledge of field of study. A youth initiative paper representing the view of young people was produced by participants and handed to the <a href="http://www.apec.org/groups/som-steering-committee-on-economic-and-technical-cooperation/working-groups/human-resources-development.aspx" target="_blank" rel="noopener noreferrer">APEC Human Resources Development Working Group</a> and even to ministerial meetings. The conference provides young people an opportunity to hear from internationally recognised speakers, and interact with senior representatives from business, academic and government over the key elements that shape the future development of the Asia-Pacific.
<h2>What is ACELS?</h2>
The Australia-China Emerging Leaders Summit (ACELS) is a signature initiative of ACYA, delivered in an intensive conference focused on fostering professional development, provide networking and up-skilling opportunities and engaging emerging young leaders within the Australia-China bilateral relationship. The 2017 ACELS Beijing conference will be held from 17-19 February 2017. We look forward to seeing you there!