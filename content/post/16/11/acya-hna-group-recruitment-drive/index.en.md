{
  "title": "ACYA x HNA Group Recruitment Drive",
  "author": "ACYA",
  "date": "2016-11-03T12:05:05+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "14753804_689846567836990_3888265826678471147_o",
      "type": "image",
      "src": "14753804_689846567836990_3888265826678471147_o.jpg"
    },
    {
      "title": "capitalairlinesqr",
      "type": "image",
      "src": "CapitalAirlinesQR.png"
    }
  ]
}
Are you graduating at the end of this year? Looking for a job? HNA Group are coming to Melbourne! HNA Group is a Fortune 500-listed global company, and recently became a major shareholder of Hilton Worldwide Holdings.

Join ACYA with HNA Group to learn about graduate employment opportunities in Beijing, Shanghai and Sanya with subsidiaries Deer Jet and Capital Airlines. HNA are recruiting to-be graduates in Finance, HR, Accounting, Marketing, and more! Read the position descriptions for <a href="http://www.acya.org.au/careers-portal/view/deerjet-graduate-recruitment-program/" target="_blank" rel="noopener noreferrer">Deer Jet</a> and <a href="http://www.acya.org.au/careers-portal/view/capital-airlines-graduate-recruitment-program/" target="_blank" rel="noopener noreferrer">Capital Airlines</a> and register your interest <a href="https://www.facebook.com/events/1304007049634115" target="_blank" rel="noopener noreferrer">here.</a>
<h3><span class="text_exposed_show">Event Schedule (some times and locations TBC)</span></h3>
<table>
<tbody>
<tr>
<th></th>
<th>Date
日期</th>
<th>Time
时间</th>
<th>Location
地点</th>
</tr>
<tr>
<td style="padding: 5px;">Monash Info Session
莫纳什大学咨询会</td>
<td style="padding: 5px;">Thursday 17th November
11月17日 星期四</td>
<td style="padding: 5px;">12pm-2pm</td>
<td style="padding: 5px;">Building 11, CL_20Chn/H5 Lecture Theatre</td>
</tr>
<tr>
<td style="padding: 5px;">Monash Interviews
莫纳什大学面试</td>
<td style="padding: 5px;">Thursday 17th November
11月17日 星期四</td>
<td style="padding: 5px;">2pm-5pm</td>
<td style="padding: 5px;">Building 11, CL_20Chn/S107</td>
</tr>
<tr>
<td style="padding: 5px;">Monash Interviews
莫纳什大学面试</td>
<td style="padding: 5px;">Friday 18th November
11月18日 星期五</td>
<td style="padding: 5px;">12pm-4pm</td>
<td style="padding: 5px;">Building 11, CL_20Chn/S106 &amp; S107</td>
</tr>
<tr>
<td style="padding: 5px;">Unimelb Info Session
墨尔本大学咨询会</td>
<td style="padding: 5px;">Monday 21st November
11月21日 星期一</td>
<td style="padding: 5px;">4pm-6pm</td>
<td style="padding: 5px;">Gryphon Gallery, 1888 Building</td>
</tr>
<tr>
<td style="padding: 5px;">Unimelb Interviews
墨尔本大学面试</td>
<td style="padding: 5px;">Tuesday 22nd November
11月22日 星期二</td>
<td style="padding: 5px;"></td>
<td style="padding: 5px;"></td>
</tr>
</tbody>
</table>
<h3><span class="text_exposed_show">Who are HNA?
</span></h3>
<span class="text_exposed_show">HNA Group is a conglomerate which has grown and prospered against the backdrop of China’s reform and opening up. It had its debut flight on May 2, 1993 which signified the beginning of new chapter in Chinese aviation. In just a little over 20 years, it has become astounding success in the business community, successfully transforming itself from a traditional aviation company to a conglomerate, from the humble beginnings of Hainan Island to the a global company. It is committed to creating a business culture fit for the future, featuring socialist values and modern corporate systems, distinct operation and management models. The ultimate goal for HNA is to integrate itself with the interests of society and individuals within.</span>

Today, HNA Group is ranked #353 on the Fortune 500 list and is worth approximately US$100bn, with major stakes in aviation, hotel, logistics, finance, real estate and more. Learn more at <a href="http://www.hnagroup.com/en/index.html" target="_blank" rel="nofollow nofollow noopener noreferrer">http://www.hnagroup.com/<wbr />en/index.html</a>