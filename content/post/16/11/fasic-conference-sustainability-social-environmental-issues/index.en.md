{
  "title": "4th FASIC Conference - Sustainability: Social and Environmental Issues",
  "author": "ACYA",
  "date": "2016-11-01T22:02:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "fasic",
      "type": "image",
      "src": "FASIC.jpg"
    },
    {
      "title": "fasic",
      "type": "image",
      "src": "FASIC-1.jpg"
    }
  ]
}
<h3></h3>
Opportunity for ACYA Members

In partnership with the <a href="http://www.fasic.org.au/" target="_blank" rel="noopener noreferrer">Foundation for Australian Studies in China</a> (FASIC) and the <a href="http://china.embassy.gov.au/" target="_blank" rel="noopener noreferrer">Australian Embassy Beijing</a>, ACYA is proud to present a unique opportunity for its members to attend the upcoming 4th annual FASIC Conference to be held at Sun Yat-sen University in Guangzhou from 16-18 November. The theme of the conference is Sustainability: Social and Environmental Issues and will feature presentations from a broad range of distinguished academics coming from Australia and China, as well as cultural events.

<!--more-->

The FASIC invites all ACYA members, especially those based in our Guangzhou and Hong Kong chapters to attend the conference, which is free of charge. The program from the conference can be found <a href="http://www.fasic.org.au/images/fasic%204%20draft%20program.pdf" target="_blank" rel="noopener noreferrer" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.fasic.org.au/images/fasic%25204%2520draft%2520program.pdf&amp;source=gmail&amp;ust=1478123142837000&amp;usg=AFQjCNEdKbThrcj1DWTkQ7kWHv1He012PA">here</a>. The conference will provide ACYA members with the ability to extend their real knowledge of academia in the Australia-China space, as well as network with distinguished guests, renowned academics and local Chinese students.

ACYA is excited to announce four sponsored places at the conference, supported by the FASIC and the Australian Embassy Beijing, which will cover two ACYA members and two New Colombo Plan scholars (from China or Hong Kong) to attend. This will cover the the full duration of the conference, as well as their domestic travel costs to the conference from Mainland China or Hong Kong.
<h2>About FASIC</h2>
FASIC is an independent non-profit foundation in Australia established to support Australian Studies Centres located across China and to provide funding and other support to the BHP Billiton Chair of Australian Studies located at Peking University in Beijing, People's Republic of China.

ACYA already has a long history of working closely with the Peking University Australian Studies Centre (PKUASC) to give our members the opportunity to conduct <a href="http://www.acya.org.au/careers/pkuasc-internship-program/" target="_blank" rel="noopener noreferrer">academic internships</a> at the prestigious university working closely with the Chair of Australian Studies.
<h2><span style="color: #000000;">About the New Colombo Plan</span></h2>
<div>

<span style="color: #000000;">The <a href="http://dfat.gov.au/people-to-people/new-colombo-plan/pages/new-colombo-plan.aspx" target="_blank" rel="noopener noreferrer">New Colombo Plan</a> is a signature initiative of the Australian Government which aims to lift knowledge of the Indo-Pacific in Australia by supporting Australian undergraduates to study and undertake internships in the region.</span>

<span style="color: #000000;">It encourages a two-way flow of students between Australia and the rest of our region, complementing the thousands of students from the region coming to Australia to study each year.</span>

<span style="color: #000000;">The New Colombo Plan is intended to be transformational, deepening Australia's relationships in the region, both at the individual level and through expanding university, business and other links.</span>

</div>
<h2>Apply Now</h2>
In order to apply for the opportunity, applicants are invited to send their CV and a 300 word comment on the theme of the conference (Sustainability: Social and Environmental Issues) in relation to the Australia-China youth space. Applicants should also note the location within Hong Kong or China from which they would require transport, as well as whether they are a New Colombo Plan scholar. Please note that the two successful New Colombo Plan applicants will have their essays published on the Australian Embassy in China's website, and the two successful ACYA applicants will have their essays published on the ACYA website.

Applications are to be sent to National Education Director, Chloe Dempsey, at <a href="mailto:education@acya.org.au" target="_blank" rel="noopener noreferrer">education@acya.org.au</a> before midnight on 10 November. Applicants will receive notification of the outcome on 11 November. ACYA members interested in attending the conference may also email <a href="mailto:education@acya.org.au" target="_blank" rel="noopener noreferrer">education@acya.org.au</a> to receive a registration form. Attendance is free of charge.