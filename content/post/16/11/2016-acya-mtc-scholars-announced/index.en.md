{
  "title": "ACYA Members Headed for the Mandarin Training Centre",
  "author": "ACYA",
  "date": "2016-11-08T10:56:07+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Congratulations to <strong>Nathalia Hardie</strong>, <strong>Alexander Joske</strong> and <strong>Sue Ye</strong>, the three winners of this year's ACYA-MTC scholarship. The scholarship provides students with the chance to study at the National Taiwan Normal University’s Mandarin Training Centre (NTNU MTC) during their Winter Term (Dec 1, 2016 – Feb 26, 2017).

<!--more-->

<strong>Sue</strong> is currently in her fifth year of a dual Law and Arts degree at the University of Queensland, majoring in Chinese and English translation and interpreting, and international relations.

<em>“Growing up as an Australian-born Chinese, I have never had the opportunity to fully immerse myself in the language or culture, and I believe that the program will provide me with the opportunity to do so, and also further improve my Chinese,”</em> she said of the chance to study in Taiwan.

The scholarship is a fantastic opportunity for ACYA members to continue their Chinese language studies in one of Taiwan’s most prestigious educational institutions. The MTC has drawn participants from around the world, including former Australian Prime Minister Kevin Rudd. The Australian Office in Taiwan commended the scholarship program, noting the strong links between the people of Taiwan and Australia across economic, cultural and social spheres and pointing out that, every year, over 25,000 young Taiwanese participate in the working holiday maker program in Australia. They commended the scholarship program as a fantastic opportunity to develop greater understanding amongst young people of each other’s languages and cultures.

<strong>Nathalia</strong> recently graduated with a Bachelor of Commerce (International) majoring in Economics, Finance and Chinese at the University of New South Wales. <em>“I have always had an interest in international studies,”</em> she says, <em>“particularly Australian-Chinese relations, which is why I majored in Chinese studies and am an active member in ACYA UNSW. I am particularly interested in Taiwan due to its complicated history, political structure and use of traditional Chinese. MTC is a world-renowned Chinese language centre and is an obvious choice for those who desire to learn Chinese.”</em>

In his childhood <strong>Alexander</strong> lived in Beijing for over six years, gaining fluency in reading, writing, and speaking Mandarin Chinese. He is currently completing his fourth semester of studying Classical Chinese as part of his Arts/Economics degree at the ANU.

<em>“As China grows in its influence and important on the world stage, I believe it is essential that Australians gain a more nuanced understanding of China. Currently, very few China correspondents are able to conduct interviews in Chinese without the aid of a translator. Yet if a Chinese reporter based in Australia needed a translator to conduct interviews in English, few would take that reporter seriously.</em>

<em>“I am hoping that my time at the MTC will compliment my interest in China journalism, giving me an advanced level of Mandarin to help me research news items and conduct interviews in the language.”</em>

In the spirit of ACYA, studying in Taiwan will allow students unique insight into the cultural ethos of a vibrant and diverse community, as well as the opportunity to forge cross-cultural connections and contribute to the Australia-Sino relationship. MTC’s Director, Prof. Yung-Cheng Shen, comments that <em>“It is important for our centre to recruit students and build relationships from Australia. We value our collaboration with ACYA. Today’s announcement aligns with my commitment to encourage Australian students to come to Taiwan to immerse themselves in Chinese language and Taiwanese culture.” </em>

We wish <strong>Nathalia</strong>, <strong>Alexander</strong> and <strong>Sue</strong> all the best in their studies!
<h2>About the MTC</h2>
The Mandarin Training Center (MTC), established in 1956, is affiliated with National Taiwan Normal University (NTNU) and is Taiwan’s oldest and best-known Chinese language institute. In cooperation with the Graduate Institute of Teaching Chinese as a Foreign/Second Language, the Graduate Institute of International Sinology Studies, the Department of Applied Chinese Languages and Literature, and the Department of Chinese Language and Culture for International Students at NTNU, the MTC is recognised as one of the world’s leading research and teaching institutions. For more information, download the <a href="http://www.acya.org.au/wp-content/uploads/2014/07/acya-mtc-info.pdf" target="_blank" rel="noopener noreferrer">MTC brochure</a>, visit <a href="http://www.mtc.ntnu.edu.tw" target="_blank" rel="noopener noreferrer">http://www.mtc.ntnu.edu.tw</a> or check out their <a href="https://www.facebook.com/mtc.ntnu" target="_blank" rel="noopener noreferrer">Facebook page</a>.

— By Declan Fry.