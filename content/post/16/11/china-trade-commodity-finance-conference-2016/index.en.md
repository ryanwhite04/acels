{
  "title": "China Trade & Commodity Finance Conference 2016",
  "author": "ACYA",
  "date": "2016-11-02T11:00:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "CTCFC16",
      "type": "image",
      "src": "CTCFC.png"
    },
    {
      "title": "shanghai_google_image-619x413",
      "type": "image",
      "src": "Shanghai_Google_image-619x413.jpg"
    }
  ]
}
Shanghai is the host city for GTR’s <a href="http://www.gtreview.com/events/asia/china-trade-commodity-finance-conference-2016/?utm_source=Pre%20PR&amp;utm_campaign=China%20Trade%20%26%20Commodity%20Finance%20Conference%202016&amp;utm_medium=Link" target="_blank" rel="noopener noreferrer">China Trade &amp; Commodity Finance Conference 2016</a>, once again providing a key meeting point for business leaders and experts from the Chinese trade finance community.

<!--more-->

Exporters, importers, producers, financiers and service providers will all be in attendance, ready to engage in critical discussions focused on China’s trade landscape.

Various networking breaks will provide unrivalled opportunities for delegates to establish new business contacts and become reacquainted with familiar faces, all with the prospect of developing growth in China and the wider region.
<h2>Opportunity for ACYA Members</h2>
ACYA has partnered with <a href="http://www.gtreview.com/" target="_blank" rel="noopener noreferrer">GTR</a> to provide discounted entry for ACYA members and friends interested in attending the conference. Simply register with the promotional code ACYA15 for a 15% discount! <a href="http://www.gtreview.com/events/asia/china-trade-commodity-finance-conference-2016/?utm_source=Pre%20PR&amp;utm_campaign=China%20Trade%20%26%20Commodity%20Finance%20Conference%202016&amp;utm_medium=Link" target="_blank" rel="noopener noreferrer">Click here</a> to register today.
<h2>What Others Have Said About the China Trade &amp; Commodity Finance Conference</h2>
<blockquote>“If you want to expand your business in China with regard to trade finance, GTR is a good platform to connect with stakeholders.”</blockquote>
<em>E Lee, Tokio Marine Kiln</em>

<hr />

<blockquote>“It was extremely well organised; interesting speakers; great variety of delegates; excellent networking.”</blockquote>
<em>J Zhou, Bomin Bunker Oil</em>

<hr />

<blockquote>“I was pleased to see such a breadth of topics and it was a fantastic opportunity to network with others. It was extremely beneficial to be on the ground and get a real feel for China. We hope to attend again next year.”</blockquote>
<em>L Taylor, Trade Advisory Network</em>

<hr />

<blockquote>“I found the GTR conference a great way to stay abreast of trends, as well as have specific deal-focused meetings on the sidelines. The calibre of the sponsors and panelists is testament to GTR conferences, with the recent China conference no exception.”</blockquote>
<em>J Reeve, AgRee Commodities</em>
<h3>Additional information</h3>
<ul>
 	<li>Click <a href="http://www.gtreview.com/wp-content/uploads/2016/07/Shanghai-2015.pdf" target="_blank" rel="noopener noreferrer">here</a> to see a list of attending companies from 2015’s event.</li>
 	<li>Join the discussion on social media with the official event hashtag <a href="https://twitter.com/search?f=tweets&amp;vertical=default&amp;q=%23gtrCHI&amp;src=typd" target="_blank" rel="noopener noreferrer">#GTRCHI</a>.</li>
 	<li>Check out the 2015 event page <a href="http://www.gtreview.com/events/asia/china-trade-export-finance-conference-2015/" target="_blank" rel="noopener noreferrer">here</a> for details on last year’s successful event.</li>
</ul>