{
  "title": "ACYA Port Adelaide Partnership Announcement",
  "author": "ACYA",
  "date": "2016-07-18T12:26:52+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_2021",
      "type": "image",
      "src": "IMG_2021.jpg"
    },
    {
      "title": "image (2)",
      "type": "image",
      "src": "image-2.png"
    }
  ]
}
ACYA looks forward to partnering with Port Adelaide on a number of initiatives in Australia. ACYA is delighted to have already hosted a delegation of Chinese students for lunch on their recent visit to Melbourne in conjunction with Port Adelaide. This partnership also provides ACYA members with a fantastic opportunity to learn more about football, engaging members in activities across both Australia and in China!

David Douglas
President

<!--more-->

<img class="aligncenter size-full wp-image-24148" src="http://www.acya.org.au/wp-content/uploads/2016/07/image-2.png" alt="image (2)" width="580" height="400" />

ACYA has an awesome new association with Port Adelaide football club. Port Adelaide is proving to be a leader in the Australia-China space, engaging Chinese audiences across both countries.

AFL is an iconic Australian sport with a history of over 100 years. Port Adelaide will grow the game in China, sharing it with Chinese audiences by:
<ul>
 	<li>holding annual Auskick training camps;</li>
 	<li>continuing to sponsor AFL teams in China;</li>
 	<li>broadcasting games into China via CCTV; and</li>
 	<li>hopefully holding a premiership-season game in Shanghai next year.</li>
</ul>
ACYA’s P2P team is excited to work with Port Adelaide to encourage this fantastic cultural exchange. ACYA members will have the opportunity to attend Port Adelaide games around Australia and next year in China – Go Power!