{
  "title": "ACELS Sydney 2016 about to kick off ",
  "author": "ACYA",
  "date": "2016-07-28T13:41:11+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS 2015 Sydney",
      "type": "image",
      "src": "ACELS-2015-Sydney.jpg"
    }
  ]
}
With only one day left to the start of the 2016 Australia-China Emerging Leaders' Summit (ACELS) in Sydney, excitement is definitely building up. Now in its fifth year, ACELS is one of ACYA's premier initiatives, intended to develop the professional skills and capabilities of key players within the ACYA network and the wider Australia-China relationship.  

This year the Summit will feature notable speakers from different industries and organisations, giving delegates a broad understanding of the highly varied and diverse nature of the Australia-China space. Speakers include Peter Cai, Lowy Institute for International Policy; Matthew Benjamin, AsiaRecon; Jeffrey Riegel, University of Sydney China Studies Centre; Dai Le, DAWN; James Hudson, Alibaba Group (ANZ) and The Hon. Bob Carr, Australia-China Research Institute. 

"ACELS is a fine example of ACYA's mission that corresponds to our three pillars: Careers, Education and People-to-People. One of the most rewarding parts of coming to ACELS is the long lasting friends you make with like-minded young people." - Jerry Ding, former ACYA Treasurer and ACELS delegate.