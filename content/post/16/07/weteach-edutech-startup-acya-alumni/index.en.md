{
  "title": "WeTeach: Edutech Startup by ACYA Alumnus",
  "author": "ACYA",
  "date": "2016-07-05T08:48:04+08:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Australia-China Perspectives",
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "13221034_1699870460264457_6694131655385776367_n",
      "type": "image",
      "src": "13221034_1699870460264457_6694131655385776367_n.jpg"
    },
    {
      "title": "13015100_1686994674885369_5637952098567956404_n",
      "type": "image",
      "src": "13015100_1686994674885369_5637952098567956404_n.jpg"
    },
    {
      "title": "13015100_1686994674885369_5637952098567956404_n",
      "type": "image",
      "src": "13015100_1686994674885369_5637952098567956404_n-1.jpg"
    },
    {
      "title": "13000296_1685414471710056_1136540868234704103_n",
      "type": "image",
      "src": "13000296_1685414471710056_1136540868234704103_n.jpg"
    },
    {
      "title": "13221034_1699870460264457_6694131655385776367_n",
      "type": "image",
      "src": "13221034_1699870460264457_6694131655385776367_n-1.jpg"
    }
  ]
}
ACYA recently spoke to ACYA alumnus Tom Williams, cofounder of WeTeach and currently based in Shanghai. <a href="http://www.weteach.info">WeTeach</a> is an exciting edutech startup connecting and fostering relationships between Chinese students learning English and teachers, mainly retirees, from around the world, disrupting the transactional nature of online education.

<!--more-->

<strong style="line-height: 1.5;">What or who sparked your original interest in China?</strong>

I started learning Chinese at university by accident. Whilst running the gauntlet of UWA’s student sign up process on Orientation Day I can across a board that listed all the “At Risk Species” of student. Well, it wasn’t called that, but that’s how I have painted it in my memory. The list contained the names of anyone who came from a high school with only a handful of graduates at UWA. Just as I happened to find my name I bumped into a primary school friend I hadn’t seen in half a decade. He’d just found his name too. It was a fortuitous coincidence. I asked him what he was studying, he said Chinese. He asked me, German. We both needed an extra subject. I took Chinese. He took German. I remember after the first day of class, I walked around the garden at home wildly conducting out the tones of nihao to the olive tree. It was weirdly fun and I have never looked back. I think my friend ended up becoming a German teacher at his old high school.

<strong>A main differentiator for WeTeach is that it puts the social element back into education, fostering relationships between teacher and student. How do you see the market for educational technologies evolving over the coming years?</strong>

I remember driving home one evening to find my Taiwanese neighbor had locked his keys in the car. He asked me for help. I took out my phone, opened YouTube, typed “keys” “car” “coat hanger” and quickly flicked through a few videos. Then I grabbed a metal coat hanger I had in the car, bent it into shape, slipped it between the rubber seal and the window and unlocked the car. He and I were both a bit shocked at how easy it was to learn and do this.

If you have the time, patience, and motivation, you can find out how to do most things on the Internet, and, thanks to the last few decades of mass pirating, most providers allow access to most content for free. The three key categories in which I see us still being willing to part with money are: personal skills training, accreditation, and value added services that save us time or make us feel that we learnt from “an authority” on a subject. Most technology that I have come across focuses on making this experience more immersive, deliverable en masse, and personalized to each student’s learning style or “gameified”. I believe educational technologies will seek to optimize the user experience in these categories over the coming years. The opportunity that I find most interesting is how to make online education highly social. At the moment most online education, especially in China, is just an online delivery of the old offline experience. It is also overly transactional and takes most of the joy out of learning. It also is a poor long-term value proposition – an accreditation collection approach to life blunts a person.

WeTeach is committed to being a social platform. We want our teachers, students, and parents to become friends, share in each other’s lives, and learn from each other’s experiences. We want students and teachers to feel that the joy has been put back into learning. Students learn through sharing their experience of a story with a teacher. Teachers can focus on what they love most: the teaching – no admin; no politics. Parents are involved at all times and share in a community of people wanting to exchange ideas about how to best educate their children. So far this approach has proved popular with all involved.

<strong>What has been your experience with the startup ecosystem in Shanghai?</strong>

My experience with the start-up ecosystem has been limited. Due to the importance of distribution networks to our platform’s success we had to find a large partner to work with to help us contact students directly. This is not something most incubators, hubs, or related business service providers can deliver. Through an ACYA event I met a Chinese friend who introduced us to our current investor who has the distribution network we needed. Perhaps ACYA is the best ecosystem to recommend to others? :-)

<strong>Austrade has recently announced a “Landing Pad” will be opened in Shanghai this year to help Australian entrepreneurs. From your experience, what are the biggest challenges Australian entrepreneurs encounter trying to bring their product to the Chinese market?</strong>

All the basic problems that come with most cross-border transactions: language; knowing who matters; understanding the unfamiliar bureaucratic maze; building trust and protecting your interests with local partners; etc. That said, I think one challenge of particular importance for Australian entrepreneurs is remembering that it is better to monopolise a small market than be a competitor in a large market. (For better insights than I could ever hope to provide please see Peter Thiel’s book <em>Zero to One</em>). Many people I hear speak about taking a business to China seem to view its relative enormity as a guarantor of success. However, 1/6<sup>th</sup> of the world’s population aren’t just potential customers they are also potential competitors. They understand the market better than an Australian ever could, have better connections, and can copy a competitor out of existence. Any Australian entrepreneur going to China needs to do something only s/he can do that is orders of magnitude better than what a competitor can achieve, and service a very specific segment of customers that no one else can service to the same degree. Clearly defining these aspects is a good starting place to answer the other questions of technological advance, suitability of timing, quality of team, effectiveness of distribution, and durability of the business.

In specific relation to the “Landing Pad”, I believe its true success will lie with whoever is selected to run it. The position requires excellent language skills, an understanding of what is actually innovation and a likely successful business and not just hype, how to manage government relations at the municipal, national, and bilateral levels, the ability to raise and effectively apply funds, and a true believer in the project who can ev­­angelise like it’s the end of days – not a recruiting job of which I’m envious to say the least!

<em style="line-height: 1.5;">This interview was conducted by Siobhan O’Sullivan.</em>
<p style="text-align: left;"></p>