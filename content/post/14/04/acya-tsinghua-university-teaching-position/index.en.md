{
  "title": "ACYA-Tsinghua University Teaching Position",
  "author": "ACYA",
  "date": "2014-04-14T08:51:47+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACYA-TSINGHUA logos(1)",
      "type": "image",
      "src": "ACYA-TSINGHUA-logos1.jpg"
    }
  ]
}
<p align="center"> <a href="http://www.acya.org.au/wp-content/uploads/2014/04/ACYA-TSINGHUA-logos1.jpg"><img class="aligncenter size-medium wp-image-2447" alt="ACYA-TSINGHUA logos(1)" src="http://www.acya.org.au/wp-content/uploads/2014/04/ACYA-TSINGHUA-logos1-300x173.jpg" width="300" height="173" /></a></p>
ACYA and Tsinghua University invite applications for a fixed 1-YEAR teaching contract at The Department of Foreign Languages and Literatures at Tsinghua University!

Tsinghua University are seeking a <b>native</b> English speaker holding a <b>Masters degree</b> (or equivalent) to fill this role and add to their large team of international teachers. The role involves teaching English at both undergraduate and graduate levels to some of the brightest students in China. Content will include basic listening, speaking, writing and reading including reading a newspaper, discussing current affairs and literature.

Ideally, you will bring a personal and uniquely Australian touch to your teaching, educating students on Australian society and culture (in addition to British and American). Teaching requirements and loads will vary depending on the department’s needs and teacher requests. Teachers will also get the chance to present lectures on topics of interest, to be agreed upon by the teacher and the Department.

As this is a full-time teaching role, successful applicants are expected to meet basic teaching duties, such as preparing lessons and exams, correcting assignments and consulting with students when necessary. Outside of the classroom you will have the opportunity to judge various English-based contests and to live in one of the most amazing cities in the world.

With <b>accommodation and international</b> <b>flights offered</b> by Tsinghua on top of a salary between <b>80,000 – 100,000 RMB</b>, this is a lucrative opportunity that will be highly sought after.

ACYA sees this as a fantastic opportunity for an enthusiastic teacher to enhance their profile and represent ACYA and Australia on the international stage.

Interested applicants should send their applications, including a cover letter, CV, and recent photo, to <a href="mailto:education@acya.org.au">education@acya.org.au</a> before 30 April, 2014.

Don’t miss out on this incredible career defining and life changing opportunity - apply now!

&nbsp;

&nbsp;
<p align="center"><b>Tsinghua University Fixed Term English Teaching Position
</b><b style="line-height: 1.5em;">September 1<sup>st</sup> 2014 –June 30<sup>th</sup> 2015
</b><b style="line-height: 1.5em;">Beijing, China</b></p>
<p align="center"><i>Please note that the deadline for application is:
</i><i> April 30<sup>th</sup>, 2014.</i></p>
<b>About:</b>

Tsinghua University is one of China’s most prestigious universities. Established in 1911, Tsinghua is one of China’s leading internationally focussed tertiary education institutions. The Department of Foreign Languages and Literatures is responsible for teaching English to exceptional students at both undergraduate and graduate levels.

For more information see: <a href="http://www.tsinghua.edu.cn/publish/then/index.html">http://www.tsinghua.edu.cn/publish/then/index.html</a>

<b>Position Details: </b>

Duration: 1 Year

Available from the fall semester (i.e. 1<sup>st</sup> September) and will be offered on a fixed-term contract for a period of one academic year (ending on 30<sup>th</sup> June).

The minimum teaching load is 16 hours per week, or 16 credit hours.

<b>Salary:</b>

Within the range of 80,000 – 100,000 RMB per annum, depending on the teaching load (key factor), qualifications and experience.

<b>Hospitality:</b>

Accommodation and international airfares (after one year contract only) are offered. Therefore the whole package is worth 140,000 – 160,000 RMB.

<b>Eligibility:</b>
<ul>
	<li><b>NATIVE</b> English speaker;</li>
	<li>Holding at least a <b>Masters degree </b>or equivalent in a relevant subject area;</li>
	<li>Teaching experience at the university level and a publication record are preferable; and</li>
	<li>Candidates should be committed to high quality teaching and willing to assist students.</li>
</ul>
<b>To Apply:</b>

The application process is a 2-step process

1. Please send a cover letter outlining how you meet the above criteria and how you would contribute to the program, along with an up to date resume and a recent photo to <a href="mailto:education@acya.org.au">education@acya.org.au</a>.
<p align="center"><i>ACYA will process applications after the closing date, and create a shortlist, </i></p>
<p align="center"><i>which will be sent to Tsinghua University</i></p>
<p style="text-align: left;" align="center">2. Tsinghua will contact applicants deemed suitable to arrange a Skype interview.</p>
&nbsp;

<b>For specific questions regarding the program</b>, please contact:

Dr. Zhongshe Lu, Deputy Director of the Department of Foreign Languages and Literatures
Email: lvzhs@mail.tsinghua.edu.cn
<span style="line-height: 1.5em;">Address: Room 211, Wennanlou, Tsinghua University, Beijing, 100084
</span><span style="line-height: 1.5em;">Tel: +86-10-62784969; Fax: 86-10-62784969; 86-10-62795726</span>

OR

Michael Twycross, ACYA National Education Director
Email: <a href="mailto:education@acya.org.au">education@acya.org.au
</a>Tel: +61 (0) 404 501 393