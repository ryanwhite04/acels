{
  "title": "ACYA Launches Brand New Facebook Page",
  "author": "ACYA",
  "date": "2014-04-22T14:48:15+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Facebook Pic2_UQ ball_v4",
      "type": "image",
      "src": "Facebook-Pic2_UQ-ball_v4.jpg"
    },
    {
      "title": "media",
      "type": "image",
      "src": "media.jpg"
    }
  ]
}
<p style="text-align: center;"><a href="http://www.acya.org.au/wp-content/uploads/2014/04/Facebook-Pic2_UQ-ball_v4.jpg"><img class="aligncenter wp-image-2534" src="http://www.acya.org.au/wp-content/uploads/2014/04/Facebook-Pic2_UQ-ball_v4.jpg" alt="Facebook Pic2_UQ ball_v4" width="511" height="189" /></a></p>
<a href="https://www.facebook.com/australiachinayouthassociation" target="_blank">ACYA National has a new Facebook page! (Please click here)</a>

ACYA’s New Facebook page is designed to act as ACYA's radio station, broadcasting <a href="https://www.facebook.com/hashtag/auschina?source=feed_text&amp;story_id=10152130766893978">‪#‎auschina</a> <a href="https://www.facebook.com/hashtag/acya?source=feed_text&amp;story_id=10152130766893978">‪#‎acya</a> Careers, Education and Publications. Like us and join in the #auschina youth conversation.

Students and professionals will find the latest business partnerships, education opportunities and internship placements to be held throughout China and Australia on the page.

The ACYA Facebook page not only provides fantastic insight into the latest happenings in the Australia-China space but it is also a platform for vibrant dialogue between anyone who is interested in the dynamic China-Australia relationship.

ACYA publications will be available through the Facebook page as well as updates from the latest ACYA activities and opportunities to participate in the many exciting upcoming events.

Photos, opportunities, news and current affairs will be shared to help foster the cross-cultural and transnational relationship that is flourishing between the young Australian and Chinese communities.
<p style="text-align: center;"></p>