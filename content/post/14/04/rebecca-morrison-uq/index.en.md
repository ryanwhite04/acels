{
  "title": "Rebecca Morrison (UQ)",
  "author": "ACYA",
  "date": "2014-04-23T05:56:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": []
}
This is my second semester as the Vice-President for People-To-People on the ACYA executive team at UQ. Out of any of the other clubs available on campus, I can honestly say that ACYA beats them hands down. The events our association organises not only give off an extremely warm and relaxed vibe but also have a deeper meaning than just a whole lot of fun. Our events are more meaningful because we all share the common goal of bridging the cultural gap between Australia and China. To me this means that every time I have organised a social dinner, beach trip, conversation class etc., I am not only ensuring I will have a fun time- because everyone involved in ACYA is awesome (!)- but also that I am forever gaining new insights into China’s complex culture and language.

It is hard to pin down my favourite memory from my involvement in ACYA.  One of them would definitely have to be the first Australia-China Exchange space that I was in charge of running, where to our great surprise we had over forty people turn up to a room which only seated fifteen. Our conversation classes’ success and popularity has continued over into 2014, where just last week we played a hilarious game of musical chairs to teach people the similar yet different pronunciation of the words睡觉(sleep) and 水饺 (dumpling)。Another special memory of one of my first encounters with ACYA, was the ‘2013 Amazing Race’ where myself and two other team mates sprinted around Brisbane completing challenges in first place only to be beaten right at the end! All in all, ACYA’s collection of cultural, educational and employment opportunities opens a door to anyone who is interested in pursuing an internationally minded career or even just making friends with a lovely bunch of people. I can’t wait to see how the rest of 2014 at ACYA UQ pans out. So long as I win this year’s amazing race, it’s going to be a good year! :P J

Rebecca Morrison