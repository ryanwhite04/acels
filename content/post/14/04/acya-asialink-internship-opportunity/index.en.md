{
  "title": "<!--:en-->ACYA-Asialink Internship Opportunity<!--:-->",
  "author": "ACYA",
  "date": "2014-04-02T06:39:57+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "asialink",
      "type": "image",
      "src": "asialink.jpg"
    }
  ]
}
<!--:en--><div title="Page 1">
<div>
<div>
<h1><a href="http://www.acya.org.au/wp-content/uploads/2014/04/Screen-Shot-2014-04-01-at-12.26.05-pm.png"><img class="aligncenter size-medium wp-image-2347" alt="Screen Shot 2014-04-01 at 12.26.05 pm" src="http://www.acya.org.au/wp-content/uploads/2014/04/Screen-Shot-2014-04-01-at-12.26.05-pm-300x106.png" width="300" height="106" /></a></h1>
<h1>ASIALINK (SYDNEY) INTERNSHIP PROGRAM</h1>
Asialink recruits for its Sydney Internship program two to three times a year. Based at the Sydney office, internship positions are unpaid and require one (maximum two) day(s) per week over 12 weeks. Applications open approximately 6-8 weeks prior to the start date and an exit interview will be held at the close of the internship to obtain feedback that ensures the program continues to provide interns with valuable learning experience. Asialink is not able to provide interns with:
<ul>
	<li>Monetary allowances or remuneration</li>
	<li>Accommodation or accommodation allowances</li>
	<li>Travel allowance</li>
</ul>
<h1>ABOUT ASIALINK</h1>
Asialink is Australia's leading centre for the promotion of public understanding of the countries of Asia and of Australia's role in the region. It is a key provider of information, training and professional networks. Asialink works with business, government, philanthropic and cultural partners to initiate and strengthen Australia-Asia engagement.

</div>
</div>
</div>
<a href="http://www.acya.org.au/wp-content/uploads/2014/04/ACYA-Asialink-Internship-PD.pdf" target="_blank">Click here to download as a PDF.</a><!--:-->