{
  "title": "Lowy Institute Intern Opportunity",
  "author": "ACYA",
  "date": "2014-04-24T13:10:46+00:00",
  "image": "",
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}

<p>
The East Asia Program at the Lowy Institute for International Affairs seeks applications for internships.
The successful candidate(s) must be able to read Mandarin with ease.
One of the main tasks of the intern(s) will be to search for, read, and summarise (into English) Chinese-language sources from the internet and journals.
Other tasks will include collecting Chinese data and assisting the Program Director in her work.
A keen interest in international relations (and China’s foreign and security policy in particular) is an asset.
The Program is conducting research on, among others, China's maritime security, China-Japan relations and China-Korea relations.
</p><!--more-->
<p>
Ideally, the Program would like to have a full-time intern on board beginning 4 June 2012, alternatively 27 August.
Both the starting date and working times are negotiable.
The intern must be based in Sydney and be at a minimum prepared to work 3 days per week.
</p>
<p>Please send by email your CV, a writing sample in English, and a brief explanation describing the motivations of being a Lowy intern to:</p>
<p>Ed Kus</p>
<p>Research Associate</p>
<p>East Asia Program</p>
<p>Lowy Institute for International Policy</p>
<p>Email: <a href="mailto:ekus@lowyinstitute.org">ekus@lowyinstitute.org</a></p>
