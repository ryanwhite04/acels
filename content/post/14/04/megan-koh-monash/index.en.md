{
  "title": "Megan Koh (Monash)",
  "author": "ACYA",
  "date": "2014-04-23T05:54:27+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "rebeca",
      "type": "image",
      "src": "rebeca.jpg"
    },
    {
      "title": "megan",
      "type": "image",
      "src": "megan.jpg"
    },
    {
      "title": "jack",
      "type": "image",
      "src": "jack.jpg"
    },
    {
      "title": "crystal",
      "type": "image",
      "src": "crystal.jpg"
    },
    {
      "title": "sarah",
      "type": "image",
      "src": "sarah.jpg"
    }
  ]
}
I feel incredibly honoured to be a tiny part of the ACYA patchwork over the past couple of years. In 2012, I was a low key first-year engineering student, finding my feet and confidence in the world of university. I was not shy by any means, but was definitely unsure of my abilities and voice in the community. One night, I was scrolling through Facebook and a post showed up on my newsfeed asking for interest in joining ACYA Monash. Fast forward to 2014 and I can not possibly imagine how my university life would have played out without ACYA. My confidence has grown alongside the new chapter, and my involvement in ACYA has completely altered my perception of what being a team player and effective leader means. Having this platform to explore new ideas, experiment and grow, individually and as a team, has truly been an opportunity I feel fortunate to have been given. Initiative and tactful communication are two key strengths that I attribute to my journey in the local committee. I am proud to be in ACYA Monash and to work with a brilliant team in P2P. Alongside personal development, ACYA has provided an outstanding cultural, professional and social domain that has really enriched my life as a student. Now I have this network of strong, remarkable individuals and we have really grown as a community here. A final thank you to my ACYA Monash family for all the support and friendship. It’s been a blast!

&nbsp;

Megan Koh