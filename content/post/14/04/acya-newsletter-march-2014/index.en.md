{
  "title": "ACYA Newsletter March 2014",
  "author": "ACYA",
  "date": "2014-04-23T05:31:36+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Hello to all!

We’re pleased to announce the March edition of the ACYA E-Bulletin is now available to be viewed online. Please click the link below!

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=50c9f2c50c">Newsletter March  2014 </a>