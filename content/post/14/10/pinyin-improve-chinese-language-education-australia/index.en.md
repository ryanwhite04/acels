{
  "title": "In with Pinyin: How to Improve Chinese Language Education in Australia",
  "author": "ACYA",
  "date": "2014-10-14T14:53:52+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "portal",
      "type": "image",
      "src": "portal-e1413280892131.jpg"
    }
  ]
}
Already the world’s second-largest economy, there is a growing sense among many analysts that China is only just getting warmed up. Many portentously believe that the Middle Kingdom will dominate this century just as thoroughly as Great Britain did the 1800s. For countries like the United States, this shift in global eminence has led to a profound sense of caution about what China’s growing clout might mean for the future strategic interactions of the two superpowers.

Australia, on the other hand, has been the prime Western beneficiary of China’s inexorable rise. Already our largest trading partner, China looks set to become more important to us than any other nation for much of the foreseeable future. As China’s burgeoning middle class continues to spread its economic might across the world, Australia can look forward to economic benefits that go far beyond the mining boom.

The tremendous opportunities which Australia is afforded by China’s rise, however, sit in stark contrast to our utter lack of preparedness for such a prospect. Over the past ten years or so, policymakers have highlighted a dearth of ‘Asian-literacy’ – that is, of skills which allow people to engage will with our Asian neighbours, such as languages and cultural understanding. This has resulted in a strong push by both state and federal governments to encourage the development of such skills in schools, in the hope of ensuring that the next generation are more Asia-literate than the last.
<div class="page" title="Page 36">
<div class="layoutArea">
<div class="column">

The promotion of Chinese language has been, and rightly should be, a major focus of this push. Few people would dispute the value of learning Mandarin, which, aside from being the world’s most-spoken mother-tongue, is fast becoming the second language of the business world. Learning Chinese also offers a more powerful insight into China’s fascinating and diverse culture, creating opportunities for the learner to interact more richly with the denizens of a 5000 year old civilisation.

Despite the investment of millions of dollars, thousands of hours, and the blood, sweat and tears of hundreds of dedicated teachers Australia-wide, however, Chinese education in this country is in an abjectly abysmal state. A report on the issue by Dr. Jane Orton, Director of the Chinese Teacher Training Centre at the University of Melbourne, found that retention rates among non-native speakers were so low that, ‘by senior secondary school, the teaching and learning of Chinese in Australia is overwhelmingly a matter of Chinese teaching Chinese to Chinese.’ The target audience of all Chinese programs – that is, students who don’t already have Mandarin language skills - are simply dropping out en masse as soon as the language is no longer compulsory to study.

Dr. Orton discusses several reasons for this phenomenon, all of which I can confirm from my own experience in the field. These include:

</div>
</div>
</div>
<div class="page" title="Page 37">
<div class="layoutArea">
<div class="column">
<ul>
 	<li>The relative difficulty of studying Mandarin in comparison to European languages or Indonesian;</li>
 	<li>The absolute difficulty of studying Mandarin due to its tonal basis, idiomatic pronunciation, and the requirement to memorise thousands of non- phonetic characters;</li>
 	<li>The lack of proficiency that even the brightest and most diligent Chinese learner can expect to achieve by the end of senior schooling;</li>
 	<li>The streaming of native and non-native speaking students in the same classroom, creating a situation in which one group cannot hope to compete with the other;</li>
 	<li>The challenges faced by native-speaker teachers in relating well to Australian students in the classroom;</li>
 	<li>The challenges faced by non-native-speaker teachers in accurately modelling authentic Chinese; and</li>
 	<li>The prevalent use of teaching methodologies unsuited to the Australian education system.</li>
</ul>
To summarise, Chinese is difficult and often poorly taught. It presents challenges to the learner that many Chinese programs and teachers in Australia (with some notable exceptions) are ill-equipped to deal with. As a result, Chinese learning in our schools has an attrition rate of more than 94 per cent, with only a fraction of the remaining six percent of students who persevere with Chinese into senior schooling coming from non-Chinese-speaking backgrounds.

</div>
<div class="column">Such a dismal state of affairs requires both a re- examination of the aims of Chinese teaching in Australia, along with a greater willingness to reform a system that is obviously not working. Fortunately, there is one simple step that can be taken which will address many of the key problems in Chinese language teaching in Australia - the removal of Chinese characters from the classroom.</div>
</div>
</div>
<div class="page" title="Page 38">
<div class="layoutArea">
<div class="column">

It is difficult for a person who has never learnt Chinese to appreciate how much time it takes to achieve even the most rudimentary knowledge of the written language. Take my case as an example. I began Chinese in primary school; continued throughout high school; majored in Mandarin at university; undertook advanced classes at a university in China; graduated and became a Chinese teacher; and moved to Beijing where I continued teaching in complete Mandarin immersion.

Yet I still can’t write a complete shopping list or read a serious novel in Chinese. If my experience were unique, we might just conclude that I am exceedingly stupid. The fact is, however, that among Chinese learners the world over, very few of us are functionally literate in Chinese. This has led to a strictly enforced code of silence on the issue, in which we all pretend we can write Chinese but never ask one another to prove it.

It is even more difficult for a native-speaker to understand the depth of this problem. Second languages are learnt in an entirely different way to our mother tongue – which is why Chinese teachers are confounded when Australian students fail to grasp characters after years of study, and still get tones wrong despite endless repetition. We fail not because Australians are inherently bad at Chinese, but simply because the tools given to us – teaching pedagogy, exposure to the language, opportunities to practice and actual time spent on the language – are wholly insufficient for the magnitude of the task.

Although it differs from state to state, most Chinese programs in Australia require students to have a working knowledge of around 500 characters by the time they finish year 12. In order to reach this point, most students will spend a majority of their classroom time enduring hour after hour of flashcards and rote memorisation, to the detriment of their speaking and listening skills. Should they persevere and succeed at this already monumental task, they will be rewarded with the approximate competency of a Chinese grade one student.

</div>
</div>
</div>
<div class="page" title="Page 39">
<div class="layoutArea">
<div class="column">

There is, however, a viable alternative to the use of characters in school, which would allow students to achieve greater fluency, literacy and communicative confidence in Chinese by the time they graduate from Year 12. By teaching students to read and write wholly in <span style="font-style: italic;">pinyin</span>, the Romanised version of Chinese, students would be able to spend less time memorising and more time communicating. Chinese, after all, presents other obstacles to the learner in addition to character writing, such as the use of tones, idiomatic syntax and a not-insignificant cultural barrier. These challenges are often neglected simply because the biggest hurdle – that of character writing – ends up taking up so much of a learner’s time.

<span style="font-style: italic;">Pinyin </span>is learnt by all students in mainland China, who can use it as freely and effectively as they do characters. It is entirely phonetic and assists in the development of tones. It is also in increasingly prevalent use in China on the signs and billboards of major cities. It is not only a logical and effective method of rendering the Chinese language into symbolic expression, but also beneficial as a frame of reference for any learner who needs to bridge the gap between English and Chinese. <span style="font-style: italic;">Pinyin </span>provides the essential ingredient for making Chinese more accessible – an alphabet, allowing the learner to write down new words that they hear and put them immediately to use.

The use of <span style="font-style: italic;">pinyin </span>rather than characters would also go a long way towards resolving the other key problem with Chinese language teaching in Australia – that of ineffective teaching methodology. Although Australia is fortunate to have a vast number of linguistically-skilled, highly- dedicated, native-speaking Chinese teachers, the challenge of adjusting to teaching in an Australian context presents a significant barrier to many. Anyone who has observed the teaching and learning in an average public school in both China and Australia will be aware that the two systems are worlds apart. Without debating the respective advantages and disadvantages of each, it is certainly fair to say that Australian students are used to an interactive and communicative school environment, while Chinese classroom learning is more academically rigorous and involves a highly-structured approach.

</div>
</div>
</div>
<div class="page" title="Page 40">
<div class="layoutArea">
<div class="column">

The result of these differences is that native-speaker teachers, having studied under the latter system, often teach Chinese in the same way that they were taught it. Australian students, meanwhile, are used to a different system altogether, and are thus predisposed to finding such methodology boring or overly rigorous. It is in the learning of characters that this conflict becomes most pronounced. The predilection of native-speaker Chinese teachers for using character-sheets and flashcards as a primary teaching method is so often quoted as a reason for students dropping Chinese that it has become the stuff of legend.

By removing the requirement to learn characters, teachers will be able to spend more of their time teaching students to communicate, and exposing them to authentic Chinese – something which native-speaker Chinese teachers naturally do very well. With more emphasis placed on communication, students will be able to use their Chinese far earlier, allowing them to develop confidence and a love for the language, making it far more likely that they will continue to study it in senior school.

Compulsory schooling is, ultimately, far more about finding out what one likes and is good at than attaining mastery in a certain subject. If a teacher of Chinese manages to instil in their students an enduring interest in the language and the ability to start a basic conversation, then they have done their job extremely well. Why should we destroy that possibility just to give kids a year one reading level in characters that they probably won’t use? While academic rigour in language study is obviously crucial, Chinese is hardly a walk in the park, characters or no characters. Replacing ideographs with <span style="font-style: italic;">pinyin </span>will simply reduce the difficulty enough for students to gain entry to the language. In finding it accessible, many more than before will soon discover the fun and challenge of Chinese, and the untold benefits that it can bring them.

<em>Nathan Lee is a former Mandarin teacher who now runs 'the China Service', which offers training in the Chinese language, culture and business environment for corporate clients.This article appears in the <strong> <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20FINAL.pdf">2011 ACYA Cultural Journal of Sino-Australian Affairs</a> </strong> (pp. 25-31). </em>

&nbsp;

</div>
</div>
</div>