{
  "title": "<!--:en-->ACYA Newsletter: September 2014<!--:--><!--:zh-->中澳青年联合会2014年9月ACYA月刊<!--:-->",
  "author": "ACYA",
  "date": "2014-10-11T14:28:45+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->The ACYA Newsletter September Edition features highly sought after careers opportunities, as well as recruitment to join the ACYA National Team in 2015. 

<a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&id=17968c2505">Please click here to read the Newsletter</a><!--:-->