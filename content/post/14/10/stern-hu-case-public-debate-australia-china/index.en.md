{
  "title": "The Stern Hu Case and the Public Debate in Australia about China",
  "author": "ACYA",
  "date": "2014-10-12T14:32:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="page" title="Page 69">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#david">David Davies</a></h2>
<h3><span style="font-weight: bold;">Introduction </span></h3>
Australia’s relationship with China is based on ‘mutual respect for each other and a recognition that societies that have different cultures ... can nevertheless work together ... if they ... focus on those things that bring [them] together.’ (Howard 2002). As bilateral trade has increased Australia and China’s economic links have proved the buttress of this ‘mutual respect’. However, the Stern Hu case illustrates that despite an intention not to focus on cultural and political issues, differences can nevertheless arise to challenge this relationship.

This essay analyses how the public criticism of the Chinese judicial system is misplaced since it assumes that Australia’s is superior. This reflects an inability to show ‘mutual respect’ to China’s different legal culture and a tendency to place as ‘other’ anything alien to Australia’s system of law. In light of this misunderstanding this essay proposes that this attention to differences is due to Australia’s identity crisis — it has difficulties integrating into the Asian region while maintaining its Western values. However, it is argued that both of these issues will be overcome as China’s legal system develops and Australia takes more active steps to become Asia-literate. Nevertheless, an underlying aspect of the Stern Hu case is Australia’s strategic position in the world, and the controversy surrounding the case is related to the worry that one-day Australia will have to choose between its economic interests with China and security interests with the US. This essay analyses how Australia’s understanding of China is challenged and what the implications may be is undertaken, concluding by commenting on the future implications for Australia’s relationship with China.

</div>
</div>
</div>
<div class="page" title="Page 70">
<div class="layoutArea">
<div class="column">
<h3><span style="font-weight: bold;">Cultural Otherness </span></h3>
Australia’s surety of its legal system as a superior model to China’s is the basis for the criticism of the Stern Hu verdict. The verdict is criticised for being ‘based on extremely shaky legal foundations’ (Kent 2010). This is because China has no ‘principle of governance in which all persons, institutions and entities, public and private, including the State itself, are accountable to laws that are publically promulgated, equally enforced and independently adjudicated’ (United Nations Security Council 2004, p. 4). This deficiency in the rule of law is illustrated by the fact that only Rio Tinto employees were arrested and none of those who received bribes were. In addition, some commentators believe the arrests to be politically motivated (in retaliation for the failed Chinalco bid for a Rio Tinto mine) rather than being the usual application of the law (Barboza 2010). From an Australian perspective this undermines any possibility of judicial fairness.

This Australian-centric analysis is a barrier to understanding the Chinese legal system. Authors like Kent (2010) position China as the ‘other’ in relation to what Australia considers the objective standard for what should constitute the rule of law. The implication is that the ‘other’s’ legal system is inferior to that of Australia. However, China’s differing legal system should be viewed objectively. This is because Australia’s conception of the rule of law, rooted in Western concepts of liberalism (with characteristics like the presumption of innocence and fair sentencing), is not applicable to a Statist Socialist country like China (Peerenboom 2002a). The Statist Socialist system of law has more emphasis on ‘serving the interests of the state and ensuring social stability’ with less emphasis on individual rights (Peerenboom 2002b, p. 300). That this is left unacknowledged by media personalities reflects a lack of objectivity on their part.

</div>
</div>
</div>
<div class="page" title="Page 71">
<div class="layoutArea">
<div class="column">

Further, China is a Unitarian state; the judiciary is not a separate legal entity and is subject to the National People’s Congress. As Article 128 of the Chinese Constitution states: 《最高人民法院对全国人民代表大会和全国人民 代表大会常务委员会负责。地方各级人民法院对产生 它的国家权力机关负责》. Further, as Peerenboom (2002b, p. 302) states, the object of Chinese courts is to ‘enhance the unity of the state, and be consistent with the present government structure based on democratic dictatorship of the people and supremacy of the people’s congress.’ So, in light of this fact and Premier Wen Jiabao’s (2010) statement that the Chinese government’s aim this year is to: 《把反腐倡廉建设摆在重要位置,这 直接关系政权的巩固》,it should come as no surprise that the judiciary, in line with the Executive’s policy objective (as implemented through the People’s Congress) would pursue corrupt people in prominent positions — corruption is perceived as a threat to government stability, and as such it is being pursued. Thus, even if there is a political subtext to targeting Stern Hu such an action is in line with the structure of the legal system.

If a more objective view were taken it would also be revealed that China may be on the way to developing a firm command of the rule of law. Peerenboom (2006) points out that China is exceeding expectations with regard to development of a rule of law in comparison to comparable countries at the same level of income. He also postulates that there is no reason to suppose China will not develop a rigorous rule of law similar to Taiwan and Hong Kong, which had similar problems in their early stages of economic development. Indeed, it has been stated in response to the effect of anti-corruption measures in Hong Kong in the 1970s — they ‘changed much in Hong Kong that many regarded as unchangeable’ (Cheung 2008, p. 57). Thus, there is cause for optimism at the possibility of China’s concept of the rule of law converging with Australia’s.

</div>
</div>
</div>
<div class="page" title="Page 72">
<div class="layoutArea">
<div class="column">

Debate over the Stern Hu case has focused on issues relating to the rule of law, but from an Australian perspective. This poses a challenge for Australia’s relationship with China as it reflects Australian commentators positioning Australia as a country with a better legal system, with China positioned as the ‘other’ (and thus inferior). The key to dealing with this challenge is to ensure that there is objectivity in any analysis. To do this Australia does not have to stop criticising China’s legal system but a degree of objectivity and acknowledging the positive aspects of this system would enhance Australia’s relationship with China — it would increase understanding and show a ‘mutual respect’ for cultural differences.
<h3><span style="font-weight: bold;">Identity Issues </span></h3>
The Australian media’s concern with China’s differing conceptualisation of the rule of law in the Stern Hu case reflects Australia’s uncertainty over its cultural identity. It highlights the incongruity between its self-identification as an Anglo-Celtic nation in an Asian region and whether it can identify as being Asian. This ambivalent relationship and the cause of the incongruity can best be understood in the context of how Australia has historically interacted with Asia.

The first act of the Australian parliament was to implement the ‘White Australia Policy’ (Meaney 1995). In the words of Prime Minister Edmond Barton (1901): ‘We are guarding the last part of the world in which the higher races can live and increase freely for the higher civilization.’ There was a desire by Australia to isolate itself from the Asian states, which were regarded as ‘populous, poor, non- industrialised, undeveloped and non-democratic’, and were a threat to ‘racial purity and the security ... of the state’ (Higgott &amp; Nossal 1997, p. 4). When this policy was enacted most of Asia was subject to European rule and Australia’s economic and political security came through its connection with the British Empire (Meaney 1995). Thus, there was no incentive to engage the Asia region — cultural identity, security and economic prosperity were all tied to the Western world.

</div>
</div>
</div>
<div class="page" title="Page 73">
<div class="layoutArea">
<div class="column">

This view can be contrasted with the ‘relocation’ perspective, which saw Keating in the 1990s attempt to make Australia more Asia-literate (Johnson et al 2010). This change in outlook can be seen as Australia coming to terms with the reality of its location in Asia. Indeed, Australia’s economic integration with Asia was well developed even in the 1990s (Meaney 1995), and has only increased since then, with China now being Australia’s largest two-way trading partner (Malik 2007). Moreover, northeast Asian economies have been identified as being more deeply complementary to Australia’s economy than any others on earth (Garnaut 1989). Thus, there is an implicit knowledge that Australia’s economic future is tied to Asia’s prosperity and willingness to continue trading on favourable terms.

Issues such as the Stern Hu challenge this idea of Australia becoming more integrated with Asia since it highlights the disconnect between Australia’s geographical reality and cultural outlook. This tension over whether Australia is Asian or not is exacerbated by the fact that Australia may be viewed by Asian nations as lacking ‘Asianess’ and thus the ability to effectively integrate into the region (Meaney 1995). These ‘push’ and ‘pull’ factors make Australia’s place in the world uncertain, and in the context of a growing Chinese influence over Australia makes every action, which is cultural antithetical seem problematic.

</div>
</div>
</div>
<div class="page" title="Page 74">
<div class="layoutArea">
<div class="column">

The best way to manage Australia’s relationship with China is to take maintain a ‘mutual respect’ for cultural differences. As Fitzgerald (1997, p. 9) points out: ‘the fundamental problem is that while [Australia] may have come to mouth the sentiment of belonging to the region, [it has] done too little ... to make the necessary cultural and intellectual adjustment.’ This intellectual adjustment does not require Australia to abandon its cultural outlook, but in the same way it does not require China to deviate from its concept of the rule of law (or other issues such as Human Rights). However, it does require Australia to be more objective in its interactions with Asia and to approach issues, such as Stern Hu, with a degree of objectivity, which can be achieved through making Australia more Asia literate, both in language and cultural terms. Doing so will help provide background on how China operates and thus facilitate ‘mutual respect’ for differences.
<h3><span style="font-weight: bold;">Security Issues </span></h3>
Thus far this essay has mainly dealt with the Stern Hu trial from a pragmatic perspective — that China was well within its legal rights to prosecute and their legal system should be accorded due respect. However, it has been contended that Australia in resisting the Chinalco bid for an Australian asset was motivated by a desire to stem the direct control of Chinese state controlled companies over natural resources — to limit China’s economic influence over Australia. In retaliation China prosecuted an Australian citizen (Tiffen 2009). If this is the case, it would not be the first conflict with China, and it represents tension over China’s growing influence over Australia. Other issues include the allegation by Chen Yonglin on his defection that there was an extensive Chinese spy network in Australia (Stewart 2005), and incidents such as the anti-Tibetan protests in Canberra (Garnaut et al. 2008). These represent incidents where China seems to misplace its ‘mutual respect’ by ignoring Australia’s national sovereignty (in the case of spies) and concept of freedom of speech (in the case of the Tibetan protests).

</div>
</div>
</div>
<div class="page" title="Page 75">
<div class="layoutArea">
<div class="column">

The cause of this conflict is the difference in economic and security alignment, as ‘for the first time in history Australia’s trade flows [with China are] out of step with its strategic alliances [with the US]’ (Medcalf 2008, p. 240). Australia is a middle power (Higgot &amp; Nossal 1997), one that has always been required to rely on its traditional allies, the British and then the US (Meaney 1995). However, a reliance on China economically, and the US for security represents a conflict of interest and the Stern Hu case raises the issue of how Australia would resolve this conflict if China and the US became embroiled in a dispute.

This threat is very real since there are key strategic conflicts between the US and China. Roy (2003) postulates that strategic rivalries between the US and China could divide the region, and it would force Australia to choose between them. For instance, Malik (2007, p. 593) China has no qualms supporting ‘pariah regimes’ or endorsing ‘divide and dominate tactics in multilateral forums’. This gives rise to concerns that China, as a nation is unwilling to work with established international frameworks (i.e. ones dominated by the US). In addition, he contends that regimes that do not ‘accept institutional constraints on the exercise of power at home are unlikely to respect the rights and interests of others in the international system.’ Essentially, despite China’s ‘peaceful rise’ policy there is still the chance of it conflicting with the US (Malik 2007). Underlying the growing potential for conflict between these two great powers is the fear that Australia may be required to support its strategic ally, the US, against China in a conflict. Of particular concern is the possibility that the US may invoke the ANZUS treaty with respect to Taiwan, requiring Australia to commit to a direct conflict with China (Zhang 2007). Thus, there is the very real threat that China and Australia’s political interests will diverge, which would force a reassessment of Australia’s economic commitments with China, potentially harming Australia’s economic prosperity.

</div>
</div>
</div>
<div class="page" title="Page 76">
<div class="layoutArea">
<div class="column">

The rejection of the Chinalco deal by the Australian government can be seen as a desire to mitigate China’s economic influence over ‘strategic’ areas of the Australian economy (Manicom &amp; O’Neil 2008), which is directly related to potential future security conflicts — this resistance to giving China too much direct control of key assets is based upon a desire to maintain alignment of strategic policy with the US. In the event of a conflict which required Australia to side with the US, control of key assets by China would be contrary to Australian security interests. The subsequent arrest of Stern Hu illustrates that China will react when its interests are impinged and this validates the fear that Australian and Chinese interests will diverge.

The implication for Australia is that as China increases its strategic role in the world Australia must implement a hedging strategy to the effect that its security interests lie with the US while economic interests lie with China. It is in Australia’s interests to balance as best it can the relationships with China and the US since this maximises its national welfare (Cheng-Chwee 2008). However, given the possibility of a conflict between China and the US it seems that there would be a preference to side with its historical ally the US — in essence security would trump economic benefit. Given this, instances where there is a definite security conflict between Australia and China like the Stern Hu case give rise to fears of this one-day occurring.
<h3><span style="font-weight: bold;">Conclusion – The Way Forward </span></h3>
The criticism inherent in the public debate surrounding the Stern Hu trial was predominantly concerned with China’s differing view of the rule of law, and the lack of objectivity surrounding the issue reflects an Australia that is unwilling to accept other legal perspectives. This is due to uncertainty over Australia’s place in the world — it is torn between identification as a European nation and an Asian nation. Nevertheless, there is still a real concern about potential conflict over key security issues given the political subtext to the Stern Hu prosecution and what it means for Australia’s security interests.

</div>
</div>
</div>
<div class="page" title="Page 77">
<div class="layoutArea">
<div class="column">

On the whole, Australia’s fear of China is to some extent unfounded and should be taken in context. As stated at the beginning of this essay, the relationship between China and Australia can be best developed through a ‘mutual respect’ for the other’s differences — such as between China and Australia legal systems. Indeed, Australia should take note of how China perceives Australia with its ‘sound legal system, stable political, social and economic conditions ... [making] it ‘an ideal partner in China’s implementation of its overseas energy and mineral resources strategy’ (Zhang 2008, p. 93). The very reasons China values Australia as a trading partner are the areas Australia has most conflict over with China. Given this, while there is a real possibility of conflict with China it is by no means certain. As such, Australia whilst exercising caution in questioning its relationship with China should also approach it with a degree of optimism.

<hr />

<strong>This article appears in the <strong> <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20FINAL.pdf">2011 ACYA Cultural Journal of Sino-Australian Affairs (pp. 59-67)</a>.</strong></strong>

<em id="david">David Davies is a B. Com / LLB student from the University of Melbourne and is the current National Vice- President (Education) for ACYA. He is also an editor of this journal.</em>

</div>
</div>
</div>