{
  "title": "Where is Home? ",
  "author": "ACYA",
  "date": "2014-10-08T04:01:08+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "where_is_home",
      "type": "image",
      "src": "Lu_Xun_Former_Home_At_Beijing-e1412740478159.jpg"
    }
  ]
}
<div class="page" title="Page 138">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#jc">Guo Juncheng (郭骏程)</a></h2>
<strong>This piece was awarded the ACYA Journal Creative Work Prize.</strong>

I lost my culture, I am homeless. I found a new culture within me, I am an outsider.

Life is like a journey during which we constantly look for a place to belong and end up finding a place that we call ‘home’. However, the destination we keep looking for is a place that we left a long time ago and can never go back to.

In Australia, the question I was asked most was ‘where do you come from?’ But I was always hesitant before replying.

My first home is Xishuangbanna, a land of dense forests, heat waves and muddy rivers. But these memories are already blurred and indistinct. I left this border town for a strange city with my parents at the age of three. Memory of this ‘home’ was rebuilt after I grew up, visiting grandparents over the Chinese New Year. Such visits were made every year, but each time only for a week or so. After ten years, I thought I had again come to know this place as home, but when I took my camera around Jinghong city in my third year of university, I discovered that still I did not understand her.

I insist on visiting a Buddhist temple whenever I am back in Xishuangbanna. There I can resolve my emotions, pray for a red line and summon unspeakable forces back to my heart. That year, they built what was hailed as the largest Theravada Buddhist temple in Southeast Asia, and I went to visit with my parents. They were able to enter as locals, but a ticket officer stopped me at the gate because my identity card shows that I am not a Jinghong local. I took out my student card from the Central University for Nationalities and tried to convince him that I was born there and am of the local Dai ethnicity. But I had nothing to say when he asked me to speak the Dai language. I realised that I had been gradually banished from my hometown ever since I was born. Finally, I had been stopped outside the door, never to be let in again.

</div>
</div>
</div>
<div class="page" title="Page 139">
<div class="layoutArea">
<div class="column">

I can only be dragged forward by time, to find another home.

</div>
</div>
<div class="layoutArea">
<div class="column">

It is often said that universities are like your second parents, and so Beijing should be my second home. I had the best years of my life there. A youth of eighteen and nineteen blossomed with satisfaction irreplaceable. When it came time to graduate, many of my fellow classmates from out of town were struggling to decide whether or not they wanted to keep drifting around Beijing and make this faraway metropolis their home forever. Without a doubt, I loved the subway system, cinemas, libraries, historical sites, scenic spots and delicious food of Beijing. However, if you asked me whether I would be willing to lay down roots in the city, I would have to decline, due to the illness I suffered in the winter of my freshman year.

I became seriously ill just after I arrived in Beijing; the freezing winters of Northern China are too hard to adapt to. I thought it was just because my body was weak, and that I would be fine once I became accustomed to the new climate. However, the same affliction would take its toll every late autumn or early winter: a relentless cough that simply could not be remedied until the weather changed for the warmer come springtime. What is more, Beijing summers are not particularly pleasant either. If my hand so much as brushed over a handrail on the subway or a public bus, even if I did not rub them my eyes would still become red and watery once I returned home. At the start, I thought this might be because my towel was dirty, but if I left Beijing the problem would suddenly disappear. My mother, who studied medicine, told me later that it was an allergy, not an infection. Ever since then I have known that I cannot live in Beijing no matter how large or attractive it is. If not even my health could be guaranteed in this place, then why should I struggle hard to make it here and call home a place so hostile to outsiders?

So I went abroad to seek my ideal home.

It was only after I went abroad that I finally realised that I only have one home, and that is China. I have not yet found the ideal place to settle down in this enormous country, but the ancient culture that penetrates through the strokes of the characters in her name is enough to give my soul something to fall back upon. It only takes the writing of a few Chinese characters or the most common of Chinese dishes to cause foreigners to regard me, this black-haired and dark-eyed person, with increased respect. Reciting classical poetry and studying ancient literature used to seem like such a burden, but now I understood these as an enormous blessing. The Chinese people possess almost identical script and grammar, use the same ways of thinking, and experience similar inspiration and emotion to our ancients who lived two thousand years ago. Chinese characters withstood the erosion of time, and from them we can still see what the ancients saw, understand what the ancients thought, and feel what the ancients felt. This is a remarkable achievement in and of itself.

</div>
</div>
</div>
<div class="page" title="Page 140">
<div class="layoutArea">
<div class="column">

This feeling became stronger when I was an Asia-Pacific Studies research student in Australia. More than half of the students in my major could speak Chinese, whom I initially though would put me to shame. But I found out later that the Chinese most of them had learnt was only sufficient for use as a tool – they could not experience the deeper meanings stored in Chinese characters. Even those foreigners who claimed to be or were recognised in their field as ‘China hands’, could only consider China with their brains and be remarkable only to the extent they could <span style="font-style: italic;">think </span>about China, without ever being able to <span style="font-style: italic;">feel </span>the Chinese language with their hearts. People cannot change where they were born or where they came from. Destiny made arrangements for you the moment you were born, and whether or not you are able to realise the beauty in this, we all have to live in a place called ‘home’.

After thinking about it carefully, I realised that Australia is not my real home. I am of Dai ethnicity, and the home that the heavens planned for me should be in the Dai language and culture. However, being a so-called ‘false Dai’, I cannot even form a full sentence in the Dai language, let alone understand the depths of the Dai. If I were to stand in front of other people and say nothing, they would undoubtedly think of me as no different from any other majority Han ethnicity Chinese. Yes, I admit it; I was long ago assimilated and am a Han Chinese through and through. In my family, not even my parents’ generation speaks the Dai language, and so I did not learn it as a child and did not know where to begin with it as an adult. I left my home from the moment I was born and am never able to go back.

There are many people of Asian descent who are born in Australia, and in a white society they are a minority. They rarely return to their ancestral homeland, but because of their parents they are fluent speakers of their ancestral language. Their situation is much better than that of most Chinese ethnic minorities. However, through the course of deep friendships I realised that their hearts have long been westernised, they speak English better than their ancestral language, and the literature and art they appreciate is in English. Apart from an ethnic identity, they are no longer able to inherit or promote their native language. So they are also a group of homeless children, the same as me, always hovering in front of the door of their ‘home’, but unable to go back in.

Home is not only a place to eat and sleep at ease, but also the resting place of a person’s body and mind. In this turbulent world we are constantly migrating in order to try and find some place to settle down and somebody to love. This place we search for has the tastes, foods, customs and festivals, and the sounds, thought and appreciation, of our mother tongue, but more importantly it has the people whom we are familiar with and do not need language to know, that the expression in their eyes says: ‘child, you belong here’.

Where is this home of mine? I keep asking myself this question. I have let so many homes slip by, and now I have come to a new place to keep searching. When will I be able to find a place where I am happy? Or return to it? I firmly believe that wherever you are standing now is your home. But from a cultural standpoint, my original home has already forsaken me and I am destined to be a wanderer, forever drifting. But I believe that I am not alone in this world. So many people move from one place to the next, all bringing with them a yearning for their hometown whilst building a new home together. All that we itinerant children can do is to keep moving forward, ceaselessly gathering the dust of our hometowns, which whirls and scatters in the air around us to speck-by- speck form the land underneath our feet. This way our bones will have a place to return to, and our descendants a place to go.

<hr />

<strong>This article appear in the <a href="http://www.acya.org.au/wp-content/uploads/2013/06/ACYA-Journal-of-Australia-China-Affairs-2013.pdf">2013 ACYA Journal of Autralia-China Affairs (pp. 138-141)</a>. It was translated into English from the original Chinese by Katrina Fan 范羽佳 and proofread by Greg Mikkelsen 葛云天. </strong>

<em id="jc">Juncheng is a Tai person from Xishuangbanna, who floated around Beijing at the Central National for Minorities for four years before floating onto foreign lands and places as a Master of Arts (Asia-Pacific Studies) student at the Australia National University. He is engrossed with literature, poetry and all other rational and irrational fancies.</em>

</div>
</div>
</div>