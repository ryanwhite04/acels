{
  "title": " Local, Translocal and Gay Identities in Beijing",
  "author": "ACYA",
  "date": "2014-10-08T21:00:01+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="page" title="Page 102">
<div class="layoutArea">
<div class="column">
<h1 style="text-align: center;">Local, Translocal and Gay Identities in Beijing: An analysis of competing local, Asian and global identities on the development of a gay identity in Beijing</h1>
<h2 style="text-align: center;">by <a href="#robert">Robert Hopkins</a></h2>
On January 15, 2010, the stage in Lan Bar, in Beijing’s busy Silk Market district, was set. International media lined a makeshift catwalk in the centre of the dance floor as cameras and journalists bustled to get a view of the contestants in the first ever Mr Gay China Pageant. Ten young Chinese men, many of who were only publically displaying their sexuality for the first time, waited nervously behind closed curtains as the crowd grew steadily by the minute. The audience waited patiently, chatting and drinking quietly, as an announcement was made. The starting time for the event was to be pushed back. Then pushed back again. Finally, after forty-five minutes, American organiser Ryan Dutcher and his Chinese boyfriend, Zhang Jin, appeared from behind the curtains. ‘We’ve been shut down,’ he said. ‘The police have come, and they have shut us down’.

The outcome of the first Mr Gay China Pageant highlights current problems relating to the development of a gay identity that exist within Beijing, and indeed across China, with aligning notions of gay identity with notions of Chinese identity. The cancellation of the event by authorities may on the surface seem indicative of an authoritarian state trying to stymie the outward expression of ‘hooligan’ activity such as homosexuality. In reality, China, despite its historical record of suppressing human rights and the restriction of the individual, incorporates no explicit prohibition of homosexual acts in its Constitution and Criminal Code (1982), nor has historical precedent of actively suppressing and targeting homosexuals as can be still witnessed in the Western arena. Rather, the implicit implications of the events cancellation can potentially provide insight into notions of local, translocal and global gay identities, and how they ally and compete within Beijing and across China. This paper is based on interviews with audience members at the Mr Gay China event. It will use these people’s understandings and experiences with gay identity within China as the foundation for a theoretical discussion of how identity is formed, and how the development of new identities is competing with the notions of Asian and global identity.

</div>
</div>
</div>
<div class="page" title="Page 103">
<div class="layoutArea">
<div class="column">

This paper relies on primary and empirical research of gay identity within Beijing. Beijing acts as a suitable ‘case study’ city, mainly because it provides a good cross-section of Chinese society. Beijing not only acts as China’s political capital, it is also the main Chinese hub of culture and society. Beijing acts as an ideational referent for the majority of Chinese people, not only politically but also culturally and ideologically. Since the ‘opening up’ and large-scale economic reforms that have transformed China’s socio-political landscape during the 1980s, Beijing has experienced drastic transformation in terms of its demographic and its cultural identity. The development of factories and labour-intensive manufacturing centres in the outer suburbs of Beijing has attracted an unprecedented number of migrant workers. Similarly, the establishment of transnational corporations in Beijing’s city centre and financial district has attracted large numbers of expatriates and business-people. Combined with the existing ethnic diversity of the city due to concentrated populations of all fifty-five of China’s minority groups, and the large student population concentrated in the city’s Haidian and Zhongguancun districts, Beijing contains a demographic and cultural identity that is largely representative of a cross-section of Chinese society as a whole.

</div>
</div>
</div>
<div class="page" title="Page 104">
<div class="layoutArea">
<div class="column">

Because of the fact that Beijing serves as a cross-section of Chinese society, it can also be assumed that examining the gay culture within Beijing will provide an insight into the identity of homosexual identity across China. This paper will utilise Beijing as a case study for an examination into the development of a gay identity in China, examining the impacts of three different ideational forces on the development of gay identity in Beijing. The notion of Chinese identity and an Asian identity will be used to analyse how Chinese and Beijingers have developed a distinctly local gay identity. This paper will then outline the impacts of expatriate and student communities on Beijing’s translocal gay identity, and the potential impacts that such an identity can have on local gay communities. Finally, this paper will question the existence of a truly ‘globalised’ gay identity, how such a global identity interacts with a Beijing based gay identity, and potential conflicts that can arise between global gay identity and the development of identities at a local and translocal level.
<h3><span style="font-weight: bold;">A Distinctly Local Gay Identity </span></h3>
Despite the premature shutdown of the Mr Gay China Pageant, the majority of the large crowd that had come to watch the show remained behind to drink and discuss the events of the evening. Among those present was Mr. Shuai Li. Shuai, as he is affectionately referred to amongst the gay community, is forty five and married with a daughter. Publically, he leads a very ‘normal and respectable’ life in China, with a steady job and a close-knit family. Privately, however, he adopts a different persona, and is an active member of Beijing’s gay community. Shuai is a businessman from Chongqing, in China’s West, but comes to Beijing on a bi-weekly basis to oversee management of his property development firm.

</div>
</div>
</div>
<div class="page" title="Page 105">
<div class="layoutArea">
<div class="column">

‘I’m very disappointed that this event was cancelled’, he said. ‘It would have been a great opportunity to let the world know that there are gays in China, and that we are working to make the world a better place for us gays.’ But what does it mean to be gay in China? ‘For my generation’, he says, ‘Being gay in China means having a wife and a child. There is no way to be gay in China for people in my generation, because Chinese custom is all about family. How can I disappoint my mother, my father, how can I be single forever, how can I not have children and let my name die. This would be a great shame.’

Shuai’s personal conflict between the function of family and sexual identity is indicative of how the majority of gay individuals in China have developed their own identity. In order to analyse how such an identity is formed, the means of cultural production in Beijing and across China must be established. Culture should be examined ‘not as a set of shared meanings found in a bounded space, but as ongoing discursive practices with sedimented histories that mark relations of power’ (Rofel, 1999, p.457). In this way, culture can be viewed as being directly impacted by spatial forces, such as the boundaries of state or a city, as well as the discursive elements of cultural production and identity which have arisen from that space (Eber, 1986).

If culture is viewed in such a way, it is easy to see the importance that local identity has in shaping the way a local gay identity is formed for local individuals such as Shuai. While transnational elements have a significant impact on the way gay identity is outwardly portrayed by Chinese individuals operating within the Chinese cultural system, individual gay identity has been marked by discursive practices, occurring in the context of traditions, customs and norms (Chou, 2001, p.28). The important role of the family in Chinese traditional culture, for example, has a significant impact on the way that gay identity is outwardly expressed, and marks a significant detachment from expressions of gay identity in Western culture. The role of family has been transformed even further by discursive forces of politics, creating a uniform and homogenous identity among all Chinese, not just Chinese homosexuals, that aims to ally the importance of citizenship with the importance of kin (Jeffreys, 2007, p.165)

</div>
</div>
</div>
<div class="page" title="Page 106">
<div class="layoutArea">
<div class="column">

Shuai is not an anomaly. From a Western viewpoint, the idea of a happily married gay man seems paradoxical, and indeed Shuai confesses that he sometimes does feel depressed, unfulfilled and torn in terms of his own conflicting identity. ‘But’, he adds, ‘Sex isn’t everything. There are other things that are more important about sex. Family. My mother’s happiness. My wife’s happiness. Raising a child who is a good citizen. These things are surely more important than sex’, he argues. Shuai touches on two of the most important distinguishing characteristics of Chinese gay identity: the importance of the collective over the individual, and the importance of family to identity.

Chinese society is based on the foundations of family and kinship. Family serves as one of the most important referents of Chinese society, whether at a personal or a public level (Chou, 2001, p. 28). Notions of the family are incorporated into the core ethos of Confucianism, and have been translated into government policy regarding social order and citizenship (Fukuyama, 1995, p.20). For Chinese gay men, too, in whatever context, the underlying ideologies of family and kinship have an enormous impact on how they develop their own cultural identity, as distinct from similar narratives experienced in the rest of the world.

For Chinese men, to comply with the role they are expected to adopt in society, marriage is the final goal. They must fulfil their role as a male in society and find a wife, reproduce, and continue the cycle (Chou, 2001, p.30). Men in China, too, have enormous pressure on them, directly imposed by state apparatus, to produce offspring. Since the one child policy was introduced in 1978, a disproportionate number of males to females have been born in China, mainly because boys are preferred in Chinese tradition due to their capacity to produce heirs which will not only be able to care for the family, but will also carry on the family name (Bongaarts, 1985, p. 585). In a state where the human right to produce offspring is so severely restricted by government policy, the notion of a gay man ‘wasting’ his potential to reproduce is incompatible with Chinese norms.

</div>
</div>
</div>
<div class="page" title="Page 107">
<div class="layoutArea">
<div class="column">

The key ideological referent of Chinese gay identity, therefore, remains the family. This is where the Chinese gay identity can be seen as divergent from Western and global perspectives of gay identity. While Western identity often ‘divides human beings into a homo-hetero binarism’ (Chou, 2001, p.30), Chinese identity is based on one’s capacity to fulfil certain obligations associated with the constructions of hierarchy among family and class lines. In this way, it seems almost absurd to construct gay identity in China using the same model of construction as was used to construct Western notions of gayness. The highly individualised notions of ‘coming out’, being ‘out and proud’ and ‘we’re here, we’re queer, get used to it’, which typify the Western identity of homosexuality, lie in direct opposition to the highly collective notions of family and social obligation that are ingrained in Chinese customs and norms, and therefore remain inappropriate to Chinese contexts (Altman, 1997, p. 422).
<h3><span style="font-weight: bold;">Expats, Students and Beijing’s Translocal Gay Identity </span></h3>
In order to qualify how a uniquely Chinese gay identity has developed in Beijing, it is also important to analyse translocal impacts on identity within the city. The influence of expatriate communities upon gay identity in Beijing should therefore be examined, in order to expound a rubric of gay identity.

</div>
</div>
</div>
<div class="page" title="Page 108">
<div class="layoutArea">
<div class="column">

Michael Williams saw the shutdown of the Pageant as ‘typical and indicative of narrow-minded Chinese governments’. Williams, a corporate lawyer from Canada who has lived in Beijing for eleven years said: ‘Gays have always been put down here, put back into their boxes, ever since I have been here at least...they have never been able to openly express themselves, and tonight is proof of that’.

The problem with Mr Williams’ approach to the shutdown of the Pageant is that he assumes that the organisation of an international pageant such as Mr Gay China is somehow meant to affirm the existence of a global gay identity within Chinese competitors and Chinese members of the audience. What Mr Williams fails to appreciate is the subtle nuances and differences of his Chinese counterparts, based on discursive cultural narratives between Western and Chinese referents, as has been discussed in the above section outlining local identity. As mentioned above, the highly sexualised notions of gay identity, which characterise notions of gayness in the Western world, lie in direct opposition to understandings of homosexuality in Beijing, China and indeed across Asia. The gay movements of the West have often been based on individualistic notions of self-affirmation and the seizure of autonomy within a marginalised community (Rofel, 1999, p. 470). Such notions comply with Western notions of liberalism and independence that are so inherent not just in gay identity, but in identity for all individuals.

The potential problems that can arise when translocal communities begin to influence the development of a uniquely local identity, as demonstrated by the increased influence of gay expatriate communities on local gay Chinese, is typified by the cancellation of the Mr Gay China Pageant. Holly Martin, a lesbian rights activist from England, who has lived in China for four years explained: ‘The crowd here is twenty Westerners to every one local. Its not the right place for any gay to affirm themself, its not an event for the local gay community, it's an event for the Western gay community to mould the locals into the gays they want to be’. Holly explains the fact that while it is all very well for Westerners to encourage Chinese people to be proud about their sexuality; they fail to appreciate the innate tension that such explicit displays stimulate within the Chinese individual.

</div>
</div>
</div>
<div class="page" title="Page 109">
<div class="layoutArea">
<div class="column">

Chinese gays are very receptive of and interested in the rapid progress of the homosexual movements experienced in the West, which began in the 1960s. The majority of local homosexuals approached at the Mr Gay China Pageant see confrontational methods such as activism and open dialogue as the clear way forward in terms of integrating a gay identity into notions of what it is to be Chinese, or what it is to be Asian. The overwhelming response, however, is that China’s current and historical contexts do not demand such confrontational responses. While the role of the homosexual within China is by no means homogenous with the broader understanding of Chinese identity, the discursive narratives that places the homosexual within time and place in modern China is homologous with the evolution of Chinese identity (Rofel, 1999, p. 471). For Chinese people, localised notions of homosexuality can potentially still comply with traditional notions of family, kinship and collectivism and therefore challenging them is considered redundant.
<h3><span style="font-weight: bold;">Towards a Global Gay Identity (?) </span></h3>
The disparities that exist when examining the elements of a gay identity between local and translocal communities calls into question whether there does, indeed, exist a truly ‘global’ gay identity. If identities conflict at levels as localised as Beijing, how can there exist a homogenous gay identity that is consistent throughout all societies in the international system? ‘Global Gayness’ is definitely a notion that has been promoted by gay activist groups in the West, because the notion of a unified body standing in solidarity has obvious benefits to how rights of the homosexual are experienced internationally. However, ‘Global Gayness, with its assumptions about the similitude of identity, the homogeneity of values and a sliding scale of identity development fails to capture the intricate complexities’ (Roefl, 1999, p.470) of gay identities not only in Beijing, but across all local groups on a global scale.

</div>
</div>
</div>
<div class="page" title="Page 110">
<div class="layoutArea">
<div class="column">

Despite the desire for Westerners to enforce the notions of a gay identity upon local Chinese interpretations of the norms of homosexuality as a means of liberation, there is still a need to tread carefully and resist the temptation to homogenise ‘gayness’ into ‘global gayness’. Rather, it should be appreciated that local and transcultural gay identities are characterised by the discursive elements of history, culture and norms, and the development of a gay identity on a global scale should not impede on the cultural imperatives of the individual operating at a local level (Levinas, 1951, p.9). The cancellation of the Mr Gay China Pageant should not be seen as a deterrent of the global gay movement, by any means. But certainly, more recognition of how gay identity is developed at a local level, by those localised individuals and their communities operating within the norms of Chinese custom, and the discursive elements of their own unique history, is imperative to how local Chinese gay identity will function with other notions of gay identity operating at translocal and global levels.

<hr />

<strong>This article appears in the <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20FINAL.pdf">2011 ACYA Cultural Journal of Sino-Australian Affairs</a> (pp. 92-100). </strong>

<em id="robert">Robert Hopkins is a recent graduate in International Studies (Asian Studies/Chinese) from the University of New South Wales, Sydney, and completed part of his degree at Tsinghua University, Beijing.</em>

</div>
</div>
</div>