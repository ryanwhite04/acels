{
  "title": "An interview with Shaun Harkness, inaugural recipient of ACYA-RBS IMBA Scholarship",
  "author": "ACYA",
  "date": "2014-10-24T05:10:10+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "98",
      "type": "image",
      "src": "98.png"
    }
  ]
}
Shaun Harkness is a young Australian and an inaugural recipient of the <a href="http://www.acya.org.au/education/scholarships/acya-ruc-imba-scholarship/">ACYA-RBS IMBA scholarship at Renmin University</a>. Following his graduation from the University of Wollongong in 2008 with a Master of Commerce, Shaun moved to Beijing, China, working in an international college teaching business studies to students who then continued on to study at Australian and UK universities.

<!--more-->

Shaun worked for several years with a small IT-startup in Sydney specialising in artificial intelligence, which was later acquired by IBM.

In 2013, Shaun travelled to Taiwan to study Mandarin Chinese at National Taiwan Normal University, before moving to Beijing to undertake the IMBA program at Renmin University.

ACYA Taipei Chapter President Charlton Martin had the pleasure of interviewing Shaun on his initial impressions of Renmin University at the commencement of his studies, and how he plans to immerse himself within the Australia-China space through this unique and prestigious opportunity.

<em>How did you find out about the Australia-China Youth Association and the scholarship opportunity, and what suggestions would you have for future ACYA-RBS IMBA Scholarship applicants?</em>

I was introduced to the ACYA-RBS IMBA scholarship by Charlton Martin, the president of the local ACYA branch in Taipei, after reading about the program on the ACYA website. I’m always sure to keep an eye on the ACYA website and Facebook page for any updates and job opportunities.

For future applicants my advice is don’t be afraid to show off your achievements, whether they are work, personal or study related. Selection committees seek a variety of backgrounds for their IMBA programs and want diversity in the classroom, which makes the learning experience so much more valuable for everyone involved.

<em>Why did you choose China as a location for studying your IMBA?</em>

The importance of China can’t be understated, especially given the growing relationship with Australia. I have long-held a great deal of interest in China, given its long history and fascinating culture. I also see living in China as an opportunity to improve my Mandarin skills, while making connections with the world’s future business leaders.

<em>What are your first impressions of life in Beijing?</em>

Beijing is an enormous city, the scale of which is hard to comprehend sometimes – over 21 million people live in Beijing, not far off the entire population of Australia. Although I have lived in Beijing before the scale of the city still blows me away - everything is built on a monumental scale, from the office blocks to the university campuses. There is so much to do and see throughout the city and surrounding areas, including the Forbidden City, the Great Wall of China and of course the historical hutong streets.

<em>What are your initial thoughts on the Renmin campus and uni life?</em>

I was blown away by the size of the campus and the facilities when I first arrived. There are at least a dozen different cafeterias and restaurants located in the campus, at least half a dozen convenience stores. The library is a great place to find a quiet corner and do some study, as are

the nearby cafes. Most Chinese students say the Renmin campus is quite small compared to others in Beijing, but I still find it amazingly big. University life on campus is great, and it’s easy to make friends with classmates and other students living nearby. The local students are extremely friendly and helpful, and are more than happy to help introduce you to Beijing life. The location of the school is also quite convenient, located nearby the Zhongguancun electronics district, with a subway station making it easy to travel around the city.

<em>How do you find the Renmin Business School and what are the profiles of your new classmates?</em>

The Renmin Business School is very professional and well-organised, with supportive and helpful administrative staff. Professors come from a rich variety of backgrounds and have a great deal of experience in China, which they are more than happy to share with us.

Around half of the students in the IMBA program are from overseas, while the other half are local students from across China. My international classmates come all over the world, including Thailand, Iraq, Georgia, Ireland and Turkey, and have a great variety of work experience. Several have backgrounds working in embassies or consular offices, while others have experience in international trade, mobile communications and textiles.

My Chinese classmates meanwhile have a great deal of experience across fields such as aviation, banking and the military, along with a great deal of knowledge in local markets and business practices.

Renmin University is considered to be one of the top business schools in China, and while many Australians may not have heard the name before, the university holds a great reputation domestically. With a growing international reputation and an impressive alumni list, I believe Renmin will be one of the most well-known Chinese universities in coming years.

<em>What advantages are there in coming to Beijing to complete a 2 year IMBA program instead of staying in Australia?</em>

I believe the biggest advantages to studying in Beijing are the networking opportunities and invaluable experience you’ll gain from living in China. Renmin University has a very prestigious reputation in China and is highly regarded by many business leaders, which will be extremely valuable in making future connections and improving your profile in China.

The internship opportunities available within the Renmin IMBA program are also very exciting. As Renmin University is located in the Zhongguancun area, considered to be ‘China’s Silicon Valley’, there are many opportunities to gain experience with some of China’s top IT companies.

A final advantage is of course the language-learning opportunities. Immersion is one of the best ways to improve your language skills, and I look forward to improving my Mandarin and exploring all that China has to offer.

<em>What do you plan to do upon completion of the Renmin IMBA?</em>

Upon completion of my Renmin University IMBA studies I plan on returning to Australia and starting my own business dealing closely with China. I definitely see my future involving China in some way, and believe the experience and connections I will gain during this program will be invaluable in my future working life.
<h4>Contact</h4>
Further information and other Australia-China scholarship opportunities can be found on the ACYA website.

For enquiries regarding the <a href="http://www.acya.org.au/education/scholarships/acya-ruc-imba-scholarship/">ACYA-RBS International MBA Scholarship</a>, please contact the National Education Director:

Michael Twycross
<a href="mailto:education@acya.org.au">education@acya.org.au</a>