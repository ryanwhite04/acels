{
  "title": "Are Domestic Food Security Concerns the Primary Motivation for Chinese Companies to ‘Go Out’ and Invest in Agricultural Projects Abroad?",
  "author": "ACYA",
  "date": "2014-10-11T19:11:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="page" title="Page 33">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#justin">Justin Steele (温海林)</a></h2>
</div>
</div>
<div class="layoutArea">
<div class="column">
<h3><strong>Introduction</strong></h3>
Recent media attention in Australia towards Chinese investment in agricultural projects has re-ignited the broader issue of food security, and brought into question the motivations of Chinese private companies and state-owned enterprises (SOEs) operating in Australia. The trend for Chinese companies to ‘go out’ (<span style="font-style: italic;">zou chuqu </span>走出去) and invest abroad is not exclusively an Australian issue, with countries in Africa, Latin America and elsewhere all experiencing an increased Chinese presence in their agricultural sectors. Amidst these reports, there is undeniable sense of fear that China is ‘buying up the farm’, with politicians warning that Australia risks becoming “one big vegetable patch for Beijing”.<a href="#f1"><sup>1</sup></a>

This essay seeks to critically analyse the proposition that China is buying up the farm – that is, investing in foreign agricultural industries to guarantee adequate food supplies to import back to China. Several alternative explanations for Chinese investment will be provided. These hypotheses will then be tested against the data available on Chinese agriculture investments in Africa, Latin America and Australia. Data has been extensively compiled from international and Chinese news agencies, Australian media resources and academic literature. This essay hopes to shed light upon what factors motivate Chinese investments in overseas agricultural projects.
<h3><strong>China’s Food Security Policy</strong></h3>
Food security concerns have always weighed heavily on the minds of China’s leaders due to the social instability that food shortages or volatile prices can create. Food security can be defined as, “when all people at all times have access to sufficient, safe, nutritious food to maintain a healthy and active life”.<a href="#f2"><sup>2</sup></a> Underpinning this definition is the concept that food security includes both physical and economic access to food, and meets people’s dietary needs as well as their food preferences. China faces substantial challenges to meeting this definition, containing 20 per cent of the world’s population but only nine per cent of global arable land and six per cent of the fresh water supply.<a href="#f3"><sup>3</sup></a> Nonetheless, China insists upon a policy of 95 per cent food self-sufficiency, a target that is increasingly under pressure. In 2011, China fell short of its grain target by importing over ten per cent of its national grain requirements, amounting to 61 million tons.<a href="#f4"><sup>4</sup></a> To ensure adequate domestic supply, the Chinese government has also stipulated that the total area of arable land in China must remain above 120 million hectares.<a href="#f5"><sup>5</sup></a> This target reflects the Chinese government’s focus on food self-sufficiency as the measure of food security, as opposed to the WHO’s broader definition. But this target is also under threat, with just 121.7 million hectares of arable land available in August 2011, according to statistics from the Ministry of Land and Resources.<a href="#f6"><sup>6</sup></a> As China draws closer to this ‘redline’ and grain imports rise to ominous levels, many commentators see increased overseas agricultural investment by Chinese companies and SOEs as inevitable. Indeed, there is some evidence to suggest that the Chinese government is changing its stance on food security in favour of greater efforts to ‘go out’ and invest in farming abroad.

</div>
</div>
</div>
<div class="page" title="Page 34">
<div class="layoutArea">
<div class="column">

In May 2008, it was reported that the Ministry of Agriculture was formulating a policy on outward investment in agriculture that extended beyond the ‘Go Out Policy’ that China had been pursuing for the past decade.<a href="#f7"><sup>7</sup></a> While the Ministry moved swiftly to deny these reports, statements by other Chinese officials suggest that government is encouraging increased investment by Chinese enterprises in foreign agriculture. However, these statements should be balanced against the overriding state policy of food self-sufficiency, which was re-iterated by Vice-Premier Hui Liangyu 回良玉 in March 2011.<a href="#f8"><sup>8</sup></a>

In 2011, both Premier Wen Jiabao 温家宝 and Minister of Commerce Chen Deming 陈德铭 stated that China will prioritise investment in agriculture (as well as certain other sectors) in respective visits to Argentina and Africa.<a href="#f9"><sup>9</sup></a> Chinese companies have also been publicly encouraged to explore agricultural opportunities in Africa by officials within the Ministry of Foreign Affairs and the Ministry of Commerce, and been advised to seek farmland abroad by officials in the Agriculture and Rural Affairs Committee of the National People’s Congress.<a href="#f10"><sup>10</sup></a> These statements combined reflect an attempt by the Chinese government to raise the profile of agriculture as a target for Chinese foreign investment under the auspices of the Go Out Policy.

However, even before such public endorsements of Chinese investment in foreign agriculture, Chinese enterprises have demonstrated a strong interest in overseas agricultural projects. The 2010 Statistical Bulletin of Outward Foreign Direct Investment reveals that the Chinese outward Foreign Direct Investment (FDI) stock in the combined sectors of agriculture, forestry, animal husbandry and fishing more than quadrupled between 2005 and 2010, to a total of US$2.61 billion. This rate of increase is virtually identical to that seen in mining, an industry that has perhaps received far greater attention from business circles and media over the past few years due to its significantly larger size in absolute terms.

Finally, in July 2012, the Ministry of Commerce included agriculture in its list of industries entitled to subsidies for investment overseas. Although such subsidies had been in place since 2005, this was the first time agriculture had been included.<a href="#f11"><sup>11</sup></a>

These public statements, subsidy incentives and FDI statistics together show a growing impetus for Chinese enterprises to make agricultural investments abroad. However, there is a disjoint between increased investment by Chinese companies in overseas agriculture projects and a Chinese policy of guaranteeing food security through foreign acquisitions. Indeed, there are four alternative explanations that can be raised in response to this argument.

</div>
</div>
</div>
<div class="page" title="Page 35">
<div class="layoutArea">
<div class="column">
<h3><strong>Explaining Chinese Overseas Agricultural Investment</strong></h3>
There are four alternative explanations for why the Chinese government is investing in foreign agriculture. The first is that Chinese investment in overseas agriculture is a commercial response to capitalise upon China’s massive foreign currency reserves. As recently stated in the <span style="font-style: italic;">Australian Financial Review</span>, “China has an understandable interest in diversifying its substantial holdings of foreign exchange reserves away from low-yielding US Treasuries to real productive assets with higher returns”.<a href="#f12"><sup>12</sup></a> Increasingly, agriculture is seen as an industry to target for these higher returns.

The second explanation argues that outward investment by Chinese enterprises in agriculture is undertaken to participate in the transfer and exchange of knowledge, technology and management skills.

The third alternative is that Chinese agricultural investments are motivated by a desire to increase the international food supply and thus reduce international food prices. The Ministry of Commerce has advanced this argument; with an official stating that “Chinese enterprises’ overseas expansion can reduce global grain prices by increasing output”.<a href="#f13"><sup>13</sup></a>

A fourth possibility is that such Chinese acquisitions are motivated by the desirability of acquiring foreign brands, which can then be used both internationally and in the domestic market, where ‘foreignness’ attracts greater consumer attention and a price premium.

In summary, the possible motivations for Chinese agricultural investments abroad are:
<ol>
 	<li>Domestic food security;</li>
 	<li>Investing China’s foreign currency reserves in productive assets;</li>
 	<li>Transfer and exchange of knowledge, technology and management skills;</li>
 	<li>Increasing international food supply and decreasing prices; and,</li>
 	<li>Acquiring foreign brands and associated commercial reasons.</li>
</ol>
The veracity of these explanations will now be evaluated based upon the data available on Chinese investments in agriculture projects in Africa, Latin America and Australia. These locations have been chosen because they represent South-South investment and trade flows, and have all received significant media and political attention.
<h3><strong>Motivations for Chinese Agricultural Investments in Africa</strong></h3>
Analysing Chinese investment patterns in African agriculture is relatively straightforward, as already several academics have written numerous papers on the issue.<a href="#f14"><sup>14</sup></a> Chinese investment in African agriculture was worth more than US$134 million in 2009, and involved the establishment of 50 agricultural enterprises and over 100 farms.<a href="#f15"><sup>15</sup></a> In discussing China’s agricultural investments in Africa, it is important to note that aid projects constitute a significant proportion of Chinese activity in this area, and while strictly speaking aid and investment are separate concepts, this essay will discuss both.

</div>
</div>
</div>
<div class="page" title="Page 36">
<div class="layoutArea">
<div class="column">

Generally speaking, Chinese involvement in African agriculture takes one of three forms:
<ul>
 	<li>Agricultural aid projects</li>
 	<li>Investment by SOEs</li>
 	<li>Investment by (mostly small-scale) private enterprises.</li>
</ul>
<span style="text-decoration: underline;">Agricultural Aid Projects</span>
China has been providing aid to Africa since 1956, and agricultural aid programs continue to constitute a substantial element of China-Africa agricultural cooperation. In fact, since the Forum on China-Africa Cooperation in 2006, China has adjusted its aid policy in Africa such that agriculture and food issues are now the top priority.<sup><a href="#f16">16</a> </sup>By 2009, China had completed 142 agricultural aid projects in over 44 African countries. These projects are generally small-scale, with an average investment of just US$500,000.<a href="#f17"><sup>17</sup></a> The projects are mostly ‘turn-key’, in that they are built by China and then handed over in working order.<a href="#f18"><sup>18</sup></a> The purpose of many such ventures appears to be improving grain production and storage.

From 2009, Chinese aid in African agriculture has begun to adopt a public-private model, with the establishment of at least 14 new agro-technical demonstration centres.<a href="#f19"><sup>19</sup></a> These centres offer training in areas such as cultivating rice and vegetables, and using agricultural machinery. Whilst some centres focus on producing agricultural products that China is increasingly seeking to import (e.g. soybeans and corn), most address a wide-range of African agricultural needs. Several of the centres are working towards introducing high-yield hybrid rice to Africa, a Chinese-designed technology.

<span style="text-decoration: underline;">Commercial Investment</span>
Investment by SOEs, such as the China State Farms Agribusiness Corporation (CFSAC), has been present in Africa since 1990. But in recent years investment channels have been diversified, from simple aid projects to full-scale FDI. CFSAC’s provincial subsidiaries, such as the Jiangsu State Farm Agribusiness Corporation, began their African undertakings by forming ‘friendship farms’, such as the China-Zambia friendship farm in 1990.<a href="#f20"><sup>20</sup></a> Other SOEs, originally assigned to implement or construct aid projects, then took stakes in the finished products.

By 2002, the Go Out Policy regarding African agricultural investment had become official, with the Vice-Minister of Foreign Aid stating, ‘China-Africa agricultural cooperation in the new century must be conducted by enterprises and should be market-orientated.”<a href="#f21"><sup>21</sup></a> Sun observes that by 2005, the Go Out Policy had resulted in a new group of small-scale Chinese private enterprises arriving in Africa.

Brautigam et al<span style="font-style: italic;">. </span>note that by 2006, there were more than 20 SOEs and private Chinese farms in Zambia, all of which were producing for the Zambian domestic market. They argue that in Africa generally the “evidence is indeed overwhelming, that at present, Chinese farms in Africa are producing almost exclusively for local sales...or for export to global markets”.<a href="#f22"><sup>22</sup></a> This seems to suggest that Chinese FDI in Africa is not motivated by domestic food security concerns.

</div>
</div>
</div>
<div class="page" title="Page 37">
<div class="layoutArea">
<div class="column">

<span style="text-decoration: underline;">Summary of Motivations</span>
Diplomacy appears to be a major motivating factor for Chinese agricultural investment in Africa, as the African continent has been a net food importer for almost 40 years, and agricultural assistance has generally always been welcome. Brautigam and Tang also note that expanding the global food supply also benefits China, which became a net food importer in 2003. In returning to our original five hypotheses, it seems China’s presence in African agriculture can largely be explained by explanations number three and four; for the transfer and exchange of knowledge, technology and management skills, and to increase international food supply and reduce prices. The second hypothesis is also a possible explanation; with the establishment in 2007 of the China-Africa Development Fund reflecting Chinese attempts to find appropriate investment targets for its foreign currency holdings in order to reduce reliance on US treasury bonds.

There is little evidence to suggest that China is buying up agricultural land in Africa to simply import food back to China, with Ministry of Agriculture officials in 2008 lambasting such claims as “not realistic” given “the costs and risks are so high” and “there are so many hungry people in Africa”.<a href="#f23"><sup>23</sup></a> Brautigam et al. agree, stating that Chinese dealings in Africa have been “driven by commercial interests rather than food security”. However, it seems unlikely that investment in African agriculture is for commercial reasons alone, with the manager of CSFAC stating in 2010 that the “unstable political situation” is the “greatest challenge for Chinese companies which want to invest in Africa”.<a href="#f24"><sup>24</sup></a>
<h3><strong>Motivations for Chinese Agricultural Investments in Latin America</strong></h3>
Chinese investment in Latin American agriculture has taken a much narrower focus than in Africa, almost exclusively targeting one agricultural commodity: soybeans. This reflects voracious demand from Chinese consumers for soybean food products, as well as demand by China’s agricultural enterprises for soybean livestock feed. Soybean import tariffs were cut from 114 per cent to three per cent in 2002,<a href="#f25"><sup>25</sup></a> leading to a massive increase in imports, from practically zero ten years ago to 52.6 million tons in 2011.<a href="#f26"><sup>26</sup></a> And with North and Latin America providing 76 per cent of China’s imported soybeans, it is no wonder that Chinese investments in Latin American agriculture have focused so heavily on soybean production.<a href="#f27"><sup>27</sup></a>

Seven of the eight reported large-scale investments by Chinese enterprises in Latin American agriculture over the past three years have been in soybean production. Three were in Argentina and four in Brazil. Almost without exception, the investments have been made by large provincial SOEs, with Heilongjiang’s Beidahuang Nongken Group and Chongqing Grain Corp (CGC) each undertaking two investment projects, and Zhejiang’s Fudi Agriculture Group investing in a joint soybean farming venture in Brazil. Sanhe Hopefull Grain &amp; Oil Group, one of the few private Chinese agribusinesses in Latin America, has announced the biggest deal to date: a US$7.5 billion investment in soybean production, storage and transport facilities in the Brazilian state of Goiás. In return, Sanhe will be receiving an annual supply of six million tons of soybeans, 80 per cent of Goiás’ annual soybean production. Beidahuang’s soybean projects, both in Argentina, are also substantial and each valued at US$1.5 billion. One project is exclusively soybean production and the other is mixed purpose, including wheat, soybean, corn, fruit, vegetables and wine. The mixed purpose project involves developing 300,000 hectares of land and expanding a local port. To ensure that local demand is satiated and local food price inflation is avoided, Beidahuang is only able to export produce surplus to Argentinian national requirements, a good-faith demonstration of responsible investment by the Chinese side. CGC’s soybean project in Brazil also involves a significant infrastructure element, with their US$500 million investment to focus on developing processing, warehousing and logistical capacities to establish a soybean industrial base in Bahia state.

</div>
</div>
</div>
<div class="page" title="Page 38">
<div class="layoutArea">
<div class="column">

Chinese domestic demand for grains, particularly soybeans, is the main motivating factor behind agricultural investment in Latin America. However, none of the major projects surveyed involve Chinese companies simply buying up land and shipping produce back to China. All are based upon increasing production capacity and improving local infrastructure to guarantee supply and quality of the produce for both local and Chinese markets. Improving supply channels are important for Chinese enterprises, with CGC’s president stating that soybean importers save around 20 per cent of their profits if they purchase directly from producers, rather than one of the four major international grain dealers.<a href="#f28"><sup>28</sup></a> Returning to the original five hypotheses, it seems that Chinese agricultural investment in Latin America is chiefly motivated by reasons four and five; to increase international food supply and reduce prices, and for commercial reasons such as higher profit margins on imports.
<h3><strong>Motivations for Chinese Agricultural Investments in Australia</strong></h3>
Data collected by KPMG and the University of Sydney China Studies Centre estimates Chinese agricultural investment in Australia between September 2006 and June 2012 at approximately A$567 million, with nine-tenths of that amount flowing into New South Wales.<a href="#f29"><sup>29</sup></a> Unlike Latin America, Chinese investment in Australian agricultural projects has taken a variety of forms, such as share-market takeovers, capital investments, private land purchases and tenders for new agricultural schemes. Investment has also been targeted towards a range of agricultural products, such as cotton, sugar, wine and dairy. As such, it is unlikely that any one of the five hypotheses will alone constitute the sole reason for Chinese investments in Australia’s agricultural industry. Indeed, examining several recent Chinese deals there is evidence of multiple motivating factors.

For instance, the most recent Chinese FDI in Australian agriculture was the purchase of Cubbie Station by a consortium led by Shandong Ruyi Group, and was undertaken for commercial reasons and to increase cotton supply on the international market. In any case, it is clearly not for food security reasons: one cannot eat cotton, and there are no plans to produce other commodities on site. Another potential deal relates to the announcement in September 2012 that China’s sovereign wealth fund, the China Investment Corporation (CIC), is in negotiations to invest A$200 million in Tasmanian dairy farms to double the state’s production capacity. This seems to be the first sign that the CIC is interested in agricultural investments. The significance of this potential deal is that the CIC was established in 2007 with the explicit mandate to mitigate risks posed by China’s enormous foreign exchange reserves by investing in commercial opportunities abroad.

China’s largest agricultural SOE, the China National Cereals, Oils and Foodstuffs Corporation (COFCO), has also made a move into Australia by acquiring Tully Sugar in May 2011.<a href="#f30"><sup>30</sup></a> Based on statements made by COFCO’s Australia president, food security is a major motivator for investment decisions made by the SOE. However, as with the majority of Chinese investment in foreign agriculture, the chief objective is to expand production for commercial reasons. In this case, that means capturing “the above-average demand forecast in south-east Asia” for sugar. The acquisition was also motivated by the opportunity to transfer knowledge and management skills, with Tully Sugar’s Australian management team keeping their positions and COFCO expressing a keenness “to learn the Australian way of doing things” so as to boost efficiency the of its sugar-cane mills in China.<a href="#f31"><sup>31</sup></a>

</div>
</div>
</div>
<div class="page" title="Page 39">
<div class="layoutArea">
<div class="column">

As a final example, the 2011 acquisition of Manassen Foods by Shanghai-based Bright Food Group at a cost of approximately A$400 million can be seen as a case of Chinese investment for the purpose of acquiring foreign brands. Manassen’s products include Sunbeam, Simon Johnson and Golden Days. Moreover, Bright Food’s vice-president made it clear that the company intends to market these products heavily in the Chinese domestic market, where foreign brands attract a price premium due to consumer perceptions of higher safety standards.
<h3><strong>Conclusion</strong></h3>
Based on Chinese investment patterns in agricultural projects in Africa, Latin America and Australia, it seems clear that the proposition that domestic food security concerns are the primary motivation for Chinese companies to ‘Go Out’ and invest abroad is too simplistic. Evidence suggests that Chinese investment in overseas agricultural projects is motivated by a range of factors, not an overriding mission to ‘buy up the farm’ and ship produce back to China en masse.

In Africa, investment is motivated by the opportunity to transfer and exchange knowledge, technology and management skills, and to increase international food supplies and thus reduce international prices. In Latin America, agricultural investment is also designed to increase international food supplies, as well as for commercial reasons such as high import profit margins. In Australia, Chinese agricultural investment reflects the whole range of motivating factors, from acquiring foreign brands, to exchanging knowledge and management skills, to finding sound investment targets for China’s foreign exchange reserves, to increasing production capacities and thus supplies of key agricultural commodities on the international market.

Chinese enterprises have demonstrated that they are willing to work within the commercial and legal parameters established by host governments in the countries they choose to invest in. It is up to every country to determine what level of access to agricultural investments they wish to allow foreign enterprises to have. For Australia, any stance that restricts access to foreign capital is unlikely to be in the best long-term national interest, given that further investment in the agricultural sector could generate an extra A$1.7 trillion for the economy by 2050.<a href="#f32"><sup>32</sup></a>

Finally, there needs to be greater awareness that despite increased Chinese investment in overseas agriculture, there has been no shift in China’s fundamental food security policy. Beijing still has its ambitious target for 95 per cent grain self-sufficiency, and has been ploughing record amounts of investment into domestic agriculture.<a href="#f33"><sup>33</sup></a> Food security will continue to be a significant concern for China’s leaders. However, the notion that China is simply off-shoring agricultural production is both erroneous and obstructive.

<hr />

<h3><strong>Footnotes</strong></h3>
<a id="f1"></a>1 Crowe, David (2012), ‘Barnaby Joyce Digs in on Cubbie as Coalition Fractures over Foreign Deals’, <span style="font-style: italic;">The Australian, </span>5 September, http://www.theaustralian.com.au/national-affairs/barnaby-joyce-digs-in-on-cubbie-as-coalition-fractures-over-foreign-deals/story- fn59niix-1226465146114#.

<a id="f2"></a>2 World Health Organisation (2013), ‘Food Security’, http://www.who.int/trade/glossary/story028/en/.

<a id="f3"></a>3 Zuo, Changsheng (2010), ‘Developments and patterns of trade in agricultural commodities between China and Latin America’, http://www.fao.org/fileadmin/templates/tci/pdf/presentations/Zuo_Changshen_-Developments_and_patterns_of_trade.pdf.

<a id="f4"></a>4 Pumin, Yin (2012), ‘Feeding a Populous Country’, <span style="font-style: italic;">Beijing Review, </span>27 April, http://www.bjreview.com.cn/nation/txt/2012- 04/27/content_449624.htm.

<a id="f5"></a>5 Affirmed during the State Council’s 149th Standing Conference, 6 September 2006.
<div class="page" title="Page 34">
<div class="layoutArea">
<div class="column">

<a id="f6"></a>6 Pumin, op cit, 4.

<a id="f7"></a>7 Anderlin, Jamil (2008), ‘China Eyes Overseas Land in Food Push’, <span style="font-style: italic;">Financial Times, </span>8 May, http://www.ft.com/cms/s/0/cb8a989a- 1d2a-11dd-82ae-000077b07658.html.

<a id="f8"></a>8 Xinhua News Agency (2011), ‘China Upholds Policy of Food Self-Sufficiency: Chinese Vice Premier’, <span style="font-style: italic;">Xinhuanet</span>, 25 March, http://news.xinhuanet.com/english2010/china/2011-03/25/c_13798399.htm.

<a id="f9"></a>9 Chi, Jianxin (2011), ‘China assists Africa with food security’, <span style="font-style: italic;">China Daily, </span>2 December, http://english.peopledaily.com.cn/90883/7663863.html.

<a id="f10"></a>10 Jin, Zhu (2011), ‘Feeding China’, <span style="font-style: italic;">China Daily, </span>4 December, http://usa.chinadaily.com.cn/china/2011- 12/04/content_14209416.htm.

<a id="f11"></a>11 Zhao, Bin Chen (2012), <span style="font-style: italic;">Jinnian qiye jingwai touzi daikuan tiexi shenbao kaishi </span>今年企业境外投资贷款贴息申报开始 [Reporting begins for this year’s interest payments on loans for overseas enterprise investment], <span style="font-style: italic;">Caixin, </span>19 July, http://economy.caixin.com/2012-07-19/100412942.html.

</div>
</div>
</div>
<a id="f12"></a>12 Tyson, Laura (2012), ‘The Benefits of Chinese FDI’, <span style="font-style: italic;">Project Syndicate</span>, http://www.project-syndicate.org/commentary/the- benefits-of-chinese-fdi-by-laura-tyson.

<a id="f13"></a>13 Jin, op cit, 10.

<a id="f14"></a>14 Sun, Helen Lei (2011), ‘Understanding China’s Agricultural Investments in Africa’, <span style="font-style: italic;">South African Institute of International Affairs Occasional Paper, </span>p. 102; Brautigam, Deborah and Xiaoyang, Tang (2009), ‘China’s Engagement in African Agriculture: Down to the Countryside’, <span style="font-style: italic;">The China Quarterly, </span>No. 199, pp. 686-706.

<a id="f15"></a>15 Sun, Ibid, p. 9.

<a id="f16"></a>16 Ibid, p. 7.

<a id="f17"></a>17 Ibid, p. 16.

<a id="f18"></a>18 Brautigam and Xiaoyang, op cit, 14, p. 686.

<a id="f19"></a>19 Ibid.

<a id="f20"></a>20 Ibid, p. 691.

<a id="f21"></a>21 Ibid, p. 693.

<a id="f22"></a>22 Ibid, p. 698.

<a id="f23"></a>23 Marks, Stephen (2008), ‘China and the great global land grab’, <span style="font-style: italic;">Pambazuka News, </span>11 December 11, http://www.pambazuka.org/en/category/africa_china/52635.

<a id="f24"></a>24 Zhu, Jin (2010), ‘China, Africa forge farming ties’, <span style="font-style: italic;">China Daily, </span>12 August, http://english.peopledaily.com.cn/90001/90776/90883/7101891.html.

<a id="f25"></a>25 Freeman, Duncan, et al (2008), ‘China’s Foreign Farming Policy: Can Land Provide Security?’, <span style="font-style: italic;">Brussels Institute of Contemporary China Studies Policy Paper, </span>Vol. 3(9), p. 6.

<a id="f26"></a>26 Pumin, op cit, 4.

<a id="f27"></a>27 Freeman, op cit, 25, p. 7.

<a id="f28"></a>28 China Economic Net (2011), ‘CGC is setting up a soybean base in Brazil’, 24 November, http://china-wire.org/?p=17240.

<a id="f29"></a>29 KPMG (2012), ‘Demystifying Chinese Investment: China’s Outbound Direct Investment in Australia’, http://www.kpmg.com/au/en/issuesandinsights/articlespublications/china-insights/pages/demystifying-chinese-investment.aspx, p. 11.

<a id="f30"></a>30 Cranston, Matthew (2011), ‘China canes rivals, takes 61pc of Tully’, <span style="font-style: italic;">Australian Financial Review, </span>6 July, http://www.afr.com/p/business/companies/china_canes_rivals_takes_pc_of_tully_AqprJWd61rslxw1qQDSJfK.

<a id="f31"></a>31 Sprague, Julie-Anne (2011), “COFCO works to seal the deal”, <span style="font-style: italic;">Australian Financial Review, </span>7 June 7, http://www.afr.com/p/business/companies/cofco_works_to_seal_the_deal_P5JUwkXBFZe2mEGVn07z7J.

<a id="f32"></a>32 Gluyas, Richard (2013), ‘Farm sector targets $500bn funding gap, $1.7 trillion bonanza at stake’, <span style="font-style: italic;">The Australian, </span>26 April, http://www.theaustralian.com.au/business/in-depth/farm-sector-targets-500bn-funding-gap-as-17-trillion-export-bonanza-at- stake/story-fni2wt8c-1226629597610.

<a id="f33"></a>33 Xinhua News Agency (2011), ‘Agriculture Still Vital to China’, <span style="font-style: italic;">Xinhuanet, </span>29 December, http://news.xinhuanet.com/english/china/2011-12/29/c_131332208.htm.

<hr />

<strong> This article appears in the <a href="http://www.acya.org.au/wp-content/uploads/2013/06/ACYA-Journal-of-Australia-China-Affairs-2013.pdf">2013 ACYA Journal of Australia-China Affairs (pp. 33-39)</a>.</strong>

<em id="justin">Justin recently completed his Bachelor of Laws/Bachelor of Asia Pacific Studies (Chinese) at the Australian National University. He has been involved with the Australia-China Youth Association in various capacities since 2009. Justin is currently completing his graduate year at a top-tier commercial law firm in Sydney.</em>

</div>
</div>
</div>