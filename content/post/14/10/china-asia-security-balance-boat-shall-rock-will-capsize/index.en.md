{
  "title": "China and the Asia Security Balance: The Boat Shall Rock, but will Not Capsize",
  "author": "ACYA",
  "date": "2014-10-02T01:30:53+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "china navy",
      "type": "image",
      "src": "china-navy-e1413808476739.png"
    }
  ]
}
<div class="page" title="Page 26">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#kara">Kara Hawkins</a></h2>
<h3>Introduction</h3>
China’s rapid rise in the international system has sparked concern and debate as to future security conditions in the Asia-Pacific region and around the world. The USA has dominated the Asia-Pacific security architecture since the end of the Second World War through its ‘hub-and-spokes’ system of bilateral alliances. The question of whether China is content to preserve the status quo, or is a revisionist power intent on undermining it, has enormous implications for international security and the global economy. Yet much of the debate about whether China is either a status quo or revisionist power overlooks an important distinction: will China fundamentally <span style="font-style: italic;">upset </span>the regional security order or merely seek to <span style="font-style: italic;">alter </span>it. An upset would involve China posing a sudden, direct and aggressive challenge to American regional primacy in the short to medium-term, whereas an alteration would see a change in the distribution of regional power that falls short of a Chinese challenge to replace the USA’s regional primacy. This distinction is vital as perceptions of Chinese intentions and capabilities shape the responses and strategies of interested parties, and consequently Chinese threat perceptions. Thus, mistaken views of Chinese revisionism are likely to result in more threatening counterbalancing strategies that unnerve China and accordingly increase the probability of conflict, the very outcome the international community wishes to avoid.

This paper argues that China will not seek to upset the regional security order due to a number of powerful constraints. Domestically, environmental and demographic factors are placing significant pressure on China’s economy, in turn generating greater potential for social unrest. Militarily, China’s abilities are vastly overestimated, with technology, capabilities and a military structure all far behind those of the USA. Even if China could match America in military might, other regional actors would be unlikely to defer to Chinese predominance, and thus Beijing would not have the same regional support as the USA enjoys for the preservation of its position in the region. This is not to say that China will cease being assertive. On the contrary, Beijing is likely to be assertive to the extent that this strengthens its primary interest of regime security and its other core interests in the retention and reclamation of disputed territories and the projection of regional and global influence. While this assertiveness increases the risk of miscalculations and an unintentional escalation of tensions, China’s primary national interests would be severely damaged by an upset of the regional order. Thus, unless circumstances drastically change such that challenging American primacy would be more likely to maximise core Chinese interests, Beijing will not seek to fundamentally upset the regional status quo.

These considerations are vital to Australia’s foreign policy calculations. The Australian interest is in a stable Asia-Pacific region and avoiding excessive divergence between economic and security ties. The question of whether China has the potential to upset the regional security order is today arguably the most significant debate in Australian policy circles. Professor Hugh White of the Australian National University ignited this debate with his controversial suggestion that the USA should share power with China in Asia, or in the future face the alternatives of either conflict with or the yielding of regional power to Beijing.<a href="#f1"><sup>1</sup></a> At the other end of the spectrum, Paul Dibb argues that China’s abilities are exaggerated, particularly its perceived military threat.<a href="#f2"><sup>2</sup></a> How Australia identifies and responds to China’s rising power will have a profound impact upon national defence preparations and spending. Given China’s lack of capability and incentives to successfully overturn the regional status quo, Australia should acknowledge this by not regarding China as a threat, whilst still recognising that the regional security balance will change. This point is especially pertinent considering the prospect of a conflict in the region stemming from misunderstandings and misperceptions, which are more likely to arise if states view and act as though China is more dangerous and its power more in need of constraint than is actually the case. Thus, in rhetoric and action, Australia must distinguish between reactionary perceptions of a China intending to upset the regional status quo, and the more correct and balanced view that China that will simply alter it. A failure to do so could heighten rather than minimise the prospects of conflict.

</div>
</div>
</div>
<div class="page" title="Page 27">
<div class="layoutArea">
<div class="column">
<h3>The USA and the Asia-Pacific Region</h3>
American domination of the Asia-Pacific security system has had a stabilising effect upon the region, something that continues to benefit China. The presence of the USA in the region has reassured its allies against any threats they may perceive from China, whilst simultaneously ameliorating threats that China itself perceives, particularly from Japan. The USA established a formal security alliance with Tokyo after the Second World War whereby Japan gained an American defence guarantee in exchange for allowing the USA to establish military bases in Japanese territory and agreeing not to rearm.<a href="#f3"><sup>3</sup></a> The military restraint of Japan is particularly critical in Chinese security calculations due to historical Sino-Japanese tensions and previous Japanese invasions and imperialism.<a href="#f4"><sup>4</sup></a> As many others in the region share China’s concerns, a remilitarising Japan could spark a regional arms race. Thus, the USA-Japan alliance has historically largely short- circuited a regional security dilemma that otherwise may have formed.<a href="#f5"><sup>5</sup></a> The American hub-and- spokes security system consists of treaty alliances with South Korea, the Philippines, Australia and Thailand, as well as strong ties with China, India, Malaysia, Singapore, Brunei, Vietnam, Malaysia, Mongolia, New Zealand and Pacific Island nations.<a href="#f6"><sup>6</sup></a> Some argue that the highest levels of regional insecurity since 1945 coincide with the highest levels of uncertainty about American commitment to the region, underlining the stabilising effect of the US presence.<a href="#f7"><sup>7</sup></a> The resulting regional stability not only benefits China form a security perspective, but has also benefited the Chinese economy.

The presence of the USA as a stabilising force in the Asia-Pacific created conditions ripe for Chinese economic growth, and arguably continues to do so. Regional stability has allowed China to reduce its focus on military and defence concerns and concentrate resources on domestic economic development, the key to its current strength. The USA has also protected sea-lanes vital to the region’s growth, directly shielding Chinese trade and resource supplies.<a href="#f8"><sup>8</sup></a> Furthermore, stability has promoted regional development, creating a market for Chinese goods and a source of investment in China. These security and economic benefits have helped lay the foundations of China’s current power, and thereby generate a disincentive for China to upset the regional security balance as the conflict this would likely cause would considerably damage China’s economic and national strength.

</div>
</div>
</div>
<div class="page" title="Page 28">
<div class="layoutArea">
<div class="column">
<h3>China’s Domestic Situation</h3>
Even so, China’s ability to transform its economic wealth into military power and regional influence is strongly constrained by domestic issues.<a href="#f9"><sup>9</sup></a> China faces major environmental challenges that threaten to hold back its economy. A study by the Chinese Academy of Environmental Planning found that the cost of environmental degradation tripled between 2004 and 2010 to 3.5 per cent of China’s Gross National Product, or approximately $230 billion.<a href="#f10"><sup>10</sup></a> Whilst the official Chinese estimate could be significantly understated, even this figure indicates that environmental factors are a major economic burden and amplify the structural challenges associated with China’s slowing economic growth.<a href="#f11"><sup>11</sup></a> One of these concerns has been increasing unemployment – partially due to structural misalignment – creating substantial potential for social unrest.<a href="#f12"><sup>12</sup></a> China’s low and decreasing birth rate will further reduce number of young and inexpensive labourers who have underpinned China’s economic growth over the last three decades.<a href="#f13"><sup>13</sup></a> Additionally, China’s population is aging rapidly and the aged care dependency ratio is estimated to double from 16 retirees for every 100 working-age adults now to 32 in 2023, and again to 61 by 2050.<a href="#f14"><sup>14</sup></a> These demographic changes will redirect funds from potential production and investment towards health and social costs, compounding the threat of economic stagnation.<a href="#f15"><sup>15</sup></a> As the economy is the primary foundation Chinese Communist Party (CCP) legitimacy, such factors pose a major threat to regime stability and the Chinese political model. While China is likely to continue its assertive behaviour and play an increasing role in the international arena, given the plethora of domestic issues the CCP is facing, it can be expected to retain and probably increase its focus on internal issues. The primary rationale for such domestic emphasis is that these potentially destabilising factors could threaten the CCP regime itself and thus limit China’s power projection capabilities to a far greater extent.

Moreover, China’s ability to effectively challenge the USA as regional military hegemon is questionable. But many observers express concern over Beijing’s rapid military modernisation.<a href="#f16"><sup>16</sup></a> The People’s Liberation Army (PLA) has 2.3 million active-duty personnel, and China’s military budget has been steadily increasing.<a href="#f17"><sup>17</sup></a> Furthermore, China recently deployed the world’s first anti- ship ballistic missile and purchased its first aircraft carrier and stealth fighter jets.<a href="#f18"><sup>18</sup></a> Nevertheless, further analysis shows that China does not in fact pose a severe military threat to American primacy in the region. China borders fourteen different countries, and patrolling these potential hotspots (such as the Indian and Burmese borders) consumes a large portion of military resources.<a href="#f19"><sup>19</sup></a> More significantly, China’s military is unprepared for such a major undertaking as conflict with the US. There is little coordination between military branches, and military hierarchies are undermined by the prevalence of ‘selling’ senior positions.<a href="#f20"><sup>20</sup></a> Corruption has become “institutionalised”, making it “much more difficult to develop, produce and field the weapons systems required to achieve world- class power projection.”<a href="#f21"><sup>21</sup></a> While China is rapidly building up its military, increasing military spending by 7.8 per cent in 2012 alone,<a href="#f22"><sup>22</sup></a> “the United States’ overwhelming lead is not going to change in a hurry”.<a href="#f23"><sup>23</sup></a> Although Beijing’s military expenditure of US$166 billion (9.5% of the world total) is the second highest in the world, it is still a long way behind American expenditures of US$682 billion (39% of the world total).<a href="#f24"><sup>24</sup></a> In addition to deficient military capabilities, the PLA has had little experience of modern warfare.<a href="#f25"><sup>25</sup></a> This reduces the likelihood of China being able to successfully challenge America’s regional primacy and shift the regional security balance in its favour.

</div>
</div>
</div>
<div class="page" title="Page 29">
<div class="layoutArea">
<div class="column">
<h3>China in the Asia-Pacific</h3>
Even though increasing Chinese military capabilities will inherently challenge US influence in the region, in order to truly achieve regional dominance Beijing requires the deference of regional actors to its strategic positions.<a href="#f26"><sup>26</sup></a> However, most are wary of Chinese power and concerned about its potential revisionist aims.<a href="#f27"><sup>27</sup></a> Many Asian states and regional powers such as Russia, India and Japan may actively limit or balance against unwelcome Chinese attempts at regional primacy. This would likely involve such states aligning more closely with the USA, potentially alienating themselves from their current position as key Chinese trading partners, upon which Beijing’s economy depends heavily.<a href="#f28"><sup>28</sup></a> The stability of the USA-dominated regional security system is partially responsible for allaying fears of Chinese revisionism, but a resurgent China is likely to destabilise the region. Already China’s assertiveness is unsettling some states such as Vietnam and the Philippines, potentially pushing them towards closer relations with the USA.<a href="#f29"><sup>29</sup></a> Thus, other powers and neighbouring states will most likely inhibit Chinese efforts to overturn the regional balance.

Consequently, a vital issue when considering China’s potential to upset the regional security balance is whether it is in China’s national interest to do so. The CCP’s primary objective is regime security, requiring the continuance of economic growth and development, as well as political stability. President Xi Jinping 习近平 recently emphasised that the PLA’s first priority is to defend the CCP regime rather than China itself.<a href="#f30"><sup>30</sup></a> Other fundamental national interests include projecting regional and global influence, as well as retaining and reclaiming disputed territories. But regional and global power ambitions are contingent on the resolution of domestic issues and political stability, which are heavily reliant on China’s continued economic growth and regional security.

</div>
</div>
</div>
<div class="page" title="Page 30">
<div class="layoutArea">
<div class="column">

Given that the USA-dominated security system has fostered these key foundations, attempts to upset this balance would undermine and severely damage the CCP’s legitimacy, as well as China’s ability to defend its core interests and project its influence into the future.

China’s regime legitimacy has been underpinned by strong and continuous economic growth that has been supported by greater integration into the international economic system.<a href="#f31"><sup>31</sup></a> China has forged ties with industrialised nations and benefited from their markets, technology and investment.<a href="#f32"><sup>32</sup></a> America’s stabilising regional presence has assisted regional actors in becoming larger markets for Chinese products and allowed Chinese leaders to focus on economic as well as security concerns. Conflict with the USA would damage these economic ties and disrupt the regional stability that the Chinese economy so requires. Despite China’s potential desire to aggressively pursue regional hegemony, it continues to be dependent on its greatest competitors.<a href="#f33"><sup>33</sup></a> The USA and Japan were respectively China’s second and fifth largest trading partners in 2011.<a href="#f34"><sup>34</sup></a> China has also become increasingly reliant on the region for its natural resources. For example, it imports Malaysian palm oil, Thai rubber, Philippine copper, Australian iron ore, and Indonesian lumber and paper.<a href="#f35"><sup>35</sup></a> China is also in the process of replacing oil with natural gas as its major energy supply, but it is predicted that by 2025 40 per cent of natural gas will have to be imported, mostly from South East Asia.<a href="#f36"><sup>36</sup></a> China and other regional actors are so economically intertwined that it is arguably in all of their interests to maintain the status quo.<a href="#f37"><sup>37</sup></a> The high level of integration and dependence upon regional and international actors for economic security illustrates how conflict that would disrupt such links would undermine China’s economic strength, CCP regime legitimacy and its ability to maintain present levels of influence.

Nevertheless, realists argue that China’s growth is inherently destabilising,<a href="#f38"><sup>38</sup></a> and power transition theorists predict that conflict will inevitably result from the existence of a rapidly rising power.<a href="#f39"><sup>39</sup></a> Notwithstanding China’s aforementioned inability and unwillingness to cause such a conflict, as China’s power grows it is likely to continue its assertiveness in regional affairs. China is involved in territorial disputes that have recently resurfaced with Japan, India, the Philippines, Vietnam and Malaysia. Eight Chinese ships crossed into the waters of the Japanese-controlled Senkaku/Diaoyu Islands on a single day in April, one of many incidents of Chinese ships and planes entering disputed territory.<a href="#f40"><sup>40</sup></a> China has intensified disputes in the South China Sea, such as by sending a passenger ship to the Paracel Islands, which a number of South East Asian nations also lay claim to.<a href="#f41"><sup>41</sup></a> On another front, Sino-Indian tensions increased when Chinese soldiers were found camped 19 kilometres beyond their side of the bilateral ‘line of actual control’.<a href="#f42"><sup>42</sup></a> Although some argue that such assertiveness is largely the result of internal politics related to the recent leadership change,<a href="#f43"><sup>43</sup></a> others argue that this is part of a longer-term trend of increased Chinese assertiveness that began in 2009.<a href="#f44"><sup>44</sup></a>

</div>
</div>
</div>
<div class="page" title="Page 31">
<div class="layoutArea">
<div class="column">
<h3>Potential for Conflict</h3>
Such behaviour may relate to connections between national interests and regional assertiveness, which are particularly relevant with regards to sea-lanes and energy security. Whilst American presence has helped prevent sea-lane disruptions, escalating Sino-American strategic and economic competition means China has become concerned that the USA could use its sea control as an instrument of economic coercion, or block Chinese access in the case of a military or economic crisis.<a href="#f45"><sup>45</sup></a> This issue is of growing significance due to China’s increasing dependence on imported energy, most of which must pass through two key regional trading bottlenecks, the Strait of Malacca and the Strait of Hormuz.<a href="#f46"><sup>46</sup></a> Thus China is likely to build its naval capacities for the purpose of sea denial and perhaps sea control, as well as secure energy import suppliers and expand trade more generally. Nonetheless, a more assertive China does not automatically imply a more aggressive China.<a href="#f47"><sup>47</sup></a> Beijing is likely to push its territorial, security and economic interests, but this does not necessarily equate to an intention to overturn the regional security system.

However, increasing Chinese assertiveness and naval activity carries higher risks of miscalculation and misperception, generating the potential for China to accidentally or unintentionally upset the regional balance. China’s rising economic and military power is likely to cause it to “push against US power to find the boundaries of the United States’ will.”<a href="#f48"><sup>48</sup></a> Internal politics and pressures will play a crucial role in potential miscalculations and misperceptions regarding the actions of other regional actors. Some suggest that Xi may have “exploited or even orchestrated”<sup>49</sup> the recent tensions with Japan over the Senkaku/Diaoyu Islands in order to discover which military generals he could trust. This is a crucial element in a political system where political power is so strongly tied to control over the military, but also contains the potential to permit the escalation of tense strategic situations towards conflict.<a href="#f50"><sup>50</sup></a> Another basis for misperceptions are Chinese naval developments, which, while most likely intended for energy security or sea denial, could also be perceived by regional actors as preparations to challenge American dominance. Thus, whilst it is unlikely that the CCP is intending to upset the regional security balance in the short to medium- term, miscalculations and misperceptions may intercede.

Additionally, there is a risk that Chinese foreign policy could become even more assertive and potentially confrontational under the influence of nationalist forces. Chinese nationalism is driven by a deep sense of humiliation at past imperialist domination, and often drives certain political and military actors to call for more aggressive stances against Western powers and rivals in regional maritime disputes.<a href="#f51"><sup>51</sup></a> Some scholars identify 2008 as the turning point when CCP leaders became less willing to restrain nationalist forces, potentially due to the merging of state and popular nationalism.<a href="#f52"><sup>52</sup></a> State nationalism portrays the Chinese nation and the Communist state as a single entity, reinforcing CCP legitimacy.<a href="#f53"><sup>53</sup></a> So encouraging, or at least failing to restrain, nationalism may support the Chinese government’s primary interest of regime security but result in greater assertiveness on the international stage. This alters the CCP’s cost-benefit analysis regarding the risks of confrontation, leaving a greater opening for escalating tensions and entertaining conflict with the USA. Similarly, failures to resolve domestic economic or social issues may force the CCP to shift focus to external affairs in an effort to preserve national unity and popular support. Such assertiveness may result in a push that inadvertently goes too far and ignites conflict.

</div>
</div>
</div>
<div class="page" title="Page 32">
<div class="layoutArea">
<div class="column">
<h3>What Does this Mean for Australia?</h3>
The discussion presented in this paper suggests that Australia must be prepared for China to unintentionally upset the regional security balance in ways that could result in conflict. However, China is likely to lack both the ability and the will to <span style="font-style: italic;">deliberately </span>upset the regional security balance in the foreseeable future. China continues to reap economic, security and political benefits from the USA-dominated Asia-Pacific security system, which has significantly contributed to China’s recent accomplishments. However, this success has not sufficiently translated into Chinese ability to effectively challenge American regional dominance. China is heavily restrained by serious domestic issues, its military capabilities lag far behind the USA, and the regional and international system is likely to reject Chinese revisionism. Furthermore, China is so strongly integrated with the regional and international system that regional instability or conflict with the USA would undermine its core national interests. Nevertheless, China is still likely to attempt to alter the regional balance in its favour and its assertiveness increases the potential for miscalculations and misperceptions.

Like any balancing act, the situation remains fragile, and Australia and the world must respond to China’s rise in a manner that is realistically prepared for mistakes to be made. Australia must also be wary that treating China as a threat to the existing security system could create a self-fulfilling prophecy. This final possibility appears to have been recognised in Australia’s 2013 Defence White Paper, which used more moderate language in reference to China’s rise than the controversial 2009 White Paper.<a href="#f54"><sup>54</sup></a> This represents one preliminary step towards understanding not only Chinese intentions and abilities, but also the possibility of arbitrary factors destabilising the regional security balance. So, China will continue to rock the boat, but is unlikely to intentionally upset the regional balance. Failure to incorporate this distinction into Australian (and regional) foreign policy could push China into challenging the primacy of the USA. In a region home to at least four nuclear powers, ignoring such a vital perspective could lead to devastating consequences for the region, Australia included.

<hr />

<h3><strong>Footnotes</strong></h3>
<a id="f1"></a>1 White, Hugh (2010), ‘Power Shift: Australia’s Future Between Washington and Beijing’, <span style="font-style: italic;">Quarterly Essay</span>, No. 39, pp. 1-47; White, Hugh (2012), <span style="font-style: italic;">The China Choice</span>, Collingwood: Black Inc.

<a id="f2"></a>2 Dibb, Paul (2012), ‘Why I Disagree with Hugh White on China’s Rise’, <span style="font-style: italic;">The Australian</span>, 13 August, http://www.theaustralian.com.au/opinion/why-i-disagree-with-hugh-white-on-chinas-rise/story-e6frg6zo-1226448713852.

<a id="f3"></a>3 Ikenberry, John G. (2004), ‘American Hegemony and East Asian Order’, <span style="font-style: italic;">Australian Journal of International Affairs</span>, Vol. 58(3), p. 355; Mizokami, Kyle (2012), ‘Japan and the United States: It’s Time to Rethink Your Relationship’, <span style="font-style: italic;">The Atlantic</span>, http://www.theatlantic.com/international/archive/2012/09/japan-and-the-us-its-time-to-rethink-your-relationship/262916/.

<a id="f4"></a>4 Dobbins, James (2012), ‘War with China’, <span style="font-style: italic;">Survival: Global Politics and Strategy</span>, Vol. 54(4), p.13.

<a id="f5"></a>5 Ikenberry, op cit, 3.

<a id="f6"></a>6 Clinton, Hillary (2011), ‘America’s Pacific Century’, <span style="font-style: italic;">Foreign Policy</span>, November, http://www.foreignpolicy.com/node/1002667.

<a id="f7"></a>7 Goh, Evelyn (2008), ‘Hierarchy and the Role of the United States in the East Asian Security Order’, <span style="font-style: italic;">International Relations in the Asia-Pacific</span>, Vol. 8(3), p. 373.

<a id="f8"></a>8 Clinton, op cit, 6.

<a id="f9"></a>9 Haas, Richard N. (2013), ‘How to Build a Second American Century’, <span style="font-style: italic;">Washington Post</span>, 27 April, http://www.washingtonpost.com/opinions/how-to-build-a-second-american-century/2013/04/26/f197a786-ad25-11e2-a198- 99893f10d6dd_story.html.

<a id="f10"></a>10 Wong, Edward (2013), ‘Cost of Environmental Damage in China Growing Rapidly Amid Industrialisation’, <span style="font-style: italic;">New York Times</span>, 29 March, http://www.nytimes.com/2013/03/30/world/asia/cost-of-environmental-degradation-in-china-is-growing.html?_r=0.

<a id="f11"></a>11 Ibid.

<a id="f12"></a>12 Orlik, Tom (2012), ‘Chinese Survey Shows Higher Jobless Rate’, <span style="font-style: italic;">The Wall Street Journal</span>, 8 December, http://online.wsj.com/article/SB10001424127887323316804578164784240097900.html

<a id="f13"></a>13 Office of the Secretary of Defense, ‘Annual Report to Congress: Military and Security Developments Involving the People’s Republic of China 2013’, <span style="font-style: italic;">United States of America Department of Defense</span>, at http://www.defense.gov/pubs/2013_China_Report_FINAL.pdf; Wang, Feng (2010), <span style="font-style: italic;">China’s Population Destiny: The Looming Crisis</span>, Brookings Institution, http://www.brookings.edu/research/articles/2010/09/china-population-wang.

<a id="f14"></a>14 Howe, Neil et al (2009), <span style="font-style: italic;">China’s Long March to Retirement Reform: The Graying of the Middle Kingdom Revisited</span>, Centre for International and Strategic Studies and Prudential Foundation, http://news.prudential.com/images/65/US%20GOTMK%20English%20Bro%2009%204_3.pdf, p. 7.

<a id="f15"></a>15 Wang, op cit, 13.

<a id="f16"></a>16 For example: Mullen (Admiral), Mike (2010), ‘Joint Chiefs of Staff Speech at Asia Society of Washington Award Dinner’, Washington D.C., 9 July. Quoted in Dobell, Graeme (2010), ‘China: The Hugh White Way’, <span style="font-style: italic;">The Lowy Interpreter</span>, 6 September, http://www.lowyinterpreter.org/post/2010/09/06/China-The-Hugh-White-way.aspx.

<a id="f17"></a>17 Perlo-Freeman, Sam et al (2013), <span style="font-style: italic;">Trends in World Military Expenditure 2012</span>, Stockholm International Peace Research Institute, SIPRI Factsheet, http://books.sipri.org/files/FS/SIPRIFS1304.pdf

<a id="f18"></a>18 Garnaut, John (2013), ‘Xi’s War Drums’, <span style="font-style: italic;">Foreign Policy</span>, May/June, http://www.foreignpolicy.com/articles/2013/04/29/xis_war_drums.

<a id="f19"></a>19 Yuan, Jingdong (2013), ‘Getting China Right: America’s Real Choice’, <span style="font-style: italic;">Security Challenges</span>, Vol. 9(1), p. 12.

<a id="f20"></a>20 Garnaut, op cit, 18.

<a id="f21"></a>21 Ibid.

<a id="f22"></a>22 Funnell, Antony (2013), ‘The Myth Around China’s Growing Military Supremacy’, <span style="font-style: italic;">ABC Radio National</span>, 6 May, http://www.abc.net.au/radionational/programs/futuretense/military-expenditure/4672124.

<a id="f23"></a>23 Perlo-Freeman, Sam: Ibid.

<a id="f24"></a>24 Perlo-Freeman et al, op cit, 17, p. 2.

<a id="f25"></a>25 Dibb, Paul (2013), <span style="font-style: italic;">What are the risks of war between China and the US?</span>, public lecture at the Australian National University in Canberra, 6 May, http://www.youtube.com/watch?v=BS-xyKCPzNQ&amp;list=PL9C779E9790954D25.

<a id="f26"></a>26 Nathan, Andrew and Scobell, Andrew (2012), ‘How China Sees America: The Sum of Beijing’s Fears’, <span style="font-style: italic;">Foreign Affairs</span>, Vol. 91(5).

<a id="f27"></a>27 Hughes, Christopher (2011), ‘Reclassifying Chinese nationalism: the Geopolitik Turn’, <span style="font-style: italic;">Journal of Contemporary China</span>, Vol. 20(71), pp. 601–620.

<a id="f28"></a>28 Cook, Malcolm et al (2010), <span style="font-style: italic;">Power and Choice: Asian Security Futures</span>, Sydney: Lowy Institute for International Policy, p. 3.

<a id="f29"></a>29 Stevens, Philip (2013), ‘The Threats to Asia’s Fragile Balance of Power’, <span style="font-style: italic;">The Financial Times</span>, 2 May, http://www.ft.com/cms/s/0/d6945102-b255-11e2-8540-00144feabdc0.html#axzz2Sqs4mWgw.

<a id="f30"></a>30 Garnaut, op cit, 18.

<a id="f31"></a>31 Kang, David C. (2005), ‘Why China’s Rise Will be Peaceful: Hierarchy and Stability in the East Asian Region’, <span style="font-style: italic;">Perspectives on Politics</span>, Vol. 3(3), p. 553.

<a id="f32"></a>32 Yuan, op cit, 19.

<a id="f33"></a>33 Nathan and Scobell, op cit, 26.

<a id="f34"></a>34 National Bureau of Statistics of China (2012), ‘Table 10: Imports and Exports by Major Countries and Regions and the Growth Rates in 2011’, <span style="font-style: italic;">Statistical Communiqué of the People's Republic of China on the 2011 National Economic and Social Development</span>, 22 February, http://www.stats.gov.cn/english/newsandcomingevents/t20120222_402786587.htm.

<a id="f35"></a>35 Herberg, Mikkal (2008), ‘China’s Search for Energy Security: The Implications for South East Asia’ in Goh, Evelyn and Simon, Sheldon W. (eds.), <span style="font-style: italic;">China, the United States and South East Asia: Contending Perspectives on Politics, Security and Economics</span>, New York: Routledge, p. 70.

<a id="f36"></a>36 US Department of Energy, Energy Information Administration, <span style="font-style: italic;">International </span><span style="font-style: italic;">Energy Outlook 2005</span>, Ibid, p. 73.

<a id="f37"></a>37 Phillips, Andrew (2010), ‘Power Shift or Power Drift?’, <span style="font-style: italic;">The Lowy Interpreter</span>, 17 September, http://www.lowyinterpreter.org/post/2010/09/17/Power-shift-or-power-drift.aspx.

<a id="f38"></a>38 Mearsheimer, John (2001), <span style="font-style: italic;">The Tragedy of Great Power Politics</span>, New York: W. W. Norton, p. 400.

<a id="f39"></a>39 Powell, Robert (2004), ‘Bargaining Theory and International Conflict’, <span style="font-style: italic;">American Review of Political Science</span>, Vol. 5(1), p. 231.

<a id="f40"></a>40 Agence France-Presse (2013), ‘China ships head into “disputed waters”’, <span style="font-style: italic;">Al Jazeera</span>, 4 May, http://www.aljazeera.com/video/asia- pacific/2013/05/20135412348979323.html.

<a id="f41"></a>41 Ibid.

<a id="f42"></a>42 Wall Street Journal (2013), ‘China’s Territorial Ambition’, <span style="font-style: italic;">Wall Street Journal</span>, 2 May, http://online.wsj.com/article/SB10001424127887323528404578454341420089874.html; The Economist (2013), ‘Thunder Out of China’, <span style="font-style: italic;">The Economist</span>, 4 May, http://www.economist.com/news/china/21577075-around-chinas-periphery-heat-keeps-rising- thunder-out-china.

<a id="f43"></a>43 Garnaut, op cit, 18.

<a id="f44"></a>44 <span style="font-style: italic;">Wall Street Journal</span>, op cit, 42.

<a id="f45"></a>45 Nathan and Scobell, op cit, 26.

<a id="f46"></a>46 Herberg, op cit, 35, p. 70.

<a id="f47"></a>47 Bitzinger, Richard A. and Desker, Barry (2008/2009), ‘Why East Asian War is Unlikely’, <span style="font-style: italic;">Survival</span>, December/January, Vol. 50(6), pp. 106-107.

<a id="f48"></a>48 Nathan and Scobell, op cit, 26.

<a id="f49"></a>49 Garnaut, op cit, 18.

<a id="f50"></a>50 Garnaut, John (2013), ‘Fears Xi’s push on Japan poses showdown risk’, <span style="font-style: italic;">Sydney Morning Herald</span>, 16 March, http://www.smh.com.au/world/fears-xis-push-on-japan-poses-showdown-risk-20130315-2g63g.html.

<a id="f51"></a>51 Zhao, Suisheng (2013), ‘Foreign Policy Implications of Chinese Nationalism Revisited: the Strident Turn’, <span style="font-style: italic;">Journal of Contemporary China</span>, Vol. 22(82), pp. 535-553, p. 535.

<a id="f52"></a>52 Ibid, p. 536.

<a id="f53"></a>53 Ibid, p. 536.

<a id="f54"></a>54 King, Amy (2013), ‘A change of tone on China’, <span style="font-style: italic;">The Drum</span>, 6 May, http://www.abc.net.au/unleashed/4670988.html.

<hr />

<strong>This article appears in the <a href="http://www.acya.org.au/wp-content/uploads/2013/06/ACYA-Journal-of-Australia-China-Affairs-2013.pdf">2013 ACYA Journal of Australia-China Affairs (pp. 26-32)</a>.</strong>

<em id="kara">Kara Hawkins is a third-year undergraduate student at the Australian National University, studying International Relations. She majors in Arabic but also has a keen interest in Asia-Pacific studies.</em>

</div>
</div>
</div>