{
  "title": "In Search of My Roots",
  "author": "ACYA",
  "date": "2014-10-03T18:09:36+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "in search of my roots",
      "type": "image",
      "src": "in-search-of-my-roots1-e1412360754150.jpg"
    }
  ]
}
<div class="page" title="Page 51">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#sl">Sue-Lin Wong (黄淑琳)</a></h2>
</div>
</div>
<div class="layoutArea">
<div class="column">

Being Australian-Chinese, my so-called 'identity crisis' has always existed deep within myself. Even though I can think of many reasons to explain why I wanted to come to China, in my heart of hearts it is certain that the most important reason was that it was some type of <em>quest</em>. I used to think that after I came to China I would easily be able to find whatever it was that I was looking for, but in fact my first experiences in China not only did not help me find what I was looking for, but on the contrary actually made me even more confused. What am I searching for? Do my 'roots' really lie in China?

I always seem to end up having the same sort of conversation with all types of Chinese people:

<em>"You've come back! That's great, you can see how your homeland has developed!"</em>

Did I really come back? Is it still coming back if I've never been to China before?

<em>"Ah, your Chinese is so bad, don't you feel you're unworthy of being Chinese?"</em>

Am I Chinese? Do you think that all people who look Chinese can speak Chinese, that all white people can speak English?

<em>"How do Australians treat us Chinese people?"</em>

Don't Australians include ethnic Chinese people? If I am born and raised in Australia, and my family and friends are all in Australia, am I still not an Australian?

</div>
<div class="column">

In China, I not only often encounter these kinds of questions, but also frequently have the opportunity to talk about these kinds of topics with overseas Chinese and foreign-born Chinese from all over the world. Just this year, I was chatting with some ethnically Chinese friends at a party and we somehow started talking about: "If there was a drug that could transform a Chinese into a white person, would you take it?" Some friends answered "definitely" right away, some replied "absolutely never", whilst some hesitatingly said "I don't know", and thus ensued a fiery debate!

While I was living in a university dormitory in China, I came to know ethnic Chinese from countries such as Japan, Burma, Thailand, Singapore, Malaysia, Sweden, Germany, France, Britain, Denmark, Mauritius, Brazil, Panama, America, and Canada. Late at night, we would gather in someone's room and talk about all the kinds of identity issues we had experienced. A tall and well-built male friend had tears streaming down his cheeks as he told us about his experiences being bullied at high school in America, and this Australian-Born Chinese girl found herself realising she had had experiences almost exactly the same as those of a Brazilian-Born Chinese boy.

Listening to songs such as "My Chinese Heart" and "Waving the Red Flag", some of us feel really proud, whilst some of us do not feel anything at all. Sometimes, Chinese people remind us: "Do not forget, you are Han Chinese, descendants of the dragon". We foreign-born Chinese often discuss how best to answer such people – many of us think that perhaps a quote from the Asian- American <em>My Chinese New Year</em> blog is closer to describing the truth of our situation: "It ended up being one of the most healing experiences I've ever had – and not because I found a home in China but because I found a community of Chinese migrants around the world, the Chinese diaspora. I finally found my home."

</div>
</div>
</div>
<div class="page" title="Page 52">
<div class="layoutArea">
<div class="column">

Of course, many foreign-born Chinese, and especially those that have not been to China, have somewhat of an aversion to this issue. In Australia I have cousins who say to me, "Sue-Lin, why are you going to study in China? No matter how much money you give me, I would never go to China in my whole life!"

For me, my curiosity was stronger than any sense of disgust – I just had to go to China! But after I arrived in China, I realised that the path to finding my roots only became more and more complex. Firstly, while many Chinese people are happy to help me in solving my so-called identity crisis, the problem is that many Chinese people also have a particularly strong clan mentality, or at least one far more intense than that held by Australians. Furthermore, the overseas Chinese and foreign-born Chinese I know in China all have their own stories, backgrounds, and experiences, meaning that they each have their own individual attitudes and feelings towards China. Everyone has their own thoughts and ideas about their 'roots'.

Personally, before the 2012 Chinese New Year, my so-called 'roots' were merely an abstract concept to me: since I was young, people have always told me to "when drinking water, think of its source – don't forget where you come from". Al Cheng, founder of the American NGO Searching for Roots in Canton, once said that "strong roots build strong character", which for me calls to mind the words of Youzi, one of Confucius' most admired students — "a noble person attends to the basic, as only after the basics are established will the path to wisdom unfold". What Youzi means here is that a "noble person" will concentrate and devote him or herself to the foundation of things, as for Youzi the concept of 'basis' points to the very fundamentals of humanity – filial piety – knowing where you are from, showing filial obedience to your parents and elders, and respecting your ancestors. Thinking about my own experiences with an open mind, it seems to me that in a sense this 'basis' is 'roots', and that it is only after you know your roots that you can truly grow to maturity. Thus I decided to use my winter vacation to search for and visit distant Chinese relatives whom I had never met before, as both my paternal and maternal grandparents moved to Malaysia from China in the 1930s. Even though all of my parents' immediate relatives live in Southeast Asia, the USA, or Australia, the descendants of the siblings of both my paternal and maternal grandparents still live in China.

</div>
<div class="column">

I decided to go first to my paternal grandfather's hometown of Fuzhou in Fujian Province and then onto my maternal grandfather's hometown of Chaozhou in Guangdong Province. It is a pity that no one knows the hometown of either my paternal or maternal grandmothers, but, looking at it from another angle, because I do not know where they are from, whenever I am taking the metro or shopping in the supermarket I am always wondering: could the passenger sitting next to me or the supermarket sales clerk be a distant relative of mine!?

On the train to Fuzhou, I suddenly realised that I would not be able to recognise my relatives' faces, so I texted my aunt to ask her what clothes she would be wearing, as otherwise I would not be able to find her at the train station! After I found her and my other relatives at the station, we went back to their home. They were especially good to me, perhaps even too good to me, as I started to get fat! During the two weeks I was there, I ate an average of five meals every day, and even now I still cannot fit into my old jeans! I lived in many different relatives' houses in turn, gradually getting to know my Chinese family. In some sense, their lives are completely alien to mine – but in other regards we are very much the same. For example, I found that in the houses of my mother's relatives, even though my mother has never returned to her hometown, the taste of their cooking was exactly the same as hers!

</div>
</div>
</div>
<div class="page" title="Page 53">
<div class="layoutArea">
<div class="column">

On a humourous note, I think that due to the language barrier I brought a great deal of joy to my relatives while I was there! Even though my father and mother can both speak their own dialects – respectively the Fuzhou and Chaozhou dialects – because they do not share a dialect they only use English to communicate, and thus my younger brother and I cannot understand a word of their dialects. Fortunately, my Chinese relatives could speak Mandarin and were very tolerant of the countless stupid mistakes I would make every day! While I was staying at my first relative's house, the older sister told me that because she was going to attend her elder brother's wedding the next day, she would prepare rice porridge for me in the morning. My mind was spinning fast trying to understand, and because in Chinese the pronunciation of this type of rice porridge is the same as "Western meal", I replied to her "Sister, no need, no need, it doesn't matter, no problem, I can eat Chinese food, you don't need to make Western food specially for me!" And then when I went to stay at the house of my second set of relatives, they told me that we were going to have white porridge the next morning, and because in Chinese the pronunciation of white porridge is similar to that of the traditional Chinese white spirit <em>baijiu</em>, in my mind I was thinking: "<em>Baijiu</em>? This is a very strange breakfast, what are we doing drinking <em>baijiu</em> in the morning, my relatives really are pretty strange people! But the Chinese New Year is approaching soon, so perhaps this is a traditional custom for passing the New Year in Southern China?" Because of my previous misunderstanding with the porridge, I thought that my best option would be just to silently respect the traditional Southern custom of drinking spirits in the morning! The next day when we were eating breakfast I was constantly waiting for the <em>baijiu</em>, but it never showed up and instead we just had a lot of white porridge!

Language was not the only obstacle; there were also many cultural differences.
The most common dialogue between my Chinese relatives and I went something like this:

<em>"Do you have children?"
</em><em>"No."
</em><em>"Oh, you don't have children. Then, are you married?"
</em><em>"No."
</em><em>"Oh, you're not married, you should hurry up!"</em>

</div>
<div class="column">

While attending the wedding of my elder male cousin, the matchmaker who arranged the wedding asked me the questions above and then proceeded to worriedly ask me questions such as what is my degree major, what university am I studying at, what type of work do my parents do, what dialects do they speak, do I have brothers or sisters? When she was done she said: "OK, leave me with your phone number and I'll notify you when I find a suitable partner for you. This is my home phone number, this is my mobile number, call me anytime, you're not young anymore and the sooner you have a child the better. May you give birth to a son soon!"

Despite the many things that moved me during my 'search for my roots', such as the misunderstanding with the  <em>baijiu</em> and the concern shown for me by the matchmaker, what really touched me was the experience I had in the hometown of my paternal grandfather – Yixu, on the outskirts of Fuzhou City. Yixu is a very representative Chinese clan village.

In Yixu, every time I walked down the street I would bump into a relative: "Sue-Lin, this is your grandfather's younger male cousin's wife, that is your great-grandfather's younger female cousin's grandson!" Three children of my paternal grandfather's younger brother are still living in Yixu and they took me to see our ancestral temple, inside of which were the spirit tablets of dozens of generations. Even though the temple was built in 1662, our ancestors could be traced back to 885 when a relative named Huang Dun followed Wang Shenzhi (one of the founders of the Five Generations of Fujian) from Guangzhou (in Henan Province) to Fujian. In the temple, I found all the names of my paternal grandparents, my father, and all of his siblings. My relatives then told me that my generation and all our further descendants would also all have our own spirit tablets!

</div>
</div>
</div>
<div class="page" title="Page 54">
<div class="layoutArea">
<div class="column">

An economics professor at Beijing Normal University has estimated that China's income disparity between the rich and poor is steadily deteriorating. China's Gini coefficient is now almost 0.5 and experts generally believe that a national Gini coefficient of above 0.4 is likely to have a destabilising effect upon society. Whilst on my journey to find my roots, I saw with my own eyes how obvious is the gaping gap between the rich and poor in China. Even though some of my relatives who live in cities have a comparatively good life, the houses of my relatives in Yixu had no furniture, their walls had no paint, and the area outside their houses and down to the street was strewn with rat-infested rubbish. Furthermore, my relatives all had to raise poultry and grow vegetables themselves. One of my uncles was in Yunnan Province searching for manual labour, and so had no time over the Spring Festival to return home for Chinese New Year. My elder male cousin worked in the Yixu post office. My elder female cousin, only a few years older than I, was already married with children and working in a nearby supermarket. But despite their poverty, they were exceptionally warmhearted, generous, and united, which made me think; is this the way we live in Australia?

My aunt in Yixu told me many stories about our family. When my paternal grandfather and his younger brother were young, they relied on selling oysters to make a living – it took them hours each day to go out and find the oysters to bring back to be sold in Yixu. In the 1930s, my family in Yixu were almost dying of hunger, so as a twelve-year old boy my paternal grandfather boarded a boat bound for Malaysia in search of work. In Malaysia he sold chillis by the side of the road, and it was only when he was seventeen that he was able to return home again for the first time. When he then left again for Malaysia, as his boat was leaving the dock his younger brother jumped into the water and swam up to the side of the boat, telling my grandfather that he wanted to go with him to Malaysia. But my grandfather said to him: "If both of us go, then who will take care of our parents? Go home, give me a few years in Malaysia to earn money for you all, and then I will come back home to live with you". So my grandfather's brother went home to wait for him, but who could know that this wait would turn into a lifetime.

</div>
<div class="column">

From then on, despite it being my grandfather's dream to return to China, historical reasons meant that he never had the chance. My father once told me that one night when he was young, when my grandfather heard the Chinese song "Only Mother Is Good In This World" he started to weep silently to himself. While my father was growing up in Malaysia, he attended a Chinese primary school in Malaysia where he was taught Libai's famous poem <em>Night Thoughts</em> (below, as translated by Herbert A. Giles):

<em>I wake, and moonbeams play around my bed,
</em><em>Glittering like hoarfrost to my wandering eyes;
Up towards the glorious moon I raise my head,
There lay me down – and thoughts of home arise.</em>

Based upon the memories of my aunt, during the 1950s and 1960s my paternal grandparents would send food parcels back to China, without which my aunt reckoned our family there would have starved to death. She also told me that her father – my grandfather's younger brother – had still been waiting for my grandfather all this time. My grandfather passed away in the 1970s, but it was only shortly after he died that Malaysia finally changed its diplomatic policies towards China and allowed Malaysians to travel there. After my grandfather passed away, his brother was still looking forward to my father returning home, but it was only two years after the death of my grandfather's brother that my family in Malaysia finally found its way back to our hometown.

I was incredibly moved to hear such stories, but I did not know how to answer all the complicated questions I was asked by my relatives: "How do you feel coming back to your hometown? Do you think we look alike? You must have a very deep sense of identity with Chinese people now?" When I was about to leave, my friends asked me: "How was your journey in search of your roots? What feelings did it give you? What did you find? How did you feel when you found your 'roots'?" Even when I was walking out the door, I still had strangers asking me: "You've come home! How does it feel? You're Australian, right? But you must feel something towards China too?"

</div>
</div>
</div>
<div class="page" title="Page 55">
<div class="layoutArea">
<div class="column">

How can I answer these types of questions!?

I do not know. There are too many questions, and their answers too complicated. I only know two things. Firstly, my experiences in China gave me a unique type of feeling. Secondly, because of my journey in search of my roots I now ponder a new thought. If my grandfather's dream to return to China had come true, if his fallen leaves had returned to their roots, if the travelling man had returned home, I would now probably be like my female cousin. Born and raised in Yixu, never even been as far Xiamen, maybe already married, maybe already with children, working every single day at the small village supermarket.

</div>
<div class="column">

But did I really win the lottery of life?

<hr />

<strong>This article appears in the <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf">2012 ACYA Journal of Australia-China Affairs (pp. 51-55)</a>. It was awarded the ACYA Journal Opinion Article (Chinese) Prize and has been translated into English from the original Chinese.</strong>

<em id="sl">Sue-Lin Wong is in her fifth year of an Asian Studies/Law degree at the Australian National University. She is currently in China on a Prime Minister's Australia Asia Endeavour Award.</em>

</div>
</div>
</div>