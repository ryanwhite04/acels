{
  "title": "<!--:en-->ACYA- China Policy Traineeship Program<!--:--><!--:zh-->ACYA- China Policy Traineeship Program<!--:-->",
  "author": "ACYA",
  "date": "2014-10-08T04:55:30+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->ACYA- China Policy Traineeship Program

ACYA would like to announce the commencement of the ACYA-China Policy Traineeship Program. 

China Policy is a Beijing-based strategic advisory and research company. Their research team’s detailed, systematic coverage of China’s own domestic policy discussion gives them the edge, and their clients' confidence in their advice. It provides the evidence base from which they tailor guidance on strategic China questions. Clients include large multinationals, financial institutions and international organisations.

The ACYA-China Policy Traineeship Program aims to improve the policy-related skillsets of Australian graduates, extending their understanding from headline issues to the deep structural underpinning of China’s policy challenges. All our positions offer an unparalleled opportunity to get inside the policy debates and learn how to map and track the policy discussion.

Our core sectors include: economy; energy, resources and environment; social policy and governance and law.

We encourage exceptional candidates to contact ACYA ahead of the deadlines to express an interest in a placement in subsequent rounds, or to fill a casual vacancy.

<a href="http://www.acya.org.au/acya-china-policy-partnership/ ">Please click here for current opportunities under the ACYA- China Policy Traineeship Program!</a>

For more information visit policycn.com<!--:-->