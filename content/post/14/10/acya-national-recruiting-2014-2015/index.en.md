{
  "title": "<!--:en-->ACYA National is recruiting for 2014-2015!<!--:--><!--:zh-->ACYA 全国总会2014-2015年度招募！<!--:-->",
  "author": "ACYA",
  "date": "2014-10-10T01:02:40+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "10583858_342345289253788_3743404028974462920_n",
      "type": "image",
      "src": "10583858_342345289253788_3743404028974462920_n-e1412902929359.jpg"
    }
  ]
}
<!--:en-->With over 5,000 members, and 23 local chapters across Australia and China, ACYA has provided many opportunities to thousands of young professionals and students across the education, careers and people to people space. These opportunities wouldn't be possible of course without an enthusiastic and dedicated team working away behind the scenes. Think you have what it takes? Looking to pursue a career in the Australia-China arena?
Positions to join the ACYA National team are now open across the portfolios of business development, communications, content creation, education, publications, and many more.  In these roles, ACYA members have the opportunity to use their skills, enhance their Australia-China capabilities and make a real difference in the Australia-China space.
 
For more information, including position descriptions and application instructions, <a href="http://www.acya.org.au/join-acya-2015/">please click here</a>. Please pass this on to your friends and colleagues who are interested in getting involved in Australia's most prominent organisation operating in the Australia-China youth space. Applications close on Thursday 23rd October 5pm AEDT.
 
Please also keep your eye out on the "<a href="http://www.acya.org.au/join-acya-2015/">Join ACYA 2015</a>" page over the next two months as more positions become available!<!--:--><!--:zh-->拥有超过5000名成员，23个分布于中澳两国的分会，ACYA在过去几年间为上千名青年专业人士和学生提供了在教育、职业发展、人文交流等领域的各种机遇。这样的成就，离不开ACYA总会幕后充满热情和奉献的团队。觉得自己也可以胜任这一职务？想要在中澳青年关系中扮演重要的角色？

ACYA全国总会团队现正招募包括商务发展、宣传、创意、教育、出版等各类职务。通过担任全国总会的这些职务，成员们可以锻炼和实践各类技能、提升其对中澳关系的理解、并为促进中澳两国关系做出自己的贡献。

如需更多相关信息，包括职务描述、申请流程等，<a href="http://www.acya.org.au/join-acya-2015/">请点击此处</a>。如您的朋友、同事对参与澳大利亚最大的中澳青年关系组织感兴趣，也请您与他们分享这一招募信息。申请将在澳大利亚东部时间10月23日星期四下午5点截止。

接下来的两个月内，也请您关注 "<a href="http://www.acya.org.au/join-acya-2015/">加入ACYA 2015</a>"网页以获取更多职务招募信息！ <!--:-->