{
  "title": "<!--:en-->Inaugural ACYA-RBS Scholarship Recipient Commences MBA Program<!--:--><!--:zh-->Inaugural ACYA-RBS Scholarship Recipient Commences MBA Program<!--:-->",
  "author": "ACYA",
  "date": "2014-10-24T05:07:39+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "89",
      "type": "image",
      "src": "89.png"
    }
  ]
}
<!--:en--><em>Feature Photo Left to Right: ACYA General Manager (Projects) Michael McGregor, and Former Director of Renmin Business School at Renmin University of China, welcoming the ACYA-RBS Scholar, Shaun Harkness
</em>
<p>The Australia-China Youth Association (ACYA) would like to congratulate Stephen Thompson and Shaun Harkness, the inaugural recipients of the ACYA-RBS International MBA Scholarship, for commencing an International MBA at Renmin University of China Business School (RBS). </p>
<p>The first agreement of its kind to be established between ACYA and a mainland Chinese university, the ACYA-RBS International MBA Scholarship allows the recipient to study on a full scholarship for the duration of the two-year degree. Due to the high calibre of Stephen and Shaun’s applications, the RBS will be providing scholarship support to both students. </p>
<p>ACYA President Tom Williams said the scholarship provides a fantastic opportunity for ACYA members.</p>
<p>“We commend and thank Renmin Business School, especially RBS MBA Director Associate Professor Zhou Yu, PHD for working with ACYA to provide this fantastic opportunity to Stephen and Shaun.” </p>
<p>“This agreement demonstrates ACYA’s commitment to providing high quality educational opportunities to its members.”</p>
<p>The RBS International MBA program provides students with the unique opportunity to study international business and management in the international market that is China. </p>
<p>Shaun anticipates that studying at such a renowned institution will provide a wealth of opportunities for growth not only in terms of Chinese language ability, but also how he may contribute to the maturation of Australia-China relations:  </p>
<p>“Renmin University has a very prestigious reputation in China and is highly regarded by many business leaders. I definitely see my future involving China in some way, and believe the experience and connections I will gain during this program will be invaluable in my future working life.”</p>
<p>Shaun has already started his degree in September, while Stephen is scheduled to commence in September 2015.</p>
<p>Due to the success and quality of this years applications, ACYA is pleased to announce that applications for the ACYA-RBS IMBA Scholarship will open again soon, for the 2015/16 academic year. So keep an eye out for more information over the coming weeks! </p>
<p>Contact<br />
A full interview with Shaun Harkness, as well as other Australia-China scholarship opportunities <a href="http://www.acya.org.au/2014/10/interview-shaun-harkness-inaugural-recipient-acya-rmb-imba-scholarship/">can be found on here</a>.</p>
<p>For enquiries regarding the ACYA-RBS International MBA Scholarship, please contact ACYA’s National Education Director: </p>
<p>Michael Twycross<br />
education@acya.org.au<br />
(+61 404 501 393)</p>


<!--:--><!--:zh--><a href="http://www.acya.org.au/wp-content/uploads/2014/10/89.png"><img src="http://www.acya.org.au/wp-content/uploads/2014/10/89-300x158.png" alt="89" width="300" height="158" class="alignnone size-medium wp-image-21802" /></a><a href="http://www.acya.org.au/wp-content/uploads/2014/10/89.png"><img src="http://www.acya.org.au/wp-content/uploads/2014/10/89-300x158.png" alt="89" width="300" height="158" class="alignnone size-medium wp-image-21802" /></a><a href="http://www.acya.org.au/wp-content/uploads/2014/10/89.png"><img src="http://www.acya.org.au/wp-content/uploads/2014/10/89-300x158.png" alt="89" width="300" height="158" class="alignnone size-medium wp-image-21802" /></a><!--:-->