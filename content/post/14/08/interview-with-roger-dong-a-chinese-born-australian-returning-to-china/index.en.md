{
  "title": "Interview with Roger Dong, a Chinese-born Australian returning to China",
  "author": "ACYA",
  "date": "2014-08-05T08:47:29+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "roger dong",
      "type": "image",
      "src": "roger-dong.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/08/roger-dong.jpg"><img class="alignleft size-thumbnail wp-image-21321" src="http://www.acya.org.au/wp-content/uploads/2014/08/roger-dong-150x150.jpg" alt="[cml_media_alt id='21321']roger dong[/cml_media_alt]" width="150" height="150" /></a>

<strong>1) Can you tell me a little bit about your upbringing (where you were born and where you grew up)?</strong>

I was born in Shanghai and lived in an old flat towards the outskirts of the city before moving to Melbourne with my parents when I was 9 years old. I didn’t know any English at the time but fortunately, the primary school I went to was very supportive and with the help of the great teachers and kids there I quickly picked up the language. I went to a few more schools afterwards and eventually studied at the University of Melbourne.

<strong>2) You completed a Bachelor of Laws / Engineering at Melbourne Uni, why the move to Beijing straight after graduation?</strong>

After I moved to Australia, I didn't have many opportunities to go back for visits and unfortunately I also did not continue learning Chinese. When an opportunity to work for an Australian engineering company in Beijing came up, which was provided by the Australia Chamber of Commerce’s Graduate Scholars program, I decided to take the chance. This not only allowed me to use my engineering degree and gain valuable Sino-Australian experience, but also to learn the language and to do some travelling around China whenever I had the chance.

<strong>3) What are your impressions of mainland China? What did you enjoy most about your time there?</strong>

I had a fantastic time in China and my biggest impression of it is that it is just such a diverse country. Whether it be the diversity of people, the diversity of viewpoints in society or even just the diversity of landscapes and climates of the country. I've realized that it is difficult to make any generalizations of China without finding out later that there is a significant exception to it. What I enjoyed the most about my time there were all the wonderful and interesting people that I met.

<strong>4) You have left Beijing, so what are you doing now? And what are your future goals?</strong>

After two years in Beijing, an opportunity arose for me to move to Hong Kong as a junior lawyer at an international law firm. As a prerequisite I had to sit some exams and complete a 1-year legal qualification course, which I have just completed at Hong Kong University. I’m currently taking a break back in Melbourne and will be going back to Hong Kong in September to begin working. Hong Kong is a great place to live and I hope to further gain high quality Sino-international experience there.

<strong>5) What would be your advice to other young Australians looking to gain China exposure?</strong>

My best advice would be to just take the plunge and go for it! Whether you do it through your university or workplace, through an organisation like the Australian Chamber of Commerce or even if you go by yourself to learn the language or as an English teacher, it will be an amazing experience that you will never forget.

<strong>6) What do you see as the key challenges moving forward for the Australia-China relationship?</strong>

I think that the Australian/Chinese business relationship is currently very strong, with many success stories in trade or partnerships between Australian and Chinese businesses already. Moving forwards, overcoming the challenges of the negotiations of the free trade agreement between the countries and finalizing it would create even more business opportunities.

<hr />

<em>From the June 2014 edition of <a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=351ce1e0a2">ACYA AustraliaBites</a>. Interview conducted by Jade Little.</em>