{
  "title": "ACYA Bilateral Youth Leadership Workshop - 15th to 18th August 2014",
  "author": "ACYA",
  "date": "2014-08-04T17:54:38+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "example2date2 (2)",
      "type": "image",
      "src": "example2date2-2.png"
    },
    {
      "title": "example2date2 (3) (618x309)",
      "type": "image",
      "src": "example2date2-3-618x309.jpg"
    },
    {
      "title": "rsz_example2date2_2",
      "type": "image",
      "src": "rsz_example2date2_2.png"
    },
    {
      "title": "jasonlim",
      "type": "image",
      "src": "jasonlim.jpg"
    },
    {
      "title": "jasonatsenli",
      "type": "image",
      "src": "jasonatsenli.jpg"
    },
    {
      "title": "james",
      "type": "image",
      "src": "james.jpg"
    },
    {
      "title": "kerrybrown",
      "type": "image",
      "src": "kerrybrown.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/06/example2date2-2.png"><img class="wp-image-19395 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/06/example2date2-2-300x150.png" alt="[cml_media_alt id='19395']example2date2 (2)[/cml_media_alt]" width="302" height="151" /></a>

<span style="color: #000000;"><b><span class="Apple-style-span">ACYA Bilateral Youth Leadership Workshop 2014, ACYA's annual flagship conference for emerging #auschina leaders!</span></b></span>

The Australia-China Youth Association (ACYA) is excited to announce that we will be hosting our second flagship Bilateral Youth Leadership Workshop (formerly National Conference) in Sydney from <strong>the 15th-18th of August 2014</strong>.

The conference will comprise of ACYA delegates from across Australia. Delegates will have the opportunity to participate in workshops led by the National Executive and key partner organisations. The delegation's professional skills and capabilities will be refined and built upon as emerging leaders within ACYA.

<a href="http://www.acya.org.au/wp-content/uploads/2014/08/kerrybrown.jpg"><img class="alignnone size-medium wp-image-21350" src="http://www.acya.org.au/wp-content/uploads/2014/08/kerrybrown-300x150.jpg" alt="[cml_media_alt id='21350']kerrybrown[/cml_media_alt]" width="300" height="150" /></a>

<a href="http://www.acya.org.au/wp-content/uploads/2014/08/james.jpg"><img class="alignnone size-medium wp-image-21349" src="http://www.acya.org.au/wp-content/uploads/2014/08/james-300x150.jpg" alt="[cml_media_alt id='21349']james[/cml_media_alt]" width="300" height="150" /></a>

<a href="http://www.acya.org.au/wp-content/uploads/2014/08/jasonatsenli.jpg"><img class="alignnone size-medium wp-image-21348" src="http://www.acya.org.au/wp-content/uploads/2014/08/jasonatsenli-300x150.jpg" alt="[cml_media_alt id='21348']jasonatsenli[/cml_media_alt]" width="300" height="150" /></a>

<a href="http://www.acya.org.au/wp-content/uploads/2014/08/jasonlim.jpg"><img class="alignnone size-medium wp-image-21347" src="http://www.acya.org.au/wp-content/uploads/2014/08/jasonlim-300x150.jpg" alt="[cml_media_alt id='21347']jasonlim[/cml_media_alt]" width="300" height="150" /></a>

&nbsp;

For more information relating to this event, please contact Ian Liu, National Communications Director: communications@acya.org.au and follow #BYLW2014.