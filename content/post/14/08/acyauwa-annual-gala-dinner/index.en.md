{
  "title": "ACYA@UWA: Annual Gala Dinner",
  "author": "ACYA",
  "date": "2014-08-20T12:08:53+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/08/ACYA-UWA.jpg"><img class="alignnone size-medium wp-image-21411" src="http://www.acya.org.au/wp-content/uploads/2014/08/ACYA-UWA-300x112.jpg" alt="[cml_media_alt id='21411']ACYA-UWA[/cml_media_alt]" width="300" height="112" /></a>

&nbsp;

Welcome to <a href="https://www.facebook.com/groups/acya.uwa/">Australia-China Youth Association (ACYA) @ UWA</a>'s grandest fundraising event of the year! To celebrate the growth of our chapter in 2014, we present you the inaugural ACYA Charity Gala.

Come join us dressed in your best Old Shanghai themed outfits and enjoy our detailed cocktail menu under live Jazz entertainment! Relive the extravagance and make it a night to remember!

All proceeds will go to <a href="https://www.facebook.com/halftheskyaus">Half the Sky Foundation Australia</a>, a charity whose dedication is to enrich the lives of orphaned children in China. For more details feel free to visit at <a href="http://halfthesky.org.au/en/au/">http://halfthesky.org.au/en/au/</a>

&nbsp;

Tickets:

$95 Members

$110 Non-Members

Tickets can be purchased at the ACYA stall on campus or off any of our friendly committee members. For your convenience, tickets can also be purchased online at <a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fevents.ticketbooth.com.au%2Fevent%2FUWAACYAGala2014&amp;h=RAQF6PIhN&amp;enc=AZN2QmeuOLNYxcFhfRGFVYiqaOhihE5B3X0bJ4wiOVPwjgR85zix6ot3Bnf2e0qbcYg&amp;s=1">http://events.ticketbooth.com.au/event/UWAACYAGala2014</a>

***member-priced tickets can only be purchased on our UWA campus

&nbsp;

Cocktail Food + Drinks included

***ACYA encourages the responsible consumption of alcohol

***Don't drink and drive, organise a designated driver

&nbsp;

Theme: Old Shanghai

Photography: <a href="https://www.facebook.com/todreamastory">To Dream A Story</a>

Please note:

This is an 18+ Event so please bring your photo ID

Stay tuned for updates on raffle prizes !

&nbsp;

为了庆祝中澳青年联合会西澳大学分会在过去几年的成长，我们将隆重推出本会今年的重头戏ACYA慈善晚会！

&nbsp;

穿着您最靓丽的老上海主题装束，享用着精致的鸡尾酒菜肴以及现场爵士音乐表演，快活逍遥，这必将成为您难以忘却的一夜！

&nbsp;

本次活动之所有盈利将全数捐献给“半边天基金会”(Half The Sky)。“半边天”是一个致力于帮助中国孤儿生计的慈善机构。更多信息请参看<a href="http://halfthesky.org/zh-hans">http://halfthesky.org/zh-hans</a>

&nbsp;

票价：

会员 95澳币

非会员 110澳币

&nbsp;

购票请前来ACYA在校园的摊位或者联系本会的委员们

&nbsp;

为了您的方便，我们向您提供网上售票服务。购票请登录<a href="http://events.ticketbooth.com.au/event/UWAACYAGala2014">http://events.ticketbooth.com.au/event/UWAACYAGala2014</a>

***会员票仅在西澳大学校园内出售

&nbsp;

&nbsp;

票价包含鸡尾酒菜肴及酒水饮料

***中澳青年联合会呼吁负责的酒精摄取

***请勿酒后驾车，如有需要，应事先指定司机

&nbsp;

主题：老上海

&nbsp;

摄影：To Dream A Story

&nbsp;

请注意：本活动参与者必须年满18周岁，请携带附有照片的身份证件

&nbsp;

敬请期待关于抽彩奖品的信息!