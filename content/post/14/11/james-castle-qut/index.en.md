{
  "title": "<!--:en-->James Castle QUT<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:20:13+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "JamesCastle",
      "type": "image",
      "src": "JamesCastle.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong></em>

James Castle	

<em><strong>Degree and University</strong></em>

Bachelor in International Business with Minors in Finance and Mandarin, Queensland University of Technology

<em><strong>Position in ACYA, and your University chapter </strong></em>

President, QUT Chapter

<em><strong>What is it you love most about ACYA? </strong>
</em>
I have formed many friendships across the world.

<em><strong>What is the best thing about ACYA? </strong</em>

Our mission is integral to Australia's future; it does not matter where your career is headed, the skills learnt and relationships formed at our Chapters will ensure success in any endeavour.
<!--:-->