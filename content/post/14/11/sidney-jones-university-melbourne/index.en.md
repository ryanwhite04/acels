{
  "title": "<!--:en-->Sidney Jones, University of Melbourne <!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:07:26+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Sid Jones",
      "type": "image",
      "src": "Sid-Jones.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong></em>

Sidney Jones

<strong><em>Degree and University</em></strong>

Juris Doctor (Postgraduate Law) – University of Melbourne.

<strong><em>Position in ACYA, and your University chapter </em></strong>

President, Melbourne University Chapter

<strong><em>What is it you love most about ACYA? </em></strong>

Although very cliché, the more you put into the ACYA community, the more it gives back to you. Throughout my time with ACYA, I have seized the opportunity to develop lasting friendships with a number of like-minded, motivated individuals. ACYA is special in that it facilitates the breaking down of cultural barriers and the coming together of personalities in a unique setting. This has enabled me to forge strong friendships which would perceivably not otherwise have been possible.

<em><strong>What is the best thing about ACYA?</strong></em> 

The variety of platforms on which ACYA allows its members to engage. The diversity of ACYA’s events, and the range of people to which they attract, provides for the opportunity to engage on a number of levels, whether that be on a social, educational or professional basis.
<!--:-->