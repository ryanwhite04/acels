{
  "title": " ACYA named finalist in the inaugural Australia-China Achievement Awards and attend State Dinner for Chinese President Xi Jinping",
  "author": "ACYA",
  "date": "2014-11-25T05:25:32+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACC awardLOGO_H (2)",
      "type": "image",
      "src": "ACC-awardLOGO_H-2-e1416892387897.jpg"
    },
    {
      "title": "unnamed",
      "type": "image",
      "src": "unnamed-e1416893574590.jpg"
    }
  ]
}
The Australia-China Youth Association (ACYA) is proud to be named as a finalist in the inaugural round of the Australia-China Achievement Awards, which are coordinated by the Australia-China Council.

The Awards showcase the leadership and creativity of Australian organisations and people in building stronger economic, cultural and education connections between Australia and China, and reflect the breadth, diversity and strength of Australia-China ties. 

The Awards were announced last week on 17th November by the Prime Minister in the Great Hall of Parliament House, at a State dinner hosted for Chinese President Xi Jinping.  With a seat at the dinner table, ACYA President Tom Williams said that it was fantastic for ACYA to receive such recognition on such a special occasion.

“It’s a great honour for ACYA to be recognised at the highest levels for our small contribution to the bilateral relationship”.

“We have have made a good start towards fostering the next generation of Australia-China talent, but there is still much to do”.

“I was very proud to represent ACYA at the State Dinner held for President Xi Jinping at the Great Hall in Parliament House. President Xi’s laudatory comments on the New Colombo Plan and desire that we can be creative with bilateral ties were very welcomed. We hope that ACYA continues to expand its role in providing opportunities for young Australians and Chinese to gain the knowledge, skills and networks to develop a more comprehensive relationship”

Special acknowledgement must also go to the Australia China Council, Australian National University, the China Studies Centre at the Sydney University, the Australian Studies Centre at Peking University and the Foundation for Australian Studies in China (FASIC), for without their on-going support, our achievements would not be possible.

The Australia-China Achievement Awards is a new national program administered by the Australia-China Council. Further information about the Awards and this year’s winners and finalists is available at http://www.dfat.gov.au/acc/awards.

Read more: 习近平：开创中澳关系更加精彩新篇章(http://t.m.youth.cn/?url=http://news.youth.cn/sz/201411/t20141114_6044542.htm&from=singlemessage&isappinstalled=0)

Read More: China’s Xi Jinping talks of deeper Australian ties ahead of G20 (http://m.afr.com/p/special_reports/opportunityasia/china_xi_jinping_talks_of_deeper_lOG48w0aSBvYf7ixp0u2rI?from=singlemessage&isappinstalled=0)

Australia-China Achievement Awards - Winners and Finalists announced

Media release

18 November 2014

I congratulate the winners and finalists of the inaugural round of the Australia-China Achievement Awards, announced by the Prime Minister on 17 November during Chinese President Xi Jinping’s visit.

The Awards showcase the leadership and creativity of Australian organisations and people in building stronger economic, cultural and education connections between Australia and China.

The Awards attracted significant interest from the Australian community with over 80 nominations received in a period of one month. The nominations reflect the breadth, diversity and strength of Australia-China ties.

Selected through a rigorous process across three categories of Arts, Entrepreneurship and Education, the winners of the inaugural round are:

Arts: Sydney Symphony Orchestra (NSW) for showcasing Australian arts and creativity to Chinese audiences and promoting Australia and Sydney as leading global cultural destinations

Entrepreneurship:  Bridestowe Estate (TAS) for pro-active and innovative market entry into China and promoting Tasmania as a destination for Chinese tourists

Education: Professor Max Lu (QLD) from the University of Queensland for exemplary leadership in advancing research and education relationship between Australia and China.

The finalists of the inaugural round are:
Arts: 
Warburton Arts Project and Warburton Community (WA); 
OzAsia Festival (SA); Lu Yiying (NSW); and 
Catherine Croll and Cultural Partnerships Australia (NSW).

Education: 
Australia-China Youth Association (National); 
Haileybury School (VIC); 
Asia Australia Mental Health (VIC); and 
StudyPerth (WA).

Entrepreneurship: 
Timothy Coghlan and the Australia-China Fashion Alliance (ACT - China); 
Australia-China Youth Dialogue (National); 
Shenzhen Aolinjia Import and Export Co. Ltd (VIC); and 
Royal District Nursing Service Limited (VIC).

Minister for Foreign Affairs
The Hon Julie Bishop MP

Media enquiries
Minister's office: (02) 6277 7500
DFAT Media Liaison: (02) 6261 1555


