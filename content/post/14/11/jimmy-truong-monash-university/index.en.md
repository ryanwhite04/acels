{
  "title": "<!--:en-->Jimmy Truong, Monash University<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:18:56+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Jimmy_Truong_ACYA_Testimonial_photo",
      "type": "image",
      "src": "Jimmy_Truong_ACYA_Testimonial_photo.jpg"
    }
  ]
}
<!--:en--><strong><em>Name</em></strong>

Jimmy Truong

<em><strong>Degree and University</strong></em>

Bachelor of Commerce (Finance) and Bachelor of Science (Microbiology) at Monash University, Clayton Campus

<em><strong>Position in ACYA, and your University chapter </strong>
</em>
President, Monash University Chapter

<em><strong>What is it you love most about ACYA? </strong></em>

The one thing that I love most about ACYA is the culture. ACYA is not a club focused only specifically at one university, but instead it broadens out across various universities in Australia and China. That being said, there will always be numerous activities running - whether educational, careers enrichment event or social gatherings. Members of ACYA Monash can freely attend events hosted by ACYA RMIT, Melbourne, La Trobe or Deakin, there is no limit and restriction on what you can learn at ACYA. It is a club to where everyone is welcome and build long lasting friendships.

<em><strong>What is the best thing about ACYA? </strong></em>

The best thing about ACYA are the opportunities it can provide for its members. These opportunities can come in a variety of forms - these include but not limited to: Study Abroad Scholarships, Working Abroad Opportunities in well-known and respected companies and organisations, volunteer opportunities (locally or abroad). But my personal favourite is the opportunity to meet people through events held by the 3 pillars - especially P2P events like AFL. Nothing is more satisfying than to explain the game to international students, helping them understand what AFL really is, and watch them understand and strive to attend more matches in the future.<!--:-->