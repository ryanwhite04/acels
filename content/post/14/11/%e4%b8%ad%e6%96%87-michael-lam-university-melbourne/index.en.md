{
  "title": "<!--:en-->Michael Lam, University of Melbourne<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:17:12+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Michael Lam",
      "type": "image",
      "src": "Michael-Lam.jpg"
    }
  ]
}
<!--:en--><em><strong>Name	</strong></em>

Michael Lam

<strong><em>Degree and University</em></strong>

Bachelor of Commerce – The University of Melbourne

<strong><em>Position in ACYA, and your University chapter </em></strong>

Secretary, Melbourne University Chapter

<em><strong>What is it you love most about ACYA?</strong> 
</em>
I love how ACYA is able to provide so many career and networking opportunities through amazing events for a variety of disciplines.
<strong>
<em>What is the best thing about ACYA? </em></strong>

The best thing about ACYA is meeting fellow members who are passionate about the Australia-China relationship and just getting to know them.

<!--:-->