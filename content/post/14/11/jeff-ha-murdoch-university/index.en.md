{
  "title": "<!--:en-->Jeff Ha, Murdoch University<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:19:38+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "100x100_jeffha",
      "type": "image",
      "src": "100x100_jeffha.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong></em>

Jeff Ha	

<strong><em>Degree and University</em></strong>

Juris Doctor, Murdoch University

<em><strong>Position in ACYA, and your University chapter </strong></em>

President, Murdoch Chapter

<strong><em>What is it you love most about ACYA? </em></strong>

ACYA's numerous publications, especially the monthly ChinaBites! It helps me keep up with interesting news from China, and I particularly enjoy learning a new '成语' (Chinese idiom) on a monthly basis!

<em><strong>What is the best thing about ACYA? </strong></em>

The best thing I find about ACYA is that it serves as a perfect platform for like-minded professionals with a passion for furthering Sino-Australian relationship. ACYA has an extensive and enviable network of professionals, which will no doubt prove to be valuable for all future endeavours for those who choose to utilise it. Additionally, ACYA's plethora of events and activities across both Australia and China welcomes all young professionals in furthering the vision of fostering closer cross-cultural ties between both countries.<!--:-->