{
  "title": "<!--:en-->Marc Lim, University of Western Australia<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:21:52+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "100x100_marclim",
      "type": "image",
      "src": "100x100_marclim.jpg"
    }
  ]
}
<!--:en--><strong><em>Name</em></strong>	

Marc Lim

<em><strong>Degree and University</strong></em>

Bachelor of Engineering, University of Western Australia

<em><strong>Position in ACYA, and your University chapter </strong></em>

President, UWA

<em><strong>What is it you love most about ACYA? </strong></em>

ACYA provides so many opportunities for a vast range of different people interested in the Australia-China space. We provide a relaxed environment for the melding of ideas and opinions leads to great discussions. Everyone from an assortment of backgrounds can come together to form long lasting friendships and professional networks, which is what I love about ACYA.

<em><strong>What is the best thing about ACYA? </strong></em>

Being apart of ACYA I have been able to develop a set of skills in leadership, management and teamwork that will directly translate into my professional career. Another great aspect of ACYA is the fact that it is an international organisation.

Being apart of this large network means that I can easily transition into many cities around Australia and China by tapping into the local ACYA chapters. Having access to the local ACYA network means that I can straight away be introduced to likeminded people and get settled right away.

These two aspects have to be the best points of ACYA for me.
<!--:-->