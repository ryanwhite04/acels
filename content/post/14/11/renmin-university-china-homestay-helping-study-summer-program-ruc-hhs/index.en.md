{
  "title": "<!--:en-->Renmin  University of China – Homestay Helping & Study Summer Program (RUC-HHS)<!--:--><!--:zh-->Renmin  University of China – Homestay Helping & Study Summer Program (RUC-HHS).<!--:-->",
  "author": "ACYA",
  "date": "2014-11-03T21:20:21+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "renmin",
      "type": "image",
      "src": "renmin-e1415049462271.png"
    },
    {
      "title": "RUCHHS",
      "type": "application",
      "src": "RUCHHS.pdf"
    }
  ]
}
<!--:en-->The Australia-China Youth Association is pleased to announce that the launch of the Renmin University of China – Homestay Helping &amp; Study Summer Program (RUC-HHS).

We are currently looking for applicants of an exceptional standard, and will be undergoing assessment based on the eligibility requirements:

• Aged from 18 – 30

• At least high school or higher Academic Background

• To be in good health, no criminal record

• With flexible and active attitude, like family life

• Have keen interest in China and Chinese culture

The RUC-HHS Program is a university-wide programme initiated to stimulate a culture of research for overseas students. Applicants selected to participate in this programme will have the opportunity to develop research interest and strong sense of cross-cultural intelligence. It covers wide variable activities related to Homestay, Study and Helping.

Real Chinese Family Experience: Unique and amazing experience to live with a real Chinese Family in an elite environment. Well educated and warm welcoming of western culture, they will treat you like a true family member. You may get surprised when you meet the host family!

Academic Modules: Applicants will study 4 courses which are 160 hours in total. Courses cover a wide range of topics and subject areas including Elementary &amp; Practical Chinese, Chinese Culture and Intercultural Communications.

Experiential Cultural Tours: Located in the heart of China, Beijing is home to China’s most famous historical sites and is a place rich in culture. Participants will have the opportunity to interact socially and culturally through organized events, such as: Great Wall, Forbidden City and Summer Palace.

If you are looking for an intriguing experience and want to take that next step in the Asian Century, then please feel free to email <strong>education@acya.org.au </strong>containing an up to date CV and cover letter; outlining your motivations, aspirations, and the ways in which you meet the above criteria.

As part of the RUC-HHS Program, you’re guaranteed full scholarship and free accommodation and planned exciting and fun weekend activities to the best tourist spots the city has to offer.

There are 24 scholarship spots left for the Winter Term. Each scholarship for the Winter Program is around $4000AUD.

Winter Program: Dec 1 - Feb 28

Applications for the Winter Program are open now and close on 13 November 2014.

We hope to hear from you shortly!

Regards,

The National Education Team

RUC-HHS official flyer: <a href="http://www.acya.org.au/wp-content/uploads/2014/11/RUCHHS.pdf">RUCHHS</a><!--:--><!--:zh--><a href="http://www.acya.org.au/wp-content/uploads/2014/11/RUCHHS.pdf">RUCHHS</a><!--:-->