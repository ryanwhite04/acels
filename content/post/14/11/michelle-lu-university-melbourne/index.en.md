{
  "title": "<!--:en-->Michelle Lu, University of Melbourne <!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:18:23+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "MichelleLu",
      "type": "image",
      "src": "MichelleLu.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong></em>	

Michelle Lu

<em><strong>Degree and University</strong></em>

Bachelor of Commerce, University of Melbourne

<em><strong>Position in ACYA, and your University chapter </strong></em>

Education Officer, University of Melbourne Chapter

<strong><em>What is it you love most about ACYA? </em></strong>

What is it you love most about ACYA? I love that I have met and befriended so many people with diversity of experience and background through ACYA; that I have discovered many scholarships and overseas opportunities that I hadn’t previously known about.

<em><strong>What is the best thing about ACYA? </strong></em>

What is the best thing about ACYA?  One of the best things about ACYA is that it is driven by students - while we all have university and work commitments, we also have a great interest and enthusiasm for Australia-China relations that we actively pursue outside of academics- and many amazing events have come out it: Yumcha lunches, BLYW, a great website packed with resources etc.
<!--:-->