{
  "title": "<!--:en-->Jason Wang, President UQ<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:20:45+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "jasonwangacya1",
      "type": "image",
      "src": "jasonwangacya1.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong></em>	

Jason Wang

<em><strong>Degree and University</strong></em>

Bachelor of Law and Bachelor of Commerce

<em><strong>Position in ACYA, and your University chapter </strong></em>

President, UQ Chapter

<em><strong>What is it you love most about ACYA?</strong> </em>

Educating young people about the cultural sensitivities that exist in today's world.

<em><strong>What is the best thing about ACYA? </strong></em>

Meeting a family of people who will support and encourage each other through life's adventures.
<!--:-->