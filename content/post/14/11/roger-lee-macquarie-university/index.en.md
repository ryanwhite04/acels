{
  "title": "<!--:en-->Roger Lee, Macquarie University<!--:-->",
  "author": "ACYA",
  "date": "2014-11-02T17:21:14+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [
    "Testimonials"
  ],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Processed with VSCOcam with a5 preset",
      "type": "image",
      "src": "Roger-Lee.jpg"
    }
  ]
}
<!--:en--><em><strong>Name</strong>	</em>

Roger Lee | 李學滔

<strong><em>Degree and University</em></strong>

Bachelors of International Studies & Laws @ Macquarie University

<em><strong>Position in ACYA, and your University chapter </strong></em>

President, Macquarie University

<em><strong>What is it you love most about ACYA? </strong></em>

The family-feel within the ACYA community where everyone shares upcoming academic and professional opportunities as well as their insights with one another. It is this knowing that others share similar interests that strengthens my enthusiasm for Oz-China relations.
	
<strong><em><strong>What is the best thing about ACYA</strong>? </em></strong>

Fostering bilateral relations, which meant I already had a network of ACYA friends when I travelled to Beijing to show me around. Moreover, in the spirit of #auschina, I have also brought together Australian and Chinese cuisines at events to come up with combinations such as ‘lamb-ingtons’ (Xinjiang lamb skewers accompanied with lamingtons) and ‘Dumplova’ (dumplings followed by Pavlova)! <!--:-->