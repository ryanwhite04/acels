{
  "title": "Introducing the ACYA Calendar; all ACYA Chapter events at a glance!",
  "author": "ACYA",
  "date": "2014-06-06T01:21:50+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "jajs",
      "type": "image",
      "src": "jajs.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/06/jajs.jpg"><img class="wp-image-13929 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/06/jajs.jpg" alt="jajs" width="506" height="293" /></a>

ACYA is proud to present the launch of the ACYA Calendar!

The ACYA calendar is a comprehensive, at-a-glance calendar, aggregating events from across all chapters in China and Australia and major ACYA departments. The Calendar can be found on the P2P page of the ACYA website (http://www.acya.org.au/p2p/). The Calendar is a great platform to promote and view what events are happening in the ACYA space.

The calendar will provide a one-stop shop for students at particular universities with an ACYA presence to access all future happenings related to that Chapter. Chapters will be constantly updating the calendar as new developments are launched.
To improve management of the calendar there are 4 categories into which events are divided:

<strong><em>Opportunities</em></strong> – ACYA affiliated or non-ACYA deadlines and education opportunities (e.g. internships)
<strong><em>Australian Chapters</em> </strong>– individual chapter events and inter-chapter for Australian chapters
<strong><em>Chinese Chapters</em></strong> - individual chapter events and inter-chapter for Chinese chapters
<strong><em>General P2P</em></strong> – ACYA national events, meetups and projects

Events will be sorted and colour-coded accordingly and automatically on the Calendar.