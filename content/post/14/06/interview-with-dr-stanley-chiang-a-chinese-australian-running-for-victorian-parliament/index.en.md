{
  "title": "In conversation with Dr Stanley Chiang, Candidate for Victorian State Parliament",
  "author": "ACYA",
  "date": "2014-06-11T11:46:58+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "stanley",
      "type": "image",
      "src": "stanley.jpg"
    },
    {
      "title": "Dr_Stanley_Chiang",
      "type": "image",
      "src": "Dr_Stanley_Chiang.jpg"
    },
    {
      "title": "chiang",
      "type": "image",
      "src": "chiang.jpg"
    }
  ]
}
<h2><a href="http://www.acya.org.au/wp-content/uploads/2014/06/stanley.jpg"><img class=" wp-image-14555 alignleft" src="http://www.acya.org.au/wp-content/uploads/2014/06/stanley.jpg" alt="stanley" width="117" height="157" /></a></h2>
A migrant from Mainland China, Dr. Stanley Chiang is now running as an Australian Labor Party candidate in the forthcoming Victoria State elections. As well as being a medical practitioner, Stanley has also acted as the City of Darebin’s local mayor. He passionately believes in the engagement of Chinese Australians in political affairs.
<h4><strong>What has motivated you to pursue a public political career, both at a local and state level?</strong></h4>
I came to Australia as an overseas student. Although I was very successful in my studies and later became a successful doctor, I still encounter racist prejudice from time to time. I also find that many migrants (especially migrants of Chinese cultural background) tend not to speak out and fight for their rights as all Australians are entitled to. It is for these reasons that I felt compelled to come out and act as a voice for those whose concerns are often not heard. I got elected as a local government Councillor and Mayor. Now I am standing for Upper House in the upcoming State Election as an endorsed Labor candidate.
<h4><strong>How has your Chinese heritage influenced your political perceptions/ethos, whether at a local, state or federal level? During your career, have prominent issues pertaining to the Chinese-Australian community changed in any way?</strong></h4>
As mentioned above, I believe we as Chinese Australians should take a very active role and be interested in the mainstream affairs. Perhaps as a result of the White Australia policy in the past, many Asians/Chinese in Australia don't feel at home, and find it hard to integrate into the mainstream society. This I must say has improved a lot in recent years, due to efforts from both the Chinese community in Australia as well as the fairer policies nowadays (at local, state as well as federal government levels). There is however still room for improvement.
<h4><strong>In your opinion, how is cross-cultural understanding best created within local Australian communities?</strong></h4>
It needs input and efforts from both the mainstream society, especially various government levels as well as ethnic/multicultural communities. It is also very important to foster better understanding and communications between different ethnic/multicultural communities. Governments at all three tiers need to facilitate this as NGOs usually don't have the resources to be able to do so.
<h4><strong>What importance do you see in the promotion of Asian studies and languages within Australian school curricula? What roles do you see for Australian youth within politics?</strong></h4>
Very important, perhaps even vital for us as a nation to survive and prosper in this day and age, the 'Asian Century'. Australian youth are often more open-minded and are keen to embrace new ideas/concepts, so encouraging young Australians (from all walks of life and from different cultural backgrounds) to participate in politics and policy-making would be very beneficial for the future of our country.
<h4><strong>Lastly, tell us three interesting facts about yourself!</strong></h4>
<ol>
 	<li>I have a rather complicated/interesting background: Born and raised in Shanghai, China until just before graduation from high school. Migrated to Japan with my parents and finished my secondary education there. Came to Australia to pursue my tertiary studies. I completed my first degree in Biological Science (with Honours); then studied medicine to become a specialist GP doctor. Having lived in this wonderful country for more than 30 years I can now truly and passionately call Australia HOME!</li>
 	<li>I am the first Mainland China born Councillor to be elected in Australia, and perhaps the first ever in the world too.</li>
 	<li>I take pride in my linguistic skills, being able to speak English as well as Mandarin Chinese, Shanghainese (Shanghai dialect), Cantonese and Japanese. All very fluent, and some at professional level.</li>
</ol>

<hr />

&nbsp;

<em>From the May 2014 edition of ACYA AustraliaBites. Interview conducted by Catherine Yen.</em>