{
  "title": "ACYA-Peking University Australian Studies Centre Internship Program",
  "author": "ACYA",
  "date": "2014-06-12T05:51:21+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/06/pkuascrelease.jpg"><img class=" wp-image-14780 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/06/pkuascrelease-300x173.jpg" alt="[cml_media_alt id='14780']pkuascrelease[/cml_media_alt]" width="373" height="215" /></a>

The Peking University Australian Studies Centre is an academic institute for the study of Australian history, politics, international relations, economics, society, culture, literature and language. The objective of the Centre is to strengthen the Australia-China relationship through advancing Australian Studies at Peking University and throughout China. To this end, the Centre hosts the BHP Billiton Chair of Australian Studies, administers Australian Studies courses at PKU, organises Australia-China conferences and roundtables, and operates this Internship Program.

An essential part of the missions of both the Australian Studies Centre and ACYA is to encourage the next generation of Chinese youth to develop an interest in Australia as a country and subject of academic inquiry, as well as to support the endeavours of young Australians studying and working in China. Established in early 2013, the Program is a unique internship that enables Australian and Chinese students to gain valuable practical academic experience working at the forefront of Australia-China relations at the Australian Studies Centre on campus at Peking University in Beijing. For more information about the Centre please visit the Centre website (<a href="http://pkuasc.fasic.org.au/">http://pkuasc.fasic.org.au</a>).

Interns are provided with the opportunity to pursue an Australia-China research project of their own design under the supervision of the BHP Billiton Chair of Australian Studies Professor David Walker, a leading authority on Australian perceptions of Asia and author of <em>Anxious Nation</em>, <em>Australia’s Asia </em>and <em>Not Dark Yet</em>. The research project can take the form of a conference presentation, short paper, or any other form of research approved by the Chair.

<strong>How to Apply</strong>

Applications should include a curriculum vita and a one-page cover letter outlining your motivation for applying and a brief research project proposal. Please also nominate your preferred starting and finishing dates for the internship. Successful applicants will be notified within two weeks of the application deadline. For more information, please visit <a href="http://www.acya.org.au/pkuasc-internship-program/">http://www.acya.org.au/pkuasc-internship-program/</a>
Please email your application to <a href="mailto:careers@acya.org.au">careers@acya.org.au</a> before <strong>5pm Monday 23 June 2014 (China Standard Time).</strong>