{
  "title": "<!--:en-->Australian Football League Diversity Social Media Officer (Volunteer Internship)<!--:-->",
  "author": "ACYA",
  "date": "2014-12-21T11:53:51+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Social Media Intern Position Description",
      "type": "application",
      "src": "Social-Media-Intern-Position-Description.doc"
    },
    {
      "title": "Australian_Football_League",
      "type": "image",
      "src": "Australian_Football_League-e1419162812828.png"
    }
  ]
}
<!--:en-->ACYA is pleased to support AFL Diversity in bringing together many people from diverse backgrounds. The AFL is working closely with state football bodies to build strong working relationships with many diverse communities to identify barriers and to develop strategies to encourage involvement.

The AFL is Australia’s premier sporting organisation supporting a constantly evolving national competition which has experienced rapid growth over the past 10 years. The AFL currently has 600 permanent employees across the four state bodies, AFL Vic, NSW/ACT, QLD, NT and at AFL House. The AFL organisation is responsible for growing and developing Australian Football across Australia.

AFL Diversity has a job opening for a social media officer (volunteer internship) based at AFL House, 140 Harbour Esplanade, Docklands 3008.

The Social Media officer will assist in developing campaigns which increase brand awareness, generate inbound traffic as well as community engagement and moderation of our social media pages and promote AFL Diversity programs. The Social Media Officer is a highly motivated individual with experience and a passion for being part of the team that implements the social media content strategy creates relevant content and encourages community participation.

Please see below for key criteria, performance indicators and responsibilities.

<a href="http://www.acya.org.au/wp-content/uploads/2014/12/Social-Media-Intern-Position-Description.doc">AFL Social Media Intern Position Description</a>

To apply, please send a CV and cover letter to Afl@acya.org.au by <strong>12 January 2015 </strong><!--:--><!--:zh--><a href="http://www.acya.org.au/wp-content/uploads/2014/12/Social-Media-Intern-Position-Description.doc">Social Media Intern Position Description</a><!--:-->