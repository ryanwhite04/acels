{
  "title": "<!--:en-->ACYA Newsletter: October/November 2014<!--:-->",
  "author": "ACYA",
  "date": "2014-12-25T11:54:16+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->ACYA is proud to announce we were named as a finalist in the inaugural Australia-China Achievement Awards!

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=56b5b3272d">Please click here to read ACYA Newsletter: October/November 2014 edition</a><!--:-->