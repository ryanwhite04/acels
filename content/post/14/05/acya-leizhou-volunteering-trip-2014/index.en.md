{
  "title": "ACYA Leizhou Volunteering Trip 2014!",
  "author": "ACYA",
  "date": "2014-05-17T09:44:08+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Applications Open_v3",
      "type": "application",
      "src": "Applications-Open_v3.pdf"
    },
    {
      "title": "Leizhou Trip Application Form",
      "type": "application",
      "src": "Leizhou-Trip-Application-Form.pdf"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/05/ajsd.jpg"><img class="wp-image-3015 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/05/ajsd.jpg" alt="ajsd" width="461" height="267" /></a>

Owing to the great success last year, we are pleased to bring to you the Leizhou Volunteering Trip for 2014!

The Leizhou Volunteering Trip is a one-week volunteer English teaching program in Leizhou, China. Through this trip organised by ACYA and Leizhou College Students Volunteer Association (LZCVA), volunteers will teach English to Leizhou high school students. It will also allow participants to glimpse a part of the ‘real China’ far from the tourist trail and first-tier cities, and contribute to a rural community in such a meaningful way. Not only is it a fun and rewarding experience, it also aims to promote Aussie culture to our Chinese counterparts, develop strong relationships with the young Chinese people and lay the foundations for a bright future of cooperation between ACYA and LZCVA.

<strong>Date:</strong> Monday, July 21, 2014 to Monday, July 28, 2014

<strong>Location:</strong> Leizhou, China

Accommodation will be provided and transportation fees will be fully funded.
<h1>Application:</h1>
Ten humorous and generous volunteers are needed, responsible for English teaching of subjects either ‘oral communication’ or ‘cultural difference’. Interested participants should download the application form attached below, and submit it to education@acya.org.au <strong>by 26th May 2014</strong>.
<h1>Contact:</h1>
For any enquiries about the trip, please free feel to contact

Michael Twycross – ACYA National Education Director:
Tel: +61 404 501 393
Email: <a href="mailto:education@acya.org.au">education@acya.org.au</a>

&nbsp;

<a href="http://www.acya.org.au/wp-content/uploads/2014/05/Leizhou-Trip-Application-Form1.pdf" target="_blank">Click here to download the Application Form (PDF)</a>

<a href="http://www.acya.org.au/wp-content/uploads/2014/05/Applications-Open_v31.pdf" target="_blank">Click here to download the official flyer</a>

&nbsp;

<a href="http://www.acya.org.au/wp-content/uploads/2014/05/asad.jpg"><img class="wp-image-3016 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/05/asad-724x1024.jpg" alt="asad" width="400" height="565" /></a>