{
  "title": "In conversation with James Hudson, CEO of the Australia China Business Council NSW Branch",
  "author": "ACYA",
  "date": "2014-05-20T07:08:49+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "dude",
      "type": "image",
      "src": "dude.jpg"
    },
    {
      "title": "Screen Shot 2014-05-20 at 1.35.05 pm",
      "type": "image",
      "src": "Screen-Shot-2014-05-20-at-1.35.05-pm.png"
    }
  ]
}
<strong><a href="http://www.acya.org.au/wp-content/uploads/2014/05/Screen-Shot-2014-05-20-at-1.35.05-pm.png"><img class="size-thumbnail wp-image-3029 alignleft" src="http://www.acya.org.au/wp-content/uploads/2014/05/Screen-Shot-2014-05-20-at-1.35.05-pm-150x150.png" alt="Screen Shot 2014-05-20 at 1.35.05 pm" width="150" height="150" /></a></strong>
<div style="color: #000000;"> <strong>What led you to study Chinese? </strong></div>
<div style="color: #000000;"></div>
<div style="color: #000000;">I grew up in regional Australia and my parents are university academics. They run a few programs for international students coming out here to study English. From the age of 13-16, we would have the odd student from Hong Kong staying with us because they couldn’t find a host family. I think I would always watch them write postcards home and was completely fascinated by the characters. I did not realise, at that time, that learning those Chinese characters would become all-consuming!</div>
<strong>I think when we talk about China we can sometimes just focus on its negatives, like the smog or traffic in Beijing. So from your time living there, what would you say you enjoyed the most about the city and China more broadly?</strong>
<p style="color: #666666;"><span style="color: #000000;">Beijing is a fantastic city and I still very much enjoy going back there to visit.  Living in Beijing in the lead up to the Olympics was great; the energy of the city was incredible.  The food, meeting people from all around the world doing interesting things with their lives, and the price of taxis are probably the three things I miss most!</span></p>
<p style="color: #666666;"><span style="color: #000000;"><strong>Before your current position, you were at the CSIRO as the China and East Asia Adviser; can you expand a bit more about what sort of projects that entailed as an organisation that has a scientific focus?  </strong></span></p>
<p style="color: #666666;"><span style="color: #000000;">My previous role at CSIRO had three primary functions: managing institutional research partnerships between CSIRO and organisations and Government bodies in Asia, administering funding programs for collaborative research programs with Asia, and supporting commercial transactions with Asia.  China has invested significant amounts of funding to develop its innovative capacity and it is only through deep collaboration between countries that the world will be able to address so many of the challenges we face.  The research relationships CSIRO had with China, and Asia more broadly, make up an important part of the Australia-China bilateral relationship.</span></p>
<p style="color: #666666;"><span style="color: #000000;">It was a great privilege to work for Australia’s leading science research organisation and to have an opportunity to understand the quality and breadth of their research and the impact it has on Australia and beyond.</span></p>
<p style="color: #666666;"><span style="color: #000000;"><strong>One of the typical areas when people think about working in China there is more in the business world, diplomacy or international relations – not so much in science. Do you think there is a whole breadth of area being opened up for these scientific partnerships? </strong></span></p>
<p style="color: #666666;"><span style="color: #000000;">I think people majoring in Chinese or hoping to pursue a career in international relations need to think outside the box.  There are few sectors China does not ‘touch’ in some capacity and thinking about how your skills may apply to support these sectors is worthwhile.  Although university graduates tend to default to hoping for a career at the Department of Foreign Affairs and Trade, there are many other options beyond DFAT, including in other departments and agencies and in a wide range of business sectors, which can provide fulfilling and interesting careers.</span></p>
<p style="color: #666666;"><span style="color: #000000;"><strong>Specifically into one of your areas of interest, and it’s something you tweet fairly frequently – food security. Do you think we should be looking at a potential Australia-China FTA through the prism of China securing its food security and seeing Australia as part of its answer? </strong></span></p>
<p style="color: #666666;"><span style="color: #000000;">I don’t think we should look at an FTA through a prism of food security, but agriculture more broadly is a really important component of the FTA – both from an inbound investment perspective and from an export perspective. I believe Australia has a lot of potential to serve China’s growing middle class and their increasing protein demands and also the demand for cleaner, higher quality food. And I think Australia can play a good part in that. I think that the free trade agreement will hopefully reduce a lot of barriers for Australian exporters.</span></p>
<p style="color: #666666;"><span style="color: #000000;">The reality is that the world needs to produce as much food in the next 60 years as we have produced since the beginning of human history and if one of every five people are Chinese on this planet then China is an important part of the global food security puzzle- and I believe Australia can contribute in addressing those challenges.</span></p>
<p style="color: #666666;"><span style="color: #000000;"><strong>Just on the New Colombo plan, what kind of initiatives would you ideally like to see coming out of that? </strong></span></p>
<p style="color: #666666;"><span style="color: #000000;">I think what I would like to see out of the New Colombo Plan is deep business involvement in the program. Involving business to not only assist those students in getting out and spending time in Asia but also providing them opportunities for work in those companies, to understand what it is like to work in Asia.</span></p>
<p style="color: #666666;"><span style="color: #000000;">I think it will be a fantastic program, but one of the things about investing in younger people is that it is a long-term investment and the impact is not easy to evaluate and show the dividends. But I am pretty confident over the next couple of decades the program will prove to be an excellent investment of Australia’s resources.</span></p>
<p style="color: #666666;"><span style="color: #000000;"><strong>Do you have a recommendation? A book, film or an article about China, something that captured your attention and that you would like to share with others?</strong></span></p>
<p style="color: #666666;"><span style="color: #000000;">Much of the China-related news and media I read comes through Twitter.  Instead of recommending a book or article, here are ten good tweeters I follow:</span></p>

<ol style="color: #666666;">
 	<li><span style="color: #000000;">Patrick Chovanac (@prchovanec): Patrick is a former business professor at Tsinghua University in Beijing and has some excellent insights into the Chinese economy.</span></li>
 	<li><span style="color: #000000;">Kaiser Kuo (@KaiserKuo): Kaiser is a Chinese American writer and guitarist living in Beijing.  He is currently the Communications Director at Baidu.  His tweets are always entertaining and a good source of what is happening ‘on the ground’ in Beijing, particularly in the technology sector.</span></li>
 	<li><span style="color: #000000;">Malcolm Moore (@MalcolmMoore): Malcolm is the Beijing Correspondent for the UK’s Daily Telegraph.</span></li>
 	<li><span style="color: #000000;">Ray Kwong (@RayKwong): Ray is a US-Asia relations consultant and a senior advisor to the University of California’s US-China Institute.</span></li>
 	<li><span style="color: #000000;">Gady Epstein (@GadyEpstein): Gady is a China correspondent for The Economist based in Beijing.</span></li>
 	<li><span style="color: #000000;">Bill Bishop (@niubi): Bill is well known in the China domain publishing the regular Sinocism email newsletter covering off on China-related news.</span></li>
 	<li><span style="color: #000000;">Christine Lu (@christinelu): Christine is a cross border China business and investment consultant based in Hawaii and the CEO/Co-Founder of Affinity China.  Christine has a very good feel for what is happening in China, particularly in the technology area.</span></li>
 	<li><span style="color: #000000;">Scott Murdoch (@murdochsj): For the Australian angle, Scott is The Australian newspaper’s China correspondent based in Beijing.</span></li>
 	<li><span style="color: #000000;">Tim Harcourt (@TimHarcourt): Although he doesn’t just tweet about China, Tim does have some interesting insights into the global economy.  Tim is also the author of The Airport Economist, which is also worth reading.</span></li>
 	<li>William Moss (@imagethief):  Will is a public relations veteran with plenty  of China experience.  He has recently returned to his native US.</li>
</ol>
<div style="color: #000000;"><em>This interview was conducted by Tyler Gleason and featured in the April 2014 issue of AustraliaBites.</em></div>