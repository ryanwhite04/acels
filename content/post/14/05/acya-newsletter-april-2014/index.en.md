{
  "title": "ACYA Newsletter April 2014",
  "author": "ACYA",
  "date": "2014-05-18T08:13:35+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
Hello to all!

We’re pleased to announce the April edition of the ACYA Newsletter is now available to be viewed online.

Please click the link below!

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=b460b3d27b">Newsletter April 2014 </a>