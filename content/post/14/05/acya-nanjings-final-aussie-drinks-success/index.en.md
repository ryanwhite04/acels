{
  "title": "ACYA Nanjing's Final 'Aussie Drinks' Success ",
  "author": "ACYA",
  "date": "2014-05-13T07:33:16+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "drinks",
      "type": "image",
      "src": "drinks.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/05/drinks.jpg"><img class="wp-image-2951 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/05/drinks.jpg" alt="drinks" width="419" height="243" /></a>

The ACYA Nanjing Chapter recently held their final monthly ‘Aussie Drinks’ event for the semester, which received an enthusiastic reception amongst locals and internationals alike. The drinks aim to provide an informal environment in which to socialise and network with young professionals and students alike. While the focus of ACYA is indeed promoting cross-cultural understanding between young Chinese and Australians, encouraging a dialogue between people from all over the world is one of the best ways to express our organisation’s mission and create opportunities for members.

Many formal exchange students to Australia were in attendance, sparking thoughtful and inspired conversation about Australia-China relations. In addition to this we ran a fund raising raffle, the prizes of which are generously provided by La Villa and its staff. The money collected is reinvested into Chapter events to make them bigger and better than ever.

The Nanjing Chapter is proud to announce a ground-breaking collaboration with the John Hopkins Nanjing Center – the first fruits of which will be a joint BBQ with ACYA to be held within the Center, providing interested ACYA members with the chance to have a tour and meet with staff and students.