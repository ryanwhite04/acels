{
  "title": "Introducing ACYA’s Careers Portal",
  "author": "ACYA",
  "date": "2014-05-13T05:19:50+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "portal",
      "type": "image",
      "src": "portal1.jpg"
    }
  ]
}
<a href="http://www.acya.org.au/wp-content/uploads/2014/05/portal1.jpg"><img class="wp-image-2937 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/05/portal1.jpg" alt="portal" width="511" height="296" /></a>
http://www.acya.org.au/careers-portal/
ACYA is proud to present the ACYA Careers Portal, a new initiative designed to provide ACYA members greater access to work, freelance and internship opportunities within the Australia China space. The Portal will be constantly updated with the newest and most relevant job opportunities from various industries and sectors in both Australia and China. The Careers Portal will act as a central hub for all Australia China related careers, a “one-stop-shop” where ACYA members including students and professionals can easily access and clearly evaluate the different work opportunities to find the listing which suits their individual backgrounds and interests. Companies and employers are able to utilise the portal to advertise opportunities, tailor their listings and have gain unrivalled exposure to a talented pool of over 5,000 students and young professionals who are dedicated to fostering greater relations between Australia and China. Please contact ACYA National Careers Director Aimee Yi (careers@acya.org.au) if you have any questions relating to the careers portal. Alternatively please contact Aimee if you would like an opportunity listed on the Careers Portal.