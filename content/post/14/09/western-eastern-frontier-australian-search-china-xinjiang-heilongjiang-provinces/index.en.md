{
  "title": "From the Western to Eastern Frontier: An Australian in Search of China in Xinjiang and Heilongjiang Provinces",
  "author": "ACYA",
  "date": "2014-09-29T16:14:05+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<div class="page" title="Page 48">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#alice">Alice Dawkins (温丽丽)</a></h2>
</div>
</div>
<div class="layoutArea">
<div class="column">

If Beijing is the central source of China's vast network of interlocking cultures and customs, Xinjiang (Kashgar) and Heilongjiang (Harbin) stand as important tributaries on the far western and eastern points respectively. Distanced from Beijing by both kilometres and identity, if you are seeking the China of <em>taijiquan</em> (Tai Chi), <em>wulong</em> (dragon dances), and pandas doing <em>gongfu</em> (Chinese martial arts), you are not going to have much luck in these unique parts of the world.

In Harbin, you can slurp richly flavoured borscht, knock back smooth-as-silk vodka direct from Moscow, spot Russian signs dotted across the city, and find scores of buildings simply dripping with traditional Russian architecture. These buildings are unfortunately all now slowly crumbling into dereliction, their past majesty confined to grainy black and white photographs scattered around overpriced antique shops. Should you have even a vaguely Caucasian face, the old <em>dongbei</em> (North- Eastern Chinese) man sitting on the sidewalk will call out "<em>preevy-et!</em>" (hello!) and other Russian greetings as you walk along the road.

In Kashgar, you will catch the whiff of lamb kebabs cooking on every street corner and fill your hungry stomach with buttery, doughy Middle Eastern-style bread. You will see women adorned with colourful headscarves embroidered in ethereal gold thread – women who fill the streets as they do their groceries at the sunny open-air markets. Strolling through the busy crowds in the Sunday livestock market, you will hear not a word of standard Mandarin spoken, and the eyes that glance at you with casual curiosity are not Han Chinese eyes, but almond-shaped Central Asian eyes of blue, green, or hazel. Watching the old men with majestically long white beards eating Xinjiang-style ramen noodles together, the younger men body-wrestling with a herd of stubborn cattle, and a group of wives making lunch on trestle tables, the world stands still for a moment with a sense of timeless community atmosphere, the same buzz felt at fundraiser barbecues and kids' weekend football games back home.

</div>
<div class="column">

The living energy and sheer vivacity of another world that thrive in the air of Kashgar's bazaars and winding old streets have long since departed Harbin. With Harbin rapidly elevating its position as a regional hotspot and talk-of-the-town in northern China, the Russians and other ethnicities of yesteryear's Heilongjiang Province are increasingly scarce. Across the world, Harbin is better known for its grandiose annual ice sculpture festival than for the legacies of Russian culture left in parts of the old city. In many ways, Harbin‘s historical place in China as 'The Moscow of the Far East' has been consigned to history. The spectacular Byzantine-style <em>Sheng Suofeiya Jiaotang</em> (Saint Sophia Cathedral) in the city centre now welcomes any layman through its door as an architectural museum. The remaining synagogues in the city have either been transformed into youth hostels and trendy cafes or just left to be, just now without a Jewish population to serve. A sign at the largest remaining synagogue mentions that the last known Jewish Harbiner died in the 1980s. It is a triste moment for a place that once boasted what was considered the largest Jewish population in the Far East.

</div>
</div>
</div>
<div class="page" title="Page 49">
<div class="layoutArea">
<div class="column">

Speak to any local in Kashgar and they will undoubtedly tell you that the city is unrecognisable from what it was even just three years ago. A number of areas of the historic old city have been demolished to make way for grey, homogenous towers that have begun to symbolise the modern era of a frenetic and faceless Chinese construction style. For many Han Chinese, the old city areas of Kashgar are not seen as a desirable place to visit. As I took the twenty-seven hour train ride from Urumqi, Xinjiang's capital, to Kashgar, I started up a conversation with a lively and wonderfully opinionated twenty-something Han Chinese female passenger. But upon hearing of my travel plans, she stared incredulously back at me and told me quite frankly that if I stayed in the <em>Weizuren</em> (Uyghur) part of town, I would undoubtedly be raped, pillaged, and plundered by all of its locals. She went out of her way to drive me from the Kashgar train station to my accommodation in the heart of the old city alleyways, and sat locked in her car glancing warily at her surroundings, refusing to drive off until she saw me safely enter the front door of my hostel.

Similarly, a chatty taxi driver in Urumqi told me frankly: "We Han Chinese and the Uyghurs used to be like brothers. But because of the last few years' events, now there is no such relationship". It is a point worth considering that Muslim Uyghurs and Han Chinese co-existing in such close proximity in cities like Kashgar is seen as so remarkable by outsiders. Travel further north in Xinjiang to Tulufan (Turpan), a town deep in the Taklamakan Desert, and you will find Buddha grottoes and towering minarets, both equally as ancient, standing just a few kilometres away from each other.

Records of Russian Jews in the history of Heilongjiang and Harbin are certainly not quite as prominent as those of Uyghurs in Xinjiang, but the Russian Jews' influence on the area is profound. Harbin is a town that was originally a campsite for workers on the China Eastern Railway, a shortcut of the celebrated and romanticised Trans-Siberian Railway. It is humbling to consider that what is now a bustling metropolis, of giant luxury fashion stores, sumptuously lit up tower blocks, and neat cobblestoned pedestrian streets, began as an outpost constructed by Russian railroad workers just last century. You have to look for it at a deeper level, but in some aspects Harbin does exhibit features of a ghost town, abandoned not in population and resources, but rather in ethnic diversity. The once significant numbers of Russians may have dwindled, but typical Russian hats and fur accessories remain on shop shelves. Russian restaurants, furnished like scenes out of a Chekhov play, continue to serve delicious, old- fashioned fare to eager clientele, but that waitress dressed in dubiously traditional Russian costume will in fact be Chinese. Take a quick turn off the main street and you will find the grounds of the Jewish school remain intact, but the schoolyard is deserted, with only the ghosts of youthful shrieks and laughter.

</div>
<div class="column">

Due to its location, Harbin was a meeting point for both the direct and indirect effects of a world's worth of events in twentieth century history: the Russian Revolution and Civil War, the Japanese invasion in China, European anti-Semitism, the rise of the Communist Party, and the Cultural Revolution. These events combined signalled the systematic departure of Harbin's Jewish, and to some extent non-Jewish, Russian residents to other cities across China and eventually to other countries.

The historical context is very different, but the current situation of urban development and "Hanification" in Kashgar begs the question of whether Kashgar is also on its way to losing the influence and significance of its ethnic groups. Any traveller to the town on a Friday can witness the sizable square around the <em>Aitigaer</em> (Id Kah Mosque) fill to the periphery with Uyghur worshippers. It's an incredible sight and homage to the notion that religion is a strong element of quotidian life throughout Xinjiang. To an outside observer, however, one cannot shake the feeling that this snapshot of culture comes with a use-by date. With the growing numbers of Han Chinese in Xinjiang cities like Kashgar, and a strong march forward by the leadership to modernise the area, the elephant in the room of the Uyghur culture's future sustainability cannot be ignored.

</div>
</div>
</div>
<div class="page" title="Page 50">
<div class="layoutArea">
<div class="column">

In fifty years' time, will this image of the Kashgar Uyghurs at the Id Kah mosque only remain as a creased photograph, and in the memories of the old, like the experiences of the Russian Jews of Harbin?

</div>
<div class="column">

<em>With thanks to the endless numbers of taxi drivers, hostel owners, food stall sellers, and young people in both Xinjiang and Heilongjiang, as well as my teachers, who all put up with my clumsy spoken Chinese and discussed many of these issues with me at length. </em>

<hr />

<strong>This article appears in the <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20of%20Australia-China%20Affairs%202012.pdf">2012 ACYA Journal of Australia-China Affairs (pp. 48-50)</a>. It was awarded the ACYA Journal Opinion Article (English) Prize.</strong>

<em id="alice">Alice Dawkins is an Asia-Pacific Studies/Law student at the Australian National University. She has recently returned from a year studying abroad at Peking University on a Chinese Bridge Scholarship.</em>

</div>
</div>
</div>