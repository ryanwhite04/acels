{
  "title": "REMINDER TO REGISTER: the 2nd FASIC Australian Studies in China conference ",
  "author": "ACYA",
  "date": "2014-09-03T07:35:08+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "2nd-FASIC-Australian-Studies-in-China-Conference-TheBigPicture-Sept2014",
      "type": "application",
      "src": "2nd-FASIC-Australian-Studies-in-China-Conference-TheBigPicture-Sept2014.pdf"
    },
    {
      "title": "Capture",
      "type": "image",
      "src": "Capture.png"
    },
    {
      "title": "rsz_capture (2)",
      "type": "image",
      "src": "rsz_capture-2.png"
    }
  ]
}
In Beijing on <span class="aBn" tabindex="0" data-term="goog_1193539113"><span class="aQJ">September 11th</span></span>-<span class="aBn" tabindex="0" data-term="goog_1193539114"><span class="aQJ">12th</span></span>?
<div>Convened by Professor David Walker, BHP Billiton Chair of Australian Studies Peking University, the 2nd FASIC Australian Studies in China Conference will focus on "The Big Picture: Lives, Landscapes, Homelands in Australian and Chinese Art"</div>
<div></div>
<div>A priceless opportunity be involved in a discussion that is becoming increasingly important to both Australia and China, yet is not in the public eye (yet)!</div>
<div></div>
<div>Attendance is free, but RSVP is essential! <a href="pkuasc.fasic.org.au/2014conference">Please click here </a>for more details and the full conference schedule!</div>
<div>#acya #auschina #中澳 #art #FASIC Peking University"<a href="http://www.acya.org.au/wp-content/uploads/2014/09/2nd-FASIC-Australian-Studies-in-China-Conference-TheBigPicture-Sept2014.pdf">Please click here for the official conference flyer and program. </a>

</div>