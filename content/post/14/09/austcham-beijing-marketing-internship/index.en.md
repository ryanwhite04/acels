{
  "title": "AustCham Beijing Marketing Internship ",
  "author": "ACYA",
  "date": "2014-09-11T01:03:55+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled.png"
    },
    {
      "title": "Internship description 2014",
      "type": "application",
      "src": "Internship-description-2014.docx"
    },
    {
      "title": "Untitled",
      "type": "image",
      "src": "Untitled1-e1410397810906.png"
    }
  ]
}
AustCham Beijing is currently offering a 12 week full-time internship at our office in Chaoyangmen, Beijing. This internship position is an excellent opportunity to gain experience in the Australia-China business sector and pending results there is an opportunity for full-time employment with the Chamber.

Internship placements are available ASAP. A modest monthly stipend is available. For applications please email your CV and cover letter to Oliver Theobald, <a href="mailto:oliver.theobald@austcham.org">oliver.theobald@austcham.org</a>

In-person or phone interviews will be held in September.

&nbsp;

<strong>Position Outline</strong>

<strong>Key Responsibilities </strong>

o    Drafting membership contracts

o    Marketing research

o    Company Member visits

o    Representing AustCham at professional &amp; networking events

o    Other duties as assigned.

&nbsp;

<strong>Qualifications Required </strong>

o    Excellent verbal and written communication skills in English (Chinese language skills are highly regarded)

o    Applicants must be at least 23 years old as of the end of 2014

o    Strong work ethic, positive attitude and a proven ability to take initiative

o    Ability to meet deadlines

o    Highly organised and punctual

o    Proficient with Microsoft Excel

o    Flexible and ability to learn quickly on the job

&nbsp;

<strong>General Awareness </strong>

Understands the considerations and values of a non-profit, membership based organisation. Ability to work in a small team environment, contribute positively to the local work environment and deliver on job responsibilities.

&nbsp;

<strong>Availability </strong>

Must be available to start late September/early October. Candidates already residing in Beijing are highly regarded.

&nbsp;

<strong>Visa</strong>

AustCham Beijing is unable to sponsor a short-term visa for intern applicants but should the internship lead to full-time employment (after 12 weeks) the Chamber can provide the necessary visa support.

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/Internship-description-2014.docx">Internship description 2014</a>