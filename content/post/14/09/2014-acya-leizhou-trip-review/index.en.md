{
  "title": "2014 ACYA Leizhou Trip Review ",
  "author": "ACYA",
  "date": "2014-09-18T03:03:23+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "leizhou",
      "type": "image",
      "src": "leizhou.png"
    },
    {
      "title": "leizhou1",
      "type": "image",
      "src": "leizhou1.png"
    }
  ]
}
From 20-30 July 2014, four ACYA representatives Fei Tao Lin, Cathy Nguyen, Siyuan Wei and Ann Yu, embarked on a life-changing trip to volunteer at an English summer camp in rural Leizhou, China. Aside from conducting five days of comprehensive English classes in conjunction with the Leizhou College Students Volunteer Association (LCSVA), volunteers also immersed themselves in local culture through homestay with their families and exploration of the surrounding tourist attractions.

For both the volunteers and their students, the Leizhou Volunteering Trip was an extremely rewarding experience. Each volunteer taught a class of local children ranging from Year 4 to Year 10, who embraced this rare opportunity to learn English from native speakers with great enthusiasm. “It was an unforgettable and life-changing experience for me,” says Ann Yu. “Their naivety and passion touched me deeply.”

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/leizhou.png"><img class="size-medium wp-image-21538 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/09/leizhou-300x227.png" alt="[cml_media_alt id='21538']leizhou[/cml_media_alt]" width="300" height="227" /></a>

ACYA participants also enjoyed the opportunity to develop their teamwork and communication skills by collaborating with the local volunteers. Fei Tao Lin and Siyuan Wei found their LCSVA peers to be incredibly supportive in the classroom. Through their shared passion and commitment to teaching their students, both Australian and Chinese volunteers connected beyond their cultural differences and came to see each other “like family members” by the end of their stay.

The resounding success of the 2014 ACYA Leizhou Volunteering Trip has meant yet another significant step towards greater cultural understanding between the youth communities of Australia and China . For Fei Tao Lin, the trip was a “great life experience” that she highly recommends to everyone. “I enjoyed this trip very much and look forward to joining another one in the future!” Ann Yu agrees.

<strong>Contact</strong>

Further informationand other Australia-China opportunities can be found on the <a href="http://www.acya.org.au/">ACYA website</a>.

For enquiries regarding the Leizhou Volunteering Trip, please contact ACYA's National Education Director:

Michael Twycross

education@acya.org.au

(+61 404 501 393)

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/leizhou1.png"><img class="size-medium wp-image-21539 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/09/leizhou1-300x225.png" alt="[cml_media_alt id='21539']leizhou1[/cml_media_alt]" width="300" height="225" /></a>