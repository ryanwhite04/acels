{
  "title": "Will China be the Next Asian Tiger – Escaping the Middle Income Trap",
  "author": "ACYA",
  "date": "2014-09-18T12:53:00+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "sk",
      "type": "image",
      "src": "sk.jpg"
    },
    {
      "title": "middleincome",
      "type": "image",
      "src": "middleincome.jpg"
    }
  ]
}
<h2 style="text-align: center;">by Charlton Martin</h2>
Back in 1978 when Deng Xiaoping began China’s ‘reform and opening up’, its economy was the same size as Portugal’s is now. This year, China is on the cusp of overtaking the US to become the world’s largest economy on a purchasing-power basis. China’s growth performance is historically unparalleled. As Kevin Rudd puts it, “It is like the English Industrial Revolution and the global information revolution combusting simultaneously and compressed into not 300 years, but 30”.

Danny Quah, professor of economics and international development at the London School of Economics, estimates that as a result the average geographic location of the world’s economic activity has moved 5,000 kilometres eastwards since 1980 – from the mid-Atlantic to central Iran. A key question for the Australian economy and the world economy more generally is: can this astonishing economic transformation be sustained?

History is strewn with examples of that certain euphoria which accompanies renewed and rapid growth. Before its asset price bubble burst in the early 1990s, Japan constructed thousands of golf courses for businesspeople who were willing to invest in memberships of between US$60,000 andUS$1 million in their belief in a new age of unprecedented prosperity. Similarly, prior to the global financial crisis, economists optimistically spoke of ‘the Great Moderation’, hailing a post boom-bust era of perpetual economic expansion. Yet, historically, such episodes have been followed by spectacular collapse. Will China’s economy suffer a similar fate?

In 1994, Nobel laureate Paul Krugman approached this question by drawing parallels with the most comparable historical case – the Soviet Union, whose economy imploded in the 1940s. At pre-industrialisation, both countries were communist nations with huge reserves of surplus labour. One need only arm these workers with capital, he argued, and they could produce more – replace a shovel with a bulldozer and a ditch will be dug more quickly. Krugman contends that the rapid expansion of the Soviet Union in the 1930s and China since 1978 have been achieved mainly by giving workers new machinery.

In China, this process was accomplished by relocating millions of unproductive agricultural workers to manufacturing jobs in the city. In 1978, the loss in output from removing one labourer from the country was close, or equal, to zero – too many cooks spoil the broth. Yet put them in a Foxconn factory in Shenzhen and there is a large increase in output. Thus relocating workers to the city essentially created ‘free’ output.

However, eventually, relocating a worker from the rice fields to Shenzhen will result in a loss of rice equivalent to the concomitant increase in iPhone production. It is at this point, known as the ‘Lewis Turning Point’, that growth must start to come from using existing outputs more efficiently – innovation. This is where the Soviet Union failed.

The Western world saw the fall of the Soviet Union as being synonymous with the failure of communism to create prosperity for its citizens. Liberal democracy and prosperity, it was thought, were inextricably intertwined, and thus Krugman predicted that China’s growth, too, would falter. It still has not. China’s renewed growth carries with it a question of whether this assumed link between liberal democracy and affluence, held so deeply by so many in the West, is really fundamental to economic success.

Thus, much rides on whether China can escape the infamous ‘middle income trap’–where growth slows before an economy reaches developed-country income levels. This situation is illustrated in Figure 1, with GDP per capita in both Brazil and South Africa failing to converge with incomes in the developed world, whilst South Korea breaks through.

Brazil and South Korea actually followed similar paths to middle income up until 1985, but due to Brazil’s high income inequality its middle class comprised only 29% of the population in 1980. However, Korea’s evenly distributed growth produced a middle class that accounted for 53% of the population. The aspirational role of the middle class in creating sophisticated consumption in goods and services is fundamental in driving the transformation toward an innovation-based economy. Today the middle class makes up 94% of Korea’s population, but this figure is still only 38% in Brazil, and its economy continues to stagnate.

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/sk.jpg"><img class="aligncenter size-full wp-image-21546" src="http://www.acya.org.au/wp-content/uploads/2014/09/sk.jpg" alt="[cml_media_alt id='21546']sk[/cml_media_alt]" width="416" height="363" /></a>

The middle income trap is far from exclusive to authoritarian countries. Figure 2 shows that, since 1960, of 101 countries there are only 13 escapees whose living standards have converged with the United States. The others failed for numerous reasons. As Tolstoy wrote, “happy families are all alike; but every unhappy family is unhappy in its own way”. Yet these unhappy families had one commonality: inertia through the encrustation of ideas. A failure to recognise that the strategies that lifted them to middle income status differ markedly from those needed to progress to a high income economy. Whether or not China’s economy can escape middle income rests on whether the characteristics of its economy align with the thirteen happy families, which all shared an ability to mobilise and upgrade their economic structures.

Within China’s Forbidden City is a building named Jiao Tai Dian (交泰殿The Hall of Union and Peace). In the center sits a throne, behind which is a plaque that Kangxi, fourth emperor of the Qing Dynasty, inscribed with the characters Wu Wei (無為), literally translating to ‘without action’. It refers to the laissez-faire Confucian political philosophy prescribing that policy, like water, should be left to run its natural course.

Yet of the tiny fraction of economies that successfully mobilized into innovation-driven growth, the vast majority have been Confucian: Hong Kong, Singapore, Taiwan, South Korea –the ‘Asian Tigers’– and Japan.

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/middleincome.jpg"><img class="aligncenter size-full wp-image-21547" src="http://www.acya.org.au/wp-content/uploads/2014/09/middleincome.jpg" alt="[cml_media_alt id='21547']middleincome[/cml_media_alt]" width="416" height="359" /></a>

These economies have high saving rates and disciplined, well-educated workforces. None have had a comparative advantage in natural resources. They are export-oriented and manufacture-intensive, with high levels of government intervention. Indeed, this characterization of the Asian Tigers and Japan is almost identical to the China we see today.

However, there is a key difference. Japan and the Asian Tigers, all to varying extents, benefited from China’s cheap labour; through foreign direct investment and engagement with its eastern coastal manufacturing belt. For example, during the 1980s when Taiwan’s labour cost advantage eroded, the outsourcing of low-value-added manufacturing to China was fundamental in its smooth transition to knowledge-intensive industries. Given that China’s manufacturing belt itself now faces similar cost escalations, China cannot continue to follow the path of the Asian Tigers unless it too can find a new cheap labour pool to dip into.

Rather than looking abroad to Africa or East Asia, China should look west. Despite 627 million Chinese being lifted out of poverty since 1981, in 2008 there were still 173 million living below $1.25 per day, nearly all in western China. This region remains hugely underdeveloped. However, the infrastructure is not yet in place to utilise these workers. Danny Quah cites the example that it is cheaper to ship a t-shirt from Nigeria to Shanghai than it is to freight it from western China. To integrate these workers, China needs more government investment.

China should learn from President Eisenhower’s establishment of the US interstate highway system in the 1950s, which was crucial in underpinning US growth and was the largest public works venture in history. China’s infrastructure investment remains dismal in comparison. Its train and road networks are just 40% and 60% respectively of those in the US, whilst US public airports outnumber China’s ten to one.

Improving this position with an infrastructure gateway to China’s west will unlock a workforce the size of the European Union, not only allowing China’s east coast to upgrade its industrial structure, but also rectifying significant regional inequality. Lifting incomes in western China will be a key building block for creating a large middle class whose consumption can pull China into the developed world.

<strong>Is China the next Asian Tiger?</strong>

The evidence shows China is following a similar path to its Confucian neighbours, who provide the only blueprint in modern history for convergence with the developed world. The modus operandi of these countries has been command-and-control style resource allocation providing the needed transition from capital accumulation to innovation-driven growth. Yet underpinning this transformation was a willingness of the polity to lift the living conditions of all in society, generating the middle class consumption central to bringing GDP per capita to US levels. To date, no country as authoritarian as China has escaped the middle income trap (apart from tiny oil-rich Equatorial Guinea). Critics argue that this is because policies to expand the middle class are fundamentally incompatible with regimes that principally answer to the interest of elites, rather than society as a whole. Yet to simply write China off as another Soviet Union, as Krugman did, would be premature in light of its distinct similarities with the Asian Tigers. Just as China’s economy has surprised critics, maybe its polity, too, will surprise and deliver the inclusive growth and structural reform required to ascend into the first world. The evolution of China’s per capita GDP over the coming two decades will provide some answers.

<em>This piece builds on discussions had during the 2014 Peking University–London School of Economics Summer School in Beijing.</em>

<em>Charlton Martin is a former student of the University of Tasmania living in Taiwan. He has completed a Bachelor of Economics with Honours (first class) and studied Chinese at the National Taiwan Normal University on a 2011 MoE Huayu Scholarship and again from 2013-14. He is a former bilingual intern at the Australian Trade Commission in Taipei and has published on the economics of China’s rise. Charlton is also the current president of the Australia-China Youth Association Taipei Chapter.</em>