{
  "title": "ACYA Journal - Best articles from previous editions!",
  "author": "ACYA",
  "date": "2014-09-19T00:04:19+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "1795669_10152101718417961_634576351_n-500x500 (500x372)",
      "type": "image",
      "src": "1795669_10152101718417961_634576351_n-500x500-500x372.jpg"
    }
  ]
}
In anticipation of the fourth Journal of Australia-China Affairs, ACYA will be republishing some of the best material from past editions. All articles have undergone a rigorous peer-review process and offer unique insights into economic, strategic and developmental aspects of the Australia-China relationship. A wide variety of topics are covered, including travel in China’s frontier provinces, exploring the Australian-Chinese identity and Asia-Pacific dynamics.

<a href="http://www.acya.org.au/wp-content/uploads/2014/08/acya-journal2.jpg"><img class="size-medium wp-image-21480 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/08/acya-journal2-250x300.jpg" alt="[cml_media_alt id='21480']2011 acya journal[/cml_media_alt]" width="250" height="300" /></a>

The Journal is co-published by the Australia-China Youth Association and the University of Sydney China Studies Centre and is the premier publication for students and young professionals interested in the Australia-China discourse.

Watch <a href="http://www.acya.org.au/en/journal-archives/">this space</a> for the republishing of the articles!

<a href="http://sydney.edu.au/china_studies_centre/en/ccpublications/acya_journal_2013.shtml">To view the 2013 ACYA Journal, please click here</a>