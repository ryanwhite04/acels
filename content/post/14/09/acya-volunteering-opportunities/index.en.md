{
  "title": "ACYA Volunteering Opportunities",
  "author": "ACYA",
  "date": "2014-09-30T14:00:47+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "unnamed",
      "type": "image",
      "src": "unnamed.png"
    },
    {
      "title": "unnamed",
      "type": "image",
      "src": "unnamed.jpg"
    },
    {
      "title": "8",
      "type": "image",
      "src": "8.jpg"
    }
  ]
}
ACYA Volunteering Opportunities open for Expression of Interests Now at Sydney and Brisbane China Digitial Conference (hosted by Australia Business Forum).

<i>China Digital Conference 2014 provides attendees with exclusive opportunities to gain the latest information and trends in doing business in the China digital market. </i>

ACYA is looking for keen volunteers to support the running of this event!

Brisbane: 20 October

Sydney: 24 October

Shift: 7:30am - 12:30pm (2 - 3 volunteers)

Shift: 12:30pm - 18:00pm (2 - 3 volunteers)

Please submit a short expression of interest to <a href="mailto:careers@acya.org.au">careers@acya.org.au</a>

Recently, in partnership with the Australia Business Forum, keen ACYA members volunteered for the <i>Australia China Business Week</i> in Sydney.

Thomas Liang, a keen volunteer at the event describes:

“The <i>Australia-China Business Week</i> was a 3-day forum focusing on commercial and trade aspects of Australia’s relationship with China. The event was attended by members of the business community, government, banks and professional services firms. It was notable that representatives from many Small and Medium Enterprises (SMEs) also attended the event.

The event was split into two forums, where speakers were invited to share their areas of expertise. In the Chinese-speaking forum, the focus was on of Chinese investment into Australia. This may be through real property, joint ventures and the merger or acquisition of Australian businesses. In the English-speaking forum, the focus was on export strategies into China.

As opportunity with Australian mining continues to dwindle, a particular important theme for this year’s event was agribusiness. It was identified that as China continues to develop, its demand on a reliable and safe source of food would continue to increase. Further, with our reputation in quality dairy and beef, Australia is in an ideal position to fill this demand. However, the forum has also learnt that without a Free Trade Agreement, the volume of our dairy export is trailing significantly behind New Zealand.

Many attendees would agree that <i>Malcolm Turbull’s </i>keynote was the highlight of the week. He reminded the forum that the relationship between Australia and China lies deeper than trade and economic ties. We share an often forgotten and rarely acknowledged military alliance in the war against Japan. He acknowledged that if it weren’t for China’s fight in North Asia, Australia would have probably fallen to the Japanese in World War II.

Volunteering for the event has definitely made a fulfilling week.  What I have learnt about the Australia-China trade was more extensive and meaningful than what I have learnt previously. I was especially inspired by my interaction and exposure to business leaders in this space. Most importantly, what made the week so enjoyable was meeting many like-minded volunteers across different ACYA chapters who share my passion for the Australia-China space!”

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/unnamed.jpg"><img class="size-medium wp-image-21601 alignright" src="http://www.acya.org.au/wp-content/uploads/2014/09/unnamed-225x300.jpg" alt="[cml_media_alt id='21601']unnamed[/cml_media_alt]" width="225" height="300" /></a><a href="http://www.acya.org.au/wp-content/uploads/2014/09/8.jpg"><img class="alignnone size-medium wp-image-21602" src="http://www.acya.org.au/wp-content/uploads/2014/09/8-300x225.jpg" alt="[cml_media_alt id='21602']8[/cml_media_alt]" width="300" height="225" /></a>