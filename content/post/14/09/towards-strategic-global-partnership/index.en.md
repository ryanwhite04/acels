{
  "title": "Towards a Strategic Global Partnership",
  "author": "ACYA",
  "date": "2014-09-29T16:31:35+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "auschina strategic partnership",
      "type": "image",
      "src": "auschina-strategic-partnership.jpeg"
    }
  ]
}
<div class="page" title="Page 66">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">by <a href="#lloyd">Lloyd Bradbury</a></h2>
Australia famously weathered the global financial crisis relatively well, riding on the back of China’s demand for our natural resources and agricultural goods. Our banking sector also performed notably during the crisis and accordingly many of Australia’s financial institutions have made significant inroads into the Chinese market. Australia’s export of education to China, too, played a key role in propping up Australia’s economy.

As a result, Australia is currently the most trade exposed country to China in the world with nearly 25% of our exports bound for China.

This tethering of Australia’s economy to China’s growth, however, will undoubtedly raise populist questions over how best to manage our trade exposure risk and protect Australia from volatility in China’s market. While these questions aren’t without merit, Australia should instead focus on ramping-up engagement with China and developing a much more comprehensive Sino-Australian relationship built on the pillars of collaboration, dialogue, investment and cultural-exchange.

In short, within the context of China’s rising ambitions and the increasing significance of the BRICS at the G20, Australia needs to look to our largest regional neighbour as a key strategic partner for global engagement, rather than merely a bilateral trading partner.

</div>
</div>
</div>
<div class="page" title="Page 67">
<div class="layoutArea">
<div class="column">

While China’s economic growth story is well known and widely commended, the country’s willingness to take a leadership role on global issues should be equally welcomed by Australia.

For example, the 2011 BRICS Summit on Hainan Island in the South China Sea showed that China now has ambitions of using the BRICS bloc as a platform for playing a leading role in setting the global economic discussion agenda. In turn, China has become an expert at leveraging these relationships to expand trade with South America, Central Asia and Africa. China’s trade relationship with these regions has gone from strength to strength over the past decade with bilateral trade between China and Africa, for instance, surpassing $100 billion in 2010 up from $40 billion in 2005. China is now a key stakeholder in the developing world and Australia must be prepared to partner with China in emerging markets globally.

China and Australia can find much common ground for collaboration through championing investment into developing nations, reducing imbalances and protectionism in global trade and strengthening market mechanisms to reduce global commodity price volatility. China’s experience in large infrastructure project management, investment-led poverty reduction and implementing market reforms is highly complementary to Australia’s natural strengths in resources, agriculture, education and financial services. Through focusing on these areas of comparative advantage as a starting point, significant bilateral opportunities can be identified.

However reaching a point where such large-scale cooperation with China is possible still requires a substantial injection of political will from both governments – the past 15 rounds of Australia-China Free Trade Agreement negotiations are testament to this. The Australian Government, therefore, needs to invest heavily in relationship building with China.

</div>
</div>
</div>
<div class="page" title="Page 68">
<div class="layoutArea">
<div class="column">

While the recent visits to China of Julia Gillard and Kevin Rudd are positive examples of relationship-driven engagement, the facilitation of bilateral dialogue must be an ongoing priority. Without regular dialogue and an increased commitment to collaboration, Australia will not realise the trade diversification benefits of partnering with China in the developing world.

Dr Geoff Raby, the outgoing Australian ambassador in Beijing, recently noted in a speech at the Australian Institute of Company Directors Conference in Beijing that Australia’s unique trade relationship with China means that “we are very much alone when dealing with China”.

The Australian government must take heed and recognise that Australia’s exposure to China is now at a point where we cannot afford to view China as anything less than a strategic global partner.

<hr />

<strong>This article appears in the <a href="http://www.acya.org.au/Documents/Journals/ACYA%20Journal%20FINAL.pdf">2011 ACYA Cultural Journal of Sino-Australian Affairs (pp. 56-58)</a>.</strong>

<em id="lloyd">Lloyd Bradbury is the Executive Director (Australia) of the Australia-China Young Professionals Initiative and recently attended the 2011 G20 Youth Summit in Paris as Australia’s Minister for Economics.</em>

</div>
</div>
</div>