{
  "title": "ACYA National Facebook Group Relaunch",
  "author": "ACYA",
  "date": "2014-09-22T11:07:59+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "10259807_293201290834855_5633814065847977384_n",
      "type": "image",
      "src": "10259807_293201290834855_5633814065847977384_n.jpg"
    }
  ]
}
After a hiatus, ACYA’s <a href="https://www.facebook.com/groups/australiachinayouthassociation/">National Facebook Group </a>has returned. Designed to act as a platform to facilitate the discussions of Australia-China relations with like-minded people, there are many ways that you can get involved. Different types of media including photos and videos of ACYA events are very much welcome! Post interesting topics for others to read or even ask questions!

Need recommendations on places to visit in China/Australia? Craving a particular type of food but don’t know where to get it? Want the latest musical or cinematic recommendations? With over 5000
students and young professionals as members, someone will be able to point you in the direction of
the best Xiaolongbao or the newest Jay Chou album!

While <a href="https://www.facebook.com/australiachinayouthassociation1">ACYA’s Facebook Page </a>will continue to act as your go to source for Australia-China current affairs, Careers, Education and Publication opportunities, the Facebook Group will act as the
“classifieds” section, providing an arena for greater two-way interaction. Members are welcome to post any opportunities that they feel will help foster members’ passion for the China-Australia space.

However, the success of this group depends on YOU, join the group today, invite your friends,
ACYA alumni, and anybody else who you think might be interested in Australia-China relations! Let
everyone know we’re back!

P.S Drop the Facebook page a like at the same time!

<a href="http://www.acya.org.au/wp-content/uploads/2014/09/10259807_293201290834855_5633814065847977384_n.jpg"><img class="size-medium wp-image-21561 aligncenter" src="http://www.acya.org.au/wp-content/uploads/2014/09/10259807_293201290834855_5633814065847977384_n-300x111.jpg" alt="[cml_media_alt id='21561']10259807_293201290834855_5633814065847977384_n[/cml_media_alt]" width="300" height="111" /></a>
#acya #auschina #中澳