{
  "title": "<!--:en-->Great Turnout for ACYA Nanjing's First Monthly 'Aussie Drinks' <!--:-->",
  "author": "ACYA",
  "date": "2014-03-12T01:35:17+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "NJ",
      "type": "image",
      "src": "NJ.jpg"
    }
  ]
}
<!--:en--><p style="text-align: center;"><a href="http://www.acya.org.au/wp-content/uploads/2014/03/NJ.jpg"><img class="aligncenter  wp-image-2277" alt="NJ" src="http://www.acya.org.au/wp-content/uploads/2014/03/NJ.jpg" width="494" height="286" /></a></p>
The first ACYA - Nanjing Chapter monthly drinks were held on Friday the 28th February. We had a turn out of approximately 150 people. Last semester our monthly drinks were hugely successful, our Christmas drinks bringing in almost 300 students, teachers and young professionals from the Nanjing area. Such events have been crucial to the success of the Nanjing Chapter as they characterise our plan to create and maintain ongoing opportunities and events for our members. The drinks are usually held on the last Friday of every month and we collect contact information of people new to the ACYA events so that we can inform them of news and upcoming functions. In addition to this we run a fund raising raffle, the prizes of which are generously provided by La Villa and its staff.

We were happy to welcome the recently arrived round 4 Hamer Scholarship recipients at our event and welcome them into our membership base. We are fortunate to draw an internationally diverse group of people to our events and therefore our membership. Our event brings in large numbers of Chinese students and teachers as well as many international students participating the Chinese Language program at Nanjing University and Nanjing Normal University. This diverse focus is what makes the Nanjing Chapter highly unique and opens up social and networking opportunities on a much broader level. While the focus of ACYA is indeed promoting cross-cultural understanding between young Chinese and Australians, encouraging a dialogue between people from all over the world is one of the best ways to express the organisations mission and create opportunities for its members.<!--:-->