{
  "title": "ACYA Newsletter January 2014",
  "author": "ACYA",
  "date": "2014-03-07T05:15:09+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->Welcome to 2014 everyone, and Happy Year of the Horse to you all!

Here at ACYA we’re busy putting in place the full new National Executive to serve you throughout the year – details to follow in our next issue.

Without further ado, we launch into the first e-bulletin of what we’re sure will be an extremely productive year for ACYA across Australia and China, further expanding our membership base and forging ever more connections between young people from our two countries.

Enjoy exploring the opportunities, profiles and updates below. We hope you all had a fantastic Chinese New Year.

The ACYA team.

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=29c89ccc1c">E-Bulletin January 2014 </a><!--:--><!--:zh--><strong>欢迎辞</strong>

2014年终于来到！祝福大家马年吉祥！

ACYA为了在新的一年继续为您服务，现正积极开展新旧全国管理委员会交接工作，具体内容将在下一期时讯中向您汇报。

现在让我们向您介绍新年第一期时讯，我们相信ACYA会继续在中澳两国扩大成员组织、提升两国青年的联系，新的一年必将硕果累累。

祝您阅读本期时讯愉快！再次祝您春节快乐！

ACYA团队

<a href="http://us1.campaign-archive2.com/?u=db2f0ea3a68f632de3088eada&amp;id=29c89ccc1c">E-Bulletin January 2014</a><!--:-->