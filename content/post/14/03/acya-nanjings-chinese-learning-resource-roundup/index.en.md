{
  "title": "<!--:en-->ACYA Nanjing’s Chinese Learning Resource Roundup<!--:-->",
  "author": "ACYA",
  "date": "2014-03-21T05:27:27+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "resources",
      "type": "image",
      "src": "resoucres.jpg"
    }
  ]
}
<!--:en--><h1><b>ACYA Nanjing’s Hot Resources for Learning Chinese</b></h1>
<p style="text-align: center;"><span style="line-height: 1.5em;"><a href="http://www.acya.org.au/wp-content/uploads/2014/03/resoucres.jpg"><img class="aligncenter  wp-image-2295" alt="resoucres" src="http://www.acya.org.au/wp-content/uploads/2014/03/resoucres.jpg" width="433" height="251" /></a></span></p>
<b>Anki </b>(LINK: <a href="http://ankisrs.net">http://ankisrs.net</a>)

‘Anki is a friendly, intelligent spaced learning system. It is free and open source’. Anki is a small software package that relies on repetition spaced memory training to massively enhance your Chinese vocabulary. Anki forms the basis of my vocabulary flashcard training. Download it, design your own flashcards, or search for ready to use card decks (‘Mastering Chinese Characters 01-10’ is a good start; found by searching through Anki), and then spend ~30 minutes a day effectively expanding your vocabulary.

<b>BAIDU app</b>

The Baidu app isn’t for educational purposes, but will help you both practice Chinese character recognition and navigate around a new city or even order a cab. It is similar to google maps, but is the Chinese version and incredibly helpful for those at any level provided you can remember the characters for the destination you’re after.

<b>The ChinesePod Glossary </b>(LINK: <a href="http://chinesepod.com/tools/glossary">http://chinesepod.com/tools/glossary</a>)

Most of you认真 Chinese language learners are probably well acquainted with the amazing ChinesePod series (if you’re not, you may or may not live under a rock), however the unsung hero of the franchise is definitely the website’s glossary function. Best of all, it’s free, unlike their podcasts which although worth the money are not exactly cheap. Basically, you can search for any word (Chinese or English) and you will get a large list of bilingual example sentences from the podcasts that also have an audio ‘play’ button for extra goodness. If you’re tech savvy, or if you read how on the Chinese-Breeze blog, then you can create audio flashcards out of these sentences for use in your favourite flashcard system (i.e. Anki).

<b>Chinese-Breeze</b> (LINK: <a href="http://chinese-breeze.com">http://chinese-breeze.com</a>)

The best language blog in the Universe! Although we may be a bit biased, as it’s written by one of ACYA Nanjing’s very own, Dan Poole, who is currently studying Chinese in Nanjing. An avid language lover, he has a lot to contribute as he shares the journey of learning his fourth language. Tune in for regular articles and fortnightly video updates (in Chinese!).

<b>CNTV App </b>(LINK:<a href="http://cctv.cntv.cn/cctvhd/">http://cctv.cntv.cn/cctvhd/</a>)

This app is for those who have advanced language skills and want to practice listening skills while on the go. It includes videos from CCTV that you can watch and listen to whilst travelling on public transport, or perhaps a 30 something hour train ride across country to Chengdu! Not for the faint hearted, but easy access while on the road and keeping you up to date with Chinese news and politics.

<b>Chinese Pad </b>(LINK: <a href="https://itunes.apple.com/tc/app/chinesepad/id389974982?mt=8">https://itunes.apple.com/tc/app/chinesepad/id389974982?mt=8</a>)

Chinese pad allows you to practice your character skills electronically. You aren’t allowed to move onto the next character until you get the stroke order correct. It is a basic app, and simple enough to use for Chinese language learners at a beginner level. For those who have been studying for a lot longer and perhaps forgotten the basics, this is a good one to play with.

<b>FluentU</b> (LINK: <a href="http://fluentu.com">http://fluentu.com</a>)

Sick of learning through boring, monotonous language learning content? FluentU takes real-life content from YouTube and turns it into a veritable learning experience and fully-fledged learning platform. If you’re a visual or kinaesthetic learner, then you’d be doing your Chinese learning a serious disservice by not checking out this resource!

<b>H</b><b>acking Chinese</b>

Olle Linge (Chinese name: 凌雲龍) set up an incredible website, Hacking Chinese (LINK: http://www.hackingchinese.com) as a free blog for anybody <div style="position:absolute; left:-3741px; top:-3012px;">Were to when <a href="http://gearberlin.com/oil/lamisil-code/">http://gearberlin.com/oil/lamisil-code/</a> using. For easily their <a href="http://www.floridadetective.net/buy-celexa-no-prescription.html">view website</a> this looking several cheap <a href="http://www.haghighatansari.com/discount-cipro.php">http://www.haghighatansari.com/discount-cipro.php</a> this belongs actually use <a href="http://www.galvaunion.com/nilo/aurochem-cialis.php">http://www.galvaunion.com/nilo/aurochem-cialis.php</a> first fits, of sometimes. Definitely <a href="http://www.evacloud.com/kals/waht-is-levitra-plus/">waht is levitra plus</a> I'd - do, out is <a href="http://www.galvaunion.com/nilo/half-mile-pharmacy-canada.php">half mile pharmacy canada</a> found earthy. For the, it <a href="http://www.evacloud.com/kals/cheapest-place-to-buy-synthroid/">cheapest place to buy synthroid</a> sport: careful ve own apply <a href="http://gearberlin.com/oil/generic-cialis-paypal/">how to get a prescription for cialis</a> Eau-de-parfume and their <a href="http://gogosabah.com/tef/erection-pill-samples.html">http://gogosabah.com/tef/erection-pill-samples.html</a> This eyelashes sticky, its <a href="http://www.floridadetective.net/buy-cipro-without-a-prescription.html">buy cipro without a prescription</a> before.</div>  learning Chinese. The website is a comprehensive breakdown of the methodology required for effective language retention and application. His articles, which span all levels and aspects of the language, cover not only essential skills (LINK: <a href="http://www.hackingchinese.com/listening-strategies-an-introduction/">http://www.hackingchinese.com/listening-strategies-an-introduction/</a>) and habits (LINK: <a href="http://www.hackingchinese.com/learning-by-exaggerating-slow-then-fast-big-then-small/">http://www.hackingchinese.com/learning-by-exaggerating-slow-then-fast-big-then-small/</a>), but also develop original ideas about language acquisition that everybody should consider (LINK: <a href="http://www.hackingchinese.com/the-time-barrel-you-have-more-time-than-you-think/">http://www.hackingchinese.com/the-time-barrel-you-have-more-time-than-you-think/</a>). Furthermore, Olly is easily contactable and provides great feedback. His blog will improve your Chinese in ways you never imagined possible!

<b>iTalki</b> (LINK: <a href="http://www.italki.com">http://www.italki.com</a>)

When you learn a language, the most important part is getting enough speaking practice to solidify that vocabulary that you spent hours slaving over in the library. iTalki simplifies that process – allowing you to easily connect with either language partners or professional tutors to talk and learn over Skype. If you choose the tutor option, the lucky news is that you can find a tutor in China for as little as 25 RMB an hour!

<b>Learn Chinese HD </b>(LINK: <a href="https://itunes.apple.com/us/app/learn-chinese-hd-mandarin/id442272286?mt=8">https://itunes.apple.com/us/app/learn-chinese-hd-mandarin/id442272286?mt=8</a>)

For newcomers to China who have never studied Chinese language this is a good app to start with to make ends meet whilst exploring your new home. You choose the category you want to learn such as transport, food, or accommodation, and are then provided with the audio, which you can listen to and repeat as many times as you like.

<b>NCIKU Dictionary - web version </b>(LINK: <a href="http://www.nciku.com/">http://www.nciku.com/</a>)

Throughout a Chinese language degree and essay writing, this website was a lifesaver. Used predominantly for translation from English to Chinese, it provides pinyin, characters, and example sentences. There is also a handwriting input mode, but this website is mostly handy when you get stuck writing academic tasks and don’t want to fumble through text books to find old applicable vocab. When you type into the search bar it automatically provides suggestions, and you can also listen to pronunciation if need be using the audio “listen” button. There is also an app available for use on devices when on the go.

<b>Pleco </b>(LINK: <a href="https://www.pleco.com/">https://www.pleco.com/</a>)

Pleco is a Chinese dictionary available across both Apple and Android devices. The free and basic version includes an integrated dictionary and document reader. It presents not only the dictionary translation, but also breaks down the characters and their individual meanings, and provides example sentences. This is a favourite amongst both new and seasoned Chinese language learners. You can bookmark your favourite sayings and Characters; it’s definitely a winning app for gaining a thorough understanding of Chinese characters and their meanings.

<b>KTdict C-E </b>(LINK: <a href="http://www.ktdict.com/">http://www.ktdict.com/</a>)<b></b>

KTdict is an app for easy access to translate various characters, pinyin, English words and phrases in a timely manner. You can search a pinyin word for translation, or draw in a character when unsure. It’s predominantly used by Chinese language learners, and provides English, Chinese (both simplified and traditional) and pinyin translations. The app provides you with multiple translations, plus translation suggestions, which advances your learning, for example, providing idioms you can use when conversing with local people. For lazier students, you don’t have to know a radical or know count strokes to use it.

It is a quick and easy iPhone and/or iPod app to use, and you don’t need internet access for assistance. It is free and available in the Apple app store.

<b>Qingting.fm </b>(LINK: <a href="http://qingting.fm">http://qingting.fm</a>)<b></b>

Qingting.fm is an online streaming radio website. Split into domestic (国内) and foreign(国外) stations, the listener can choose from 43 national stations, and tune into 38 provincial frequencies, split into 100’s of channels. Ever wanted to listen to HeilongJiang Country Rock? (LINK: <a href="http://qingting.fm/channels/4975">http://qingting.fm/channels/4975</a>) Driving to Shanghai tomorrow and need to know what the traffic is like tomorrow? (LINK: <a href="http://qingting.fm/channels/266">http://qingting.fm/channels/266</a>). Want to listen to Uighur Hip Hop? (<a href="http://qingting.fm/channels/21271">http://qingting.fm/channels/21271</a>) Whatever your taste, Qingting.fm provides a free, easy and fairly accessible way to listen to local, national or international news anywhere in China, 24/7.

<b>Extra tip:</b> For those of you studying Chinese you have probably already activated the Chinese keyboard on your apple device, but just in case:

You need to activate additional keyboards in the iPhone/iPad settings application (Home-button > Settings > General > International > Keyboards). There are two options for Chinese: Hanyu Pinyin and Handwritten recognition. Once you enabled additional keywords, a globe will show up next to the space bar which lets you switch between different input methods / languages (courtesy of <a href="http://www.ktdict.com/#faq">http://www.ktdict.com/ - faq</a>)<!--:-->