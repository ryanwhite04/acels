{
  "title": "ACYA Newsletter February 2014",
  "author": "ACYA",
  "date": "2014-03-24T09:37:35+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
<!--:en-->Hello to all!

We're pleased to announce the February edition of the ACYA E-Bulletin is now available to be viewed online. Please click the link below!

<a href="http://us1.campaign-archive1.com/?u=db2f0ea3a68f632de3088eada&amp;id=b304fe722a&amp;e=%5BUNIQID">ACYA Newsletter February 2014 </a><!--:-->