{
  "title": "The Hamer Scholarships Program - Study Chinese in Jiangsu Province, China!",
  "author": "ACYA",
  "date": "2014-07-20T04:49:43+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Capture (640x424)",
      "type": "image",
      "src": "Capture-640x424.jpg"
    },
    {
      "title": "Digital PDF - China",
      "type": "application",
      "src": "Digital-PDF-China.pdf"
    }
  ]
}
The next round of applications for Victorian Government Hamer Scholarships to China will open from 11 August - 21 September 2014 for the early 2015 intake. The Hamer scholarships are targeted at Victorians seeking to further their engagement, understanding and develop partnerships with China.

Scholarship recipients spend at least five-six months in-country focusing on intensive language study, cultural immersion and building professional networks and partnerships. To date 81 Victorians have been awarded a Hamer Scholarship to China with a majority from businesses and organisations representing a wide range of industries.

The rapidly growing Chinese economy offers unparalleled opportunities for Victorians and Victorian businesses. Gaining strong Chinese language skills and a deeper understanding of the local culture gives you the best chance to succeed in this exciting market.

&gt;&gt; Hamer Scholarships for intensive language study in China are valued at $10,000.
&gt;&gt; The scholarships are offered for a minimum of one semester (5-6 months) at selected universities and institutions in China.
&gt;&gt; The program is open to Victorians who are aged 21 or above who are Australian citizens or permanent residents (Victorians living and working in China may also be considered).
&gt;&gt; Applicants should demonstrate how the scholarship would significantly benefit their career path, study plan, or their
employer’s future engagement with China.
&gt;&gt; Scholarship recipients will become business, cultural and educational representatives for Victoria.

<a href="http://www.acya.org.au/wp-content/uploads/2014/07/Digital-PDF-China.pdf">For the official flyer, please click here.</a>

<a href="http://dsdbi.vic.gov.au/grants-tenders-and-awards/scholarships-and-fellowships/the-hamer-scholarships-program/program-information/hamer-scholarships-china">For more information and application instructions, please click here.</a>