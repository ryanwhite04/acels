{
  "title": "ACYA- MTC Scholarship Program",
  "author": "ACYA",
  "date": "2014-07-18T08:21:51+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "temple (2)",
      "type": "image",
      "src": "temple-2.jpg"
    },
    {
      "title": "unnamed",
      "type": "image",
      "src": "unnamed.png"
    }
  ]
}
<p style="text-align: left;"><strong>ACYA- MTC Scholarship Program</strong></p>
<a href="http://www.acya.org.au/wp-content/uploads/2014/07/temple-2.jpg"><img class="size-medium wp-image-21263" src="http://www.acya.org.au/wp-content/uploads/2014/07/temple-2-300x173.jpg" alt="[cml_media_alt id='21263']temple (2)[/cml_media_alt]" width="300" height="173" /></a>

The Australia-China Youth Association (ACYA) is delighted to formally announce a partnership with the National Taiwan Normal University Mandarin Training Centre (NTNU MTC) in Taipei, Taiwan. Recognised internationally for Chinese language-learning, MTC has drawn participants from around the world, including former Australian Prime Minister Kevin Rudd.

The partnership will continue developing the community ties between Australian and Taiwanese youth and provide three eager participants with a truly life-changing and global experience.

The scholarship will provide three ACYA members with 3 months of tuition-waived study at Taiwan’s oldest Chinese language institute. They will have the opportunity to learn from top-quality teaching staff, immerse themselves in Taiwan’s vibrant culture, and improve all aspects of their Chinese language skills.

For ACYA UQ and Taipei member Jack Fisher, studying at MTC was a fantastic experience: “The teachers were extremely supportive and friendly. The activities organized by MTC each added something different to my learning experience – whether it be learning to row a Dragon Boat or learning how to cook Stinky Tofu... My time at MTC was nothing short of life-changing.”

<a href="http://www.acya.org.au/en/education/scholarships/acya-mtc-scholarship/">For more information on this exciting opportunity, please click here.</a>

<strong>Application</strong>
Applications for ACYA-MTC scholarships will open on the <em>1st of August 2014</em>.
Applications will close on the <em>1st of October 2014</em>.

<strong>About ACYA</strong>

ACYA is a vibrant community of young Australians and Chinese interested in promoting cross-cultural understanding and developing lasting friendships, academic and business partnerships, as well as provide a central platform to bring together those interested in the Australia-China space.

Since 2008, ACYA has grown to over 5000 members and provides hundreds of events annually across Australia and China, including unique professional and educational opportunities. ACYA has formed strong partnerships with many well-known institutions, including Tsinghua University, Renmin University, Peking University and the University of Sydney.

<strong>Contact</strong>

Enquiries about the ACYA MTC Scholarship and living in Taipei should be directed to previous recipient Michaela Jenkin (michaela.jenkin@uqconnect.edu.au).

General enquiries about ACYA scholarships should be directed to the ACYA National Education Director Michael Twycross (education@acya.org.au) or Louise Mao (louise.mao19@gmail.com) from ACYA Information.