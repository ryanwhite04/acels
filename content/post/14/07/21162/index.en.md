{
  "title": "Call for Submissions: ACYA Journal of Australia-China Affairs 2014 / 征稿启事:中澳青年联合会中澳关系学报-2014年刊总第4期",
  "author": "ACYA",
  "date": "2014-07-11T06:14:04+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "rsz_capture999 (2)",
      "type": "image",
      "src": "rsz_capture999-2.png"
    }
  ]
}
<strong>CALL FOR SUBMISSIONS</strong>

ACYA Journal of Australia-China Affairs 2014

Co-published with the University of Sydney China Studies Centre (http://sydney.edu.au/china_studies_centre/)

Now in its fourth volume, the annual ACYA Journal of Australia China Affairs is a unique academic publication that provides a platform for students, researchers and professionals to publish essays, opinion articles and creative work touching upon the Australia-China relationship. It is peer-reviewed, English-Chinese bilingual, and offers up to A$500 in prizes.

The Journal embraces a diversity of themes and writing styles, and submissions can be made in either English or Chinese. It is published electronically by the China Studies Centre and distributed across Australia and China by the Australia-China Youth Association, University of Sydney, and Foundation for Australian Studies in China.

<a href="http://sydney.edu.au/%20 china_studies_centre/en/ccpublications/acya_journal.shtml">Click here for more details and submission guidelines</a>.

Submissions close Sunday 10 August 2014.

征稿启事

中澳青年联合会中澳关系学报-2014年刊总第4期

中澳青年联合会中澳关系学报与悉尼大学中国研究中心联合出版,每年出刊一期,旨在为学生、青年专业人士和研究学者提供一个高质量、高可见性平台,让他们可 以就中澳关系相关问题,发表深度学术论文、原创观点文章和创意作品。学报欢迎不同主题和写作风格的投稿,并同时接受中英双语来稿。

学报为悉尼大学中国研究中心同行评议期刊,全刊中英双语出版,并为每一写作大项提供最高达500澳元的奖金。学报电子版将由悉尼大学中国研究中心出版发布,并将分发至中澳青年联合会下属中澳所有分会和在华澳大利亚研究基金会。

<a href="http://sydney.edu.au/%20 china_studies_centre/en/ccpublications/acya_journal.shtml">详情请点击这里，发表刊物请点击这里</a>