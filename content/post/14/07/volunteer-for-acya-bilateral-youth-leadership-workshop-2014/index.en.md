{
  "title": "Volunteer for ACYA Bilateral Youth Leadership Workshop 2014!",
  "author": "ACYA",
  "date": "2014-07-23T06:39:06+00:00",
  "image": {
    "type": "image"
  },
  "categories": [
    "Uncategorized"
  ],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "unnamed",
      "type": "image",
      "src": "unnamed1-e1406097522189.png"
    },
    {
      "title": "rsz_unnamed (2)",
      "type": "image",
      "src": "rsz_unnamed-2.png"
    }
  ]
}
<strong>Purpose</strong>

Volunteers are responsible for assisting and facilitating the social and professional events of the Annual ACYA Bilateral Youth Leadership Workshop. This role will be under the guidance of the Operations Managers.

<strong>Key Dates</strong>

The ACYA Bilateral Youth Leadership workshop is to be held on the 15<sup>th</sup> to 18<sup>th</sup> August.

<strong>Responsibilities                  </strong>
<ul>
	<li>Ensuring safety throughout the event</li>
	<li>Assist with transport between venues</li>
	<li>Food collection for meals</li>
	<li>Set-Up and serve breakfast</li>
	<li>Awareness of the timetable and activities to direct on cue</li>
	<li>If you have additional AV and equipment skills (particularly on location at USYD) please provide details of this in your application</li>
	<li>If you have additional photography/filming skills (DSLR camera ownership is a plus) please provide details of this in your application</li>
</ul>
<strong>Requirements</strong>
<ul>
	<li>Availability to attend the Workshop on the specified dates</li>
	<li>Must be from Sydney</li>
	<li>Good knowledge of Sydney CBD and surrounding areas</li>
	<li>1 page cover letter only detailing previous ACYA experience, relevant &amp; special skills and past work experience</li>
	<li>Most recent curriculum vitae</li>
	<li>Applications are to be sent to <a href="mailto:acya-conference-2014@acya.org.au">acya-conference-2014@acya.org.au</a> with the subject line BYLW-VOLUNTEER-[YOUR NAME] by <strong>1 August 2014 5pm (AEST)</strong></li>
</ul>
<strong>Why should you apply?</strong>
<ul>
	<li>CV building experience – practical, professional and hands on.</li>
	<li>Develop organisational, leadership &amp; teamwork skills</li>
	<li>Great networking opportunity</li>
	<li>Meet key people in the Australia China Space and gain insights from distinguished speakers and workshops throughout.</li>
	<li>Food &amp; drinks will be provided and you will be attending the conference just like the delegates. This is rare to receive from an student based organisation, especially one that spans multiple days and nights</li>
</ul>
&nbsp;