---
title: Testimonials
menu: main
---

> ![Julie Bishop](/images/profiles/Julie Bishop.jpg)
> Events like ACELS, which unite passionate and talented youth in the Australia-China space, are essential to nurture the future of the bilateral relationship. Strengthening people-to-people connections and enhancing youth engagement is at the heart of the New Colombo Plan, which is supporting more than 4700 Australian undergraduates to live, study and work in China.  
> -- Julie Bishop, Former Minister for Foreign Affairs 

> ![Jan Adams](/images/profiles/Jan Adams.jpg)
> The people-to-people connections between Australia and China are the real strength of the relationship … ACELS is a particularly important part of that, because we need young leaders to have full exposure to each other in China and Australia to keep improving and expanding these networks.  
> -- Jan Adams, Australia’s Ambassador to the People’s Republic of China.