---
title: Previous Sponsors
linkTitle: Previous Sponsors
identifier: 'previous-sponsors'
menu:
  main:
    parent: 'sponsorship'
---

{{< figure class="logo" src="/images/logos/Schwarzmann Scholars.png" title="Schwarzmann Scholars" >}}
{{< figure class="logo" src="/images/logos/King & Wood Mallesons.png" title="King & Wood Mallesons" >}}
{{< figure class="logo" src="/images/logos/Herbert Smith Freehills.png" title="Herbert Smith Freehills" >}}
{{< figure class="logo" src="/images/logos/Australian Government.svg" title="Australian Government" >}}
{{< figure class="logo" src="/images/logos/Trade Victoria.png" title="Trade Victoria" >}}
{{< figure class="logo" src="/images/logos/Australia-China Business Council.jpg" title="Australia-China Business Council" >}}
{{< figure class="logo" src="/images/logos/China Studies Centre.jpg" title="China Studies Centre" >}}
{{< figure class="logo" src="/images/logos/Westpac.png" title="Westpac" >}}
{{< figure class="logo" src="/images/logos/Department of Foreign Affairs and Trade.png" title="Department of Foreign Affairs and Trade" >}}
{{< figure class="logo" src="/images/logos/Geoff Hardy.jpg" title="Geoff Hardy" >}}
{{< figure class="logo" src="/images/logos/KPMG.svg" title="KPMG" >}}
{{< figure class="logo" src="/images/logos/Foundation for Australian Studies in China.jpg" title="Foundation for Australian Studies in China" >}}
{{< figure class="logo" src="/images/logos/Australian Studies Centre.png" title="Australian Studies Centre" >}}
{{< figure class="logo" src="/images/logos/ShineWing.png" title="ShineWing" >}}
{{< figure class="logo" src="/images/logos/Australia-China Council.svg" title="Australia-China Council" >}}
{{< figure class="logo" src="/images/logos/Austrade.svg" title="Austrade" >}}
{{< figure class="logo" src="/images/logos/Confucius Institute.png" title="Confucius Institute" >}}
{{< figure class="logo" src="/images/logos/The University of Melbourne.png" title="The University of Melbourne" >}}