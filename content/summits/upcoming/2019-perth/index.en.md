---
title: ACELS Perth 2019
featured_image: /images/Skyline.jpg
menu:
  main:
    parent: upcoming-summits
linkTitle: 2019 Perth
---

The ninth iteration of the Australia China Youth Association signature initiative - the Australia China Emerging Leaders Summit is designed to unite young people passionate about the bilateral relationship. This year the conference will be held in Perth, Western Australia. Western Australia is Australia’s only state that shares a common time zone with China, moreover the economic importance of Western Australia cannot be understated as it accounts for over 60% of Australia’s total merchandise exports to China in 2017 and 80% of China's investment into Australia over the last five years. With just under 100 delegates, this will be the largest summit held to date connecting youth across the Australia-China space.

- 80 Delegates
- 18 Events
- 9 ACELS
- 4 Days
- 2 Countries
- 1 City

Throughout the conference delegates will be presented with a new theme every day, reflecting of the current Australia-China environment, the four themes will be:

- **Human Mobility**: A Globalised World
- **Diplomacy**: The Australia China Narrative
- **International Trade**: Imports and Exports
- **Soft Power**: Influence and Culture

*If you have any questions, please message the team at <perth@acels.org>.
