---
title: ACELS Perth 2019 Delegate FAQ
featured_image: /images/Skyline.jpg
menu:
  main:
    parent: upcoming-summits
linkTitle: 2019 Perth
---


<style>
.entry-content h2 {
  color: #BE1302;
}
</style>

*Current as of June 2019*

## The Summit

### What is the Australia-China Emerging Leaders’ Summit (ACELS)? 

ACELS is the Australia-China Youth Association’s (ACYA) signature event, and a key platform to unite and enrich a new generation of Australia-China literate leaders. Over four days, delegates are exposed to highly topical and wide-ranging issues in the Australia-China space by engaging with business, diplomatic, cultural, and academic experts at the forefront of their fields, in seminars, workshops and panels. 

ACELS Perth is the groundbreaking, first-ever Australia-China youth conference in Western Australia. The Summit will be held on 24-27 July 2019 in Perth and will bring together 80 delegates from Australia and China to discuss a wide range of topics in the multifaceted bilateral relationship. ACELS Perth will feature a variety of interactive presentations, workshops and seminars, led and facilitated by some of the most experienced professionals in the space and their industry. 

<!--more-->

### Why should I attend the Summit? 

Delegates will have the opportunity to connect and build friendships with young professionals, industry leaders and fellow delegates through networking, social, and cultural events. They will also be exposed to the most topical, pressing, and innovative topics in the Australia-China space.

## Application

### What is the application process? 

To attend ACELS Perth, delegates must successfully complete the following steps: 

- Fill out and submit an [application form](https://forms.gle/uVSZD9H8bdMvaXVBA) before Sunday 5th May (11:59pm AWST/CST);
  If unable to submit the form online, fill out [this PDF version](/wp-content/uploads/ACELS-Perth-Delegate-Application-Form.pdf) and email to <delegates_acels@acya.org.au> with the subject "*ACELS Perth 2019 Delegate Application*".
- If accepted, delegates must then pay the $180 AUD Delegate Fee; and
- Closer to the date of the Summit, delegates will also fill out a form to provide further details so we can make sure your experience is as smooth as possible.

### Who is eligible to apply?

Anyone between the ages of 18 and 30 demonstrating a keen interest for the relationship between Australia and China whether it be culturally, professionally or both. Please note previous involvement with ACYA is not a prerequisite to attend ACELS Perth.

### When will I be notified of the application outcome? 

Applicants will be notified within 2 weeks of applications closing of the outcome of their application.

### I have been waitlisted for the Summit. What happens now? 

After the registration deadline has passed for accepted delegates, we will notify waitlisted delegates if there are still positions available.

### How and when will delegates be selected?

In our selection process, we aim to recruit a diverse group of delegates from a range of disciplines at the forefront of the Australia-China space. We are looking for delegates who are between the age of 18-30 and interested in the Australia-China relationship, and emulate the Summit’s theme and vision. 

## Travel 

### When is ACELS Perth? {#when}

ACELS Perth will take place from Wednesday 24th July (noon) to Saturday 27th July (midday). There may also be some unofficial optional events taking place on the afternoon and evening of Tuesday the 23rd. Delegates are recommended to arrive by the evening of Tuesday the 23rd. 

### Which airport should I book my ticket to?

Book your ticket to Perth Airport as it is the closest to the location of ACELS. 

### What time should I be booking my flights to arrive and leave?

We recommend that you book your flights to arrive by the evening of the 23rd of July, but bear in mind that this requires an extra day of accommodation that must be organised and booked by yourself.

The Summit will aim to finish around midday on Saturday, 27th July, but please make sure you leave plenty of time to move from the Summit to the airport with your belongings.

## Accommodation / Expenses 

### I live in Perth, can I stay at home (instead of the provided accommodation)? 

Delegates are expected to stay with the group at the accommodation. This will allow you to get to know other delegates, which is part of the ACELS experience! Note, the Delegate Fee is a fixed amount and there is no discount if you choose not to stay at the accommodation provided for the Summit.

### Where is the accommodation? 

Accommodation will be at [Hostel G](https://www.hostelgperth.com/rooms.php), located at 80 Stirling St, Perth WA 6000.

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13541.590454383551!2d115.864614!3d-31.9501082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7bb1da08ed0473fe!2sHostel+G!5e0!3m2!1sen!2sau!4v1560065086469!5m2!1sen!2sau" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

Wherever possible, delegates will stay in rooms divided by gender, but this cannot be guaranteed. If you have a strong preference about staying in a gender-separate room, please contact the Delegates Director, <delegates_acels@acya.org.au>, and we can arrange it for you.

If you wish to book an extra night before or after the event, you can email the Delegates Director at <delegates_acels@acya.org.au> with the chosen day and we can arrange for a reduced price of $23.20/night.

### How much is the Delegate Fee?

The Delegate Fee will be $180 AUD. 

### What does the Delegate Fee cover?

The Delegate Fee covers:

- Three nights’ excellent quality accommodation (Wednesday, Thursday and Friday nights)
- Access to Summit workshops, seminars and networking events
- All meals including Networking evening and Gala dinner
- All transportation within Perth to and from Summit venues
- Professional group and individual photography and videography

The Delegate Fee does not cover:

- Travel costs to and from Perth (including to and from the airport)

## Schedule

### What does the ACELS Perth program include?
	
ACELS will feature an intensive four-day program of workshops, panel discussions, speaker events and networking. These sessions will focus on different areas, such as:

- Diplomacy;
- Tourism;
- Trade and Investment;
- Education;
- Media;
- Creative Arts;
- Sports;
- Health
- Environment
- Economics; and/or 
- Public Policy.

A more detailed schedule and summit theme are currently being confirmed, and will be sent to successful delegates as soon as it is finalised, as part of the Delegate Pack.

### Where will the Summit be held?

ACELS Perth will be held in various key locations in the heart of Perth. More specific details regarding location will be included in the Delegate Pack.

### Do I need to prepare for the Summit?

We recommend that you do some personal research on some of the key topical themes in the Australia-China space. We will also provide resources to delegates in the lead up to the Summit, as well as information on the speakers and presenters, that will help you to start thinking about the main questions and issues.

### Do I need to able to speak Mandarin to attend the Summit?

As the Summit will be held mostly in English, there is no requirement for you to have any prior Chinese Language knowledge.  
### Is there a Summit Schedule available?
The ACELS Perth Team is in the process of finalising a Summit Schedule. This will be provided if you are accepted as a delegate to attend the Summit.

## Scholarships 

### Are there scholarships available? 

Yes, there are a few equity scholarships available to attend ACELS Perth. Each applicant will be assessed on the strength of their application, as well as their eligibility to demonstrate one or more of the following criteria: 

- Financial hardship;
- Disadvantaged background;
- Regional or remote disadvantages; and/or
- Other hardship/disadvantage.

If you wish to apply for this scholarship, please send an email to the Delegate Manager at <delegates_acels@acya.org.au>, with a paragraph (no more than 200 words) detailing why you would like to apply for the equity scholarship and outlining your current disadvantage/hardship.

Please include any relevant supporting documentation. 

### How much will the scholarships be worth?

Scholarship recipients will not be required to pay the $180 AUD delegate fee. 

## Other

### Are all the workshops and events for the Summit compulsory to attend?

You must attend ALL workshops, seminars and social events such as networking sessions and the Finale Gala.

### Will there be free time to explore Perth?

The ACELS Perth programme is, at its core, designed to be quite intense. However, the organised activities will give you ample opportunity to get to know the heart of Perth.  Rest assured, you will have time in the evenings to explore the beautiful city that is Perth.

### What should I wear to ACELS?

Delegates are expected to be wearing business attire during seminars, workshops, and networking events. The dress codes for the finale gala is black tie.

### Where can I find the Privacy Policy, Terms and Conditions, and Welfare Policy for the Summit?

The ACELS Privacy Policy, Terms and Conditions, and Welfare Policy can be accessed by emailing the ACELS Delegates team at <delegates_acels@acya.org.au>. 

### I am coming from a foreign country and require a visa to enter Australia, can the ACELS team help me secure one?

We recommend you apply for an Instant Tourist Visa (also known as the Electronic Travel Authority or ETA Visa) if you are coming from Taiwan or Hong Kong and the [Subclass 600 Visa](//immi.homeaffairs.gov.au/visas/getting-a-visa/visa-listing/visitor-600) if you are coming from Mainland China. Please contact the ACELS Delegates team on <delegates_acels@acya.org.au> for more information or if you require a formal invitation letter as part of your visa application letter.

Please note that as we aren’t representatives of government or immigration departments, we can’t guarantee the authenticity of any visa advice and any information we provide has been found on government websites.

### I have more questions, who can I contact for more information?
Feel free to contact the ACELS Delegates team on <delegates_acels@acya.org.au> — they’re more than happy to assist!
