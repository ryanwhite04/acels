---
title: Australia-China Emerging Leaders Summits
linkTitle: Summits
menu:
  main:
    identifier: summits
---

The Australia-China Emerging Leaders Summit (ACELS) is the signature initiative of the Australia-China Youth Association (ACYA), designed to up-skill and engage young leaders interested in the Australia-China bilateral relationship.

The Australia-China Emerging Leaders Summit, in both Australia and China, delivers an intensive program focused on fostering professional development, providing networking and up-skilling opportunities and people to people connections.

In this way, ACELS equips summit delegates with the tools needed for success in a world where the Asia-Pacific’s importance on the world stage is undeniable.

ACYA also believes that by fostering strong relationships between delegates, we ensure that Australia’s future in the Asia-Pacific is invested in a generation that understands the culturally unique region in which it is situated.

ACELS brings together people from all over the country, and even multinationally, to one buzzing location. Delegates not only learn from the intensive program but from each other and their experiences.

The various backgrounds and values of each individual, when collaborated, brings a new dimension of ideas and solutions to real-world issues.

Whether a delegate has been in ACYA previously, or new to the association, ACELS provides the perfect foundation for each person to grow, learn, and be world-changers.
