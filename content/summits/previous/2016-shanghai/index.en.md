{
  "title": "ACELS Shanghai 2016",
  "author": "ACYA",
  "date": "2016-01-11T09:57:13+00:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "ACELS 2016 Shanghai-0729",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0729.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0731",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0731.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0734",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0734.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0735",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0735.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0741",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0741.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0745",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0745.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0758",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0758.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0762",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0762.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0769",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0769.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0772",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0772.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0776(1)",
      "type": "image",
      "src": "ACELS-2016-Shanghai-07761.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0776(2)",
      "type": "image",
      "src": "ACELS-2016-Shanghai-07762.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0776",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0776.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0780(1)",
      "type": "image",
      "src": "ACELS-2016-Shanghai-07801.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0780(2)",
      "type": "image",
      "src": "ACELS-2016-Shanghai-07802.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0780",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0780.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0782",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0782.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0792",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0792.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0799",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0799.jpg"
    },
    {
      "title": "ACELS 2016 Shanghai-0813",
      "type": "image",
      "src": "ACELS-2016-Shanghai-0813.jpg"
    }
  ]
}
<img class="aligncenter size-medium wp-image-23270" src="http://www.acya.org.au/wp-content/uploads/2015/07/logotext3-initiative-1-300x245.png" alt="logotext3 initiative" width="300" height="245" />

ACYA is excited to bring the premier youth development forum for youth engaged in the Australia-China space to China for the first time! ACELS China will provide a platform for delegates to share their ideas on the challenges and opportunities presented by the bilateral relationship.

Delegates will be exposed to speakers from a variety of backgrounds and industries with expertise in the bilateral relationship through a number of workshops and networking and social events during the Summit.

Speakers and other guests will be locally based professional leaders in their respective fields in China. Information regarding speakers and guests will be published pending final confirmation of their attendance via social media and this page.

<!--more-->

- Date: Friday 19th February - Sunday 21st February, 2016
- Location: Shanghai

Applications have closed

## Meet the Project Team

- [Jimmy Zeng - ACYA President](#president)
- [David Douglas - Project Manager](#manager)
- [Will Fong - Operations Manager](#operations)
- [Georgia Sands - Delegates Manager](#delegates)

---

## Jimmy Zeng - President {#president}

<img class="wp-image-22024 alignleft" src="http://www.acya.org.au/wp-content/uploads/2013/04/Jimmy-Zeng.jpg" alt="Jimmy Zeng" width="101" height="150" />

Jimmy Zeng studied a dual Bachelors in Electrical Engineering (Hons) and Economics at the University of Queensland. Since 2014, he has been under the employ of Royal Dutch Shell Plc., as an Engineering graduate. In 2011, under an Endeavour Australia Cheung Kong Award, he studied Economics at the Chinese University of Hong Kong, before returning to take up a Chapter Executive position at the University of Queensland. He was subsequently elected UQ President in 2012, and then Australia Manager in 2013. As a Chinese born Australian, Jimmy relishes being part of the only youth NGO run by members, for members, that strives to develop cross-cultural ties and provide unique career, education and social opportunities to members on both sides of the Australia-China bilateral relationship.

Jimmy merges his interests in Energy, Economics and Australia-China affairs by conducting research into energy markets as a member of the Institute of Electrical &amp; Electronic Engineers, with particular interest in how China’s burgeoning energy demands will influence the global economy. In between commitments, Jimmy enjoys skiing, diving or hiking, and if the time for that is not forthcoming, catching up on an ever-growing personal library whilst appreciating the simple pleasure of a pot of pu’er tea.

---

## David Douglas - Project Manager {#manager}

<img class="alignleft wp-image-22025" src="http://www.acya.org.au/wp-content/uploads/2013/04/David-Douglas.jpg" alt="David Douglas" width="100" height="150" />

David is currently completing the final year of his Juris Doctor at the University of Melbourne. Prior to that, he completed a Bachelor of Arts (Chinese Language, Politics) and a Diploma in Languages (Russian). He has previously studied in both China and Taiwan on scholarships and has clerked with leading commercial law firms in Hong Kong and Australia. David also has previous work experience at the Australia China Business Council where he was responsible for Business Development.

David has been involved with ACYA since 2009 when he became a committee member of the University of Melbourne Chapter and was President of the Chapter between 2011-2014. David has thoroughly enjoyed his longstanding participation in ACYA and has personally experienced the many benefits that being involved with ACYA can bring. Upon graduating at the end of 2015, David is looking to commence a career as a commercial lawyer and plans to use his language and legal skills to facilitate increased bilateral economic activity between Australia and China.

---

## William Fong - Operations Manager {#operations}

<img class="alignleft wp-image-22027" src="http://www.acya.org.au/wp-content/uploads/2013/04/William-Fong.jpg" alt="William Fong" width="100" height="150" />

William Fong (邝捷盛) is the sales support executive at Dragon Trail Interactive, a Chinese digital marketing company. Specialising on the digital and technological side of the tourism and travel industry, William focuses primarily on Chinese consumer behaviour in outbound travel and tourism.

Completing his studies at the ANU under a Bachelor of Commerce and Bachelor of Asian Studies, William has studied abroad in China on various occasions, including at Tsinghua University in Beijing, Jiaotong University in Shanghai and Chinese archaeology across Zhejiang province. William has also spent time working in China as a business development manager for a small foreign business and construction consulting firm, and as an e-commerce project officer for Austrade in Shanghai.

As a very active person, William enjoys many different sports including badminton, rugby and touch, while also being a traditional Chinese lion dance trainer with over 12 years of performance experience. As a beneficiary of much of ACYA’s great work, William is excited to finally give back to the fantastic community that has gathered under ACYA and hopes to build on its legacy and strengthen Sino-Australian relations across Greater China.

---

## Georgia Sands - Delegates Manager {#delegates}

<img class="alignleft size-medium wp-image-23787" src="http://www.acya.org.au/wp-content/uploads/2016/01/Georgia-Sands-227x300.jpg" alt="Georgia Sands" width="105" height="150" />

Georgia Sands is embarking upon her final year of Law, French and Chinese language studies at Griffith University in Brisbane, Australia. Georgia’s interest in China was sparked by a one-month Chinese language and culture course in Hangzhou at the end of her first year of university studies. As a Prime Minister’s Australia Asia Endeavour Award recipient, Georgia undertook a semester-long exchange at the University of Hong Kong and completed internships with international law firms in Hong Kong and Beijing.

Georgia was the founding President of the Griffith University ACYA Chapter and joined the National Executive Committee in 2015 as the Portfolio Manager. China continues to engage and excite Georgia through its fascinating ancient history, artful tea culture and assortment of regional culinary delicacies. Georgia enjoys perfecting her fried rice methodology, which she can now whip up in the flash of a gas flame. Georgia has also been known to whip up the occasional qipao (旗袍) – a traditional Chinese form of dress – for a China-related event!

[gallery ids="24783,24784,24785,24786,24787,24788,24789,24790,24791,24792,24793,24794,24795,24796,24797,24798,24799,24800,24801,24802,24803,24804,24805,24806,24807,24808,24809,24810,24811,24812,24813,24814,24815,24816,24817,24818,24819,24820,24821,24822,24823,24824,24825,24826,24827,24828,24829,24830,24831,24832,24833,24834,24835,24836,24837,24838,24839,24840,24841,24842,24843,24844,24845,24846,24847,24848,24849,24850,24851,24852,24853,24854,24855,24856,24857,24858,24859,24860,24861,24862,24863,24864,24865,24866,24867,24868,24869,24870,24871,24872,24873,24874,24875,24876,24877,24878,24879,24880,24881,24882" orderby="rand"]