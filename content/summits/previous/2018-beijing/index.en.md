{
  "title": "ACELS Beijing 2018",
  "author": "ACYA",
  "date": "2017-11-28T02:39:21+00:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": []
}
ACYA is pleased to announce that the Australia China Emerging Leaders Summit (ACELS) will be returning to Beijing from 22-25 February 2018.

ACELS Beijing 2018 will be guided by the theme: Broadening the Bilateral: Embracing a New Asian Paradigm – reflecting unparalleled opportunities and new challenges that come with a region undergoing massive transformation.

<!--more-->

<img class="alignnone size-large wp-image-26904" src="http://www.acya.org.au/wp-content/uploads/2017/11/ACELS-Beijing-Flyer-1024x1365.png" alt="" width="525" height="700" />

Now in its fifth year, the Australia China Emerging Leaders Summit (ACELS) is ACYA’s signature initiative. It has become the premier event to unite, upskill and engage a new generation of Australia-China literate leaders, giving them opportunities to connect with business, government and industry leaders shaping the future of this bilateral relationship and the region at large.

Over 60 young leaders from all corners of Australia will be invited to partake in this four-day intensive program, featuring workshops, panel discussions, speaker events and networking. We will explore how unprecedented changes occurring in our region are shaping the future of the Australia-China relationship in a number of thematic areas, including innovation and youth entrepreneurship, education and social mobility, trade and responsible investment, climate change, social media and soft power. The summit will be guided by the theme: Broadening the Bilateral: Embracing a New Asian Paradigm – reflecting unparalleled opportunities and new challenges that come with a region undergoing massive transformation.

Earlier this year the ACELS was held in Beijing in collaboration with Model APEC (Asia-Pacific Economic Cooperation). The Summit brought together over 60 delegates to discuss a range of topics including artificial intelligence, foreign policy and rural education, and engaged guests and speakers from the highest levels of government, business, and media. Now we will return to China’s capital, supported by the Australia-China Council, to deliver the best ACELS to date.

Apply <a href="https://acelsbeijing2018.typeform.com/to/Bo1SrA" target="_blank" rel="noopener">here</a> before <b>15 December 2017 (11:59pm AEST)</b> if you would like to attend. Before applying, it is essential that you read the<strong> <a href="http://www.acya.org.au/wp-content/uploads/2017/11/ACELS-Beijing-FAQ.pdf">ACELS Beijing FAQ</a>.</strong>

[embed]https://www.youtube.com/watch?v=w1fOo5dIEeQ[/embed]

The Australia China Youth Association (ACYA) is a non-profit devoted to promoting engagement between students and young professionals in both Australia and China. We position ourselves as the preeminent stakeholder for youth in the crucial bilateral relationship between Australia and China. We strive to bridge the gap between Australia and China and to develop a generation of young professionals who are able to identify, seize and create opportunities for closer bilateral ties and greater mutual understanding between our two countries.

Contact
ACELS Beijing Project Team
<a href="mailto:acels@acya.org.au" target="_blank" rel="noopener">acels@acya.org.au</a>