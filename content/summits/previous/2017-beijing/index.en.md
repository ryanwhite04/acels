{
  "title": "ACELS Beijing 2017",
  "author": "ACYA",
  "date": "2016-10-23T13:00:11+00:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "IMG_2563",
      "type": "image",
      "src": "IMG_2563.jpg"
    },
    {
      "title": "IMG_2568",
      "type": "image",
      "src": "IMG_2568.jpg"
    },
    {
      "title": "IMG_2570",
      "type": "image",
      "src": "IMG_2570.jpg"
    },
    {
      "title": "IMG_2573",
      "type": "image",
      "src": "IMG_2573.jpg"
    },
    {
      "title": "IMG_2577",
      "type": "image",
      "src": "IMG_2577.jpg"
    },
    {
      "title": "IMG_2580",
      "type": "image",
      "src": "IMG_2580.jpg"
    },
    {
      "title": "IMG_2581",
      "type": "image",
      "src": "IMG_2581.jpg"
    },
    {
      "title": "IMG_2582",
      "type": "image",
      "src": "IMG_2582.jpg"
    },
    {
      "title": "IMG_2585",
      "type": "image",
      "src": "IMG_2585.jpg"
    },
    {
      "title": "IMG_2587",
      "type": "image",
      "src": "IMG_2587.jpg"
    },
    {
      "title": "IMG_2588",
      "type": "image",
      "src": "IMG_2588.jpg"
    },
    {
      "title": "IMG_2589",
      "type": "image",
      "src": "IMG_2589.jpg"
    },
    {
      "title": "IMG_2594",
      "type": "image",
      "src": "IMG_2594.jpg"
    },
    {
      "title": "IMG_2596",
      "type": "image",
      "src": "IMG_2596.jpg"
    },
    {
      "title": "IMG_2598",
      "type": "image",
      "src": "IMG_2598.jpg"
    },
    {
      "title": "IMG_2602",
      "type": "image",
      "src": "IMG_2602.jpg"
    },
    {
      "title": "IMG_2610",
      "type": "image",
      "src": "IMG_2610.jpg"
    },
    {
      "title": "IMG_2612",
      "type": "image",
      "src": "IMG_2612.jpg"
    },
    {
      "title": "IMG_2598",
      "type": "image",
      "src": "IMG_2598.jpg"
    },
    {
      "title": "cropped-IMG_1507.jpg",
      "type": "image",
      "src": "cropped-IMG_1507.jpg"
    }
  ]
}
From 17-19 February 2017 the Australia China Youth Association, in collaboration with Model APEC (Asia-Pacific Economic Cooperation) delivered the Australia China Emerging Leaders Summit (ACELS). The Summit was held in Beijing, and brought together young leaders, passionate about the Australia-China Space to engage on topics ranging from innovation to diplomatic policy to social issues between Australia and China.

<!--more-->

<a href="http://www.acya.org.au/wp-content/uploads/2017/04/cropped-IMG_1507.jpg"><img class="alignnone size-medium wp-image-25885" src="http://www.acya.org.au/wp-content/uploads/2017/04/cropped-IMG_1507-300x180.jpg" alt="" width="300" height="180" /></a>

The first session delved into the realm of Media in China. The keynote speaker was Stephen McDonnell, BBC’s China Correspondent.His very down to earth “fire-side chat” with Timeout Beijing’s Frank Sweet revealed many of the interesting observations and “behind the story” experiences throughout his 10 years in China.

At the Innovation Session, guest speaker, Qian Fei Fei, a Senior Product Marketing Manager at Microsoft, gave the delegates visions into the rapidly developing world of Artificial Intelligence (AI). Delegates were separated into teams to compete in a PITCH competition themed around AI, and coached by guests including IP Counsellor to China, David Bennett. These sessions were held at one of the newest locations of UR Work, China’s equivalent of the world’s largest co-working space provider, WeWork.

To wrap up an action-packed first day, delegates attended a Business Networking Evening <span style="font-weight: 400;">held with the support of the Australian Embassy Beijing, which brought together established leaders of the political, business and academic communities in Beijing including Australia’s Deputy Head of Mission to China, Mr Gerald Thomson, and former Chinese Ambassador, Mr Jiang Chengzhong.</span>

<a href="http://www.acya.org.au/wp-content/uploads/2017/04/IMG_2598.jpg"><img class="alignnone size-medium wp-image-25878" src="http://www.acya.org.au/wp-content/uploads/2017/04/IMG_2598-300x200.jpg" alt="" width="300" height="200" /></a>

The final day began with a panel discussion surrounding Public Policy between Mr Gerald Thompson, David Kelly – Director Research at China Policy, and Ms Hu Dan, Professor of Australian studies from Beijing Foreign Studies University. Prior to the summit, ACYA had invited submission from members and delegates in regards to the Australian Government’s Foreign Policy White paper. A select few submissions were discussed and workshopped combining thoughts from delegates with valuable insights from the panelists.

During the afternoon session on Education and Social Issues, delegates heard from pioneers of the Chinese social and education sectors including, Keren Wong, Cofounder of Tech NGO Xueyuanqiao and Steven Wang, CEO of Yiqiao China on their projects and social responsibility. Cindy Jensen, founder of BOLDMOVES China, then spoke about the non-profit organisation, Educating Girls of Rural China (EGRC), before the delegates heard from one of their peers whose life had been transformed by the EGRC emphasising the impact of NGOs.

The February 2017 ACELs brought together passionate young minds, to participate in a program filled with panel discussions and workshops, designed to foster professional development, and strengthen networks in the next generation of leaders.

[embed]https://www.youtube.com/watch?v=w1fOo5dIEeQ[/embed]