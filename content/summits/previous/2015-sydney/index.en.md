{
  "title": "ACELS Sydney 2015",
  "author": "ACYA",
  "date": "2016-01-11T15:28:49+00:00",
  "featured_image": "/images/logos/tall.jpg"
}

The Australia-China Youth Association (ACYA) is proud to collaborate with the brightest minds from the Australia-China space to deliver the Australia-China Emerging Leaders Summit (ACELS).
ACELS is one of ACYA’s key events for youth development. In 2015, ACELS delegates participated in forums, workshops, round-table sessions, a varied speaker program and networking sessions.
The 2015 ACELS delegates were handpicked from a wide range of applicants. They are the youth of our nation who will be at the forefront of shaping Australia-China relations in the Asia-Pacific century.

<!--more-->

- **Date**: Thursday 30th July – Sunday 2nd August 2015.
- **Location**: Sydney
- [**Report**](Publication.pdf)

## Meet the ACELS Australia Project Team

- [Sam Johnson - Project Manager](#manager)
- [Amber Miller - Greenman - Incubation Manager](#incubation)
- [Yu Feng Nie - Communications Manager](#communications)
- [Cynthia Yuan - Operations Manager](#operations)
- [Victor Luo - Logistics Manager](#logistics)

---

## Sam Johnson - Project Manager {#manager}

![Sam Johnson](/images/profiles/Sam Johnson.jpg)

> As project manager, I’ll be heading up the project team to make sure the Summit’s busy schedule of events are executed smoothly. We’re here to make sure the Summit is as enriching and fun for our delegates as possible, so feel free to get in touch and say "Hi" during incubation!

I can’t seem to stay in Sydney for too long before planning my next trip overseas. A keen traveller, I first visited China on a whim in 2013, where I was captivated by the Chinese culture while roaming Beijing during the Spring Festival fireworks.
In 2014 I returned to China as a delegate on the U.S. Studies Centre’s inaugural study program to Fudan University in Shanghai. I subsequently joined ACYA as a committee member in the Sydney University chapter, where I now serve as Treasurer. I further explore my interest in China as a Junior Policy Associate with the China Studies Centre.
I have experience with Encompass Corporation as a clerical assistant and I have interned with Asialink Business and the U.S. Consulate General in Sydney. I am completing an undergraduate degree in Government an International Relations at the University of Sydney.

---

## Amber Miller-Greenman - Incubation Manager {#incubation}

[Amber Miller-Greenman](/images/profiles/Amber Miller-greenman.jpg)

> I am your friendly Incubation Manager! I am here to help you prepare for ACELS so that you get the most out of your ACELS experience.
> I will be on Facebook, LinkedIn, Twitter and Wechat and will provide you with articles, stories and general information that will help you to be ready for the summit.

Amber Miller-Greenman was born in New Zealand and moved to Australia in 2000. She has a multicultural background; in depth knowledge of Chinese culture and speaks Cantonese and Mandarin.
She enjoys high paced environments and loves the challenges associated with dynamic workplaces. She has worked for Virgin Australia and has represented Australia at Shanghai World Expo. Amber was a Future of Tourism Think Tank delegate in the China Australia Millennial Project.
Amber is now a Destination Specialist at Tourism and Events Queensland, the peak Destination Marketing Organisation for the state of Queensland.

---

## Yu Feng Nie - Communications Manager {#communications}

![Yu Feng Nie](/images/profiles/Yu Feng Nie.jpg)

> I will be responsible for ACYA’s external communications for the summit and here to help with any general questions about ACYA!
> Want to know more about what we do beyond ACELS? Contact me at <mailto:communications@acya.org.au>.

I am studying a Bachelor of Medical Studies/Doctor of Medicine at the University of New South Wales. Born in Shanghai but raised in Melbourne, I speak Shanghainese, Mandarin and English.
I spend my holidays visiting China keeping in touch with my cultural roots while also acting as an ad-hoc tour guide to friends from Australia. My awareness of the cultural nuances in the Australia-China space spurs my passion to promote ACYA.
I am the Communications and Marketing Director as well as the Vice-President (P2P) at the AYCA UNSW chapter. Outside of my study and work commitments, I enjoy watching Chinese TV, keeping up to date with Australia-China affairs or sitting back with a cup of coffee while listening to some classical music.
Ultimately I hope to be involved in the healthcare sector in Shanghai.

## Cynthia Yuan - Operations Manager {#operations}

![Cynthia Yuan](/images/profile/Cynthia Yuan.jpg)

> As operations manager, I will be your first point of contact if you have any queries regarding logistics e.g. flights, accommodation, schedule. My philosophy is that there are no silly questions! Not only is it my job to ensure you all know what’s happening during the summit, but also to ensure you’re all having a good time. So if you’re happy, I’m happy; we’re all happy.
> If you have anything you’re unsure about or you just want to chat, feel free to shoot me a message via Facebook, WeChat or email.

I was born and raised in Sydney, and I am currently studying a Bachelor of Laws with a Bachelor of Commerce at Macquarie University. Although seeming racially ambiguous to many, I am in fact of full Chinese descent, my family origins tracing back to Jiangsu, Nantong. I am fluent in English and Mandarin.
Growing up as an ABC in Australia, I have always had trouble coming to terms with my Asian background during my childhood and in order to fit in at school, I often suppressed my roots, only speaking Mandarin at home to my family. It was only until joining ACYA and meeting like-minded individuals that I was able reconcile my inner cultural dichotomy and develop a genuine passion for the China-Australia space. Thus, a beautiful romance blossomed and I am now the proud President of ACYA’s Macquarie University Chapter.
In my free time, I enjoy exploring and photographing the nooks and crannies of Sydney, devouring a hearty eggs benedict, immersing myself in a cult classic/indie/french film or grooving along to some Triple J. Yes, I sound like a walking cliche but I embrace it.

---

## Victor Luo - Logistics Manager {#logistics}

![Victor Luo](/images/profiles/Victor Luo.jpg)

> I am responsible for overseeing the safety and well-being of the delegates as well as coordinating the social events for the Summit. I will be making sure that the socials are the best thing about the Summit.
> Keen for a chat? Feel free to find me on Facebook, WeChat, LinkedIn and Twitter.

I am a born and bred Chinese-Australian from Sydney and a current Juris Doctor law student at the University of New South Wales.
My Chinese background traces from the southern region of China and thus I am able to speak both Cantonese and Mandarin.
During my undergraduate studies, I spent time in Beijing attending Tsinghua and Beijing Language and Culture University.
It is from this exchange adventure that I fell in love with China and was able to reconcile my dualistic identity as both a Chinese and an Australian.
I enjoy planning thrilling adventures and seeking new challenges to broaden my horizons within the China-Australia space.
I am also the President of the UNSW Chapter of ACYA.
Outside of university, work and ACYA commitments, I enjoy cooking Cantonese dishes, playing the piano, going out for a run with my gorgeous dog, and attempting to discover the secrets to what makes the perfect cheong fan 肠粉(rice noodle roll).