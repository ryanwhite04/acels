{
  "title": "ACELS Brisbane 2018",
  "author": "ACYA",
  "date": "2018-05-31T19:00:48+08:00",
  "image": {
    "type": "image"
  },
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "Brisbane Skyline",
      "type": "image",
      "src": "Brisbane-Skyline.jpg"
    }
  ]
}

ACELS Brisbane will provide a platform to examine tensions within the bilateral space, as well as the many opportunities this strong relationship holds. Key aspects of this Summit will include a Networking Evening with professionals at King & Wood Mallesons, an Alumni Mixer Evening and the Shinewing Gala Dinner.

![Brisbane Skyline](Brisbane-Skyline)

[display-posts id="27779" include_content="true" image_size="true" wrapper="div"]

<!--more-->

The Australia China Youth Association (ACYA) is a non-profit devoted to promoting engagement between students and young professionals in both Australia and China. We position ourselves as the preeminent stakeholder for youth in the crucial bilateral relationship between Australia and China. We strive to bridge the gap between Australia and China and to develop a generation of young professionals who are able to identify, seize and create opportunities for closer bilateral ties and greater mutual understanding between our two countries.