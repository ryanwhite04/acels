{
  "title": "ACELS Sydney 2016",
  "author": "ACYA",
  "date": "2016-05-30T13:35:00+00:00",
  "image": "",
  "categories": [],
  "tags": [],
  "locations": [],
  "organisations": [],
  "resources": [
    {
      "title": "acels16-61",
      "type": "image",
      "src": "ACELS16-61.jpg"
    },
    {
      "title": "acels16-62",
      "type": "image",
      "src": "ACELS16-62.jpg"
    },
    {
      "title": "acels16-62-edit-removed-projector-screen",
      "type": "image",
      "src": "ACELS16-62-Edit-Removed-projector-screen.jpg"
    },
    {
      "title": "acels16-63",
      "type": "image",
      "src": "ACELS16-63.jpg"
    },
    {
      "title": "acels16-64",
      "type": "image",
      "src": "ACELS16-64.jpg"
    },
    {
      "title": "acels16-65",
      "type": "image",
      "src": "ACELS16-65.jpg"
    },
    {
      "title": "acels16-66",
      "type": "image",
      "src": "ACELS16-66.jpg"
    },
    {
      "title": "acels16-67",
      "type": "image",
      "src": "ACELS16-67.jpg"
    },
    {
      "title": "acels16-68",
      "type": "image",
      "src": "ACELS16-68.jpg"
    },
    {
      "title": "acels16-69",
      "type": "image",
      "src": "ACELS16-69.jpg"
    },
    {
      "title": "acels16-70",
      "type": "image",
      "src": "ACELS16-70.jpg"
    },
    {
      "title": "acels16-71",
      "type": "image",
      "src": "ACELS16-71.jpg"
    },
    {
      "title": "acels16-72",
      "type": "image",
      "src": "ACELS16-72.jpg"
    },
    {
      "title": "acels16-73",
      "type": "image",
      "src": "ACELS16-73.jpg"
    },
    {
      "title": "acels16-1447-edit",
      "type": "image",
      "src": "ACELS16-1447-Edit.jpg"
    },
    {
      "title": "img_1666",
      "type": "image",
      "src": "IMG_1666.jpg"
    },
    {
      "title": "img_1668",
      "type": "image",
      "src": "IMG_1668.jpg"
    },
    {
      "title": "img_1670",
      "type": "image",
      "src": "IMG_1670.jpg"
    },
    {
      "title": "img_1672",
      "type": "image",
      "src": "IMG_1672.jpg"
    },
    {
      "title": "img_1731",
      "type": "image",
      "src": "IMG_1731.jpg"
    }
  ]
}
<img class="aligncenter size-full wp-image-23990" src="http://www.acya.org.au/wp-content/uploads/2016/05/image-1.png" alt="image (1)" width="500" height="350" />

ACYA is pleased to present the 2016 Australia-China Emerging Leaders Summit (ACELS). The 2016 ACELS Conference was held in Sydney from the 29-31 July 2016.

ACELS is ACYA's premier flagship initiative event aimed to bring together emerging young leaders in the Australia-China bilateral relationship since the first time it was held in 2013. The 2016 Conference delivered an intensive program filled with workshops, speaker networking events and social function events designed to foster professional development, provide networking opportunities, up-skill and further strengthen networks between the next generation of leaders.

Over 250 delegates have participated in previous ACELS Conferences and ACYA was pleased to welcome the next group of emerging young leaders to Sydney in late July. To learn more about ACELS Sydney 2016, you can read the following

- [Delegate Pack](/wp-content/uploads/2016/05/ACELS-Sydney-2016-Delegate-Pack.pdf)
- ACELS Sydney publication:
  - [Enriching a New Generation of Australia-China Literate Youth](/wp-content/uploads/2016/08/ACELS-Enriching-a-New-Generation-of-Australia-China-Literate-Youth.pdf)
  - [中文版 - 丰富新一代的中澳文化青年](/wp-content/uploads/2016/01/%E4%B8%B0%E5%AF%8C%E6%96%B0%E2%BC%80%E4%BB%A3%E7%9A%84%E4%B8%AD%E6%BE%B3%E2%BD%82%E5%8C%96%E9%9D%92%E5%B9%B4.pdf)
- [Report](/wp-content/uploads/2016/05/03-30_ACELSPublication-1-1.pdf)

[gallery ids="24488,24490,24491,24492,24493,24494,24495,24496,24497,24498,24499,24500,24501,24502,24503,24504,24505,24506,24507,24508,24509,24510,24511,24512,24513,24514,24515,24516,24517,24518,24519,24520,24521,24522,24523,24524,24525,24526,24527,24528,24529,24530,24531,24532,24533,24534,24535,24536,24537,24538,24539,24540,24541,24542,24543,24544,24545,24546,24547,24548,24549,24550,24551,24552,24553,24554,24555,24556,24557,24558,24559,24560,24561,24562,24563,24564,24565,24566,24567,24568,24569,24570,24571,24572,24573,24574,24575,24576,24577,24578,24579,24580,24581,24582,24583,24584,24585,24586,24587,24489" orderby="rand"]