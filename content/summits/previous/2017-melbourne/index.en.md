{
  "title": "ACELS Melbourne 2017",
  "author": "ACYA",
  "date": "2017-06-07T06:08:54+00:00"
}

In July 2017, seventy exceptional students and young professionals gathered in Melbourne for the Australia-China Emerging Leaders Summit (ACELS). Renowned for its laneways, rich history and cultural institutions, this vibrant city formed the perfect backdrop to explore theme of “Connecting Through Culture”.

<!--more-->

## Highlights

### The Laneway Race

On the first day, a race around the city allowed the delegates to explore Melbourne’s landmarks and get to know each other

![Laneway selfie](Laneway-Selfie.jpg)

---

### The two formal events

These created the opportunity for delegates to engage with professionals working across Australia and China:

- The Australia-China Emerging Leaders Networking Evening
- The ShineWing Australia ACELS Gala Dinner

---

### The informative and inspiring speaker sessions

- David Olsson — China Practice Consultant at King & Wood Mallesons
- Kate Ben-Tovim — Associate Director of Asia Pacific Triennial of Performing Arts
- Lucy Lv — Producer of Mandarin Radio and Online Content at SBS Australia
- And many more!

![Artist panel](Artist-Panel.jpg)

---

### The "Three Minute Thesis"

Ten delegates shared their own expertise of the Australia-China relationship from a youth perspective. Here, topics ranged from the advantages of language learning, China’s poverty alleviation project, to the experience of Chinese international students in Australia.

---

## Melbourne-based social enterprises

- Crêpes for Change at the conclusion of the Laneway Race
- The Asylum Seekers Resource Centre Catering at the Summit Opening

---

## ACYA alumni

- Councillor Phillip Le Liu - Chair of International Engagement Portfolio at City of Melbourne
- Thomas Day - External Relations & Projects Manager at the Confucius Institute
- Scott Gigante - Computational biologist at the Walter & Eliza Hall Institute of Medical Research and PhD Candidate at Yale

---

All in all, the summit provided a platform for analytical and frank discussion regarding the Australia-China relationship and, in turn strengthened friendships and professional relationships.

![Workshop Photo](Workshop-Photo.jpg)