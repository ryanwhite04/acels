var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var AssetsPlugin = require('assets-webpack-plugin');
var FaviconsPlugin = require('favicons-webpack-plugin');

let assets = new AssetsPlugin({
  filename: 'assets.json',
  path: path.join(__dirname, './data'),
  prettyPrint: true,
})

let text = new ExtractTextPlugin({
  filename: getPath => getPath('css/[contenthash].css'),
  allChunks: true
})

// let favicons = new FaviconsPlugin({
//   integrityPropertyName: 'icon',
//   logo: path.resolve(__dirname, './static/images/logos/acels.png'),
//   background: '#BE1302',
//   prefix: 'icons/[hash]/',
//   name: 'icon',
//   statsFilename: '../../data/icons.json',
//   emitStats: true,
//   inject: true,
// })

module.exports = {
  entry: './src/js/main.js',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?importLoaders=1!postcss-loader'
        })
      },
      {
        test: /\.((png)|(eot)|(woff)|(woff2)|(ttf)|(svg)|(gif))(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader?name=/[hash].[ext]"
      },
    ]
  },

  output: {
    path: path.join(__dirname, './static/dist'),
    filename: 'js/[chunkhash].js'
  },

  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules']
  },
  plugins: [assets, text,
    // favicons,
  ],
  watchOptions: {
    watch: true
  }
};